<?
class AgencePromoForfait extends Ada_Object
{
	protected $_categories = null;
	
	/** le premier identifiant des nouvelles promotions */
	const FIRST_NEW_ID = 40000;
	
	/** Liste des forfaits */
	const PROMO_TYPE_SIMPLE = 'simple';
	const PROMO_TYPE_EARLY = 'early';
	
	// Chemin pour les visuels illustrant les promotions
	const PROMO_VISUEL_PATH = 'fichiers/promos/';
	
	static public $PROMO_TYPES = array(
		self::PROMO_TYPE_SIMPLE => 'simple',
		self::PROMO_TYPE_EARLY  => 'early booking'
	);
	static public $PROMO_PERIMETRE = array(
		'ADAFR'	=> 'ADAFR Promos d�fini par ADA',
		'RESAS'	=> 'RESAS Promos d�fini par les agences'
	);
	
	
	
	static private $_FIELDS_DATE = array('depuis','jusqua','debut','fin');
	
	/** Liste des forfaits */
	static private $_FORFAITS = array(
		'vp' => array('JCVP', 'WE'),
		'vu' => array('JCVU', 'WEVU','VENVU','SAMVU','DIMVU')
	);
	
	/**
	* Renvoie la liste des forfaits ($_FORFAITS)
	*/
	static public function getForfaits()
	{
		return self::$_FORFAITS;
	}
	/**
	* Renvoie la liste des forfaits dans un seul tableau
	*/
	static public function getAllForfaits()
	{
		return array_merge(self::$_FORFAITS['vp'], self::$_FORFAITS['vu']);
	}
	/**
	* Renvoie un tableau avec la liste des visuels
	* @returns array
	*/
	static public function getAllVisuels() {
		$visuels = array();
		foreach(glob(BP.'/'.self::PROMO_VISUEL_PATH.'*.png') as $file) {
			$visuels[] = self::PROMO_VISUEL_PATH.basename($file);
		}
		return $visuels;
	}
	

	/**
	* factory
	* 
	* @param int $id
	* @param bool $tryArchive
	* @return AgencePromoForfait
	*/
	static public function factory($id, $tryArchive = false)
	{
		if (!$id) return null;
		$x = new AgencePromoForfait();
		if ($x->load($id)->isEmpty()) {
			// on essaye dans les archives
			$x->setTableName('_archive_agence_promo_forfait');
			if (!$tryArchive || $x->load($id)->isEmpty())
				return null;
			$x->setData('archive', 1);
		}
		return $x;
	}
	
	/**
	* Cr�e une promotion
	* @param array $a Liste des champs
	* @param array $errors liste des erreurs
	* @param array $summary messages indiquant les promotions cr��s
	* @return Boolean
	*/
	static public function create(&$a, &$errors, &$summary)
	{
		// champs erron�s
		$fieldsError = array();
		// si non renseign� mode toujours � "P" 
		if (!isset($a['mode'])) {
			$a['mode'] = 'P';
		}
		
		// champs obligatoires
		if (!self::_checkField($a['code_societe'], '/^[a-z0-9]{2}$/i'))
			$fieldsError[] = 'code_societe';
		if ($a['type'] && !isset(Categorie::$TYPE[$a['type']]))
			$fieldsError[] = 'type';
		if(!self::_checkField($a['montant'], '/^[\+\-]?\d{1,3}(\.\d{1,2})?$/', true))
			$fieldsError[] = 'montant';
		// agence : on en construit un tableau avec la liste des agences
		$agences = array();
		if (is_array($a['agence'])) {
			$agences = $_POST['agence'];
		} else if ($a['agence']) {
			$agences = array($a['agence']);
		} else if ($a['code_societe']) {
			$agences = array();
			$rs_agence = sqlexec("select ap.id from agence_pdv ap join agence a on ap.agence=a.id where (a.statut&1=1) and a.code_societe='".filter_var($_POST['code_societe'], FILTER_SANITIZE_STRING)."'");
			while ($row_agence = mysql_fetch_assoc($rs_agence))
				$agences[] = $row_agence['id'];
		} else {
			$agences = array(null);
		}
		foreach($agences as $agence) {
			if ($agence && !Agence::isValidID($agence)) {
				$fieldsError[] = 'agence';
				break;
			}
		}
		// forfait
		if ($a['forfait'] && !in_array($a['forfait'], self::$_FORFAITS['vp']) && !in_array($a['forfait'], self::$_FORFAITS['vu'])) {
			$fieldsError[] = 'forfait';
		}
		foreach(self::$_FIELDS_DATE as $k)
		{
			if ($a[$k]) {
				$a[$k] = date_iso($a[$k]);
				if(!self::_checkField($a[$k], '/^\d{4}-\d{2}-\d{2}$/'))
					$fieldsError[] = $k;
			}
		}
		foreach(array('jours_avant','jours_min', 'jours_max', 'km_min', 'km_max','jour_mois_debut','jour_mois_fin') as $f)
		{
			if(!self::_checkField($a[$f], '/^\d{1,4}$/'))
				$fieldsError[] = $f;
		}
		if (!$a['promo_type']) {
			$a['promo_type'] = ($a['jours_avant'] ? 'early' : 'simple');
		}
		if($a['jours_max'] && $a['jours_max'] < $a['jours_min'])
			$errors[] = 'les dur�es de location min/max ne sont pas coh�rentes';
		if($a['km_max'] && $a['km_max'] < $a['km_min'])
			$errors[] = 'les km min/max ne sont pas coh�rents';
		// v�rifier les dates
		self::checkDates($a, $errors);
		
		// categories
		if (is_array($a['categories'])) {
			foreach ($a['categories'] as $c) {
				if (!self::_checkField($c, '/^[^\s]+$/i')) {
					$fieldsError[] = 'categories';
					break;
				}
			}
			$a['categories'] = implode(',', $a['categories']);
		}
		
		// on ajoute les champs de l'�dito (si valeurs pr�sentes dans le post, elles ont la priorit�s)
		$a = array_merge(PromotionEdito::getEdito($a), $a);
		// enregistrement
		if (empty($fieldsError) && empty($errors)) {
			// pour chaque agence on cr�e un enregistrement
			foreach($agences as $agenceID) {
				// on �vite le cas o� la valeur "Tous" est s�lectionn�e avec d'autres agences
				if (!$agenceID && count($agences) > 1) continue;
				// on compl�te avec les informations de l'gence
				$data = $a;
				if ($agenceID) {
					$agence = Agence::factory($agenceID);
					if (!$agence) continue;
					$search = array(
						'/#agence_id#/'	=> $agence['id'],
						'/#agence_nom#/'=> $agence['nom']
					);
					$data['agence'] = $agence['id'];
					foreach(array('zone','reseau','code_societe') as $k) {
						$data[$k] = $agence[$k];
					}
					foreach(array('nom','slogan','slogan_iphone','description','mentions') as $k) {
						$data[$k] = preg_replace(array_keys($search), array_values($search), $a[$k]);
					}
				}
				// puis on en registre
				if (save_record('agence_promo_forfait', $data)) {
					$summary[$data['id']] = $data['nom'];
				} else {
					$errors[] = "Erreur lors de l'enregistrement de la promotion.".$data['nom'];
				}
			}
			// si tout va bien on arr�te l�
			if (empty($errors)) {
				return true;
			}
		}
		// erreurs
		foreach($fieldsError as $f) {
			$errors[] = 'Le champ '.$f.' doit �tre correctement renseign�.';
		}
		return (empty($errors) && empty($fieldsError));
	}
	
	/**
	* V�rifie le champ
	* NB : renvoie true si champ optionnel vide. Renvoie false si champ optionnel rempli incorrectement.
	* 
	* @param mixed $field
	* @param mixed $value
	* @param mixed $regex
	* @param mixed $required
	*/
	static private function _checkField($value, $regex, $required = false)
	{
		// champ OK si : (requis && regex) || (!requis && (vide || regex))
		return ($required && preg_match($regex, $value)) || (!$required && (empty($value) || preg_match($regex, $value)));
	}
	
	/**
	* V�rifie la coh�rence des dates : depuis, jusqua, debut, fin
	* Les dates doivent �tre au format ISO
	* @param array $a
	* @param array $errors
	* @returns bool
	*/
	static public function checkDates(&$a, &$errors) {
		if(($a['jusqua'] && strtotime($a['jusqua']) < strtotime($a['depuis'])) || ($a['fin'] && strtotime($a['fin']) < strtotime($a['debut'])))
			$errors[] = 'les dates ne sont pas coh�rentes';
	}
	/**
	* La promotion a-t-elle �t� utilis�e ?
	* @returns bool
	*/
	public function isUsed() {
		return getsql("SELECT COUNT(*) FROM reservation WHERE statut IN ('V','P','A','X') AND promotion=".$this->getId());
	}
	
	/**
	* Met � jour une promotion : seules les dates d'application peuvent changer
	* 
	* @param array $a
	* @param array $errors
	* @returns bool
	*/
	public function update(&$a, &$errors)
	{
		// remettre les dates au format ISO
		foreach(self::$_FIELDS_DATE as $k) {
			if ($a[$k]) {
				$a[$k] = date_iso($a[$k]);
			}
		}
		// v�riifer la coh�rence des donn�es
		self::checkDates($a, $errors);
		// v�rifier le nombre de jours avant
		if (isset($a['jours_avant']) && !preg_match('/^\d*$/', $a['jours_avant'])) {
			$errors[] = "le nombre de jours n'est pas correct";
		}
		foreach(array('jour_mois_debut', 'jour_mois_fin') as $k) {
			if (isset($a[$k]) && !preg_match('/^\d*$/', $a[$k])) {
				$errors[] = sprintf("le jour du mois (%s) n'est pas correct", $k);
			}
		}
		
		// enregistrer s'il n'y a pas d'erreurs
		if (empty($errors))
		{
			if (isset($a['actif'])) {
				// on traite les 2 impl�mentations : bouton radio dans l'OAV (resas) et case � cocher dans le back office (/admin/)
				if (is_array($a['actif'])) {
					$this->setData('actif', array_sum($a['actif']));
				} else if (preg_match('/^[01]$/', $a['actif'])) {
					$this->setData('actif', $a['actif']);
				}
			}
			foreach(self::$_FIELDS_DATE as $k) {
				if (isset($a[$k])) {
					$this->_data[$k] = $a[$k];
				}
			}
			foreach(array('jours_avant','jour_mois_debut','jour_mois_fin') as $k) {
				if (isset($a[$k])) {
					$this->setData($k, $a[$k]);
				}
			}
			// on ajoute les champs de l'�dito (si valeurs pr�sentes dans le post, elles ont la priorit�s)
			foreach(PromotionEdito::getEdito($this->getData()) as $k => $v) {
				$this->_data[$k] = ($a[$k] ? $a[$k] : $v);
			}
			$this->setData('modification', date('Y-m-d H:i:s'));
			// save
			$this->save('actif,depuis,jusqua,debut,fin,jours_avant,jour_mois_debut,jour_mois_fin,nom,slogan,slogan_iphone,description,mentions,modification');
			return true;
		}
		return false;
	}
	/**
	* D�publie une promotion
	* 
	*/
	public function unpublish() {
		$this->setData('actif', 0);
		$this->setData('modification', date('Y-m-d H:i:s'));
		$this->save('actif,modification');
		return $this;
	}
	
	/**
	* Renvoie true si un forfait est valide pour cette promotion
	* 
	* @param string $forfait
	* @returns bool
	*/
	public function isValidForfait($forfait)
	{
		return (!$this->_data['forfait'] || ($this->_data['forfait'] == substr($forfait, 0, strlen($this->_data['forfait']))));
	}
	/**
	* Renvoie true si une cat�gorie est valide pour cette promotion
	* 
	* @param string $mnem
	* @returns bool
	*/
	public function isValidMnem($mnem)
	{
		// si aucune cat�gorie n'est pr�cis�e elles sont toutes valides
		if (!$this->_data['categories'])
			return true;
		// sinon on recherche dans la liste des cat�gories
		if (!$this->_categories) {
			$this->_categories = explode(',', $this->_data['categories']);
		}
		return in_array($mnem, $this->_categories);
	}
	/**
	* Renvoie le prix avec la promotion
	* 
	* @param float $ttc
	* @returns float
	*/
	public function getPrixPromo($ttc) {
		// on ajoute la variation car montant comporte le sens de la variation
		$ttc += ($this->_data['montant']) * ($this->_data['mode'] == 'P' ? ($ttc/100) : 1);
		if ($ttc < 0) $ttc = 0;
		return round($ttc, 2);
	}

}
?>