<?

class Partenaire extends Ada_Object
{
	/** @var PartenaireConducteur_Collection */
	protected $_conducteurs;
	
	// tranche des salari�s
	static public $SALARIES = array
	(
		 0  => 'ne sait pas'
		,1  => '1 � 10'
		,10 => '10 � 50'
		,50 => '50 � 100'
		,100 => '100 � 500'
		,500 => '500 � 1000'
		,1000 => 'plus de � 1000'
	);
	static public $ORIGINES = array(
		 'ce'		=> 'CE'
		,'locapass'	=> 'LocaPass'
		,'webpro'	=> 'WebPro'
		,'webce'	=> 'WebCe'
		,'coupon'	=> 'R�duction'
	);
	
	/**
	* Cr�e un objet � partir d'un identifiant
	* 
	* @param string $id
	* @return Partenaire
	*/
	static public function factory($id)
	{
		$x = new Partenaire();
		if ($x->load($id)->isEmpty())
			return null;
		return $x;
	}
	/**
	* Cr�e un partenaire
	* 
	* @param Client $client
	* @param string $fonction	fonction du client dans le partenaire
	* @param string $origine	origine au sens de self::$ORIGINES
	* @param array $data
	* @param array $errors
	* @return Partenaire
	*/
	static public function create(Client $client, $fonction, $origine, &$data, &$errors)
	{
		// v�rification de l'origine
		if (!$client)
			$errors[] = "Une erreur interne s'estproduite";
		if (!self::$ORIGINES[$origine])
			$errors[] = sprintf('le type de partenaire "%s" est inconnu', filter_var($origine, FILTER_SANITIZE_STRING));
		$fonction = filter_var(trim($fonction), FILTER_SANITIZE_STRING);
		if (!$fonction)
			$errors[] = "Vous devez pr�ciser une fonction !";
		
		// on compl�te les donn�es de $data � partir des donn�es du client
		$data['origine'] = $origine;
		if (!$data['email'] && !$data['interlocuteur'])
		{
			$data['email'] = $client['email'];
			$data['interlocuteur'] = sprintf("%s %s", $client['prenom'], $client['nom']);
		}
		
		// on cr�e le partenaire
		$partenaire = new Partenaire();
		if ($partenaire->validateData($data, $errors))
		{
			// s'il n'y a pas d'erreurs � ce stade on :
			// - cr�e le partenaire
			// - cr�e la relation avec le client
			// - on cr�e le coupon
			// - on envoie un mail au partenaire
			// - il faudra rediriger l'utilisateur vers la page de confirmation
			if (!count($errors))
			{
				$data['id'] = '';
				$partenaire->setData($data);
				$tarif = PartenaireTarif::factory($partenaire);
				if (!$tarif)
					$errors[] = "Aucun tarif ne correspond � votre demande !";
				else
				{
					// c'est bon on a tous les �l�ments !
					$partenaire->save();
					$partenaire->setId(mysql_insert_id());
					
					// maintenant le lien entre partenaire et client
					$partenaire_client = array('id'=>'', 'partenaire'=>$partenaire['id'], 'client'=>$client['id'],'fonction'=>$fonction);
					save_record('partenaire_client', $partenaire_client);
					
					// cr�er le coupon puis le r�glement
					$coupon = PartenaireCoupon::create($partenaire, $client, $tarif);
					$reglement = PartenaireReglement::create($coupon, $tarif);
					
					// pr�parer les donn�es pour l'envoi du mail
					$a = array();
					foreach(array('partenaire'=>$partenaire,'coupon'=>$coupon,'tarif'=>$tarif) as $prefix => $o)
					{
						foreach($o->getData() as $k => $v)
							$a[$prefix.'_'.$k] = $v;
					}
					$a['url_confirmation'] = Page::getURL('pro/validation.html?key='.self::_encryptData(array($coupon['id'],$partenaire['id'])));
					// r�sum� de toutes les donn�es
					foreach ($a as $k => $v)
						$str .= "#$k#\t$v\n";
					$a['data'] = $str;
					
					// envoyer les mails
					$html = load_and_parse(BP.'/emails/em-'.$partenaire['origine'].'-confirmation.html', $a);
					send_mail($html, $client['email'], SUBJECT_PRO_CONFIRMATION, EMAIL_FROM, EMAIL_FROM_ADDR, null, EMAIL_PRO);
					
					$html = load_and_parse(BP.'/emails/em-webpro-commercial.html', $a);
					send_mail($html, EMAIL_PRO, SUBJECT_PRO_COMMERCIAL, EMAIL_FROM, EMAIL_FROM_ADDR, null, '');
				
					// renvoyer le partenaire
					return $partenaire;
				}
			}
		}
		return null;
	}
	
	/**
	* Renvoie une liste de conducteurs associ�es au partenaire
	* @returns PartenaireConducteur_Collection
	*/
	public function getConducteurs() {
		if (!$this->_conducteurs) {
			$this->_conducteurs = PartenaireConducteur_Collection::factory($this);
		}
		return $this->_conducteurs;
	}
	
	/**
	* Renvoie le nombre de r�servations entre deux date pour ce formulaire
	* 
	* @param string $date_from date de d�part
	* @param string $date_to  date de fin
	* @returns int nombre de r�servations effectu�es
	*/
	public function getReservationsBetween($date_from, $date_to)
	{
		if ($this->getId())
		{
			$sql = "select count(r.id)\n";
			$sql.=" from reservation r\n";
			$sql.=" join partenaire_coupon pc on r.coupon=pc.id\n";
			$sql.=" where pc.partenaire=".$this->getId()."\n";
			$sql.=" and r.paiement between  '$date_from' and '$date_to' and r.statut IN ('A','P') and r.forfait_type='ADAFR'";
			return getsql($sql);
		}
		return 0;
	}
	/**
	* Renvoie la liste des coupons pour un partenaire
	* @returns PartenaireCoupon_Collection
	*/
	public function getCoupons()
	{
		return PartenaireCoupon_Collection::factory($this);
	}
		/**
	* Renvoie une adresse en HTML
	* 
	* @param string $sep s�parateur entre les �l�ments de l'adresse
	* @param mixed $with champs � mettre dans l'adresse, sinon valeur par d�faut
	* @param miexed $except champs � ne pas mettre (adresse1, adresse2, cp_ville, tel, fax)
	* @returns string
	*/
	public function getHtmlAddress($sep = '<br/>', $with = array(), $except = array())
	{
		// champs � prendre en compte
		if ($with && !is_array($with))
			$with = explode(',', $with);
		if ($except && !is_array($except))
			$except = explode(',', $except);
		if (!$with || !is_array($with))
			$with = array('nom', 'adresse1','adresse2','cp_ville', 'tel', 'siret_format');
		
		// cr�er cp_ville si n'existe pas
		if (!$this->hasData('cp_ville'))
			$this->setData('cp_ville', $this->getData('cp').' '.$this->getData('ville'));
		if (!$this->hasData('siret_format'))
			$this->setData('siret_format', self::formatSIRET($this->getData('siret')));
		
		$prefix = array('siret_format'=>'<br/><strong>SIRET</strong><br/>', 'tel' => '<br/><strong>T�l�phone</strong><br/> ');
		foreach ($with as $k)
		{
			if (in_array($k, $except)) continue;
			if ($v = $this->getData($k))
				$html[] = $prefix[$k].$v;
		}
		return is_array($html) ? join($sep, $html) : '';
	}
	
	/**
	* Renvoie le tableau des r�gles de validation
	*/
	protected function _getValidatingRules()
	{
		return array (
			'id'		=> $this->_addRuleInt(),
			'origine'	=> $this->_addRuleRegexp('/^webpro|webce$/', null, null),
			'actif'		=> $this->_addRuleBoolean(null),
			'nom'		=> $this->_addRuleString('Vous devez pr�ciser le nom de la soci�t�'),
			'siret'		=> $this->_addRuleRegExp('/^\d{14}$/', 'Vous devez pr�ciser le num�ro SIRET', "Le num�ro SIRET %s n'est pas valide"),
			'naf'		=> $this->_addRuleRegExp('/^\d{3,4}[A-Z]$/i', "Vous devez pr�ciser le code NAF", "Le code NAF n'est pas valide"),
			'gerant'	=> $this->_addRuleString("Vous devez pr�ciser le g�rant"),
			'salaries'	=> $this->_addRuleInt("Vous devez pr�ciser le nombre de salari�s"),
			'annee'		=> $this->_addRuleInt("Vous devez pr�ciser l'ann�e de cr�ation", "L'ann�e de cr�ation n'est pas valide", 1800, date('Y')),
			'interlocuteur' => $this->_addRuleString(),
			'email'		=> $this->_addRuleEmail(null, "L'e-mail %s n'est pas valide"),
			'adresse1'	=> $this->_addRuleString("Vous devez pr�ciser l'adresse"),
			'adresse2'	=> $this->_addRuleString(),
			'cp'		=> $this->_addRuleRegExp('/^\d{5}$/', "Vous devez pr�ciser le code postal", "Le code postal n'est pas valide"),
			'ville'		=> $this->_addRuleString("Vous devez pr�ciser la ville"),
			'pays'		=> $this->_addRuleRegExp('/^fr$/', "Vous devez pr�ciser le pays", "L'offre est r�serv�e � la France m�tropolitaine"),
			'tel'		=> $this->_addRulePhone("Vous devez pr�ciser le t�l�phone", "Le num�ro de t�l�phone n'est pas valide"),
			'fax'		=> $this->_addRulePhone(null, "Le num�ro de fax n'est pas valide"),
		);
	}
	
	/**
	* Valider les donn�es puis v�rifications compl�mentaire
	* 
	* @param array $data
	* @param array $errors
	* @param mixed $fields
	* @returns boolean
	*/
	public function validateData(&$data, &$errors, $fields = null)
	{
		if (parent::validateData($data, $errors, $fields))
		{
			if ($data['siret'] && !self::validateSIRET($data['siret']))
			{
				$errors[] = "Le num�ro SIRET ".$data['siret']." n'est pas valide.";
				$data['siret'] = null;
				return false;
			}
			return true;
		}
		return false;
	}
	/**
	* Valider le SIRET : formule de LUHN
	* 
	* @param string $siret
	* @returns bool
	*/
	static public function validateSIRET($str)
	{
		$sum = 0;
		for($len=strlen($str),$i=$len; $i > 0; $i--)
		{
			$v = (2-($i%2)) * (int) substr($str, $len - $i, 1);
			if ($v >= 10) $v -= 9; // pour passer de 14 � 1 + 4 = 5
			$sum += $v;
		}
		return ($sum %10 == 0);
	}
	/**
	* Format un SIRET (14):
	* 	SIREN (9), NIC (4) CLE (1)
	* 
	* @param string $siret
	* @return string
	*/
	static public function formatSIRET($siret)
	{
		if (!preg_match('/^\d{14}$/', $siret))
			return $siret;
		$a = str_split(substr($siret, 0, 9), 3);
		$a[] = substr($siret, 9, 4);
		$a[] = substr($siret, -1, 1);
		return join(' ', $a);
	}
}
?>