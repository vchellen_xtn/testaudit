<?

class Categorie_Collection extends Ada_Collection
{
	static protected $_CACHE_STORE = array();
	
	protected $_zone; // zone des cat�gories
	protected $_type; // type de v�hicule
	protected $_mnemIndex = array();

	/**
	* Cr�� une collection de cat�gorie depuis une zone et un type
	*
	* @param string $zone - zone
	* @param string $type - type de v�hicule
	* @param string $variante - compl�ment d'information sur la cat�gorie
	* @return Categorie_Collection
	*/
	static public function factory($zone, $type, $jeunes = false)
	{
		// on cache la liste des cat�gories
		$cacheKey = strtolower($zone.'-'.$type);
		if ($jeunes)
			$cacheKey .= '-jeunes';
		if (isset(self::$_CACHE_STORE[$cacheKey])) {
			return self::$_CACHE_STORE[$cacheKey];
		}
		// on cr�e la collection qui sera retourn�e
		$x = new Categorie_Collection();
		// SQL pour les applications
		$sql = "SELECT c.*, concat(ifnull(concat(nullif(f.nom,c.nom),' '),''), c.nom) nom, f.nom famille_nom, c.nom categorie_nom, f.categorie_defaut";
		if ($jeunes)
			$sql.=", j.nom jeune_nom, j.description jeune_description";
		$sql.=" FROM categorie c";
		$sql.=" LEFT JOIN vehicule_famille f ON f.id=c.famille";
		if ($jeunes)
			$sql.=" LEFT JOIN categorie_jeune j ON j.categorie=c.id";
		$sql.=" WHERE c.zone='".addslashes($zone)."' AND c.type='".addslashes($type)."' AND publie=1";
		$sql.=" ORDER BY zone, type, position, mnem";
		$x->_loadFromSQL($sql);
		if (!$x->isEmpty())
		{
			$x->_zone = $zone;
			$x->_type = $type;
		}
		// on cache le r�sultat
		self::$_CACHE_STORE[$cacheKey] = $x;
		return $x;
	}
	
	public function add(Ada_Object $item)
	{
		// pr�ciser les noms ADA Jeunes
		if ($item['jeune_nom'])
			$item['nom'] = $item['jeune_nom'];
		if ($item['jeune_description'])
			$item['description'] = $item['jeune_description'];
		// l'index par mnem
		$this->_mnemIndex['cat'.$item['mnem']] = $item['id'];
		// laisser la cat�gorie s'ajouter � la collection
		return parent::add($item);
	}
	/**
	* Acc�s par mn�monique et par id
	* @param string $index
	* @return Categorie
	*/
	public function getItem($index)
	{
		if (isset($this->_mnemIndex['cat'.$index]))
			$index = $this->_mnemIndex['cat'.$index];
		return $this->_items[$index];
	}
}
?>