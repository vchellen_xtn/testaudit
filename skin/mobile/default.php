<? 
	// inclure un menu s'il existe
	$d = dirname($this->getId());
	foreach(array('mobile', SKIN_DEFAULT) as $str) {
		$f = BP.sprintf('/skin/%s/%s/menu.php', $str, $d);
		if (is_file($f)) {
			include ($f);
		}
	}
	// inclure le fichier principal
	include($this->getPagePath());

