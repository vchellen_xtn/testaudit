<?php
/**
* AB Tasty
* 	https://support.abtasty.com/hc/fr/articles/200308297-Tag-de-transaction-pour-remonter-les-donn%C3%A9es-ecommerce-dans-le-reporting
*/
class Tracking_ABTasty
{
	const TRACKING_TOTAL = 1;
	const TRACKING_COURTAGE = 2;
	
	static protected $_mode = self::TRACKING_TOTAL;

	static public function doTracking(Page $page)
	{
		//ABTasty
		if ($page->getABTracking()) :
?>
		<script type="text/javascript">
		// <![CDATA[
			if (typeof(_abtasty) != "undefined") {
				<? foreach($page->getABTracking() as $variables) : ?>
				_abtasty.push(<?=json_encode($variables);?>);
				<? endforeach; ?>
			}
		// ]]>
		</script>
<?		endif;
		//!ABTasty
	}
	
	/** 
	* Renoive le tracking pour Google Analytics
	* 
	* @param Page $page
	* @param Reservation $reservation
	*/
	static public function reservationValidation(Page $page, Reservation $reservation)
	{
		// si le mode est "COURTAGE" on r�cup�re les options concern�es
		$options = $reservation->getResOptions();
		$total = 0;
		if (self::$_mode == self::TRACKING_COURTAGE) {
			// on prend le total des options "franchises"
			$type = strtolower($reservation['type']);
			$a = array();
			foreach(array('nf'.$type, 'rf'.$type, 'af'.$type) as $k) {
				if ($o = $options[$k]) {
					$a[$k] = $o;
					$total += $o['prix'];
				}
			}
			// s'il n'y a pas de "franchise" on ne transforme pas
			$options = $a;
		} else {
			$total = $reservation->getPrixTotal();
		}
		
		// r�cup�rer les varaibles principales
		$agence = $reservation->getAgence();
		$categorie = $reservation->getCategorie();
		
		// cr�er la transaction
		$page->addABTracking('transaction',
			'Achat',						// NOM DE VOTRE OBJECTIF
			$reservation['id'],				// ORDER ID
			$total 	// TRANSACTION VALUE
		);
		// ...puis on pr�cise le forfait...
		$type = strtoupper($categorie['type']);
		$mnem = $categorie['mnem'];
		if ($mnem == 'D+') $mnem = "D'";
		$sku = sprintf('%s/%s', $type, $mnem);
		
		$page->addABTracking('eco', 'Nom du produit', $categorie['canon']);
		$page->addABTracking('eco', 'ID du produit', $sku );
		$page->addABTracking('eco', 'Categorie de produit', 'forfait');
		
		// ...puis les diff�rentes options...
		foreach($options as $optionID => $option) {
			$page->addABTracking('eco', 'Nom du produit', filter_chars($option['nom']));
			$page->addABTracking('eco', 'ID du produit', $optionID );
			$page->addABTracking('eco', 'Categorie de produit', filter_chars($option['theme']));
		}
		
		// ... on finit par quelques variables personnalis�es...
		$page->addABTracking('eco', 'TypeVehicule', $type);
		$page->addABTracking('eco', 'Duree', $reservation['duree']);
		$page->addABTracking('eco', 'JoursAvant', ceil($reservation->getHoursBeforeStart() / 24));
		$forfait = mysql_fetch_assoc(sqlexec(sprintf("select * from forfait2 where id='%s'", $reservation['forfait'])));
		if ($forfait && isset($forfait['prefix'])) {
			$page->addABTracking('eco', 'Forfait', $forfait['prefix']);
		}
	}
}
?>
