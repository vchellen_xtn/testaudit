<?
abstract class Reservation_ConditionsAnnulation
{
	/** @var Reservation $reservation */
	protected $_reservation;
	
	/**
	* Renvoie une condition d'annulation en fonction de la r�servation
	* 
	* @param Reservation $reservation
	* @return Reservation_ConditionsAnnulation
	*/
	static public function factory(Reservation $reservation)
	{
		if ($reservation->isLocapass()) {
			return new Reservation_ConditionsAnnulationLocapass($reservation);
		}
		if ($reservation['type']=='ml') {
			return new Reservation_ConditionsAnnulationMalin($reservation);
		}
		// pour les WebPro, m�me condition que Locapass
		$coupon = $reservation->getPartenaireCoupon();
		if ($coupon && $coupon['origine']=='webpro') {
			return new Reservation_ConditionsAnnulationLocapass($reservation);
		}
		// les conditions d'annulation peuvent varier dans le temps
		/*
		$t = $reservation->hasData('paiement') ? $reservation->getData('paiement', 'U') : null;
		if ($t) {
			if ($t < strtotime('2011-09-07'))
				return new Reservation_ConditionsAnnulation01($reservation);
			else if ($t < strtotime('2013-03-04'))
				return new Reservation_ConditionsAnnulation02($reservation);
			else if ($t < strtotime('2015-10-22'))
				return new Reservation_ConditionsAnnulation03($reservation);
		}
		*/
		return new Reservation_ConditionsAnnulation04($reservation);
	}

	public function __construct(Reservation $reservation = null) {
		$this->_reservation = $reservation;
	}
	
	/** fonctons pour contr�ler les conditions d'annulation */
	abstract public function isCancellable($preavis, &$msg);
	abstract public function getCancellationFees($preavis);
	public function getCancellationFeesSiege($preavis)
	{
		// la part agence des frais de dossier
		return $this->getCancellationFees($preavis);
	}
	/**
	* Renvoie si une r�servation est remboursable
	* 
	* @param int $preavis le nombre d'heures avant le d�part
	* @returns bool
	*/
	public function isRefundable($preavis) {
		return $this->isCancellable($preavis, $msg);
	}
	abstract public function getConditions();
	abstract public function getInfos();
}
?>
