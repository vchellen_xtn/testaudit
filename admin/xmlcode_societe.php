<?
	require_once('secure.php');
	include "../xml.php";

	$element = "option";
	$sql = "select distinct a.code_societe val, concat(a.code_societe, ' ', a.societe, ' (VP: ', COALESCE(c.tarifs_vp,'ADAFR'),', VU: ',COALESCE(c.tarifs_vu,'ADAFR'),')') title";
	$sql.=" from agence a";
	$sql.=" left join agence_config c on c.agence=a.code_societe";
	$sql.=" where a.zone='".(isset($_GET['zone']) && preg_match('/^[a-z]{2}$/', $_GET['zone']) ? $_GET['zone'] : $_SESSION["ADA001_ADMIN_ZONE"])."' and (a.statut&1)=1 and nullif(a.code_societe,'') is not null";
	if ($_GET['reseau']) $sql.=" and a.reseau='".addslashes($_GET['reseau'])."'";
	if ($_GET['groupe']) 
	{
		$groupe = ZoneGroupe::factory((int)$_GET['groupe']);
		if ($groupe['nom']=='JEUNES')
			$sql.=" and (a.statut & ".Agence::STATUT_JEUNES.')='.Agence::STATUT_JEUNES;
		else
			$sql.=" and a.groupe=".(int)$_GET['groupe'];
	}
	$sql.=" order by a.code_societe";
	xml_from_sql($element, $sql);
?>