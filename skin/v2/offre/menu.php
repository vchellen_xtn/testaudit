<?
	$menu = array(
		'forfaits'		=> 'Nos forfaits',
		'../jeunes/index'	=> 'Jeune conducteur',
		'service_livraison'	=> 'Ada service livraison',
		'../agences/livraison'	=> 'Ada service voiturier', // service_voiturier pour le remettre dans le répertoire /offre/
	);
	$action = basename($page);
?>
<ul class="menu">
	<? foreach($menu as $k => $lbl) : ?>
		<li>
			<a href="<?=$k?>.html"<?if ($action==$k) echo ' class="current"'; ?>><?=$lbl;?></a>
		</li>
	<? endforeach; ?>
</ul>