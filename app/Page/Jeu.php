<?php

class Page_Jeu extends Page
{
	const JEU_TITRE = "Hunger Games";
	const JEU_CANON = 'hungergames';
	const DATE_START = '2015-10-21'; // format yyyy-mm-dd
	const DATE_END = '2015-11-18'; // format yyyy-mm-dd
	
	protected function prepareHead()
	{
		parent::prepareHead();
		$this->addJS('scripts/jquery.validate.min.js');
	}
	public function getUniversTitre() {
		return 'Grand jeu concours';
	}
	/**
	* Revnoie le titre du film
	* @return string
	*/
	public function getFilmTitre() {
		return self::JEU_TITRE;
	}
	/**
	* Renvoie un extrait du r�gl�ment
	* @return string
	*/
	public function getFilmReglement() {
		return "Jeu-concours gratuit et sans obligation d'achat du 21 octobre au 18 novembre 2015 inclus,"
		." dont le r�glement est disponible gratuitement sur demande �crite � MERCREDI : 44, rue La Fayette - 75009 Paris."
		." Dotations :"
		." 5x1 an de cin�ma Metropolitan Films d�une valeur unitaire de 250� TTC,"
		." 100 pin�s Geai Moqueur d�une valeur unitaire de 10� TTC,"
		." 150 affiches du film HUNGER GAMES LA R�VOLTE - PARTIE 2 d�une valeur unitaire de 5� TTC."
		."<br/><br/>"
		."Conform�ment � la loi n�78-17 du 06 janvier 1978, les participants disposent d'un droit d'acc�s, de rectification et de radiation aux informations les concernant"
		." en adressant un courrier simple � l'adresse ci-dessus."
		." Cr�dits non contractuels. Timbre de demande de r�glement rembours� au tarif lent en vigueur sur demande conjointe � l'envoi initial � l'adresse ci-dessus.";
	}
	
	/**
	* Renvoie les questions poru le film
	* @returns array
	*/
	public function getFilmQuestions()
	{
		return array
		(
			"Quelle actrice tient le r�le-titre dans la saga HUNGER GAMES ?" => array(
				'Jennifer Lawrence',
				'Emma Watson',
				'Emma Stone',
			),
			"Quel est le District leader de la r�volte ?" => array(
				'District 1',
				'District 13',
				'District 7',
			),
			"Quelle est l'arme attitr�e de Katniss ?" => array(
				'un poignard',
				'une arme � feu',
				'un arc',
			),
		);
	}
	
	
	
	public function le_filmAction()
	{
		// si le jeu est fini ou pas commenc�, on redirige vers la home
		if ((time() < strtotime(Page_Jeu::DATE_START) || time() > strtotime(Page_Jeu::DATE_END)) && SITE_MODE == 'PROD') {
			self::redirect(self::getURL());
			exit();
		}
		$this->hideLinkTop();
		if (!$this->hasData('titre'))
			$this->setData('titre', "Jeu - " . Page_Jeu::JEU_TITRE . " - ADA");
		if (!count($_POST)) return;
		
		// On cr�e le prospect jeu
		$errors = array();
		// on cr�e le prospect
		$_POST['origine'] = 'JEU';
		if (!$_POST['naissance'])
			$_POST['naissance'] = '0000-00-00';
		if ($x = Prospect::create($_POST, $errors))
		{
			$this->setData('is_form_valid', true);
			$this->setTrackPage('confirmation', true);
			// enregisterr les r�ponses et envoyer l'e-mail de participation au jeu
			$data = $x->getData();
			foreach($_POST['questions'] as $q => $reponse)
			{
				$a = array
				(
					'id'		=> '',
					'email' 	=> $data['email'],
					'jeu'		=> Page_Jeu::JEU_CANON,
					'question'	=> (int) $q,
					'reponse'	=> filter_var($reponse, FILTER_SANITIZE_STRING)
				);
				$data['question'.$a['question']] = $a['reponse']; // stockage des r�ponses pour l'envoi de mail
				save_record('prospect_jeu', $a);
			}
			$html = load_and_parse(BP.'/emails/em-jeu.html', $data);
			send_mail($html, EMAIL_JEU_CONCOURS, 'Participation au jeu concours', EMAIL_FROM, EMAIL_FROM_ADDR, null, '');
		}
		else if (count($errors))
			$this->_addMessage($errors);
	}
}