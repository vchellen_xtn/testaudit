<?php
class AgenceBonPlan_Collection extends Ada_Collection {
	/**
	* Renvoie une lsite de bons plans pour une agence
	* 
	* @param Agence $agence
	* @param string $statut
	* @return AgenceBonPlan_Collection
	*/
	static public function factory(Agence $agence, $statut = AgenceBonPlan::STATUT_PUBLIC) {
		$x = new AgenceBonPlan_Collection();
		// pr�parer le SQL
		$sql = sprintf("select * from agence_bon_plan where agence='%s' and statut='%s'", $agence->getId(), $statut);
		if ($statut == AgenceBonPlan::STATUT_PUBLIC) {
			$sql.=" and current_date() <= coalesce(fin,current_date())";
		}
		$sql.=" order by debut, fin, titre";
		// charger les bons plans
		$x->loadFromSQL($sql);
		return $x;
	}
}
?>
