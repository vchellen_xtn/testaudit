<?
class ResOption_Collection extends Ada_Collection
{
	static public $OPTIONS_EMAIL_DESC = array
	(
		'L' => 'pay�es en ligne',
		'A' => '� payer en agence sous r�serve de stock disponible'
	);
	static public $OPTIONS_HTML_DESC = array(
		'L' => 'r�glement en ligne',
		'A' => 'r�glement en agence sous r�serve de stock disponible'
	);
	/** @var Ada_Object (soit Reservation, soit AgenceResa) */
	protected $_reservation = null;

	/**
	*
	*
	* @param Reservation $reservation
	* @return ResOption_Collection
	*/
	static public function factory(Ada_Object $reservation)
	{
		$className = get_class($reservation);
		if ($className=='Reservation') {
			$fieldName = 'reservation';
			$tableName = 'res_option';
		} else if ($className == 'AgenceResa') {
			$fieldName = 'resa';
			$tableName = 'agence_resa_option';
		} else {
			throw new Exception($className.' is not allowed here');
		}
		// on cr�e la collection qui sera retourn�e
		$x = new ResOption_Collection();
		// la requ�te
		$sql = "SELECT r.".$fieldName.", r.option, r.prix, r.paiement, r.position, r.facturation, r.quantite, r.retrait, r.nom";
		$sql.=", o.theme, o.groupe, o.commentaire, t.nom theme_nom";
		$sql.=" FROM ".$tableName." r";
		$sql.=" LEFT JOIN `option` o ON o.id=r.`option`";
		$sql.=" LEFT JOIN option_theme t ON t.id=o.theme";
		$sql.=" WHERE ".$fieldName." = ".$reservation->getId();
		$sql.=" ORDER BY r.paiement DESC, COALESCE(r.position,9999) ASC, r.option ASC";

		// on charge la collection
		$x->_loadFromSQL($sql);
		$x->_reservation = $reservation;
		return $x;
	}

	/**
	* Renvoie une ResOption_Collection tri�e
	* @returns ResOption_Collection
	*/
	public function sortByPosition()
	{
		uasort($this->_items, 'ResOption_Collection::compareOptionsByPosition');
		return $this;
	}
	/** Commparaison des optiosn */
	static public function compareOptionsByPosition($a, $b)
	{
		if ($a['paiement'] == $b['paiement'])
		{
			if ($a['position'] == $b['position'])
				return 0;
			return ($a['position'] < $b['position']) ? -1 : 1;
		}
		return ($a['paiement'] < $b['paiement']) ? 1 : -1;
	}

	/**
	* Enregistre les options dans la base de donn�es si elles n'en provienent pas
	*
	* @param Ada_Object $reservation
	* @resturns ResOption_Collection
	*/
	public function setReservation(Ada_Object $reservation)
	{
		if (is_object($reservation))
			$className = get_class($reservation);
		$fieldName = ''; $tableName = '';
		if ($className == 'Reservation') {
			$fieldName = 'reservation';
			$tableName = 'res_option';
		} else if ($className == 'AgenceResa') {
			$fieldName = 'resa';
			$tableName = 'agence_resa_option';
		} else {
			throw new Exception($className." is not allowed here");
		}

		if ($this->isLoaded() || !$reservation->getId()) return $this;
		$this->_reservation = $reservation;
		$fields = array($fieldName, 'option', 'prix', 'quantite', 'retrait', 'paiement', 'facturation', 'position', 'nom');
		$values = array();
		foreach($this->_items as $option)
		{
			$option->setData($fieldName, $reservation->getId());
			$o = array();
			foreach($fields as $k)
			{
				$v = $option[$k];
				if (is_numeric($v))
					$o[] = $v;
				else if ($v)
					$o[] = "'".addslashes($v)."'";
				else
					$o[] = 'NULL';
			}
			$values[] = '('.join(',', $o).')';
		}
		if (count($values))
		{
			$sql = 'INSERT INTO '.$tableName.'(`'.join('`,`', $fields).'`) VALUES '."\n";
			$sql.= implode(",\n", $values);
			sqlexec($sql);
		}
		return $this;
	}
	/**
	* Renvoie une collection avec uniquement les options associ�es � la facturaiton
	*
	* @param string $facturation ada | courtage
	* @param string paiement L | A | null
	* @return ResOption_Collection
	*/
	public function filter($facturation, $paiement = null, $theme = null)
	{
		$facturation = strtoupper($facturation);
		$x = new ResOption_Collection();
		foreach($this->_items as $o)
		{
			if ($o['facturation']==$facturation && (!$paiement || $paiement==$o['paiement']) && (!$theme || $theme==$o['theme']))
				$x->add($o);
		}
		return $x;
	}
	/**
	* Facturation ADA / COURTAGE / MATERIEL
	*
	* @param string $facturation	ADA | COURTAGE
	* @returns bool
	*/
	public function hasFacturation($facturation)
	{
		$facturation = strtoupper($facturation);
		foreach($this->_items as $o)
		{
			if ($o['facturation']==$facturation)
				return true;
		}
		return false;
	}
	/**
	* Renvoie le total du prix des options en filtrant avec les param�tres pass�s
	*
	* @param string $facturation ADA | COURTAGE
	* @param string	 $paiement A(gence) | L(igne)
	*/
	public function getTotal($facturation = null, $paiement='L', $theme = null)
	{
		$total = 0;
		foreach ($this->_items as $item)
		{
			if ($facturation && $facturation != $item['facturation']) continue;
			if ($paiement && $paiement != $item['paiement']) continue;
			if ($theme && $theme != $item['theme']) continue;
			$total += $item['prix'];
		}
		return $total;
	}
	/**
	* Une option est-elle pr�sente
	*
	* @param string $id
	* @returns bool
	*/
	public function hasOption($id)
	{
		return isset($this->_items[strtolower($id)]);
	}
	public function hasOptions($ids)
	{
		// les identifiants sont en miniscules
		if (!is_array($ids))
			$ids = explode(',',strtolower($ids));
		foreach($ids as $k)
		{
			if (isset($this->_items[$k]))
				return true;
		}
		return false;
	}

	public function getDateRetrait()
	{
		foreach($this->_items as /** @var ResOption */ $option)
		{
			if ($option['retrait'])
				return $option['retrait'];
		}
		return;
	}

	/**
	* Renvoie le HTML correspondant aux options d'une r�servation
	*
	* @param bool $email_txt
	* @param bool $show_prix
	*/
	public function toHtml($email_txt=false, $show_prix=true)
	{
		// intitul� des paiements
		$html = $paiement = null;

		// parcourir les options
		foreach($this->_items as /** @var ResOption */ $option)
		{
			// pour l'e-mail on n'affiche pas les options gratuites
			if ($email_txt && $option['paiement']=='L' && !$option['prix'])
				continue;
			// le titre des options
			if ($option['paiement'] != $paiement)
			{
				$paiement = $option['paiement'];
				if($email_txt)
					$html.='<tr><td colspan="3" style="font: normal 11px arial, sans-serif;" height="35">'.self::$OPTIONS_HTML_DESC[$paiement].'</td></tr>';
				else
					$html.='<dd class="twosup">'.self::$OPTIONS_HTML_DESC[$paiement].'</dd>';
			}
			if($email_txt)
			{
				$html.='<tr>';
				$html.='<td width="20"><img src="img/a14.gif" alt=""></td>'."\n";
				$html.='<td style="font: normal 11px arial, sans-serif;">'.$option['nom'].'</td>';
				$html.='<td style="font: normal 11px arial, sans-serif;"  width="50">&nbsp;'.$option->getPrix($show_prix).'</td>';
				$html.='</tr>'."\n";
			}
			else
			{
				if ($show_prix && $option['prix'])
				{
					$html.= '<div class="droite rouge imprprix" style="width: 50px;">';
					$html.= '<strong>'.$option->getPrix().'</strong>';
					$html.= '<p class="clear"></p></div>'."\n";
				}
				$html.='<dd class="case" style="width:400px;">'.$option['nom'].'</dd>'."\n";
			}
		}
		if (!strlen($html))
			$html = '<span class="nongras">Aucune option s�lectionn�e</span>';
		return $html;
	}
	/**
	* Renvoie le HTML correspondant aux options d'une r�servation
	*
	* @param bool $show_prix
	*/
	public function toHtml2($show_prix=true)
	{
		// intitul� des paiements
		$html = $paiement = null;

		// parcourir les options
		$theme = null;
		foreach($this->_items as /** @var ResOption */ $option)
		{
			if (preg_match('/^nfv[pu]$/', $option['option'])) continue;
			if ($option['paiement'] != $paiement)
			{
				if ($paiement)
					$html.=  "\n</ul>";
				$paiement = $option['paiement'];
				$html.= '<br/><strong>Vos options</strong> | <span class="infos_agence">'.self::$OPTIONS_HTML_DESC[$paiement].'</span><br />'."\n";
				$html.= '<ul>'."\n";
			}
			if ($theme != $option['theme'])
			{
				$html.='<li>'.$option['theme_nom'].'</li>';
				$theme = $option['theme'];
			}
			$html.='<li class="res_opt_detail">';
			$html.= '<div class="res_opt_nom res_opt_'.$option['option'].'"><span class="rouge">&#10003;</span> '.$option;
			if ($show_prix && $option['prix'])
				$html.= '<span class="res_opt_prix"> : <strong>'.$option->getPrix().'</strong></span>';
			$html.='</div>';
			$html.='</li>'."\n";
		}
		if (!strlen($html))
			$html = '<span class="res_opt_none">Aucune option s�lectionn�e</span>';
		else
			$html .= "\n</ul>\n";
		return $html;
	}

	public function toHtmlEmail()
	{
		// intitul� des paiements
		$type = strtolower($this->_reservation['type']);
		$html = $paiement = $theme = $groupe = $info = null;

		// parcourir les options
		foreach($this->_items as /** @var ResOption */ $option)
		{
			$optionID = $option['option'];
			if ($option['paiement'] != $paiement)
			{
				if ($paiement)
					$html.=  $info."\n</table><br/>";
				$paiement = $option['paiement'];
				$theme = $groupe = $info = null;
				$html.='<img src="img/puce4.gif" alt=""><b>RECAPITULATIF DES OPTIONS '.self::$OPTIONS_EMAIL_DESC[$paiement].'</b><br>'."\n";
				$html.= '<table cellpadding="0" cellspacing="0">'."\n";
			}
			if ($theme != $option['theme'])
			{
				$html.= $info;
				$html.= '<tr>'."\n";
					$html.='<td colspan="3" style="font:normal 12px arial;">'."\n";
						$html.='<br><font color="#cc0000"><b>'.$option['theme_nom'].'</b></font>';
					$html.='</td>';
				$html.='</tr>'."\n";

				$theme = $option['theme'];
				$info = null;
			}
			$groupe = $option['groupe'];
			if ($optionID != 'nf'.$type)
			{
				$html.='<tr>';
					$html.='<td valign="top" width="20" style="font:normal 12px arial;"><font color="#cc0000">&#10004;</font></td>';
					$html.='<td valign="top" width="340" style="font:normal 12px arial;">';
					if ($option['quantite'])
						$html.= $option['quantite'].' ';
					$html.= $option['nom'];
					$html.='</td>';
					$html.='<td valign="top" width="40" align="right" style="font:normal 12px arial;">'.$option->getPrix().'</td>';
				$html.='</tr>'."\n";
				// afficher les commentaires pour les kits
				if ($theme == 'kits')
				{
					$html.= '<tr>';
						$html.= '<td colspan="3" style="font:normal 12px arial;">'.strip_tags($option['commentaire'],'<br><br>').'</td>';
					$html.= '</tr>';
				}
			}
			// afficher de l'infomration si n�cessaire
			if ($theme == 'livraison' && !$info)
				$info = '<tr><td colspan="3" style="font-size:10px;">'."Notre agence vous recontactera par t�l�phone afin d'organiser la livraison et/ou reprise de votre v�hicule".'</td></tr>'."\n";
			// explications pour la franchise
			if ($groupe == 'franchise' && $this->_reservation)
			{
				if ($optionID == 'nf'.$type && !$this->hasOption('rf'.$type) && !$this->hasOption('af'.$type))
				{
					$categorie = $this->_reservation->getCategorie();
					$info = "Vous n'avez pas souhait� souscrire � la garantie r�duction de franchise.";
					$info.="<br/>Vous n'avez pas souhaite souscrire � l'assurance remboursement de franchise.";
					$info.="<br/>Le montant de votre franchise est de ".show_money($categorie['franchise'])."&euro;. Si vous souhaitez compl�ter votre r�servation, vous pourrez le faire en agence le jour de la prise de votre v�hicule.";
				}
				else if ($optionID == 'rf'.$type && !$this->hasOption('af'.$type))
				{
					$info = "Vous n'avez pas souhait� souscrire � l'assurance remboursement de franchise.";
 					$infi.="<br/>Le montant  de votre responsabilit� en cas de sinistre est �gal au montant de la franchise r�duite soit ".show_money($categorie['franchise_rf'])."�. Si vous souhaitez compl�ter votre r�servation, vous pourrez le faire en agence le jour de la prise de votre v�hicule.";
				}
				if ($info)
				{
					$html.='<tr><td colspan="3" style="font-size:11px;">'.$info.'</td></tr>'."\n";
					$info = null;
				}
			}
		}
		$html.= $info."</table>\n";
		return $html;
	}
	/**
	* Renvoie le HTML correspondant aux options d'une r�servation
	* Utilis� dans le skin V3
	* @param bool $show_prix
	*/
	public function toHtml3($show_prix=true)
	{
		// intitul� des paiements
		$html = $paiement = null;

		// parcourir les options
		$theme = null;
		foreach($this->_items as /** @var ResOption */ $option)
		{
			if (preg_match('/^nfv[pu]$/', $option['option'])) continue;
			if ($option['paiement'] != $paiement)
			{
				if ($paiement)
					$html.=  "\n</ul>";
				$paiement = $option['paiement'];
			//	$html.= '<p class="options_titre"><span class="res_opt_titre">Vos options</span> <span class="res_opt_separator">|</span> <span class="infos_agence">'.self::$OPTIONS_HTML_DESC[$paiement].'</span></p>'."\n";
				$html.= '<ul>'."\n";
			}
			if ($theme != $option['theme'])
			{
			//	$html.='<li class="res_opt_theme">'.$option['theme_nom'].'</li>';
				$theme = $option['theme'];
			}
			$html.='<li class="res_opt_detail">';
			$html.= '<table class="res_opt_nom res_opt_'.$option['option'].'"><tr><td class="res_opt_name">'.$option;
			if ($show_prix && $option['prix']) {
				$html.= '</td> <td class="res_opt_prix">'.$option->getPrix().'</td></tr>';
			} else {
				$html.= '</td></tr>';
			}
			$html.='</table>';
			$html.='</li>'."\n";
		}
		if (!strlen($html))
			$html = '<span class="res_opt_none">Aucune option s�lectionn�e</span>';
		else
			$html .= "\n</ul>\n";
		return $html;
	}
}
?>