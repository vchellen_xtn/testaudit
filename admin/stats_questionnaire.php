<?
	require_once('../constant.php');
	require_once("secure.php"); 
	require_once("lib.php");
	if (!$adminUser || (!$adminUser->hasRights(Acces::SB_ADMIN) && !$adminUser->hasRights(Acces::SB_SATISFACTION))) {
		die("Vous n'avez pas le droit d'acc�der � cette page !");
	}
	// �dition d'un commentaire
	if (isset($_POST['frm_commentaire'])) {
		$json = array('status' => 'KO');
		$commentaire = array(
			'agence' => $_POST['agence'],
			'reservation' => (int) $_POST['reservation'],
			'question' => (int) $_POST['question'],
			'prenom' => utf8_decode($_POST['prenom']),
			'refnat' => iconv('UTF-8', 'WINDOWS-1252', $_POST['refnat']),
			'publie' => (int) $_POST['publie'],
			'publication' => $_POST['publication'],
			'createur' => $adminUser['login'],
		);
		if (Agence::isValidID($commentaire['agence'])) {
			$json['status'] = 'OK';
			$json['publie'] = $commentaire['publie'];
			save_record('agence_commentaires', $commentaire);
		}
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($json);
		exit();
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
	<link rel="stylesheet" type="text/css" href="../css/anytime.min.css" />
	<link rel="stylesheet" type="text/css" href="../css/jquery-ui.custom.1.11.css" />
	<script type="text/javascript" src="../scripts/anytime.min.js"></script>
	<script type="text/javascript" src="../scripts/script.js"></script>
	<script type="text/javascript" src="admin.js"></script>
	<script type="text/javascript">
	// <![CDATA[
		// onload
		var dateFormat = '%d-%m-%Y';
		var dateConverter = new AnyTime.Converter({format: dateFormat});
		var dateOptions = getOptionsAnyTime(dateFormat, "depuis");

		// onload
		$(document).ready(function()
		{
			AnyTime.picker('date_from', dateOptions);
			dateOptions.labelTitle = "jusqu'�";
			AnyTime.picker('date_to', dateOptions);
			
			// les mises � jour de listes
			$("#zone, #reseau").change(function(){
				updateSelect(this.form.code_societe);
			});
			$("#zone, #reseau, #code_societe").change(function(){
				updateSelect(this.form.agence);
			});
			// ouvrir le dialogue pour �diter un commentaire
			$("a.edit").click(function() {
				// quel est le statut actuel ?
				var $tr = $(this).parents('tr');
				// remplir le formulaire
				var frm = document.getElementById("frm_commentaire");
				frm.reset();
				frm.reservation.value = $(this).data("reservation");
				frm.question.value = $(this).data("question");
				frm.agence.value = $(this).data("agence");
				frm.prenom.value = $(this).data("prenom");
				frm.publication.value = $(this).data("publication");
				$editDialog.dialog("option", "title", $tr.find(".commentaire--title").text());
				$("#commentaire_original").html($tr.find(".commentaire--original").html());
				$("#commentaire_refnat").val($tr.find(".commentaire--refnat").html());
				if ($tr.hasClass('publie')) {
					$("#commentaire_publie_1").prop("checked", true);
				} else {
					$("#commentaire_publie_0").prop("checked", true);
				}
				
				// puis on affiche le dialogu
				$editDialog.dialog("option", "position", { my: "left top", at: "right center", "of": this });
				$editDialog.dialog("open");
				
				// ne pas suivre le lien
				return false;
			});
			// traiter le formulaire � la soumission
			$("#frm_commentaire").submit(function() {
				var frm = this;
				var $tr = $("#commentaire-" + frm.reservation.value + "-" + frm.question.value);
				$.post(frm.action, $(frm).serialize(), function(data) {
					// on met � jour
					if (data.status == "OK") {
						if (data.publie == 1) {
							$tr.addClass("publie");
							$tr.find("span.publie").text("publi�");
						} else {
							$tr.removeClass("publie");
							$tr.find("span.publie").text("non publi�");
						}
						$tr.find(".commentaire--refnat").html(frm.refnat.value);
					} else {
						alert("Une erreur s'est produite lors de l'enregistrement !");
					}
					$editDialog.dialog("close");
				}, 'json');
				return false;
			});
			
			// configuration du dialogue
			$editDialog = $( "#dialog-form" ).dialog({
				autoOpen: false,
				height: "auto",
				width: 500,
				modal: true,
				resizable: false,
				draggable: false,
				buttons: {
					"Enregistrer": function() {
						$("#frm_commentaire").submit();
					},
					"Annuler": function() {
						$editDialog.dialog("close");
					}
				}
			});
		});
	// ]]>
	</script>
	<style type="text/css">
		select,input { width: inherit;}
		label { padding-left: 2px; }
		label.col1 { padding-left: 0; float: left; width: 65px;}
		tr.publie { background-color: #00ff00; }
		pre.commentaire--refnat { border: dashed 1px red; }
	</style>
</head>
<body>
<?= SQLExporter::getForm(); ?>
<h1>S�lection des r�ponses :</h1>
	<form method="post" action="" name="frm_satisfaction" id="frm_satisfaction">
		<fieldset><legend>Dates</legend>
			<p>
				<?
					foreach(array('date_from' => date('d-m-Y', strtotime(SITE_MODE=='PROD' ? '-1 week' : '-2 years')),'date_to'=>date('d-m-Y')) as $k => $v)
					{
						if (!$_POST[$k])
							$_POST[$k] = $v;
					}
				?>
				<? 
				foreach(array('date_from'=>'Depuis','date_to'=>"Jusqu'�") as $k => $v)
				{
					echo '<label for="'.$k.'">'.$v.'</label>';
					echo '<input type="text" id="'.$k.'" name="'.$k.'" size="10" maxlength="10" value="'.htmlspecialchars($_POST[$k]).'">';
				}
				?>
			</p>
		</fieldset>
		<fieldset><legend>Localisation</legend>
			<p>
				<label class="col1" for="pdv">Pt de vente</label><input type="text" value="<?=htmlspecialchars($_POST['pdv'])?>" id="pdv" name="pdv" size="7" maxlength="6"/>
			</p>
			<p>
				<label class="col1" for="zone">Zone</label><? selectsql('zone', "select id, nom from zone order by nom", $_POST['zone'], '---');?>
				<label for="reseau">R�seau</label><? selectsql('reseau', "select distinct reseau, reseau from agence where statut&1=1 order by reseau", $_POST['reseau'], '---');?>
				<label for="code_societe">Soci�t�</label>
				<?
					$sql = "select distinct code_societe, concat(code_societe, ' - ', societe) from agence where nullif(code_societe,'') is not null and statut&1=1";
					if (preg_match('/^[a-z]{2}$/', $_POST['zone'])) {
						$sql.=" and zone='".$_POST['zone']."'";
					}
					$sql.=" order by code_societe";
					selectsql('code_societe', $sql, $_POST['code_societe'], '---');
				?>
			</p>
			<p>
				<label class="col1" for="agence">Agence</label>
				<?
					$sql = "select ap.id, concat(ap.id,' - ', ap.nom) from agence_pdv ap join agence a on a.id=ap.agence where a.statut&1=1";
					if (preg_match('/^[a-z]{2}$/', $_POST['zone'])) {
						$sql.=" and a.zone='".$_POST['zone']."'";
					}
					if ($_POST['code_societe']) {
						$sql.=" and a.code_societe='".filter_var($_POST['code_societe'], FILTER_SANITIZE_STRING)."'";
					}
					$sql.=" order by id";
					selectsql('agence', $sql, $_POST['agence'], '---');
				?>
			</p>
		</fieldset>
		<fieldset><legend>V�hicule</legend>
			<p>
				<label class="col1" for="type">Type</label><? selectsql('type', "select distinct type, UCASE(type) from categorie where publie=1 order by type", $_POST['type'], '---');?>
				<label for="categorie">Cat�gorie</label><? selectsql('categorie', "select id, CONCAT(type,' - ',mnem,' - ', nom) from categorie where publie=1 order by type, mnem", $_POST['categorie'], '---');?>
			</p>
		</fieldset>
		<p>
			<input type="submit" name="search" value="rechercher">
			&nbsp;<input type="submit" name="reset" value="effacer">
		</p>
		<div class="clear"></div>
</form>
<?

	if (isset($_POST['search']))
	{
		$fields = explode(',','zone,agence,pdv,code_societe,reseau,type,categorie');
		echo "<h1>R�sultats</h1>\n";
		$x = QuestionResults::factory($_POST);
		$totalSend = $x->getTotalSend();
		$total = $x->getTotal();
		$results = $x->getResults();
		echo "<p><strong>$total r�pondant(s) sur $totalSend invitations</strong></p>\n";
		if (!$total)
		{
			$results = array();
			echo "<p>Aucune r�ponse ne correspond � votre recherche</p>\n";
		}
		else
		{
			// les boutons pour exporter
			// construire le nom du fichier
			$names = array($_POST['date_from'], $_POST['date_to']);
			foreach($fields as $k)
			{
				if ($_POST[$k])
				{
					if ($k == 'categorie')
						$names[] = Categorie::factory($_POST[$k])->getData('mnem');
					else
						$names[] = $_POST[$k];
				}
			}
			// export des r�ponses
			$sql_export = $x->getSQLExportResults();
			echo SQLExporter::getButton('exporter r�ponses', 'ADA_rep_'.join('_', $names), $sql_export);
			echo '&nbsp;&nbsp;';
			$sql_export = $x->getSQLExportComments();
			echo SQLExporter::getButton('exporter commentaires', 'ADA_txt_'.join('_', $names), $sql_export);
			echo '&nbsp;&nbsp;';
			$sql_export = $x->getSQLExportByAgence();
			echo SQLExporter::getButton('exporter notes par agence', 'ADA_pdv_'.join('_', $names), $sql_export);
			echo '&nbsp;&nbsp;';			
			$sql_export = $x->getSQLExportByAgenceQuestion();
			echo SQLExporter::getButton('exporter notes par agence et question', 'ADA_pdvquestion_'.join('_', $names), $sql_export);
		}
		
		// tableau des cat�goreis
		$categories = array();
		// afficher les r�sulstats
		foreach ($x->getResults() as $theme => $questions)
		{
			echo "<h2>$theme</h2>\n";
			foreach($questions as $q_id => $question)
			{
				echo '<table class="framed stats" border="1" cellpadding="2" cellspacing="0" width="400">'."\n";;
				echo '<th colspan="3">'.$question['question'].' ('.$question['total'].' r�ponses)</th>'."\n";
				if ($question['type']=='O' && $question['total'] > 0)
				{
					$rs_text = sqlexec($x->getSQLComments($q_id));
					$cnt = 1;
					while ($row_text = mysql_fetch_assoc($rs_text))
					{
						if (!$categories[$row_text['type']]) {
							$categories[$row_text['type']] = Categorie_Collection::factory('fr', $row_text['type']);
						}
						echo '<tr id="'.sprintf('commentaire-%ld-%ld"', $row_text['reservation'], $row_text['question']).'"';
						if ($row_text['publie']) {
							echo ' class="publie"';
						}
						echo '>';
						echo '<td width="20" valign="top" align="right">'.$cnt++.'.</td>';
						echo '<td width="50" valign="top">';
							echo euro_iso(substr($row_text['creation'], 0, 10));
							echo '<br/><span class="publie">'.($row_text['publie'] ? 'publi�' : 'non publi�').'</span>';
							echo '<br/><a class="edit" href="#" data-reservation="'.$row_text['reservation'].'" data-question="'.$row_text['question'].'" data-agence="'.$row_text['pdv'].'" data-prenom="'.$row_text['prenom'].'" data-publication="'.$row_text['creation'].'">�diter</a>';
						echo '</td>';
						echo '<td>';
							echo '<div class="commentaire--title">';
								echo '<a target="_blank" href="reservation.php?id='.$row_text['reservation'].'">#'.$row_text['reservation'].'</a>';
								echo ' - '.$row_text['prenom'];
								echo ' - '.strtoupper($row_text['type']).' - '.$categories[$row_text['type']][$row_text['categorie']]['mnem'];
								echo ' - '.$row_text['pdv'].' - '.$row_text['nom'];
							echo '</div>';
							echo '<pre class="commentaire--original">';
								echo $row_text['txt'];
							echo '</pre>';
							echo '<pre class="commentaire--refnat">';
								echo $row_text['refnat'];
							echo '</pre>';
						echo '</td>';
						echo "</tr>\n";
					}
				}
				else
				{
					foreach ($question['choix'] as $choix => $nb) {
						echo '<tr><td>'.$choix.'</td><td width="50" align="right">'.$nb.'</td><td width="50" align="right">'.round(($nb*100)/max(1, $question['total']),2).' %</td></tr>'."\n";
					}
					echo '<tr><th>Total</th><th align="right">'.$question['total'].'</th><th align="right">100%</th></tr>'."\n";
				}
				echo '</table>';
				echo '<br/>';
			}
		}
	}
?>
<div id="dialog-form" title="Edition Commentaire">
  <form id="frm_commentaire" action="" method="post">
    <input type="hidden" name="frm_commentaire" value="1"/>
    <input type="hidden" name="reservation" value=""/>
    <input type="hidden" name="question" value=""/>
    <input type="hidden" name="agence" value=""/>
    <input type="hidden" name="prenom" value=""/>
    <input type="hidden" name="publication" value=""/>
    <fieldset><legend>Texte original</legend>
    	<pre id="commentaire_original"></pre>
    </fieldset>
    <fieldset><legend>Texte modifi�</legend>
	 	<textarea id="commentaire_refnat" name="refnat" rows="6"></textarea>
		<label><input type="radio" id="commentaire_publie_1" name="publie" value="1"/>publi�</label>
		<label><input type="radio" id="commentaire_publie_0" name="publie" value="0"/>non publi�</label>
      </p>
    </fieldset>
  </form>
</div>
</body>
</html>