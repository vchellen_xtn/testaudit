<?
	include ('../secure.php');
	$title 	 = "Campagnes";
	$url 	 = "../promotion_campagne.php";
	$key	 = "p.id";
	$order	 = "";

	$sql = "select p.id, p.cible, p.debut, p.fin, c.nom as classe, p.nom";
	$sql.= " from promotion_campagne p";
	$sql.= " left join partenaire_classe c on c.id=p.classe";
	$sql.= " where 1";
	
	if ($_GET['classe'])
		$sql.= " and p.classe=".(int) $_GET['classe'];
	
	$out_url = "list.php?sql=" . urlencode($sql);
	$out_url.= "&title=" . urlencode($title);
	$out_url.= "&url=" . urlencode($url);
	$out_url.= "&new_url=" . urlencode($url . "?" . $_SERVER['QUERY_STRING']);
	$out_url.= "&color=4587ba";
	$out_url.= "&key=".$key."&order=".$order."&nb_rows=30";
	header("location: ".$out_url);
?>
