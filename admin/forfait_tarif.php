<?
	require_once("secure.php"); 
	require_once("lib.php");

	// identifiant
	$id = (int) getarg("id");
	if (!$id) $id = null;
	if (!$_GET['type'])
	{
		if ($_GET['forfait'] && preg_match('/^[a-z0-9\_\-]+$/i', $_GET['forfait']))
			$_GET['type'] = getsql("select type from forfait2 where id='".mysql_escape_string($_GET['forfait'])."'");
		if ($_GET['categorie'])
			$_GET['type'] = getsql("select type from categorie where id='".(int)$_GET['categorie']."'");
	}
	
	// suppression
	if ($id && getarg("delete")) 
	{
		$sql = "delete from forfait2_tarif where id = '".addslashes($id)."'";
		getsql($sql);
		unset($id);
	}
	
	// modification, cr�ation
	if (getarg("update") || getarg("create"))
	{
		// on v�rifie l'unicit� avant de cr�er un nouvel enregistrement
		if (!$id)
		{
			$sql = "select id from forfait2_tarif where 1=1";
			foreach(array('zone','forfait','categorie') as $k)
				$sql.=" and $k='".addslashes($_POST[$k])."'";
			$sql.=' and groupe'.($_POST['groupe'] ? "='".addslashes($_POST['groupe'])."'" : ' is null');
			$sql.=' and saison'.($_POST['saison'] ? "='".addslashes($_POST['saison'])."'" : ' is null');
			if ($exist = getsql($sql))
				die('<a href="?id='.$exist.'">Le tarif avec ces conditions existe d�j� : #'.$exist.'</a>');
		}
		save_record('forfait2_tarif', $_POST);
		if (!$id) $id = mysql_insert_id();
	}
	
	if ($id)
	{
		$rs = sqlexec('select * from forfait2_tarif where id = "'.addslashes($id).'"');
		if (!$rs || !($row = mysql_fetch_assoc($rs)))
			die("Aucun enregistrement correspondant ($id).");
	}
	$zone = $row['zone'] ? $row['zone'] : $_SESSION['ADA001_ADMIN_ZONE'];
	if (!$zone) die("Vous devez choisir une zone de travail !");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script>
	<!-- //
	// doDelete
	function doDelete(frm)
	{
		return (confirm("Voulez-vous r�ellement supprimer cet �l�ment ?\nCette op�ration est irr�versible."));
	}
	// doCheck
	function doSubmit(frm)
	{
		var err = "";
		if (frm.forfait && !frm.forfait.options[frm.forfait.selectedIndex].value)
			err+="Veuillez indiquer un forfait\n";
		if (frm.categorie && !frm.categorie.options[frm.categorie.selectedIndex].value)
			err += "Veuillez indiquer une categorie\n";
		var prix = frm.prix.value.replace(/,/, ".");
		if(!frm.prix.value.length || isNaN(prix))
			err+= "Veuillez indiquer le prix\n";
		var prix_jour = frm.prix_jour.value.replace(/,/, ".")
		if(!frm.prix_jour.value.length || isNaN(prix_jour))
			err+= "Veuillez indiquer le prix_jour\n";
		var km_sup = frm.km_sup.value.replace(/,/, ".")
		if(frm.km_sup.value.length && isNaN(km_sup))
			err+= "Veuillez indiquer le kilom�tre suppl�mentaire\n";
		// affiche le message d'erreur
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		return true;
	}
	// -->
	</script>
</head>
<body>
<form action="" method="post">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre">
	<img src="img/puce.gif" align="absmiddle" hspace="1">
<? 
	echo "Tarif";
?>
	</th>
</tr>
<tr>
	<td>
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="15%">
		<col width="35%">
		<col width="15%">
		<col width="35%">
		<tr>
			<td>ID</td>
			<td>
			<?
				echo $id;
				echo '<input type="hidden" name="id" value="'.$id.'">';
				echo '<input type="hidden" name="zone" value="'.$zone.'">';
			?>
			</td>
			<td colspan="2"></td>
		</tr>
		<tr>
			<td><label for="forfait">Forfait</label></td>
			<td colspan="3">
			<?
				if ($row['forfait'])
				{
					list ($_GET['type'], $forfait_nom) = getsql("select type, nom from forfait2 where id='".addslashes($row['forfait'])."'");
					echo '<a href="forfait.php?id='.$row['forfait'].'">'.$row['forfait'].' - '.$forfait_nom.'</a>';
				}
				else
				{
					$sql = "select id, concat(id,' - ', nom) from forfait2 where 1";
					if ($_GET['forfait'])
						$sql.=" and id='".addslashes($_GET['forfait'])."'";
					if ($_GET['type'])
						$sql.=" and type='".addslashes($_GET['type'])."'";
					$sql.=' order by id';
					selectsql('forfait', $sql, $_GET['forfait'], '--');
				}
			?>
			</td>
		</tr>
		<tr>
			<td><label for="categorie">Cat�gorie</label></td>
			<td colspan="3">
			<?
				if ($row['categorie'])
					echo '<a href="categorie.php?id='.$row['categorie'].'">'.getsql("select concat(mnem,' - ', nom) from categorie where id='".$row['categorie']."'").'</a>';
				else
				{
					$sql = "select id, concat(mnem,' - ', nom) from categorie where zone='fr' and publie=1";
					if ($_GET['type']) $sql.=" and type='".addslashes($_GET['type'])."'";
					$sql.=" order by mnem";
					selectsql('categorie', $sql, $_GET['categorie'], "--");
				}
			?>
			</td>
		</tr>
		<tr>
			<td><label for="groupe">Groupe</label></td>
			<td>
			<?
				if ($id)
					echo $row['groupe'] ? getsql("select nom from zone_groupe where id='".$row['groupe']."'") : 'par d�faut';
				else
					selectsql('groupe', "select id, nom from zone_groupe where zone='".$zone."'", $row['groupe'], '-- par d�faut --');
			?>
			</td>
			<td><label for="saison">Saison</label></td>
			<td>
			<?
				if ($id)
					echo $row['saison'] ? getsql("select libelle from zone_saison where id='".$row['saison']."'") : 'par d�faut';
				else
					selectsql('saison', "select id, libelle from zone_saison where zone='".$zone."'", $row['saison'], '-- par d�faut --');
			?>
			</td>
		</tr>
		<tr>
			<td><label for="prix">Prix TTC</label></td>
			<td><input type="text" class="l" id="prix" name="prix" value="<?=$row["prix"]?>" style="width:50%">&nbsp;�</td>
			<td><label for="prix_jour"><?=(strtolower($_GET['type'])=='ml' ? 'Heure' : 'Jour')?> supp.</label></td>
			<td><input type="text" class="l" id="prix_jour" name="prix_jour" value="<?=$row["prix_jour"]?>" style="width:50%">&nbsp;�</td>
		</tr>
		<tr>
			<td><label for="km_sup">Km supp.</label></td>
			<td><input type="text" class="l" id="km_sup" name="km_sup" value="<?=$row['km_sup']?>" style="width:50%">&nbsp;�</td>
			<td colspan="2">par d�faut le co�t du kilom�tre suppl�mentaire de la cat�gorie</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td align="right">
		<input type="submit" name="<?=$id ? "update" : "create"?>" value="enregistrer" onclick="return doSubmit(this.form);">
<?
	if ($id) 
	{
?>
		&nbsp;
		<input type="submit" onclick="return doDelete(this.form);" name="delete" value="supprimer">
<?
	}
?>
	</td>
</tr>
</table>
</form>
</body>
</html>
