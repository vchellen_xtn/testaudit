<?php
/**
* Tracking Facebook
*/
class Tracking_Facebook 
{
	static public function doTracking(Page $page) {
		/* tracking retargeting */
		if (SITE_MODE == 'PROD' && in_array($page->getSkin(), array('jeunes','v2','v3'))) :?>
		<script type="text/javascript">
		(function() {
			var _fbq = window._fbq || (window._fbq = []);
			if (!_fbq.loaded) {
			var fbds = document.createElement('script');
			fbds.async = true;
			fbds.src = '//connect.facebook.net/en_US/fbds.js';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(fbds, s);
			_fbq.loaded = true;
		}
		_fbq.push(['addPixelId', '1425506251092006']);
		})();
		window._fbq = window._fbq || [];
		window._fbq.push(['track', 'PixelInitialized', {}]);
		</script>
		<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=1425506251092006&amp;ev=PixelInitialized" /></noscript>
		<? endif;
		return;
	}
	
}
?>
