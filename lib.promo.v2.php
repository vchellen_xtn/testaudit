<?
	if ($_GET['mode'] == 'ajax')
	{
		header("Content-type: text/html; charset=iso-8859-1");
		header("X-Robots-Tag: noindex");
		require_once 'constant.php';
	}
	// validation des arguments
	if ($_GET['dept'] && !Dept::isValidID($_GET['dept']))
		unset($_GET['dept']);
	if ($_GET['agence'] && $_GET['agence']!='%' && !Agence::isValidID($_GET['agence']))
		unset($_GET['agence']);
//	$_GET['agence'] = $_GET['agence'] ? $_GET['agence'] : ($_GET['dept'] ? '' : '%');

	// affichage
	$promotions = PromotionForfait_Collection::factory($_GET, false);
	$num_rows = $promotions->count();
	// pour les agences on n'arffiche rien s'il n'y a pas de promotions
	if (!$num_rows && is_object($this) && $this->getId()=='agences/agence')
		return;
	$str_found = ($num_rows > 1 ? 'promos trouv�es ' : 'promo trouv�e ');
	if ($_GET['dept'])
		$str_found .= "pour location de voiture ".getsql("SELECT nom FROM dept WHERE id='".addslashes($_GET['dept'])."'")." :";
	if($_GET['agence'] && $_GET['agence'] != '%')
		$str_found .= "pour l'agence location voiture ".getsql("SELECT aa.nom FROM agence_alias aa WHERE aa.id='".addslashes($_GET['agence'])."'")." :";
	echo '<br clear="all"><strong>'.$num_rows.' '.$str_found.'</strong>';
	echo '<br /><br />';
	$i=0;
	foreach($promotions as $row)
	{
		// afficher la promotion
		show_promotion ($row, ($i == 0));
		echo '<div class="dot_line"></div>';
		$i++;
	}

	// affiche le contenu d'une promotion
	// $row contient : slogan, reduction, visuel, description, mentions
	function show_promotion (PromotionForfait $row, $display='none')
	{
?>
<div class="promo_contenu<?if($display) echo ' open'; ?>" onclick="$(this).toggleClass('open');">
	<div class="promo_head">
		<h2><?=$row['slogan']?> <span class="rouge"><?=$row['benefice']?></span></h2>
	</div>
	
	<div class="promo_body" id="promo<?=$row['id']?>_<?=$row['agence']?>">
<?
	if ($row['agence']) 
	{
		$agence = Agence::factory($row['agence'], true);
?>
		<div class="adresse">
<?
	echo $agence['nom']."<br />\n"
		.$agence['adresse1']."<br />\n"
		.$agence['adresse2'].($agence['adresse2'] ? "<br />\n" : "\n")
		.$agence['cp'].' '.$agence['ville']."<br />\n"
		.($agence['tel'] ? 'Tel. '.$agence['tel'] : '')."<br />\n";
?>
			<a rel="noindex,nofollow" href="../popup.php?page=agences/agence&id=<?=$agence["id"]?>" onclick="return wOpen(this.href, 630, 800)" style="font-size:10px;color:#cc0000">Informations sur l'agence</a>
			<div class="bottom"></div>
		</div>
<?
	}
?>
		<div class="description">
			<p><?=$row['description']?></p>
			<p class="mentions"><?=$row['mentions']?></p>
		</div>
		<a rel="nofollow,noindex" href="<?=$row->getURLPromo();?>" onclick="wPromo(this.href);" class="btn center" title="location voiture promo <?=$row['nom'];?>">Profitez-en<span class="end"></span></a>
	</div>
</div>
<?
	}
?>