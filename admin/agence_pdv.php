<?
	require_once('secure.php');
	require_once('lib.php');
	require_once('lib.agence.php');

	// agence parente
	$agence = getarg("agence");
	// identifiant
	$id = getarg("id");

	// suppression
	if ($id && getarg("delete"))
	{
		// supprimer les pdv, les alias et dans refnat_canon
		sqlexec("DELETE refnat_canon FROM refnat_canon JOIN agence_alias ON (agence_alias.id=refnat_canon.id AND refnat_canon.type='agence') WHERE agence_alias.pdv='$id'");
		sqlexec("DELETE FROM agence_alias WHERE pdv = '$id'");
		foreach(array('agence_horaire','promotion_forfait','agence_promo_forfait','stop_sell') as $k)
			sqlexec("DELETE FROM $k WHERE agence = '$id'");
		sqlexec("DELETE FROM agence_pdv WHERE id='$id'");
		unset($id);
	}

	// modification
	if (getarg("update") || getarg("create"))
	{
		if (!$id)
		{
			list ($_POST['zone'], $_POST['statut']) = getsql("select zone, statut from agence where id='".$_POST['agence']."'");
			$id = $_POST['id'] = agence_get_id($agence);
		}
		$_POST["latitude"]  = deg2rad($_POST["latitude"]);
		$_POST["longitude"] = deg2rad($_POST["longitude"]);
		save_record("agence_pdv", $_POST);
		if (getarg('create'))
		{
			//... et les horaires
			$sql = "INSERT INTO agence_horaire (agence, jour) VALUES ('".$id."', 1)";
			for ($i=2; $i<8; $i++)
				$sql .= ", ('".$id."',".$i.")";
			sqlexec($sql);
		}
		// enregistrer l'alias'
		$_POST['canon'] = getsql("select canon from agence_alias where id='".$id."'");
		save_alias($_POST);
		// on met � jour les statuts (si publie a chang�...)
		agence_update_statut ($agence);

		// on enregistre le plan d'acc�s si fourni
		if (count($_FILES))
		{
			$sql = "UPDATE agence_pdv SET id = id";
			foreach ($_FILES as $key => $f)
			{
				if ($f['name']) {
					move_uploaded_file($f['tmp_name'], "../fichiers/agence/".$id.'.jpg');
					$sql.= ", ".$key.' = "fichiers/agence/'.$id.'.jpg"';
				} else {
					$sql.= ", ".$key.' = null';
				}
			}
			$sql.= " WHERE id = '".$id."'";
			getsql($sql);
		}
	}

	if($id)
	{
		$row = mysql_fetch_assoc(sqlexec("SELECT * FROM agence_pdv WHERE id = '".$id."'"));
		$agence = $row['agence'];
	}
	// nouveau pdv
	else if($agence)
	{
		$row = mysql_fetch_assoc(sqlexec("SELECT nom FROM agence WHERE id = '".$agence."'"));
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script language="javascript" src="../scripts/script.js"></script>
	<script language="javascript" src="admin.js"></script>
	<script>
	<!--

	// doDelete
	function doDelete(frm)
	{
		return (confirm("Voulez-vous r�ellement supprimer cet �l�ment ?\nCette op�ration est irr�versible."));
	}

	// doSubmit
	function doSubmit(frm)
	{
		var err = "";
		console.log(frm.surcharge_sp.value.length);
		if(!frm.nom.value.length)
			err += "Veuillez indiquer le nom\n";
		if(!frm.adresse1.value.length)
			err += "Veuillez indiquer l'adresse1\n";
		if (frm.zone && frm.zone.value=='fr' && !frm.cp.value.match(/^[0-9]{5}$/))
			err += "Le code postal est incorrect\n";
		if(!frm.ville.value.length)
			err += "Veuillez indiquer la ville\n";
		if(frm.latitude.value==0 || frm.latitude.value.length && !frm.latitude.value.match(/^\-?\d+\.?\d*$/) )
			err += "La latitude est invalide\n";
		if(frm.longitude.value==0 || frm.longitude.value.length && !frm.longitude.value.match(/^\-?\d+\.?\d*$/) )
			err += "La longitude est invalide\n";
		if (frm.zoom && !frm.zoom.value.match(/^\d+$/))
			err += "Le zoom n'est pas valide\n";
		if (frm.googleplus && frm.googleplus.value.length && !frm.googleplus.value.match(/\d{21}/))
			err += "L'identifiant Google+ n'est pas correct\n";
		if(frm.surcharge_vp.value.length && !frm.surcharge_vp.value.match(/^\d+(\.\d{2})?$/))
			err += "La valeur de la surcharge VP est incorrecte\n";
		if(frm.surcharge_vu.value.length && !frm.surcharge_vu.value.match(/^\d+(\.\d{2})?$/))
			err += "La valeur de la surcharge VU est incorrecte\n";
		if(frm.surcharge_sp.value.length && !frm.surcharge_sp.value.match(/^\d+(\.\d{2})?$/))
			err += "La valeur de la surcharge SP est incorrecte\n";
		if(frm.surcharge_ml.value.length && !frm.surcharge_ml.value.match(/^\d+(\.\d{2})?$/))
			err += "La valeur de la surcharge ML est incorrecte\n";
		if (!frm.vp_delai_min.value.match(/^\d*$/))
			err+= "Veuillez indiquer un d�lai VP correct\n";
		if (!frm.vu_delai_min.value.match(/^\d*$/))
			err+= "Veuillez indiquer un d�lai VU correct\n";
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		if (frm.googleplus && frm.googleplus.value.length && frm.googleplus.defaultValue!=frm.googleplus.value) {
			if (!confirm("Vous avez modifi� le lien vers Google+.\nConfirmez-vous ?")) {
				frm.googleplus.focus();
				return false;
			}
		}
		if (frm.vp_delai_min.value.match(/^\d+$/) || frm.vu_delai_min.value.match(/^\d+$/) )
		{
			if (!confirm("Vous enregistrez des d�lais d'ouverture sp�cifiques � cette agence.\nConfirmez-vous ?"))
				return false;
		}
		return true;
	}
	//-->
	</script>
<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
var geocoder = null;
function showLatLng(frm)
{
	var lat = frm.latitude;
	var lng = frm.longitude;
	var cp = (frm.cp.value == '...' ? '' : frm.cp.value);
	var address = frm.adresse1.value + ' ' + frm.adresse2.value + ' ' + cp + ' ' + frm.ville.value + ' <?=getsql("select z.nom from zone z join agence a on a.zone=z.id where a.id='$agence'")?>';
	if (!geocoder)
		geocoder = new google.maps.Geocoder();
	if (geocoder)
	{
		geocoder.geocode( { 'address': address}, function(results, status)
		{
			var output = [], found;
			if (status == google.maps.GeocoderStatus.OK)
			{
				for ( var i = 0; i < results.length; i++ )
				{
					output[i] = {
						location_type : results[i].geometry.location_type,
						latitude : results[i].geometry.location.lat(),
						longitude : results[i].geometry.location.lng(),
						formatted_address :  results[i].formatted_address
					};
					if ( results[i].geometry.location_type == google.maps.GeocoderLocationType.ROOFTOP
						|| results[i].geometry.location_type == google.maps.GeocoderLocationType.RANGE_INTERPOLATED)
					{
						found = output[i];
						break;
					}
				}
				if ( found )
				{
					var msg = "Les coordonn�es vont �tre mises � jour :\n"
						+ found.formatted_address + '\n';
					msg += lat.value + ' -> ' + found.latitude + "\n"; 
					msg += lng.value + ' -> ' + found.longitude + "\n";
					if (confirm(msg))
					{
						lat.value = found.latitude;
						lng.value = found.longitude;
					}
				}
				else
				{
					var msg = "Les coordonn�es sont approximatives, souhaitez-vous utiliser la carte pour les pr�ciser ?";
					if (confirm(msg))
					{
						lat.value = output[0].latitude;
						lng.value = output[0].longitude;
						show_gmap(document.forms[0]);
					}
				}
			}
			else
			{
				alert("Aucun r�sultat fiable trouv�, merci d'utiliser la carte");
			}
		});
	}
	return false;
}

function show_gmap(frm){
	var url = "gmap.php?pdv=<?=$id?>&lat="+frm.latitude.value+"&lng="+frm.longitude.value;
	w = window.open(url, 'target', 'toolbar=0, location=0, directories=0, status=1, scrollbars=0, resizable=1, copyhistory=0, menuBar=1, width=450, height=550, left=500, top=200')
	w.focus();
}
// afficher la page GooglePlus si renseign� dans le formulaire
function checkGooglePlus(anchor) {
	// r�cup�rer l'�l�ment du formulaire
	var el = document.getElementById('googleplus');
	if (el && el.value.match(/\d{21}/)) {
		anchor.href = "https://plus.google.com/" + el.value;
		return true;
	} else {
		alert("L'identifiant Google+ n'est pas correct !");
		el.focus();
	}
	return false;
}
</script>
</head>

<body>

<form action="" method="post" onsubmit="return doSubmit(this);" enctype="multipart/form-data">
	<input type="hidden" name="agence" value="<?=$agence?>" />
	<input type="hidden" name="zone" value="<?=$row['zone']?>" />
<table class="framed" border="0" cellspacing="0" cellpadding="4" style="float:left;">
<tr>
	<th class="bg_titre">
	<img src="img/puce.gif" align="absmiddle" hspace="1">
		<a href="agence.php?id=<?=$agence?>">Agence <?=$agence?></a> � Point de vente <?=$id?>
		<? if (isset($row['publie'])) echo '('.($row['publie']?'publi�':'archiv�').')'; ?>
	</th>
</tr>
<tr>
	<td>
	<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="15%">
		<col width="32%">
		<col width="15%">
		<col width="33%">
		<tr>
			<td><label for="id">identifiant</label></td>
			<td>
		<?
			echo $row['id'];
			echo '<input type="hidden" id="id" name="id" value="'.$row['id'].'" maxlength="6">';
		?>
			</td>
			<td><label for="publie">publi�</label></td>
			<td>
				<input type="hidden" name="publie[]" value="0">
				<input type="checkbox" id="publie" name="publie[]" value="1" style="width: inherit;" <? if (!isset($row['publie']) || $row['publie']) echo 'checked';?>>
			</td>
		</tr>
		<tr>
			<td><label for="nom">Nom</label></td>
			<td><input type="text" id="nom" name="nom" value="<?=$row["nom"]?>"></td>
			<td><label for="bal">Bo�te aux lettres</label></td>
			<td><input type="checkbox" id="bal" name="bal[]" value="1" style="width: inherit;"<? if ($row['bal']) echo ' checked'; ?>><input type="hidden" name="bal[]" value="0"></td>
		</tr>
		<tr>
			<td><label for="adresse1">Adresse 1</label></td>
			<td><input type="text" id="adresse1" name="adresse1" value="<?=$row["adresse1"]?>" maxlength="128"></td>
			<td><label for="adresse2">Adresse 2</label></td>
			<td ><input type="text" id="adresse2" name="adresse2" value="<?=stripslashes($row["adresse2"])?>" maxlength="128">
			</td>
		</tr>
		<tr>
			<td><label for="cp">Code postal</label></td>
			<td><input type="text" id="cp" name="cp" value="<?=$row["cp"]?>" maxlength="5" size="5" style="width: inherit;"></td>
			<td><label for="ville">Ville</label></td>
			<td><input type="text" id="ville" name="ville" value="<?=$row["ville"]?>" maxlength="64"></td>
		</tr>
		<tr>
			<td><label for="photo">Photo</label></td>
			<td colspan="3"><?=url_ctrl("photo", $row["photo"])?></td>
		</tr>
		<tr>
			<td colspan="2">
				<label for="latitude">Lat. :</label>
				<input type="text" name="latitude" id="latitude" value="<?=rad2deg($row["latitude"])?>" size="8"/>
				<label for="longitude">Long. :</label>
				<input type="text" name="longitude" id="longitude" value="<?=rad2deg($row["longitude"])?>" size="8"/>
				<a href="#"  onclick="showLatLng(document.forms[0]); return false;" ><img src="img/geoloc.gif" alt="calcul coordonn�es" title="calcul coordonn�es" border="0" align="absmiddle"/></a>
				&nbsp;
				<a href="#" onclick="show_gmap(document.forms[0]);return false;">voir carte</a>
			</td>
			<td><label for="zoom">Zoom</label></td>
			<td><input type="text" id="zoom" name="zoom" value="<?=(int)$row['zoom']?>" size="2" maxlength="2"/></td>
		</tr>
		<tr>
			<td><label for="googleplus">Google+</label></td>
			<td>
				<input type="text" id="googleplus" name="googleplus" size="26" maxlength="21" value="<?=$row['googleplus']?>"/>
				<a href="#" onclick="return checkGooglePlus(this);" target="_blank">voir G+</a>
			</td>
			<td><label for="emplacement">Emplacement</label></td>
			<td><? selectarray('emplacement', Agence::$EMPLACEMENT, $row['emplacement']); ?></td>
		</tr>
				<tr>
			<td valign="top"><label for="commentaire_localisation">Commentaire localisation</label></td>
			<td colspan="3"><input id="commentaire_localisation" name="commentaire_localisation" style="width:100%" value="<?=htmlspecialchars($row['commentaire_localisation'])?>"/></td>
		</tr>
		<tr>
			<td valign="top"><label for="commentaire_offre">Commentaire offre</label></td>
			<td colspan="3"><input id="commentaire_offre" name="commentaire_offre" style="width:100%" value="<?=htmlspecialchars($row['commentaire_offre'])?>"/></td>
		</tr>
		<tr>
			<td><label for="surcharge_vp">Surcharge VP</label></td>
			<td><input type="text" name="surcharge_vp" id="surcharge_vp" value="<?=$row["surcharge_vp"]?>"></td>
			<td><label for="surcharge_vu">Surcharge VU</label></td>
			<td><input type="text" name="surcharge_vu" id="surcharge_vu" value="<?=$row["surcharge_vu"]?>"></td>
		</tr>
		<tr>
			<td><label for="surcharge_sp">Surcharge SP</label></td>
			<td><input type="text" name="surcharge_sp" id="surcharge_sp" value="<?=$row["surcharge_sp"]?>"></td>
			<td><label for="surcharge_ml">Surcharge ML</label></td>
			<td><input type="text" name="surcharge_ml" id="surcharge_ml" value="<?=$row["surcharge_ml"]?>"></td>
		</tr>
		<tr>
			<td><label for="vp_delai_min">D�lai VP</label></td>
			<td><input type="text" maxlength="2" style="width: 3em;" name="vp_delai_min" id="vp_delai_min" value="<?=$row['vp_delai_min']?>"> heures</td>
			<td><label for="vu_delai_min">D�lai VU</label></td>
			<td><input type="text" maxlength="2" name="vu_delai_min" style="width: 3em;" id="vu_delai_min" value="<?=$row['vu_delai_min']?>"> heures</td>
		</tr>
		<tr>
			<td><label for="promo_mode">Restrictions promotions</label></td>
			<td colspan="3"><? selectarray('promo_mode', Forfait_Collection::$PROMO_MODES_LBL, $row['promo_mode'], '-- aucuen restriction --'); ?></td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%">
		<tr>
			<td valign="top">
			<? if ($id) : ?>
				<input type="button" onclick="go_frame(parent.list, 'lists/horaire.php?agence=<?=$id?>')" value="horaires">
				<input type="button" onclick="go_frame(parent.list, 'lists/promotion_forfait.php?agence=<?=$id?>')" value="promotions">
				<input type="button" onclick="go_frame(parent.list, 'lists/stopsell.php?agence=<?=$id?>')" value="stop sell">
				<br>
				<input type="button" onclick="go_frame(parent.list, 'lists/reservation.php?pdv=<?=$id?>')" value="r�servations">
			<? endif; ?>
			</td>
			<td align="right" valign="top">
				<input type="submit" name="<?=$id ? "update" : "create"?>" value="enregistrer">
				<? if ($id && $row['agence'] != $row['id'] && !getsql("SELECT id FROM reservation WHERE pdv='".$id."' LIMIT 1")) : ?>
				&nbsp;
				<input type="submit" onclick="return doDelete(this.form);" name="delete" value="supprimer">
				<? endif; ?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<?
	show_pdv($agence, $id);
?>
</form>
</body>
</html>
