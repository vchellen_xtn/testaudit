<?
	header("Content-Type: text/xml; charset: utf-8");
	header("X-Robots-Tag: noindex");
	include ('constant.php');

	// obtenir la zone de l'agence et les d�lais minimum
	$sql = "select upper(aa.id) as id, aa.pdv, aa.agence, lower(aa.zone) as zone, ap.bal, aa.nom "
		  ."	  ,coalesce(ap.vp_delai_min,z.vp_delai_min) as vp_delai_min"
		  ."	  ,coalesce(ap.vu_delai_min,z.vu_delai_min) as vu_delai_min"
		  ."	  ,coalesce(ap.sp_delai_min,z.sp_delai_min) as sp_delai_min"
		  ."	  ,coalesce(ap.sp_delai_min,z.sp_delai_min) as el_delai_min"
		  ."	  ,coalesce(ap.ml_delai_min,z.ml_delai_min) as ml_delai_min"
		  ."	  ,z.delai_mode"
		  ."  from agence_pdv ap join agence_alias aa on aa.pdv=ap.id join zone z on aa.zone = z.id"
		  ." where 1 = 1";
	if ($_GET['id'])
		$sql .= " and aa.id = '".addslashes($_GET['id'])."'";
	if ($_GET['name'])
		$sql .= " and '".addslashes($_GET['name'])."' = aa.recherche";
	echo '<'.'?xml version="1.0" encoding="utf-8"?'.'>'."\n";
	echo '<data ';
	if (is_array($row = mysql_fetch_assoc(sqlexec($sql))))
		foreach ($row as $key => $val)
			echo ' '.$key.'="'.($key=='nom' ? utf8_encode($val) : $val).'"';
	echo '>'."\n";

	// charger les fermetures
	$sql = "select unix_timestamp(debut) as debut, unix_timestamp(fin) as fin";
	$sql.="   from agence_fermeture f left join agence_ouverture o on (o.agence='".$row['agence']."' and o.fermeture=f.id)";
	$sql.= " where f.zone='".$row['zone']."' and f.agence in ('%','".$row['agence']."') and f.fin >= '".date('Y-m-d')."'";
	$sql.=" and o.id is null";
	$sql.= " order by debut";

	echo '<fermetures>'."\n";
	for ($rs=sqlexec($sql); $a=mysql_fetch_assoc($rs); )
	{
		for ($t=$a["debut"];$t<=$a["fin"]; $t+=24 * 3600)
			echo '<fermeture an="'.date("Y", $t).'" mois="'.date("m", $t).'" jour="'.date("d", $t).'"/>'."\n";
	}
	echo '</fermetures>'."\n";

	// charger les horaires
	$sql = "select * from agence_horaire";
	$sql.= " where agence='".$row['pdv']."'";
	$sql.= " order by jour";

	echo '<horaires>'."\n";
	for ($rs=sqlexec($sql); $a=mysql_fetch_assoc($rs); )
		echo '<horaire jour="'.$a["jour"].'" ouverture="'.$a["ouverture"].'" fermeture="'.$a["fermeture"].'" pause_debut="'.$a["pause_debut"].'" pause_fin="'.$a["pause_fin"].'"/>'."\n";

	echo '</horaires>'."\n";
	echo '</data>'."\n";
?>