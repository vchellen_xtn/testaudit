<?
class Franchise extends Ada_Object
{
	/**
	* Renvoie le tableau des r�gles de validation
	*/
	protected function _getValidatingRules()
	{
		return array (
			'civilite'	=> $this->_addRuleString("Veuillez pr�ciser votre civilit�"),
			'nom' 		=> $this->_addRuleString("Veuillez pr�ciser votre nom"),
			'prenom'	=> $this->_addRuleString("Veuillez pr�ciser votre pr�nom"),
			'email'		=> $this->_addRuleEmail("Veuillez indiquer votre adresse email", "L'e-mail %s est incorrecte"),
			'tel'		=> $this->_addRuleRegExp('/^\+?[0-9\s-\.]{10,16}$/', null, "Veuillez indiquer un num�ro de t�l�phone fixe valide"),
			'gsm'		=> $this->_addRuleRegExp('/^\+?[0-9\s-\.]{10,16}$/', null, "Veuillez indiquer un num�ro de t�l�phone portable valide"),
			'naissance'	=> $this->_addRuleDate("Veuillez pr�ciser votre date de naissance", "Votre date de naissance doit �tre au format JJ/MM/AAAA"),
			'localopt'	=> $this->_addRuleInt("Veuillez pr�ciser si vous poss�dez un local", null, 0, 1),
			'situation_fam'	=> $this->_addRuleString("Veuillez pr�ciser votre situation familiale"),
			'situation_pro'	=> $this->_addRuleString("Veuillez pr�ciser votre situation professionnelle"),
			'delai'		=> $this->_addRuleString("Veuillez pr�ciser votre d�lai de r�alisation souhait�"),
			'montant'	=> $this->_addRuleString("Veuillez indiquer le montant financier dont vous disposez"),
			'optin'		=> $this->_addRuleBoolean(),
			'societe'	=> $this->_addRuleString(),
			'adresse'	=> $this->_addRuleString(),
			'cp' 		=> $this->_addRuleRegExp('/^\d{5}$/', null, "Votre code postal est invalide"),
			'ville'		=> $this->_addRuleString(),
			'local'		=> $this->_addRuleString(),
			'suggestion_ville'	=> $this->_addRuleString(),
			'message'	=> $this->_addRuleString(),
			'rappel'	=> $this->_addRuleInt(),
		);
	}
}