<?
	$title 	 = "Reservation Annulation Motif";
	$url 	 = "../res_annulation_motif.php";
	$key	 = "position";
	$order	 = "";

	$sql = "select id, position, case actif when 1 then 'oui' else 'non' end as actif, case partiel when 1 then 'oui' else 'non' end as partiel, libelle from res_annulation_motif";
	
	$out_url = "list.php?sql=" . urlencode($sql);
	$out_url.= "&title=" . urlencode($title);
	$out_url.= "&url=" . urlencode($url);
	$out_url.= "&new_url=" . urlencode($url . "?" . $_SERVER['QUERY_STRING']);
	$out_url.= "&color=4587ba";
	$out_url.= "&key=".$key."&order=".$order."&nb_rows=30";
	header("location: ".$out_url);
?>
