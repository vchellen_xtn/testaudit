<?php
class Transaction_Cheque extends Transaction
{
	
	// on n'accepte pas le paiement par ch�que
	protected function _doPayment(Reservation $reservation, $args)
	{
		$this->_addMessage("Le paiement par ch�que n'est pas autoris�e.");
		return false;
	}
	// et l'annulation fonctionne syst�matiquement
	protected function _doRefund(Reservation $reservation, $montant)
	{
		$this->_addMessage('<strong style="color:red;">Vous devez effectuer le paiement par ch�que.</strong>');
		return true;
	}
	protected function _getModalitesRemboursementTotal()
	{
		return 'Afin de proc&eacute;der au remboursement dans les plus brefs d&eacute;lais, nous vous invitons &agrave; nous transmettre votre RIB &agrave; l\'adresse suivante <a href="mailto:serviceclient@ada.fr" style="color:#CC0000;">serviceclient@ada.fr</a>.<br/>Nous vous pr&eacute;sentons toutes nos excuses pour la g&ecirc;ne occasionn&eacute;e.';
	}
	protected function _getModalitesRemboursementPartiel($rembourse)
	{
		return $this->_getModalitesRemboursementTotal();
	}
}
?>
