<?
	$menu = array(
		'trouver-son-camion'	=> 'Choisir son utilitaire',
		'materiel'				=> 'Prévoir son matériel de déménagement',
		'memo'					=> 'Organiser son déménagement',
		'homebox'				=> 'Louer un garde meuble',
	);
	$action = basename($page);
	if(in_array($action, array('trouver-son-camion','choisir-son-camion','reserver', 'studio', '2-pieces', 'grand-volume')))
		$action = 'trouver-son-camion';
?>
<ul class="menu">
	<? foreach($menu as $k => $lbl) : ?>
		<li>
			<a href="<?=$k?>.html"<?if ($action==$k) echo ' class="current"'; ?>><?=$lbl;?></a>
		</li>
	<? endforeach; ?>
</ul>