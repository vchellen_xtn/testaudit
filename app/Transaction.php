<?php
/**
* Classe utilis� pour la r�servation et l'annulation
* Le paiement et/ou le remboursement est effectu�e par les classes d�riv�es
* Les modifications du statut de la r�servation et les notifications aux clients / agences / syst�me sont pilot�s par le parent
*/
abstract class Transaction extends Ada_Object
{
	public static $PAIEMENT_MODE = array('PB'=>'Paybox','PP'=>'Paypal','LP'=>'Locapass','CQ'=>'Cheque','UNEURO'=>'UnEuro');
	public static $PAIEMENT_LABEL = array('PB' => 'carte bancaire', 'PP' => 'PayPal', 'LP'=>'Locapass','UN' => '1 Euro');


	protected $_mode; // mode de paiement
	protected $_messages = array();
	protected $_errors = array();

	/**
	* Cr�e l'objet Transaction selon le mode de paiement
	*
	* @param string $mode (PB,PP,CQ,LP)
	* @returns Transaction
	*/
	static public function factory($mode)
	{
		if (!self::$PAIEMENT_MODE[$mode])
			$mode = key(self::$PAIEMENT_MODE);
		$className = 'Transaction_'.self::$PAIEMENT_MODE[$mode];
		/** @var Transaction */
		$x = new $className();
		if ($x) $x->_mode = substr($mode, 0, 2);
		return $x;
	}
	// fonctions abstraites devant �tre impl�ment�s par les sous classes
	abstract protected function _doPayment(Reservation $reservation, $args);
	abstract protected function _doRefund(Reservation $reservation, $montant);
	abstract protected function _getModalitesRemboursementTotal();
	abstract protected function _getModalitesRemboursementPartiel($rembourse);


	// gestion des messages et des erreurs
	protected function _addMessage($msg) { $this->_messages[] = $msg; return $this; }
	public function hasMessages() { return !empty($this->_messages); }
	public function getMessages() { return $this->_messages; }
	protected function _addError($msg) { $this->_errors[] = $msg; return $this; }
	public function hasErrors() { return !empty($this->_errors); }
	public function getErrors() { return $this->_errors; }

	// fonctions publiques
	/**
	* Effectue le paiement d'une r�servation
	*
	* @param Reservation $reservation
	* @param array $args informations sur le mode de paiement
	* @returns bool
	*/
	public function doPayment(Reservation $reservation, $args)
	{
		// on veut �viter que 2 pages s'ex�cutent en parall�lle
		sqlexec("UPDATE reservation SET statut='X' WHERE id=".$reservation['id']." AND statut='V'");
		if (1 != rowsaffected())
		{
			$this->_addMessage("La r�servation est d�j� en cours de paiement! Veuillez consulter votre compte.");
			return false;
		}
		// s'il y a un code de r�duction on l'applique
		if ($code = $reservation->getPromotionCode())
		{
			$reservation->setData('total', $reservation['total'] - $code['reduction']);
		}
		// on appelle le paiement selon l'impl�mentation
		if (!$this->_doPayment($reservation, $args))
		{
			$reservation->setData('statut','V')->save('statut');
			return false;
		}
		/** @var ResOption_Collection r�cup�rer les options */
		$options = $reservation->getResOptions();
		// on change le statut de la r�servation et les informations assoic�es
		$this->_addMessage(($reservation->isLocapass() ? "La r�servation a �t� valid�e" : "Le r�glement a �t� effectu�").". Un e-mail de confirmation de r�servation vous a �t� envoy�.");
		$reservation->setData('statut','P')
					->setData('paiement_mode', $this->_mode)
					->setData('paiement',date('Y-m-d H:i:s'))
					->setData('facturation',date('Y-m-d H:i:s'))
					->save('statut,paiement_mode,paiement,facturation');
		if ($code)
		{
			$reservation->save('total');
			$code->doPayment();
		}
		// on cr�e les factures apr�s le paiement sauf pour Locapass
		$facturation = array();
		if (!$reservation->isLocapass())
		{
			$facturation['ADA'] = $reservation->getPrixForfait() + $reservation['surcharge'] + $options->getTotal('ADA');
			$facturation['MATERIEL'] = $options->getTotal('MATERIEL');
			$facturation['COURTAGE'] = $options->getTotal('COURTAGE');
			$facturation['PRO'] = $reservation->getPrixCoupon();
		}
		// on enregistre les factures
		// en r�partissant la r�duction du code grillable s'il y a lieu
		$reduction = $code['reduction'];
		foreach ($facturation as $k => $f)
		{
			if (!$f)
			{
				unset($facturation[$k]);
				continue;
			}
			if ($reduction)
			{
				$f = round($f - $reduction, 2);
				if ($f >= 0)
					$reduction = 0;
				else
				{
					$reduction = -$f;
					$f = 0;
				}
				$facturation[$k] = $f;
			}
			sqlexec("INSERT INTO facturation_".strtolower($k)." SET type='F', creation = NOW(), reservation=".$reservation->getId().", montant=".$f.", mode='".$this->_mode."'");
		}
		// on r�cup�re le coupon, si c'est un webpro il va servir pour l'adresse de facturation
		if ($reservation['coupon'])
			$coupon = $reservation->getPartenaireCoupon();
		// enregistrer l'adresse facturation
		if ($coupon['origine']=='webpro')
		{
			// on r�cup�re le partenaire
			$partenaire = $coupon->getPartenaire();

			// on enregistre l'adresse de facturation
			$res_info['f_societe'] = $partenaire['nom'];
			$res_info['f_adresse'] = $partenaire['adresse1'];
			$res_info['f_adresse2'] = $partenaire['adresse2'];
			$res_info['f_cp'] = $partenaire['cp'];
			$res_info['f_ville'] = $partenaire['ville'];
			$res_info['f_pays'] = $partenaire['pays'];

			// on enregisre le paiement du coupon si n�cessaire
			if ($coupon->isPaidBy($reservation) && $facturation['PRO'])
				ComptaClientys::doInscriptionPro($reservation, $coupon->getPartenaire() , $facturation['PRO']);
		}
		else
		{
			$res_info = $reservation->getClient()->toArray(array('f_societe','f_adresse','f_adresse2','f_cp','f_ville','f_pays'));
		}
		$res_info['reservation'] = $reservation->getId();
		save_record('res_info', $res_info);


		// on enregistre l'�v�nement comptable
		ComptaClientys::doReservation($reservation, $facturation['ADA'] + $facturation['MATERIEL']);
		// on cr�e maintenant les donn�es pur envoyer l'e-mail de confirmation et notifier client / agence / UNIPRO / CITER
		$data = $this->_getDataEmail($reservation);

		// les uatres donn�es
		$agence = $reservation->getAgence();
		$client = $reservation->getClient();
		$categorie = $reservation->getCategorie();
		$type = strtolower($reservation['type']);
		$coupon = $reservation->getPartenaireCoupon();

		// et finaliser les donn�es pour les e-mails
		if ($coupon) {
			$data['entreprise'] = $coupon->getEntreprise();
			if ($reservation->isLocapass()) {
				$data['code_locapass'] = $coupon['code'];
			}
		}
		$data['options_manquantes'] = join("<br>\n", $reservation->getMissingOptions());
		$data['materiel_retrait'] = '';
		if ($retrait = $reservation->getResOptions()->getDateRetrait())
		{
			if ($reservation->getHoursBeforeStart() <= 48)
				$data['materiel_retrait'] = "Confirmation de votre commande de mat�riels sous 24h";
			else
				$data['materiel_retrait'] = "Le mat�riel de d�m�nagement sera � votre disposition dans votre agence � la date que vous avez s�lectionn� : le ".Util_Format::dateFR('l d F Y', strtotime($retrait)).'.';
		}
		// on ajoute le block  des offres partenaires
		$data['res_partenaire'] = ResPartenaire::createPartenaires($reservation)->toEmailBlock();

		/**
		* On effectue plusieurs notifications
		* 1. e-mail : au client
		* 2. e-mail : au service client et � l'agence
		* 3. au syst�me de r�servation (UNIPRO ou CITER)
		* 4. � l'agence si �chec de la notificaiton automatique
		*/
		// email  client : diff�rent pour la livraison � domicile
		$eml = 'em1'; // em1.html
		if ($options->hasOptions("la$type,lr$type")) $eml .= '-ld'; // em1-ld.html
		else if (in_array($reservation['forfait_type'], array('JEUNES','LOCAPASS')))
			$eml .= '-'.strtolower($reservation['forfait_type']); // em1-locapass.html
		else if ($coupon['origine']=='ce') $eml .= '-ce';
		$html = load_and_parse(BP.'/emails/'.$eml.'.html', $data);

		if (substr($client['email'],-14)=="@nomail.ada.fr") {
		    $client['email']=$agence['email'];
		}

		send_mail (
			$html,
			$client['email'],
			($reservation['canal']=='WEB') ? SUBJECT_RESA_CLIENT : SUBJECT_RESA_CLIENT_CANAL_TC,
			($reservation['canal']=='WEB') ? EMAIL_FROM : EMAIL_FROM_CANAL_TC,
			EMAIL_FROM_ADDR,
			'',
			($reservation['canal']=='WEB') ? BCC_EMAILS_TO : BCC_EMAILS_TO_CANAL_TC
		);
		// Envoyer les e-mails li�s auc ourtage
		if (isset($facturation['COURTAGE']))
		{
			foreach(array('ab','af','ap') as $k)
			{
				if ($options->hasOption($k.$type))
				{
					$html = load_and_parse(BP."/emails/em14-$k.html", $data);
					if ($reservation['forfait_type']=='JEUNES')
						$html = str_replace('img/voucher3.gif', 'img/email/header_jeunes.jpg', $html);
					send_mail ($html, $client['email'], SUBJECT_RESA_CLIENT_ASSURANCE.' ['.$options->getItem($k.$type)->getData('nom').']', EMAIL_FROM, EMAIL_FROM_ADDR, '', (SITE_MODE!='PROD' ? BCC_EMAILS_TO : ''));
				}
			}
		}
		if (isset($facturation['MATERIEL']))
		{
			$data['list_materiel'] = $options->filter('MATERIEL')->toHtmlEmail();
			$html = load_and_parse(BP.'/emails/em1-materiel.html', $data);
			send_mail ($html, $client['email'], SUBJECT_RESA_CLIENT_MATERIEL, EMAIL_FROM, EMAIL_FROM_ADDR, '', BCC_EMAILS_TO);
		}

		// email agence : pr�parer avant l'appel UNIPRO
		$eml = 'em2'; // em2.html
		if ($reservation->isLocapass()) $eml .= '-locapass'; // em2-locapass.html
		else if ($coupon['origine']=='ce') $eml .= '-ce';
		$html = load_and_parse(BP.'/emails/'.$eml.'.html', $data);
		$to = (defined('EMAIL_AGENCE_DEBUG') ? EMAIL_AGENCE_DEBUG : $agence['email']);
		$subject = ($reservation['canal']=='WEB') ? SUBJECT_RESA_AGENCE : SUBJECT_RESA_AGENCE_CANAL_TC;
		$from = ($reservation['canal']=='WEB') ? EMAIL_FROM : EMAIL_FROM_CANAL_TC;
		$bcc = ($reservation['canal']=='WEB') ? BCC_EMAILS_TO : BCC_EMAILS_TO_CANAL_TC;
		// envoyer au service client avant l'appel UNIPRO en cas de plantage
		// on envoie directement � l'agence si elle n'est pas ouverte � la r�servation
		send_mail ($html, ($agence['notification'] == 'email' ? $to : $bcc), $subject, $from, EMAIL_FROM_ADDR, '', $bcc);

		// puis UNIPRO si l'agence est compatible
		if ($agence['notification']=='rpc')
		{
			set_time_limit(60);
			if ($agence['reseau']=='ADA')
			{
				// envoi � UNIPRO et en cas d'�chec envoi du mail � l'agence (// en cas d'�chec, envoi du mail � l'agence (encore en PROD jusqu'au 01/02/2010)
				try { $unipro = @new Unipro(); }
				catch (Exception $e) { $e = (string) $e; }
				if (!$unipro || !$unipro->sendReservation($reservation))
					send_mail ($html, $to, '[UNIPRO-ERREUR] '.$subject, $from, EMAIL_FROM_ADDR, '', $bcc);
			}
			else if ($agence['reseau']=='CITER')
			{
				send_mail ($html, $bcc, '[CITER-ERREUR] '.$subject, $from, EMAIL_FROM_ADDR, '', $bcc);
			}
		}

		// pour les v�hicules Premier Prix on cr�e automatiquement un STOP SELL du lundu au vendredi
		if ($agence->hasStatut(Agence::STATUT_STOP_PREMIER_PRIX) && strtolower($categorie['type'])!='ml' &&  in_array($categorie['mnem'], array('0','Z')))
		{
			// on doit se ramener au lundi
			$tDebut = $reservation->getDebut('U');
			$offset = 1 - $reservation->getDebut('N');
			if ($offset < 0)
				$tDebut = strtotime("$offset days", $tDebut);
			StopSell::create($agence, $categorie, date('Y-m-d', $tDebut), date('Y-m-d', strtotime('+5 days', $tDebut)));
		}

		// on indique que le paiement s'est bien pass�
		return true;
	}

	/**
	* Annule une r�servation
	* Le montant est d�termin� par les conditions d'annulation
	*
	* @param Reservation $reservation
	* @returns bool
	*/
	public function doCancel(Reservation $reservation, ResAnnulationMotif $motif = null)
	{
		// on cr�e un motif par d�faut
		if (!$motif) $motif = ResAnnulationMotif::factory();

		/** @var Client */
		$client = $reservation->getClient();

		// 0. v�rification avant annulation
		// =======================================
		// la reservation concerne l'utilisateur ou il s'agit d'un administrateur
		if($client['email'] != $_COOKIE['ADA_CUSTOMER_ID'] && !$motif->isAdmin())
		{
			$this->_addMessage("Appel incorrect : la r�servation n'est pas au nom de l'utilisateur");
			return false;
		}
		// compatibilit� du mode de remboursement : ch�que ou le mode de paiement
		if ($this->_mode != $reservation['paiement_mode'] && $this->_mode!='CQ')
		{
			$this->_addMessage("Appel incorrect : le mode de remboursement n'est pas coh�rent avec le mode de paiement.");
			return false;
		}
		// statut ne permettant pas l'annulation
		if($reservation['statut'] != 'P')
		{
			$this->_addMessage("Le statut de la r�servation ne permet pas l'annulation");
			return false;
		}

		// on racup�re la liste des avoirs possibles
		$montant = array();
		$avoirs = $reservation->getFacturations()->getAvailableAvoirs();
		foreach($avoirs as /** @var Facturation */ $f)
		{
			if ($f['erreur'])
			{
				$this->_addMessage("Une erreur s'est produite lors de la v�rification du retrait du mat�riel");
				// dans le cas de l'admin on continue l'annulation...
				// ... sinon on arr�te l�
				if (!$motif->isAdmin())
					return false;
			}
			$montant[$f['facturation']] = $f['montant'];
		}

		// d�terminer le cas d'annulation
		// 1. si l'annulation est possible (d�lai avant r�servation et paybox)
		// 2. les frais d'annulation
		// 3. si l'annulation est possible
		// - on annule dans la base
		// - Paybox : si le remboursement est total on essaie d'annuler, sinon ou si l'annulation �choue on rembourse
		// - Paypal : on rembourse
		// - Locapass : on annulle

		// on v�rifie si la r�servation est annulable sauf si cela est fait depuis l'admin
		if (!$motif->isAdmin())
		{
			if (!$reservation->isCancellable($msg))
			{
				$this->_addMessage($msg);
				return false;
			}
			// la r�servation peut �tre remboursable o pas...
			if (!$reservation->isRefundable()) {
				// on remet � z�ro tous les remboursements possibles
				foreach($montant as $k => $v) {
					$montant[$k] = 0;
				}
			}
			$frais_dossier = min($reservation->getCancellationFees(), $montant['ada']);
			$montant['ada'] = max(round($montant['ada'] - $frais_dossier,2), 0);
		}
		else
		{
			// on v�rifie que l'annulation par Paybox est possible
			if ($this->_mode=='PB' && !$reservation->isCancellableByPaybox())
			{
				$this->_addMessage("Le d�lai de remboursement par par Paybox est expir�. Vous devez rembourser par ch�que.");
				return false;
			}
			// puis que le montant est correct
			if (array_sum($motif->getAdminMontant()) > array_sum($montant))
			{
				$this->_addMessage("Le montant rembours� ne peut pas �tre sup�rieur au montant pay�.");
				return false;
			}
			$montant = $motif->getAdminMontant();
		}

		// 1. effectuer l'annulation
		// =========================
		// on veut �viter que 2 pages s'ex�cutent en parall�lle
		sqlexec("UPDATE reservation SET statut='X' WHERE id=".$reservation['id']." AND statut='P'");
		if (1 != rowsaffected())
		{
			$this->_addMessage("La r�servation est d�j� en cours d'annulation! Veuillez consulter votre compte.");
			return false;
		}
		// on effectue le remboursement
		$total = $reservation->getTotal();
		$rembourse = array_sum($montant);
		if (!$this->_doRefund($reservation, $rembourse))
		{
			$reservation->setData('statut','P')->save('statut');
			return false;
		}
		// enregistrer les modifications dans la r�ervation
		$reservation->setData('rembourse', $reservation['rembourse'] + $rembourse)
					->setData('remboursement', date('Y-m-d H:i:s'))
					->setData('remboursement_mode', ($reservation['remboursement_mode'] ? $reservation['remboursement_mode'].',' : '').$this->_mode)
					->setData('fraisdossier', $frais_dossier)
					->setData('annulation_motif', ($reservation['annulation_motif'] ? $reservation['annulation_motif'].',' : '').$motif['id']);
		if ($motif->isPartiel())
			$reservation->setData('statut','P');
		else
		{
			$reservation->setData('statut','A')
						->setData('annulation',date('Y-m-d H:i:s'));
		}
		$reservation->save('statut,annulation,rembourse,remboursement,remboursement_mode,fraisdossier,annulation_motif');

		// cr�er les avoirs
		foreach($avoirs as /** @var Facturation */ $f)
		{
			if ($montant[$f['facturation']] > 0)
			{
				$f['montant'] = $montant[$f['facturation']];
				$f['mode'] = $this->_mode;
				$f->save();
			}
		}
		// et enregistrer dans la comptabilit�
		ComptaClientys::doCancel($reservation, $montant['ada'] + $montant['materiel']);

		// si �a a r�ussi on pr�pare les textes d'explications pour les notifications
		if ($rembourse == 0) {
			$txt_client = "Conform�ment � nos conditions de r�servation, vous n'�tes pas rembours� de votre r�servation.";
			$txt_franchise = "L'annulation a �t� faite sans remboursement. Vous recevrez le montant de la r�servation.";
		} else if ($rembourse < $total) {
			// pour les frais de dossier
			if ($frais_dossier > 0) {
				$txt_client = "Conform�ment � nos conditions de r�servation, $frais_dossier � TTC de frais de dossier seront retenus sur le montant total de votre r�servation."; // Car votre annulation intervient moins de 7 jours avant la date pr�vue de votre d�part.";
				$txt_franchise = "$frais_dossier � TTC de frais de dossier ont �t� retenus au client"; // car l'annulation a �t� faite moins de 7 jours avant le d�part.";
				$txt_franchise.= "\nVous conservez donc ces frais. Vous recevrez donc un virement de $total � TTC, puis un d�bit de $rembourse � TTC correspondant au trop per�u.";
			}
			$txt_client .= '<p>'.$this->_getModalitesRemboursementPartiel($rembourse);
		} else {
			if ($this->_mode == 'LP') {
				$txt_franchise = "La r�servation LOCAPASS a �t� annul�e.";
			} else {
				$txt_franchise = "Vous ne recevrez pas de virement correspondant � cette r�servation. Si le r�glement du client vous a d�j� �t� vir�, vous serez pr�lev� du m�me montant, soit ".$total."� ";
			}
			$txt_client = $this->_getModalitesRemboursementTotal();
		}

		// l'annulation s'est bien d�roul�e
		$this->_addMessage("Nous vous confirmons l'annulation de la r�servation ".$reservation->getResIDClient().".<br>L'annulation a �t� effectu�e.");

		// envoyer des e-mails pour pr�venir
		// info client
		$data = $this->_getDataEmail($reservation);
		$data['remboursement_type'] = ($motif->isPartiel() ? 'rembours�e partiellement' : 'annul�e');
		$data['remboursement'] = ($rembourse ? number_format($rembourse, 2) : '-');
		$data['txt_client'] = $txt_client;
		$data['txt_franchise'] = $txt_franchise;
		$data['excuses'] = $motif['explications'];
		// cr�er le code de r�duction si c'est possible et renseigner l'e-mail
		$code = $motif->getPromotionCode($reservation);
		$data['reduction_offerte'] = ($code ? $code->toHtml('email-reduction-offerte') : '');

		$client = $reservation->getClient();
		$agence = $reservation->getAgence();
		$type = strtolower($reservation['type']);

		// email  client
		if ($motif->mustSendEmail() && $txt_client)
		{
			// composer le message
			$html = 'em3';
			if ($this->_mode == 'LP')
				$html .= '-locapass';
			else if ($reservation->getResOptions()->hasOptions("la$type,lr$type"))
				$html .= '-ld';
			else if (in_array($reservation['forfait_type'], array('JEUNES','LOCAPASS')))
				$html .= '-'.strtolower($reservation['forfait_type']); // em1-jeunes.html
			$html = "/emails/$html.html";
			$html = load_and_parse(BP.$html, $data);
			// le sujet
			$subject = ($reservation['canal']=='WEB') ? SUBJECT_RESA_CLIENT_ANNULATION : SUBJECT_RESA_CLIENT_ANNULATION_CANAL_TC;
			if ($motif->isPartiel())
				$subject = str_replace('Annulation ','Remboursement partiel ' , $subject);
			// envoyer le mail
			send_mail
			(
				$html, $client['email'], $subject,
				($reservation['canal']=='WEB') ? EMAIL_FROM : EMAIL_FROM_CANAL_TC,
				EMAIL_FROM_ADDR, '',
				($reservation['canal']=='WEB') ? BCC_EMAILS_TO : BCC_EMAILS_TO_CANAL_TC
			);
			$this->_addMessage("Un e-mail de confirmation ".($motif->isPartiel() ? "de remboursement partiel" : "d'annulation")." vient de vous �tre envoy�.");
		}

		// email franchis�
		if ($reservation['statut']=='A' && $motif->mustSendEmail() && $txt_franchise)
		{
			$html = 'em4';
			if ($this->_mode == 'LP')
				$html .= '-locapass';
			$html = load_and_parse(BP.'/emails/'.$html.'.html', $data);
			$html = html_entity_decode(str_replace(array('<br>','<BR>','<br/>','<BR/>'), "\n", strip_tags($html,'<br>')));
			$to =  (defined('EMAIL_AGENCE_DEBUG') ? EMAIL_AGENCE_DEBUG : $agence['email']);
			if ($frais_dossier > 0)
				$subject = ($reservation['canal']=='WEB') ? SUBJECT_RESA_AGENCE_ANNULATION_FRAIS : SUBJECT_RESA_AGENCE_ANNULATION_FRAIS_CANAL_TC;
			else
				$subject = ($reservation['canal']=='WEB') ? SUBJECT_RESA_AGENCE_ANNULATION : SUBJECT_RESA_AGENCE_ANNULATION_CANAL_TC;
			$from = ($reservation['canal']=='WEB') ? EMAIL_FROM : EMAIL_FROM_CANAL_TC;
			$bcc = ($reservation['canal']=='WEB') ? BCC_EMAILS_TO : BCC_EMAILS_TO_CANAL_TC;
			// on envoie au si�ge ou � l'agence si elle ne suupporte pas unipro
			send_mail ($html, $agence['notification']=='email' ? $to : $bcc, $subject, $from, EMAIL_FROM_ADDR, '', $bcc);
		}

		// puis on pr�vient UNIPRO / CITER si l'agence le supporte
		if (($reservation['statut']=='A' || $montant['materiel']) && $agence['notification']=='rpc')
		{
			// on choisit le bon m�canisme et en cas d'�chec on envoie un e-mail � l'agence
			set_time_limit(60);
			do
			{
				if ($agence['reseau']=='ADA')
				{
					try { $unipro = @new Unipro(); } catch (Exception $e) { $e = (string) $e; }
					if ($unipro)
					{
						if ($reservation['statut']=='A' && $unipro->cancelReservation($reservation))
							break;
						else if ($montant['materiel'] && $unipro->updateOptions($reservation))
							break;
					}
					$subject = '[UNIPRO-ERREUR] '.$subject;
				}
				else if ($agence['reseau'] == 'CITER')
				{
					$subject = '[CITER-ERREUR] '.$subject;
					$to = $bcc;
				}
				if ($motif->mustSendEmail() && $txt_franchise)
					send_mail ($html, $to, $subject, $from, EMAIL_FROM_ADDR, '', $bcc);
			} while (false);
		}
		return true;
	}

	/**
	* Renvoie le HTML associ�e � une surcharge
	*
	* @param Reservation $reservation
	* @returns string
	*/
	protected function _getHtmlSurcharge(Reservation $reservation)
	{
		if ((int)$reservation['surcharge'] > 0)
		{
			$html = '<tr>';
			$html.='<td width="180" height="22" style="font:bold 12px arial;"><img src="img/puce1.gif" alt=""><strong>'.$reservation->getAgence()->getSurchargeLabel().'</strong></td>';
			$html.='<td width="144" align="right" background="img/bg_case.gif" style="font:bold 12px arial;">'.$reservation['surcharge'].' &euro; TTC</td>';
			$html.='</tr>';
			return $html;
		}
	}
	/**
	* Renvoie les donn�es pour les e-mails de r�servation
	* @param Reservation $reservation
	* @returns array
	*/
	protected function _getDataEmail(Reservation $reservation)
	{
		global $AGE, $PERMIS;

		$agence = $reservation->getAgence();
		$client = $reservation->getClient();
		$conducteur = $reservation->getConducteur();
		$categorie = $reservation->getCategorie();
		$options = $reservation->getResOptions();
		$code = $reservation->getPromotionCode();
		$resInfo = $reservation->getResInfo();

		// v�hicule
		$vehicule = $categorie['nom'];
		if ($categorie['commentaire'])
			$vehicule .= ': <br>'.$categorie['commentaire'];

		// remplir le tableau
		$data = array(
			 'date_jour'=>date('d/m/Y � H:i')
			,'civilite'	=> $CIVILITE[$conducteur['titre']]
			,'nom'		=> $conducteur['prenom'].' '.$conducteur['nom']
			,'adresse'	=> $client['adresse'].' '.$client['cp'].' '.$client['ville'].' '.$client['pays_nom']
			,'age'		=> $AGE[$reservation['age']]
			,'conducteur_tel'=> $conducteur['tel']
			,'conducteur_gsm'=> $conducteur['gsm']
			,'annee_permis' => $PERMIS[$reservation['permis']]
			,'resid'	=> $reservation->getResIDClient()
			,'debut'	=> $reservation->getDebut()
			,'fin'		=> $reservation->getFinAvecVerificationHoraires()
			,'jours'	=> $reservation->getDuree()
			,'km'		=> $reservation->getKm()
			,'vehicule_type' => strtolower($reservation['type'])
			,'vehicule' => $vehicule
			,'agence'	=> $agence->getHtmlAddress(' ', 'nom,adresse1,cp_ville,tel')
			,'agence_email' => $agence['email']
			,'agence_bal'=> $agence->getBal('strong', null, '<br>')
			,'agence_url'=> $agence->getURLAgence()
			,'commentaire_agence'=> $agence['commentaire']
			,'commentaire_agence_localisation'=> $agence['commentaire_localisation']
			,'commentaire_agence_offre'=> $agence['commentaire_offre']
			,'commentaire_client'=> $resInfo['commentaire'] ? '<strong>Le client a laiss&eacute; un commentaire :</strong><br>'.$resInfo['commentaire'].'<br><br>' : ''
			,'commentaire_pour_client'=> $resInfo['commentaire_client'] ? '<strong>Commentaire de l\'agence :</strong><br>'.$resInfo['commentaire_client'].'<br><br>' : ''
			,'tarif'	=> number_format($reservation->getPrixForfait(),2)
			,'tarif_ht'	=> $reservation->getPrixForfaitHT()
			,'options'	=> number_format($reservation->getPrixOptions(),2)
			,'options_ht'=>$reservation->getPrixOptionsHT()
			,'surcharge'=> $this->_getHtmlSurcharge($reservation)
			,'reduction'=> $code ? $code->toHtml('email-reduction') : ''
			,'total'	=> $reservation->getPrixTotal()
			,'total_ht'	=> $reservation->getPrixTotalHT()
			,'paiement'	=> $reservation->getDate('paiement', 'd/m/Y � H:i')
			,'paiement_mode'=> self::$PAIEMENT_LABEL[$reservation['paiement_mode']]
			,'list_options' => $options->toHtmlEmail()
			,'login'	=> $client['email']
			,'msg_partenaire' => $agence->getHtmlMsgPartner('<br><span style="color:#CC0000">', '</span>')
			,'href_cgl'	=> $agence->getHrefCGL()
		);
		if ($agence->isPointLoc())
		{
		    $data['vehicule_type']= "pointloc";
		}
		return $data;
	}
}
?>
