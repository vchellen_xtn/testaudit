<?php

class PromotionForfait_Collection extends Ada_Collection
{
	/**
	* Renvoie le nombre de promotions pour une zone et une date donn�e
	* 
	* @param string $zone
	* @param string $date
	* @returns int
	*/
	static public function countByZone($zone)
	{
		// compter le nombre de promotions actives dans cette zone pour cette date
		$sql ="SELECT COUNT(pf.id)\n";
		$sql.="FROM agence_promo_forfait pf\n";
		$sql.="LEFT JOIN agence_config ac ON ac.agence=pf.code_societe\n";
		$sql.='WHERE '.self::createSQLCondition(array('zone'=>$zone,'agence'=>'%'));
		return getsql($sql);
	}
	
	/**
	* Renvoie une liste de d�partements avec promotions
	* @returns Dept_Collection
	*/
	static public function getDepartements()
	{
		$x = new Dept_Collection();
		$sql = "SELECT d.*\n";
		$sql.="FROM dept d\n";
		$sql.="JOIN agence_alias aa ON (aa.dept=d.id AND aa.zone=d.zone)\n";
		$sql.="JOIN agence_promo_forfait pf ON aa.pdv=pf.agence\n";
		$sql.="LEFT JOIN agence_config ac ON ac.agence=pf.code_societe\n";
		$sql.="WHERE aa.zone='fr' AND (aa.statut & ".Agence::STATUT_VISIBLE.")=".Agence::STATUT_VISIBLE."\n";
		$sql.="  AND ".self::createSQLCondition(array('agence'=>'%'));
		$sql.=" ORDER BY d.nom";
		return $x->_loadFromSQL($sql);
	}
	
	/**
	* Construit la requ�te SQL � partir d'un tableau d'arguments
	* 
	* @param array $args
	* @returns string
	*/
	static public function createSQLCondition($args)
	{
		// r�cup�rer les arguments en les v�rifiant
		if ($args['zone'] && Zone::isValidID($args['zone']))
			$zone = $args['zone'];
		else
			$zone = 'fr';
		if ($args['date'] && preg_match('/^\d{4}-\d{2}-\d{2}$/', $args['date']))
			$date = $args['date'];
		else
			$date = date('Y-m-d');
		if ($args['classe'])
			$classe = (int) $args['classe'];
		if ($args['debut'] && preg_match('/^\d{4}-\d{2}-\d{2}$/', $args['debut']))
			$debut = $args['debut'];
		if ($args['type'] && Categorie::$TYPE[strtolower($args['type'])])
			$type = $args['type'];
		if ($zone=='fr' && $args['dept'] && Dept::isValidID($args['dept']))
			$dept = $args['dept'];
		if (!$dept && $args['agence'])
		{
			$agences = array();
			if (is_array($args['agence']))
			{
				foreach($args['agence'] as $a)
				{
					if (Agence::isValidID($a))
						$agences[] = $a;
				}
			}
			else if (Agence::isValidID($args['agence']))
				$agences[] = $args['agence'];
		}

		// construire la condition SQL
		$cond = "pf.zone='$zone' and pf.actif=1";
		if (!$dept && !is_array($agences))
			$cond.=' and pf.agence is null';
		else
		{
			if ($dept)
				$cond.= " and pf.agence IN (SELECT DISTINCT aa.pdv FROM agence_alias aa WHERE aa.dept='$dept')\n";
			else if (count($agences) > 0)
				$cond.=" and pf.agence IN ('".join("','", $agences)."')\n";
		}
		if ($classe) {
			$cond.= " and (pf.classe is null or pf.classe=$classe)\n";
		} else {
			$cond.=" and pf.classe is null\n";
		}
		if ($type) {
			$cond.=" and (pf.type is null or pf.type='$type')\n";
		}
		$cond .= " and (pf.jusqua is null or pf.jusqua >= '$date')\n";
		$cond .= " and (pf.fin is null or  pf.fin >= '$date')\n";
		if ($debut)
		{
			$cond .= " and (pf.depuis is null or pf.depuis <= '$date')\n";
			$cond .= " and (pf.debut is null or pf.debut <= '$debut')\n";
			$cond .= " and (pf.fin is null or  pf.fin >= '$debut')\n";
		}
		// on ne garde que les promotions n�gatives
		$cond .= " and pf.montant < 0\n";
		// si des promotions locales sont s�lectionn�es, le p�rim�tre de la promotion doit correspondre � celui de l'agence
		$cond.="  and (\n";
		$cond.="    (ac.tarifs_vp is null and pf.perimetre='ADAFR')\n";
		$cond.="    or\n";
		$cond.="    (pf.type is null and pf.perimetre IN (ac.tarifs_vp, ac.tarifs_vu))\n";
		$cond.="    or\n";
		$cond.="    (pf.type='ml')\n";
		$cond.="    or\n";
		$cond.="    (pf.type='vp' and pf.perimetre=ac.tarifs_vp)\n";
		$cond.="    or\n";
		$cond.="    (pf.type='vu' and pf.perimetre=ac.tarifs_vu)\n";
		$cond.="    or\n";
		$cond.="    (pf.type='sp' and pf.perimetre=ac.tarifs_sp)\n";
		$cond.="  )\n";
		return $cond;
	}

	/**
	* Cr�e une liste de promotions
	* 
	* @param array $args
	* 	zone	zone de recherche des promotions
	* 	agence	identifiant de l'agence
	* 	search	recherche selon Agence_Collection
	* 	lat,lon idem
	* @return PromotionForfait_Collection
	*/
	static public function factory($args = array(), $short_title = false, $published_only = false)
	{
		$x = new PromotionForfait_Collection();
		// v�rification des arguements
		if ($args['search'] && !$args['zone'] && !$args['agence'] && !$args['dept'])
		{
			$a = Agence_Collection::factory($args, Agence_Collection::MODE_AGENCES_LISTE, false);
			if ($a->isEmpty())
				return $x;
			$args['agence'] = $a->getIds();
		}
		$sql = "SELECT DISTINCT pf.*";
		if ($short_title)
			$sql.=", COALESCE(pf.slogan_iphone, slogan) slogan";
		$sql.=", aa.canon\n";
		$sql.="FROM agence_promo_forfait pf\n";
		$sql.="LEFT JOIN agence_config ac ON ac.agence=pf.code_societe\n";
		$sql.="LEFT JOIN agence_alias aa ON aa.pdv=pf.agence\n";
		$sql.="WHERE ".self::createSQLCondition($args)."\n";
		if ($published_only) {
			$sql.=" AND pf.publication IS NOT NULL\n";
		}
		$sql.="ORDER BY slogan";
		if ($args['agence'])
			$sql = str_replace('pf.agence IN', 'aa.id IN', $sql);
//		die("<pre>$sql</pre>");
		$x->_loadFromSQL($sql);
		return $x;
	}
	
	/**
	* Appel� � la fin de _loadFromSQL()
	* 
	*/
	protected function _afterLoad()
	{
		foreach ($this->_items as /** @var PromotionForfait */ $item)
		{
			$item['url_promo'] = $item->getURLPromo();
			$item['benefice'] = '-'.str_replace('.00', '', $item['montant']).($item['mode']=='P' ? '%' : ' �');
		}
	}
}
?>
