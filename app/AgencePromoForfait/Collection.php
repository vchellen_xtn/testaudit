<?php

class AgencePromoForfait_Collection extends Ada_Collection 
{
	/**
	* Renvoit une liste de promos � partir d'un type promo (simple/early)
	* @returns AgencePromoForfait_Collection
	*/
	static public function factory(Reservation $reservation)
	{
		$agence = $reservation->getAgence();
		$age = $reservation['age'];
		
		// pr�parer le SQL
		$sql ="SELECT p.*\n";
		$sql.="FROM agence_promo_forfait p\n";
		$sql.="WHERE p.code_societe='".$agence['code_societe']."'\n";
		$sql.="  AND (p.agence is null or p.agence='".addslashes($reservation['pdv'])."')\n";
		$sql.="  AND p.classe IS NULL\n";
		$sql.="  AND (p.type is null or p.type='".$reservation['type']."')\n"; 
		$sql.="  AND (p.jours_min is null or ".(int)$reservation['duree']." >= p.jours_min)\n";
		$sql.="  AND (p.jours_max is null or ".(int)$reservation['duree']." <= p.jours_max)\n";
		$sql.="  AND (p.km_min is null or ".(int)$reservation['distance']." >= p.km_min)\n";
		$sql.="  AND (p.km_max is null or ".(int)$reservation['distance']." <= p.km_max)\n";
		$sql.="  AND (p.debut is null or p.debut <= '".$reservation->getDebut('Y-m-d')."')\n";
		$sql.="  AND (p.fin is null or p.fin >= '".$reservation->getFin('Y-m-d')."')\n";
		$sql.= " AND (p.jour_mois_debut is null or p.jour_mois_debut <= ".$reservation->getDebut('j').")\n";
		$sql.= " AND (p.jour_mois_fin is null or p.jour_mois_fin >= ".$reservation->getDebut('j').")\n";
		$sql.="  AND (p.actif=1)\n";
		$sql.="  AND (p.depuis is null or p.depuis <= CURRENT_DATE)\n";
		$sql.="  AND (p.jusqua is null or p.jusqua >= CURRENT_DATE)\n";
		$sql.="  AND (p.jours_avant is null or p.jours_avant <= ".ceil( $reservation->getHoursBeforeStart() / 24).")\n";
		$sql.="  AND ($age BETWEEN IFNULL(p.age_min, 18) AND IFNULL(p.age_max, 120))\n";
	
		// renvoyer la collection
		$x = new AgencePromoForfait_Collection();
		$x->loadFromSQL($sql);
		return $x;
	}
	
	/**
	* Renvoie la meilleure promotion pour ce type de forfait et cette cat�gorie
	* 
	* @param string $forfait	code forfait � comparer avec la promotion
	* @param string $mnem		code mn�monique de la cat�gorie
	* @param float $ttc			montant de base avant application
	* @returns AgencePromoForfait
	*/
	function findBestPromo($forfait, $mnem, $ttc)
	{
		$bestPromo = null; $bestPrice = $ttc;
		// on parcourt les promotions
		foreach($this->_items as /** @var AgencePromoForfait */ $promo) {
			if ($promo->isValidForfait($forfait) && $promo->isValidMnem($mnem)) {
				$prix = $promo->getPrixPromo($ttc);
				if ($prix < $bestPrice) {
					$bestPrice = $prix;
					$bestPromo = $promo;
				}
			}
		}
		return $bestPromo;
	}
	
}

