<?
require_once("secure.php");
require_once("lib.php");

if (getarg("update")){
	$msg = array();
	foreach ($_POST['categorie'] as $categorie) {
		$str = array();
		if (!preg_match('/^[0-9]+$/',$categorie['franchise']))
			$str[] = "Franchise";
		if (!preg_match('/^[0-9]+$/',$categorie['depot']))
			$str[] = "Depot";
		if (!preg_match('/^[0-9]+$/',$categorie['franchise_rf']))
			$str[] = "Franchise rf";
		if (!preg_match('/^[0-9]+$/',$categorie['depot_rf']))
			$str[] = "Depot rf";
		if (!preg_match('/^[0-9]+$/',$categorie['franchise_af']))
			$str[] = "Franchise af";
		if (!preg_match('/^[0-9]+$/',$categorie['depot_af']))
			$str[] = "Depot af";
		if (!preg_match('/^[0-9]+$/',$categorie['frais_af']))
			$str[] = "Frais af";
		if (!preg_match('/^[0-9]+\.?[0-9]{0,2}$/',$categorie['frais_immo']))
			$str[] = "Frais immo";
			
		if(count($str))
			$msg[] = $categorie['type']." ".$categorie['nom']. " : ". join(', ', $str);
		else 
			save_record('categorie', $categorie);
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script language="javascript" src="../scripts/check.js"></script>
	<script language="javascript" src="../scripts/jquery-1.3.2.min.js"></script>
	<script>
	<!-- //
	// doSubmit
	function doSubmit(frm)
	{
		var err = 0;
		$("input.int").each(function(i){
			$(this).removeClass('rouge');
			if (!($(this).val().match(/^[0-9]+$/))){
				err = 1;
				$(this).addClass('rouge');
			}
		});
		$("input.number").each(function(i){
			$(this).removeClass('rouge');
			if (!($(this).val().match(/^[0-9]+\.?[0-9]{0,2}$/))){
				err = 1;
				$(this).addClass('rouge');
			}
		});
		if (err)
			return false;
		return true;
	}
	// -->
	</script>
	<style>
		input.rouge {border:1px solid red;}
		input.int, input.number {text-align: right; }
	</style>
</head>
<body>
<?
if (count($msg))
echo "<strong>Erreurs lors de l'enregistrement<br/>".join('<br/>', $msg).'</strong>';
?>
<form action="" method="post" onsubmit="return doSubmit(this);" id="form_categorie">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
	<tr>
		<th class="bg_titre">
			<img src="img/puce.gif" align="absmiddle" hspace="1">
			Cat�gorie Franchise
		</th>
	</tr>
	<tr>
		<td>
			<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
			<tr>
				<th colspan="3"></th>
				<th colspan="2">R�duction</th>
				<th></th>
				<th colspan="3">Rachat</th>
				<th></th>
			</tr>
			<tr>
				<th>cat�gorie</th>
				<th>franchise</th>
				<th>d�p�t</th>
				<th>franchise rf</th>
				<th>d�p�t</th>
				<th></th>
				<th>franchise</th>
				<th>d�p�t</th>
				<th>frais</th>
				<th>frais immo</th>
			</tr>
			<? foreach(array('vp','vu') as $type) : ?>
			<tr>
				<th colspan="9"><?=strtoupper($type)?></th>
			</tr>
			<? foreach(Categorie_Collection::factory('fr', $type) as $c) : ?>
				<? $name = 'categorie['.$c['id'].']'; ?>
			<tr>
				<td>
					<input type="hidden" name="<?=$name?>[id]" value="<?=$c['id']; ?>" />
					<?=$c['mnem'].' - '.$c['nom'];?>
				</td>
				<? foreach(array('franchise','depot','franchise_rf','depot_rf','franchise_af','depot_af','frais_af') as $k) :?>
					<? if ($k == 'franchise_af') : ?>
					<td>&nbsp;</td>
					<? endif; ?>
					<td><input type="text" name="<?=$name?>[<?=$k?>]" value=<?=$c[$k];?> maxlength="4" size="4" class="int"/></td>
				<? endforeach; ?>
				<td><input type="text" name="categorie[<?=$c['id']; ?>][frais_immo]" value="<?= $c['frais_immo']; ?>" maxlength="6" size="6" class="number" /></td>
			</tr>
			<? endforeach; ?>
			<? endforeach; ?>
		</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="100%">
			<tr>
				<td align="right">
					<input type="submit" name="update" value="enregistrer" onclick="return doSubmit(this.form);">
				</td>
			</tr>
			</table>
		</td>
	</tr>
</table>
</form>
</body>