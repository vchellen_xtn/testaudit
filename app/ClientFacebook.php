<?php
class ClientFacebook extends Ada_Object
{
	/** @var Client */
	protected $_client;
	
	public function _construct()
	{
		parent::_construct();
		$this->setIdFieldName('client');
	}
	
	/**
	* Renvoie un ClientFacebook
	* 
	* @param mixed $client	id ou Client
	* @returns ClientFacebook
	*/
	static public function factory($client)
	{
		$x = new ClientFacebook();
		if (is_numeric($client))
			$clientID = (int)$client;
		else if (is_object($client) && get_class($client)=='Client')
		{
			$clientID = (int)$client->getId();
			$x->_client = $client;
		}
		return $x->load((int)$clientID);
	}
	
	/**
	* Renvoyer un ClientFacebook associ�e � une signed_request
	* 
	* @param string $signed_request
	* @return ClientFacebook
	*/
	static public function createFromSignedRequest($signed_request)
	{
		$x = new ClientFacebook();
		if ($signed_request)
		{
			// on d�code la Signed Request de Facebook
			$auth = FacebookSignedRequest::parseSignedRequest($signed_request, FB_APP_SECRET);
			if ($auth['user_id'])
			{
				// cet utilisateur est-il d�j� connu ?
				$x->setIdFieldName('facebook_id')->load($auth['user_id'])->setIdFieldName('client');
				if ($x['modification'] > date('Y-m-d')) 
					return $x;
				
				// on r�cup�re les informations de Facebook
				$facebook = new Facebook(array('appId' => FB_APP_ID, 'secret' => FB_APP_SECRET));
				if ($userID = $facebook->getUser())
				{
					try
					{
						// donn�es facebook
						$user_profile = $facebook->api('/me');
						// on essaye de trouver un client s'il n'y en a pas
						if (!$x['client'] && $user_profile['email'])
						{
							$x['client'] = getsql(sprintf("select id from client where email='%s'", filter_var($user_profile['email'], FILTER_VALIDATE_EMAIL)));
						}
						// on met � jour les informations dans ClientFacebook
						foreach(explode(',', 'id,email,birthday,name,first_name,last_name,username,gender,locale') as $k)
						{
							if ($v = $user_profile[$k])
							{
								if ($k=='birthday')
									$v = preg_replace('/^(\d{2})\/(\d{2})\/(\d{4})$/', '\3-\1-\2', $v);
								$x->setData('facebook_'.$k, $v);
							}
						}
						// s'il y a un client on enregistre
						if ($x['client'])
						{
							$x->save();
						}
					}
					catch (FacebookApiException $e)
					{
						$x->setData('error_msg', (string) $e);
					}
				}
			}
		}
		return $x;
	}
	
	/**
	* Renvoie le Client
	* @returns Client
	*/
	public function getClient()
	{
		if (!$this->_client && $this->_data['client'])
			$this->_client = Client::factory($this->_data['client']);
		return $this->_client;
	}
}
?>
