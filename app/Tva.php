<?php

define('TVA_2014', strtotime('2014-01-01'));

class Tva {
	/**
	* Renvoie le taux de TVA en fonction du taux
	* 
	* @param int $t
	* @return double
	*/
	static public function getRate($t = null)
	{
		if (!$t) $t = time();
		if ($t < TVA_2014)
			return 0.196;
		return 0.2;
	}
	static public function TTC2HT($prix, $t = null)
	{ 
		return round($prix/(1+self::getRate($t)), 2);
	}
	static public function HT2TTC($prix, $t = null)
	{ 
		return round($prix*(1+self::getRate($t)), 2);
	}
	static public function format($rate)
	{
		$rate *= 100;
		$prec = (round($rate) == $rate) ? 0 : 2;
		return number_format($rate, $prec, ',', ' ').'%';
	}
}
?>
