<?php
/**
* $this est Reservation
* $forfait doit �tre pass� en arguemnt
*/
/** @var Reservation */
$reservation = $this;
/** @var Forfait */
$forfait = $forfait;
if (!$forfait) return;
?>
<!-- afficher les tarifs -->
<div id="offre_options"<? if ($forfait->isEmpty()) echo ' style="display: none;"'?>>
	<form action="../reservation.php" method="post" onsubmit="return doCheckReservationTarif(this);" id="frm_reservation_tarif">
		<input type="hidden" name="key" value="<?=Page::getKey('reservation')?>"/>
		<input type="hidden" name="forfait" value="<?=$forfait->doSerialize();?>"/>
		<?
			copy_form($reservation->getFields());
			$table_options_def = '<table border="0" cellpadding="0" cellspacing="0">'."\n";
			$table_options_def.= '<colgroup><col width="344" /><col width="30" /><col width="60" /><col width="50" /></colgroup>'."\n";
			// d�marrer la table des options
			echo $table_options_def;
			// afficher les frais d'adh�sion si n�cessaire
			if ($prix_coupon = $reservation->getPrixCoupon())
			{
?>
			<tr class="surcharge">
				<td><span><strong>Frais d'adh�sion compte pro</strong></span></td>
				<td>&nbsp;</td>
				<td class="option_prix checked">
					<?=show_money($prix_coupon)?> �
					<input type="hidden" name="prix_coupon" value="<?=$prix_coupon?>" />
				</td>
				<td>&nbsp;</td>
			</tr>
<?
			}
			// afficher la surcharge
			if (!$reservation->isLocapass())
				$surcharge = round($reservation->getPrixSurcharge());
			if($surcharge > 0)
			{
			?>
			<tr class="surcharge">
				<td><span><?=$reservation->getAgence()->getSurchargeLabel();?></span></td>
				<td>&nbsp;</td>
				<td class="option_prix checked">
					<?=$surcharge?> �
					<input type="hidden" name="surcharge" value="<?=$surcharge?>" />
				</td>
				<td>&nbsp;</td>
			</tr>
<?
			}
			
	// distance a prendre en compte pour les options
	$mode_paiement = array
	(
		'L' => 'Options payables en ligne',
		'A' => 'Options payables en agence<br /><span>sous r�serve de stocks disponibles</span>'
	);
	// extraire la liste des options
	$options = Option_Collection::factory($forfait, $reservation);
	$prix_options = $surcharge + $prix_coupon;
	$paiement = null; $i = 0;
	foreach ($options as /** @var Option */ $option)
	{
		if ($option['paiement'] != $paiement)
		{
			if ($paiement)
			{
				echo "</table>\n";
				echo $table_options_def;
			}
			$i = 0;
			$paiement = $option['paiement'];
?>
			<tr class="first">
				<td colspan="4" class="option_titre option_titre_<?=$paiement?>" style="padding-right:10px;"><?=$mode_paiement[$paiement]?></td>
			</tr>
<?
		}
		// Livraison � domicile : on ins�re un titre
		$optionID = $option['id'];
		if ($optionID == 'lavp' || $optionID == 'lavu')
		{
		?>
			<tr>
				<td class="option_livraison">Livraison � domicile</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
		<?
		}
		// affichage de l'option
		$checked = '';
		if (($option['init'] && !$_POST['options']) || (is_array($_POST['options']) && in_array($option['id'],$_POST['options'])))
		{
			$option['init'] = 1;
			$checked = ' checked';
			$prix_options += $option['prix'];
		}
		else
		{
			$option['init'] = 0;
		}
		
		?>
		<tr id="option_<?=$optionID?>">
			<td class="option_label">
				<label for="<?=$optionID?>"><?=$option['nom']?></label>
				<?
				if ($option['lien'])
					echo ' <a class="option_ensavoirplus" onclick="window.open(\''.$here.$option['url'].'\'); return false;" href="#">'.$option['lien'].'</a>';
				?>
			</td>
			<td>
			<?
			if (!$option['commentaire'])
				echo '&nbsp;';
			else
			{
				echo '<a title="'.htmlspecialchars(strip_tags($option['commentaire'])).'" style="cursor:pointer;"><img src="../css/img/options_details.gif" alt="" /></a>';
			}
			?>
			</td>
			<td class="option_prix<?=$checked?>">
				<?=$option->getPrix();?>
			</td>
			<td class="option_choice">
			<?
			if ($checked) $checked .='="checked"';
			echo '<input autocomplete="off" id="'.$optionID.'" value="'.$optionID.'"'.$checked;
			if (!$options->isInGroup($option['groupe']))
				echo ' type="checkbox" name="options[]" onclick="updateTotal(this, '.$option['prix'].', null);" />'."\n";
			else
			{
				// bouton radio
				echo ' type="radio" name="group_'.$option['groupe'].'" onclick="updateTotal(this, '.$option['prix'].',\''.$option['groupe'].'\');"/>'."\n";
				// gestion du groupe
				if (!$optGroup[$option['groupe']]['hidden'])
				{
					echo '<input type="hidden" name="options[]" id="'.$option['groupe'].'" autocomplete="off"';
					if ($option['init'])
						echo ' value="'.$option['id'].'"';
					echo '/>';
					$optGroup[$option['groupe']]['hidden'] = true;
				}
			}
			echo '<input type="hidden" name="px_opt_'.$optionID.'" value="'.$option['prix'].'"/>';
			echo '</td>';
			echo "</tr>\n";
			$i++;
		}
		?>
		</table>
		<?
		if (!$reservation->isLocapass()) 
		{
			$prixTotal = $forfait->getPrix() + $prix_options;
		?>
		<div class="offre_bottom">
			<div class="offre_total">
				<p>
					<span class="total">Total TTC</span>
					<span class="prix_total" id="prix_total"><?=show_money($prixTotal)?></span>
					<span class="euro">�</span>
					<span class="end"></span>
				</p>
				<span class="paiement_immediat">tarif pour un paiement imm�diat</span>
			</div>
			<div class="clear"></div>
			<? echo $reservation->show1Euro(-54, -315, $prixTotal); ?>
			<div class="clear"></div>
			<? } ?>
			<p class="valid">
				<input type="submit" value="Confirmez" />
			</p>
			<input type="hidden" autocomplete="off" name="prix_forfait" value="<?=(int)$forfait->getPrix();?>"/>
			<input type="hidden" autocomplete="off" name="prix_options" value="<?=$prix_options?>"/>
			<div class="clear"></div>
		</div>
	</form>
</div>
<script language="javascript" type="text/javascript">
	function doCheckReservationTarif (f)
	{
		var err = '';
		<? if ($reservation->getAgence()->hasStatut(Agence::STATUT_LIVRAISON_SEULEMENT)) : ?>
		// v�rification livraison � domicile
		// une des 2 cases doit �tre coch�e
		la = document.getElementById('la<?=strtolower($forfait['type']);?>');
		lr = document.getElementById('lr<?=strtolower($forfait['type']);?>');
		if (la && lr && !la.checked && !lr.checked)
			err += "Vous devez s�lectionner au moins la livraison ou la reprise du v�hicule  !";
		<? endif; ?>
		if (err.length)
		{
			alert (err);
			return false;
		}
		return true;
	}
	// updateTotal
	function updateTotal(box, montant, groupe)
	{
		var elt, frm = box.form;
		if (box.type!='hidden' && !box.checked)
			montant = -montant;
		else if (groupe)
		{
			// si un groupe on effectue le calcul pour l'option d�coch�e
			var eltGroupe = document.getElementById(groupe);
			if (eltGroupe)
			{
				if (eltGroupe.value)
				{
					elt = document.getElementById(eltGroupe.value);
					if (elt.id == box.id) return;
					if (elt.onclick)
						elt.onclick();
				}
				eltGroupe.value = box.id;
			}
		}
		// le prix total des options		
		var prix_options = (Math.round(parseFloat(frm.prix_options.value)*100)/100) + montant;
		frm.prix_options.value = prix_options;
		// ajuster le total en fonction des options choisies
		elt = document.getElementById('prix_total');
		if (elt) 
		{
			var s = (Math.round((parseFloat(frm.prix_forfait.value) + prix_options)*100)/100);
			elt.innerHTML = s.toString().replace('.', ',').replace(/\,(\d)$/,',$10');
		}
		// visualiser l'activation
		if (box.type!='hidden')
			elt = $('#option_' + box.id + " td.option_prix").addClass('checked');
	}
</script>
