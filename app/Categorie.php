<?

class Categorie extends Ada_Object
{
	static public $TYPE = array('vp'=>'voiture','vu'=>'utilitaire','sp'=>'sans permis','ml'=>"� l'heure");
	static public $TYPE_LBL = array('vp'=>'voiture de tourisme','vu'=>'v�hicule utilitaire','sp'=>'v�hicule sans permis','ml'=>"v�hicule � l'heure");
	static public $TYPE_OAV = array('vp'=>'VP','vu'=>'VU','sp'=>'VSP','ml'=>"� l'heure");
	
	protected $_imgPath = 'fichiers/categories/';
	protected $_zone = 'fr';
	protected $_alternatives = array();
	
	static public function getCatIDByDefaultFromType($type) {
		if (isset(self::$TYPE[$type])) {
			return getsql(sprintf('select id from categorie where zone="fr" and type="%s" and publie=1 and defaut=1 order by position limit 1', $type));
		}
		return null;
	}
	
	/**
	* Cr�e un objet � partir d'un identifiant
	* 
	* @param string $id
	* @param string $cols liste des colonnes � charger
	* @param bool $jeunes - faut-il charger le nom "jeune" ?
	* @return Categorie
	*/
	static public function factory($id, $cols = '*', $jeunes = false)
	{
		$x = new Categorie();
		if ($x->load((int)$id, $cols)->isEmpty())
			return null;
		if ($jeunes)
			$x->loadJeunes();
		return $x;
	}
	/**
	* Charge les infos sp�cifique aux JEUNES
	* @returns Categorie
	*/
	public function loadJeunes()
	{
		list($nom, $description, $visuel) = getsql('select nom, description, visuel from categorie_jeune where categorie='.$this->getId());
		if ($nom)
		{
			$this->setData('nom', $nom);
			$this->setData('categorie_nom', $nom);
			$this->setData('famille_nom', null);
		}
		if ($description)
			$this->setData('description', $description);
		if ($visuel)
			$this->_imgPath = 'css/jeunes/categories/';
		return $this;
	}
	
	/**
	* Charge les onn�es sp�cifiques � une zone si n�cessaire
	* 
	* @param string $zone
	* @returns Categorie
	*/
	public function loadZone($zone)
	{
		if ($zone != $this->_zone)
		{
			$this->_zone = $zone;
			if ($row = mysql_fetch_assoc(sqlexec(sprintf("select age, permis, km_sup from categorie_zone where categorie=%ld and zone='%s'", $this->getId(), $this->_zone))))
			{
				foreach($row as $k => $v)
					$this->_data[$k] = $v;
			}
		}
		return $this;
	}
	
	protected function _prepareSQL($id, $cols='*', $data=null)
	{
		if (!is_numeric($id))
			$id = "'".addslashes($id)."'";
		$cols = 'c.'.join(', c.', explode(',', $cols));
		$sql = "select $cols , concat(ifnull(concat(nullif(f.nom,c.nom),' '),''), c.nom) nom, f.nom famille_nom, c.nom categorie_nom";
		$sql.=" from categorie c left join vehicule_famille f on f.id=c.famille";
		$sql.=" where c.id=$id";
		return $sql;
	}
	public function __toString()
	{
		return $this->getData('mnem').' '.$this->getData('nom');
	}
	public function getType() { return strtolower($this->_data['type']); }
	public function getTrackName() { return sprintf('%s_%s', $this->getType(), str_replace('+', "'", $this->_data['mnem'])); }
	/**
	* renvoie les sp�cifications d'une cat�gories
	* @returns string
	*/
	public function getSpecifications($elt = 'span', $with_label = true)
	{
		// liste des formats
		$version = ($with_label ? 'label' : 'raw');
		$format = array(
			'places' => array('label' => '%s places', 'raw' => '%s'),
			'portes' => array('label' => '%s portes', 'raw' => '%s'),
			'carburant' => array('label' => '%s', 'raw' => '%s'),
			'valises' => array('label' => '%s bagages', 'raw' => '%s'),
			'sacs' => array('label' => '%s sacs', 'raw' => '%s'),
			'charge' => array('label' => '%ld kg', 'raw' => '%ld'),
			'dimensions' => array('label' => 'Int. utiles : L %sm x l %sm x H %sm', 'raw' => 'Dim. int.: L %sm x l %sm x H %sm'),
		);
		// pr�parer l'affichage
		$a = array();
		$type = $this->getType();
		if($type == 'vp' || $type == 'sp' || $type=='ml')
		{
			foreach(array('places', 'portes','valises','sacs') as $k) {
				if ($this->_data[$k]) {
					$a[$k] = sprintf($format[$k][$version], $this->_data[$k]);
					// enlever le plurier si n�cessaire
					if ($this->_data[$k] < 2) {
						$a[$k] = rtrim($a[$k], 's');
					}
				}
			}
			if ($this->_data['carburant']) $a['carburant '.str_replace(' ', '_', strtolower($this->_data['carburant']))] = sprintf($format['carburant'][$version], $this->_data['carburant']);
		}
		else // if($type == "vu")
		{
			$format['places']['label'] = '%s personnes';
			foreach(array('places', 'charge') as $k) {
				if ($this->_data[$k]) {
					$a[$k] = sprintf($format[$k][$version], $this->_data[$k]);
				}
			}
			if ($this->_data['carburant']) $a[str_replace(' ', '_', strtolower($this->_data['carburant']))] = $this->_data['carburant'];
			if ($this->_data['longueur'] && $this->_data['largeur'] && $this->_data['hauteur'])
				$a['dimensions'] = sprintf($format['dimensions'][$version], $this->_data['longueur'], $this->_data['largeur'], $this->_data['hauteur']);
		}
		// pr�senter les d�tails
		foreach ($a as $k => $v)
			$txt .= str_repeat("\t",4).'<'.$elt.' class="spec '.$k.'">'.$v.'</'.$elt.'>'."\n";
		return $txt;
	}
	/**
	* Renvoie le chemin vers l'image de la cat�gorie
	* 
	* @param string $size (sml|med|big)
	* @returns chemin vers le fichier
	*/
	public function getImg($size, $folder = null)
	{
		if (!preg_match('/^(sml|med|big)(\-rear)?$/', $size)) {
			$size = 'med';
		}
		$imgPath = $this->_imgPath;
		if ($folder && in_array($folder, array('categories', 'categories3'))) {
			$imgPath = 'fichiers/'.$folder.'/';
			if (!is_file(BP.'/'.$imgPath.$this->getType().'/'.$this->_data['mnem']."-$size.png")) {
				$imgPath = $this->_imgPath;
			}
		}
		return $imgPath.$this->getType().'/'.urlencode($this->_data['mnem'])."-$size.png";
	}
	public function getURLResa($catID = null)
	{
		if (!$catID) $catID = $this->getId();
		$q = array('type' => strtolower($this->_data['type']), 'categorie'=> $catID);
		return Page::getURL('?'.http_build_query($q));
	}
	public function getAlternatives()
	{
		if (!$this->_alternatives) {
			// pour les VP et VU on recherche les alternatives
			if (in_array(strtolower($this->_data['type']), array('vp','vu'))) {
				if ($str = getsql(sprintf('select alternatives from categorie_alternatives where categorie="%s"', $this->getData('mnem'))))
					$this->_alternatives = explode(',', $str);
			}
			// il doit y avoir la cat�gorie courante
			if (!in_array($this->_data['mnem'], $this->_alternatives))
				$this->_alternatives[] = $this->_data['mnem'];
		}
		return $this->_alternatives;
	}
}
?>
