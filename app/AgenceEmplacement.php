<?php
class AgenceEmplacement extends Ada_Object
{
	/**
	* Renvoie une AgenceEmplacement
	* 
	* @param int $id
	* @return AgenceEmplacement
	*/
	static public function factory($id)
	{
		$x = new AgenceEmplacement();
		$x->load((int)$id);
		return ($x->isEmpty() ? null : $x);
	}
	
	public function __toString()
	{ 
		return $this->_data['nom'];
	}
}
?>
