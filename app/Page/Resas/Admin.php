<?php

class Page_Resas_Admin extends Page_ResasBase {
	
	static public $_MENU = array (
		'index'	=> array(
			'title' => "Cr�ation des offres locales",
			'menu'	=> array(
				'tarifs'	=> array('title'	=> 'Tarifs'),
				'promo_simple' => array('title' => "Promotions"),
				'promo_early' => array('title' => "R�servations anticip�es"),
				'promo_yield' => array('title' => "Gestion tarifaire selon stocks disponibles"),
			),
		),
		'recherche' => array(
			'title' => 'Gestion des offres locales',
		),
		'config' => array(
			'title' => "Param�tres de gestion"
		),
		'agence' => array(
			'title'	=> 'Page Agence',
			'nolink' => true,
			'menu'	=> array(
				'agence_promo'	=> array('title' => 'Remont�e promotions'),
				'agence_bonsplans'	=> array('title' => 'Remont�e bons plans'),
			),
		),
	);
	
	// neturaliser le tracking pour resas/admin
	public function writeJSStats($hashKey = null) {
		/* on ne veut pas suivre l'activit� de l'admin */
		return;
	}
	
	/**
	* Renvoie un menu <ul> <li> 
	* 
	* @param array $menu
	* @returns string
	*/
	public function getMenu($menu = null) {
		if (!$menu) {
			return $this->getMenu(self::$_MENU);
		}
		$current = basename($this->getId());
		$html = '<ul class="menu">'."\n";
		foreach($menu as $k => $info) {
			if (!$this->isActionAllowed($k))
				continue;
			$html .= '<li';
			$class = 'menuu-'.$k;
			if ($current == $k) {
				$class.=' current';
				$this->setData('menu_title', $info['title']);
			}
			$html.=' class="'.$class.'"';
			$html.='>';
			if (isset($info['nolink'])) {
				$html .= '<a>'.$info['title'].'</a>';
			} else {
				$html.= '<a href="'.$k.'.html">'.$info['title'].'</a>';
			}
			if (isset($info['menu'])) 
				$html.= $this->getMenu($info['menu']);
			$html.='</li>'."\n";
		}
		$html.= '</ul>';
		return $html;
	}
	/**
	* L'utilisateur courant a-t-il le droit ?
	* 
	* @param string $action au sens de Page (getAction() et de getConfigData())
	* @param string $op		edit, view
	* @returns bool
	*/
	public function isActionAllowed($action, $op='edit')
	{
		// il doit y avior un utilisatuer authentifier
		if (!$this->_resasUser) 
			return false;
		if (!$this->_resasUser->hasRights(Acces::SB_GESTION))
			return false;
		// pour les diff�rentes actions possibles
		switch($action) {
			case 'index':
			case 'recherche':
			case 'agence':
			case 'agence_promo':
				return true;
			case 'agence_bonsplans':
				return true;
			case 'config':
				return $this->_resasUser->hasRights(Acces::SB_OFFRES);
			case 'promo_simple':
			case 'promo_early':
				return $this->_resasUser->getConfigData($action);
			case 'promo_yield':
				if ($this->_resasUser->getConfigData($action)) {
					if ($op == 'view' || $this->_resasUser->hasRights(Acces::SB_OFFRES)) {
						return true;
					}
				}
				return false;
			case 'tarifs':
				if ($this->_resasUser->getConfigData($action)) {
					if ($this->_resasUser->hasRights(Acces::SB_OFFRES)) {
						return true;
					}
				}
				return false;
			default:
				return false;
		}

	}
	
	/**
	* Affichage de la page d'accueil
	* 
	*/
	public function indexAction() {
		return;
	}
	
	/**
	* Page promo simple.
	*/
	public function promo_simpleAction()
	{
		// v�rifier les droits
		if (!$this->isActionAllowed($this->getAction())) {
			self::redirect(self::getURL('resas/admin/index.html', null, true));
			return;
		}
		$this->addCSS('css/anytime.min.css');
		$this->addJS('scripts/anytime.min.js');
		$this->addJS('skin/resas/resas/admin/js/resas_admin.js');
		
		// modification ou suppression d'une promo existante
		$id = (int) ($_POST['id'] ? $_POST['id'] : $_GET['id']);
		if ($promo = AgencePromoForfait::factory($id))
		{
			// on v�rifie que l'utilisateur a le droit de manipuler cette promotion
			if (!$promo['agence'] || $promo['perimetre'] != 'RESAS' || !$this->_resasUser->hasRightsOnAgence($promo['agence'])) {
				$promo = null;
			}
			// suppression promo (renvoie un JSON)
			if ($_GET['unpublish']) {
				header('Content-type: application/json; charset=utf-8');
				if ($promo) {
					$promo->unpublish();
					$output = '{"status":1, "message":"La promotion a �t� supprim�e"}';
				} else {
					$output = '{"status":0, "message":"Erreur : cette promotion n\'existe pas ou vous n\'�tes pas autoris� � la modifier."}';
				}
				echo utf8_encode($output);
				exit();
			}
			// sinon on va modifier la promotion
			if (!$promo) {
				self::redirect(self::getURL('resas/admin/index.html', null, true));
				return;
			}
			// promo_type doit �tre coh�rent avec l'action
			if ('promo_'.$promo['promo_type'] != $this->getAction()) {
				self::redirect(self::getURL('resas/admin/promo_'.$promo['promo_type'].'.html?id='.$id, null, true));
				return;
			}
			// modification promo
			$this->setData('promo', $promo);
		}
		
		// traitement du formulaire (creation/modification promo)
		$data = $_POST;
		if (!empty($data['frm_promotion']))
		{
			$errors = array();
			// modification promo
			if ($promo) {
				if ($promo->update($data, $errors)) {
					$this->setData('form_valid', true);
					$this->_addMessage('La promotion a bien �t� modifi�e.');
				}
			} else {
				// nouvelle promo : renseigner et v�rifier quelques champs
				$data['perimetre'] = 'RESAS';
				$data['createur'] = $this->_resasUser['login'];
				if (!$data['agence']) {
					// l'agenc est obligatoire
					$errors[] = "L'agence doit �tre renseign�e";
				} else if (!$this->_resasUser->hasRightsOnAgence($data['agence'])) {
					// l'utilisateur doit avoir les droits
					$errors[] = "Vous n'avez pas le droit de cr�er une promotion pour cette agence";
				} else if (AgencePromoForfait::create($data, $errors, $summary)) {
					// OK c'est cr�� !
					$this->setData('form_valid', true);
					if (count($summary) > 1) {
						$msg = "Une nouvelle promotion cr��e :";
					} else {
						$msg = "Des nouvelles promotions cr��s :";
					}
					foreach($summary as $k => $v)
						$msg.='<br/>&nbsp;&nbsp;#'.$k.': '.$v;
					$this->_addMessage($msg);
				}
			}
			// s'il y a des erreurs on les signale
			if (count($errors)) {
				$this->setData('form_valid', false);
				$this->setData('errors', $errors);
				$this->setData('post_data', $data);
			}
		}
	}
	
	/**
	* Page promo early. Meme logique que promo simple.
	*/
	public function promo_earlyAction() {
		return $this->promo_simpleAction();
	}
	
	/**
	* Page promo yield
	*/
	public function promo_yieldAction()
	{
		// v�rifier les droits
		if (!$this->isActionAllowed($this->getAction())) {
			self::redirect(self::getURL('resas/admin/index.html', null, true));
			return;
		}
		// renvoie un json des categories pour lesquelles il y a une promo (sur cette agence)
		if(preg_match('/^v[pu]$/i', $_GET['type']) && preg_match('/^[a-z0-9\-\_\s]+$/i', $_GET['code_groupe']))
		{
			header("Content-type: application/json; charset=utf-8");
			$sql = 'SELECT DISTINCT categorie FROM agence_promo_yield WHERE code_base="%s" AND code_groupe="%s" AND type="%s"';
			$rs = sqlexec(sprintf($sql, $this->getResasUser()->getCodeBase(), $_GET['code_groupe'], $_GET['type']));
			$categories = array();
			while ($row = mysql_fetch_assoc($rs)) {
				$categories[] = addslashes($row['categorie']);
			}
			echo json_encode($categories);	
			exit();
		}
		// suppression d'une promo yield
		elseif($_GET['unpublish'] && preg_match('/^[A-Z0-9]{1,2}[\'\+]?$/', $_GET['categorie']) && preg_match('/^[a-z0-9\s\-\_]+$/i', $_GET['code_groupe']))
		{
			header("Content-type: application/json; charset=utf-8");
			$sql  = 'DELETE FROM agence_promo_yield WHERE code_base="%s" AND code_groupe="%s" AND categorie="%s"';
			sqlexec(sprintf($sql, $this->getResasUser()->getCodeBase(), $_GET['code_groupe'], $_GET['categorie']));
			$output = '{"status":1, "message":"La promotion a �t� supprim�e"}';
			echo utf8_encode($output);	
			exit();
		}
		// enregistrement promo
		$data = $_POST;
		$errors = array();
		if(!empty($data['frm_promotion']))
		{
			$data['code_base'] = $this->getResasUser()->getCodeBase();
			if(AgencePromoYield::create($data, $errors)) {
				$this->setData('form_valid', true);
			} else {
				$this->setData('form_valid', false);
				$this->setData('errors', $errors);
				$this->setData('post_data', $data);
			}
		}
		// si on a en GET une categorie et un groupe, on veut modifier : donc on pr�remplit le formulaire
		if(preg_match('/^[A-Z0-9]{1,2}[\'\+]?$/', $_GET['categorie']) && preg_match('/^[a-z0-9\-\_\s]+$/i', $_GET['code_groupe']))
		{
			$sql  = 'SELECT * FROM agence_promo_yield WHERE code_base="%s" AND code_groupe="%s" AND categorie="%s" ORDER BY jauge_min';
			$rs = sqlexec(sprintf($sql, $this->getResasUser()->getCodeBase(), $_GET['code_groupe'], $_GET['categorie']));
			$promo = array();
			while($row = mysql_fetch_assoc($rs))
			{
				$promo[] = $row;
			}
			$this->setData('promo', $promo);
		}
	}
	
	/**
	* Page recherche
	* Note : meme formulaire que promo_simple
	*/
	public function rechercheAction() {
		if (!$this->isActionAllowed($this->getAction())) {
			self::redirect(self::getURL('resas/admin/index.html', null, true));
			return;
		}
		$this->addCSS('css/anytime.min.css');
		$this->addJS('scripts/anytime.min.js');
		$this->addJS('skin/resas/resas/admin/js/resas_admin.js');
		
		// traitement du formulaire (note : agence obligatoire)
		$data = $_POST;
		// on r�cup�re l'agence recherch�e
		$agences = $this->getResasUser()->getPointsDeVente();
		$agence = $agences[$data['agence']];
		if(!empty($data['frm_recherche']) && $agence)
		{
			// 1.promos simple/early
			
			// dates
			$depuis = euro_iso($data['depuis']);
			$jusqua = euro_iso($data['jusqua']);
			$debut = euro_iso($data['debut']);
			$fin = euro_iso($data['fin']);
			// effectuer la recherche
			$sql  = "SELECT * FROM agence_promo_forfait\n";
			$sql .= "WHERE agence='".$agence['id']."'\n";
			$sql .= "  AND actif = 1\n";
			$sql .= "  AND perimetre = 'RESAS'\n";
			$sql .= "  AND classe IS NULL\n";
			// type de vehicule
			if(!empty($data['type']))
				$sql .="  AND (type IS NULL OR type='".$data['type']."')\n";
			// forfait
			// !in_array('', $data['forfait']) car le select multiple poste au moins : array(1) { [0]=> string(0) "" } (et pas NULL ou array vide)
			if(!empty($data['forfait']) && !in_array('', $data['forfait']))
			{
				$sql .='  AND (forfait IS NULL OR forfait IN (';
				$sql .= '"'.implode('","', $data['forfait']).'"';
				$sql .= ' ))'."\n";
			}
			// categories (au moins une des categories coch�es est concern�e par les promos remont�s)
			if(is_array($data['categories']) && count($data['categories']) > 0)
			{
				$sql .="  AND (";
				$sql .= " categories IS NULL OR ";
				foreach($data['categories'] as $c) {
					$sql .= ' FIND_IN_SET("'.$c.'", categories) + ';
				}
				$sql .= "0 > 0 )\n";	// meilleure methode ?
			}
			// dates debut/fin resa
			if(!empty($depuis))
				$sql .="  AND (jusqua IS NULL OR jusqua >= '".$depuis."')\n";
			if(!empty($jusqua))
				$sql .="  AND (depuis IS NULL OR depuis <= '".$jusqua."')\n";
			// dates debut/fin location
			if(!empty($debut))
				$sql .="  AND (fin IS NULL OR fin >= '".$debut."')\n";
			if(!empty($fin))
				$sql .="  AND (debut IS NULL OR debut <= '".$fin."')\n";
			// ordre des resultat
			$sql .=" ORDER BY promo_type+0 ASC, forfait, type, debut, jours_avant";
			// execution de la requete, resultats dans 2 sous-tableaux (simple/early)
			$rs = sqlexec($sql);
			$resultats = array(
				'simple' => array(),
				'early' => array()
			);
			while ($row = mysql_fetch_assoc($rs)) {
				$key = $row['type'] ? strtoupper($row['type']) : 'Tous types ';
				$key.= '/'.($row['forfait'] ? $row['forfait'] : ' Tous forfaits');
				$resultats[$row['promo_type']][$key][] = $row;
			}
			
			// 2.promos yield (une ligne par type de v�hicule)
			$resultats['yield'] = array();
			$sql = "SELECT y.*\n";
			$sql.="FROM agence_promo_yield AS y\n";
			$sql.="JOIN categorie c ON (c.type=y.type AND y.categorie=c.mnem AND c.publie=1)\n";
			$sql.='WHERE y.code_base = "'.$agence['code_base'].'"'."\n";
			$sql.='  AND y.code_groupe = "'.$agence['code_groupe'].'"'."\n";
			if(!empty($data['type']))
				$sql .= " AND y.type='".$data['type']."'\n";
			if(!empty($data['categories'])) {
				$sql .='  AND y.categorie IN (';
				$sql .= '"'.implode('","', $data['categories']).'"';
				$sql .=")\n";
			}
			$sql .= ' ORDER BY c.type, c.position, y.jauge_min';
			
			$rs = sqlexec($sql);
			while ($row = mysql_fetch_assoc($rs)) {
				$resultats['yield'][$row['categorie']][] = $row;
			}
			
			// resultats
			$this->setData('resultats', $resultats);
			
		}
		// donn�es formulaire
		$this->setData('post_data', $data);
	}
	
	/**
	* Enregistrement des tarifs locaux
	* 
	*/
	public function tarifsAction()
	{
		// l'utilisateur est-il autoris� ?
		if (!$this->isActionAllowed('tarifs')) {
			self::redirect(self::getURL('resas/admin/index.html', null, true));
			return;
		}
		// v�rifier les diff�rents champs
		// zone
		if (isset($_POST['zone'])) {
			if (!preg_match('/^[a-z]{2}$/', $_POST['zone'])) {
				unset($_POST['zone']);
			}
		}
		// agences
		if (isset($_POST['agences'])) {
			if (!is_array($_POST['agences'])) {
				unset($_POST['agences']);
			} else {
				$agences = $this->_resasUser->getAgences();
				$societes = $this->_resasUser->getSocietes();
				foreach($_POST['agences'] as $k) {
					if (!isset($agences[$k]) && !isset($societes[$k])) {
						unset($_POST['agences']);
						break;
					}
				}
			}
		}
		// type
		$type = null;
		if (isset($_POST['type']) && !in_array($_POST['type'], array('vp','vu'))) {
			unset($_POST['type']);
		} else {
			$type = $_POST['type'];
		}
		// cat�gories
		if (isset($_POST['categories'])) {
			if (!is_array($_POST['categories'])) {
				unset($_POST['categories']);
			} else {
				foreach($_POST['categories'] as $k) {
					if (!preg_match('/^\d{1,3}$/', $k)) {
						unset($_POST['categories']);
						break;
					}
				}
			}
		}
		// prefixes
		if (isset($_POST['prefixes'])) {
			if (!is_array($_POST['prefixes']) || !$type) {
				unset($_POST['prefixes']);
			} else {
				$prefixes = AgencePromoForfait::getForfaits();
				foreach($_POST['prefixes'] as $k) {
					if (!in_array($k, $prefixes[$type])) {
						unset($_POST['prefixes']);
						break;
					}
				}
			}
		}
		// dur�es
		foreach(array('duree_min','duree_max') as $k) {
			if (isset($_POST[$k]) && !preg_match('/^\d{1,2}$/', $_POST[$k])) {
				unset($_POST[$k]);
			}
		}
		/*
		** Enregistrer les tarifs
		*/
		if (isset($_POST['frm_tarifs']) && isset($_POST['tarifs']) && is_array($_POST['tarifs'])) {
			$deletedIDs = array();
			// on parcourt les tarifs, on v�rifie puis on enregistre si n�cessaire
			foreach($_POST['tarifs'] as $tarif) {
				// v�rification des arguments
				if (!in_array($tarif['categorie'], $_POST['categories'])) continue;
				if (!in_array($tarif['agence'], $_POST['agences'])) continue;
				if (!preg_match('/^[A-Z0-9\_]+$/i', $tarif['forfait'])) continue;
				if (!preg_match('/^(\d+(\.\d{2})?)?$/', $tarif['prix'])) continue;
				if (!preg_match('/^(\d+(\.\d{2})?)?$/', $tarif['prix_jour'])) continue;
				// s'il n'y a pas de modification on n'enregistre pas
				if ($tarif['prix']==$tarif['o_prix'] && (!isset($tarif['prix_jour']) || $tarif['prix_jour']==$tarif['o_prix_jour'])) {
					continue;
				}
				// s'il y a un ID et que les prix ne sont plus renseign�es, on supprime
				if ($tarif['id'] && $tarif['prix']=='' && (!isset($tarif['prix_jour']) || $tarif['prix_jour']=='')) {
					if (preg_match('/^\d+$/', $tarif['id'])) {
						$deletedIDs[] = $tarif['id'];
					}
				} else {
					save_record('agence_forfait_tarif', $tarif);
				}
			}
		}
		// si n�cessaire on supprime certains tarifs
		if (count($deletedIDs)) {
			sqlexec('DELETE FROM agence_forfait_tarif WHERE id IN ('.join(',', $deletedIDs).')');
		}
	}
	
	/**
	* Enregistrement de la configuration
	* 
	*/
	public function configAction() {
		if (!$this->isActionAllowed('config')) {
			self::redirect(self::getURL('resas/admin/index.html', null, true));
			return;
		}
		if (isset($_POST['frm_config']) && is_array($_POST['config'])) {
			// on r�cup�re la liste des agences et des soci�t�s pour v�rification
			$agences = $this->_resasUser->getAgences();
			$societes = $this->_resasUser->getSocietes();
			foreach($_POST['config'] as $key => $config) {
				if ($key != $config['agence']) continue;
				if (!isset($societes[$key]) && !isset($agences[$key])) {
					continue;
				}
				// enregistrer en v�rifiant les param�tres
				$a = array();
				$a['agence'] = $key;
				/* les valeurs bool�ennes */
				foreach(array('oav_acces','promo_simple','promo_early','promo_yield','grf_inclus', 'oav_tarifs_web', 'tarifs') as $k) {
					if (isset($config[$k])) {
						$a[$k] = (in_array($config[$k], array('0','1')) ? $config[$k] : '');
					}
				}
				/* les tarifs locaux ou nationaux */
				foreach(array('tarifs_vp','tarifs_vu','tarifs_sp') as $k) {
					if (isset($config[$k])) {
						$a[$k] = (isset(AgenceConfig::$TARIFICATIONS[$config[$k]]) ? $config[$k] : '');
					}
				}
				foreach(array('remise_vendeur','remise_paiement') as $k) {
					if (isset($config[$k]) && preg_match('/^(\d{1,2}(\.\d{2})?)?$/', $config[$k])) {
						$a[$k] = $config[$k];
					}
				}
				save_record('agence_config', $a);
			}
			// forcher le rechargement de la configuration apr�s cette mise � jour
			$this->_resasUser->resetConfig();
		}
	}
	/**
	* Enregistrer les promotions � publier
	* 
	*/
	public function agence_promoAction()
	{
		// l'utilisateur est-il autoris� ?
		if (!$this->isActionAllowed('agence_promo')) {
			self::redirect(self::getURL('resas/admin/index.html', null, true));
			return;
		}
		// on r�cup�re l'agence s�lectionn�e
		if ($_POST['frm_promos']) {
			if (isset($_POST['agence']) && is_array($_POST['promos'])) {
				$pdvs = $this->getResasUser()->getPointsDeVente();
				if ($agence = $pdvs[$_POST['agence']]) {
					// on parcourt les promotions pour les enregistrere
					$unpublished = array();
					foreach($_POST['promos'] as $id => $visuel) {
						if (!$visuel) {
							$unpublished[] = (int) $id;
						} else {
							// pr�paerer la requ�te SQL
							$visuel = filter_var($visuel, FILTER_SANITIZE_STRING);
							$sql = 'UPDATE agence_promo_forfait';
							$sql.=sprintf(" SET publication = NOW(), visuel='%s'", $visuel);
							$sql.=sprintf(" WHERE id=%ld AND agence='%s'", (int)$id, $agence['id']);
							// enregistrer la modidfication
							sqlexec($sql);
						}
					}
					// on d�publie tous les autres
					if (count($unpublished)) {
						sqlexec(sprintf("UPDATE agence_promo_forfait SET visuel=NULL, publication=NULL WHERE id IN (%s) AND agence='%s'", join(',', $unpublished), $agence['id']));
					}
				}
			}
			
		}
	}
	/**
	* G�re les bons plans pour les agences
	* 
	*/
	public function agence_bonsplansAction() {
		// ajouter le contr�le pour les dates
		$this->addCSS('css/anytime.min.css');
		$this->addJS('scripts/anytime.min.js');
		$this->addJS('skin/resas/resas/admin/js/resas_admin.js');
		// CKEditor
		$this->addJS($this->getProtocol().'://cdn.ckeditor.com/4.4.6/full/ckeditor.js');
		$this->addJS('skin/resas/resas/admin/js/ckeditor.js');
		
		// l'agence courante est-elle pass�e en arguement ?
		$agenceID = (isset($_POST['agence']) ? $_POST['agence'] : (isset($_GET['agence']) ? $_GET['agence'] : null));
		// et l'utilisateur a-t-il le droit de l'�diter ?
		if ($agenceID) {
			$pdvs = $this->getResasUser()->getPointsDeVente();
			if (!isset($pdvs[$agenceID])) {
				$agenceID = null;
			} else {
				$this->setData('agence', $pdvs[$agenceID]);
			}
		}
		
		// ... puis le bon plan
		$bonPlan = null;
		if (isset($_GET['bonplan'])) {
			if ($bonPlan = AgenceBonPlan::factory((int)$_GET['bonplan'])) {
				if ($bonPlan['agence'] != $agenceID) {
					$this->_addMessage(sprintf("Le bon plan #%ld n'est pas associ�e � cette agence !", (int)$_GET['bonplan']));
					$bonPlan = null;
				} else {
					$this->setData('bon_plan', $bonPlan);
				}
			}
		}
		
		// si le formulaire est enregistr�e
		if (isset($_POST['frm_bonplan'])) {
			// si le bon plan existe
			$this->setData('form_valid', false);
			$errors = array(); $post_data = $_POST;
			if ($bonPlan) {
				if ($bonPlan->update($post_data, $_FILES, $errors)) {
					// si le statut est public et uqe l'utilisateur n'est pas "admin" on change le statut
					if (!$this->getResasUser()->hasRights(Acces::SB_ADMIN) && $bonPlan['statut'] == AgenceBonPlan::STATUT_PUBLIC) {
						$bonPlan->updateStatut(AgenceBonPlan::STATUT_ATTENTE, true);
					}
					$this->setData('form_valid', true);
				} else {
					$this->setData('post_data', $post_data);
					$this->_addMessage($errors);
				}
			} else {
				$post_data['createur'] = $this->_resasUser['login'];
				if ($bonPlan = AgenceBonPlan::create($post_data, $_FILES, $errors)) {
					$this->setData('form_valid', true);
					$this->_addMessage("Le bon plan a �t� cr�� !");
				} else {
					$this->setData('post_data', $post_data);
					$this->_addMessage($errors);
				}
			}
		}
	}
}

?>
