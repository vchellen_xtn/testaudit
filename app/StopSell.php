<?php
class StopSell extends Ada_Object
{
	static public function create(Agence $agence, Categorie $categorie, $debut, $fin)
	{
		// obtenir l'id de l'agence
		foreach(array('agence','pdv','id') as $k)
		{
			if ($agenceID = $agence[$k])
				break;
		}
		$a = array(
			'id'		=> '',
			'proveance'	=> 'web',
			'zone'		=> $agence['zone'],
			'reseau'	=> $agence['reseau'],
			'agence'	=> $agenceID,
			'type'		=> $categorie['type'],
			'categorie'	=> $categorie['id'],
			'nb'		=> 0,
			'debut'		=> $debut.' 00:00:00',
			'fin'		=> $fin.' 23:59:00'
		);
		$x = new StopSell($a);
		$x->save();
	}
}
?>
