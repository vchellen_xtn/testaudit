<?
	$menu = array(
		'resa'			=> 'La réservation en ligne',
		'agences'		=> 'Les agences ADA',
		'assurances'	=> 'Assurances',
		'conditions'	=> 'Les conditions de location',
		'vehicules'		=> 'Les véhicules',
		'contact'		=> 'Contacts utiles',
		'tarifs'		=> 'Les tarifs',
		'reseau'		=> 'Franchises'
	);
	$action = basename($page);
?>
<ul class="menu">
	<? foreach($menu as $k => $lbl) : ?>
		<li>
			<a href="<?=$k?>.html"<?if ($action==$k) echo ' class="current"'; ?>><?=$lbl;?></a>
		</li>
	<? endforeach; ?>
</ul>