<?
/**
* Conditions d'annulation historique
* - 24h avant r�servation
* - 20� si moins de 7 jours avant
* - gratuit sinon
*/
class Reservation_ConditionsAnnulation01 extends Reservation_ConditionsAnnulation
{
	const FRAIS_ANNULATION = 20;
	
	public function isCancellable($preavis, &$msg)
	{
		if ($preavis <= 24)
		{
			$msg = "L'annulation doit �tre faite au moins 24 heures avant le jour de votre d�part. En de�� de 24h vous n'avez pas la possibilit� d'annuler votre r�servation.";
			$msg .= '<br /><span class="rouge">Vous ne pouvez plus annuler cette r�servation.</span>';
			return false;
		}
		// on d�tailles les conditions d'annulation selon le d�lai
		if ($preavis <= 24 * 7)
			$msg = 'L\'annulation intervient <span class="rouge">moins de 7 jours avant votre d�part</span> : le montant de votre r�servation va vous �tre rembours�, <span class="rouge">avec une d�duction pour '.$this->getCancellationFees($preavis).' � de frais de dossier</span>';
		else
			$msg = 'L\'annulation intervient <span class="rouge">7 jours ou plus avant votre d�part</span> : le montant total de votre r�servation va vous �tre rembours�.';
		return true;
	}
	public function getCancellationFees($preavis)
	{
		if ($preavis <= 24 * 7)
			return self::FRAIS_ANNULATION;
		return 0;
	}
	
	public function getConditions()
	{
		$str = <<<EOT
		<p style="margin-bottom:10px;">
			L'annulation doit �tre faite au moins 24 heures avant le jour de votre d�part. En de�� de 24h vous n'avez pas la possibilit� d'annuler votre r�servation.<br />
			Si votre annulation intervient :
		</p>
		<ul class="tiret">
			<li><span class="rouge gras">7 jours ou plus avant votre d�part</span> :  le montant total de votre r�servation vous est rembours�</li>
			<li><span class="rouge gras">moins de 7 jours avant votre d�part</span> : le montant de votre r�servation vous est rembours�, avec une d�duction de 20 � de frais de dossier</li>
			<li><span class="rouge gras">2 mois apr�s votre r�glement initial</span> : Cette r�servation ne peut pas �tre annul�e en ligne. Pour demander un remboursement, merci d'adresser un email � <a href="mailto:reservation@ada.fr">reservation@ada.fr</a> en pr�cisant le n� de la r�servation concern�e. Nous vous confirmerons l'annulation dans les plus brefs d�lais.</li>
		</ul>
		<p style="margin-bottom:10px;">
			<strong>Il n'y a pas lieu � remboursement :</strong>
		</p>
		<ul class="tiret">
			<li>pour toute location plus courte que la dur�e pr�vue dans la r�servation</li>
			<li>si le conducteur principal ne se pr�sente pas pour la prise du v�hicule</li>
			<li>en cas de retard dans la prise du v�hicule</li>
			<li>en cas de d�faut de prise du v�hicule � l'heure pr�vue de d�but de location</li>
			<li>en cas d'annulation faite moins de 24h avant votre d�part</li>
		</ul>
EOT;
		return $str;
	}
	
	/**
	* Informations sur l'annulation de cette r�servation
	* returns @string
	*/
	public function getInfos()
	{
		$str = <<<EINFO
		Vous pouvez annuler votre r�servation jusqu'� 48h avant la prise du v�hicule.<br />
		Des frais de dossier de 20 euros TTC vous seront factur�s pour toute annulation au-del� de 48h avant votre d�part. Moins de 48h avant votre d�part : le montant de votre r�servation ne vous sera pas rembours�.<br/>
EINFO;
		return $str;
	}
}
?>
