/* Ecrit un cookie 1st part */
function sp_setCookie(name,value) {
	/* Le cookie expire apr�s 30 minutes d'inactivit� */
	var today = new Date();
	today.setTime( today.getTime() );
	var expires = 30 * 60 * 1000 ;
	var expires_date = new Date( today.getTime() + (expires) );
	document.cookie = name+"="+value+";expires="+expires_date.toGMTString();
}
/* Supprime un cookie */
function sp_delCookie(name) {
	document.cookie=name+"=;expires=Thu, 01-Jan-1970 00:00:01 GMT";
}
/* R�cup�re la valeur d'un cookie 1st part */
function sp_getCookie(name,reset) {
	var a_all_cookies = document.cookie.split( ';' );
	var a_temp_cookie = '';
	var cookie_name = '';
	var cookie_value = '';
	var b_cookie_found = false; 	
	for (i=0; i<a_all_cookies.length; i++) {
		a_temp_cookie = a_all_cookies[i].split('=');
		cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g,'');		
		if ((cookie_name==name) && (a_temp_cookie[1])) {
			b_cookie_found = true;
			cookie_value = unescape(a_temp_cookie[1].replace(/^\s+|\s+$/g,''));
			if (reset) { sp_delCookie(name); }
			return cookie_value;
			break; }
		a_temp_cookie = null;
		cookie_name = ''; }
	if (!b_cookie_found) { return ''; }
}

function sp_tracker(sp,ci)
{var smartParam = "";for(var i=0;i<sp.length;i++) smartParam += sp[i];document.cookie = 'sp=1; expires=Sunday, 02-Feb-2020 20:20:20 GMT';
if (ci!="") smartParam += "&_ci=" +ci;
var res, col;
if (window.screen) { res = screen.width + "x" + screen.height; col = screen.colorDepth;} else {res ="n/a"; col="n/a";}
ctz=(new Date()).getTimezoneOffset();smartParam += "&_cr=" + Math.round (Math.random()*1000000000000000);
smartParam += "&_dom=" + escape(window.location.protocol+"//"+window.location.hostname);
smartParam += "&_ref=" + escape(document.referrer.substring(0,254));
smartParam += "&_res=" + escape(res) + "&_col=" + escape(col);
smartParam += "&_tz=" + Math.round(((ctz==(new Date(20010101)).getTimezoneOffset())?ctz:(ctz+60))/-60);
smartParam += "&_ck=" + (document.cookie.indexOf('sp=1') >= 0);
smartParam += "&_event=" + escape(getParamValue('event'));
var sp_sess=sp_getCookie('sp_sess',0);
if (sp_sess=="") { // Pas de cookie de session
	sp_sess=Math.round (Math.random()*1000000000000000);
}
sp_setCookie('sp_sess',sp_sess); // on remet la date d'expiration � jour
smartParam += "&_SessionId=" + sp_sess;
smartImageF=new Image();
smartImageF.src=window.location.protocol+"//ws2.smartp.com/sp_tracker.cfm"+smartParam;
}
var sp_t=1;

function sp_send(sp)
{var clicParam="";for(var i=0;i<sp.length;i++)clicParam+=sp[i];document.cookie='spc=1; expires=Sunday, 02-Feb-2020 20:20:20 GMT';clicParam+="&_cr="+Math.round(Math.random()*1000000000000000);clicParam+="&_ck="+(document.cookie.indexOf('spc=1')>=0);clicParam+="&_dom="+escape(window.location.protocol+"//"+window.location.hostname);clicImage=new Image();clicImage.src=window.location.protocol+"//ws2.smartp.com/sp_tracker.cfm"+clicParam;}

var spClicHashID = "8DD48D6A2E2CAD213179A3992C0BE53C";
function sp_clic_init(hashID)
{ if (hashID) spClicHashID = hashID;}
function sp_clic(ctyp,clab,cfrom,cto)
{var sp=new Array();sp[0]="?_hid="+spClicHashID;sp[1]="&_ctyp="+ctyp;sp[2]="&_clab="+clab;sp[3]="&_cfrom="+cfrom;sp[4]="&_cto="+cto;sp_send(sp);}

function getParamValue(param){var qs=window.location.search.substr(1);var vars=qs.split("&");for(var i=0;i<vars.length;i++){var pair=vars[i].split("=");if(pair[0]==param){return pair[1];}}
return"";}

/**
* Appel d'un tag statistique
*/
var spECommerceHashID = null;
function sp_ecommerce_init(hashID)
{ if (hashID) spECommerceHashID = hashID;}
function callStatsTag(pTitle, pClass, pVersion) {
    if (!spECommerceHashID) return;
	var sp = new Array();
    var ci = "";
    sp[0]="?_hid=" + spECommerceHashID;
    sp[1]="&_title="+ ( (pTitle != 'null' && pTitle != 'undefined' && pTitle != undefined) ? pTitle : "");
    sp[2]="&_class="+ ( (pClass != 'null' && pClass != 'undefined' && pClass != undefined) ? pClass : "");
    sp[3]="&version="+( (pVersion != 'null' && pVersion != 'undefined' && pVersion != undefined) ? pVersion : "");

    if (typeof sp_t != 'undefined') sp_tracker(sp,ci);
} 