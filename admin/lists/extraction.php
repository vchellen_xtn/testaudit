<?
	$title 	 = "Extraction";
	$url 	 = "../extraction.php";
	$key	 = "id";
	$order	 = "desc";

	$sql = "select id, zone, reseau, canal, type, debut, fin, lignes from compta_extraction where 1";
	
	// filtrages
	if ($_GET['zone'])
		$sql .=" and zone='".addslashes($_GET['zone'])."'";
	if ($_GET["type"])
		$sql.= " and type = '".addslashes($_GET["type"])."'";
	if ($_GET["debut"])
		$sql.= " and debut >= '".addslashes($_GET["debut"])."'";
	if ($_GET["fin"])
		$sql.= " and fin >= '".addslashes($_GET["fin"])."'";
	if ($_GET['lignes'])
		$sql.= " and lignes >= ".addslashes($_GET['lignes']);
		
	$out_url = "list.php?sql=" . urlencode($sql);
	$out_url.= "&title=" . urlencode($title);
	$out_url.= "&url=" . urlencode($url);
	$out_url.= "&new_url=" . urlencode($url . "?" . $_SERVER['QUERY_STRING']);
	$out_url.= "&color=4587ba";
	$out_url.= "&key=".$key."&order=".$order."&nb_rows=30";
	header("location: ".$out_url);
?>
