<?
/**
* Conditions d'annulation historique
* - 24h avant r�servation
* - 20� si moins de 7 jours avant
* - gratuit sinon
*/
class Reservation_ConditionsAnnulationLocapass extends Reservation_ConditionsAnnulation
{
	public function isCancellable($preavis, &$msg)
	{
		if ($preavis <= 24)
		{
			$msg = "L'annulation doit �tre faite au moins 24 heures avant le jour de votre d�part. En de�� de 24h vous n'avez pas la possibilit� d'annuler votre r�servation.";
			$msg .= '<br /><span class="rouge">Pour annuler votre r�servation, merci de contacter directement votre agence</span>';
			return false;
		}
		// on d�tailles les conditions d'annulation selon le d�lai
		$msg = "Votre r�servation peut �tre annul�e.";
		return true;
	}
	// pas de frais pour Locapas
	public function getCancellationFees($preavis)
	{
		return 0;
	}
	
	public function getConditions()
	{
		return '<p>'.$this->getInfos().'</p>';
	}
	public function getInfos()
	{
		return "L'annulation doit �tre faite au moins 24 heures avant le jour de votre d�part. En de�� de 24h vous n'avez pas la possibilit� d'annuler votre r�servation.";
	}
}
?>
