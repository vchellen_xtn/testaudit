<?php
/**

* $ville: nom de laville
* $zone : fr, co ,...
* $weather : code de nebulosit�
* $description : texte d�crivant le temps
* $temperature     temperature
*
*/
?>
<div class="encart-meteo box-sizing">
	<p class="ville"><?=$ville;?></p>
	<p class="temps">
		<img class="inblock" src='../img/meteo/<?php echo $weather ?>.png'>
		<span class="inblock"><?=$temperature;?> &#176;C</span>
		<span class="desc inblock"> <?=$description;?></span>
	</p>
	<p class="date"><?=date('j/m/Y');?></p>
</div>
