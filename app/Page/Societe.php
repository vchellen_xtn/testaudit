<?
class Page_Societe extends Page
{
	public function getUniversTitre() {
		if ($this->getAction()=='index') {
			return 'Qui sommes-nous ?';
		} else {
			return 'Soci�t�';
		}
	}

	public function reseauAction()
	{
		if ($_POST['valid']) {
		
		
			$franchise = new Franchise();
			$a = $errors = array();
			$fields = array_keys($franchise->_getValidatingRules());
			foreach ($fields as $k) {
				$a[$k] = $_POST[$k];
			}
			if (is_array($a['rappel']))
				$a['rappel'] = array_sum($a['rappel']);
			// validation des donn�es
			if ($franchise->validateData($a, $errors, $fields)) {
				$a['id'] = '';
				foreach ($a as $fld => $v ) {
					$franchise->setData($fld, $v);
				}
				$franchise->save();
				// donn�es pour mail
				if ($a['rappel']) {
					if($a['rappel'] & 4) $rappel .= 'le matin';
					if($a['rappel'] & 2) $rappel .= ($rappel ? ', ' : '') . 'entre 12h et 18h';
					if($a['rappel'] & 1) $rappel .= ($rappel ? ', ' : '') . 'apr�s 18h';
					$a['rappel'] = $rappel;
				} else {
					$a['rappel'] = '';
				}
				$a['optin'] = ($a['optin'] == 1) ? 'oui' : 'non';
				$a['localopt'] = ($a['localopt'] == 1) ? 'oui' : 'non';
				// envoi du mail
				$txt_mail = load_and_parse('emails/em10.html', $a);
				send_mail($txt_mail, EMAIL_FRANCHISE, SUBJECT_FRANCHISE, EMAIL_FROM, EMAIL_FROM_ADDR, '', '');
				// le formulaire est valide
				$this->setData('is_form_valid', true);
			// il y a des erreurs
			} else if ( $errors ) {
				// tableau des donn�es filtr�es
				if ( is_array($a) ) {
					$this->setData('post_data', $a);
				}
				$this->setData('is_form_valid', false);
				$this->_addMessage($errors);
			}
		}
	}
}