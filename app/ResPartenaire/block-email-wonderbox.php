<?php
/**
* Bloc pour l'insertion du code de r&eacute;duction Wonderbox dans les emails de confirmation de r&eacute;servation emails/em1.html
*/
?>
<table cellpadding="0" cellspacing="0" border="0" style="font-family:Arial, sans-serif;">
	<tr>
		<td colspan="2">
			<img src="img/email/wonderbox/wonderbox_1.gif" alt="Merci d'avoir choisi ADA ! Vos 15&euro; offerts !" style="vertical-align:bottom;"><br>
		</td>
	</tr>
	<tr>
		<td height="67">
			<img src="img/email/wonderbox/wonderbox_2.gif" alt="Wonderbox" style="vertical-align:bottom;"><br>
		</td>
		<td height="67">
			<table cellpadding="0" cellspacing="0" border="0" bgcolor="#cc0000" style="color: #fff; font-family:Arial, sans-serif; font-size:10px;" align="center" height="67">
				<tr>
					<td colspan="3" align="center" height="27" valign="middle">
						<span style="font-size:10px; line-height: 12px; font-family:Arial, sans-serif;">Profitez de 70 coffrets cadeaux et 37.000 activit&eacute;s : bien-&ecirc;tre, s&eacute;jour, gastronomie, sport...</span><br>
					</td>
				</tr>
				<tr >
					<td width="10" height="20">
						&nbsp;
					</td>
					<td bgcolor="#ffffff" align="center" height="20" valign="middle">
						&nbsp;<b style="color:red; font-family:Arial, sans-serif;">En utilisant ce code : </b><span style="color: #000; font-size:12px;"><?=$this->getData('code');?></span>
					</td>
					<td width="10" height="20">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td colspan="3" align="center" height="20" valign="middle">
						<font style="font-size:10px; font-family:Arial, sans-serif;">Code &agrave; utiliser <b>&agrave; partir de 49,90 &euro; d'achats sur <a href="http://www.wonderbox.fr/" target="_blank" style="color:#fff;">www.wonderbox.fr</a></b></font>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td colspan="2"><font style="font-family:Arial, sans-serif; font-size:9px; line-height: 11px;">Ce code est valable &agrave; partir de 49,90 &euro; d'achats sur <a href="http://www.wonderbox.fr/" target="_blank" style="color:#000;">www.wonderbox.fr</a> jusqu'au 31/12/2016 et n'est pas cumulable avec d'autres promotions. Offre limit&eacute;e &agrave; un code de r&eacute;duction par commande.</font></td></tr>
</table>
