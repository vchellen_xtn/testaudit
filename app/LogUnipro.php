<?php
class LogUnipro extends Ada_Object
{
	static public function create($reservation, $action, $code, $msg)
	{
		$a = array
		(
			'id'			=> '',
			'reservation'	=> $reservation,
			'action'		=> $action,
			'erreur_code'	=> $code,
			'erreur_message'=> $msg
		);
		$x = new LogUnipro($a);
		$x->save();
		return $x;
	}
}
?>
