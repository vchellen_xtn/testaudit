<?

class Dept extends Ada_Object
{
	static public function isValidID($id)
	{
		return preg_match('/^\d{2}$/', $id);
	}
	
	/**
	* Cr�e un objet � partir d'un identifiant
	* 
	* @param string $id
	* @return Dept
	*/
	static public function factory($id)
	{
		$x = new Dept();
		if ($x->load($id)->isEmpty())
			return null;
		return $x;
	}
	public function getURL()
	{
		return Page::getURL(gu_agence($this->_data));
	}
	public function __toString()
	{
		$str = $this->_data['nom'].' ('.$this->_data['id'].')';
		if ($nb = $this->_data['agences'])
			$str .= ", $nb agence".($nb > 1 ? 's': '');
		return $str;
	}
	/**
	* pour assuer la compatibilti� avec le flash des d�partements fran�ais /agences/index
	* 
	* @param array $arrAttributes
	* @param string $rootName
	* @param bool $addOpenTag
	* @param bool $addCdata
	* @return string
	*/
	public function toXml(array $arrAttributes = array(), $rootName = 'item', $addOpenTag=false, $addCdata=true)
	{
		$url = substr(gu_agence($this->_data), 0, -5);
		return '<'.$rootName.' val="'.$this->getId().'" title="'.utf8_encode($this->_data['nom']).'" nb="'.$this->_data['agences'].'" dept_url="../'.$url.'" agence_url="../'.$url.'"/>';
	}
	/**
	* Renvoie les d�partements limitrophes
	* 
	*/
	public function getContours()
	{
		return Dept_Collection::factory(Agence::STATUT_VISIBLE, null, $this);
	}
}
?>