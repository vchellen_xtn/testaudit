<?php
/**
* $this est Reservation
* $page page o� se trouve le bloc
* $showTotal : afficher le total ou pas ?
* $showPrixOptions : affichr ou non le prix des options
* $lblTotal : label pour le Prix Total
*/
	if (!$lblTotal) $lblTotal = 'Prix';
	/** @var Reservation */
	$reservation = $this;
	/** @var ResOption_Collection */
	$options = $reservation->getResOptions();
	/** @var Categorie */
	$categorie = $reservation->getCategorie();
	/** @var Agence */
	$agence = $reservation->getAgence();
?>
<div id="recapitulatif" class="grey-box recap">
	<h2 id="title-agence" class="title-like loc-agence toggle-bar no-background dropdown-trigger" data-target="#info-agence"><?=$agence['nom']; ?></h2>
	<div id="info-agence" style="display: none;" class="tpl_popin">
		<?php echo $agence->toHtml('block-agence-mobile'); ?>
	</div>
	<div class="row type-table">
		<div class="col-xs-6">
			<p>
				<span class="title-like">R�servation</span><br />
				du <?=$reservation->getDebut('d/m/y H:i');?> au <?=$reservation->getFin('d/m/y H:i');?><br />
				<span class="categorie title-like nowrap"><?=$categorie['categorie_nom']?></span><br />
				<? if (strstr('AP', $reservation['statut'])) : ?>
				R�servation <span class="nowrap">n&#176;<? echo $reservation->getResIDClient(); ?></span><br />
				<? endif; ?>
				Pour <?=$reservation->getDuree();?><br />
				<? if (!$reservation->isLocapass()) :?>
					<?=$reservation->getKm(false);?><br />
				<? elseif($conducteur = $reservation->getConducteur()) :?>
					Conducteur <?=$conducteur['prenom'].' '.$conducteur['nom'];?>
				<? endif; ?>
			</p>
		</div>
		<div class="col-xs-6 text-right">
			<img src="../<?=$categorie->getImg('med', 'categories3')?>" class="visu" alt="" />
		</div>
	</div>

	<? if ($showTotal): ?>
		<table class="total-price">
			<tr>
				<th><?=$lblTotal;?> : </th>
				<td>
					<span class="prix_total" id="recap_prix_total" data-var="<?=$reservation->getPrixTotal()-round($code['reduction_max']-$code['reduction'],2);?>"><?=show_money($reservation->getPrixTotal())?> &euro;</span>
				</td>
			</tr>
		</table>
	<? endif; ?>

	<a href="#" class="dropdown-trigger btn btn-default btn-submit small fullwidth" data-target="#booking--more-info">D�tails de la r�servation</a>
	<div id="booking--more-info" class="booking--more-info dropdown-target" style="display:none;">
		<div class="recap--options-n-price">
			<? if ($reservation->isLocapass()) :?>
			<img src="../css/img/locapass.png" alt="Locapass" />
			<? else :?>
				<? if ($showPrixOptions) : ?>
				<ul>
					<li>
						<table class="res_opt_nom res_opt_dk02">
							<tr>
								<td class="res_opt_name">Tarif de r�servation</td>
								<td class="res_opt_prix"><?=sprintf('%01.2f', $reservation->getPrixForfait());?>&nbsp;�</td>
							</tr>
							<? if ($surcharge = $reservation->getPrixSurcharge()) : ?>
							<tr>
								<td class="res_opt_name"><?=$reservation->getAgence()->getSurchargeLabel();?></td>
								<td class="res_opt_prix"><?=sprintf('%01.2f', $surcharge);?>&nbsp;�</td>
							</tr>
							<? endif; ?>
							<? if ($prix_coupon = $reservation->getPrixCoupon()) : ?>
							<tr>
								<td class="res_opt_name">Offre Pro</td>
								<td class="res_opt_prix"><?=sprintf('%01.2f', $prix_coupon);?>&nbsp;�</td>
							</tr>
							<? endif; ?>
						</table>
					</li>
				</ul>
				<? endif; ?>
				<? if ($reservation->getResOptions()->count()) : ?>
					<? echo $reservation->getResOptions()->toHtml3($showPrixOptions); ?>
				<? endif; ?>
				<? if ($code = $reservation->getPromotionCode()) : ?>
				<ul>
					<li>
						<table class="res_opt_nom res_opt_dk02">
							<tr>
								<td class="res_opt_name">R�duction <?=$code['campagne_nom']?></td>
								<td class="res_opt_prix">-<?=sprintf('%01.2f', $code['reduction']);?>&nbsp;�</td>
							</tr>
						</table>
					</li>
				</ul>
				<? endif; ?>
			<?endif; // !$isLocapass ?>
		</div>
	</div>
</div>
<? if (!strstr('VPXA', $reservation['statut'])) : ?>
	<form id="frm_recapitulatif" name="reservation" method="post" action="<?=Page::getURL('reservation/resultat.html');?>">
	<?
		foreach($reservation->getFields() as $k => $v) {
			if (!$v) continue;
			echo '<input type="hidden" name="'.$k.'" value="'.$v.'"/>';
		}
		if ($reservation->hasForfait()) {
			echo '<input type="hidden" name="forfait" value="'.$reservation->getForfait()->doSerialize().'"/>';
		}
		$type = strtolower($reservation['type']);
		$date_retrait = null;
		foreach($reservation->getResOptions() as $k => $option) {
			if ($option['quantite'])
				echo '<input type="hidden" name="options_u['.$k.']" value="'.$option['quantite'].'"/>';
			else
				echo '<input type="hidden" name="options[]" value="'.$k.'"/>';
			if (!$date_retrait && $option['retrait'])
				$date_retrait = $option['retrait'];
		}
		if ($date_retrait)
			echo '<input type="hidden" name="options_retrait" value="'.$date_retrait.'"/>';
	?>
	</form>
<? endif; ?>

<script type="text/javascript" language="javascript">
// <![CDATA[
	$(document).ready(function()
	{
		$("#title-agence").click(function()
		{
		//	$(this).toggleClass('opened');
		//	$("#infos_agence").stop().slideToggle();
			if ($(this).hasClass("opened") && initMapAgenceInfos) {
				initMapAgenceInfos();
			}
			return false;
		});
	});
// ]]>
</script>
