<?php
/**
* @var $title
* @var $term
*/
$here = Page::getURL(null, null, false);
?>
<div class="agglos_block">
	<h2 class="type-title-grey"><?=$title;?></h2>
	<hr />
	<ul class="dept agglos_liste">
		<li><a href="<?=$here?>location-voiture-paris.html"><?=$term;?> PARIS</a></li>
		<? foreach(AgenceAgglo_Collection::factory() as $agglo) : ?>
		<li><a href="<?=$here?>location-voiture-<?=$agglo['canon']?>.html"><?=sprintf('%s %s', $term, $agglo['nom']);?></a></li>
		<? endforeach; ?>
	</ul>
</div>