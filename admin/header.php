<?
	require('secure.php');
	require('lib.php');
	
	// si la zone est pr�cis� on la change (si l'utilisateur a le droit...)
	$zone = $_POST['zone'];
	if ($zone && preg_match('/^[a-z]{2}$/i', $zone))
	{
		$_SESSION['ADA001_ADMIN_ZONE'] = $adminUser->setCurrentZone($zone)->getCurrentZoneID();
		Page::setCookie('ADA001_ADMIN_AUTH', $adminUser->getCookie());
	}
	
	// WriteButton
	function WriteMenu($title, $url)
	{
		echo '<input value="'.$title.'" onclick="javascript:return goMenu(\''.$url.'\');" type="button">';
	}
	// pr�parer le menu en fonction des droits
	$menu = array();
	if ($adminUser->hasRights(Acces::SB_GESTION) || $adminUser->hasRights(Acces::SB_SATISFACTION) || $adminUser->hasRights(Acces::SB_OPERATEUR)) {
		$menu['resa'] 	= 'G�n�ral';
	}
	if ($adminUser->hasRights(Acces::SB_OFFRES) || $adminUser->hasRights(Acces::SB_GESTION))
	{
		$menu['gestion']= 'Gestion';
		//$menu['offre']	= 'Offre';
	}
	if ($adminUser->hasRights(Acces::SB_ADMIN))
	{
		$menu['pro']	= 'Pro.';
		$menu['campagne']	= 'Campagnes';
		$menu['compta']	= 'Compta.';
		$menu['pub']	= 'Visuels';
		$menu['geo']	= 'G�o.';
	}
	if ($adminUser->hasRights(Acces::SB_EDITO))
		$menu['seo']	= 'R�f. Nat';
	if ($adminUser->hasRights(Acces::SB_ADMIN))
		$menu['users']	= 'Acc�s';
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>ADA</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css"/>
	<style type="text/css">
		body{ position:relative; margin:0; padding:0; background:url(img/header_bg.gif) repeat-x 0 0; }
		.header-content{ position:relative; width:895px; height:60px;}
			.header-content form{ margin:0; }
		img{ display:block; float:left; margin:14px 0 0 45px; }
		select{ width:auto; }
		input.bouton{ width:auto; }
		.utilisateur{ position:absolute; top:0; right:0; margin:0; font:bold 12px Arial; line-height:20px; color:#FFF; }
		.utilisateur a{ color:#FFF; }
		.header-menu{ position:absolute; top:30px; right:0; }
	</style>
	<script type="text/javascript">
		function goMenu(url)
		{
			if (window.parent && window.parent.menu && window.parent.menu.location)
				window.parent.menu.location.href = 'menu.php?section=' + url;
			return false;
		}
		// se d�connecter
		function reset()
		{
			if (window.parent && window.parent.location)
				window.parent.location.href = "login.php?force=Yes&url=/admin/";
			return false;
		}
<? if ($_POST['zone']) { ?>
		// mettre � jour le menu
		goMenu('gestion');
<? } ?>
	</script>
</head>
<body>
	<div class="header-content">
		<img src="img/logo.png" alt="logo ADA"/>
		<form action="" method="post" class="">
			<p class="utilisateur">
				<?=$adminUser['login']?> | <a title="D�connexion" href="javascript:reset();">d�connexion</a>
			</p>
			<div class="header-menu">
<?
		foreach($menu as $url => $title)
		{
			echo '&nbsp;';
			if ($url == 'gestion')
			{
				selectarray ('zone', $adminUser->getZones(), $adminUser->getCurrentZoneID(), '-- zone --', 'onchange="this.form.submit();"');
			}
			WriteMenu($title, $url);
		}
?>
			</div>
		</form>
	</div>
</body>
</html>
