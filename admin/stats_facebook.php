<?
/**
* Facebook App Edition - http://developers.facebook.com/docs/reference/api/application/
* 
*/
	session_start();
	require_once("secure.php"); 
	require_once("lib.php");
	if (!defined('FB_APP_ID') || !defined('FB_APP_SECRET'))
		die("FB_APP_ID et FB_APP_SECRET ne sont pas d�finies");
	if (!defined('FB_APP_TOKEN') || !FB_APP_TOKEN)
	{
		die("FB_APP_TOKEN: ".file_get_contents("https://graph.facebook.com/oauth/access_token?client_id=".FB_APP_ID."&client_secret=".FB_APP_SECRET."&grant_type=client_credentials"));
	}
	$modes = array
	(
		'app'	=> "Application",
		//'insights'=> 'Insights',
		'accounts'=> 'Utilisateurs Tests',
	);
	if (!count($_GET)) $_GET['app'] = '';
	
	// interroger facebook
	$facebook = new Facebook(array('appId' => FB_APP_ID, 'secret' => FB_APP_SECRET));
	$params = array('access_token' => FB_APP_TOKEN);
	if (!$_GET['action']) $_GET['action'] = 'app';
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script type="text/javascript" src="<?=Page::getProtocol()?>://www.google.com/jsapi"></script>
	<script type="text/javascript">
		google.load('jquery', '1.4');
	</script>
	<link rel="stylesheet" type="text/css" href="../css/anytime.min.css" />
	<script type="text/javascript" src="admin.js"></script>
	<style type="text/css">
		select,input { width: inherit;}
		label { padding-left: 2px; }
		label.col1 { padding-left: 0; float: left; width: 65px;}
	</style>
</head>
<body>
<h1>Facebook</h1>
<p>
<?

	foreach($modes as $k => $v)
	{
		if (isset($_GET[$k]))
		{
			$h1 = $v;
			echo '<strong>';
		}
		else
			echo '<a href="'.basename(__file__).'?'.$k.'">';
		echo $v;
		echo (isset($_GET[$k]) ? '</strong>' : '</a>').str_repeat('&nbsp;', 4);
	}
?>
</p>
<? foreach ($modes as $k => $v) : ?>
	<? if (isset($_GET[$k])) : ?>
	<h2><?=$modes[$k]?></h2>
	<?
	$p = $params; $verb = 'GET';
	if ($k == 'app')
	{
		$url = '/app';
		$data = $facebook->api($url, $verb, $p);
		echo '<table border="1" cellpadding="2" cellspacing="0" class="framed stats">'."\n";
		foreach ($data as $k => $v)
			echo "<tr><th>$k</th><td>$v</td></tr>\n";
		echo '</table>';
		// nombre d'utilisateurs
		show_table("derniers jours", "select date(creation) `jour`, count(*) nombre from client_facebook where creation >= '".date('Y-m-d', strtotime('-1 week'))."'group by date(creation)");
		show_table("par an", "select year(creation) `ann�e`, count(*) nombre from client_facebook group by year(creation) with rollup");
		show_table("par mois", "select year(creation) `ann�e`, month(creation) mois, count(*) nombre from client_facebook group by year(creation), month(creation)");
	}
	else if ($k == 'insights')
	{
		$url = 'app/insights';
		echo '<pre>'.print_r($facebook->api($url, $verb, $p), true).'</pre>';
	}
	else if ($k == 'accounts')
	{
		$url = '/app/accounts/test-users';
		// ajouter un utilisateur
		if (count($_POST))
		{
			if ($user = $facebook->api($url, 'POST', array_merge($_POST, $p)))
			{
				echo '<p>Nouvel utilisateur cr��<ul>';
				echo '<li>User ID :'.$user['id'].'</li>';
				echo '<li>Email: '.$user['email'].'</li>';
				echo '<li>Password: '.$user['password'].'</li>';
				echo '<li><a href="'.$user['login_url'].'" target="_blank">se connecter</a></li>';
				echo '</ul></p>';
			}
		}
		// supprimer un utilisateur de test
		if ($_GET['delete'])
			$facebook->api($_GET['delete'], 'DELETE', $p);
		
		// afficher les utilisateurs
		$data = $facebook->api($url, $verb, $p);
		?>
		<table border="1" cellpadding="2" cellspacing="0" class="framed stats">
		<tr><th>Facebook ID</th><th>Application</th><th>Se connecter</th><th>Supprimer</th></tr>
		<? foreach($data['data'] as $account) : ?>
		<tr>
			<td><?=$account['id']?></td>
			<td><?=empty($account['access_token']) ? 'NON' : 'OUI'?></td>
			<td><a href="<?=$account['login_url']?>" target="_blank">se connecter</a></td>
			<td><a href="?accounts&delete=<?=$account['id']?>">supprimer</a></td>
		</tr>
		<? endforeach; ?>
		<tr>
			<td colspan="4">
				<form action="" method="post">
					<input type="hidden" name="installed" value="false"/>
					<input type="hidden" name="locale" value="fr_FR"/>
					<input type="hidden" name="permissions" value=""/>
					<fieldset><legend>Cr�er un utilisateur de test</legend>
						<p><label for="name">Nom utilisateur</label><input type="text" name="name" id="name" size="64" maxlength="64"/></p>
						<p><input type="submit" value="cr�er"/></p>
					</fieldset>
				</form>
			</td>
		</tr>
		</table>
		<?
	}
	?>
	<?endif; ?>
<? endforeach ?>
</body>
</html>
