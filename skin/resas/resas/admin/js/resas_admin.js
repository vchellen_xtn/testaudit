// options anytime
var oneDay = 24*60*60*1000;
var today = new Date();
today.setHours(0,0,0,0);
function getOptionsAnyTime(dateFormat, lblTitle){
	options	= {
		"format": dateFormat,
		"firstDOW":	1,
		"dayAbbreviations":	['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'],
		"dayNames":	['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
		"monthAbbreviations": ['Jan','F�v','Mar','Avr','Mai','Juin','Juil','Ao�t','Sept','Oct','Nov','D�c'],
		"labelTitle": lblTitle,
		"labelHour": "Heure",
		"labelYear": "Ann�e",
		"labelMonth": "Mois",
		"labelDayOfMonth": "Jour"
	};
	return options;
}

// url_control
function url_control(name, url)
{
	box = document.getElementById("div_" + name);
	if (!box) return;
	if (url != undefined && url.length)
	{
		html = url;
		html+= '&nbsp;&nbsp;<a href="javascript:void(0)" onclick="window.open(\'../../' + url + '\')">[voir]</a>';
		html+= '<a href="#" onclick="url_control(\'' + name + '\'); return false;">[remplacer]</a>';
		html+= '<input type="hidden" name="' + name + '" value="' + url + '">';
		html+='<br/><img src="../../'+ url + '"/>'
	}
	else
	{
		html = '<input type="file" name="' + name + '" value="">';
	}
	box.innerHTML = html;
}
