<?
	date_default_timezone_set('Europe/Paris');

	// IPs pour autorisations sp�ciales
	define('IP_RND', '109.190.104.215');
	define('IP_ADA', '194.177.53.10');
	define('IP_ADA2', '194.177.53.10,82 123 3 58'); // ADA et IP Gilles Franckhauser
	define('IP_SERENIS', '109.7.37.114,78.243.221.204'); // ,82.230.23.227

	define('IS_RND', in_array($_SERVER['REMOTE_ADDR'], array(IP_RND, '127.0.0.1', '::1')));
	define('IS_ADA', in_array($_SERVER['REMOTE_ADDR'], explode(',', IP_ADA2)));
	define('IS_SERENIS', in_array($_SERVER['REMOTE_ADDR'], explode(',', IP_SERENIS)));

	// on inclut un fichier de constante locale
	define('BP', dirname(__file__));
	@include (BP.'/cache/constant.local.php');

	// param�tres de l'application
	define('PROJECT_ID', 'ADA001');
	define('DEBUG', 0);
	define('PRIVATE_KEY', base64_encode($_SERVER['SERVER_NAME']));

	define('EMAIL_FROM', 'ADA');
	define('EMAIL_FROM_CANAL_TC', 'Central R�sa');
	define('EMAIL_FROM_ADDR', 'reservation@ada.fr');
	define('EMAIL_PAYPAL_DEBUG', 'avebole@rnd.fr');

	// sujet des e-mails
	define('SUBJECT_LOST_PWD', 'Votre mot de passe');
	define('SUBJECT_ADAFR_ACCOUNT', 'Votre compte client Ada.fr');
	define('SUBJECT_RESA_CLIENT', 'Votre reservation Internet');
	define('SUBJECT_RESA_CLIENT_ASSURANCE', 'Votre assurance complementaire');
	define('SUBJECT_RESA_CLIENT_MATERIEL', 'Votre mat�riel de d�m�nagement');
	define('SUBJECT_RESA_CLIENT_CANAL_TC', 'Votre r�servation pr�-pay�e');
	define('SUBJECT_RESA_CLIENT_ANNULATION', 'Annulation de votre reservation Internet');
	define('SUBJECT_RESA_CLIENT_ANNULATION_CANAL_TC', 'Annulation de votre reservation pr�-pay�e');
	define('SUBJECT_RESA_AGENCE', 'Reservation Internet');
	define('SUBJECT_RESA_AGENCE_CANAL_TC', 'R�servation pr�-pay�e');
	define('SUBJECT_RESA_AGENCE_ANNULATION', 'Annulation reservation Internet sans frais');
	define('SUBJECT_RESA_AGENCE_ANNULATION_CANAL_TC', 'Annulation r�servation pr�-pay�e sans frais');
	define('SUBJECT_RESA_AGENCE_ANNULATION_FRAIS', 'Annulation reservation Internet avec frais');
	define('SUBJECT_RESA_AGENCE_ANNULATION_FRAIS_CANAL_TC', 'Annulation r�servation pr�-pay�e avec frais');
	define('SUBJECT_RESA_TEL', 'Prospect Internet demande de rappel client');
	define('SUBJECT_CONTACT', 'Contact Internet');
	define('SUBJECT_PRO', 'Demande professionnelle');
	define('SUBJECT_PRO_CONFIRMATION','Votre inscription sur ADA.fr');
	define('SUBJECT_PRO_COMMERCIAL','Nouveau PRO');
	define('SUBJECT_NEWSLETTER', 'Newsletter ADA : votre inscription');
	define('SUBJECT_FRANCHISE', "Nouvelle demande d'affiliation");
	define('SUBJECT_CONTACT_VSP', 'Demande vehicule sans permis');
	define('SUBJECT_2ND_VOUCHER', 'Rappel: votre reservation ADA');
	define('SUBJECT_QUESTIONNAIRE', 'A votre retour de location, votre point de vue est essentiel');
	define('SUBJECT_PARRAINAGE', 'ADA, un site a decouvrir');

	// fichier des e-mails
	define('TPL_EMAIL_CONTACT_VSP', 'emails/em11.html');
	define('TPL_EMAIL_2ND_VOUCHER', 'em12');
	define('TPL_EMAIL_QUESTIONNAIRE', 'em15');
	define('TPL_EMAIL_PARRAINAGE',  'emails/em13.html');

	// Contenus des metas tags
	define('META_DESCRIPTION', 'Avec ADA, r�servez votre voiture et votre v�hicule utilitaire en ligne au meilleur prix.');
	define('META_KEYWORDS', 'ada,location,louer,reservation,reserver,voiture,utilitaire,auto,v�hicule,automobile,agence,france,corse,tourisme,vacances,service,soci�te,promo,promotion,international');

	// constantes
	$JOURS = array('1'=>'Lundi', '2'=>'Mardi', '3'=>'Mercredi', '4'=>'Jeudi', '5'=>'Vendredi', '6'=>'Samedi', '7'=>'Dimanche');
	$MOIS  = array('1'=>'janvier', '2'=>'f�vrier', '3'=>'mars', '4'=>'avril', '5'=>'mai', '6'=>'juin', '7'=>'juillet', '8'=>'ao�t', '9'=>'septembre', '10'=>'octobre', '11'=>'novembre', '12'=>'d�cembre');
	$CIVILITE = array('M' => 'Monsieur', 'Mme' => 'Madame', 'Mlle' => 'Mademoiselle');
	$EMPLACEMENT = array('1' => 'Ville', '2' => 'Gare', '3' => 'A�roport', '0' => 'Autre');
	$AGE = array('25' => '25 ans et +','23' => 'entre 23 et 25 ans', '21' => 'entre 21 et 23 ans','18' => 'moins de 21 ans');
	$PERMIS = array('5' => '5 ans et +', '1' => 'entre 1 et 5 ans', '0' => "moins d'1 an");
	$TYPE = array('VP' => 'Tourisme', 'VU' => 'Utilitaire', 'SP' => 'Sans permis', 'ML' => "A l'heure");

	// nom du serveur
	$host = $_SERVER['SERVER_NAME'] ? $_SERVER['SERVER_NAME'] : $_SERVER['HOSTNAME'];
	define('SERVER_NAME', $host);

	// Production
	if (preg_match('/^(www|ww2|location-vehicule|mobile|guadeloupe).ada.fr$/', $host))
	{
		define('SQL_HOST_R', 'localhost');
		define('SQL_HOST_W', SQL_HOST_R);
		define('SQL_USER', 'ada');
		define('SQL_DB', 'ada');
		define('SQL_PASSWORD', 'nlKQc7AZB0');
		
		// serverus
		define('SERVER_RESA', (substr($host, 0, 3) == 'ww2' ? 'ww2.ada.fr' : 'www.ada.fr'));
		define('SERVER_ANNU', 'location-vehicule.ada.fr');
		define('SERVER_MOBI', 'mobile.ada.fr');

		define('BASE_URL_360', 'http://360.ada.fr');

		/// configuration
		define('SITE_MODE', 'PROD');
		define('GOOGLE_API_KEY', '&key=AIzaSyC5Ik_645XeeEaK68OJjwMtCD5UGejjyew'); // V3 : https://code.google.com/apis/console/

		// Facebook [Test] ADA https://developers.facebook.com/apps/346633848764541/summary
		define('FB_APP_ID', '481961631847517');
		define('FB_APP_SECRET', '47947be33fea3eb1210a9a823b697ba8');
		define('FB_APP_TOKEN', '481961631847517|FvTPbYdP-avX4IJNQkPiauVrJ3Y'); // "https://graph.facebook.com/oauth/access_token?client_id=".FB_APP_ID."&client_secret=".FB_APP_SECRET."&grant_type=client_credentials"
	}
	else
	{
		// Int�gration
		if (preg_match('/^(dev|annu|gp|mob|mobi|pprod)\.ada\.lbn\.fr$/', $host) || $host == 'www.ada.fr.ada-wbdd01-pp.ada.lbn.fr')
		{
			define('SQL_HOST_R', 'localhost');
			define('SQL_HOST_W', 'localhost');
			if (preg_match('/^dev\.ada\.lbn\.fr$/', $host)) {
				define('SQL_USER', 'devada');
				define('SQL_DB', 'devada');
				define('SQL_PASSWORD', 'X5KhmCMbvQ');

				define('SERVER_RESA','dev.ada.lbn.fr');
			} else {
				define('SQL_DB', 'pprod_ada');
				define('SQL_USER', 'pprod_ada');
				define('SQL_PASSWORD', 'VtNikcLlDE');
			}

			// serveurs
			define('SERVER_RESA','pprod.ada.lbn.fr');
			define('SERVER_ANNU', 'annu.ada.lbn.fr');
			define('SERVER_MOBI', 'mobi.ada.lbn.fr');

			define('BASE_URL_360', 'http://360.ada.lbn.fr');

			// configuration
			define('SITE_MODE', 'TEST');
			define('GOOGLE_API_KEY', '&key=AIzaSyC5Ik_645XeeEaK68OJjwMtCD5UGejjyew'); // V3 : https://code.google.com/apis/console/
		}
		// dev (local)
		else if (preg_match('/^[a-z0-9]+$/', $host) || $host='ada.camille' || $host='adabr.camille')
		{
			if (preg_match('/camille$/', $host)) {
				define('SQL_HOST_R', 'localhost');
				define('SQL_PASSWORD', '12345');
			}
			define('SQL_HOST_R', 'mysql.rnd.lan');
			define('SQL_HOST_W', SQL_HOST_R);
			define('SQL_USER', 'root');
			define('SQL_PASSWORD', '');
			define('SQL_DB', 'ada001');

			// serveurs
			define('SERVER_RESA', 'localhost');
			define('SERVER_ANNU', 'localhost');
			define('SERVER_MOBI', 'localhost');

			define('BASE_URL_360', 'http://localhost/Ada/ADA094-refonteparcours/html');

			// configuration
			define('SITE_MODE', 'DEV');
			define('GOOGLE_API_KEY', ''); // V3 : https://code.google.com/apis/console/

			// emails en local
			define('BCC_EMAILS_TO', 'alemoal@rnd.fr,avebole@rnd.fr,esudre@rnd.fr');
			define('EMAIL_CONTACT', BCC_EMAILS_TO);

			define('EMAIL_REPORT', 'avebole@rnd.fr');
			define('EMAIL_UNIPRO', 'avebole@rnd.fr');
			define('EMAIL_PAYBOX','avebole@rnd.fr');
			define('EMAIL_JEU_CONCOURS', 'alemoal@rnd.fr,avebole@rnd.fr');
		} else {
			die("Configuration pour http://$host/ non support�e");
		}

		// e-mails
		define('BCC_EMAILS_TO', 'avebole@rnd.fr,alemoal@rnd.fr,gfranckhauser@ada.fr');
		define('BCC_EMAILS_TO_ABANDONNISTES', BCC_EMAILS_TO);
		define('EMAIL_CONTACT', 'gfranckhauser@ada.fr');
		define('EMAIL_AGENCE_DEBUG', BCC_EMAILS_TO);
		define('EMAIL_PRO', EMAIL_CONTACT);
		define('EMAIL_MOYENNEDUREE', EMAIL_CONTACT);
		define('EMAIL_FRANCHISE', BCC_EMAILS_TO);
		define('EMAIL_VSP', 'fpauzie@ada.fr');
		define('EMAIL_JEU_CONCOURS', 'jeu-concours@ada.fr,alemoal@rnd.fr,avebole@rnd.fr');
		define('BCC_EMAIL_VSP', 'avebole@rnd.fr,alemoal@rnd.fr');
		define('EMAIL_PROSPECT_RESAS', BCC_EMAILS_TO);

		// Facebook [Test] ADA https://developers.facebook.com/apps/346633848764541/summary
		define('FB_APP_ID', '346633848764541');
		define('FB_APP_SECRET', 'e135ea99c3be9e3c772802921fb93a00');
		define('FB_APP_TOKEN', '346633848764541|RUlW0SZOBovaQwVsTH50jE8OGDI'); // "https://graph.facebook.com/oauth/access_token?client_id=".FB_APP_ID."&client_secret=".FB_APP_SECRET."&grant_type=client_credentials"
	}
	// error reporting
	if (SITE_MODE == 'PROD' && !IS_RND) {
		error_reporting(0);
		//ini_set('display_errors', 0);
	} else {
		error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);
		ini_set('display_errors', 1);
	}
	
	// e-mails de gestion
	if (SITE_MODE != 'DEV') {
		define('SQL_ADMIN_EMAIL', 'avebole@rnd.fr,wdauchy@rnd.fr');
	}
	define('BCC_EMAILS_TO', 'teleacteur-03@ada.fr');
	define('BCC_EMAILS_TO_CANAL_TC', BCC_EMAILS_TO);
	define('BCC_EMAILS_TO_ABANDONNISTES', 'teleacteur-03@ada.fr');
	define('EMAIL_CONTACT', 'serviceclient@ada.fr');
	define('EMAIL_RESA_TEL', 'reservation@ada.fr');
	define('EMAIL_PRO', 'commercial@ada.fr');
	define('EMAIL_MOYENNEDUREE', 'commercial@ada.fr');
	define('EMAIL_FRANCHISE', 'contact.franchise@ada.fr');
	define('EMAIL_VSP', 'reservation@ada.fr');
	define('EMAIL_JEU_CONCOURS', 'jeu-concours@ada.fr,alemoal@rnd.fr');
	define('BCC_EMAIL_VSP', 'reservation@ada.fr');
	define('EMAIL_PROSPECT_RESAS', 'serviceclient@ada.fr');
	define('EMAIL_BONPLAN', (SITE_MODE == 'PROD' ? 'webmaster@ada.fr' : BCC_EMAILS_TO));

	// e-mails de rapports
	define('EMAIL_REPORT', 'infoada@pragmatik.fr,webmaster@ada.fr,avebole@rnd.fr');
	define('EMAIL_UNIPRO', 'infoada@pragmatik.fr,webmaster@ada.fr,avebole@rnd.fr');
	define('EMAIL_PAYBOX', 'avebole@rnd.fr');

	// configuration d�pendant du SITE_MODE
	define('SQL_PROFILE', (false && (SITE_MODE=='DEV'|| (SITE_MODE=='TEST' && IS_RND))) );

	define('USE_MINIFIED', SITE_MODE!='DEV');
	define('USE_HTTPS', (SITE_MODE=='PROD' || (SITE_MODE=='TEST' && SERVER_NAME!=SERVER_MOBI)));
	define('PBX_COMPTE', SITE_MODE=='PROD' ? 'ADAPROD' : 'ADATEST');
	define('USE_PAYPAL', false); //SITE_MODE!='PROD');
	define('USE_PAYPAL_SANDBOX', SITE_MODE!='PROD');
	define('USE_1EURO', 60);
	define('USE_3DS', (in_array(SITE_MODE, array('PROD','TEST')) ? 500 : false));
	define('USE_1CLIC', true); //in_array(SITE_MODE, array('DEV','TEST')) );

	// tracking
	define('USE_ABTASTY', (true && (SITE_MODE=='PROD')));
	define('USE_WEB2ROI', (false && (SITE_MODE=='PROD')));

	// param�tres des r�servations
	define('NUIT_OFFERTE', false);
	define('FRANCHISE_MINUTES', 30);
	define('HEURE_DEBUT_NUIT', 18);
	define('HEURE_FIN_NUIT', 8);

	// Google Analytics
	define('GA_ACCOUNT', 'UA-36308004-1');

	// librairie sp�cifique
	include_once (BP.'/lib/lib.util.php');
	include_once (BP.'/lib/lib.sql.php');
	include_once (BP.'/lib.ada.php');
