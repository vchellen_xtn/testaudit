<?php
/**
* Export se basant sur ComptaClientys
* 
* @param string $zone
* @param string $reseau
* @param string $canal
* @param string $debut
* @param string $fin
*/
function clientys_export2($zone, $reseau, $canal, $debut, $fin, $journal = null)
{
	if ($journal)
		$journal = is_array($journal) ? $journal : array($journal);
	else
		$journal = array('ACH','OD','ANN');
	// requ�te SQL
	$sql = "SELECT cc.*";
	$sql.=" FROM compta_clientys cc\n";
	$sql.=" JOIN reservation r ON r.id=cc.reservation\n";
	$sql.=" JOIN agence a ON a.id=r.agence\n";
	$sql.=" WHERE cc.DATECOMPTABLE BETWEEN '$debut' AND '$fin'\n";
	$sql.="    AND cc.JOURNAL IN ('".join("','", $journal)."')\n";
	$sql.="    AND r.forfait_type!='LOCAPASS'\n";
	if ($canal) {
		$sql.="AND r.canal='$canal'\n";
	}
	if ($zone) {
		$sql.="    AND a.zone='$zone'\n";
	}
	if ($reseau == 'CITER') {
		$sql.="    AND a.reseau='$reseau'\n";
	} else if ($reseau) {
		// ADA ou POINTLOC
		$sql.="    AND a.reseau='ADA'";
		if ($reseau == 'POINTLOC') {
			$sql.=" AND RIGHT(a.id,1)='P'\n";
		} else {
			$sql.=" AND RIGHT(a.id,1)!='P'\n";
		}
	}
	$sql.=' ORDER BY cc.NUMEROPIECE, cc.id';
	
	$numero = 0; $k = null;
	$rs = sqlexec($sql);
	if ($cnt = mysql_num_rows($rs))
		echo "!\r\n";
	while ($x = new ComptaClientys(mysql_fetch_assoc($rs)))
	{
		if ($x->isEmpty()) break;

		// corriger le num�ro de pi�ce
		if ($k != $x['NUMEROPIECE'])
		{
			$k = $x['NUMEROPIECE'];
			$numero++;
		}
		$x['NUMEROPIECE'] = $numero;
		// sortir la ligne corespondante
		echo $x->getLine()."\r\n";
	}
	return $cnt;
}


