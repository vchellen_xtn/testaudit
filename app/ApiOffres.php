<?php

class ApiOffres extends Ada_Object
{

	/**
	*	Cr�e un acc�s � l'API
	**	@param array $request le contenu de $_REQUEST
	* 	@param string $ip IP de l'appelant
	**	@returns ApiOffres
	*/
	static public function factory($request, $ip)
	{
		$x = new ApiOffres();
		$x->setData('status', 'OK');
		$x->setData('errors', array());
		$x->setData('agence', null);
		$x->setData('offres', array());
		if (!isset($request['token']) || !ApiToken::isThatTokenValid($request['token'], $ip)) {
			$x->setData('status', 'KO');
		}
		return $x;
	}

	/** Renvoie la liste des offres pour la requ�te effectu�e
	* 	Les arguments sont :
			agence		code agence
			type		vp | vu
			depart	    DD-MM-YYYY
			retour		DD-MM-YYYY
			h_depart	HH:MM
			h_retour	HH:MM
		@param array $args
		@returns ApiOffres
	*/
	public function getOffres($args)
	{
		$a = array('age' => 25, 'permis' => 5);
		$fields = array('agence', 'type', 'depart', 'retour', 'h_depart', 'h_retour');
		$errors = array();
		foreach ($fields as $fld) {
			if (empty($args[$fld])) {
				$errors[] = "argument manquant $fld";
			} else {
				$a[$fld] = $args[$fld];
			}
		}
		$a['categorie'] = ($args['type'] == 'vp' ? 71 : 11);
		if (empty($errors) && $reservation = Reservation::create($a, $errors, $url)) {
			$categories = Categorie_Collection::factory('fr', $a['type']);
			$offres = array();
			// pour les zones Gualdeoupe et R�union on ne renvoie pas d'offre
			if (!in_array($reservation->getAgence()->getData('zone'), array('gp','re'))) {
				$offresTarifs = Offre_Collection::factory($reservation, null, null, -1);
				$offresTarifs->loadNegociations();
				foreach ($offresTarifs as $offre) {
					if (isset($offre['indispo'])) continue;
					if ($forfait = $offre->getForfait()) {
						$categorie = $categories[$offre['categorie']];
						$offres[] = array(
							'id' => $categorie['id'],
							'mnem' => $offre['mnem'],
							'prix' => $forfait->getPrix(),
							'nego' => $forfait->hasNego(),
						);
					}
				}
			}
			$this->setData('agence', $reservation->getAgence()->getData('nom'));
			$this->setData('offres', $offres);
			return $this;
		}
		$this->setData('errors', $errors);
	}
}