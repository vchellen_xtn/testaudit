<?
	require_once("secure.php"); 
	require_once("lib.php");

function visuel_moveup($page, $situation, $from, $to) {
	$sql = "UPDATE visuel_page SET position = position + 1";
	$sql.=" WHERE	page = '".$page."' and situation='".$situation."'";
	if ($from) $sql.=" AND position >= ".$from;
	if ($to) $sql.=" AND position < ".$to;
//	echo $sql;
	sqlexec($sql);
}
function visuel_movedown($page, $situation, $from, $to) {
	$sql = "UPDATE	visuel_page SET position = position - 1";
	$sql.=" WHERE	page = '".$page."' and situation='".$situation."'";
	if ($from) $sql.=" AND position > ".$from;
	if ($to) $sql.=" AND position <= ".$to;
//	echo $sql;
	sqlexec($sql);
}
function show_visuels($page, $situation) {
	$sql = "SELECT v.*, p.id pid, p.position FROM visuel_page p JOIN visuel v ON v.id=p.visuel WHERE p.page='".$page."' AND situation='".$situation."' ORDER BY position";
	$rs = sqlexec($sql);
	if (!mysql_num_rows($rs)) {
		echo '<p style="text-align: center">Aucun visuel associ�</p>';
		return;
	}
	echo '<table align="center">';
	while ($row = mysql_fetch_assoc($rs)) {
		echo '<tr>';
		// afficher le visuel
		echo '<td colspan="'.($situation=='top' || $situation=='bottom' ? '2' : '1').'">';
		echo '<'.(substr($row['src'],-3)=='swf' ? 'embed' : 'img').' src="../'.$row['src'].'" '.($situation=='top' ? 'height' : 'width').'="50">';
		if ($situation != 'top' && $situation!='bottom') echo '</td><td>'; else echo '<br>';
		// infomrations sur le fichi�
		echo '<a href="visuel.php?id='.$row['id'].'">#'.$row['id'].' - '.$row['libelle'].'</a>';
		echo '&nbsp;<strong>('.($row['publie'] ? 'publi�' : '<span style="color: red">non publi�</span>').')</strong>';
		echo '<br>';
		echo $row['src'].'<br>';
		echo $row['href'].'<br>';
		echo '<a href="visuel_page.php?delete=1&page='.urlencode($page).'&situation='.$situation.'&position='.$row['position'].'&id='.$row['pid'].'&visuel='.$row['id'].'">supprimer</a>';
		echo '</td>';
		echo '</tr>';
	}
	echo '</table>';
}

	// identifiant
	$id = getarg("id");
	$page = getarg('page');
	$situation = getarg('situation');
	$position = getarg('position');
	
	// suppression
	if ($id && getarg("delete")) 
	{
		$sql = "delete from visuel_page where id = '".$id."'";
		getsql($sql);
		unset($id);
		visuel_movedown($page, $situation, $position, null);
	}
	
	// modification, cr�ation
	if (getarg("update") || getarg("create"))
	{
		// avant d'enregistrer il faut faire la place dans les positions si n�cessaire
		$max = 1+getsql("SELECT COUNT(*) FROM visuel_page WHERE page='".$page."' AND situation='".$situation."'");
		if (!$position) $position = $max;
		if ($position < 1) $position = 1;
		if ($position > $max) $position = $max;
		visuel_moveup($page, $situation, $position, null);
		$_POST['position'] = $position;

		// enregistrer la page
		save_record("visuel_page", $_POST);
		$id = mysql_insert_id();
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script language="javascript" src="../scripts/check.js"></script>
	<script>
	<!-- //

	// doDelete
	function doDelete(frm)
	{
		return (confirm("Voulez-vous r�ellement supprimer cet �l�ment ?\nCette op�ration est irr�versible."));
	}
	
	// doSubmit
	function doSubmit(frm)
	{
		var err = "";
		if (frm.visuel.selectedIndex == 0)
			err += "Vous devez choisir un visuel!\n";
		if (frm.situation.selectedIndex == 0) 
			err += "Vous devez choisir une situation!\n";
		if (frm.position.value.length && !frm.position.value.match(/^\d$/))
			err += "Vous devez indiquer une valeur num�rique pour la position!\n";
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		return true;
	}
	function doPage (frm) {

		var err = "";
		var lst = document.getElementById('page');
		var txt = document.getElementById('new_page');
		if (lst.selectedIndex == 0 && !txt.value.replace(/(^\s*)|(\s*$)/g,'').length)
			err += "Vous devez pr�ciser la page\n";
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		return true;
	}
	function pageChange(lst) {
		var txt = document.getElementById('new_page');
		txt.disabled = (lst.selectedIndex != 0);
		if (!txt.disabled) 
			txt.focus();
		else
			lst.form.submit();
	}
	// -->
	</script>
</head>
<body>
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre">
	<img src="img/puce.gif" align="absmiddle" hspace="1">
<? 
	echo "Page";
?>
	</th>
</tr>
<tr>
	<td>
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="15%">
		<col width="32%">
		<col width="15%">
		<col width="32%">
		<tr>
			<td><label for="page">Page</label></td>
			<td colspan="3">
			<form action="<?=$_SERVER['SCRIPT_NAME']?>" method="post" onsubmit="doPage(this);">
<? 
				selectsql('page', 'select distinct page, page from visuel_page order by page', $page, array(' ', '(page)'), 'style="width:40%;" onchange="pageChange(this);"');
?>
				&nbsp;<label for="new_page">ou</label>&nbsp;
				<input type="text" name="page" id="new_page" style="width: 30%;" value="<?=$page?>"
				<? if (getsql("SELECT COUNT(*) FROM visuel_page WHERE page='".$page."'")) echo ' disabled '; ?>
				>
				<input type="submit" name="view" value="afficher">
			</form>
			</td>
		</tr>
<?
	// on affiche la page si elle est indiqu�e
if ($page) {
?>
<tr><th colspan="4" style="text-align: center;">Haut</th></tr>
<tr><td colspan="4"><? show_visuels ($page, 'top'); ?></td></tr>
<tr><th colspan="2" style="text-align: center;">Gauche</th><th colspan="2" style="text-align: center;">Droite</th></tr>
<tr>
	<td colspan="2"><? show_visuels ($page, 'left'); ?></td>
	<td colspan="2"><? show_visuels ($page, 'right'); ?></td>
</tr>
<tr><th colspan="4" style="text-align: center;">Bas</th></tr>
<tr><td colspan="4"><? show_visuels ($page, 'bottom'); ?></td></tr>
<form action="<?=$_SERVER['SCRIPT_NAME']?>" method="post" onsubmit="doSubmit(this);">
<input type="hidden" name="id" value="">
<input type="hidden" name="page" value="<?=$page?>">
<tr>
	<th colspan="4" style="text-align: center">Ajouter un visuel</th>
</tr>
<tr>
	<td><label for="visuel">Visuel</label></td>
	<td colspan="3"><? selectsql('visuel', "select id, CONCAT(src, ' - ', libelle) FROM visuel ORDER BY src", getarg('visuel'), '--'); ?></td>
</tr>
<tr>
	<td><label for="situation">Situation</label></td>
	<td><? selectarray('situation', array('top'=>'haut','left'=>'gauche', 'right'=>'droite','bottom'=>'bas'), null, '--'); ?>
	<td><label for="position">Position</label></td>
	<td><input type="text" maxlength="1" size="1" id="position" name="position">
</tr>
<tr>
	<td>
		<table width="100%">
		<tr>
			<td align="right">
				<input type="submit" name="<?=$id ? "update" : "create"?>" value="enregistrer" onclick="return doSubmit(this.form);">
			</td>
		</tr>
		</table>
	</td>
</tr>
</form>
<? } ?>
</table>
</body>
</html>

