<?
	include ('../constant.php');
	header("Content-Type: text/plain");
	
	// cr�ation des fichiers minimis�s
	foreach(Page::getToMinify() as $min_file => $files)
	{
		$x = "/* GENERATED - DO NOT EDIT ".basename($min_file)." : ".join(",", $files)." */\r\n";
		foreach($files as $file)
		{
			$x .= "/* ".basename($file)." */\r\n";
			$txt = file_get_contents(BP.'/'.$file);
			$x .= preg_match('/((\.css)|(min\.js))$/', $file) ? $txt : JSMin::minify($txt);
			$x .= "\r\n";
		}
		file_put_contents(BP.'/'.$min_file, $x);
		echo "$min_file\n";
	}
?>