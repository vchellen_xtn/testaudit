<?
/**
* Correspond aux tables facturation_ada et facturation_courtage
*/
class Facturation extends Ada_Object
{
	/** @var Reservation */
	protected $_reservation;
	/**
	* Cr�e un objet � partir d'un identifiant
	* 
	* @param string $id
	* @return Facturation
	*/
	public static function factory($id)
	{
		$x = new Facturation();
		if ($x->load($id)->isEmpty())
			return null;
		return $x;
	}
	// m�me objet our facturation_ada et facturation_courtage
	public function getId()
	{
		$id = $this->_data['facturation'];
		if ($this->_data['id'])
			$id .='_'.$this->_data['id'];
		return $id;
	}
	public function getTableName()
	{
		return 'facturation_'.$this->_data['facturation'];
	}
	public function getReservation()
	{
		if (!$this->_reservation && $this->hasData('reservation'))
			$this->_reservation = Reservation::factory($this->getData('reservation'));
		return $this->_reservation;
	}
	public function setReservation(Reservation $reservation)
	{
		if ($reservation && $reservation->getId()==$this->getData('reservation'))
			$this->_reservation = $reservation;
		return $this;
	}
	
	/**
	* Cr�e un avoir depuis uen facture
	* @returns Facturation
	*/
	public function createAvoir()
	{
		if ($this->_data['type'] != 'F') return;
		$data = $this->_data;
		$data['id'] = '';
		$data['creation'] = date('Y-m-d H:i:s');
		$data['type'] = 'A';
		return new Facturation($data);
	}
	public function getSociete() { return ($this->getData('facturation')=='courtage' ? 'ADA COURTAGE' : 'ADA'); }
	public function getType() { return ($this->getData('type')=='A' ? 'AVOIR' : 'FACTURE'); }
	public function getNumber()
	{
		return $this->getDate('ym').'I'.sprintf('%07d', $this->getData('id'));
	}
	public function getDate($format)
	{
		if (!$this->hasData('t_creation'))
			$this->setData('t_creation', strtotime($this->getData('creation')));
		return date($format, $this->getData('t_creation'));
	}
	public function getMontant()
	{
		return $this->getData('montant');
	}
	public function getMontantHT()
	{
		return round($this->getData('montant')/(1+$this->getTVARate()), 2);
	}
	public function getTVARate()
	{
		return Tva::getRate($this->getDate('U'));
	}
	public function getTVA()
	{
		return ($this->getMontant() - $this->getMontantHT());
	}
	public function getLink()
	{
		$url = 'client/';
		$url.= ($this->_data['type']=='F' ? 'facture' : 'avoir');
		if ($this->_data['facturation']!='ada')
			$url.='_'.$this->_data['facturation'];
		$url.='.html?'.$this->_reservation->getKeyParams();
		return $url;
	}
}
?>