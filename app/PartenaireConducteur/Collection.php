<?php
class PartenaireConducteur_Collection extends Ada_Collection {
	
	/**
	* Renvoie les conducteurs associ�es � un partenaire
	* 
	* @param Partenaire $partenaire
	* @return PartenaireConducteur_Collection
	*/
	static public function factory(Partenaire $partenaire)
	{
		$x = new PartenaireConducteur_Collection();
		if ($partenaire) {
			$sql = sprintf('select * from partenaire_conducteur where partenaire=%ld and actif=1 order by nom', $partenaire->getId());
			$x->loadFromSQL($sql);
		}
		return $x;
	}
}
?>
