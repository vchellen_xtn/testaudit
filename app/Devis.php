<?php

/**
 *
 * @author flasselin
 *
 */
class Devis extends Ada_Object
{

    protected $_reservation;

    /**
     *
     * It's called from the popin 'detail' on 'reservation/resultat.html'
     * on that bloc a field email can be posted on the api.php with method=devis GET parameters
     * the email is send by POST
     *
     * the frm_reservation_tarif is used to instanciate
     * the form frm_reservation_tarif is also POSTed
     *
     * @param unknown $frm_reservation_tarif
     * @msg   a status message send by reference to be used in API to be returned in json
     */
    public static function create($frm_reservation_tarif, &$msg)
    {
        $x = null;
        if (isset($frm_reservation_tarif['reservation']) && isset($frm_reservation_tarif['key']) && $frm_reservation_tarif['key'] == Page::getKey((int)$frm_reservation_tarif['reservation'])) {
			$reservation = Reservation::factory((int)$frm_reservation_tarif['reservation']);
        } else {
        	$reservation = Reservation::create($frm_reservation_tarif, $errors, $url);
		}
		if ($reservation) {
			$x = new Devis();
			$x->_reservation = $reservation;
        	$x->sendDevisByEmail(filter_var($frm_reservation_tarif['email'], FILTER_SANITIZE_EMAIL), $msg);
		}
        return $x;

    }
    /**
     * This method send a devis by email
     * @param string $email
     * @param string $msg
     */
    private function sendDevisByEmail($email, &$msg)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $msg = "Le format de l'email est incorrect";
        } else {
            if ($this->is_abuse($email)) {
                $msg = "Veuillez r&eacute;essayer plus tard.";
            } else {
                $data = $this->gatherDevisData();
                // pr�parer le mail
                if($this->_reservation->isLocapass) {
                    $html = load_and_parse(BP . '/emails/em-devis-locapass.html', $data);
                } else {
                    $html = load_and_parse(BP . '/emails/em-devis.html', $data);
                }
                $agence = $this->_reservation->getAgence();
                $bcc = BCC_EMAILS_TO;

                // envoyer le mail
                 if(send_mail($html, $email, 'Votre demande de devis', $agence['nom'], $agence['email'], '', $bcc)){
                    $msg = "Votre devis a &eacute;t&eacute; envoy&eacute; &agrave; " . $email . ".";
                } else {
                    $msg = "Un probl&egrave;me est survenu lors de l'envoie de votre devis. Merci de contacter votre agence.";
                }
                $this->_data = $this->_reservation->getData();
                unset($this->_data['id']);
                $this->_data['tarif'] = $this->_reservation->getPrixForfait();
                $this->_data['client_email'] = $email;
                $this->_data['ip'] = $_SERVER["REMOTE_ADDR"];
                $this->_data['option'] = implode(',', $this->_reservation->getResOptions()->getIds());
                $this->_data['prix_option'] = $this->_reservation->getPrixOptions();
                $this->_data['id']="";
                parent::save();
            }
        }
    }

    /**
     *  Dans le cas d'un agence pointLOC, le logo � afficher est celui de pointLOC
     *  La liste des options ne doit pas �tre affich�e
     * @return multitype:unknown string NULL multitype:
     */
    public function gatherDevisData()
    {
        $agence = $this->_reservation->getAgence();
        $categorie = $this->_reservation->getCategorie();
        $options = $this->_reservation->getResOptions();

        $vehicule = $categorie['mnem'] . ' ' . $categorie['nom'];
        if ($categorie['commentaire'])
            $vehicule .= ': <br>' . $categorie['commentaire'];
        if ($agence->isPointLoc()) {
            $url_logo= 'css/img/structure-2015/logo-pointloc.png';
            $agence_name= "Agence POINT LOC de ". $agence->getHtmlAddress(' ', 'nom');
        }
        else {
            $agence_name= "Agence ADA de ". $agence->getHtmlAddress(' ', 'nom');
            $url_logo= 'css/img/logo_ada.jpg';
            $list_options = $options->toHtmlEmail();
        }
            // remplir le tableau
        $data = array(
            'date_jour' => date('d/m/Y'),
            'civilite' => $CIVILITE[$client['titre']],
            // ,'nom' => $client['prenom'].' '.$client['nom']
            // ,'adresse' => $client['cp']
            'url_logo' => $url_logo,
            'debut' => $this->_reservation->getDebut(),
            'fin' => $this->_reservation->getFinAvecVerificationHoraires(),
            'jours' => $this->_reservation->getDuree(),
            'km' => $this->_reservation->getKm(),
            'vehicule_type' => strtolower($this->_reservation['type']),
            'vehicule' => $vehicule,
            'agence' => $agence->getHtmlAddress(' ', 'nom,adresse1,cp_ville'),
            'agence_name'	=> $agence_name,
            'agence_adress'	=> $agence->getHtmlAddress(' ', 'adresse1,cp_ville'),
            'agence_email' => $agence['email'],
            'agence_bal' => $agence->getBal('strong', null, '<br>'),
            'agence_url' => $agence->getURLAgence(),
            'commentaire_agence' => $agence['commentaire'],
            'commentaire_agence_localisation' => $agence['commentaire_localisation'],
            'commentaire_agence_offre' => $agence['commentaire_offre'],
            'agence_tel' => $agence['tel'],
            'tarif' => number_format($this->_reservation->getPrixForfait(), 2),
            'tarif_ht' => Tva::TTC2HT($this->_reservation->getPrixForfait()),
            'options' => number_format($options->getTotal(), 2),
            'options_ht' => number_format($this->_reservation->getPrixOptionsHT(), 2),
            'surcharge_row' => $this->_getHtmlSurcharge($this->_reservation),
            'total' => $this->_reservation->getPrixTotal(),
            'total_ht' => $this->_reservation->getPrixTotalHT(),
            'total_tva' => Tva::getRate()*100,
            'list_options' =>$list_options,
            'href_cgl' => $agence->getHrefCGL(),
            'resa_id' => $this->_reservation->getResIDClient()
        );
        if ($retrait = $options->getDateRetrait()) {
            if ($this->_reservation->getHoursBeforeStart() <= 48)
                $data['materiel_retrait'] = "Confirmation de votre commande de mat�riels sous 24h";
            else
                $data['materiel_retrait'] = "Le mat�riel de d�m�nagement sera � votre disposition dans votre agence � la date que vous avez s�lectionn� : le " . Util_Format::dateFR('l d F Y', strtotime($retrait)) . '.';
        }
        return $data;
    }
    
	/**
	* Renvoie le HTML associ�e � une surcharge
	*
	* @param Reservation $reservation
	* @returns string
	*/
	protected function _getHtmlSurcharge(Reservation $reservation)
	{
		if ((int)$reservation['surcharge'] > 0)
		{
			$html = '<tr>';
			$html.='<td width="180" height="22" style="font:bold 12px arial;"><img src="img/puce1.gif" alt=""><strong>'.$reservation->getAgence()->getSurchargeLabel().' :</strong></td>';
			$html.='<td width="144" align="right" style="font:bold 12px arial;">'.$reservation['surcharge'].' &euro; TTC</td>';
			$html.='</tr>';
			return $html;
		}
	}

    /**
     *
     * @return boolean
     */
    private function is_abuse($mail)
    {
        $request_nb_last_min = mysql_fetch_assoc(sqlexec("select count(id) AS nb from devis where client_email='" . $mail . "' AND creation <= NOW() AND creation > NOW() - interval 1 minute"));
        $request_nb_last_hour = mysql_fetch_assoc(sqlexec("select count(id) AS nb from devis where client_email='" . $mail . "' AND creation <= NOW() AND creation > NOW() - interval 1 hour"));
        $request_nb_last_day = mysql_fetch_assoc(sqlexec("select count(id) AS nb from devis where client_email='" . $mail . "' AND creation <= NOW()  AND creation > NOW() - interval 1 day"));
        if ($request_nb_last_min['nb'] > 4 || $request_nb_last_hour['nb'] > 10 || $request_nb_last_day['nb'] > 30) {
            return true;
        }
        return false;
    }
}
