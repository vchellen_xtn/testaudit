<?
class Page_Pro extends Page
{
	protected function prepareHead()
	{
		parent::prepareHead();

		$action = $this->getAction();
		if (in_array($action, array('inscription_societe','moyenne_duree', 'courte_duree','offre_hyundai')))
			$this->addJS('scripts/jquery.validate.min.js');
	}
	
	public function getUniversTitre() {
		return "Professionnels";
	}
	
	// ancienne page ada pro qui est maintenant sur pro/index
	public function ada_proAction()
	{
		self::redirect(self::getURL('pro/index.html', SERVER_RESA));
	}
	
	// formuliare de contact de pro/demande
	public function demandeAction()
	{
		if(!isset($_POST['valid_pro_x']) && !isset($_POST['valid_pro']))
			return;
		
		// verifie les eventuelles errreur de saisie
		$data = array('date' => date("d-m-Y � h:m:s"));
		foreach($_POST as $k => $v) 
		{
			if(!$v && !in_array($k, array('activite','loueur','besoin','fax')))
				$this->_addMessage('Champ obligatoire : '.$k);
			$data[$k] = htmlentities(strip_tags($v));
		}
		if ($this->hasMessages())
			return;
		$data['contrat_location'] = $data['contrat'];
		$data['description'] = $data['besoin'];
		// email contact Profesionnel
		$html = load_and_parse(BP.'/emails/em9.html', $data);
		send_mail ($html, EMAIL_PRO, SUBJECT_PRO, EMAIL_FROM, EMAIL_FROM_ADDR, '', BCC_EMAILS_TO);
		// renvoyer vers la page de confirmation
		self::redirect(self::getURL('pro/confirmation.html'));
	}
	
	public function inscriptionAction()
	{
		// origine : webpro ou webce, en GET ou dans le REFERER
		$origine = $_GET['origine'];
		if (!$origine && preg_match('/_(.+)\.html$/', $_SERVER['HTTP_REFERER'], $m))
			$_GET['origine'] = $origine = 'web'.$m[1];
		if (!Partenaire::$ORIGINES[$origine] || substr($origine,0,3)!='web')
			self::redirect(self::getURL('pro/index.html'));
		// cacher le lien haut de page
		$this->hideLinkTop();
		// ajouter la CSS skin/v2/reservation.css pour le formulaire d'inscription
		if ($this->isSkin('v2'))
			$this->addCSS('skin/v2/reservation/reservation.css');
	}
	
	// formulaire de description de la soci�t�
	public function inscription_societeAction()
	{
		// on v�rifie l'origine
		$this->inscriptionAction();
		
		/** @var Client on r�cup�re le client en v�rifiant son authentification, sinon le redriiger vers l'nscription */
		$client = Page::_checkKeyCustomer(null, 'inscription');
		$this->setData('client', $client);
		// si le client a d�j� un partenaire on le rediriger vers son compte
		if (/** @var  Partenaire */ $partenaire = $client->getPartenaire())
			self::redirect(self::getURL('client/compte_pro.html'));
		
		/* on essaie maintenant d'enregistrer les diff�rents �l�ments d'informations
			1. client : informations compl�mentaires
			2. partenaire : cr�ation
			3. partenaire_client : lien entre le partenaire et la fonction en enregistrant la fonction
		*/
		
		// si les informations sont pass�es sur le client
		$errors = array();
		if ($_POST['client'])
		{
			// on v�rifie les champs � valider et s'ils sont correct on 
			$fieldsClient = 'naissance_pays,naissance_lieu,identite_type,identite_numero';
			if ($client->validateData($_POST['client'], $errors, $fieldsClient))
			{
				foreach(explode(',', $fieldsClient) as $k)
					$client->setData($k, $_POST['client'][$k]);
				$client->save($fieldsClient);
			}
		}
		// si le formulaire n'est pas renseign� on s'arr�te l�
		if ($_POST['partenaire'])
		{
			//$_POST['partenaire']['nom'] = null;
			// OK... maintenant on cr�e le partenaire
			// ce qui va �galement cr�er le coupon, la relation avec le client et le r�glement
			$partenaire = Partenaire::create($client, $_POST['partenaire_client']['fonction'], $_GET['origine'], $_POST['partenaire'], $errors);
			if ($partenaire)
			{
				self::redirect(self::getURL('pro/confirmation.html'));
				return;
			}
			// sionon on cr�e le coupon pour ce partenaire
		}
		// s'il y a des erreurs on l'ajoute au message
		if ($errors)
			$this->_addMessage($errors);
	}
	
	/**
	* Appel�e pour confirmer et conna�tre le code de r�duction
	* 
	*/
	public function validationAction()
	{
		if ($_GET['key'])
		{
			list($couponID, $partenaireID) = self::_decryptData($_GET['key']);
			if ($couponID && $partenaireID)
			{
				if (is_numeric($couponID) && is_numeric($partenaireID))
				{
					$coupon = PartenaireCoupon::factory($couponID);
					if ($coupon['partenaire'] == $partenaireID)
					{
						$this->setData('coupon', $coupon);
						return;
					}
				}
			}
		}
		self::redirect(self::getURL('pro/index.html'));
	}
	
	// enregistrement dans prospect_pro
	public function moyenne_dureeAction()
	{
		return $this->_saveProspectPro();
	}
	public function courte_dureeAction()
	{
		return $this->_saveProspectPro();
	}
	public function offre_hyundaiAction()
	{
		self::redirect(self::getURL('pro/moyenne_duree.html'));
		exit();
		return $this->_saveProspectPro();
	}
	protected function _saveProspectPro()
	{
		if (!count($_POST)) return;
		
		// on essaie de cr�er un prospect moyenne dur�e
		$errors = array();
		$x = ProspectPro::create($this->getAction(), $_POST, $errors);
		if ($x)
		{
			$this->setData('is_form_valid', true);
			$this->setTrackPage('confirmation', true);
		}
		else if (count($errors))
			$this->_addMessage($errors);
	}
}
?>
