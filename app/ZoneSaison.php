<?

class ZoneSaison extends Ada_Object
{
	/**
	* Renovie si un jour / mois est dans une saison
	* 
	* @param int $jour
	* @param int $mois
	*/
	public function isIn($jour, $mois)
	{
		// on repr�sente la date sous la forme "28 mai" sous la forme 5.28
		$x = $mois + (0.01 * $jour);
		if (!$this->_data['debut'])
			$this->_data['debut'] = $this->_data['debut_mois'] + 0.01 * $this->_data['debut_jour'];
		$debut = $this->_data['debut'];
		if (!$this->_data['fin'])
			$this->_data['fin'] = $this->_data['fin_mois'] + 0.01 * $this->_data['fin_jour'];
		$fin = $this->_data['fin'];
		// tester la saison
		if ($debut <= $fin)
			return ($x >= $debut && $x <= $fin);
		// ou pour les saisons sur 2 ann�es... de 11 � 2 par exemple
		else if ($debut > $fin)
		{
			return ($x >= $debut || $x <= $fin);
		}
		return false;
	}
}
?>