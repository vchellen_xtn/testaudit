<?php

class PromotionCode extends Ada_Object
{
	const CRC_ENC_KEY = 'Quos vult perdere Jupiter dementat';
	const KEY_LENGTH = 10;
	const BASE_32 = '23456789ABCDEFGHJKLMNPQRSTUVWXYZ';



	// CRC16-CCITT validator
	static $CRC_TABLE = array(
		0x0,  0x1021,  0x2042,  0x3063,  0x4084,  0x50a5,  0x60c6,  0x70e7,
		0x8108,  0x9129,  0xa14a,  0xb16b,  0xc18c,  0xd1ad,  0xe1ce,  0xf1ef,
		0x1231,  0x210,  0x3273,  0x2252,  0x52b5,  0x4294,  0x72f7,  0x62d6,
		0x9339,  0x8318,  0xb37b,  0xa35a,  0xd3bd,  0xc39c,  0xf3ff,  0xe3de,
		0x2462,  0x3443,  0x420,  0x1401,  0x64e6,  0x74c7,  0x44a4,  0x5485,
		0xa56a,  0xb54b,  0x8528,  0x9509,  0xe5ee,  0xf5cf,  0xc5ac,  0xd58d,
		0x3653,  0x2672,  0x1611,  0x630,  0x76d7,  0x66f6,  0x5695,  0x46b4,
		0xb75b,  0xa77a,  0x9719,  0x8738,  0xf7df,  0xe7fe,  0xd79d,  0xc7bc,
		0x48c4,  0x58e5,  0x6886,  0x78a7,  0x840,  0x1861,  0x2802,  0x3823,
		0xc9cc,  0xd9ed,  0xe98e,  0xf9af,  0x8948,  0x9969,  0xa90a,  0xb92b,
		0x5af5,  0x4ad4,  0x7ab7,  0x6a96,  0x1a71,  0xa50,  0x3a33,  0x2a12,
		0xdbfd,  0xcbdc,  0xfbbf,  0xeb9e,  0x9b79,  0x8b58,  0xbb3b,  0xab1a,
		0x6ca6,  0x7c87,  0x4ce4,  0x5cc5,  0x2c22,  0x3c03,  0xc60,  0x1c41,
		0xedae,  0xfd8f,  0xcdec,  0xddcd,  0xad2a,  0xbd0b,  0x8d68,  0x9d49,
		0x7e97,  0x6eb6,  0x5ed5,  0x4ef4,  0x3e13,  0x2e32,  0x1e51,  0xe70,
		0xff9f,  0xefbe,  0xdfdd,  0xcffc,  0xbf1b,  0xaf3a,  0x9f59,  0x8f78,
		0x9188,  0x81a9,  0xb1ca,  0xa1eb,  0xd10c,  0xc12d,  0xf14e,  0xe16f,
		0x1080,  0xa1,  0x30c2,  0x20e3,  0x5004,  0x4025,  0x7046,  0x6067,
		0x83b9,  0x9398,  0xa3fb,  0xb3da,  0xc33d,  0xd31c,  0xe37f,  0xf35e,
		0x2b1,  0x1290,  0x22f3,  0x32d2,  0x4235,  0x5214,  0x6277,  0x7256,
		0xb5ea,  0xa5cb,  0x95a8,  0x8589,  0xf56e,  0xe54f,  0xd52c,  0xc50d,
		0x34e2,  0x24c3,  0x14a0,  0x481,  0x7466,  0x6447,  0x5424,  0x4405,
		0xa7db,  0xb7fa,  0x8799,  0x97b8,  0xe75f,  0xf77e,  0xc71d,  0xd73c,
		0x26d3,  0x36f2,  0x691,  0x16b0,  0x6657,  0x7676,  0x4615,  0x5634,
		0xd94c,  0xc96d,  0xf90e,  0xe92f,  0x99c8,  0x89e9,  0xb98a,  0xa9ab,
		0x5844,  0x4865,  0x7806,  0x6827,  0x18c0,  0x8e1,  0x3882,  0x28a3,
		0xcb7d,  0xdb5c,  0xeb3f,  0xfb1e,  0x8bf9,  0x9bd8,  0xabbb,  0xbb9a,
		0x4a75,  0x5a54,  0x6a37,  0x7a16,  0xaf1,  0x1ad0,  0x2ab3,  0x3a92,
		0xfd2e,  0xed0f,  0xdd6c,  0xcd4d,  0xbdaa,  0xad8b,  0x9de8,  0x8dc9,
		0x7c26,  0x6c07,  0x5c64,  0x4c45,  0x3ca2,  0x2c83,  0x1ce0,  0xcc1,
		0xef1f,  0xff3e,  0xcf5d,  0xdf7c,  0xaf9b,  0xbfba,  0x8fd9,  0x9ff8,
		0x6e17,  0x7e36,  0x4e55,  0x5e74,  0x2e93,  0x3eb2,  0xed1,  0x1ef0
	);

	/**
	* Renvoie un PromotionCode � partir d'un ID ou d'un CODE
	*
	* @param mixed $id (Reservation ou int)
	* @param bool $idIsCode v�rifie avant d'essayer de charger
	* @param string $msg message d'erreur
	* @returns PromotionCode
	*/
	static public function factory($id, $idIsCode = true, &$msg = null)
	{
		if (!$idIsCode)
		{
			if (!is_object($id))
				$id = (int) $id;
			else if (get_class($id)!='Reservation')
				return null;
		}
		else if (!self::checkCode($id))
		{
			// on essaie s'il s'agit d'une campagne UNIK
			$code = PromotionCampagne::createCodeFromUnik($id, $msg);
			if (!$code && !$msg)
				$msg = "Le code de r�duction n'est pas valide !";
			return $code;
		}
		$x = new PromotionCode();
		$x->load($id);
		if ($x->isEmpty())
		{
			$msg = "Ce code de r�duction n'existe pas !";
			return null;
		}
		return $x;
	}
	protected function _prepareSQL($id, $cols='*', $data=null)
	{
		$sql = "select g.*, p.debut campagne_debut, p.fin campagne_fin, p.hotline campagne_hotline, p.classe campagne_classe,";
		$sql.=" p.montant_min campagne_montant_min, p.montant_base campagne_montant_base,p.montant_base_options campagne_montant_base_options, p.forfait_type campagne_forfait_type,";
		$sql.=" p.unik campagne_unik, p.nom campagne_nom, p.slogan campagne_slogan, p.cible campagne_cible, p.franchise_montant";
		$sql.=" from promotion_code g join promotion_campagne p on p.id=g.campagne";
		$sql.=" where ";
		if (is_object($id))
			$sql.=" g.reservation=".$id->getId();
		else if (!is_numeric($id))
			$sql .=" g.code='".addslashes($id)."'";
		else
			$sql.=' g.id='.(int)$id;
		return $sql;
	}
	public function __toString()
	{
		return ($this->_data['campagne_unik'] ? $this->_data['campagne_unik'] : $this->_data['code']);
	}
	public function toString($format = null)
	{
		return $this->__toString();
	}

	/**
	* Renvoie le client associ�e au bon de r�duction
	* @returns Client
	*/
	public function getClient()
	{
		if ($this->_data['client'])
			return Client::factory($this->_data['client']);
		return null;
	}

	/**
	* Le code est-il valide pour cette r�servation ?
	*
	* @param Reservation $reservation
	* @param string $msg message d'erreur
	* @returns bool
	*/
	public function isValidFor(Reservation $reservation, &$msg, $isHotline = false)
	{
		$is_valid = false;
		/* r�cup�rer les dates de validit� */
		$forfaitType = $this->getData('campagne_forfait_type');
		$tDebut = $this->_getTime('campagne_debut');
		$tFin = $this->_getTime('campagne_fin');
		$tPaiement = $this->_getTime('paiement');
		$tDistribution = $this->_getTime('distribution');
		$t = strtotime(date('Y-m-d'));
		// si le code a �t� annul�, expir� ou d�j� utilis�
		if ($this->_data['annulation'])
			$msg = "Ce code de r�duction a �t� annul� !";
		else if (!$tDistribution)
			$msg = "Ce code de r�duction n'a pas encore �t� autoris� !";
		else if (strtotime('+1 year', $tDistribution) < $t)
			$msg = "Ce code est arriv� � expiration depuis le ".date('d/m/Y', strtotime('+1 year', $tDistribution)).'.';
		else if ($tPaiement)
			$msg = "Ce code de r�duction a d�j� �t� utilis� le ".date('d/m/Y', $tPaiement).'.';
		else if ($tDebut && $tDebut > $t)
			$msg = "Ce code de r�duction n'est pas encore valable ! Il le sera � partir du ".date('d/m/Y', $tDebut).".";
		else if ($tFin && $tFin < $t)
			$msg = "Ce code de r�duction n'est plus valable depuis le ".date('d/m/Y', $tFin).'.';
		else if ($forfaitType && $forfaitType != $reservation['forfait_type'])
		{
			if ($forfaitType == 'JEUNES')
				$msg = "Ce code de r�duction est valable uniquement sur ADA Jeunes";
			else
				$msg = "Ce code de r�duction n'est pas valable sur ADA Jeunes";
		}
		else if ($this->getData('campagne_hotline') && !IS_ADA && !IS_SERENIS && !IS_RND && !$isHotline)
			$msg = "Ce code de r�duction n'est utilisable qu'en appelant un t�l�-conseiller.";
		else if ($reservation->getCategorie()->getData('nopromo'))
			$msg = "Ce code de r�duction ne s'applique pas pour cette cat�gorie de v�hicules.";
		else
		{
			// on v�rifie le seuil d'application
			$montant_min = $this->getData('campagne_montant_min');
			if ($montant_min)
			{
				$montant = 0;
				$base = $this->getData('campagne_montant_base');
				if ($base == 'forfait') {
					$montant = $reservation->getPrixForfait();
				} else if ($base == 'total') {
					$montant = $reservation->getPrixTotal();
				} else if ($base == 'forfait_complementaires') {
					$montant = $reservation->getPrixForfait() + $reservation->getResOptions()->getTotal('ADA', 'L', 'complementaires');
				} else if ($base == 'materiel') {
					$montant = $reservation->getResOptions()->getTotal('MATERIEL', 'L');
				} else if ($base == 'demenagement') {
					$resOptions = $reservation->getResOptions();
					$montant = $resOptions->getTotal(null, null, 'matloc')+ $resOptions->getTotal(null, null, 'matach') + $resOptions->getTotal(null, null, 'kits');
				} else if ($base == 'options') {
					$campagneOptions = explode(',', $this->getData('campagne_montant_base_options'));
					$resOptions = $reservation->getResOptions();
					foreach($campagneOptions as $k) {
						if (isset($resOptions[$k]['prix'])) {
							$montant += $resOptions[$k]['prix'];
						}
					}
				}
			}
			// on v�rifie que des promotions s'appliquent
			if ($montant_min && $montant < $montant_min)
				$msg = "Le montant de votre r�servation ne remplit pas les conditions de l'offre.";
			else
				$is_valid = $this->applyPromotions($reservation, $msg);
		}
		// enregistrer le message et retourner
		$this->addCommentaire(sprintf("reservation #%ld, client #%ld, %s", $reservation['id'], $reservation['client_id'], $msg), true);
		return $is_valid;
	}
	protected function _getTime($name)
	{
		if (!$this->hasData('t_'.$name))
		{
			$t = $this->getData($name);
			if ($t) $t = strtotime($t);
			$this->setData('t_'.$name, $t);
		}
		return $this->getData('t_'.$name);
	}

	/**
	* Annulle un code de r�duction
	* @param string $login login de l'utilisateur
	* @returns PromotionCode
	*/
	public function doCancel($login)
	{
		if (!$this->_data['annulation'])
		{
			$this->_data['annulation'] = date('Y-m-d H:i:s');
			$this->addCommentaire("annulation par [$login]");
			$this->save('annulation,commentaire');
		}
		return $this;
	}

	/**
	* Ajoute un commentaire et l'enregistre optionnellement
	*
	* @param string $commentaire
	* @param bool $doSave
	* @returns PromotionCode
	*/
	public function addCommentaire($commentaire, $doSave = false)
	{
		if ($commentaire = filter_var($commentaire, FILTER_SANITIZE_STRING))
		{
			if ($this->_data['commentaire'])
				$this->_data['commentaire'] .= "\n";
			$this->_data['commentaire'] .= date('d/m/Y H:i').' '.$commentaire;
			if ($doSave)
				$this->save('commentaire');
		}
		return $this;
	}
	/**
	* Renvoie la date d'expiration format�e
	*
	* @param string $format
	* @return string
	*/
	public function getExpirationDate($format = 'd/m/Y')
	{
		$tExp = strtotime('+1 year', $this->_getTime('distribution'));
		if ($this->_data['campagne_fin'])
		{
			$tFin = $this->_getTime('campagne_fin');
			if ($tFin > 0 && $tFin < $tExp)
				$tExp = $tFin;
		}
		return date($format, $tExp);
	}

	/**
	* Obtenir la liste des promotions pouvant �tre associ�es � cette r�servation
	*
	* @param Reservation $reservation
	* @returns Promotion_Collection
	*/
	public function applyPromotions(Reservation $reservation, &$msg)
	{
		// on r�cup�re l'agence
		$agence = $reservation->getAgence();
		$categorie = $reservation->getCategorie();
		$age = $reservation['age'];
		$forfait_prefix = getsql(sprintf("select prefix from forfait2 where id='%s'", $reservation['forfait']));

		// pr�parer le SQL
		$sql = "select SQL_NO_CACHE p.id, p.montant, p.mode";
		$sql.=" from agence_promo_forfait p";
		$sql.=" where (p.classe=".(int) $this->getData('campagne_classe').")\n";
		$sql.="   and (p.agence is null or p.agence='".addslashes($reservation['pdv'])."')\n";
		$sql.= "  and (p.zone='".$agence['zone']."')\n";
		$sql.= "  and (p.reseau is null or p.reseau='".$agence['reseau']."')\n";
		$sql.= "  and (p.groupe is null or p.groupe='".$agence['groupe']."')\n";
		$sql.= "  and (p.code_societe is null or p.code_societe='".addslashes($agence['code_societe'])."')\n";
		$sql.= "  and (p.type is null or p.type='".$reservation['type']."')\n";
		$sql.= '  and (p.categories is null or FIND_IN_SET("'.$categorie['mnem'].'" , p.categories))'."\n";
		$sql.= "  and (p.forfait is null or p.forfait='".addslashes($forfait_prefix)."')\n";
		$sql.= "  and (p.jours_min is null or ".(int)$reservation['duree']." >= p.jours_min)\n";
		$sql.= "  and (p.jours_max is null or ".(int)$reservation['duree']." <= p.jours_max)\n";
		$sql.= "  and (p.km_min is null or ".(int)$reservation['km']." >= p.km_min)\n";
		$sql.= "  and (p.km_max is null or ".(int)$reservation['km']." <= p.km_max)\n";
		$sql.= "  and (p.debut is null or p.debut <= '".$reservation->getDebut('Y-m-d')."')\n";
		$sql.= "  and (p.fin is null or p.fin >= '".$reservation->getFin('Y-m-d')."')\n";
		$sql.= "  and (p.jour_mois_debut is null or p.jour_mois_debut <= ".$reservation->getDebut('j').")\n";
		$sql.= "  and (p.jour_mois_fin is null or p.jour_mois_fin >= ".$reservation->getDebut('j').")\n";
		$sql.= "  and (p.actif=1)\n";
		$sql.= "  and (p.depuis is null or p.depuis <= CURRENT_DATE)\n";
		$sql.= "  and (p.jusqua is null or p.jusqua >= CURRENT_DATE)\n";
		$sql.= "  and (p.jours_avant is null or p.jours_avant <= ".ceil( $reservation->getHoursBeforeStart() / 24).")\n";
		$sql.= "  and ($age BETWEEN IFNULL(p.age_min, 18) AND IFNULL(p.age_max, 120))\n";
		$sql.= "  and (p.montant < 0)\n";

		$promotions = sqlexec($sql);
		if (!$promotions || !mysql_num_rows($promotions))
		{
			$msg = "Ce code de r�duction n'est pas valable pour vos conditions de r�servation !";
			return false;
		}
		/*  sinon on parcourt les promotions pour trouver la meilleure
		**	sur la base du prix du forfait avec les r�ductions d�j� obtenues
		* 	sans tenir compte de la surcharge et des options
		 */
		$base = $reservation->getPrixTotal();
		$best = null;
		while($promo = mysql_fetch_assoc($promotions))
		{
			// 2014-08-27 : le montant de la promotion est maintenant n�gatif, on calcule ici une r�duction
			$x = (-1) * round($promo['mode']=='P' ? ($base * $promo['montant']) / 100 : $promo['montant']);
			if (!$best || $best['reduction'] < $x)
				$best = array('promotion'=>$promo['id'], 'reduction'=>$x);
		}
		// on enregistre la promotion ainsi s�lectionn�e
		$this->setData('reservation', $reservation->getId())
			->setData('promotion', $best['promotion'])
			->setData('reduction', min($base, $best['reduction']))
			->setData('reduction_max', $best['reduction'])
			->setData('validation', date('Y-m-d H:i:s'))
			->save('reservation,promotion,reduction,validation');
		return true;
	}
	public function doPayment()
	{
		$this->setData('paiement', date('Y-m-d H:i:s'))
			->save('paiement');
	}


	/**
	* Hex To Base32
	*
	* @param string $hexString hexadecimal string
	* @return string
	*/
	static public function HexToBase32($hexString)
	{
		return self::BinToBase32(self::HexToBin($hexString));
	}

	/**
	* Base number from an ID
	*
	* @string int $id
	* @return string
	*/
	static public function B32FromID($id)
	{
		return substr(self::HexToBase32(str_pad(dechex($id),2,'0', STR_PAD_LEFT)), -2);
	}

	/**
	* Hexa en Binaire
	*
	* @param string $hexStr
	* @returns string
	*/
	static public function HexToBin($hexStr)
	{
		$hexStr = strtolower($hexStr);
		for($i=0, $length = strlen($hexStr);$i < $length; $i++)
			$out .= str_pad(decbin(hexdec($hexStr{$i})), 4, '0', STR_PAD_LEFT);
		return $out;
	}

	/**
	* Binary to Base32
	*
	* @param string $binStr binary string
	* @returns base32 string
	*/
	static public function BinToBase32($binStr)
	{
		// on met des 0 devant la cha�ne
		$m = strlen($binStr) % 5;
		if ($m > 0)
			$binStr = str_repeat('0', 5-$m).$binStr;
		// transformer en Base32
		$out = '';
		foreach(str_split($binStr, 5) as $v)
			$out .= substr(self::BASE_32, bindec($v), 1);
		return $out;
	}


	/**
	* G�n�re des codes al�atoires
	*
	* @param string $batch Base32 de l'identifiant du BATCH
	* @param int $i num�ro de code
	*/
	static public function makeCode($batch, $i)
	{
		$s = $batch.date('d/m/Y H:i:s').str_pad($i, 7, '0', STR_PAD_LEFT);
		$s = substr(self::HexToBase32(md5($s)), 0, self::KEY_LENGTH);

		// calculer le CRC
		$crc = str_pad(dechex(self::getCRC16($batch.$s.CRC_ENC_KEY)), 4, '0', STR_PAD_LEFT);

		// formatage
		$x[] = $batch;
		$x[] = substr($s, 0, 3);
		$x[] = substr($s, 3, 3);
		$x[] = substr($s, 6, 4);
		$x[] = self::HexToBase32($crc);
		return join('-', $x);
	}

	static public function getCRC16 ($ptr)
	{
		$crc = 0x0000;
		$crc_table = self::$CRC_TABLE;
		for ($i = 0; $i < strlen($ptr); $i++)
			$crc =  $crc_table[(($crc>>8) ^ ord($ptr[$i]))] ^ (($crc<<8) & 0x00FFFF);
		return $crc;
	}
	/**
	* V�rifier la validit� d'un code avant de le chercher dans la base de donn�es
	*
	* @param string $code
	* @return bool
	*/
	static public function checkCode($code)
	{
		$a = explode('-', $code);
		$check = array_pop($a);
		if (!$check) return false;
		$crc = str_pad(dechex(self::getCRC16(join('', $a).CRC_ENC_KEY)), 4, '0', STR_PAD_LEFT);
		return ($check == self::HexToBase32($crc));
	}
};

?>
