<?php

class ApiLog extends Ada_Object
{
	/**
	* Enregistre une requ�te API
	* 
	* @param ApiAccess $access
	* @param mixed $args
	* @return ApiLog
	*/
	static public function create(ApiAccess $access, $args)
	{
		$x = new ApiLog();
		$x['login'] = $access['login'];
		$x['ip'] = $access['ip'];
		$x['origine'] = $access['origine'];
		foreach(array('localisation','agence','type','debut','fin') as $k)
			$x[$k] = $args[$k];
		$x['id'] = '';
		$x->save();
		return $x;
	}
}