<?php
class Page_Demenagement extends Page
{
	public function getUniversTitre() {
		return "Votre espace déménagement";
	}
	
	protected function _getCustomScripts()
	{
		$action = $this->getAction();
		$files = parent::_getCustomScripts();
		if (in_array($action, array('studio', '2-pieces','grand-volume')))
		{
			$files['js'][] = 'scripts/form_reservation.js';
			$files['js'][] = 'scripts/agence_liste.js';
			$files['js'][] = 'scripts/jquery.tagdragon.min.js';
		}
		return $files;
	}
	
	public function trouver_son_camionAction() {
		// 2014-01-12 : ne pas indexer cette page pour ne pas concurencer la page d'accueil sur "location utilitaire"
		self::noIndex(false);
	}
	
	
	public function reserverAction()
	{
		self::redirect(self::getURL('demenagement/trouver-son-camion.html'));
	}
}
?>
