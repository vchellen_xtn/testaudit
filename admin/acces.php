<?
/*
Edition de :
	login
	nom
	entite	RND|ADA
	statut	1 ou 0 (activ ou d�sactiv�)
	droits
		bitmask
			128		cr�ation et gestion des utilisateurs
			32		acc�s aux offres
			2		t�l�conseiller
			1		acc�s � l'administration
		r�les
			255		Administrateur
			2		T�l� Conseiller
			1		Gestionnaire
	password
*/

	require_once("secure.php"); 
	require_once("lib.php");
	include_once(BP.'/lib/lib.fips181.php');

	// v�rification des droits
	if (!$adminUser->hasRights(Acces::SB_ADMIN)) 
		die("Vous n'avez pas le droit d'acc�der � cette fonctionnalit�!");

	// identifiant
	$login = getarg("login");
	
	// suppression
	if ($login && getarg("delete")) 
	{
		$sql = "delete from acces_zone where acces='".$login."'";
		getsql($sql);
		$sql = "delete from acces where login = '".$login."'";
		getsql($sql);
		unset($login);
	}
	
	// modification
	if (getarg("update") || getarg("create")) {
		save_record("acces", $_POST);
		// modifier les zones associ�es
		$zones = "'".join("','", $_POST['zone'])."'";
		$sql = "DELETE FROM acces_zone WHERE acces='".$login."' AND zone NOT IN (".$zones.")";
		getsql($sql);
		$sql = "INSERT INTO acces_zone SELECT '".$login."', z.id, NOW(), NOW() FROM zone z LEFT JOIN acces_zone a ON (z.id=a.zone AND a.acces='".$login."') WHERE z.id IN (".$zones.") AND a.zone IS NULL";
		getsql($sql);

		// enregistrer le mot de passe si n�cessaire
		if( getarg("password") ) {
			$sql = "update acces set password = (CASE WHEN version()>='5.0' THEN old_password('".addslashes($_POST['password'])."') ELSE password('".addslashes($_POST['password'])."') END) where login='".$login."'";
			
			getsql($sql);
		}
	}
	if ($login)
	{
		$rs = sqlexec("select * from acces where login = '".$login."'");
		if (!$rs || !($row = mysql_fetch_assoc($rs)))
			die("Aucun enregistrement correspondant.");
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script language="javascript" src="../scripts/check.js"></script>
	<script>
	<!--

	// doDelete
	function doDelete(frm)
	{
		return (confirm("Voulez-vous r�ellement supprimer cet �l�ment ?\nCette op�ration est irr�versible."));
	}
	// enablePassword
	function enablePassword() {
		var frm = document.forms[0];
		frm.password.disabled = !frm.password.disabled;
		frm.rpwd.disabled = !frm.rpwd.disabled;
	}
	
	// doSubmit
	function doSubmit(frm)
	{
		var err = "";
		
		if(!frm.login.value.length)
			err += "Veuillez indiquer le login\n";
		if(!frm.nom.value.length)
			err += "Veuillez indiquer le nom\n";
		if(frm.password && !frm.password.disabled) {
			if (!frm.password.value.length)
				err += "Veuillez indiquer le mot de passe\n";
			if (frm.password.value != frm.rpwd.value)
				err += "Les 2 mots de passe doivent �tre identiques\n";
		}
		// on compte le nombre de zones coch�s, au mois une
		var cnt = 0;
		for (var i=0; i < frm.elements.length; i++ ) {
			var c = frm.elements[i];
			if (c.name =='zone[]' && c.checked) cnt++;
		}
		if (!cnt)
			err += "Vous devez au moins s�lectionner  une zone!\n";
		// s'il y a une erreur
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		return true;
	}
	
	//-->
	</script>
</head>

<body>

<form action="" method="post" onsubmit="return doSubmit(this)">
<input type="hidden" name="type" value="">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre">
	<img src="img/puce.gif" align="absmiddle" hspace="1">
<? 
	echo "acces";
?>
	</th>
</tr>

<tr>
	<td>
	<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="15%">
		<col width="32%">
		<col width="15%">
		<col width="33%">
		<tr>
			<td><label for="login">Login</label></td>
			<td>
<? 
	echo $login;
	echo '<input type="'.($login ? "hidden" : "text").'" name="login" id="login" value="'.$login.'">';
?>
			</td>
			<td><label for="entite">Entit�</label></td>
			<td>
				<?
				$entite = array('ADA'=>'ADA', 'RND'=>'RND','SERENIS'=>'SERENIS');
				selectarray("entite", $entite, $row["entite"]);
				?>
			</td>
		</tr>
		<tr>
			<td><label for="nom">Nom</label></td>
			<td colspan="3"><input type="text" name="nom" id="nom" value="<?=$row["nom"]?>"></td>
		</tr>
		<tr>
			<td><label for="droits">R�le</label></td>
			<td><? selectarray('droits', Acces::$ROLES, $row["droits"]); ?></td>
			<td>Statut</td>
			<td>
			<?
			$statut = array( 1=>"Activ�", 0=>"D�sactiv�");
			selectarray("statut", $statut, $row["statut"]);
			?>
			</td>
		</tr>
		<? if ($login) $disabled = ' disabled="disabled"'; ?>
		<tr>
			<td><label for="password">Mot de passe</label></td>
			<td><input type="password" id="password" name="password" value="" <?=$disabled?>></td>
			<td><label for="rpwd">Confirmation</label></td>
			<td><input type="password" id="rpwd" name="rpwd" value="" <?=$disabled?>></td>
		</tr>
		<tr>
			<td>Suggestion</td>
			<td><?php $a = generate_password(10); echo reset($a);?></td>
			<td colspan="2">
			<? if ($disabled) : ?>
				<a href="#" onclick="enablePassword();">modifier le mot de passe</a>
			<? endif; ?>
			</td>
		</tr>
		<tr>
			<td>Zones</td>
			<td colspan="3">
			<?
				$rs = sqlexec("SELECT z.id, z.nom, a.acces FROM zone z LEFT JOIN acces_zone a ON (a.zone=z.id AND a.acces='".$login."') ORDER BY z.nom");
				while ($az = mysql_fetch_assoc($rs) ) {
					echo '<div style="float: left; width: 33%; font-size: 0.9em;">';
					echo '<input type="checkbox" name="zone[]" id="z_'.$az['id'].'" value="'.$az['id'].'" style="width: auto;"';
					if ($az['acces']) echo ' checked="checked"';
					echo '>&nbsp;<label for="z_'.$az['id'].'">'.$az['nom'].'</label>';
					echo "</div>\n";
				}
			?>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%">
		<tr>
			<td align="right">
				<input type="submit" name="<?=$login ? "update" : "create"?>" value="enregistrer">
<?
	if ($login)
	{
?>
				&nbsp;
				<input type="submit" onclick="return doDelete(this.form);" name="delete" value="supprimer">
<?
	}
?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
</form>

</body>
</html>
