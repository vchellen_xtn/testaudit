<?
class ResNego extends Ada_Object
{
	/** @var PromotionNegociation */
	protected $_negociation;
	/** @var AgencePromoForfait */
	protected $_promotion;
	
	/**
	* Cr�e un ResNego � partir d'une r�servation
	* 
	* @param Reservation $reservation
	* @return ResNego
	*/
	static public function factory(Reservation $reservation)
	{
		$x = new ResNego();
		$x->load($reservation->getId());
		if ($x->isEmpty()) {
			$x->setData('reservation', $reservation->getId());
		}
		return $x;
	}
	public function _construct()
	{
		parent::_construct();
		$this->setIdFieldName('reservation');
	}
	static public function create(Reservation $reservation) {
		if ($reservation && $reservation['nego'] > 0 && $reservation->hasForfait()) {
			if ($forfait = $reservation->getForfait()) {
				if ($forfait['n_id'] && $forfait['n_promotion'])
				$data = array(
					'reservation'	=> $reservation->getId(),
					'negociation' => (int)$forfait['n_id'],
					'promotion' => (int)$forfait['n_promotion'],
					'steps' => (int)$forfait['n_step'],
					'seuil' => (int)$forfait['n_prix'],
				);
				$x = new ResNego($data);
				$x->save();
				return $x;
			}
		}
		return null;
	}
	
	/**
	* Renvoie la n�gociation associ�e
	* @returns PromotionNegociation
	*/
	public function getNegociation() {
		if (!$this->_negociation) {
			$this->_negociation = PromotionNegociation::factory($this->_data['negociation']);
		}
		return $this->_negociation;
	}
	/**
	* Renvoie la promotion associ�e
	* @returns AgencePromoForfait
	*/
	public function getPromotion() {
		if (!$this->_promotion) {
			$this->_promotion = AgencePromoForfait::factory($this->_data['promotion']);
		}
		return $this->_promotion;
	}


}
?>
