<?
include BP.'/app/Unipro/WSTKRPCEncodedService.php';
if (SITE_MODE == 'PROD')
{
	define('UNIPRO_WSDL_URL','http://sa01sn.ada.fr:25011/wsa/wsa1/wsdl?targetURI=urn:prgs:RPCEncoded');
	define('UNIPRO_LOGIN','RNDWAY');
	define('UNIPRO_PWD','RESACAR');
}
else
{
	define('UNIPRO_WSDL_URL','http://sa01cy.ada.fr:25011/wsa/wsa1/wsdl?targetURI=urn:prgs:RPCEncoded');
	define('UNIPRO_LOGIN','RNDWAY');
	define('UNIPRO_PWD','RESACAR');
}

/**
* Unirpo : permet d'acc�der � unipro
*/
class Unipro extends WSTKRPCEncodedService
{
	/** @var Unipro conserve une connexion Unipro */
	static private $_uniproConnexion = null;

	private $_sessionID; // identifiaction de sesion
	private $_lastError = null;
	private $_cp2zone = array('98000'=>'fr','20'=>'co','98'=>'nc','971'=>'gp','972'=>'mq','973'=>'gf','974'=>'re');

	static public function getSingleUnipro() {
		if (!self::$_uniproConnexion) {
			try {
				self::$_uniproConnexion = @new Unipro();
			}
			catch (Exception $e) { $e = (string) $e; }
		}
		return self::$_uniproConnexion;
	}

	function __construct()
	{
		parent::__construct(
			UNIPRO_WSDL_URL,
			array (
				'trace'					=> 0		// trace WSDL calls
				,'cache_wsdl'			=> 2 // cache in memory
				//,'compression'			=> 1
				,'connection_timeout'	=> 4
			)
		);
	}
	function __destruct ()
	{
		$this->logout();
	}
	public function getSessionID() { return $this->_sessionID;}
	public function getLastError() {
		$error = $this->_lastError;
		$this->_lastError = null;
		return $error;
	}
	/**
	* cr�er une session UNIPRO
	*
	*/
	public function login()
	{
		if (!$this->_sessionID)
		{
			try
			{
				$this->_sessionID = $this->loginUNIPRO(UNIPRO_LOGIN, UNIPRO_PWD);
			}
			catch (Exception $e)
			{
				$this->sendError ('login', null, str_replace(array(UNIPRO_LOGIN, UNIPRO_PWD), '*******', (string) $e));
				return false;
			}
		}
		return true;
	}
	public function logout()
	{
		if (!$this->_sessionID) return;
		try { $this->logoutUNIPRO($this->_sessionID); $this->_sessionID = null; } catch (Exception $e) { $this->sendError('logout', null, (string) $e); }
	}
	/**
	* envoie un emssage d'erreur
	*
	* @param string $action
	* @param arrray $reservation
	* @param string $msg
	*/
	private function sendError ($action, $reservation, $msg)
	{
		// pr�parer le message
		$a = array (
			 'serveur' => $_SERVER['SERVER_NAME']
			,'action' => $action
			,'reservation' => $reservation['id']
		);
		foreach (array('agence','statut','debut','fin','total') as $v)
			$a[$v] = $reservation[$v];
		if ($msg && !is_array($msg))
		{
			$this->_lastError = $msg;
			$msg = array('Message' => $msg);
		}
		$out = '';
		if (is_array($msg))
		{
			foreach ($msg as $k => $v) {
				$out.= '<tr>';
				$out.='<th>'.$k.'</th>';
				$out.='<td>'.(is_array($v) ? print_r($v, true) : nl2br($v)).'</td>';
				$out.="</tr>\n";
			}
		}
		$a['msg'] = $out;
		$html = load_and_parse(BP.'/emails/em-err-unipro.html', $a);
		$subject = '[ADA-UNIPRO-'.$_SERVER['SERVER_NAME']."] $action";
		if ($reservation['id'])
			$subject.= '#'.$reservation['id'];
		if (isset($reservation['agence']))
			$subject.='-'.$reservation['agence'];
		if (isset($msg['cErrorCode']))
			$subject.='-'.$msg['cErrorCode'];
		send_mail ($html, EMAIL_UNIPRO, $subject, EMAIL_FROM, EMAIL_FROM_ADDR, "", "");
	}
	/**
	* renvoie la liste des horaires
	* @return array tableau des horaires
	*/
	public function getHoraires()
	{
		if (!$this->login()) return;
		try
		{
			$reponse = $this->getHorairesStationsADA($this->_sessionID);
			if ($reponse['cErrorCode'] == 'W0000')
				return $reponse['listeHoraires'];
			$msg = utf8_decode($reponse['cErrorMessage']);
		}
		catch (Exception $e) { $msg = (string) $e; }
		$this->sendError ('horaires', null, $msg);
	}
	public function convertHoraire(&$row)
	{
		if ($row && is_object($row)) $row = get_object_vars($row);
		if (!$row || !is_array($row)) return null;
		// pour unipro : 1: dimnache -> 7: samedi
		// pour ADAFR : 1: lundi -> 7 : dimanche
		$jour = $row['cJourSemaine']-1;
		if (!$jour) $jour = 7;
		return array (
			 'agence'	=> $row['cCodeStation']
			,'jour'		=> $jour
			,'type'		=> $row['cTypeHoraire'] // G: G�n�ral, E: Exceptionnel, C: ferm�e
			,'ouverture'=> $row['cHeureDebut']
			,'fermeture'=> $row['cHeureFin']
		);
	}
	/**
	* renvoie la liste des agences
	*
	* @param mixed $search
	* @return mixed
	*/
	public function getAgences($search = null)
	{
		if (!$this->login()) return;
		if (!$search) $search = 'TOUTES';
		try
		{
			// obtenir les agences ADA
			$reponse = $this->getAgencesAdafr($this->_sessionID, $search);
			if ($reponse['cErrorCode'] == 'W0000')
				return $reponse['listeAgencesAdafr'];
			$msg = utf8_decode($reponse['cErrorMessage']);
		}
		catch (Exception $e) { $msg = (string) $e; }
		$this->sendError ('agences', null, $msg);
	}
	/**
	* convertit du format UNIPRO vers le format ADAFR
	* @param array $agence format UNIPRO
	* @return array au format ADAFR
	*/
	public function convertAgence(&$row)
	{
		if ($row && is_object($row)) $row = get_object_vars($row);
		if (!$row || !is_array($row)) return null;
		// pr�ciser le code pays
		$zone = strtolower($row['cCodePays']);
		if ($zone=='sui') $zone = 'ch';
		else if ($zone=='guy') $zone = 'gf';
		else if ($zone=='fr')
		{
			$cp = $row['cCodePostal'];
			if ($x = $this->_cp2zone[$cp]) $zone = $x;
			else if ($x = $this->_cp2zone[substr($cp, 0, 2)]) $zone = $x;
			else if ($x = $this->_cp2zone[substr($cp, 0, 3)]) $zone = $x;
		}
		// emplacement
		// cf $EMPLACEMENT dans constant.php
		$x = array('V' => 1,'G'=>2,'A'=>3);
		$emplacement = $x[$row['cTypeStation']];
		if (!$emplacement) $emplacement = 0;

		// renvoie un tableau ADAFR
		return array (
			 'id'	=> $row['cCodeAgence']
			,'zone'	=> $zone
			,'emplacement' => $emplacement
			,'nom'	=> utf8_decode($row['cNomAgence'])
			,'ville'=> utf8_decode($row['cNomVille'])
			,'cp'	=> $row['cCodePostal']
			,'adresse1' => utf8_decode($row['cAdresse1'])
			,'adresse2' => utf8_decode($row['cAdresse2'])
			,'tel'	=> $row['cTelephone']
			,'fax'	=> trim($row['cFax'],' .-')
			,'email'=> $row['cEmail']
			,'societe' => utf8_decode($row['cNomSociete'])
			,'rcs' => utf8_decode($row['cRcs'])
			,'code_base'=> strtoupper(utf8_decode($row['cCodeBase']))
			,'code_groupe' => ($row['cGroupepAgence'] ? strtoupper(utf8_decode($row['cGroupepAgence'])) : $row['cCodeAgence'])
			,'code_societe' => utf8_decode($row['cCodeSociete'])
			,'code_region'	=> $row['cCodeResponsableRegion']
			,'franchise'	=> utf8_decode($row['cResponsableLegal'])
		);
	}
	/**
	* Obtenir les jauges varialbes
	*
	* @param string $search
	* @return mixed
	*/
	public function getJauges($agence = null, $categorie = null)
	{
		if (!$this->login()) return;
		if (!$agence) $agence = 'TOUTES';
		if (!$categorie) $categorie = 'TOUTES';
		try
		{
			// obtenir les agences ADA
			$reponse = $this->getJaugesVariablesAdafr($this->_sessionID, $agence, $categorie);
			if ($reponse['cErrorCode'] == 'W0000')
				return $reponse['listeJaugesVariablesAdafr'];
			$msg = utf8_decode($reponse['cErrorMessage']);
		}
		catch (Exception $e) { $msg = (string) $e; }
		$this->sendError ('jauges', null, $msg);
	}
	public function convertJauge (&$row)
	{
		if ($row && is_object($row)) $row = get_object_vars($row);
		if (!$row || !is_array($row)) return null;
		// si la jauge vaut -1, on prend la valeur par d�faut
		if ($row['cValJauge'] == -1) return null;
		return array (
			 'agence'	=> $row['cCodeAgence']
			,'categorie'=> $row['cCategorie']
			,'debut'	=> euro_iso($row['cDateDebut']).' '.$row['cHeureDebut']
			,'fin'		=> euro_iso($row['cDateFin']).' '.$row['cHeureFin']
			,'nb'		=> $row['cValJauge']
		);
	}
	/**
	* renvoie les fermetures d'agence
	*
	* @param string $agence
	* @return array liste des fermetures
	*/
	public function getFermetures($agence = null)
	{
		if (!$this->login()) return;
		if (!$agence) $agence = 'TOUTES';
		try
		{
			// obtenir les agences ADA
			$reponse = $this->getFermetureCialeAgence($this->_sessionID, $agence);
			if ($reponse['cErrorCode'] == 'W0000')
				return $reponse['listeFermetureCialeAgence'];
			else if ($reponse['cErrorCode'] == 'W0091') // plus de fermetures dans la base
				return array();
			$msg = $reponse['cErrorCode'].' - '.utf8_decode($reponse['cErrorMessage']);
		}
		catch (Exception $e) { $msg = (string) $e; }
		$this->sendError ('fermetures', null, $msg);
	}
	public function convertFermeture(&$row)
	{
		if ($row && is_object($row)) $row = get_object_vars($row);
		if (!$row || !is_array($row)) return null;
		return array (
			 'agence'	=> $row['cCodeAgence']
			,'debut'	=> euro_iso($row['cDateDebut'])
			,'fin'		=> euro_iso($row['cDateFin'])
		);
	}

	/**
	*  sendReervation : informe unipro d'une nouvelle r�servation
	* @param Reservation $reservation r�servation
	* @param bool $reprise est ce une relance ?
	* @return bool succ�s ou �chec de l'envoi � UNIPRO
	*/
	public function sendReservation (Reservation $reservation, $reprise=false)
	{
		if (!$reservation) return false;
		if (!$reservation['id'] || !strchr('PA', $reservation['statut'])) return false;

		// on initialise la r�servation
		$resUnipro = ResUnipro::create($reservation);

		// cr�er la connexion si n�cessaire et obtenir les informations sur la cat�gorie si n�cessaire
		if (!$this->login()) return false;
		$client = $reservation->getClient();
		$conducteur = $reservation->getConducteur();
		$categorie = $reservation->getCategorie('id,mnem');

		// pour rattraper les exceptions SOAP
		try
		{
			/* pr�parer le champ "Remarks" : i4ch, reprise et liste des options */
			$remarks = array();
			$remarks[] = 'PKM.'.($client['pgkm'] ? 'O' : 'N');
			if ($reservation->hasData('numero_carte'))
				$remarks[] = 'NCB.'.$reservation['numero_carte'];
			if ($reservation['valid'])
				$remarks[] = 'VCB.'.$reservation['valid'];
			if ($reservation['agence'] != $reservation['pdv'])
				$remarks[] = 'PVT.'.utf8_encode(getsql("select nom from agence_pdv where id='".$reservation['pdv']."'"));
			if ($code = $reservation->getPromotionCode())
			{
				if ($code['franchise_montant'] > 0)
					$remarks[] = 'OPS.'.utf8_encode($code['campagne_nom']);
			}
			// un commentaire ?
			if ($commentaire = trim($reservation->getResInfo()->getData('commentaire')))
				$remarks[] = 'TXT.'.utf8_encode(str_replace(';',' ', $commentaire));
			// le v�hicule
			$immat = '';
			if ($unipro_vehicule = $reservation->getResInfo()->getData('unipro_vehicule')) {
				list($immat, $vehicule) = explode('_', $unipro_vehicule);
			}
			$typeClient = 'GP'; // GP = Grand Public, LO = LOCAPASS, WP = WEBPRO, CE = Comit� d�Entreprise, CO = Coupon, OE = Offre Jeunes, OS = origine t�l� conseillers, OV = origine agence (RESAS)
			if ($reservation['canal']=='TEL') {
				$typeClient = 'OS'; // OS = origine t�l� conseillers
			} else if ($reservation['forfait_type']=='JEUNES') {
				$typeClient = 'OE'; // OE = Offre Jeune
			} else if ($reservation['forfait_type']=='LOCAPASS') {
				$typeClient = 'LO'; // LO = Locapass
				$coupon = $reservation->getPartenaireCoupon();
			} else if ($reservation['coupon']) {
				$coupon = $reservation->getPartenaireCoupon();
				$x = $coupon['origine'];
				if ($x == 'webce') $x = 'ce';
				else if ($x == 'webpro') $x = 'wp';
				else $x = substr($x, 0, 2);
				$typeClient = strtoupper($x);
				if ($typeClient == 'LO') {
					// on ne peut pas laisser LOcapass dans ce cas, il a d�j� �t� g�r� au-dessus
					$typeClient = 'CO';
				}
			}
			// quelques v�rifications suppl�mentaires
			if ($conducteur['naissance'] == '0000-00-00')
				$conducteur['naissance'] = null;
			// on n'envoie qu'aux agences avec le statut ad�quat
			if (!preg_match('/^\+?[\d\-\.\s]+$/',$conducteur['tel']))
				$conducteur['tel'] = '1111111111';
			if (!preg_match('/^\+?[\d\-\.\s]+$/',$conducteur['gsm']))
				$conducteur['gsm'] = '1111111111';
			/* on envoie la r�servation � UNIPRO */
			// construire la liste des options $listOptions
			$type = strtolower($reservation['type']);
			foreach($reservation->getResOptions() as /** @var ResOption */ $o)
			{
				// on n'envoie pas � UNIPRO nfv[pu] si une autre assurance dommages est pr�sente
				if ($o['option'] == 'nf'.$type)
				{
					if ($reservation->hasOption('rf'.$type) || $reservation->hasOption('af'.$type))
						continue;
				}
				$option = new putReservationAdafrv4_lstPrestRow();
				$option->cCodePrestation = $o['option'];
				$option->cPrixPrestation = is_null($o['prix']) ? '0.00' : $o['prix'];
				if ($o['quantite'] > 0)
				{
					$option->cQuantitePrestation = $o['quantite'];
					$option->cPrixUnitairePrestation = (is_null($o['prix']) ? '0.00' : round($o['prix'] / $o['quantite'], 2));
				}
				if ($o['retrait'])
					$option->dateEnlevement = strtr(euro_iso(substr($o['retrait'], 0, 10)), '-', '/');
				// P  = Pay�e en Ligne, D = Demand�e en Ligne, O = Offerte
				$option->cTypePrestation = ($o['paiement']=='A' ? 'D' : ($o['prix'] > 0 ? 'P' : 'O'));
				$listOptions[$o['option']] = $option;
			}
			// Comme UNIPRO ne sait pas le g�rer, on nous demande de mutualiser les options afv[pu] et abv[pu] et en apv[pu]
			$k = strtolower($reservation['type']);
			if ($listOptions['af'.$k] && $listOptions['ab'.$k])
			{
				$option = new putReservationAdafrv4_lstPrestRow();
				$option->cCodePrestation = 'ap'.$k;
				$option->cPrixPrestation = $listOptions['af'.$k]->cPrixPrestation + $listOptions['ab'.$k]->cPrixPrestation;
				$option->cTypePrestation = $listOptions['af'.$k]->cTypePrestation; // P  = Pay�e en Ligne, D = Demand�e en Ligne, O = Offerte
				$listOptions[$option->cCodePrestation] = $option;
				unset($listOptions['af'.$k], $listOptions['ab'.$k]);
			}

			// et options sp�cifiques pour les statistiques UNIPRO
			if ($reservation['paiement_mode']!='LP')
			{
				$a = array();
				// le forfait jour doit tenir compte de la r�duction des codes grillables
				$a['forjour'] = $reservation->getPrixForfait();
				if ($code && !$code['franchise_montant'])
					$a['forjour'] = $a['forjour'] - $code['reduction'];
				if ($reservation['surcharge'])
				{
					$k = (getsql("select emplacement from agence_pdv where id='".addslashes($reservation['pdv'])."'") == Agence::EMPL_AERO) ? 'suraero' : 'surstat';
					$a[$k] = $reservation['surcharge'];
				}
				foreach($a as $k => $v)
				{
					$option = new putReservationAdafrv4_lstPrestRow();
					$option->cCodePrestation = $k;
					$option->cPrixPrestation = $v;
					$option->cTypePrestation = 'P'; // P  = Pay�e en Ligne, D = Demand�e en Ligne, O = Offerte
					$listOptions[$k] = $option;
				}
			}

			// nouveau WSDL pour LOCAPASS
			$x = $this->putReservationAdafrv4
			(
				$this->_sessionID,				// session
				$reservation['agence'],	// code agence
				strtr(euro_iso(substr($reservation['debut'], 0, 10)), '-', '/'), // date de d�part JJ/MM/AAAA
				substr($reservation['debut'], 11, 5),							 // heure de d�part HH:MM
				strtr(euro_iso(substr($reservation['fin'], 0, 10)), '-', '/'),	// date de retour JJ/MM/AAAA
				substr($reservation['fin'], 11, 5),								// heure de retour HH:MM
				$categorie['mnem'],		// cat�gorie du v�hicule
				($reservation->isLocapass() ? 0 : $reservation['km']),	// km inclus dans le forfait
				utf8_encode($conducteur['nom']),	// nom du conducteur
				utf8_encode($conducteur['prenom']), // pr�nom du conducteur
				($conducteur['titre']=='M' ? 'M' : 'F'),	// Sexe: F ou M (titre = M, Mme, Mlle)
				strtr(euro_iso($conducteur['naissance']),'-','/'), // date de naissance
				null,	// lueu de naissance (20 caract�res max)
				($reservation->getAgence()->hasStatut(Agence::STATUT_TEL_CLIENT) ? $conducteur['tel'] : str_repeat('1', 10)),		// num�ro de t�l�phone
				($reservation->getAgence()->hasStatut(Agence::STATUT_TEL_CLIENT) ? $conducteur['gsm'] : str_repeat('1', 10)),		// num�ro de t�l�phone portable
				utf8_encode($client['adresse']), // adresse 1
				$client['adresse2'] ? utf8_encode($client['adresse2']) : str_repeat(' ', 10),	// adresse 2
				utf8_encode($client['ville']),	// ville
				utf8_encode($client['cp']), // code postal
				strtoupper($client['pays']), // code pays
				$reservation->isLocapass() ? 0 : $reservation['total'], // $cMontant
				'EUR', //$cCodeDevise,
				str_repeat('0',15), // $cNumPermis, varchar(15)
				'B', // $cTypePermis, varchar(2)
				'01/01/1900', // $cDatePermis, jj/mm/aaaa
				'A DEFINIR', // $cLieuPermis, varchar(20)
				$conducteur['email'], //$cEmail : e-mail si client est dans Programme Kilom'�tre
				$reservation->getResIDClient(), // $cNumResaAdafr,
				(($reservation->isLocapass() && $coupon['locapass']) ? $coupon['locapass']: 'ADAFR'), // $cCodeClient : Si client en compte sinon ADAFR
				join(';', $remarks),	// Mention PVT
				$reprise ? 'O' : 'N',
				utf8_encode($coupon['partenaire_nom']),
				$typeClient,	// GP = Grand Public, OE = Offre Jeune, LO = LOCAPASS, WP = WEBPRO, CE = Comit� d�Entreprise, CO = Coupon, OS = origine t�l� conseillers, OV = origine agence (RESAS)
				$reservation['paiement_mode']=='LP' ? 'N' : 'O', // r�glement en ligne
				$listOptions,
				$reservation['duree'],
				'',	// $cOperationClient ADAFR, DEVIS (devis client � stocker), RESA (autre r�servation � stocker) ou PREPA (r�servation � pr�parer)
				'', // $cCodeTarif retourn� lors de l'appel � getTarifs()
				'', // $cCodeDuree retourn� lors de l'appel � getTarifs()
				''  // $immat // $cImmat : Si PREPA
			);

			// en cas de succ�s ou si la r�servation �tait d�j� enregistr�e
			if ($x['cErrorCode'] == 'W0000' ||  $x['cErrorCode']=='W0086')
			{
				$resUnipro->setData('unipro', $x['cReservationNr']);
				$resUnipro->setData('validation', date('Y-m-d H:i:s'));
				$resUnipro->save('unipro,validation');
				return true;
			}
			// si le code d'erreur est W0092 (Notification par email), on ferme l'agence � la r�servation
			if ($x['cErrorCode'] == 'W0092')
				getsql("UPDATE agence SET notification='email' WHERE id='".$reservation['agence']."'");

			// on stocke la requ�te et  le r�sultat
			foreach ($x as $k => $v)
			{
				if (is_array($x[$k]))
					$x[$k] = print_r($x[$k], true);
				$x[$k] = utf8_decode($x[$k]);
			}
			if ($listOptions)
			{
				foreach($listOptions as $k => $v)
					$x['Option '.$k] = utf8_decode(print_r($v, true));
			}
			LogUnipro::create($reservation['id'], 'put', $x['cErrorCode'], $x['cErrorMessage']);
			$this->sendError ('putReservation', $reservation, $x);
		}
		catch (Exception $e)
		{
			$x = array();
			if ($listOptions)
			{
				foreach($listOptions as $k => $v)
					$x['Option '.$k] = utf8_decode(print_r($v, true));
			}
			$x['Message'] = (string) $e;
			$this->sendError ('putReservation', $reservation, $x);
		}
		return false;
	}

	/**
	*  sendAgenceResa : informe LEA d'un nouveau devis
	* @param AgenceResa $resa r�servation
	* @param string $operation DEVIS, RESA ou PREPA
	* @return bool succ�s ou �chec de l'envoi � LEA
	*/
	public function sendAgenceResa (AgenceResa $resa, $operation)
	{
		if (!$resa) return false;
		if (!$resa['id'] || $resa['statut']=='P') return false;

		// cr�er la connexion si n�cessaire et obtenir les informations sur la cat�gorie si n�cessaire
		if (!$this->login()) return false;
		$client = $resa->getClient();
		$categorie = $resa->getCategorie();

		// pour rattraper les exceptions SOAP
		try
		{
			/* pr�parer le champ "Remarks" : i4ch, reprise et liste des options */
			$remarks = array();
			if ($resa['agence'] != $resa['pdv'])
				$remarks[] = 'PVT.'.utf8_encode(getsql("select nom from agence_pdv where id='".$resa['pdv']."'"));
			// un commentaire ?
			if ($commentaire = trim($resa['commentaire'])) {
				$remarks[] = 'TXT.'.utf8_encode(str_replace(';',' ', $commentaire));
			}
			$typeClient = 'OV'; // GP = Grand Public, LO = LOCAPASS, WP = WEBPRO, CE = Comit� d�Entreprise, CO = Coupon, OE = Offre Jeunes, OS = origine t�l� conseillers, OV = origine agence (RESAS)
			if (in_array($resa['canal'], array('ADM','TEL'))) {
				$typeClient = 'OS';
			}
			// quelques v�rifications suppl�mentaires
			if ($client['naissance'] == '0000-00-00')
				$client['naissance'] = null;
			// on n'envoie qu'aux agences avec le statut ad�quat
			if (!preg_match('/^\+?[\d\-\.\s]+$/',$client['tel']))
				$client['tel'] = '1111111111';
			if (!preg_match('/^\+?[\d\-\.\s]+$/',$client['gsm']))
				$client['gsm'] = '1111111111';
			/* on envoie la r�servation � UNIPRO */
			// construire la liste des options $listOptions
			$type = strtolower($resa['type']);
			$options = $resa->getResOptions();
			foreach($options as /** @var ResOption */ $o)
			{
				// on n'envoie pas � UNIPRO nfv[pu] si une autre assurance dommages est pr�sente
				if ($o['option'] == 'nf'.$type)
				{
					if ($options->hasOption('rf'.$type) || $options->hasOption('af'.$type))
						continue;
				}
				// le prix de l'option
				$prixOption = is_null($o['prix']) ? '0.00' : $o['prix'];
				if ($o['facturation']!='COURTAGE') {
					$prixOption = Tva::TTC2HT($prixOption);
				}
				// remplir la structure UNIPRO
				$option = new putReservationAdafrv4_lstPrestRow();
				$option->cCodePrestation = $o['option'];
				$option->cPrixPrestation = $prixOption;
				if ($o['quantite'] > 0)
				{
					$option->cQuantitePrestation = $o['quantite'];
					$option->cPrixUnitairePrestation = (is_null($o['prix']) ? '0.00' : round($prixOption / $o['quantite'], 2));
				}
				if ($o['retrait'])
					$option->dateEnlevement = strtr(euro_iso(substr($o['retrait'], 0, 10)), '-', '/');
				// P  = Pay�e en Ligne, D = Demand�e en Ligne, O = Offerte
				$option->cTypePrestation = 'D';
				$listOptions[$o['option']] = $option;
			}
			// Comme UNIPRO ne sait pas le g�rer, on nous demande de mutualiser les options afv[pu] et abv[pu] et en apv[pu]
			$k = strtolower($resa['type']);
			if ($listOptions['af'.$k] && $listOptions['ab'.$k])
			{
				$option = new putReservationAdafrv4_lstPrestRow();
				$option->cCodePrestation = 'ap'.$k;
				$option->cPrixPrestation = $listOptions['af'.$k]->cPrixPrestation + $listOptions['ab'.$k]->cPrixPrestation;
				$option->cTypePrestation = $listOptions['af'.$k]->cTypePrestation; // P  = Pay�e en Ligne, D = Demand�e en Ligne, O = Offerte
				$listOptions[$option->cCodePrestation] = $option;
				unset($listOptions['af'.$k], $listOptions['ab'.$k]);
			}

			// et options sp�cifiques pour les statistiques UNIPRO
			$a = array();
			$a['forjour'] = Tva::TTC2HT($resa['tarif']);
			if ($resa['surcharge'])
			{
				$k = (getsql("select emplacement from agence_pdv where id='".addslashes($resa['agence'])."'") == Agence::EMPL_AERO) ? 'suraero' : 'surstat';
				$a[$k] = Tva::TTC2HT($resa['surcharge']);
			}
			foreach($a as $k => $v)
			{
				$option = new putReservationAdafrv4_lstPrestRow();
				$option->cCodePrestation = $k;
				$option->cPrixPrestation = $v;
				$option->cTypePrestation = 'D'; // P  = Pay�e en Ligne, D = Demand�e en Ligne, O = Offerte
				$listOptions[$k] = $option;
			}
			// le v�hicule
			$immat = '';
			if ($resa['unipro_vehicule']) {
				list($immat, $vehicule) = explode('_', $resa['unipro_vehicule']);
			}
			// r�cup�rer le code client
			$cCodeClient='';
			if ($resa->isLocapass()) {
				$coupon = $resa->getPartenaireCoupon();
				if ($coupon['locapass']) {
					$typeClient = 'LO'; // pour LOCAPASS
					$cCodeClient = $coupon['locapass'];
				}
			} else if ($client['unipro']) {
				$cCodeClient=$client['unipro'];
			}

			// nouveau WSDL pour LOCAPASS
			$x = $this->putReservationAdafrv4
			(
				$this->_sessionID,	// session
				$resa['agence'],	// code agence
				strtr(euro_iso(substr($resa['debut'], 0, 10)), '-', '/'), // date de d�part JJ/MM/AAAA
				substr($resa['debut'], 11, 5),							 // heure de d�part HH:MM
				strtr(euro_iso(substr($resa['fin'], 0, 10)), '-', '/'),	// date de retour JJ/MM/AAAA
				substr($resa['fin'], 11, 5),								// heure de retour HH:MM
				$categorie['mnem'],		// cat�gorie du v�hicule
				$resa['km'],	// km inclus dans le forfait
				utf8_encode($client['nom']),	// nom du conducteur
				utf8_encode($client['prenom']), // pr�nom du conducteur
				($client['titre']=='M' ? 'M' : 'F'),	// Sexe: F ou M (titre = M, Mme, Mlle)
				strtr(euro_iso($client['naissance']),'-','/'), // date de naissance
				null,	// lueu de naissance (20 caract�res max)
				$client['tel'],		// num�ro de t�l�phone
				$client['gsm'],		// num�ro de t�l�phone portable
				utf8_encode($client['adresse']), // adresse 1
				$client['adresse2'] ? utf8_encode($client['adresse2']) : str_repeat(' ', 10),	// adresse 2
				utf8_encode($client['ville']),	// ville
				utf8_encode($client['cp']), // code postal
				strtoupper($client['pays']), // code pays
				$resa->getPrixTotalHT(), // $cMontant
				'EUR', //$cCodeDevise,
				str_repeat('0',15), // $cNumPermis, varchar(15)
				'B', // $cTypePermis, varchar(2)
				'01/01/1900', // $cDatePermis, jj/mm/aaaa
				'A DEFINIR', // $cLieuPermis, varchar(20)
				$client['email'], //$cEmail : e-mail si client est dans Programme Kilom'�tre
				$resa->getResIDClient(), // $cNumResaAdafr,
				$cCodeClient, // $cCodeClient : vaut codecoupon locapass, code client unipro si d�j� connu, vide sinon
				join(';', $remarks),	// Mention PVT
				'N',
				'',
				$typeClient,	// GP = Grand Public, OE = Offre Jeune, LO = LOCAPASS, WP = WEBPRO, CE = Comit� d�Entreprise, CO = Coupon, OS = origine t�l� conseillers, OV = origine agence (RESAS)
				'N', // r�glement en ligne
				$listOptions,
				$resa['duree'],
				$operation,	// $cOperationClient ADAFR, DEVIS (devis client � stocker), RESA (autre r�servation � stocker) ou PREPA (r�servation � pr�parer)
				'', // $cCodeTarif retourn� lors de l'appel � getTarifs()
				'', // $cCodeDuree retourn� lors de l'appel � getTarifs()
				'' // $cImmat : Si PREPA
			);

			// en cas de succ�s ou si la r�servation �tait d�j� enregistr�e
			if ($x['cErrorCode'] == 'W0000' ||  $x['cErrorCode']=='W0086')
			{
				$resa->setData('unipro', $x['cReservationNr']);
				$resa->setData('operation', $operation);
				$resa->setData('transmission', date('Y-m-d H:i:s'));
				$resa->setData('statut','P');
				$resa->save('unipro,operation,transmission,statut');
				return true;
			}
			// on stocke la requ�te et  le r�sultat
			foreach ($x as $k => $v)
			{
				if (is_array($x[$k]))
					$x[$k] = print_r($x[$k], true);
				$x[$k] = utf8_decode($x[$k]);
			}
			if ($listOptions)
			{
				foreach($listOptions as $k => $v)
					$x['Option '.$k] = utf8_decode(print_r($v, true));
			}
			$this->sendError ('sendResa', $resa, $x);
		}
		catch (Exception $e)
		{
			$x = array();
			if ($listOptions)
			{
				foreach($listOptions as $k => $v)
					$x['Option '.$k] = utf8_decode(print_r($v, true));
			}
			$x['Message'] = (string) $e;
			$this->sendError ('sendResa', $resa, $x);
		}
		return false;
	}


	/**
	* Annulation des options mat�riels
	*
	* @param Reservation $resUnipro
	* @praam float $montant : montant de la r�servation apr�s remboursmeent
	* @returns bool
	*/
	public function updateOptions(Reservation $reservation)
	{
		// r�cup�rer l'identifiant UNIPRO
		$resUnipro = ResUnipro::factory($reservation);
		$montant = round($reservation['total'] - $reservation['rembourse'], 2);
		$listOptions = array();
		$options = $reservation->getResOptions()->filter('MATERIEL');
		foreach($options->getIds() as $prestation)
		{
			$option = new updateOptionsAdafr_lstPrestRow();
			$option->cCodePrestation = $prestation;
			$listOptions[] = $option;
		}

		if (!$resUnipro) return false;
		// cr�ation de la connexion
		if (!$this->login()) return false;
		try
		{
			// on informe UNIPRO de l'annulation
			$x = $this->updateOptionsAdafr
			(
				$this->_sessionID,
				$resUnipro['unipro'],
				$montant,
				'EUR',
				$listOptions
			);

			// diff�rents cas d'erreur
			if ($x['cErrorCode'] == 'W0000') 	// succ�s
			{
				$resUnipro->setData('materiel', date('Y-m-d H:i:s'));
				$resUnipro->save('materiel');
				return true;
			}
			else
			{
				// on stocke la requ�te et  le r�sultat
				foreach ($x as $k => $v)
					$x[$k] = utf8_decode($x[$k]);
				$msg = sprintf("%s - %S", $x['cErrorCode'], $x['cErrorMessage']);
				LogUnipro::create($resUnipro['reservation'], 'update', $x['cErrorCode'], $x['cErrorMessage']);
				$this->sendError ('update', $reservation, $x);
			}
		}
		catch (Exception $e)
		{
			$this->sendError('update', $reservation, (string) $e);
		}
		return false;
	}
	/**
	* Met � jour la d�ate d'enl�vement des options
	*
	* @param ResUnipro $resUnipro
	* @param Reservation $reservation
	* @param string $msg
	* @returns bool
	*/
	public function getDateEnlevementOptions(ResUnipro $resUnipro, Reservation $reservation, &$msg)
	{
		// cr�ation de la connexion
		if (!$this->login()) return false;
		try
		{
			// on informe UNIPRO de l'annulation
			$x = $this->getDateEnlevementOptionsAdafr
			(
				$this->_sessionID,
				$resUnipro['unipro']
			);

			// diff�rents cas d'erreur
			if ($x['cErrorCode'] == 'W0000') 	// succ�s
			{
				foreach($x['lstPrestDemenagement'] as /** @var getDateEnlevementOptionsAdafr_lstPrestDemenagementRow */ $option)
				{
					if ($option->lEnleve)
					{
						$resUnipro->setData('materiel', $option->dateEnlevement)->save('materiel');
						break;
					}
				}
				return true;
			}
			else
			{
				// on stocke la requ�te et  le r�sultat
				foreach ($x as $k => $v) {
					if (is_string($v)) {
						$x[$k] = utf8_decode($x[$k]);
					}
				}
				$msg = sprintf("%s - %S", $x['cErrorCode'], $x['cErrorMessage']);
				LogUnipro::create($resUnipro['reservation'], 'getDate', $x['cErrorCode'], $x['cErrorMessage']);
				$this->sendError ('getDate', $reservation, $x);
			}
		}
		catch (Exception $e)
		{
			$this->sendError('getDate', $reservation, (string) $e);
		}
		return false;
	}
	/**
	* annule une r�servation
	* @param array $reservation la r�servation � annuler
	*/
	public function cancelReservation (Reservation $reservation)
	{
		// v�rification des arguements
		if (!$reservation['id'] || $reservation['statut']!='A') return false;

		// on recherche le num�ro d'enregistrement dans UNIPRO, si on ne le trouve pas on laisse la correction quotidienne r�gler le probl�me
		$resUnipro = ResUnipro::factory($reservation);
		if (!$resUnipro['unipro']) return false;
		if ($resUnipro['statut'] != $reservation['statut'])
		{
			$resUnipro->setData('statut', $reservation->getData('statut'))->save('statut');
		}

		// cr�ation de la connexion
		if (!$this->login()) return false;
		try
		{
			// on informe UNIPRO de l'annulation
			$x = $this->cancelReservationAdafrv2
			(
				$this->_sessionID,
				$resUnipro['unipro'],
				'0', // ($reservation['fraisdossier'] ? $reservation['fraisdossier'] : '0'),
				'EUR'
			);

			// diff�rents cas d'erreur
			if ($x['cErrorCode'] == 'W0000' 	// succ�s
				|| $x['cErrorCode'] == 'W0058'	// d�j� annul�e
				|| ($x['cErrorCode'] == 'W0057' // UNIPRO ne connait pas cette r�servation
					&& $reservation['debut'] //... et la date de d�part est d�pass�e
					&& time() > strtotime($reservation['debut'])
					)
				)
			{
				$resUnipro->setData('annulation', date('Y-m-d H:i:s'))->save('annulation');
				return true;
			}
			else
			{
				// on stocke la requ�te et  le r�sultat
				foreach ($x as $k => $v)
					$x[$k] = utf8_decode($x[$k]);
				LogUnipro::create($reservation['id'], 'cancel', $x['cErrorCode'], $x['cErrorMessage']);
				$this->sendError ('cancelReservation', $reservation, $x);
			}
		}
		catch (Exception $e)
		{
			$this->sendError('cancelReservation', $reservation, (string) $e);
		}
		return false;
	}

	/**
	* renvoie les tarifs
	*
	* @param string $agence
	* @return array liste des fermetures
	*/
	public function getTarifs($agence, $type, $catTarifs, $depart, $retour, $km, $tarification)
	{
		if (!$this->login()) return;
		try
		{
			// obtenir les agences ADA
			$reponse = $this->getTarifCd
			(
				$this->_sessionID,
				$agence,
				strtr(euro_iso(substr($depart, 0, 10)), '-', '/'), // date de d�part JJ/MM/AAAA
				substr($depart, 11, 5),							 // heure de d�part HH:MM
				strtr(euro_iso(substr($retour, 0, 10)), '-', '/'), // date de d�part JJ/MM/AAAA
				substr($retour, 11, 5),							 // heure de d�part HH:MM
				strtoupper($type),
				$catTarifs,
				$km,
				$tarification
			);
			if ($reponse['CodRetour'] == '1')
				return $reponse['lstTarifCd'];
			$msg = $reponse['CodRetour'].' - '.utf8_decode($reponse['LibelleRetour']);
		}
		catch (Exception $e) { $msg = (string) $e; }
		$data = array(
			'agence' => $agence,
			'statut' => $type.' : '.$catTarifs.' : '.$km.' : '.$tarification,
			'debut'  => $depart,
			'fin'    => $retour
		);
		$this->sendError ('tarifs', $data, $msg);
	}

	/**
	* Renvoie la disponibilti� des v�hicules
	*
	* @param string $agence
	* @param string $type
	* @param string $depart
	* @param string $retour
	* @return array liste des fermetures
	*/
	public function getDispos($agence, $type, $catTarifs, $depart, $retour)
	{
		if (!$this->login()) return;
		try
		{
			// obtenir les agences ADA
			$reponse = $this->getPlanningCdDispo
			(
				$this->_sessionID,
				$agence,
				strtoupper($type),
				$catTarifs,
				strtr(euro_iso(substr($depart, 0, 10)), '-', '/'), // date de d�part JJ/MM/AAAA
				substr($depart, 11, 5),							 // heure de d�part HH:MM
				strtr(euro_iso(substr($retour, 0, 10)), '-', '/'), // date de d�part JJ/MM/AAAA
				substr($retour, 11, 5),							 // heure de d�part HH:MM
				''//manque parametre detail
			);
			if ($reponse['CodRetour'] == '1')
				return array($reponse['WPLANNINGCD'], $reponse['WDISPOPARCS']);
			$msg = $reponse['CodRetour'].' - '.utf8_decode($reponse['LibelleRetour']);
		}
		catch (Exception $e) { $msg = (string) $e; }
		$data = array(
			'agence' => $agence,
			'statut' => $type.' : '.$catTarifs,
			'debut'  => $depart,
			'fin'    => $retour
		);
	//	$this->sendError ('dispos', $data, $msg);
	}

	/*
	 *
	 */
	/**
	 *  Verifier l'exitence d'un utilisateur
	 *
	* @param string $COD_Agence
	* @param string $Utilisateur
	* @param string $prenom
	* @param string $ville
	* @param string $telephone
	* @returns array
	*/
	public function searchUser($COD_Agence, $Utilisateur=NULL, $prenom=NULL, $ville=NULL, $telephone=NULL)
	{
		if (!$this->login()) return;
		try
		{
			// obtenir les agences ADA
			$reponse = $this->getClientsB1($this->_sessionID, $COD_Agence, $Utilisateur, $prenom, $ville, $telephone);
		 	if ($reponse['CodRetour'] == '1')
				return $reponse['listeClient'];
			$msg = utf8_decode($reponse['LibelleRetour']);
		}
		catch (Exception $e) { $msg = (string) $e; }
	}


	/**
	* affiche le contenu du tableau
	*
	* @param array $data tableau de donn�es
	*/
	public function debug (&$data)
	{
		if (!$data || !is_array($data) || !count($data)) return;
		echo '<table border="1" cellpadding="2" cellspacing="0">'."\n";
		foreach ($data as $i => $row)
		{
			$row = get_object_vars($row);
			if ($i == 0)
			{
				echo '<tr><th>Compteur</th><th>';
				echo join('</th><th>', array_keys($row));
				echo "</th></tr>\n";
			}
			echo '<tr><td>'.(string)(1+$i).'</td><td>';
			echo join('</td><td>', $row);
			echo "</td></tr>\n";
		}
		echo "</table>\n";
	}
}

?>
