<?
	include_once(BP.'/lib/lib.tcp.php');

	// -- constantes --
	// 2011-05-25 : on utilise la pr�-production de PAYBOX avec le compte ADA
	if (SITE_MODE != 'PROD')
	{
		define('PBX_SYSTEM_SERVERS', 'preprod-tpeweb.paybox.com');
		define('PBX_DIRECT_SERVERS', 'preprod-ppps.paybox.com');
		/* informations pour tester 3DS */
		if (USE_3DS)
		{
			define('PBX_SITE', '1999888'); 	// num�ro d�adh�rent fourni par la banque du commer�ant.
			define('PBX_RANG', '69'); 		// num�ro de rang du site fourni par la banque du commer�ant.(PAYXBOX DIRECT PLUS 44)
			define('PBX_IDENTIFIANT','200932363');
			define('PBX_CLE', '1999888I'); 	// cle (PAYXBOX DIRECT PLUS LMFENRJC) - 1999888I pour test
		}
	}
	
	/* serveurs de production */
	define('PBX_SYSTEM_SERVERS', 'tpeweb.paybox.com,tpeweb1.paybox.com');
	define('PBX_3DSMPI_SERVERS', PBX_SYSTEM_SERVERS);
	define('PBX_DIRECT_SERVERS', 'ppps.paybox.com,ppps1.paybox.com');
	
	/* urls */
	define('PBX_SYSTEM_URL', '/cgi/MYchoix_pagepaiement.cgi');
	define('PBX_DIRECT_URL', '/PPPS.php');
	define('PBX_3DSMPI_URL', '/cgi/RemoteMPI.cgi');
	
	
	define('PBX_VERSION', '00104'); 	// valeur num�rique de la version PPPS (valeur par d�faut 00103).(PAYXBOX DIRECT  PLUS 104)
	define('PBX_SITE', '4477405'); 	// num�ro d�adh�rent fourni par la banque du commer�ant.
	define('PBX_RANG', '01'); 		// num�ro de rang du site fourni par la banque du commer�ant.(PAYXBOX DIRECT PLUS 44)
	define('PBX_IDENTIFIANT','511329854'); /* VISA: 511329854, AMEX: 511330157 */
	define('PBX_CLE','J7pD9qT7fX'); 	// cle (PAYXBOX DIRECT PLUS LMFENRJC)

	define('PBX_DEVISE', '978'); 		// code devise de la monnaie utilis�e (978 pour l�euro).
	define('PBX_ACTIVITE', '024'); 		// (Indicateur de Commerce Electronique) demande par internet
	define('PBX_ARCHIVAGE', 'ADALOCATION'); // text appara�ssant sur le relev� de compte du client 12 caract�res
	define('PBX_DIFFERE', '002'); 		// nombre de jours d�attentes avant la mise � disposition de la transaction 3 chiffres
	
	// -- ex�cutables --
	$exec_dir = (strstr($_SERVER["SERVER_SOFTWARE"], "IIS") || strstr($_SERVER["SERVER_SOFTWARE"], "Win32"))? "\\scripts\\iis\\" : "/scripts/debian/";
	define('PAYBOX_MODULE', BP.$exec_dir.'modulev3.exe');

class Paybox
{
	// type de demande concernant la transaction : 
	const TYPE_AUTORISATION	= 1; // autorisation
	const TYPE_DEBIT		= 2; // d�bit
	const TYPE_PAIEMENT		= 3;// autorisation + d�bit
//	const TYPE_CREDIT		= 4; // Cr�dit
	const TYPE_ANNULATION	= 5; // annulation
//	const TYPE_VERIFICATION = 11; // V�rification de l'existence d'une transaction
//	const TYPE_DIRECT		= 12 ; // Transaction sans demande d'autorisation
	const TYPE_MODIFICATION	= 13; // Modification du montant d�une transaction.
	const TYPE_REMBOURSEMENT = 14; // Remboursement
//	const TYPE_CONSULTATION	= 17; // Consultation

	const TYPE_ABT_AUTORISATION = 51; // Autorisation seule sur un abonn�
	const TYPE_ABT_DEBIT		= 52; // D�bit sur un abonn�
	const TYPE_ABT_PAIEMENT		= 53; // Autorisation + D�bit sur un abonn�,
//	const TYPE_ABT_CREDIT 		= 54; // Cr�dit sur un abonn�
	const TYPE_ABT_ANNULATION	= 55; // Annulation d'une op�ration sur un abonn�
	const TYPE_ABT_INSCRIPTION	= 56; // Inscription nouvel abonn�
	const TYPE_ABT_MODIFICATION	= 57; // Modification abonn� existant
	const TYPE_ABT_SUPPRESSION	= 58; // Suppression abonn�


	/** Variables renvoy�es par PAYBOX sur /paybox_confirm.php
	* 	montant		M	le Montant de la transaction (pr�cis� dans PBX_TOTAL).
	* resid			R	votre R�f�rence commande (pr�cis�e dans PBX_CMD) : espace URL encod�
	* autorisation	A	le num�ro d'Autorisation (num�ro remis par le centre d'autorisation) : URL encod�
	* numappel		T	un identifiant de Transaction (num�ro d'appel s�quentiel PAYBOX SERVICES)
	* mode			P	le type de Paiement retenu (CARTE, �)
	* typecarte		C	le type de Carte retenu (CB, VISA, EUROCARD_MASTERCARD, AMEX, �)
	* numerotrans	S	le num�ro de la tranSaction (identifiant unique de la transaction)
	* codereponse	E	le code Erreur de la transaction ([voir page 23]),
	* sign			K	K : Signature sur les variables de l'URL ([voir page 23]) : URL encod�
	*/
	//const PBX_RETOUR = 'MONTANT:M;REFERENCE:R;AUTORISATION:A;NUMAPPEL:T;mode:P;TYPECARTE:C;NUMTRANS:S;CODEREPONSE:E;SIGN:K';
	const PBX_RETOUR = 'MONTANT:M;AUTORISATION:A;NUMAPPEL:T;mode:P;NUMTRANS:S;CODEREPONSE:E;SIGN:K';
	
	// erreurs
	static protected $_ERRORS = array(
			 '00000' => "Op�ration r�ussie!"
			,'00001' => "la connexion au centre d'autorisation a �chou�." // "Paiement refus� par le centre d�autorisation."
			,'00002' => "Une erreur de coh�rence est survenue."
			,'00003' => "Erreur Paybox."
			,'00004' => "Le num�ro de carte et/ou le cryptogramme visuel ne sont pas corrects." //"Num�ro de porteur invalide."
			,'00005' => "Num�ro de question invalide."
			,'00006' => "Site et/ou rang incorrect."
			,'00007' => "La date de fin de validit� n'est pas correcte."
			,'00008' => "La date de fin de validit� n'est pas correcte." // Date de fin de validit� incorrecte.
			,'00009' => "Type d�op�ration invalide."
			,'00010' => "Devise inconnue."
			,'00011' => "Montant incorrect."
			,'00012' => "R�f�rence commande invalide."
			,'00013' => "Cette version n�est plus soutenue."
			,'00014' => "Trame re�ue incoh�rente."
			,'00015' => "Erreur d�acc�s aux donn�es pr�c�demment r�f�renc�es."
			,'00016' => "Abonn� d�j� existant"
			,'00017' => 'Ces coordonn�es bancaires ont �t� supprim�es.' // Abonn� inexistant
			,'00018' => "Transaction non trouv�e."
			,'00020' => "Le cryptogramme n'est pas valide."
			,'00021' => "Carte non autoris�e."
			,'00097' => "La connexion au serveur de paiement n'a pas pu �tre �tablie." // Timeout de connexion atteint.
			,'00098' => "Erreur de connexion interne au serveur de paiement." // Erreur de connexion interne.
			,'00099' => "Incoh�rence entre la question et la r�ponse. Refaire une nouvelle tentative ult�rieurement."
			,'PB'  => array
			(
			// les messages commen�ant par _ seront filtr�s pour �viter les messages "infamants"...
					 '00102'  => "_ccontacter l'�metteur de carte."
					,'00104' => '_conserver la carte'
					,'00105' => '_ne pas honorer'
					,'00107' => '_conserver la carte, conditions sp�ciales.'
					,'00114'	=> "Le num�ro de carte n'est pas correct."
					,'00134' => '_suspicion de fraude.'
					,'00141' => '_carte perdue'
					,'00151' => '_provision insuffisante ou cr�dit d�pass�.'
					,'00154'	=> "La date de fin de validit� de votre carte est d�pass�e."
					,'00156'	=> "Le num�ro de carte n'est pas correct."
					,'00157' => '_transaction non permise � ce porteur.'
					,'00159' => '_suspicion de fraude'
					,'00175' => "_nombre d'essais code confidentiel d�pass�."
			)
		);
		
	/**
	* Enregistre un mouvement dans log_paybox
	* 
	* @param Reservation $reservation
	* @param array $a
	*/
	static public function logger(Reservation $reservation, $mode, $a)
	{
		$log = array('id' => '');
		$log['reservation'] = $reservation['id'];
		$log['statut'] = $reservation['statut'];
		$log['mode'] = $mode;
		$log['numerotrans'] = $a['NUMTRANS'];
		$log['numappel'] = $a['NUMAPPEL'];
		$log['code_reponse'] =  $a['CODEREPONSE'];
		$log['autorisation'] = $a['AUTORISATION'];
		$log['pays'] = $a['PAYS'];
		$log['typecarte'] = $a['TYPECARTE'];
		save_record('log_paybox', $log);
	}

	// paybox_msg
	static public function getMessage($err, $mode='PB', $skipInfamous=true)
	{
		$msg = self::$_ERRORS[$err];
		if (!$msg)
			$msg = self::$_ERRORS[$mode][$err];
		// on supprimer certains message g�n�ants pour le client...
		if ($msg && substr($msg,0,1) == '_')
			$msg = $skipInfamous ? null : substr($msg, 1);
		if (!$msg)
			$msg = "Une erreur s'est produite lors de la demande d'autorisation [$err].";
		return $msg;
	}

	// paybox_init
	static protected function _init($type, $reference, $montant)
	{
		$a = array(
			 'VERSION' => PBX_VERSION		// version de PayBox
			,'DATEQ' => date('dmYHis')		// date et heure d�envoi de la trame (jjmmaaaahhmmss)
			,'TYPE' => str_pad($type, 5, '0', STR_PAD_LEFT)	// type de demande concernant la transaction : 3 = autorisation + d�bit | 5 = annulation | 13 = Modification du montant d�une transaction.|| 14=Remboursement
			,'NUMQUESTION' => '000'.date('His').rand(0, 9)	// identifiant unique de la requ�te permettant d��viter les confusions au niveau des r�ponses
			,'SITE' => PBX_SITE				// num�ro d�adh�rent fourni par la banque du commer�ant.
			,'RANG' => PBX_RANG				// num�ro de rang du site fourni par la banque du commer�ant.
			,'CLE' => PBX_CLE				// cl� activ�e uniquement avec la version 00103
			,'IDENTIFIANT' => ''			// champ vide (r�serv�)
			,'MONTANT' => sprintf('%010s', round($montant * 100))	// montant en centimes de la transaction (sans virgule ni point).
			,'DEVISE' => PBX_DEVISE			// 978 = Euro
			,'REFERENCE' => $reference		// r�f�rence du commer�ant permettant d�identifier clairement la commande correspondant � la transaction.
			,'ACTIVITE' => PBX_ACTIVITE		// indicateur de commerce �lectronique (ECI) permettant de diff�rencier la provenance des diff�rents flux mon�tiques envoy�s
			,'ARCHIVAGE' => PBX_ARCHIVAGE	// r�f�rence d�archivage transmise aux banques des porteurs. Si elle est g�r�e par la banque concern�e, elle pourra alors appara�tre sur le relev� de compte du client lui permettant ainsi d�identifier plus facilement l�origine de son achat.
			,'DIFFERE' => PBX_DIFFERE		// 7
			,'NUMAPPEL' => ''
			,'NUMTRANS' => ''
		);
		return $a;
	}
	/**
	* Envoyer une requ�te PAYBOX DIRECT
	* 
	* @param array $a
	* @returns array
	*/
	static protected function _request ($a)
	{
		/// pour chaquer serveur Paybox
		$url = PBX_DIRECT_URL.'?'.http_build_query($a);
		
		// on enregistre la requ�te sans les informations sensibles
		$a['CLE'] = preg_replace('/./', 'X', $a['CLE']);;
		if ($a['PORTEUR'])
			$a['PORTEUR'] = substr($a['PORTEUR'], 0, 6).preg_replace('/\d/', 'X', substr(trim($a['PORTEUR'],'?'), 6));
		if (SITE_MODE == 'DEV') {
			$a['request'] = http_build_query($a);
		}
		if (preg_match('/^[^\d]+0+(\d+)[^\d]/', $a['REFERENCE'], $m))
			$a['reservation'] = $m[1];

		// on interroge PAYBOX
		foreach(explode(',', PBX_DIRECT_SERVERS) as $srvr)
		{
			// on enregistre avant l'envoi pour ce serveur
			$a['id'] = '';
			$a['server'] = $srvr;
			save_record('paybox', $a);

			// on envoie la requ�te en HTTPS
			$response = array();
			if ($ppps = @get_http_request('https://'.$srvr, $url, null, $cookie, 'POST'))
			{
				parse_str($ppps, $response);
				// enregistrer la r�ponse
				$b = $response;
				$b['id'] = $a['id'];
				if (SITE_MODE == 'DEV') {
					$b['response'] = $ppps;
				}
				unset($b['PORTEUR']);
				save_record('paybox', $b);
				
				// pour certains CODEREPONSE, on essaie sur l'autre serveur
				if (in_array($response['CODEREPONSE'], array('00001','00003','00097','00098')))
					continue;
				// sinon on erenvoie la r�ponse
				return $response;
			}
		}
		// si aucun des serveurs n'a r�pondu, il faut quand m�me renvoyer quelques chose
		if (!$ppps && !$response)
		{
			$response['CODEREPONSE'] = '00003';
			$response['COMMENTAIRE'] = 'Le service de paiement est actuellement indisponible.';
		}
		return $response;
	}

	// desabonne une carte pour le paiement 1 clic
	static public function desabonnement($refabonne, $porteur, $dateval, $cvv)
	{
		$a = self::_init(self::TYPE_ABT_SUPPRESSION, null, null);
		$a['REFABONNE'] = $refabonne;
		$a['PORTEUR'] = $porteur;
		$a['DATEVAL'] = $dateval;
		$a['CVV'] = $cvv;
		return self::_request($a);
	}
	
	// paybox_paiement
	static public function paiement($ref, $montant, $numcarte, $valid, $crypto, $type = self::TYPE_PAIEMENT, $ID3D = null, $refabonne = null)
	{
		// initialiser la requ�te � PayBox
		$a = self::_init($type, $ref, $montant);
		if ($refabonne)
			$a['REFABONNE'] = $refabonne;
		// un m�canisme de tests pour les messages d'erreurs PAYBOX
		if (SITE_MODE != 'PROD' && preg_match('/^0+$/', $numcarte))
		{
			$a['ERRORCODETEST'] = '00'.$crypto;
			$numcarte = '1111222233334444';
			$crypto = '123';
		}
		// puis les param�tres sp�cifiques
		$a['PAYS'] = '';
		$a['TYPECARTE'] = '';
		$a['PORTEUR'] = str_pad($numcarte, 19, '?');	// num�ro de carte du porteur (client) sans espace, cadr� � gauche et compl�t� avec des �?� � droite.
		$a['DATEVAL'] = $valid;						// date de fin de validit� de la carte porteur au format MMAA.
		$a['CVV'] = $crypto;							// cryptogramme visuel situ� au dos de la carte bancaire.
		if ($ID3D)
			$a['ID3D'] = $ID3D;
		// et faire la requ�te
		return self::_request($a);
	}
	
	// paybox_annulation
	static public function annulation($ref, $montant, $numappel, $numtrans)
	{
		// initialiser la requ�te � PayBox
		$a = self::_init(self::TYPE_ANNULATION, $ref, $montant);
		// puis les param�tres sp�cifiques
		$a['NUMAPPEL'] = str_pad($numappel, 10, '0', STR_PAD_LEFT);
		$a['NUMTRANS'] = str_pad($numtrans, 10, '0', STR_PAD_LEFT);
		// et faire la requ�te
		return self::_request($a);
	}

	// payback_changermontant
	static public function changer_montant($ref, $montant, $numappel, $numtrans)
	{
		// initialiser la requ�te � PayBox
		$a = self::_init(self::TYPE_MODIFICATION, $ref, $montant);
		// puis les param�tres sp�cifiques
		$a['NUMAPPEL'] = str_pad($numappel, 10, '0', STR_PAD_LEFT);
		$a['NUMTRANS'] = str_pad($numtrans, 10, '0', STR_PAD_LEFT);
		// et faire la requ�te
		return self::_request($a);
	}
	// payback_remboursement ($montant est le montant � rembourser)
	static public function remboursement($ref, $montant, $numappel, $numtrans)
	{
		// initialiser la requ�te � PayBox
		$a = self::_init(self::TYPE_REMBOURSEMENT, $ref, $montant);
		// puis les param�tres sp�cifiques
		$a['NUMAPPEL'] = str_pad($numappel, 10, '0', STR_PAD_LEFT);
		$a['NUMTRANS'] = str_pad($numtrans, 10, '0', STR_PAD_LEFT);
		// et faire la requ�te
		return self::_request($a);
	}
	
	static public function get1EuroForm(Reservation $reservation, $PBX_OUTPUT='C', $withData=true)
	{
		$keyParams = '?'.$reservation->getKeyParams();
		// informations g�n�rales
		$a = array (
			// interface
			 'PBX_MODE'			=> 4	// en ligne de commande
			,'PBX_OUTPUT'		=> $PBX_OUTPUT	// Mode de gestion de la page interm�diaire. Les valeurs possibles sont : A, B, C, D et E [voir page 25].
			/* PBX_TXT			Texte pouvant �tre affich� sur la page interm�diaire � la place du texte par d�faut [voir page 25].
			PBX_WAIT		D�lai d'affichage de la page interm�diaire. La valeur est en milli-secondes [voir page 25].
			PBX_BOUTPI		Intitul� du bouton de la page interm�diaire (� nul � pour la suppression de ce bouton) [voir page 25].
			PBX_BKGD		Fond d'�cran de la page interm�diaire (nom de couleur, code couleur ou image) [voir page 25].
			*/
			// compte Paybox
			,'PBX_SITE'			=> PBX_SITE
			,'PBX_RANG'			=> PBX_RANG // 99 ?
			,'PBX_IDENTIFIANT'	=> PBX_IDENTIFIANT	// mot de passe
			// configuration du paiement
			,'PBX_DEVISE'		=> PBX_DEVISE	// EURO
			,'PBX_DIFF'			=> 2	// 02	Nombre de jours de diff�r� (02)
			,'PBX_TYPEPAIEMENT'	=> 'UNEURO'	// Moyen de paiement � utiliser [voir page 31]
			,'PBX_TYPECARTE'	=> 'UNEURO' // Type de carte � utiliser [voir page 31]
			,'PBX_ARCHIVAGE'	=> PBX_ARCHIVAGE // R�f�rence transmise � votre banque au moment de la t�l�collecte. Elle devrait �tre unique et peut permettre � votre banque de vous fournir une information en cas de litige sur un paiement. (12 caract�res)
			/* PBX_ENTITE			R�f�rence num�rique d'une subdivision g�ographique, fonctionnelle, commerciale, �*/
			/* PBX_1EURO_CODEEXTERNE	Uniquement pour la solution de paiement � 1Euro.com � : offre promotionnelle externe */
			// retour vers le site
			,'PBX_EFFECTUE'		=> Page::getURL('reservation/validation.html'.$keyParams, SERVER_NAME, USE_HTTPS)	// Page de retour de Paybox vers votre site apr�s paiement accept� [voir page 17].
			,'PBX_REFUSE'		=> Page::getURL('reservation/paiement.html'.$keyParams, SERVER_NAME, USE_HTTPS)	// Page de retour de Paybox vers votre site apr�s paiement refus� [voir page 17].
			,'PBX_ANNULE'		=> Page::getURL('reservation/paiement.html'.$keyParams, SERVER_NAME, USE_HTTPS)	// Page de retour de Paybox vers votre site apr�s paiement annul� [voir page 17].
			/* PBX_ERREUR		URL de votre site permettant d'afficher la description des �ventuelles erreurs pouvant survenir � l'affichage de la page de paiement [voir page 27]. */
			,'PBX_REPONDRE_A'	=> Page::getURL('paybox_confirm.php?id='.$reservation->getId(), SERVER_NAME, false) //URL d'appel serveur � serveur apr�s chaque tentative de paiements [voir page 19]
			,'PBX_RETOUR'		=> self::PBX_RETOUR //Variables renvoy�es par Paybox (montant, r�f�rence commande, num�ro de transaction, num�ro d'abonnement et num�ro d'autorisation) [voir page 21].
		);
		/* PBX_PAYBOX		URL du serveur de paiement primaire de Paybox si diff�rente de celle par d�faut (IFRAME, MOBILE) */
		if (defined('PBX_SYSTEM_SERVERS'))
		{
			foreach(explode(',', PBX_SYSTEM_SERVERS) as $k => $v)
				$a[($k == 0 ? 'PBX_PAYBOX' : 'PBX_BACKUP'.$k)] = 'https://'.$v.PBX_SYSTEM_URL;;
		}
		// li�es � la r�servation
		$client = $reservation->getClient();
		$a['PBX_TOTAL'] = round(100 * $reservation->getPrixTotal()); //Montant total de l'achat en centimes sans virgule ni point.
		$a['PBX_CMD'] = $reservation->getResID().($withData ? 'T' : 'N').date('His'); // r�f�rence commande
		$a['PBX_PORTEUR'] = $client['email'];
		
		// informations pour 1EURO (cf p. 11) M#DUPONT#Lecourbe#BatimentA## 0102030405##0#0#12#
		$data = $client->toArray(explode(',', 'titre,nom,prenom,adresse,adresse2,adresse3,cp,ville,pays,tel,gsm'));
		$data['pays'] = strtoupper($data['pays']);
		// 12. Flag indiquant si l'internaute est connu du commer�ant (0 :Non connu, 1 :Connu),
		$data['connu'] = ($client->getReservations(null, 1, false)->isEmpty() ? 0 : 1);
		// 13. Flag indiquant si le commer�ant a d�j� eu des incidents de paiements avec cet internaute, 
		$data['incident'] = 0;
		// 14. Code action COFIDIS (valeur fig�e et fournie par COFIDIS)
		$actionCofidis = array('FR'=>'12','BE'=>'1EU007');
		$data['cofidis'] = $actionCofidis[$data['pays']];
		$data['externe'] = '';
		$data['reseau'] = '1EU';
		$data['parcours'] = 'WEB';
		$data['facturation'] = 'FRA';
		$data['personnalisation'] = '';
		$data['encoding'] = 'ISO-8859-1';
		if (false && $withData)
			$a['PBX_1EURO_DATA'] = join('#', $data).'#';

		// appeler le module PAYBOX
		$cmd = PAYBOX_MODULE;
		foreach ($a as $k => $v)
		{
			$debug .= $k."\t".strlen($v)."\t".$v."\n";
			$cmd .=' '.escapeshellarg("$k=$v");
		}
		$debug = $cmd."\n\n".$debug;
//		return $debug;
		$output = shell_exec($cmd);
		if ($PBX_OUTPUT=='A' || $PBX_OUTPUT=='E')
			$output = preg_replace('/^[^\<]+/','', $output);
		return $output;
	}
	/**
	* V�rifier la signature dans la query string
	* 
	* @param string $queryString
	* @returns bool
	*/
	static public function checkSignature()
	{
		// r�cup�ration de l'URL, de la Query String puis s�parer entre la signature et les autres arguments
		// on n'utilise pas $_SERVER[QUERY_STRING] car elle contient l'argument "page=xxx" que la signature ne conna�t pas
		$url = Page::getBrowserURI();
		$pos = strpos($url,'?');
		if ($pos === false) 
			return false;
		$qs = substr($url, 1+$pos);
		// recupere les variables non cod�es
		$pos = strrpos($qs, '&');
		if ($pos === false)
			return false;
		$data = substr($qs, 0, $pos);
		// puis la signature
		$pos = strpos($qs,'=',$pos);
		if ($pos === false)
			return false;
		$signature = substr($qs, 1 + $pos);
		$signature = base64_decode(urldecode($signature));

		// ouverture de la cl� publique Paybox
		$key = openssl_pkey_get_public(file_get_contents(BP.'/app/Paybox/pubkey.pem'));
		if (!$key) return false;
		// les diff�rentes valuers possibles � tester
		// la documentation et l'impl�mentation ne sont pas coh�rentes
		$a = array(
			$data, 
			urldecode($data),
			$data = preg_replace('/^id=\d+&/','', $data),
			str_replace(array('%20%e0%20ne%20pas%20','%20%e0%20','%20%c3%a0%20ne%20pas%20','%20%c3%a0%20'),array(' � ne pas ',' � ',' � ne pas ',' � '), $data),
			$data = urldecode($data)
		);

		foreach (array_reverse($a) as $value)
		{
			$t = openssl_verify($value, $signature, $key);
			if ($t > 0) return true;
		}
		return false;
	}

	/**
	* Envoie une erreur pour PAYBOX
	* 
	* @param string $description
	* @param array $a
	*/
	static public function sendError($msg, $a)
	{
		$subject = '[ADA-'.SITE_MODE.'] PAYBOX '.date('Y-m-d H:i').': '.$msg;
		$row['srvr'] = $_SERVER['SERVER_NAME'];
		$row['msg'] = $msg;
		foreach ($a as $k => $v)
			$args .= $k.': '.$v."\n";
		$row['args'] = $args;
		$row['url'] = Page::getBrowserURI();
		$html = load_and_parse('emails/em-err-paybox.html', $row);
		send_mail ($html, EMAIL_PAYBOX, $subject, EMAIL_FROM, EMAIL_FROM_ADDR, null, '');
	}
	
	static public function get3DSecureForm(Reservation $reservation, $numcarte, $valid, $cvv, $libelle_1clic)
	{
		// obtenir l'identification pour la r�servation
		if ($reservation['canal'] == 'TEL') {
			$url = 'resas/validation.html';
			$keyParams = '?reservation='.$reservation->getId();
		} else {
			$url = ($reservation['forfait_type']=='JEUNES' ? 'jeunes' : 'reservation').'/validation.html';
			$keyParams = '?'.$reservation->getKeyParams();
		}
		
		// pr�parer le tableau pour l'URL
		$a = array
		(
			'IdMerchant'	=> PBX_IDENTIFIANT,	// Num�rique 	Identifiant commer�ant PAYBOX
			'IdSession'		=> $reservation->getResIDClient().'_'.date('His'),	// Alphanum�rique, longueur 250 	Permet l'identification de la r�ponse sur les URLs de retour. Il ne doit pas �tre utilis� 2 fois cons�cutivement pour un m�me IdMerchant
			'Amount'		=> round(100 * $reservation->getPrixTotal()),	// Num�rique 	Montant de la transaction en plus petite unit� de monnaie (cent quant il s'agit de l'euro)
			'Currency'		=> PBX_DEVISE,		// Alphanum�rique, longueur 3 	Code ISO de la monnaie (978 pour l'euro)
			'CCNumber'		=> $numcarte,		// Alphanum�rique, longueur 19 	Num�ro de carte
			'CCExpDate' 	=> $valid, 			// Alphanum�rique, longueur 4 	Date d'expiration au format MMAA
			'CVVCode'		=> $cvv,			// Alphanum�rique, longueur 3 	Cryptogramme visuel (3 caract�res au dos de la carte)
			'URLRetour' 	=> Page::getURL($url.$keyParams, SERVER_NAME, USE_HTTPS), 	// Alphanum�rique, longueur 250 	(facultatif) URL de retour depuis le navigateur du client. Si l'URL n'est pas pr�sente, PAYBOX utilisera celle param�tr�e sur la fiche site du commerce (�quivalent � PBX_EFFECTUE)
			'URLHttpDirect' => Page::getURL('paybox_confirm.php?id='.$reservation->getId(), SERVER_NAME, false)	//Alphanum�rique, longueur 250 	(
		);
		/*
		*  dans le cas des codes grillables le prix peut �tre � z�ro 
		** on demande alors une autorisation pour 1 � et le montant 3D Secure doit �tre coh�rente
		*/
		if ($reservation->getPrixTotal() <= 0) {
			$a['Amount'] = 100;
		}
		// construire l'URL
		list($server, ) = explode(',', PBX_3DSMPI_SERVERS);
		$content = @get_http_request('https://'.$server, PBX_3DSMPI_URL.'?'.http_build_query($a), null, $cookie, 'POST');
		$a['reservation'] = $reservation->getId();
		if ($libelle_1clic)
			$a['libelle_1clic'] = $libelle_1clic;
		save_record('paybox_secure', $a);
		return $content;
	}
}
?>
