<?

class Page_Agences extends Page {
	protected $_breadcrumb = array();
	protected $_linksAround = array();
	protected $_linksZoom = array();
	
	
	protected function prepareHead()
	{
		parent::prepareHead();
		$action = $this->getAction();
		if ($action =='index')
			$this->addJS('scripts/jquery.tagdragon.min.js', false);
		else if ($action == 'liste' || ($action == 'livraison' && $this->isSkin('v2')))
			$this->addJS('scripts/agence_liste.js');
		if (($action == 'agence' || $action=='liste') && $this->isSkin('v2')) {
			// formulaire de r�servation
			$this->addJS('scripts/form_reservation.js');
		}
	}
	
	public function getUniversTitre() {
		switch (($this->getId())) {
			case 'agences/index':
				return "Agences de location de voiture et v�hicule utilitaire en France";
			case 'agences/liste':
				$agences = $this->getAgences();
				if ($agences->getLibelle())
					return 'Agences de location de voiture et utilitaire '.htmlspecialchars($agences->getLibelle()); // extraire nom d�partement
				else
					return 'Agences de location de voiture et utilitaire '.htmlspecialchars($agences->getSearch());
			case 'agences/agence':
				$agence = $this->getAgence();
				return 'Agence de location de voiture et utilitaire <span class="agence--nom">'.$agence['nom'].'</span>';
			case 'agences/livraison':
				return 'ADA service voiturier';
			case 'agences/malin':
				return "Votre location de v�hicule en 1 clic";
		}
	}
	
	/**
	* appel�e puor l'affichage de agences/agence
	* 
	*/
	public function agenceAction()
	{
		if ($_GET['id'] && !$_GET['agence'])
			$_GET['agence'] = $_GET['id'];
		// on essaie de charger l'agence
		$agence = Agence::factory($_GET['agence'], true);
		// si l'agence existe on la conserve, sinon on ne veut pas que la page soit index�
		if (!$agence)
		{
			Page::set404();
			$this->noIndex();
			return;
		}
		if (!$agence['pageweb'] && !$this->isSkin('popup') && $agence['pdv'] != $agence['id'])
		{
			$agence = Agence::factory($agence['pdv']);
			self::redirect($agence->getURLAgence());
			return;
		}
		$this->addJS('scripts/jquery.slides.js', false);
		
		// on garde l'agence pour l'affichage
		$this->setData('agence', $agence);
		// GooglePlus et Footer
		if ($googleplus_url = $agence->getGooglePlusURL()) {
			// on n'indique que pour l'alias correspondant au point de vente
			if ($agence['id'] == $agence['pdv']) {
				$this->addLink(array('rel' => 'publisher', 'href' => $googleplus_url));
				$agence['footer'] .= "\n".sprintf('Vous pouvez �galement <a href="%s" target="_blank">nous retrouver sur Google+</a>', $googleplus_url);
			} else {
				$googleplus_url = null;
			}
		}
		if ($agence->hasData('footer')) {
			$this->setData('footer', $agence->getData('footer'));
		}
		// base pour les liens ci-dessous
		$here = Page::getURL(null, null, false);
		// on construit le fil d'ariane
		$this->_addBreadcrumb('Agences', $here.'agences/index.html');
		$up = ($agence['zone']=='fr' ? $agence->getDept() : $agence->getZone());
		$this->_addBreadcrumb($up['nom'], $here.gu_agence($up), 'location voiture '.$up['nom']);
		if ($agglo = $agence->getAgglo())
			$this->_addBreadcrumb($agglo['nom'], $here.gu_agence($agglo), 'location voiture '.$agglo['nom']);
		$this->_addBreadcrumb('Location voiture '.$agence['nom']);

		// les liens autour du d�partement
		if ($agence['zone']=='fr')
		{
			// les agllom�rations du d�partement sauf si d�j� dans le fil d'Ariane
			foreach(AgenceAgglo_Collection::factory($up['id'], false) as $a)
			{
				if ($agglo && $agglo['id']==$a['id']) continue;
				$this->_addLinkAround('Agences de location voiture <span class="location-dept">'.$a['nom'].'</span>', $here.gu_agence($a));
			}
		}
		// les villes autour
		$arounds = $agence->getAgencesAround(10);
		foreach($arounds as $a)
			$this->_addLinkAround('Agence de location voiture <span class="location-dept">'.$a['nom'].'</span>', $here.gu_agence($a));

		// ajouter gare, a�roport, corse, guadeloupe, paris et marseille, puis 6 agences au hasard
		$zoomListe = array
		(
			array('type' => 'AgenceEmplacement', 'id'=>'2', 'nom'=>'Gare', 'canon'=>'gare'),
			array('type' => 'AgenceEmplacement', 'id'=>'3', 'nom'=>'A�roport', 'canon'=>'aeroport'),
		);
		if ($agence['zone'] != 'co') {
			$zoomListe[] = array('type' => 'Zone', 'id'=>'co', 'nom'=>'Corse', 'canon'=>'corse');
		}
		if ($agence['zone'] != 'gp') {
			$zoomListe[] = array('type' => 'Zone', 'id'=>'gp', 'nom'=>'Guadeloupe', 'canon'=>'guadeloupe');
		}
		if ($up['id'] != '75')
			$zoomListe[] = array('type' => 'Dept', 'id'=>'75', 'nom'=>'Paris', 'canon'=>'paris');
		if ($up['id'] != '13')
			$zoomListe[] = array('type' => 'AgenceAgglo', 'id'=>'9', 'nom'=>'Marseille', 'canon'=>'marseille');
		// on affiche toutes ces agences
		foreach($zoomListe as $zoom)
			$this->_addLinkZoom('Agences de location voiture <span class="location-dept">'.$zoom['nom'].'</span>', $here.gu_agence($zoom));
		// ... et puis 6 agences prises au hasard
		$agenceStatut = Agence::STATUT_VISIBLE + Agence::STATUT_RESAWEB;
		$agencesNotIn = $arounds->getIds();
		$agencesNotIn[] = $agence['id'];
		$rs = sqlexec(sprintf("select 'AgenceAlias' type, aa.id, aa.nom, aa.canon from agence_alias aa where aa.zone='fr' and (aa.statut&%ld)=%ld and aa.agence not in (%s) order by rand() limit 0,6", $agenceStatut, $agenceStatut, "'".join("','", $agencesNotIn)."'"));
		while($row = mysql_fetch_assoc($rs))
			$this->_addLinkZoom('Agence de location voiture <span class="location-dept">'.$row['nom'].'</span>', $here.gu_agence($row));
	}
	
	/**
	* Affiche la liste des agences � partir d'une recherche
	* 
	*/
	public function listeAction()
	{
		// pour les d�partements Corse on redirige vers la zone
		if (in_array($_REQUEST['dept'], array('2A','2B')))
		{
			self::redirect(self::getURL('location-voiture-corse.html'));
			return;
		}
		// si une recherche est pass�e et qu'elle correspond � une forme canonique on y renvoie
		foreach (array($_REQUEST['search'], $_REQUEST['ville']) as $search)
		{
			if ($search && $canon = canonize($search))
			{
				if (getsql(sprintf("select count(*) from refnat_canon where canon='%s' and type in ('agglo','dept','emplacement','zone')", $canon)))
				{
					self::redirect(self::getURL(gu_agence(array('canon' => $canon))));
					return;
				}
				// on n'indexe pas les recherches
				$this->noIndex();
				break;
			}
		}
		
		// on cr�e une liste � partir de la recherche en cours
		$agences = Agence_Collection::factory($_REQUEST);
		$this->setData('agences', $agences);
		if (!$agences->count())
			$this->noIndex();
		// s'il n'y a qu'une agence on redirige
		if ($agences->count() == 1) {
			foreach($agences as /** @ar Agence */ $agence) {
				self::redirect($agence->getURLAgence());
				exit();
			}
		}
		// base pour les liens
		$here = Page::getURL(null, null, false);
		// on construit le fil d'ariane
		$this->_addBreadcrumb('Agences', $here.'agences/index.html');
		if ($what = $agences->getWhat())
		{
			$type = get_class($what);
			$zoomListe = array
			(
				array('type' => 'AgenceEmplacement', 'id'=>'2', 'nom'=>'Gare', 'canon'=>'gare'),
				array('type' => 'AgenceEmplacement', 'id'=>'3', 'nom'=>'A�roport', 'canon'=>'aeroport'),
				array('type' => 'Zone', 'id'=>'co', 'nom'=>'Corse', 'canon'=>'corse'),
				array('type' => 'Zone', 'id'=>'gp', 'nom'=>'Guadeloupe', 'canon'=>'guadeloupe'),
				array('type' => 'Dept', 'id' => '75','nom'=>'Paris','canon'=>'paris'),
			);
			// les liens autour du d�partement
			if ($type == 'Dept')
			{
				foreach($what->getContours() as /** @var Dept */ $dept)
					$this->_addLinkAround('Agences de location de voiture <span class="location-dept">'.$dept['nom'].'</span> ('.$dept['id'].')', $here.gu_agence($dept));
				$dept = $what;
			}
			if ($type == 'AgenceAgglo')
			{
				/** @var Dept */
				$dept = $what->getDept();
				$this->_addBreadcrumb($dept['nom'], $here.gu_agence($dept));
			}
			// les mises en avant et liesn vers villes
			if ($dept || $type == 'AgenceEmplacement')
			{
				foreach($zoomListe as $zoom)
				{
					if ($type==$zoom['type'] && $what['id']==$zoom['id']) continue;
					$this->_addLinkZoom('Agences de location de voiture <span class="location-dept">'.$zoom['nom'].'</span>', $here.gu_agence($zoom));
				}
				foreach(AgenceAgglo_Collection::factory($dept['id'], true) as $agglo)
				{
					if ($type=='AgenceAgglo' && $what['id']==$agglo['id']) continue;
					$this->_addLinkZoom('Agences de location de voiture <span class="location-dept">'.$agglo['nom'].'</span>', $here.gu_agence($agglo));
				}
			}
			// zone
			if ($type == 'Zone')
			{
				$this->setTrackInfo('pays', $what['id']);
				foreach(Zone_Collection::factory() as /** @var Zone */ $zone)
				{
					if ($zone['id']=='fr' || $zone['id']==$what['id']) continue;
					$this->_addLinkZoom('Agences de location de voiture <span class="location-dept">'.$zone['nom'].'</span>', $here.gu_agence($zone));
				}
			}
			// breadcrum et refnat
			$this->_addBreadcrumb('Location de voiture '.$what['nom']);
			if ($what->hasData('refnat'))
				$this->setData('refnat', $what->getData('refnat'));
			if ($what->hasData('footer'))
				$this->setData('footer', $what->getData('footer'));
		}
		// tracking
		$this->setTrackInfo('search', '1,'.$agences->getSearch());
	}
	protected function _createAnchor($lbl, $href, $title=null)
	{
		if ($href)
		{
			$str = '<a href="'.$href.'"';
			if ($title) $str.=' title="'.$title.'"';
			$str.='>';
		}
		$str .= $lbl;
		if ($href) $str.= '</a>';
		return $str;
	}
	
	// cr�e le fil d'ariane
	protected function _addBreadcrumb($lbl, $href = null, $title=null)
	{
		$this->_breadcrumb[] = $this->_createAnchor($lbl, $href, $title);
		$this->setData('html_breadcrumb', '<ul class="breadcrumb"><li>'.join('</li><li>', $this->_breadcrumb).'</li></ul>');
		return $this;
	}
	// cr�e les liens autour
	protected function _addLinkAround($lbl, $href, $title=null)
	{
		$this->_linksAround[] = $this->_createAnchor($lbl, $href, $title);;
		$this->setData('html_links_around', '<ul class="links_around"><li>'.join('</li><li>', $this->_linksAround).'</li></ul>');
		return $this;
	}
	// cr�e les liens de mise en avant
	protected function _addLinkZoom($lbl, $href, $title=null)
	{
		$this->_linksZoom[] = $this->_createAnchor($lbl, $href, $title);;
		$this->setData('html_links_zoom', '<ul class="links_around zoom"><li>'.join('</li><li>', $this->_linksZoom).'</li></ul>');
		return $this;
	}
	public function livraisonAction() {
		self::redirect(self::getURL('offre/service_voiturier.html'));
	}
}
?>
