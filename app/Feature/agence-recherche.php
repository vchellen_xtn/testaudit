<?php
/* Param�tres pouvant �tre pass� :
	$options		options � passer � api/json/agences c'est-�-dire � Agence_Collection::factory()
	$title			pour le titre de la popin
	$inside_page	pour �tre int�gr� � la page et non en popin
*/
	if (!$title) {
		$title = "Recherchez les agences les plus proches d'une ville ou d'une adresse";
	}
	$mainDivID = 'popin_recherche_agence';
	if ($inside_page) {
		$mainDivID = 'block_recherche_agence';
	}

?>
<!-- popin recherche agence //-->
<div id="<?=$mainDivID;?>" class="popin" title="Trouvez l'agence ADA la plus proche">
	<form id="form_recherche_agence" name="form_recherche_agence" action="#" method="post">
		<div>
			<p>
				<label for="input_recherche_agence" style="width:100%;"><?=$title;?></label>
				<input type="text" id="input_recherche_agence" name="input_recherche_agence" />
				<br/><em>(par exemple: "75", "a�roport", "corse", "place de la r�publique, paris")</em>
			</p>
			<p class="valid">
				<input type="submit" id="valid_recherche_agence" name="valid_recherche_agence" value="valider" />
			</p>
		</div>
	</form>
	<div id="results_recherche_agence">
		<div id="liste_recherche_agence" class="left"></div>
		<div id="carte_recherche_agence" class="left"></div>
		<div class="clear"></div>
	</div>
</div>
<script type="text/javascript">
/* <![CDATA[  */
	$(document).ready(function()
	{
		<? if ($inside_page) : ?>
			google.load("maps", "3", {other_params:'sensor=false<?=GOOGLE_API_KEY?>', callback: initMap});
			//$('#input_recherche_agence').val('').focus();
		<? else : ?>
		// d�finition du dialogue
		$('#popin_recherche_agence').dialog(
		{
			 autoOpen:false
			,resizable:false
			,draggable:false
			,width:Math.min(800, $(window).width())
			,height:Math.min(550, $(window).height())
			,modal:true
		});
		// click sur le lien trouvez votre agence dans le formulaire principal
		$('#trouvez_agence').click(function()
		{
			$('#popin_recherche_agence').dialog('open');
			google.load("maps", "3", {other_params:'sensor=false<?=GOOGLE_API_KEY?>', callback: initMap});
			$('#input_recherche_agence').val('').focus();
			return false;
		});
		$('#liste_recherche_agence .btn, a.lien_agence').live('click', function()
		{
			$('#popin_recherche_agence').dialog('close');
			$('#ville').val($(this).attr('data-nom')).effect('highlight', {color:"#b5ff6d"}, 2000);
			doAgence($(this).attr('data-id'));
			$('#ville_ok').show();
			$('#ville_ko').hide();
			return false;
		});
		<? endif; ?>
		// formulaire dans la bo�te de dialogue
		$('#form_recherche_agence').submit(function()
		{
			var search = $.trim($('#input_recherche_agence').val());
			if (search)
				doSearch(search);
			return false;
		});
	});
	// initialise la Google Map
	function initMap()
	{
		$("#carte_recherche_agence").width($("#results_recherche_agence").width() - $("#liste_recherche_agence").outerWidth(true)-3);
		doSearch();
	}
	// localisation par navigator.geolocation
	function doGeoLocation()
	{
		// sinon on recherche avec la g�olocalisation par le navigteur
		if (navigator.geolocation)
		{
			navigator.geolocation.getCurrentPosition
			(
				function(position)
				{
					doSearch(null, position.coords);
				},
				function()
				{
					handleNoGeolocation(false);
				}
			);
		}
		else
		{
			// Browser doesn't support Geolocation
			handleNoGeolocation();
		}
	}
	// g�rer la localisation avec google.loader
	function handleNoGeolocation()
	{
		if (google.loader && google.loader.ClientLocation.longitude)
			doSearch(null, google.loader.ClientLocation);
	}
	// effectue la recherche
	function doSearch(search, p)
	{
		var args = {"method": "agences", "format": "json", "distance": 100, "limit": 10};
		var options = <?=json_encode($options)?>;
		if (options)
			$.extend(args, options);
		if (search)
			args.search = search;
		else if ((agence = $('#agence').val()))
			args.agence = agence;
		else if (p && p.latitude)
		{
			args.lat = deg2rad(p.latitude);
			args.lon = deg2rad(p.longitude);
		}
		else if (navigator.geolocation || (google.loader && google.loader.ClientLocation.longitude))
		{
			doGeoLocation();
			return;
		}
		else
			return false;
		// lancer la recherche
		$.ajax({
			 url:'../api.php'
			,data: $.param(args)
			,dataType:'json'
			,success:function(json)
			{
				$('#form_recherche_agence').data('results', json);
				rechercheDisplayList();
				rechercheMapLoaded();
			}
		});
	}
	// affiche la liste des agences
	function rechercheDisplayList()
	{
		var results = $('#form_recherche_agence').data('results');
		if (!results.agences || !results.agences.length)
		{
			$('#liste_recherche_agence').html('<strong>Aucun r�sultat trouv�</strong>').css('height','400px');
			return;
		}
		var agences = results.agences;
		$('#liste_recherche_agence').html('').css('height','400px');
		for (var i = 0; i < agences.length; i++)
		{
			var texte = '<p class="recherche_agence" id="recherche_agence_'+agences[i].id+'">'+getHtmlAddress(agences[i])+'</p>';
			$('#liste_recherche_agence').append(texte);
		}
	}
	// affiche la carte GoogleMaps
	function rechercheMapLoaded()
	{
		$('#carte_recherche_agence').css('height','400px').show();
		var mapOptions = {zoom: 5, mapTypeId: google.maps.MapTypeId.ROADMAP};
		var map = new google.maps.Map(document.getElementById('carte_recherche_agence'), mapOptions);
		var results = $('#form_recherche_agence').data('results');
		if (!results || !results.agences || !results.agences.length)
		{
			map.setCenter(new google.maps.LatLng(46.22, 2.22), 5);
			return;
		}
		var center, agences = results.agences;

		// pr�parer les agences
		for (var i = 0; i < agences.length; i++)
		{
			agences[i].lat = rad2deg(agences[i].latitude);
			agences[i].lon = rad2deg(agences[i].longitude);
			agences[i].html = getHtmlAddress(agences[i], true);
		}
		// afficher la liste des agences
		if (results.center.latitude)
			center = {"point": new google.maps.LatLng(results.center.latitude, results.center.longitude), "title": results.search ? results.search : "Votre localisation"};
		var bounds = createMarkersFromAgences(map, center, agences, function(marker)
		{
			var agence_id = 'recherche_agence_' + marker.agence_id;
			$('.recherche_agence').removeClass('current').filter('#'+agence_id).addClass('current');
			// scroll dans la div
			$target = $('#'+agence_id);
			if ( $target && $target.length)
			{
				vposition = $('#liste_recherche_agence').scrollTop() + $target.position().top - $target.outerHeight();
				$('#liste_recherche_agence').animate({scrollTop: vposition}, 1000);
			}
		});
		map.setCenter(bounds.getCenter(), Math.min(map.fitBounds(bounds), 15));
	}

	function getHtmlAddress(agence, court)
	{
		court = !!court;

		// Si on est sur la page Prestige, alors aucune r�servation n'est possible sur le site
		// On d�sactive le lien sur le nom de l'agence, et le bouton pour R�server
		var typeOffre = "<?=$type_offre?>";
		var lien = '';

		if (typeOffre != 'prestige') {
			lien = '<' + 'a class="lien_agence" hr' + 'ef="'+agence.url_agence+'" data-id="'+agence.id+'" data-nom="'+agence.recherche +'">'+agence.nom+'<span class="end"></span></a><br />';
		}else{
			lien = '<'+ 'p class="lien_agence_prestige" data-id="'+agence.id+'" data-nom="'+agence.recherche +'">'+agence.nom+'</p>';
		}

		var output = lien
			+agence.adresse1+'<br />'
			+( agence.adresse2 && agence.adresse2.length ? agence.adresse2+'<br />' : '' )
			+agence.cp+' '+agence.ville+'<br />';
		if ( court ) {
			return output;
		}
		if (agence.tel && agence.tel.length)
			output += 'T�l : '+agence.tel+'<br />';
		if (agence.fax && agence.fax.length)
			output += 'Fax : '+agence.fax+'<br />';

		if (typeOffre != 'prestige')
			output += '<' + 'a class="btn" hr' + 'ef="'+agence.url_agence+'" data-id="'+agence.id+'" data-nom="'+agence.recherche +'">R�servez dans cette agence<span class="end"></span></a>';

		output += '<div class="clear"></div>';

		return output;
	}
	function deg2rad(deg) { return (deg *  Math.PI) / 180; }
	function rad2deg(rad) { return (rad * 180) / Math.PI; }	
/* ]]> */
</script>

