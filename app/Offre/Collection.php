<?
class Offre_Collection extends Ada_Collection
{
	const OFFRE_TOUTES = -1;
	const OFFRE_ALTERNATIVES = -2;
	
	const INFO_PROFIL = 1;
	const INFO_DELAI = 2;
	const INFO_DISPO = 3;
	
	protected $_infoText = null;
	protected $_infoType = null;
	protected $_indexByCategory = array(); // index des offres par cat�gorie
	/** @var Forfait_Collection */
	protected $_forfaits = null;
	/** @var Forfait_Collection */
	protected $_forfaitsLocal = null;
	/** @var Reservation */
	protected $_reservation = null;
	/** @var Agence */
	protected $_agence = null;

	/** restriction sur la recherche des promotions associ�es � l'offre */
	protected $_promoRestrictions = null;
	
	/** disponibilit�s remont�es par Unipro */
	protected $_disposUnipro = array();
	
	/**
	* D�termine les offres en v�hicules � partir d'une r�servation et d'une agence
	* @param Reservation $reservation
	* @param Agence $agence
	* @param string $catZone zone pour s�lectionner les cat�gories
	* @param int $nb nombre de cat�gories � renvoyer
	* 				  >= 0 la cat�gorie demand�e et les cat�gories sup�rieures
	* 				  = -1 toutes les cat�gories
	* 				  = -2 les cat�gories "alternatives"
	* @retrns Offre_Collection
	*/
	static public function factory(Reservation $reservation, Agence $agence = null, $catZone = null, $nb = 2)
	{
		// constituer l'offre
		$offres = array();
		$indexCategory = array();
		$indexMnem = array();
		$info = null;
		$infoType = null;
		
		if (!$agence)
			$agence = $reservation->getAgence();
		$categorie = $reservation->getCategorie();
		if (!$catZone)
			$catZone = $categorie['zone'];
		$catID = $categorie['id'];
		$d1 = addslashes($reservation['debut']);
		$d2 = addslashes($reservation['fin']);
		/* Mode T�l�-Conseiller
		- on ne v�rifie pas les d�lais de r�servation
		*/
		$is_tc_mode = ($_COOKIE["ADA001_TC_MODE"] != null);
		if ($agence->hasStatut(Agence::STATUT_RESAWEB))
		{
			// v�rifie la disponibilit� pour cette agence
			$sql = "select SQL_NO_CACHE d.categorie, d.type, c.mnem, MIN(case when s.id is null then d.nb else ifnull(s.nb,0) end) nb, coalesce(cz.age, c.age) age, coalesce(cz.permis, c.permis) permis\n";
			$sql.= "  from categorie c\n";
			$sql.= "  join agence_dispo d on (d.agence='".addslashes($agence['agence'])."' and d.categorie=c.id)\n";
			$sql.= "  left join categorie_zone cz on (cz.zone='".$agence['zone']."' and cz.categorie=c.id)\n";
			$sql.= "  left join stop_sell s\n";
			$sql.= "	on (s.agence is null or s.agence='".addslashes($agence['agence'])."' or s.agence='".addslashes($agence['pdv'])."')\n";
			$sql.="    and s.zone = '".addslashes($agence['zone'])."'\n";
			$sql.= "   and (s.type is null or s.type=c.type)\n";
			$sql.= "   and (s.reseau is null or s.reseau='".$agence['reseau']."')\n";
			$sql.= "   and (s.groupe is null or s.groupe='".$agence['groupe']."')\n";
			$sql.= "   and (s.code_societe is null or s.code_societe='".$agence['code_societe']."')\n";
			$sql.= "   and (s.categorie is null or s.categorie=c.id)\n";
			$sql.= "   and (s.debut is null or s.debut <= '".$d2."')\n";
			$sql.= "   and (s.fin is null or s.fin >= '".$d1."')\n";
			$sql.= "   and (coalesce(s.forfait_prefix, s.forfait) is null)\n";
			$sql.= " where c.zone = '".$catZone."'\n";
			// pour le type ML on regarde les autres types avec le m�me mn�monique
			$sql.= "   and (c.type = '".$categorie['type']."' OR 'ML'='".$categorie['type']."')\n";
			$sql.= "   and c.publie = 1\n";
			if ($nb > 0) // on ne veut pas toutes les cat�gories mais seulement une partie
			{
				$sql.= "   and (c.mnem ".($categorie['type']=='ML' || $nb==1 ? '=' : '>=')." '".addslashes($categorie['mnem'])."'";
				if ($nb==1 && $categorie['famille'])
					$sql.= " or c.famille=".$categorie['famille'];
				$sql.=")\n";
				if ($nb > 1 || ($nb==1 && $categorie['famille'])) // dans le cas o� on veut deux cat�gories on ne veut pas d'indispo pour les suivantes
					$sql.= "   and d.nb > 0 and c.position >= '".$categorie['position']."'\n"; // on diff�rencie la disponibilit� � z�ro et l'absence de disponibilit�
			} else if ($nb == self::OFFRE_ALTERNATIVES) {
				// ne rechercher que les cat�gories souhait�es
				$sql.=' and c.mnem in ("'.implode('","', $reservation->getCategorie()->getAlternatives()).'")'."\n";
			}
			$sql.= " group by d.categorie, d.type, c.mnem, coalesce(cz.age, c.age) , coalesce(cz.permis, c.permis)\n";
			$sql.= " order by c.zone, c.type, c.position, c.mnem";
			if ($nb > 0) 
				$sql.=' limit '.$nb;
			
			// cr�er le tableau des offres
			$age = $reservation['age']; $permis = $reservation['permis'];
			for ($rs=sqlexec($sql); $row=mysql_fetch_assoc($rs); )
			{
				// v�rifier age et permis
				if (($row['age'] > $age) || ($row['permis'] > $permis)) 
				{
					// l'offre est indisponible
					$row['nb'] = 0;
					// si c'est la cat�gorie demand�e on informe l'utilisateur
					if ($catID == $row['categorie'])
					{
						$infoType = self::INFO_PROFIL;
						$info = '<p style="margin:0 0 15px;">Votre profil conducteur ne vous permet pas de r�server la cat�gorie de v�hicule s�lectionn�e.';
						$info.= ' Nous vous invitons � modifier votre demande.</p>';
						$info.= '<strong>Pour la location de '.Categorie::$TYPE_LBL[$reservation['type']].' : '.$categorie['nom'].'</strong>';
						$info.= '<ul class="tiret" style="margin:0 0 15px;">';
						$info.= '<li>Age minimum requis : '.$row['age'].' ans</li>';
						$info.= "<li>Nombre d'ann�es de permis minimum : ".$row['permis']." ann�e</li>";
						$info.= "</ul>";
					}
				}
				// ajouter aux offres
				$offres[] = new Offre($row);
				$indexCategory[$row['categorie']] = count($offres)-1;
				$indexMnem[$row['mnem']] = count($offres)-1;
			}
			// v�rifier que le nombre de r�servations n'exc�de pas la dispo pour la p�riode et les cat�gories
			if (count($offres))
			{
				// la liste des cat�goirs avec la gstion ADA Malin
				// une m�me cat�gorie de v�hicule peut �tre dans ADA Malin et ADA Premier prix
				$catIDs = join(',', array_keys($indexCategory));
				if ($categorie['type']=='ML' && count($offres)==1 && ($x = $offres[0]) && $x['type']!='ML' && $x['mnem']==$categorie['mnem'])
					$catIDs .= ','.$categorie['id'];
				else if ($categorie['type']!='ML' && in_array($categorie['mnem'], array('0','S','Z')))
				{
					// existe-t-il une cat�gorie ADA Malin avec le m�me mn�monique et si oui il ne doit pas y avior de disponiblit�s sp�cifiques
					$sql = "select c.id";
					$sql.=" from categorie c ";
					$sql.=" left join agence_dispo d on (d.agence='".$agence['agence']."' and d.categorie=c.id)";
					$sql.=' where c.zone="fr" and c.type="ML" and c.position >= 0 and c.mnem IN ("'.join('","', array_keys($indexMnem)).'") and d.id is null';
					if ($x = getsql($sql))
						$catIDs .= ','.$x;
				}
				
				// compter la disponibilit� r�elle
				$sql = "select SQL_NO_CACHE c.mnem, count(*) cnt\n";
				$sql.=" from reservation r\n";
				$sql.=" join categorie c on c.id=r.categorie\n";
				$sql.=" where r.agence='".$agence['agence']."'";
				$sql.="  and r.categorie IN (".$catIDs.")\n";
				$sql.="  and r.statut='P'"; // on ne tient plus compte du statut 'V' pour les cas o� le paiement ne se fait pas, il reste un risque de surbooking "faible"
				$sql.= " and (('".$d1."' between r.debut and r.fin)\n or ('".$d2."' between r.debut and r.fin)\n or (debut between '".$d1."' and '".$d2."'))\n";
				$sql.=" group by c.mnem\n";
				for ($rs=sqlexec($sql); $row=mysql_fetch_assoc($rs); ) {
					$offres[$indexMnem[$row['mnem']]]['nb'] = max($offres[$indexMnem[$row['mnem']]]['nb'] - $row['cnt'], 0);
				}
				// ... et maintenant pour ADA Malin on a pu trouver une cat�gorie dans un autre type
				// ... il faut corriger
				if ($categorie['type']=='ML' && count($offres)==1 && ($x = $offres[0]) && $x['type']!='ML' && $x['mnem']==$categorie['mnem'])
				{
					unset($indexCategory[$x['categorie']]);
					$indexCategory[$categorie['id']] = 0;
					$x['type'] = 'ML';
					$x['categorie'] = $categorie['id'];
				}
			}
		}

		// pas de dispo, il faut quand m�me afficher le tarif th�orique
		if (!count($offres) ||  !isset($indexCategory[$catID]))
		{
			array_unshift ($offres, new Offre(array('categorie' => $catID, 'mnem' => $categorie['mnem'], 'nb' => 0, 'indispo' => true)));
			foreach($indexCategory as $k => $v) {
				$indexCategory[$k] = $v+1;
			}
			$indexCategory[$catID] = 0;
		}

		// v�rifier les d�lais de r�servation minimum
		if (!$is_tc_mode && $reservation['forfait_type']!='RESAS' && $reservation->isTooLate($agence))
		{
			$infoType = self::INFO_DELAI;
			$info = '<strong style="font-size: 15px;">Votre date / heure de d�part ne permet pas de r�server ce v�hicule en ligne</strong>';
			// pas d'upsell dans ce cas et pas de r�servation possible
			$offres = array(new Offre(array('categorie' => $catID, 'mnem' => $categorie['mnem'], 'nb' => 0)));
			$indexCategory = array($catID => 0);
		}
		// pas d'upsell pour les v�hicules sans permis
		if (($reservation['type']=='sp' || $reservation['type']=='ml') && count($offres) > 1)
		{
			$offres = array($offres[0]);
			$indexCategory = array($offres[0]['categorie'] => 0);
		}

		// v�hicule non dispo dans cette agence ou agence non ouverte en ligne
		if ($offres[$indexCategory[$reservation['categorie']]]['nb'] <= 0 && !$info)
		{
			$infoType = self::INFO_DISPO;
			$info = '<strong class="num_'.strtolower($reservation['type']).' num_1">'."Le v�hicule {$reservation->getCategorie()->getData('nom')} n'est plus disponible sur ADA.fr.</strong>";
			if ($agence['tel'])
				$info.= "<span class=\"contactez\">(contactez votre agence au ".$agence['tel'].")</span>";
		}
		// Cr�er Offre_Collection
		$x = new Offre_Collection();
		$x->_reservation = $reservation;
		$x->_agence = $agence;
		$x->_items = $offres;
		$x->_indexByCategory = $indexCategory;
		$x->_infoText = $info;
		$x->_infoType = $infoType;
		foreach ($x->_items as /** @var Offre */ $o)
			$o->setCollection($x);
		return $x;
	}
	public function hasInfo() { return !empty($this->_infoText); }
	public function getInfo() { return $this->_infoText; }
	public function getInfoType() { return $this->_infoType; }
	public function isInfoType($infoType) { return ($this->_infoType == $infoType); }
	
	/**
	* Renvoie une offre � partir de la cat�gorie
	* 
	* @param int $catID identifiant de la cat�gorie
	* @returns Offre
	*/
	public function getOffreByCategory($catID)
	{
		return $this->_items[$this->_indexByCategory[$catID]];
	}
	/**
	* Renvoie un tableau avec les identifiants des cat�goires
	* @returns array
	*/
	public function getCategories()
	{
		return array_keys($this->_indexByCategory);
	}
	/**
	* Pr�icse des restrictions aux promotions
	* 
	* @param int $restrictions cf ForfaitCollection
	* @returns Offre_Collection
	*/
	public function setPromoRestrictions($restrictions)
	{
		$this->_promoRestrictions = $restrictions;
		return $this;
	}
	public function getPromoRestrictions()
	{
		return $this->_promoRestrictions;
	}
	/**
	* Renvoie les forfaits associ�es aux offres
	* @returns Forfait_Collection
	*/
	public function getForfaits()
	{
		if (!$this->_forfaits) {
			// on pr�cise le p�rim�tre des tarifs sauf pour les JEUNES et LOCAPASS
			// ce code ne peut pas �tre g�r� au niveau de la r�servation : dans le cas d'une indispo, l'agence de offre_Collection est diff�rente de celle de la r�servation
			$tarifScope = 'ADAFR';
			if (!in_array($this->_reservation['forfait_type'], array('JEUNES','LOCAPASS'))) {
				$tarifScope = $this->_agence->getTarifsWeb($this->_reservation['type']);
			}
			$this->_forfaits = Forfait_Collection::factory($this->_reservation, $this->getCategories(), $this->_promoRestrictions, $tarifScope);
			// faut-il appliquer le yield ?
			if ($tarifScope == 'RESAS' && $this->_agence->getTarifsWeb('promo_yield')) {
				// pour �viter d'interroger UNIPRO inutilement, on v�rifie l'existence de Yield pour chaque cat�gorie?
				$yields = $this->_agence->getYields($this->_reservation['type']);
				// pour chaque offre
				$disposUnipro = null;
				foreach($this->_items as /** @var Offre */ $offre) {
					if ($yields->exists($offre['mnem'])) {
						if (is_null($disposUnipro)) {
							$disposUnipro = $this->getDisposUnipro();
						}
						if ($dispos = $disposUnipro[$offre['categorie']]) {
							if (isset($dispos['offre']['jauge'])) {
								$jauge = $dispos['offre']['jauge'];
								if ($yield = $yields->find($offre['mnem'], $jauge)) {
									$yield['cat_id'] = $offre['categorie'];
									$this->_forfaits->applyYield($yield);
								}
							}
						}
					}
				}
			}
		}
		return $this->_forfaits;
	}
	/**
	* Renvoie les forfaits locaux (agence_forfait_tarif, agence_promo_forfait) associ�es aux offres
	* @returns Forfait_Collection
	*/
	public function getForfaitsLocal()
	{
		if (!$this->_forfaitsLocal)
			$this->_forfaitsLocal = Forfait_Collection::factory($this->_reservation, $this->getCategories(), $this->_promoRestrictions, Forfait_Collection::TARIF_LOCAL);
		return $this->_forfaitsLocal;
	}
	/**
	* Renvoie un forfait � partir d'une cat�goire
	* 
	* @param int $catID identifiant d'une cat�goire
	* @return Forfait
	*/
	public function getForfait($catID)
	{
		return $this->getForfaits()->getItem($catID);
	}
	/**
	* Renvoie un forfait local � partir d'une cat�goire
	* 
	* @param int $catID identifiant d'une cat�goire
	* @return Forfait
	*/
	public function getForfaitLocal($catID)
	{
		return $this->getForfaitsLocal()->getItem($catID);
	}
	/**
	* Renvoie le kilom�trage
	* @returns array
	*/
	public function getKilometrages()
	{
		return $this->getForfaits()->getKilometrages();
	}
	/** y-a-til au moins une dispo ? 
	**
	**@param $exceptCatID int cat�gorie � ne pas v�rifier
	* @returns bool 
	*/
	public function hasDispo($exceptCatID = null)
	{
		foreach($this->_items as $o)
		{
			if (($o['nb'] > 0) && (is_null($exceptCatID) || $o['categorie']!=$exceptCatID)) 
				return true;
		}
		return false;
	}
	/**
	* Charger les tarifs Unipro et mettre � jour les Offre et les Forfaits
	* @param AccesAgence $resasUser l'utilisateur courant de l'outil
	* @param bool $checkOtherCategories indique si on regarde les cat�gories alternatives ou juste celles concern�es
	* @returns Offre_Collection
	*/
	public function loadTarifsUnipro(AccesAgence $resasUser, $checkOtherCategories = true)
	{
		if (!$this->_reservation) return;
		$this->_infoText = array();
		
		// 1. r�cup�rer les informations sur le planning
		$infoUnipro = $this->getDisposUnipro();
		
		// 2. compl�ter les offres et les forfaits avec les tarifs locaux
		$reservation = $this->_reservation;
		$agence = $this->_agence; // Attention ! l'agence peut �tre diff�rente de celle de la r�servation pour les indispos
		$categories = Categorie_Collection::factory('fr', $reservation['type']);
		if ($resasUser->getConfigData('promo_yield', 'agence', $agence['agence'])) {
			// on r�cup�re les Yields une fois pour toute
			$yields = $agence->getYields($reservation['type']);
		} else {
			$yields = new AgencePromoYield_Collection();
		}
		foreach($this->_items as /** @var Offre */ $offre) {
			$info = $infoUnipro[$offre['categorie']];
			// la lsite des v�hicules
			$vehicules = array();
			if (isset($info['vehicules'])) {
				foreach($info['vehicules'] as $k => $modeles) {
					foreach($modeles as $vehicule => $immats) {
						$vehicules[$k][reset($immats).'_'.$vehicule] = $vehicule.' ('.count($immats).')';
					}
				}
			}
			$offre['unipro_vehicules'] = $vehicules;
			// la disponibilit�
			if (isset($info['offre'])) {
				$offre['nb'] = $info['offre']['nb'];
				$offre['max'] = $info['offre']['max'];
				if (isset($info['offre']['jauge'])) {
					$offre['jauge'] = $info['offre']['jauge'];
				}
			}
			
			// compl�ter le forfait
			if ($f = $this->getForfait($offre['categorie'])) {
				if (!$f->isLocapass()) {
					$f->setData('forfait_type', 'RESAS');
				}
				if ($local = $this->getForfaitLocal($offre['categorie'])) {
					$f['local_tarif_ttc'] = $local->getPrixBase();
					$f['local_promo_ttc'] = $local->getPrix();
					$f['local_forfait'] = $local['id'];
					$f['local_prix_origine'] = $local['prix_origine'];
					$f['local_nom'] = $local['nom'];
					$f['local_km'] = $local['km'];
					$f['local_promo_id'] = $local['p_id'];
					$f['local_promo_nom'] = $local['p_nom'];
					$f['local_promo_type'] = $local['promo_type'];
					foreach(array('nom','local_nom') as $k) {
						$f[$k] = preg_replace('/\([^\)]+\)$/' ,'', $f[$k]);
					}
				} else {
					$f['local_tarif_ttc'] = $f->getPrixBase();
					$f['local_promo_ttc'] = $f->getPrixBase();
				}
				// appliquer le yield s'il y a lieu
				// on v�rifie que la cat�gorie permet la promotion
				$c = $categories->getItem($offre['mnem']);
				if (!$c['nopromo'] && !$f['nopromo']) {
					// on recherche le yield s'il existe
					$yield = null;
					if (isset($offre['jauge']) && $yields->exists($c['mnem'])) {
						// la jauge est un taux d'utilisation : nb est le nombre disponible et max le nombre maximum
						if ($yield = $yields->find($c['mnem'], $offre['jauge'])) {
							$f['local_yield_ttc'] = $yield->getPrixYield($f['local_promo_ttc']);
							$f['local_promo_yield'] = $yield->getBornes();
							$f['local_yield'] = $yield['montant'];
							$f['local_promo_nom'] .= ($f['local_promo_nom'] ? ' - ' : '').$yield->getDescription();
						}
					}
				}
			}
		}
		return $this;
	}
	/**
	* Renvoie les informations de dispnibilit�s � partir d'UNIPRO
	* @param	$mnems liste de cat�gories
	* @returns 	array
	*/
	public function getDisposUnipro($checkOtherCategories = true)
	{
		// 1. si on a d�j� effectu� la requ�te on s'arr�te l�
		if ($this->_disposUnipro) {
			return $this->_disposUnipro;
		}
		$reservation = $this->_reservation;
		$agence = $this->_agence;
		
		// 2. faire la liste des cat�gories � interroger
		$alternatives = array();
		if (!$checkOtherCategories) {
			$alternatives = array($reservation->getCategorie()->getData('mnem'));
		} else if ($reservation['RESAS']) {
			// on se limite aux cat�gories qui sont affich�es
			$alternatives = $reservation->getCategorie()->getAlternatives();
		} else {
			foreach($this->_items as $offre) {
				$alternatives[] = $offre['mnem'];
			}
		}
		$CAT_UNIPRO_V = '30';
		$type = strtoupper($reservation['type']);
		if ($type == 'SP') {
			$type = 'VP'; // UNIPRO ne conna�t pas SP, cela fait partie des VP
			$catTarifs = $CAT_UNIPRO_V;
		} else {
			$catTarifs = implode(',',  $alternatives);
		}
		
		// 3. r�cup�rer les disponibilit�s aupr�s d'UNIPRO
		$dispos = array(); $parcs = array();
		if ($unipro = Unipro::getSingleUnipro()) { /* si UNIPRO ne r�pond pas ! */
			list($dispos, $parcs) = $unipro->getDispos($agence['agence'], $type, $catTarifs, $reservation->getDebut('Y-m-d H:i'), $reservation->getFin('Y-m-d H:i'));
			if ($error = $unipro->getLastError()) {
				$this->_infoText[] = "Erreur LEA lors de la r�cup�ration des disponibilit�ss<br/>Nous vous invitons � renouveler votre demande.";
				if (IS_RND) {
					$this->_infoText[] = $error;
				}
			}
		} else {
			$this->_infoText[] = "Impossible de se connecter � LEA pour obtenir les disponibilit�s";
		}
		
		// 4. on consolide l'information
		$infoUnipro = array();
		$categories = Categorie_Collection::factory('fr', $reservation['type']);
		if ($dispos) {
			// les disponibilit�s et nombre maximum de v�hicules par cat�gorie
			foreach($dispos as $dispo) {
				$mnem = $dispo->cat;
				// traitement particulier pour la cat�gorie V (voiture sans permis)
				if ($mnem == $CAT_UNIPRO_V) $mnem = 'V';
				if ($c = $categories->getItem($mnem)) {
					$o = array('nb'	=> $dispo->dispo, 'max'	=> $dispo->total);
					if ($o['max'] > 0) {
						$o['jauge'] = round((($o['max']-$o['nb'])*100)/$o['max']);
					}
					$infoUnipro[$c['id']]['offre'] = $o;
				}
			}
		}
		if ($parcs) {
			// la liste des v�hicules disponibles
			foreach($parcs as $parc) {
				$mnem = $parc->cat;
				// traitement particulier pour la cat�gorie V (voiture sans permis)
				if ($mnem == $CAT_UNIPRO_V) $mnem = 'V';
				if ($c = $categories->getItem($mnem)) {
					$infoUnipro[$c['id']]['vehicules'][$parc->agence][$parc->marquemodele][] = $parc->immat;
				}
			}
		}
		if ($checkOtherCategories) {
			// si une seule cat�gorie a �t� demand�e on ne garde pas le r�sultat
			$this->_disposUnipro = $infoUnipro;
		}
		return $infoUnipro;
	}
	/**
	* Charge les informations pour pr�parer les n�gociations
	* @returns Offre_Collection
	*/
	public function loadNegociations() {
		// est ce que l'agence autorise les n�gociations
		if ($this->_agence && ($this->_agence->hasStatut(Agence::STATUT_STOP_NEGO) || $this->_agence->isPointLoc())) {
			return $this;
		}
		
		// y-a-t-il des n�gociations aujourd'hui
		$negociations = PromotionNegociation_Collection::factory($this);
		if ($negociations && !$negociations->isEmpty()) {
			$forfaits = $this->getForfaits();
			// pour chaque n�gociation, on calcule des prix
			foreach($negociations as /** @var PromotionNegociation */ $negociation) {
				$mnems = explode(',', $negociation['categories']);
				$forfaitsNegociation = Forfait_Collection::factory($this->_reservation, $this->getCategories(), $forfaits->getPromoRestrictions(), $forfaits->getTarifScope(), $negociation);
				// pour chaque forfait n�goci�, on 
				foreach($forfaitsNegociation as $forfaitNego) {
					// si la cat�gorie n'est pas dans les n�gociations on arr�te l�
					if (!in_array($forfaitNego['mnem'], $mnems)) continue;
					// si le forfait n'utilise pas la classe associ�e � la n�gociation, ce n'est pas la peine d'aller plus loin
					if ($forfaitNego['p_classe'] != $negociation['classe']) continue;
					// on applique la n�gociation au forfaits
					if ($forfait = $this->getForfait($forfaitNego['categorie'])) {
						$forfait->initNegociation($negociation, $forfaitNego);
					}
				}
				
			}
		}
		return $this;
	}
}
?>
