<?php
/** @var Agence */ $agence = $this;
?>
<? if($agence['latitude'] && $agence['longitude']) : ?>
<div id="map_agence_infos"></div>
<? endif; ?>
<div class="adresse-agence">
	<?=$agence->getHtmlAddress(' ', null, 'tel');?>
	<br/><?=$agence->getHtmlAddress(' ', 'tel');?>
</div>
<?php echo $agence->toHtml('block-horaires-vertical'); ?>
<? if ($agence['latitude'] && $agence['longitude']) : ?>
<script type="text/javascript">
// <![CDATA[
	$(document).ready(function() {
		if ($("#map_agence_infos").is(":visible")) {
			initMapAgenceInfos();
		}
	});

	/* si on charge la carte avec l'onglet fermé, le plan ne s'affiche pas correctement */
	function initMapAgenceInfos() {
		if (!google.maps) {
			google.load("maps", "3", {other_params:'sensor=false<?=GOOGLE_API_KEY?>', callback: initMapAgenceInfos});
			return;
		}
		// on n'initialise pas 2 fois la map
		if ($("#map_agence_infos").html().length) {
			return;
		}
		// on ne charge la carte qu'une fois... et si la carte est visible
		var latitude = <?=rad2deg($agence['latitude']);?>;
		var longitude = <?=rad2deg($agence['longitude']);?>;
		var mylatlng = new google.maps.LatLng(latitude ,longitude);
		var mapOptions = {
			center: mylatlng,
			zoom: <?= isset($agence['zoom']) ? $agence['zoom'] : 15 ?>,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			disableDoubleClickZoom : false,
			scrollwheel: false,
			draggable: true,
			keyboardShortcuts: true,
			mapTypeControl: false,
			panControl: true,
			zoomControl: true,
			scaleControl: false,
			streetViewControl: false
		};
		mapOptions.maxZoom = mapOptions.zoom + 2;
		mapOptions.minZoom = mapOptions.zoom - 2;
		var map = new google.maps.Map(document.getElementById("map_agence_infos"), mapOptions);
		// creation d'un marker
		var image = new google.maps.MarkerImage("../img/gmap_marker.png",
			// This marker is 20 pixels wide by 32 pixels tall.
			new google.maps.Size(12, 18),
			// The origin for this image is 0,0.
			new google.maps.Point(0,0),
			// The anchor for this image is the base of the flagpole at 0,32.
			new google.maps.Point(6, 18)
		);
		var marker = new google.maps.Marker({
			position: mylatlng,
			map: map,
			icon: image
		});
	}
// ]]>
</script>
<? endif; ?>
