<?
class QuestionUser extends Ada_Object
{
	public function _construct() {
		$this->setIdFieldName('reservation');
	}
	
	/**
	* Cr�er l'user d'un questionnaire
	* 
	* @param string $key
	* @returns QuestionUser
	*/
	static public function factory($key=null)
	{
		// on filtre les caract�res possibles
		$x = new QuestionUser();
		if ($x->loadFromKey($key)->isEmpty()) {
			$x->createFromKey($key);
		}
		if ($x->isEmpty()) {
			return null;
		}
		return $x;
	}
	
	/**
	* Contr�le la cl�
	* 
	* @param string $key
	* @returns QuestionUser
	*/
	public function loadFromKey($key)
	{
		// controle de la cl�
		if ($id_reservation = $this->_decryptData($key)) {
			$this->load($id_reservation);
		}
		return $this;
	}
	
	/**
	* Cr�e un QuestionUser � partir des infos Reservation
	* 
	* @param string $key
	* @returns QuestionUser
	*/
	public function createFromKey($key)
	{
		// controle de la cl�
		if ($id_reservation = $this->_decryptData($key)) {
			// donn�es Reservation
			$reservation = Reservation::factory($id_reservation);
			if ($reservation) {
				$this->_data['reservation'] = $id_reservation;
				foreach (explode('|', 'forfait_type|type|categorie|agence|pdv|debut|fin') as $fld) {
					$this->_data[$fld] = $reservation->getData($fld);
				}
				// donn�es Agence
				$agence = $reservation->getAgence();
				foreach (explode('|', 'zone|reseau|code_societe') as $fld) {
					$this->_data[$fld] = $agence->getData($fld);
				}
				$this->save();
			}
		}
		return $this;
	}
}