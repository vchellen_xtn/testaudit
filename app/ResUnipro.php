<?php
class ResUnipro extends Ada_Object
{
	/** @var Reservation $reservation */
	protected  $_reservation = null;
	/** @var Unipro */
	protected $_unipro;
	
	public function _construct()
	{
		$this->setIdFieldName('reservation');
	}
	/**
	* Cr�e un objet ResUnipro
	* 
	* @param Reservation $reservation int | Reservation
	* @return ResUnipro
	*/
	static public function factory(Reservation $reservation)
	{
		$x = new ResUnipro();
		$x->load($reservation->getId());
		$x->_reservation = $reservation;
		return $x;
	}
	
	/**
	* Cr�e un nouvel enregistrement UNIPRO
	* 
	* @param Reservation $reservation
	* @return ResUnipro
	*/
	static public function create(Reservation $reservation)
	{
		$a = array('reservation' => $reservation['id'], 'statut' => $reservation['statut']);
		$x = new ResUnipro($a);
		$x->save();
		$x->_reservation = $reservation;
		return $x;
	}
	/**
	* R�cup�re la date d'enl�vement du mat�riel
	* 
	* @param string $error contient la description en cas d'erreur
	*/
	public function getDateElevement(&$error)
	{
		if (!$this->_data['materiel'])
		{
			try { $unipro = @new Unipro(); }
			catch (Exception $e) { $e = (string) $e; }
			if (!$unipro->getDateEnlevementOptions($this, $this->_reservation, $error))
				$error = "Une erreur s'est produite lors de la connexion � UNIPRO. ".$error;
		}
		return $this->_data['materiel'];
	}
	
	
}
?>
