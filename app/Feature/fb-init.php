<script type="text/javascript" language="javascript">
//<![CDATA[ 
// Load Facebook SDK Asynchronously
(function(d)
{
	var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
	if (d.getElementById(id)) {return;}
	js = d.createElement('script'); js.id = id; js.async = true;
	js.src = "//connect.facebook.net/fr_FR/all.js";
	ref.parentNode.insertBefore(js, ref);
}(document));

// Init the SDK upon load
window.fbAsyncInit = function()
{
	FB.init({
		appId  : '<?= FB_APP_ID ?>',
		<? if (SITE_MODE!='DEV') : ?>
		channelUrl : '//<?=SERVER_NAME?>/fb-channel.php', // Channel File
		<? endif; ?>
		logging: false, // logging
		status : true, // check login status
		cookie : true, // enable cookies to allow the server to access the session
		xfbml  : false  // parse XFBML
	});
	if (window.adaAsyncInit)
		adaAsyncInit();
}
// ]]>
</script>
