<?php
/**
* Stocke l'ensemble des r�gles de Yield pour une agence et un type de v�hicule
*/
class AgencePromoYield_Collection extends Ada_Collection
{
	/** @var Agence */
	protected $_agence;
	/** @var string */
	protected $_type = null;
	protected $_rulles = array();
	
	static public function factory(Agence $agence, $type)
	{
		$x = new AgencePromoYield_Collection();
		if ($agence && isset(Categorie::$TYPE[strtolower($type)])) {
			$x->_agence = $agence;
			$x->_type = $type;

			// pr�parer le SQL pour r�cup�rer l'esnsemble des r�gles
			$sql = sprintf("select * from agence_promo_yield where code_base='%s' and code_groupe='%s' and type='%s' order by categorie, jauge_min", $agence['code_base'], $agence['code_groupe'], $type);
			$x->loadFromSQL($sql);
		}
		return $x;
	}
	/**
	* Renvoie l'agence associ�e
	* @returns Agence
	*/
	public function getAgence() {
		return $this->_agence;
	}
	/**
	* Renvoie le type de v�hicule
	* @returns string
	*/
	public function getTypeVehicule() {
		return $this->_type;
	}
	/**
	* @param Ada_Object
	* @return Ada_Collection
	*/
	public function add(Ada_Object $item) {
		// conserver en indexant par cat�gorie
		$this->_rules[$item['categorie']][] = $item;
		return parent::add($item);
	}
	/**
	* Renvoie si une r�gle de Yield existe pour cette cat�gorie
	* 
	* @param string $mnem
	* @returns bool
	*/
	public function exists($mnem) {
		return isset($this->_rules[$mnem]);
	}
	/**
	* Renvoie la r�gle pour la cat�gorie et la jauge courante
	* 
	* @param string $mnem
	* @param int $jauge
	* @returns AgencePromoYield
	*/
	public function find($mnem, $jauge) {
		if ($this->exists($mnem)) {
			foreach($this->_rules[$mnem] as $yield) {
				if ($jauge >= $yield['jauge_min'] &&  $jauge <= $yield['jauge_max']) {
					return $yield;
				}
			}
		}
		return null;
	}
}
?>
