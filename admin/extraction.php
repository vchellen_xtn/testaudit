<?

require_once("secure.php"); 
require_once("lib.php");
require("clientys.php");
require("comptabilite.php");

if (!$adminUser->hasRights(Acces::SB_ADMIN))
	die("Vous n'avez pas les droits n�cessaires !");

// identifiant
$id =  (int) getarg("id");
if (!$id) $id = null;

// si l'extraction est demand�e
if ($_POST['extraire'])
{
	$debut = euro_iso($_POST['debut']);
	$fin = euro_iso($_POST['fin']);
	$canal = $_POST['canal'];
	$type = $_POST['type'];
	$reseau = $_POST['reseau'];
	$zone = $_POST['zone'];

	// on fait quelques v�rifications
	$err = '';
	if (!preg_match('/^\d{4}-\d{2}-\d{2}$/', $debut)) {
		$err .= "La date de d�but n'est pas correcte !<br/>";
	}
	if (!preg_match('/^\d{4}-\d{2}-\d{2}$/', $fin)) {
		$err .= "La date de fin n'est pas correcte !<br/>";
	}
	if (!empty($zone) && !preg_match('/^[a-z]{2}$/', $zone)) {
		$err .= "La zone n'est pas correcte !<br/>";
	}
	if (!empty($canal) && !preg_match('/^(WEB|TEL)$/', $canal)) {
		$err .= "Le canal n'est pas correct !<br/>";
	}
	if (!preg_match('/^(ADA|POINTLOC)$/', $reseau)) {
		$err .= "Le r�seau n'est pas correct !<br/>";
	}
	if (!preg_match('/^[a-z_]+$/', $type)) {
		$err .= "Le type n'est pas correct !<br/>";
	}
	if (strtotime($debut) > strtotime($fin))  {
		$err.= 'La date de fin de p�riode ('.$_POST['fin'].') est ant�rieure � la date de d�but de p�riode ('.$_POST['debut'].')!<br>';
	}
	if (strtotime('+1 day', strtotime($fin)) > time()) {
		$err.= 'La fin de la p�riode ('.$_POST['fin'].') doit �tre termin�e!<br/>';
	}
	
	if (!$err) {
		// exporter le ficier CLIENTYS ou COMPTABILITE
		header('Content-Type: text/plain');
		header('Content-Disposition: attachment; filename="'.preg_replace('/\_+/', '_', join('_', array($type, $reseau, $canal, strtoupper($zone), $debut, $fin))).'.txt"');

		$lignes = 0;
		if ($type=='clientys_resa')
			$lignes = clientys_export2 ($zone, $reseau, $canal, $debut, $fin);
		else if ($type=='clientys_pro')
			$lignes = clientys_export2 ($zone, $reseau, $canal, $debut, $fin, 'PRO');
		else if ($type=='clientys_coupon')
			$lignes = clientys_export2 ($zone, $reseau, $canal, $debut, $fin, 'COU');

		// si l'enregistrement n'existait pas on le cr�e
		if (!$id)
		{
			$a = array(
				'id'	=> '',
				'zone' => $zone,
				'reseau' => $reseau,
				'type' => $type,
				'canal'=> $canal,
				'debut'=> $debut,
				'fin'  => $fin,
				'lignes'=> $lignes,
			);
			save_record('compta_extraction', $a);
			$id = mysql_insert_id();
		}
		exit();
	}
}
// on r�cup�rer l'enregistrement pour l'affichage
if ($id)
{
	$rs = sqlexec("select * from compta_extraction where id = ".$id);
	if (!$rs || !($row = mysql_fetch_assoc($rs)))
		die("Aucun enregistrement correspondant.");
	$debut = date('d/m/Y',strtotime($row['debut']));
	$fin = date('d/m/Y',strtotime($row['fin']));
}
else
{
	$date = strtotime('last month');
	$debut = date('01/m/Y', $date);
	$fin = date('t/m/Y',$date);
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script language="javascript" src="../scripts/check.js"></script>
	<script>
	<!-- //
	// doSubmit
	function doSubmit(frm)
	{
		var err = "";
		if(!frm.debut.value.length || !ValidateDate(frm.debut))
			err += "La date de d�but '"+frm.debut.value+"' est invalide!\n";
		if(!frm.fin.value.length || !ValidateDate(frm.fin))
			err += "La date de d�but '"+frm.fin.value+"' est invalide!\n";
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		return true;
	}
	// -->
	</script>
</head>
<body>

<form action="" method="post" onsubmit="return doSubmit(this)">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre">
		<img src="img/puce.gif" align="absmiddle" hspace="1">
		<?= "Extraction Clientys"; ?>
	</th>
</tr>
<tr>
	<td>
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<? if ($err) echo '<tr><td colspan="2" style="color:red"><strong>'.$err.'</strong></td></tr>'; ?>
		<tr>
			<td>ID</td>
			<td>
				<? echo $id; echo '<input type="hidden" name="id" value="'.$id.'">'; ?>
			</td>
			<td><label for="zone">Zone</label></td>
			<td>
			<?
			if ($id) {
				echo getsql("select nom from zone where id='".$row['zone']."'");
				echo '<input type="hidden" name="zone" value="'.$row['zone'].'"/>';
			} else {
				selectarray ('zone', $adminUser->getZones(), $adminUser->getCurrentZoneID(), '-- toutes les zones --');
			}
			?>
			</td>
		</tr>
		<tr>
			<td><label for="debut">D�but</label></td>
			<td>
				<input type="text" name="debut" id="debut" value="<?=$debut?>" size="11" maxlength="10" <?=($id ? 'readonly' : '')?> style="width: inherit;"/>
				<em>(jj/mm/aaaa)</em>
			</td>
			<td><label for="fin">Fin</label></td>
			<td>
				<input type="text" name="fin" id="fin" value="<?=$fin?>" size="11" maxlength="10" <?=($id ? 'readonly' : '')?> style="width: inherit;"/>
				<em>(jj/mm/aaaa)</em>
			</td>
		</tr>
		<tr>
			<td><label for="reseau">R�seau</label></td>
			<td>
			<?
				if ($id) {
					echo $row['reseau'].'<input type="hidden" name="reseau" value="'.$row['reseau'].'"/>';
				} else {
					selectarray ('reseau', array('ADA'=>'ADA','POINTLOC'=>'POINTLOC'), getarg('reseau'), '-- Tous --');
				}
			?>
			</td>
			<td><label for="canal">Canal</label></td>
			<td>
			<?
				if ($id) {
					echo $row['canal'].'<input type="hidden" name="canal" value="'.$row['canal'].'"/>';
				} else {
					selectarray ('canal', array('WEB'=>'WEB','TEL'=>'TEL'), getarg('canal'), '-- Tous --');
				}
			?>
			</td>
		</tr>
		<tr>
			<td>Type</td>
			<td>
			<?
			if ($id)
			{
				echo $row['type'];
				echo '<input type="hidden" name="type" value="'.$row['type'].'"/>';
			}
			else
			{
				$a = array();
				foreach(array('clientys_resa','clientys_pro','clientys_coupon') as $k) {
					$a[$k] = $k;
				}
				selectarray ('type', $a, getarg('type'));
			}
			?>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr><td colspan="2" align="right"><input type="submit" name="extraire" value="extraire"></td></tr>
		</table>
	</td>
</tr>
</table>
</form>
</body>
</html>

