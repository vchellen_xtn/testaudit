<?php
/** @var Agence */ $agence = $this;
?>
<? if ($agence->isPointDeLivraison()) : ?>
<div class="agence-livraison">
	<img src="../css/img/logo-ada--service-livraison.png" alt="ADA Service Livraison"/>
	<div class="agence-livraison--explications">
		Ici, nous n'avons pas de locaux...
		<br/>...mais des id�es !
		<br/>
		<br/> <span class="blanc">Nous vous attendons <?php echo Agence::$EMPLACEMENT_LIVRAISON[$agence['emplacement']];?>...
		<br/>...et vous y ramenons !</span>
		<br/>
		<br/>Nous vous contactons avant votre arriv�e pour convenir d'un rendez-vous.
	</div>
</div>
<? endif; ?>
