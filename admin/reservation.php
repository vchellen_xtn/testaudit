<?
	require_once("secure.php"); 
	require_once("lib.php");


	// identifiant
	$id = (int) getarg('id');
	$reservation = Reservation::factory($id, true);
	if (!$reservation)
		die("Aucun enregistrement correspondant.");
	$client = $reservation->getClient();
	$conducteur = $reservation->getConducteur();
	$agence = $reservation->getAgence();
	$categorie = $reservation->getCategorie();
	$coupon = $reservation->getPartenaireCoupon();
	$options = $reservation->getResOptions();
	$resInfo = $reservation->getResInfo();
	// si la demande de relance est faite
	if ($_GET['rpc_reset'])
	{
		$reservation->getResRpc()->doReset();
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	
	<script language="javascript" src="../scripts/check.js"></script>
	<script>
	// <![CDATA[
	// doCancel
	function doCancel(id)
	{
		if (confirm("Voulez-vous r�ellement annuler cette r�servation ?"))
			window.open("annulation.php?id=" + id);
		return false;
	}
	function doRpcReset(id)
	{
		if (confirm("Voulez-vous r�ellement renvoyer cette r�servation ?\nAttention! Cette op�ration est irr�versible et peut cr�er des doublons dans le syst�me de r�servation."))
			window.location.href = "reservation.php?id=" + id + "&rpc_reset=1";
		return false;
	}
	// ]]>
	</script>
</head>
<body>
<form action="" method="post" onsubmit="return false;">
<table class="framed" border="0" cellspacing="0" cellpadding="4" style="float: left;">
<tr>
	<th class="bg_titre">
		<img src="img/puce.gif" align="absmiddle" hspace="1">
		Reservation <?if ($id) echo '#'.$id?>
		<? if ($reservation['ref']) echo ' ('.$reservation['ref'].')'; ?>
		<? if ($reservation['client_id']) { ?>
		- Client <a href="client.php?id=<?=$reservation['client_id']?>"><?='(#'.$reservation['client_id'].') '.$client['email']?></a>
		<? } ?>
	</th>
</tr>
<tr>
	<td>
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="15%">
		<col width="33%">
		<col width="15%">
		<col width="35%">
		<tr><th colspan="4">Demande : <?=$reservation['canal']?><? if ($reservation['agenct']) echo ' - '.$reservation['agent'];?></th></tr>
		<tr>
			<td>Agence</td>
			<td colspan="3"><a href="agence_pdv.php?id=<?=$agence['pdv']?>"><?=$agence['id'].' - '.$agence['nom']?></a></td>
		</tr>
		<tr>
			<td>Cat�gorie</td>
			<td><?=$categorie['type'].'/'.$categorie['mnem'].' : '.$categorie['nom']?></td>
			<td>Dur�e</td>
			<td><?=$reservation->getDuree();?></td>
		</tr>
		<tr>
			<td>D�but</td>
			<td><?=$reservation->getDebut()?></td>
			<td>Fin</td>
			<td><?=$reservation->getFin()?></td>
		</tr>
		<tr>
			<td>Distance</td>
			<td><?=($reservation['distance'] ? $reservation['distance'].' km' : 'non pr�cis�');?></td>
			<td>Coupon</td>
			<td>
			<?
				if (!$coupon)
					echo '--';
				else
				{
					echo '<a href="partenaire_coupon.php?id='.$coupon['id'].'" title="'.htmlspecialchars($coupon['partenaire_nom']).'">';
					echo Partenaire::$ORIGINES[$coupon['origine']].' - '.$coupon['code'];
					echo '</a>';
				}
			?>
			</td>
		</tr>
		<tr>
			<td>Age</td>
			<td><?=$AGE[$reservation['age']]?></td>
			<td>Permis</td>
			<td><?=$PERMIS[$reservation['permis']]?></td>
		</tr>
		<? if ($resInfo['commentaire']) : ?>
		<tr>
			<td>Commentaire</td>
			<td colspan="3"><?=nl2br($resInfo['commentaire'])?></td>
		</tr>
		<? endif; ?>
		<tr>
			<th colspan="4">Offre : 
			<?
				echo $reservation['forfait_type'];
				if ($reservation['forfait_type']!='LOCAPASS')
					echo ' - '.$reservation['forfait'].' - '.$reservation['jours'].' jours - '.$reservation->getKm();
			?>
			</th>
		</tr>
		<? if ($reservation['forfait_type']=='LOCAPASS') : ?>
		<tr>
			<td>Coupon</td>
			<td colspan="3"><?=$coupon['code'].' : '.$coupon['nom']?></td>
		</tr>
		<tr>
			<td>Partenaire</td>
			<td colspan="3"><?=$coupon['partenaire_nom']?></td>
		</tr>
		<? else : ?>
		<tr>
			<td>Tarif</td>
			<td align="right"><?=money($reservation['tarif'])?></td>
			<td colspan="2"></td>
		</tr>
		<? if ($reservation['reduction']) : ?>
		<tr>
			<td>Promotion<? if ($reservation['reduction'] < 0) echo ' invers�e';?></td>
			<td align="right"><?=sprintf('%+.2f', -$reservation['reduction'])?>&nbsp;�</td>
			<td colspan="2">
				<? if ($pID = (int) $reservation['promotion']) : ?>
				<a href="agence_promo_forfait.php?id=<?=$pID;?>">
				<?
				$promo = AgencePromoForfait::factory($pID, true);
				if (!$promo || $promo->isEmpty())
					$promo = PromotionForfait::factory($pID, true);
				echo sprintf('#%ld - %s', $pID, $promo['nom']);
				if ($promo['archive'])
					echo ' (Archiv�)';
				?>
				</a>
				<? endif; ?>
			</td>
		</tr>
		<? endif; ?>
		<? if ($reservation['nego']) : ?>
		<tr>
			<td valign="top">N�gociation</td>
			<td align="right" valign="top"><?=sprintf('%+.2f', -$reservation['nego'])?>&nbsp;�</td>
			<td colspan="2">
				<? if ($nego = $reservation->getResNego()) : ?>
				<a href="agence_promo_forfait.php?id=<?=$nego['promotion'];?>"><? echo sprintf('#%ld - %s', $nego['promotion'], $nego->getPromotion()->getData('nom')); ?></a>
				<br/><a href="promotion_negociation.php?id=<?=$nego['negociation'];?>"><? echo sprintf('#%ld - %s', $nego['negociation'], $nego->getNegociation()->getData('nom')); ?></a>
				<br/><?=sprintf('%ld �tapes - Prix minimum: %ld &euro;', $nego['steps'], $nego['seuil']);?>
				<? endif; ?>
			</td>
		</tr>
		<? endif; ?>
		<? if ($reservation['yield']) : ?>
		<tr>
			<td>Yield</td>
			<td align="right"><?=sprintf('%+.2f', $reservation['yield'])?></td>
			<td>Variation</td>
			<td align="right"><?=sprintf('%+.2f', $reservation->getYieldVariation()); ?>%</td>
		</tr>
		<? endif; ?>
		<? if ($reservation['surcharge'] > 0) : ?>
		<tr>
			<td>Surcharge</td>
			<td align="right"><?=money($reservation['surcharge'])?></td>
			<td colspan="2"></td>
		</tr>
		<? endif; ?>
		<tr>
			<td>Options</td>
			<td align="right"><?=money($reservation->getPrixOptions());?></td>
			<td colspan="2"></td>
		</tr>
		<? if ($coupon['origine']=='webpro' && ($reglement = $coupon->getPartenaireReglement()) && $reglement['reservation']==$reservation['id']) :?>
		<tr>
			<td>Adh�sion Pro</td>
			<td align="right"><?=money($reglement->getPrixTTC());?></td>
			<td colspan="2"></td>
		</tr>
		<? endif; ?>
		<? if ($code = $reservation->getPromotionCode()) : ?>
		<tr>
			<td>R�duction</td>
			<td align="right">-<?=money($code['reduction']);?></td>
			<td>Code</td>
			<td>
				<a href="promotion_code.php?id=<?=$code['id']?>"><?=$code['code']?></a>
				<? if ($pID = $code['promotion']) : ?>
				<br/>
				<a href="agence_promo_forfait.php?id=<?=$pID?>">
				<?
				$promo = AgencePromoForfait::factory($pID, true);
				if (!$promo || $promo->isEmpty())
					$promo = PromotionForfait::factory($pID, true);
				echo sprintf('#%ld - %s', $pID, $promo['nom']);
				if ($promo['archive'])
					echo ' (Archiv�)';
				?>
				</a>
				<? endif; ?>
			</td>
		</tr>
		<? endif; ?>
		<tr>
			<td>Total</td>
			<td align="right"><?=money($reservation['total'])?></td>
			<td colspan="2"></td>
		</tr>
		<? endif; // !locapass?>
		<tr>
			<th colspan="3">Statut: <?=$reservation->getStatut().' ('.$reservation['statut'].')'?></th>
			<th style="text-align: right;">&nbsp;
			<? if ($id && ($adminUser->hasRights(Acces::SB_GESTION) || $adminUser->hasRights(Acces::SB_OPERATEUR)) && $reservation['statut']=='P') : ?>
				<input type="button" onclick="return doCancel(<?=$id?>);" name="cancel" value="annuler"/>
			<? endif; ?>
			</th>
		</tr>
		<tr>
			<td>Cr�ation</td>
			<td><?=$reservation->getDate('creation')?></td>
			<td>Validation</td>
			<td><?=$reservation->getDate('validation')?></td>
		</tr>
		<? if ($reservation['paiement']) : ?>
		<tr>
			<td>Paiement</td>
			<td><?=$reservation->getDate('paiement')?></td>
			<td>Mode</td>
			<td><?=Transaction::$PAIEMENT_LABEL[$reservation['paiement_mode']].' ('.$reservation['paiement_mode'].')'?></td>
		</tr>
		<? endif; ?>
		<? if ($reservation['annulation']) : ?>
		<tr>
			<td>Annulation</td>
			<td><?=$reservation->getDate('annulation')?></td>
			<td>Motif</td>
			<td>
			<?
				foreach(explode(',', $reservation['annulation_motif']) as $motif)
					echo $motif.' : '.ResAnnulationMotif::factory($motif).'<br/>';
			?>
			</td>
		</tr>
		<? endif; ?>
		<? if ($reservation['remboursement']) : ?>
		<tr>
			<td>Remboursement</td>
			<td><?=$reservation->getDate('remboursement');?></td>
			<td colspan="2"></td>
		</tr>
		<tr>
			<td>Rembours�</td>
			<td align="right"><?=money($reservation['rembourse'])?></td>
			<td>Mode</td>
			<td><?=Transaction::$PAIEMENT_LABEL[$reservation['remboursement_mode']].' ('.$reservation['remboursement_mode'].')'?></td>
		</tr>
		<tr>
			<td>Reste � ADA</td>
			<td align="right"><?=money($reservation['total']-$reservation['rembourse'])?></td>
			<td>Frais dossier</td>
			<td align="right"><?=money($reservation['fraisdossier'])?></td>
		</tr>
		<? endif; ?>
		<?  if (($rpc = $reservation->getResRpc()) && !$rpc->isEmpty()) : ?>
		<tr>
			<th colspan="4">
				<?=$rpc->getSystem(). ' : '.$rpc->getCode();?>
				<? if ($id && $adminUser->hasRights(Acces::SB_GESTION) && $reservation['statut']=='P' && $rpc->getCode()) : ?>
				<input type="button" onclick="return doRpcReset(<?=$id?>);" name="rpc_reset" value="relancer"/>
				<? endif; ?>
			</th>
		</tr>
		<tr>
			<td>Validation</td>
			<td><? if ($rpc['validation']) echo date('d/m/Y H:i', strtotime($rpc['validation']));?></td>
			<td>Annulation</td>
			<td><? if ($rpc['annulation']) echo date('d/m/Y H:i', strtotime($rpc['annulation']));?></td>
		</tr>
		<? endif; ?>
		<? if (($logPaybox = $reservation->getLogPaybox()) && (!$logPaybox->isEmpty())) : ?>
		<tr><th colspan="4">PAYBOX : Transactions</th></tr>
		<tr>
			<td colspan="4">
				<table border="0" cellpadding="2" cellspacing="0">
					
					<tr><th>Date</th><th>N� Appel</th><th>N� Trans</th><th>Autorisation</th><th>R�ponse</th></tr>
					<?
					foreach ($logPaybox as $log)
					{
						$a = $log->toArray(array('creation','numappel','numerotrans','autorisation','code_reponse'));
						$a['creation'] = date('d/m/Y H:i', strtotime($a['creation']));
						$a['code_reponse'] .= ' - '.Paybox::getMessage($a['code_reponse'],'PB',false);
						echo '<tr><td>'.join('</td><td>',$a).'</td></tr>'."\n";
					}
					?>
				</table>
			</td>
		</tr>
		<? endif; ?>
		</table>
	</td>
</tr>
</table>
<?
if ($reservation)
{
	echo '<table class="framed" border="0" cellspacing="0" cellpadding="4" style="float:left; margin-left:5px; width:300px;">';
	// adresse Client
	echo '<tr><th class="bg_titre"><img src="img/puce.gif" align="absmiddle" hspace="1">Conducteur</th></tr>';
	echo '<tr><td>';
	echo $conducteur['email']."<br/>\n";
	if (!$resInfo || $resInfo->isEmpty()) $resInfo = $client;
	if ($resInfo['f_societe'])
		echo $resInfo['f_societe']."\n<br/>";
	echo $conducteur['prenom'].' '.$conducteur['nom'];
	echo "\n<br/>".$resInfo['f_adresse'];
	if ($resInfo['f_adresse2'])
		echo "\n<br/>".$resInfo['f_adresse2'];
	echo "\n<br/>".$resInfo['f_cp'].' '.$resInfo['f_ville'];
	echo "\n<br/>".$resInfo['f_pays_nom'];
	foreach(array('tel','gsm') as $k) {
		foreach(array($conducteur[$k], $client[$k]) as $v) {
			if ($v) {
				echo "\n<br/>T�l: ".$v;
				break;
			}
		}
	}
	echo '</td></tr>';
	// options
	echo '<tr><th class="bg_titre"><img src="img/puce.gif" align="absmiddle" hspace="1">Options</th></tr>';
	echo' <tr><td>';
	if ($options->isEmpty())
		echo 'Aucune option.';
	else
	{
		echo '<table border="0" cellpadding="2" cellspacing="0">';
		foreach($options as /** @var ResOption */ $option)
			echo '<tr><td>'.$option['option'].'</td><td align="right">'.$option->getPrix().'</td><td>'.(string)$option.'</td><td></tr>';
		echo '</table>';
	}
	echo '</td></tr>';
	echo '<tr><th class="bg_titre"><img src="img/puce.gif" align="absmiddle" hspace="1">Facturations</th></tr>';
	echo '<tr><td>';
	$facturations = $reservation->getFacturations();
	if ($facturations->isEmpty())
		echo 'Aucune facturation';
	else
	{
		echo '<table border="0" cellpadding="2" cellspacing="0">';
		foreach($facturations as /** @var Facturation */ $f)
		{
			echo '<tr>';
			echo '<td>'.$f['facturation'].'</td>';
			echo '<td>'.$f['type'].'</td>';
			echo '<td align="right">'.$f['montant'].'�</td>';
			echo '<td><a href="../'.$f->getLink().'" target="_blank">voir</a></td>';
			echo '</tr>';
		}
		echo '</table>';
	}
	echo '</td></tr>';
	echo '</table>';
}
?>
</form>

</body>
</html>
