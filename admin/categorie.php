<?
	require_once("secure.php"); 
	require_once("lib.php");

	// identifiant
	$id = (int) getarg('id');
	if (!$id) $id = null;
	
	// suppression
	if ($id && getarg("delete")) 
	{
		$sql = "delete from categorie where id = '".$id."'";
		getsql($sql);
		unset($id);
	}
	
	// modification, cr�ation
	if (getarg("update") || getarg("create"))
	{
		save_record("categorie", $_POST);
		if (!$id) $id = mysql_insert_id();

		// upload des fichiers pour la V2
		if (count($_FILES) && isset(Categorie::$TYPE[strtolower($_POST['type'])]) && preg_match('/^[0-9A-Z]{1,2}[\+\\\']?$/', $_POST['mnem'])) {
			$dir = 'fichiers/categories/'.strtolower($_POST['type']);
			if (!is_dir('../'.$dir))
				mkdir('../'.$dir, 0755);
			foreach ($_FILES as $key => $f) {
				if (!in_array($key, array('url_sml','url_med','url_big'))) continue;
				$ext = strtolower(substr($f['name'],-4));
				if ($ext != '.png') continue;
				$fname = strtoupper($_POST['mnem']).'-'.substr($key, -3).$ext;
				move_uploaded_file($f['tmp_name'], '../'.$dir.'/'.$fname);
			}
		}
	}

	if ($id)
	{
		$rs = sqlexec('select * from categorie where id = "'.addslashes($id).'"');
		if (!$rs || !($row = mysql_fetch_assoc($rs)))
			die("Aucun enregistrement correspondant.");
		foreach(array('sml','med','big') as $k)
		{
			if (is_file($fname = '../fichiers/categories/'.strtolower($row['type']).'/'.strtoupper($row['mnem']).'-'.$k.'.png'))
				$row['url_'.$k] = substr($fname, 3);
		}
	}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script language="javascript" src="../scripts/check.js"></script>
	<script>
	<!-- //

	// doDelete
	function doDelete(frm)
	{
		return (confirm("Voulez-vous r�ellement supprimer cet �l�ment ?\nCette op�ration est irr�versible."));
	}
	
	// doSubmit
	function doSubmit(frm)
	{
		var err = "";
		if(!frm.mnem.value.length)
			err += "Veuillez indiquer l'ID\n";
		if(!ValidateSelect(frm.type))
			err += "Veuillez indiquer le type\n";
		if(!frm.nom.value.length)
			err += "Veuillez indiquer le nom\n";
		if(!frm.description.value.length)
			err += "Veuillez indiquer une description\n";
		if(!frm.url_sml.value.length)
			err += "Veuillez indiquer l'image de petite taille\n";
		if(!frm.url_med.value.length)
			err += "Veuillez indiquer l'image de taille moyenne\n";
		if(!frm.url_big.value.length)
			err += "Veuillez indiquer l'image de grande taille\n";
		if(frm.longueur.value.length && !frm.longueur.value.match(/^\d+(\.\d+)?$/))
			err += "La longueur est invalide\n";
		if(frm.largeur.value.length && !frm.largeur.value.match(/^\d+(\.\d+)?$/))
			err += "La largeur est invalide\n";
		if(frm.entre_roues.value.length && !frm.entre_roues.value.match(/^\d+(\.\d+)?$/))
			err += "La distance entre roues  est invalide\n";
		if(frm.hauteur.value.length && !frm.hauteur.value.match(/^\d+(\.\d+)?$/))
			err += "La hauteur est invalide\n";
		if(!ValidateSelect(frm.carburant))
			err += "Veuillez indiquer le carburant\n";
		if(frm.reservoir.value.length && !frm.reservoir.value.match(/^\d+$/))
			err += "Veuillez indiquer la contenance du r�servoir\n";
		if(frm.conso_90.value.length && !frm.conso_90.value.match(/^\d+(\.\d+)?$/))
			err += "Veuillez indiquer la consomation\n";
		if(!ValidateSelect(frm.age))
			err += "Veuillez indiquer l'age\n";
		
		if(!ValidateSelect(frm.permis))
			err += "Veuillez indiquer le permis\n";
		
		if(frm.charge.value.length && !ValidateInteger(frm.charge))
			err += "Veuillez indiquer la charge\n";
		
		if(!frm.km_sup.value.length || !frm.km_sup.value.match(/^\d+(\.\d{1,2})?$/))
			err += "Veuillez indiquer le prix du kilometre suplementaire\n";
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		return true;
	}

	// -->
	</script>
</head>
<body>
<form action="" method="post" enctype="multipart/form-data">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre">
	<img src="img/puce.gif" align="absmiddle" hspace="1">
<? 
	echo "Categorie";
	if ($id)
	{
		echo ' #'.$id.' - '.$row['mnem'];
		if (!$row['publie']) echo ' - non publi�';
	}
	echo '<input type="hidden" name="id" value="'.$id.'">';
?>
	</th>
</tr>
<tr>
	<td>
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="15%">
		<col width="32%">
		<col width="15%">
		<col width="38%">
		<tr>
			<td><label for="type">Type</label></td>
			<td><? selectarray("type", $TYPE, $row["type"], "--"); ?></td>
			<td>Code</td>
			<td>
				<label for="mnem">ADA:</label>
				<?
					if (isset($row['mnem'])) echo '<strong>'.$row['mnem'].'</strong>';
					echo '<input type="'.(isset($row['mnem']) ? "hidden" : "text").'" id="mnem" name="mnem" maxlength="2" value="'.$row['mnem'].'" size="2" style="width: inherit;">';
				?>
				&nbsp;&nbsp;
				<label for="acriss">ACRISS:</label>
				<input type="text" id="acriss" name="acriss" value="<?=$row['acriss']?>" size="4" maxlength="4" style="width: inherit;">
				<a href="http://www.acriss.org/car-codes/pseudo-car-codes/" target="_blank">info ACRISS</a>
			</td>
		</tr>
		<tr>
			<td><label for="famille">Famille</label></td>
			<td><? selectsql('famille', "select id, nom from vehicule_famille where type='".$row['type']."'order by position", $row['famille'], '--')?></td>
			<td colspan="2">
				<input type="hidden" name="nopromo[]" value="0"/>
				<input type="checkbox" name="nopromo[]" value="1" id="nopromo_1" style="width: inherit;"<? if ($row['nopromo']) echo ' checked';?>/>
				<label for="nopromo_1">Exclure de toute promotion</label>
			</td>
		</tr>
		<tr>
			<td><label for="nom">Nom</label></td>
			<td><input type="text" class="l" id="nom" name="nom" value="<?=stripslashes($row["nom"])?>"></td>
			<td><label for="canon">Canon</label></td>
			<td><input type="text" class="l" id="canon" name="canon" value="<?=stripslashes($row["canon"])?>"></td>
		</tr>
		<tr>
			<td valign="top"><label for="description">Description</label></td>
			<td colspan="3"><input type="text" class="l" name="description" value="<?=stripslashes($row["description"])?>" maxlength="128" size="128"/></td>
		</tr>
		<tr>
			<td valign="top">Commentaire</td>
			<td colspan="3">
				<textarea name="commentaire" rows="1" style="width:100%"><?=stripslashes($row["commentaire"])?></textarea>
			</td>
		</tr>
		<?
		foreach(array('sml'=>'Petite','med'=>'Moyenne','big'=>'Grande') as $k => $v)
		{
			echo "<tr>\n";
			echo '<td>'.$v.'</td>'."\n";
			echo '<td colspan="3">';
			url_ctrl('url_'.$k, $row['url_'.$k]);
			echo '</td>'."\n";
			echo "</tr>\n";
		}
		?>
		<tr>
			<td><label for="portes">Portes</label></td>
			<td>
			<?
				$a = array();
				foreach(array('1','2','3','3 ou 5','4','5') as $k)
					$a[$k] = $k;
				selectarray("portes", $a, $row["portes"], 'aucune'); 
			?>
			</td>
			<td><label for="places">Places</label></td>
			<td>
			<?
				$a = array();
				foreach(array('1','2','2 ou 3','3','4','4 ou 5','5','6','7','8','9') as $k)
					$a[$k] = $k;
				selectarray("places", $a, $row["places"], 'aucune');
			?>
		</tr>
		<tr>
			<td>Valises</td>
			<td><? selectarray("valises", array(0,1,2,3,4), $row["valises"], null); ?></td>
			<td>Sacs</td>
			<td><? selectarray("sacs", array(0,1,2,3,4), $row["sacs"], null); ?></td>
		</tr>
		<tr>
			<td>Longueur (m)</td>
			<td><input type="text" name="longueur" value="<?=$row["longueur"]?>"></td>
			<td>Largeur (m)</td>
			<td><input type="text" name="largeur" value="<?=$row["largeur"]?>"></td>
		</tr>
		<tr>
			<td>Hauteur (m)</td>
			<td><input type="text" name="hauteur" value="<?=$row["hauteur"]?>"></td>
			<td>Entre roues</td>
			<td><input type="text" name="entre_roues" value="<?=$row["entre_roues"]?>"></td>
		</tr>
		<tr>
			<td>Carburant</td>
			<td colspan="3">
			<? selectarray("carburant", array("Essence" => "Essence", "Essence ou diesel" => "Essence ou diesel", "Diesel" => "Diesel", 'Electrique'=>'Electrique'), $row["carburant"], "--"); ?>
			</td>
		</tr>
		<tr>
			<td>R�servoir (l)</td>
			<td><input type="text" name="reservoir" value="<?=$row["reservoir"]?>"></td>
			<td>Conso.&nbsp;/100km</td>
			<td><input type="text" name="conso_90" value="<?=$row["conso_90"]?>"></td>
		</tr>
		<tr>
			<td>Age minimum</td>
			<td><? selectarray("age", $AGE, $row["age"], "--"); ?></td>
			<td>Permis&nbsp;mini.</td>
			<td><? selectarray("permis", $PERMIS, $row["permis"], "--"); ?></td>
		</tr>
		<tr>
			<td>Charge (kg)</td>
			<td><input type="text" name="charge" value="<?=$row["charge"]?>"></td>
			<td>Km supp. (�)</td>
			<td><input type="text" name="km_sup" value="<?=$row["km_sup"]?>"></td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%">
		<tr>
			<td>
<? if ($id) : ?>
				<input type="button" value="opt. interdites" onclick="window.open('catoption.php?type=<?=$row["type"]?>&cat=<?=$id?>','','width=400,height=300'); return false;">
				<input type="button" value="tarifs" onclick="parent.list.location.href='lists/forfait_tarif.php?categorie=<?=$id?>'; return false;">
				<input type="button" value="par zone" onclick="location.href='categorie_zone.php?categorie=<?=$id?>'; return false;">
				<input type="button" value="stop sell" onclick="parent.list.location.href='lists/stopsell.php?categorie=<?=$id?>&type=<?=$row['type']?>'; return false;">
<? endif; ?>
			</td>
			<td align="right">
				<input type="submit" name="<?=$id ? "update" : "create"?>" value="enregistrer" onclick="return doSubmit(this.form);">
<? if ($id && !getsql('select id from reservation where categorie='.$id.' limit 1')) : ?>
		&nbsp;
		<input type="submit" onclick="return doDelete(this.form);" name="delete" value="supprimer">
<? endif; ?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
</form>

</body>
</html>
