<?
	$that = /** @var Page_Faq */ $this;
	$action = $that->getFAQRubrique();
?>
<ul class="menu">
	<? foreach($that->getFAQRubriques() as $k => $lbl) : ?>
		<li class="faq--<?= $k ?>">
			<a href="<?=$k?>.html"<?if ($action==$k) echo ' class="current"'; ?>><?=$lbl;?></a>
		</li>
	<? endforeach; ?>
</ul>