<?php
	/** @var Page_Annuaire */ $that = $this;
?>
<div class="bg">
	<div class="header">
		<h1 class="type-h1"><?=$that->getH1();?></h1>
	</div>
	<div class="content">
		<?
			include($this->getPagePath());
		?>
		<div class="clear"></div>
	</div>
</div>
