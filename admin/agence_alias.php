<?
	require_once('secure.php'); 
	require_once('lib.php');
	require_once('lib.agence.php');

	// agence parente
	$agence = getarg('agence');
	$pdv = getarg('pdv');
	// identifiant
	$id = getarg('id');
	// v�rification des arguments
	foreach(array($id, $pdv, $agence ) as $k)
	{
		if ($k && !Agence::isValidID($k))
			die('Identifiant '.$k.' non valide');
	}
	

	// suppression
	if ($id && getarg("delete")) 
	{
		// suppr alias li�s
		sqlexec("DELETE FROM agence_alias WHERE id ='".$id."'");
		sqlexec("DELETE FROM refnat_canon WHERE type='agence' AND id='".$id."'");
		header('Location: agence_pdv.php?id='.$pdv);
	}
	
	// modification
	if (getarg("update") || getarg("create"))
	{
		$id = save_alias($_POST);
	}

	if($id)
	{
		$row = mysql_fetch_assoc(sqlexec("SELECT * FROM agence_alias WHERE id = '".$id."'"));
		$pdv = $row['pdv'];
		$agence = $row['agence'];
	}
	// nouvel alias
	if(!$row && !$agence && !$pdv)
	{
		die("Veuillez preciser un id ou une agence et un point de vente");
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script language="javascript" src="../scripts/check.js"></script>
	<script>
	<!--
	// doDelete
	function doDelete(frm)
	{
		return (confirm("Voulez-vous r�ellement supprimer cet �l�ment ?\nCette op�ration est irr�versible."));
	}

	// doSubmit
	function doSubmit(frm)
	{
		var err = "";
		if(!frm.nom.value.length)
			err += "Veuillez indiquer le nom\n";
		if (frm.zone.value=="fr" && !frm.cp.value.match(/^[0-9]{5}$/))
			err += "Le code postal est incorrect\n";
		if(!frm.ville.value.length)
			err += "Veuillez indiquer la ville\n";
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		return true;
	}
	//-->
	</script>
</head>
<body>

<form action="" method="post" onsubmit="return doSubmit(this);">
	<input type="hidden" name="agence" value="<?=$agence?>" />
	<input type="hidden" name="pdv" value="<?=$pdv?>" />
	<input type="hidden" name="zone" value="<?=$row['zone']?>"/>
<table class="framed" border="0" cellspacing="0" cellpadding="4" style="float:left;">
<tr>
	<th class="bg_titre">
	<img src="img/puce.gif" align="absmiddle" hspace="1">
		<a href="agence.php?id=<?=$agence?>">Agence <?=$agence?></a> � 
		<a href="agence_pdv.php?id=<?=$pdv?>">Point de vente <?=$pdv?></a> � Alias
	</th>
</tr>

<tr>
	<td>
	<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="15%">
		<col width="32%">
		<col width="15%">
		<col width="33%">
		<tr>
			<td><label for="id">identifiant</label></td>
			<td>
			<?
				echo $row['id'];
				echo '<input type="hidden" id="id" name="id" value="'.$row['id'].'" maxlength="6">';
			?>
			</td>
			<td><label for="emplacement">Emplacement</label></td>
			<td><? selectarray('emplacement', Agence::$EMPLACEMENT, $row['emplacement']); ?></td>
		</tr>
		<tr>
			<td><label for="nom">Nom</label></td>
			<td><input type="text" id="nom" name="nom" value="<?=$row["nom"]?>" maxlength="64"></td>
			<td><label for="canon">Canonique</label></td>
			<td>
				<input type="text" id="canon" name="canon" value="<?=$row["canon"]?>" maxlength="64" readonly/>
				<br/><a href="#" onclick="document.getElementById('canon').value=''; return false;">effacer</a>
			</td>
		</tr>
		<tr>
			<td><label for="ville">Ville</label></td>
			<td><input type="text" id="ville" name="ville" value="<?=$row["ville"]?>" maxlength="64"></td>
			<td><label for="cp">Code postal</label></td>
			<td><input type="text" id="cp" name="cp" value="<?=$row["cp"]?>" maxlength="5" size="5" style="width: inherit;"></td>
		</tr>
		<tr>
			<td>D�pt</td><td><?=$row['dept']?></td>
			<td>Recherche</td>
			<td><?=$row['recherche']?></td>
		</tr>
		<tr>
			<td><label for="agglo">Agglom�ration</label></td>
			<td><input type="text" id="agglo" name="agglo" value="<?=htmlspecialchars($row['agglo'])?>" maxlength="64"></td>
			<td colspan="2"><em>pour rattacher un alias � une agglom�ration diff�rnete de la ville</em></td>
		</tr>
		<tr>
			<td><label for="pageweb">Comportement</label></td>
			<td colspan="3">
			<?
				$a = array('1' => "Page Web : cet alias a une page Web et peut �tre trouv� dans la liste des agences sur la page d'accueil");
				if ($id != $pdv)
					$a['0'] = "Recherche et redirection : cet alias n'a pas de page web mais peut �tre trouv� dans la liste des agences sur la page d'accueil";
				selectarray('pageweb', $a, $row['pageweb']);
			?>
			</td>
		</tr>
		<? if (!is_null($row['refnat'])) : ?>
		<tr>
			<td>R�f. Nat.</td>
			<td colspan="3">
				<strong>Pour infirmation :</strong>
				<a target="_blank" href="refnat.php?tbl=agence&id=<?=$row['id'];?>">Il y a un texte sp�cifique pour cette agence !</a>
			</td>
		</tr>
		<? endif; ?>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%">
		<tr>
			<td valign="top">
			<? if ($id) : ?>
			<input type="button" onclick="parent.list.location.href='lists/reservation.php?alias=<?=$id?>'" value="r�servations">
			<? endif; ?>
			</td>
			<td align="right" valign="top">
				<input type="submit" name="<?=$id ? "update" : "create"?>" value="enregistrer">
				<? if ($id && $id != $pdv && is_null($row['refnat'])) : ?>
				&nbsp;
				<input type="submit" onclick="return doDelete(this.form);" name="delete" value="supprimer">
				<? endif; ?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<?
	show_pdv($agence, null, $id);
?>
<form>
</body>
</html>
