<?

require_once(BP.'/lib/lib.tcp.php');
/**
* Acc�s simplifi� au g�ocodage de Googel
* Documentation: http://code.google.com/intl/fr-FR/apis/maps/documentation/geocoding/
* Exemple : http://maps.googleapis.com/maps/api/geocode/json?address=3%20rue%20Herg�%2016000%20ANGOULEME&sensor=false
*/
class Geocoding extends Ada_Object
{
	const GEOCODING_SERVER = 'http://maps.googleapis.com';
	const GEOCODING_URL = '/maps/api/geocode/json';
	
	/**
	* Renvoie un objet Geocondig pr�t � �tre analys�
	* 
	* @param string $adresse en UTF-8
	* @param string	 $region puor pr�ciser le pays de recherche
	* @param string	 $language langue de r�ponse
	* @param string $sensor 'true' ou 'false'
	* @return Geocoding
	*/
	static public function factory($adresse, $region=null, $language='fr', $sensor='false')
	{
		// les param�gres
		$a = array('address' => $adresse,'sensor'	=> $sensor);
		if ($region) 
			$a['region'] = $region;
		if ($language) 
			$a['language'] = $language;
		// constituer l'url
		$url = self::GEOCODING_URL.'?'.http_build_query($a);
		// lire les donn�es
		
		return new Geocoding(json_decode(@get_http_request(self::GEOCODING_SERVER, $url), true));
	}
	public function isOK()
	{ 
		return ($this->_data['status'] == 'OK');
	}
	public function getNumberOfResults()
	{
		return count($this->_data['results']);
	}
	public function getResults($index = null) 
	{
		if (!$index)
			return $this->_data['results']; 
		return $this->_data['results'][$index]; 
	}
	/**
	**	Revoie le type de "localisation"
	**		"ROOFTOP" indicates that the returned result is a precise geocode for which we have location information accurate down to street address precision.
	**		"RANGE_INTERPOLATED" indicates that the returned result reflects an approximation (usually on a road) interpolated between two precise points (such as intersections). Interpolated results are generally returned when rooftop geocodes are unavailable for a street address.
	**		"GEOMETRIC_CENTER" indicates that the returned result is the geometric center of a result such as a polyline (for example, a street) or polygon (region).
	**		"APPROXIMATE" indicates that the returned result is approximate.
	**	@param double $lat latitude � renseigner
	**	@param double $lng longitude � renseigner
	**	@param int $index r�sultat � renvoyer
	**	@return string location_type
	*/
	public function getLocation(&$lat, &$lng, $index=0)
	{
		$geometry = $this->_data['results'][$index]['geometry'];
		$lat = $geometry['location']['lat'];
		$lng = $geometry['location']['lng'];
		return $geometry['location_type'];
	}
	public function getFormattedAddress($index = 0)
	{
		return $this->_data['results'][$index]['formatted_address'];
	}
}
?>