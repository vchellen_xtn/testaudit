<?
	require_once("secure.php"); 
	require_once("lib.php");

	// identifiant
	$id = getarg("id");
	if ($id && !preg_match('/^[a-z0-9]{3,4}$/i', $id))
		die("L'identifiant n'est pas correct !");
	
	// suppression
	if ($id && getarg("delete")) 
	{
		$sql = "delete from `option` where id = '".$id."'";
		getsql($sql);
		$sql = "delete from option_tarif where `option`='".$id."'";
		getsql($sql);
		unset($id);
	}
	
	// modification, cr�ation
	if (getarg("update") || getarg("create"))
	{
		// seuls les administrateurs peuvent modifier la description d'une option
		if ($adminUser->hasRights(Acces::SB_ADMIN))
			save_record('option', $_POST);
	}
	// obtenir les donn�es
	if ($id)
	{
		$sql = "select o.*";
		$sql.=" from `option` o";
		$sql.=" where o.id = '".$id."'";
		$rs = sqlexec($sql);
		if (!$rs || !($row = mysql_fetch_assoc($rs)))
			die("Aucun enregistrement correspondant.");
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script language="javascript" src="../scripts/script.js"></script>
	<script language="javascript" src="admin.js"></script>
	<script type="text/javascript">
	<!-- //
	// doDelete
	function doDelete(frm)
	{
		return (confirm("Voulez-vous r�ellement supprimer cet �l�ment ?\nCette op�ration est irr�versible."));
	}
	
	// doSubmit
	function doSubmit(frm)
	{
		var err = "";
		if(!frm.id.value.length)
			err+= "Veuillez indiquer l'Id de l'option\n";
		if(!frm.nom.value.length)
			err+= "Veuillez indiquer le nom\n";
		if(frm.fraction && (!frm.fraction.value.match(/^\d+$/) || parseInt(frm.fraction.value) < 1))
			err += "Veuillez indiquer un fractionnement du prix valide.\n";
		if(frm.delai_min && !frm.delai_min.value.match(/^\d*$/))
			err += "Veuillez indiquer un d�lai minimum valide.\n";
		if (!frm.position.value.match(/^\d{4}$/))
			err += "La position est invalide\n";
		if (frm.quantite_max && !frm.quantite_max.value.match(/^\d*$/))
			err += "La quantit� maximale est invalide\n";
		if (!frm.en_avant.value.match(/^\d*$/))
			err += "La mie en avant est invalide\n";
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		return true;
	}
	// -->
	</script>
</head>
<body>
<form action="" method="post" onsubmit="return doSubmit(this)">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre">
		<img src="img/puce.gif" align="absmiddle" hspace="1">Option
		<? 
		if ($id)
		{
			echo " <strong>$id</strong> &gt; client entre {$row['age_min']} et {$row['age_max']} ans";
			if ($row['agence_statut'] > 0)
			{
				foreach(Agence::$STATUTS as $k => $v)
				{
					if (($k & $row['agence_statut'])==$k)
						echo " &gt; Statut agence $v";
				}
			}
		}
		?>
	</th>
</tr>
<tr>
	<td>
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="15%">
		<col width="35%">
		<col width="15%">
		<col width="35%">
		<tr>
			<td><label for="id">ID</label></td>
			<td><? echo $id; echo '<input type="'.($id ? "hidden" : "text").'" name="id" value="'.$id.'" maxlength="4">'; ?></td>
			<td><label for="facturation">Facturation</label></td>
			<td>
			<? 
				if ($row['facturation'])
					echo $row['facturation'];
				else
					selectarray('facturation', array('ADA'=>'ADA','COURTAGE'=>'COURTAGE','MATERIEL'=>'MATERIEL'), null);
			?>
			</td>
		</tr>
		<tr>
			<td><label for="theme">Th�me</label></td>
			<td>
			<?
				if ($row['theme'])
					echo $row['theme'];
				else
					selectsql('theme', "select id, concat(page,'/',id) nom from option_theme order by nom", null, '--');
			?>
			</td>
			<td><label for="groupe">Groupe</label></td>
			<td><? echo $row['groupe'] ?></td>
		</tr>
		<tr>
			<td><label for="type">Type</label></td>
			<td>
			<?
				if ($id)
					echo $row['type'].' - '.Categorie::$TYPE[strtolower($row['type'])];
				else
					selectarray('type', Categorie::$TYPE, null);
			?>
			</td>
			<td><label for="presentation">pr�sentation</label></td>
			<td>
			<?
				if ($id)
					echo $row['presentation'];
				else
				{
					$a = array('ligne' => 'ligne','bloc' => 'bloc','liste' => 'liste');
					selectarray('presentation', $a, $row['presentation']);
				}
			?>
			</td>
		<tr>
			<td><label for="nom">Nom</label></td>
			<td><input type="text" id="nom" name="nom" value="<?=htmlspecialchars($row['nom'])?>" maxlength="64" size="64"/></td>
			<td><label for="delai_min">D�lai min. (en h)</label></td>
			<td><input type="text" id="delai_min" name="delai_min" size="3" maxlength="3" value="<?=$row['delai_min'];?>"/></td>
			
		</tr>
		<tr>
			<td><label for="accroche">Accroche</label></td>
			<td colspan="3"><input type="text" id="accroche" name="accroche" value="<?=htmlspecialchars($row['accroche'])?>" maxlength="256" size="128"/></td>
		</tr>
		<tr>
			<td><label for="paiement">Paiement</label></td>
			<td><? selectarray("paiement", Option::$MODE_PAIEMENT, $row["paiement"]); ?></td>
			<td><label for="par_agence">Par agence</label></td>
			<td>
				<input type="hidden" name="par_agence[]" value="0"/>
				<input type="checkbox" id="par_agence" name="par_agence[]" value="1"<? if ($row['par_agence']) echo ' checked';?>/>
			</td>
		</tr>
		<tr>
			<td><label for="tarification">Tarification</label></td>
			<td>
			<?
				$a = array('F' => 'Forfaitaire', 'J' => 'Par jour', 'K' => 'Par km', 'U' => 'Unitaire');
				selectarray('tarification', $a, $row["tarification"]);
			?>
			</td>
			<? if (in_array($row['theme'], array('kits','matach','matloc','complementaires'))) : ?>
			<td><label for="quantite_max">Qantit� max.</label></td>
			<td><input type="text" id="quantite_max" name="quantite_max" size="2" value="<?=$row['quantite_max']?>" style="text-align: right;"/><em>(&gt;1)</em></td>
			<? else: ?>
			<td colspan="2"></td>
			<? endif; ?>
		</tr>
		<tr>
			<td><label for="init">Par d�faut</label></td>
			<td><? selectarray('init', array('inactif','actif'), $row['init']);?></td>
			<td><label for="position">Position</label></td>
			<td><input type="text" id="position" name="position" value="<?=$row['position']?>" size="6" maxlength="4" style="text-align: right; width: inherit;"></td>
		</tr>
		<tr>
			<td><label for="popin">Popin</label></td>
			<td>
			<?
				$a = array();
				foreach(glob(BP.'/skin/popin/popin/*.htm') as $f)
				{
					$f = substr(basename($f), 0, -4);
					if ($f == 'index') continue;
					$a[$f] = $f;
				}
				selectarray('popin', $a, $row['popin'], '--');
			?>
			</td>
			<td><label for="en_avant">Mise en avant</label></td>
			<td><input type="text" id="en_avant" name="en_avant" value="<?=$row['en_avant']?>" size="6" maxlength="2" style="text-align: right; width: inherit;"/></td>
		</tr>
		<? if ($row['url'] || $row['lien']) : ?>
		<tr>
			<td><label for="lien">Lien</label></td>
			<td><input type="text" id="lien" name="lien" value="<?=htmlspecialchars($row['lien'])?>" mexlength="128"/></td>
			<td><label for="url">URL</label></td>
			<td><input type="text" id="url" name="url" value="<?=htmlspecialchars($row['url'])?>" maxlength="128"/></td>
		</tr>
		<? endif; ?>
		<tr>
			<td valign="top"><label for="commentaire">Commentaire</label></td>
			<td colspan="3">
				<textarea id="commentaire" name="commentaire" rows="2"><?=$row['commentaire']?></textarea>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%">
		<tr>
			<td align="left">
				<? if ($id) :  ?>
				<input type="button" onclick="return go_frame(parent.list, 'lists/option_tarif.php?option=<?=$id?>');" value="tarifs">
				<? if ($row['par_agence']) : ?>
				<input type="button" onclick="return go_frame(parent.list, 'lists/agence_option.php?option=<?=$id?>');" value="agences">
				<? endif; ?>
				<? endif; ?>
			</td>
			<td align="right">
		<? 
			if ($id || $adminUser->hasRights(Acces::SB_ADMIN)) { // seuls les administrateurs peuvent cr�er une nouvelle option 
		?>
				<input type="submit" name="<?=$id ? "update" : "create"?>" value="enregistrer" onclick="return doSubmit(this.form);">
		<?
			}
			if ($id && $adminUser->hasRights(Acces::SB_ADMIN)) 
			{
		?>
				&nbsp;
				<input type="submit" onclick="return doDelete(this.form);" name="delete" value="supprimer">
		<?
			}
		?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
</form>
</body>
</html>
