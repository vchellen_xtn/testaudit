<?php
class Prospect extends Ada_Object
{
	/**
	* R�cup�rer un prospect
	* 
	* @param int $id
	* @returns Prospect
	*/
	static public function factory($email)
	{
		if (!filter_var($email, FILTER_VALIDATE_EMAIL))
			return null;
		$x = new Prospect();
		return $x->load($email);
	}
	public function _construct()
	{
		$this->setIdFieldName('email');
		return parent::_construct();
	}
	
	/**
	* Cr�ee un prospect
	* 
	* @param array $data
	* @param array $errors
	* @return Prospect
	*/
	static public function create(&$data, &$errors)
	{
		$x = new Prospect();
		if ($x->validateData($data, $errors))
		{
			// s'il n'y a pas d'erreurs � ce stade on :
			// - cr�e le prospect
			// - envoie un e-mail au prospect
			if (!count($errors))
			{
				$x->setData($data)->save();
				$x = self::factory($data['email']);
				if (!$x->getData('desabo'))
					$x->sendConfirmation();
				return $x;
			}
		}
		return null;
	}
	
	/**
	* Envoie la confirmation par mail
	* 
	*/
	public function sendConfirmation()
	{
		$data = $this->getData();
		// on n'envoie pas pour le JEU
		if ($data['origine']=='JEU')
			return;
		// formule de politesse
		if ($data['nom'])
		{
			if ($data['prenom'])
				$data['bonjour'] = $data['prenom'].' ';
			else if ($data['titre'])
			{
				global $CIVILITE;
				$data['bonjour'] = $CIVILITE[$data['titre']].' ';
			}
			$data['bonjour'] .= $data['nom'];
		}
		// envoyer le mail
		$html = load_and_parse(BP.'/emails/'.($data['origine']=='JEUNES' ? 'em6-jeunes.html' : 'em6.html'), $data);
		send_mail ($html, $data['email'], SUBJECT_NEWSLETTER, EMAIL_FROM, EMAIL_FROM_ADDR, '', BCC_EMAILS_TO);
	}
	
	/**
	* Renvoie le tableau des r�gles de validation
	*/
	protected function _getValidatingRules()
	{
		return array (
			'email'		=> $this->_addRuleEmail("Vous devez pr�ciser un e-mail", "L'e-mail %s n'est pas valide"),
			'origine'	=> $this->_addRuleString(),
			'titre'		=> $this->_addRuleString("Vous devez pr�ciser la civilit�"),
			'prenom'	=> $this->_addRuleString("Vous devez pr�ciser votre pr�nom"),
			'nom'		=> $this->_addRuleString("Vous devez pr�ciser votre nom"),
			'cp'		=> $this->_addRuleRegExp('/^\d{5}$/', "Vous devez pr�ciser le code postal", "Le code postal n'est pas valide"),
			'naissance' => $this->_addRuleDate("Vous devez pr�ciser votre date de naissance", "La date de naissance n'est pas valide"),
			'desabo'	=> $this->_addRuleInt(),
		);
	}
}
?>
