<?
/**
* CronTask
*/
abstract class CronTask
{
	private $_logfile; // chemin du fichier de log
	private $_logpath; // chemin des fichiers de log
	private $_errors = array(); // tableau des erreurs
	private $_errorsType = array(); // nombres d'erreurs par type
	private $_actions = 0;
	private $_isRunning = false;
	
	/**
	* cr�e une CronTask
	* 
	* @param string $logpath chemin vers la racine du fichier log
	* @return CronTask
	*/
	public function __construct($logpath)
	{
		$this->_logpath = rtrim($logpath, '/');
	}
	protected function incActions() { $this->_actions++; }
	protected function hasDone() { return ($this->_actions > 0); }
	
	// fonctions abstraites
	abstract protected function getEmailTpl(); // pour renvoyer le template de l'e-mail
	abstract protected function work(); // rappel� par run() pour donner la main apr�s initialisation
	
	// publiques
	public function run()
	{
		// initialise les chemins
		$this->init();
		// fait le boulot
		$this->work();
		// on termine
		$this->finalize();
	}
	
	// fonctions utilitaires
	protected function init()
	{
		// pour capteurer la sortie vers STDOUT
		ob_start(); 
		$this->msg(sprintf('initialisation: %s, depuis IP %s', date('H:i:s'), $_SERVER['REMOTE_ADDR']),'I', true);
		// verification template de l'email
		if( !is_file($this->getEmailTpl()) )
			$this->error("Le template de l'email ".$this->getEmailTpl()." n'existe pas !");
		// verification que les constantes sont d�finies
		$const = array('PROJECT_ID', 'SITE_MODE', 'EMAIL_CRON_REPORT', 'EMAIL_FROM', 'EMAIL_FROM_ADDR');
		foreach($const as $c)
			if(!defined($c))
				$this->error("Constante non d�finie : ".$c);
		// verification de l'existence du repertoire de log
		if(!is_dir($this->_logpath))
			$this->error("Le r�pertoire de log ".$this->_logpath." n'existe pas!");
		// ne pas autoriser 2 ex�cutions simultan�s
		if (file_exists($this->_logpath.'/isrunning.txt'))
			$this->error("Une t�che est d�j� en ex�cution (".$this->_logpath."/isrunning.txt)");
		else if (!touch($this->_logpath.'/isrunning.txt'))
			$this->error("Impossible de cr�er ".$this->_logpath.'/isrunning.txt');
		else
			$this->_isRunning = true;
		$this->checkErrors();
		// creation de l'arborescence des logs
		$d_log = $this->_logpath.'/'.date('Y/m/d/');
		$this->_logfile = $d_log.'/'.date('Y-m-d_H-i-s').'_log.txt';
		// cr�ation du r�pertoire de log du jour
		if(!is_dir($d_log) && !mkdir($d_log, 0777, true))
			$this->error("Impossible de cr�er le r�pertoire ".$d_log."!");
		// creation du fichier de log
		if(!touch($this->_logfile ))
			$this->error("Impossible de cr�er le fichier de log: ".$this->_logfile);
		// verification des erreurs
		$this->checkErrors();
	}
	protected function finalize()
	{
		// r�cup�rer le STDOUT
		if ($this->_isRunning) unlink($this->_logpath.'/isrunning.txt');
		$this->msg('finalisation: '.date('H:i:s'),'I',true);
		$stdout = ob_get_contents();
		if (!$this->hasDone() && !$this->hasErrors()) return;
		
		// composer le sujet du message
		$subject = '['.PROJECT_ID.']['.SITE_MODE.'] cron '.date('Y-m-d').': ';
		if (!count($this->_errorsType))
			$subject .= 'OK';
		else 
		{
			foreach($this->_errorsType as $k => $v)
				$s[] = $v.' '.$k;
			$subject .= join(', ', $s);
		}
		$subject.='-'.$_SERVER["SERVER_NAME"];
		// composer le message
		if ($this->hasErrors()) 
		{
			$msg = "Erreurs :\n\t";
			$msg.= join("\n\t",$this->_errors);
			$msg.="\n\n";
		}
		// renvoyer le fichier de log
		$html = load_and_parse(
					$this->getEmailTpl(),
					array(
						'srvr'	=> $_SERVER["SERVER_NAME"] 
						,'msg'	=> $msg
						,'log'	=> (is_file($this->_logfile) ? file_get_contents($this->_logfile) : '')
						,'out'	=> $stdout
					)
				);

		// envoyer l'e-mail
		send_mail($html, EMAIL_CRON_REPORT, $subject, EMAIL_FROM, EMAIL_FROM_ADDR, null, "");
		return;
	}
	/* fonctions de log*/
	protected function error($msg, $stop=false)
	{
		$this->_errors[] = $msg;
		$this->msg($msg, 'E', true);
		if($stop) $this->checkErrors();
	}
	protected function msg($msg, $type='I', $stdout=false)
	{
		if ($type != 'I') $this->_errorsType[$type]++;
		if ($stdout) echo $msg."\n";
		if ($this->_logfile && is_file($this->_logfile))
		{
			$line = $type."\t".date('H:i:s')."\t".$msg."\n";
			file_put_contents($this->_logfile, $line, FILE_APPEND);
		}
	}
	protected function getSql($sql)
	{
		if (!$sql) $this->error('getsql: argument non valide', true);
	//	$this->msg('getsql - '.$sql);
		return getsql($sql);
	}
	protected function sqlExec($sql)
	{
		if (!$sql) $this->error('sqlexec: argument non valide', true);
	//	$this->msg('sqlexec - '.$sql);
		return sqlexec($sql);
	}
	protected function hasErrors() { return (count($this->_errors) > 0);}
	/**
	* Arr�te le processus 'il y a une erreur
	* 
	* @param mixed $send_mail
	*/
	protected function checkErrors($send_mail=true)
	{
		if (!$this->hasErrors()) return;
		$this->msg('checkErrors: arr�t du traitement', 'F');
		if($send_mail)
			$this->finalize();
		die();
	}
	/*
	** gestion des donn�es
	*/
	/**
	* Met � NULL les champs qui peuvent l'�tre
	* @param string $table
	*/
	protected function tableSetNulls ($table)
	{
		$rs = sqlexec("show columns from ".$table);
		while ($row = mysql_fetch_assoc($rs)) 
		{
			if ($row['Null'] == 'YES' && preg_match('/^(char|varchar|date|int|smallint)/', $row['Type']))
			{
				$sql .= ($sql ? ', ' : ' SET ');
				if ($row['Type'] == 'datetime')
					$s = '0000-00-00 00:00:00';
				else if ($row['Type'] == 'date')
					$s = '0000-00-00';
				else if (preg_match('/^(int|smallint)/', $row['Type']))
					$s = '0';
				else
					$s = '';
				$sql .= $row['Field']." = NULLIF(".$row['Field'].",'".$s."')";
			}
		}
		if (!$sql) return;
		$sql = 'UPDATE '.$table.$sql;
		$this->getSql($sql);
		$this->msg(mysql_affected_rows(). ' enregistrments de '.$table.' mis � NULL\n');
	}
	/**
	* renvoie les champs d'une table
	* 
	* @param string $table
	* @returns array liste des champs
	*/
	protected function tableGetFields ($table) {
		$rs = sqlexec("show columns from ".$table);
		while ( $row = mysql_fetch_assoc($rs)) {
			$a[] = $row['Field'];
		}
		return $a;
	}
}

?>
