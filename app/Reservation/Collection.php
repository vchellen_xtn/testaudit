<?

class Reservation_Collection extends Ada_Collection
{
	protected $_loadFacturation = true;
	
	/**
	* Cr�� une collection de r�servations
	* @param Ada_Object $client - client
	* @param string $fromDate : depuis la date
	* @param int $$limit : nombre maximum
	* @return Reservation_Collection
	*/
	static public function factory(Ada_Object $object, $fromDate = null, $limit = null, $loadFacturation = true)
	{
		// on cr�e la collection qui sera retourn�e
		$x = new Reservation_Collection();
		$x->_loadFacturation = $loadFacturation;
		if ($object instanceof Client)
			$cond = 'r.client_id ='.$object->getId();
		else if ($object instanceof PartenaireCoupon)
			$cond = 'r.coupon='.$object->getId();
		else
			return $x;
		
		// SQL pour les applications
		$sql = "SELECT r.id, r.client_id, r.agence, r.coupon, r.statut, r.duree, r.km, r.paiement, r.remboursement, r.rembourse, r.total, r.forfait_type, r.debut,\n";
		$sql.=" r.type, c.mnem categorie_mnem, CASE WHEN r.forfait_type='JEUNES' AND j.nom IS NOT NULL THEN j.nom ELSE concat(ifnull(concat(nullif(f.nom,c.nom),' '),''), c.nom) END categorie_nom, c.description categorie_description, coalesce(aa.nom, ap.nom) as agence_nom\n";
		$sql.=" FROM reservation r\n";
		$sql.=" JOIN categorie c ON r.categorie=c.id\n";
		$sql.=" JOIN agence_alias ap on r.pdv=ap.id\n";
		$sql.=" LEFT JOIN agence_alias aa on r.alias=aa.id\n";
		$sql.=" LEFT JOIN vehicule_famille f ON f.id=c.famille\n";
		$sql.=" LEFT JOIN categorie_jeune j ON j.categorie=c.id\n";
		$sql.=" WHERE $cond";
		if ($fromDate)
			$sql.=" AND r.paiement >= '$fromDate'\n";
		$sql.=" AND r.statut IN ('A','P')\n";
		$sql.=" ORDER BY r.debut DESC";
		if ($limit > 0)
			$sql.="\n LIMIT 0, $limit";
		$x->_loadFromSQL($sql);
		return $x;
	}
	
	/**
	* D�terminer les facturations Courtage
	* 
	*/
	protected function _afterLoad()
	{
		if (!$this->_loadFacturation || !$this->count()) return;
		$type = array('F'=>'facture_','A'=>'avoir_');
		// r�cup�rer la liste des factures / avoir du courtage et des pro
		foreach(array('materiel','courtage','pro') as $k)
		{
			$sql = "SELECT f.reservation, f.type";
			$sql.=" FROM facturation_$k f";
			$sql.=" WHERE f.reservation IN (".join(',', $this->getIds()).")";
			$sql.=" ORDER BY f.reservation";
			$rs = sqlexec($sql);
			while ($row = mysql_fetch_assoc($rs))
				$this->_items[$row['reservation']][$type[$row['type']].$k] = true;
		}
	}
}
?>