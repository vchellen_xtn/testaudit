<?
class AgenceHoraire_Collection extends Ada_Collection
{
	public function toSExp(array $arrAttributes = array(), $rootName = null, $childName = null, $depth = 0)
	{
		return $this->__toSExp(array(), 'horaires', 'horaire', $depth);
	}
	public function toXml(array $arrAttributes = array(), $rootName = null, $childName = null) {
		return $this->__toXml(array(), 'horaires', 'horaire');
	}
	protected function _getAttributes()
	{
		return array();
	}
}
?>