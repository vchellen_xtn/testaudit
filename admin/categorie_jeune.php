<?
	require_once("secure.php");
	require_once("lib.php");


	// modification, cr�ation
	if(getarg("update") || getarg("create"))
	{
		// on supprimer les informations actuellement pr�sentes
		getsql("DELETE FROM categorie_jeune");
		$msg = array();
		foreach ($_POST['categorie_jeune'] as $k => $row)
		{
			if (!$row['nom'] && !$row['description']) continue;
			save_record('categorie_jeune', $row);
		}
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script>
	<!-- //
	// doSubmit
	function doSubmit(frm)
	{
		var err = "";
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		return true;
	}
	// -->
	</script>
</head>
<body>
<form action="" method="post" onsubmit="return doSubmit(this);" enctype="multipart/form-data">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre">
		<img src="img/puce.gif" align="absmiddle" hspace="1">
		Cat�gorie Jeunes
	</th>
</tr>
<tr>
	<td>
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="30%">
		<col width="35%">
		<col width="35%">
		<tr>
			<th>Cat�gorie</th>
			<th>Nom</th>
			<th>Description</th>
		</tr>
<?
		$sql = "select c.type, c.id categorie, c.mnem, concat(ifnull(concat(nullif(f.nom,c.nom),' '),''), c.nom) categorie_nom, j.nom, j.description";
		$sql.=" from categorie c";
		$sql.=" left join vehicule_famille f on f.id=c.famille";
		$sql.=" left join categorie_jeune j on j.categorie=c.id";
		$sql.=" where c.publie=1 and c.id IN (select distinct t.categorie from forfait2_tarif t join zone_groupe zg on t.groupe=zg.id where zg.zone='fr' and zg.nom='JEUnES')";
		$sql.=" order by c.type, c.position";
		$type = ''; 
		$rs = sqlexec($sql);
		while ($row = mysql_fetch_assoc($rs)) :
		?>
			<? $fld = 'categorie_jeune['.$row['categorie'].']'; ?>
			<? if ($type != $row['type']) : ?>
				<tr><th colspan="3"><?=strtoupper($row['type']);?></th></tr>
				<? $type = $row['type']; ?>
			<? endif; ?>
			<tr>
				<td>
					<strong><?=$row['mnem'].' - '.$row['categorie_nom']?></strong>
					<input type="hidden" name="<?=$fld?>[categorie]" value="<?=$row['categorie']?>"/>
				</td>
				<td><input type="text" name="<?=$fld?>[nom]" value="<?=$row['nom']?>" maxlength="42" size="42"/></td>
				<td><input type="text" name="<?=$fld?>[description]" value="<?=$row['description']?>" maxlength="128" size="64"/></td>
			</tr>
		<? endwhile; ?>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%">
		<tr>
			<td align="right">
				<input type="submit" name="<?=$id?"update":"create"?>" value="enregistrer" onclick="return doSubmit(this.form);">
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
</form>
</body>
</html>
