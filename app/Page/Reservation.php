<?
class Page_Reservation extends Page
{
	protected function _getCustomScripts()
	{
		$action = $this->getAction();
		$files = array('js'=>array(), 'css'=>array());
		if ($action =='index' || $action=='resultat')
		{
			$files['js'][] = 'scripts/form_reservation.js';
			$files['js'][] = 'scripts/agence_liste.js';
			if ($action == 'index')
			{
				$files['js'][] = 'scripts/jquery.tagdragon.min.js';
				if ($this->isSkin('v2'))
				{
					$files['js'][] = 'scripts/carrousel.js';
					$files['js'][] = 'scripts/jquery.slides.js';
				}
			}
		}
		return $files;
	}

	public function getUniversTitre() {
		return "R�servation";
	}

	/** La page supporte-t-elle le HTTPS ? @returns bool */
	protected function isThatPageNeedsHttps()
	{
		return (in_array($this->getAction(), array('inscription','paiement','validation')));
	}

	/**
	* Dans certains cas, on redirige vers un site externe
	* 
	* @param Reservation $reservation
	*/
	protected function checkReservationRedirect(Reservation $reservation)
	{
		$agence = $reservation->getAgence();
		if (($agence['zone']=='gp') || ($agence['zone']=='re')) {
			$args = array(
				'partenaire'	=> 'adafr',
				'agence'		=> $reservation['agence'],
				'date_depart'	=> $reservation->getDebut('d-m-Y'),
				'date_retour'	=> $reservation->getFin('d-m-Y'),
				'heure_depart'	=> $reservation->getDebut('H:i'),
				'heure_retour'	=> $reservation->getFin('H:i'),
				'type'			=> $reservation['type'],
				'categorie'		=> $reservation->getCategorie()->getData('mnem')
			);
			if ($agence['zone']=='gp') {
				$url = 'http://www.ada-guadeloupe.com/?'.http_build_query($args);
			} else if ($agence['zone']=='re') {
				$url = 'https://www.ada-reunion.com/adafr.php?'.http_build_query($args);
			}
			self::redirect($url, 301);
			exit();
		}
		return false;
	}
	
	//
	// ACTIONS
	//
	public function indexAction()
	{
		// quelques redirections pour �tre s�r que type et categorie sont correctement renseign�es
		if ($_GET['categorie'] && !$_GET['agence'])
		{
			$categorie = Categorie::factory((int)$_GET['categorie']);
			// si la cat�gorie n'existe pas ou n'est pas publi�
			if (!$categorie || $categorie->isEmpty() || !$categorie['publie'])
			{
				self::redirect(self::getURL(), 301);
				return;
			}
			// si l'URL n'est pas "canonique"on redirige
			$canonical = $categorie->getURLResa();
			if ($canonical != $this->getBrowserURI() && !isset($_GET['co']) && !isset($_GET['codepromo']))
			{
				self::redirect($canonical, 301);
				return;
			}
		}
		// lien Google+ depuis la page d'accueil
		if (!$this->isNoIndex()) {
			$this->addLink(array('rel' => 'publisher', 'href' => Agence::getADAGooglePlusURL()));
		}
	}
	
	// afficher la liste des v�hicules disponibles
	public function vehiculeAction()
	{
		// cr�er la r�servation � partir de $_POST
		$msg = $url = null;
		$search = array();
		if (!empty($_POST)) {
			$search = $_POST;
		} else {
			$search = Reservation::createSearchFromCookieAndGet();
		}
		if ($reservation = Reservation::create($search, $msg, $url)) {
			// se souvenir de la recherche
			$this->_setCookie('ADA001_LAST_SEARCH', serialize($reservation->getFields()), (SITE_MODE=='PROD' ? strtotime('+1 week') : null));

			// pour la Guadeloupe on renvoie vers leur site
			if ($this->checkReservationRedirect($reservation)) {
				exit();
			}

			// stocker la r�servation et r�cup�rer les offres
			$this->setData('reservation', $reservation);
			
			// r�cup�er les offres
			$offres = $reservation->getOffres(Offre_Collection::OFFRE_TOUTES);
			if ($offres) {
				$offres->loadNegociations();
			}

			// pr�ciser le type dans le trackng
			$this->setTrackPage('reservation_budget', false);
			$this->setTrackPage(strtolower($reservation['type']));

			// [modif WD 080208] - $_track["agence_search"] = $agence;
			$track = array();
			$track['agence_search'] = Tracking_SmartProfile::getAgenceSearch($reservation);
			$track['agence_init'] = $track['agence_search'];
			$track['pays'] = $reservation->getAgence()->getData('zone');
			if ($reservation->isLocapass()) {
				$track['locapass'] = $reservation->getPartenaireCoupon()->getData('code');
			}
			if ($reservation->isTooLate()) {
				$this->setTrackPage('delai');
				$track['appel_agence'] = $reservation['alias'];
			} else if (!$reservation->getAgence()->hasStatut(Agence::STATUT_RESAWEB)) {
				$this->setTrackPage('ferme');
				$track['callback1_agence'] = $track['agence_search'];
			}
			$this->setTrackInfo($track);
			// et le cookie
			self::_setCookie('ADA001_TRACK_AGENCE', $track['agence_search']);
		}
		$this->_addMessage($msg);
		if ($url) {
			Page::redirect($url);
		}
		return;
	}

	// affiche le r�sultat de la recherche d'une r�servation
	public function resultatAction()
	{
		// cr�er la r�servation � partir de $_POST
		if ($reservation = Reservation::create($_POST, $msg, $url))
		{
			// se souvenir de la recherche
			$this->_setCookie('ADA001_LAST_SEARCH', serialize($reservation->getFields()), (SITE_MODE=='PROD' ? strtotime('+1 week') : null));

			// pour la Guadeloupe on renvoie vers leur site
			if ($this->checkReservationRedirect($reservation)) {
				exit();
			}

			// stocker la r�servation et r�cup�rer les offres
			$this->setData('reservation', $reservation);
			if ($this->getSkin() == 'jeunes')
			{
				$agence = $reservation->getAgence();
				$groupe = ZoneGroupe::factory('JEUNES', $agence['zone']);
				if (!$agence->hasStatut(Agence::STATUT_JEUNES))
				{
					$this->_addMessage("L'agence s�lectionn�e ne propose pas l'offre ADA Jeunes");
					$this->setData('reservation', null);
					return;
				}
				$agence->setData('groupe', $groupe->getId());
				$reservation->setData('forfait_type', 'JEUNES');
				$reservation->getCategorie()->loadJeunes();
			}
			$offres = $reservation->getOffres($this->getSkin());
			if ($offres) {
				$offres->loadNegociations();
			}

			// pr�ciser le type dans le trackng
			$this->setTrackPage(strtolower($reservation['type']));

			// [modif WD 080208] - $_track["agence_search"] = $agence;
			$track['agence_search'] = Tracking_SmartProfile::getAgenceSearch($reservation);
			$track['agence_init'] = $track['agence_search'];
			$track['pays'] = $reservation->getAgence()->getData('zone');
			if ($reservation->isLocapass())
				$track['locapass'] = $reservation->getPartenaireCoupon()->getData('code');
			if ($reservation->isTooLate())
			{
				$this->setTrackPage('delai');
				$track['appel_agence'] = $reservation['alias'];
			}
			else if (!$reservation->getAgence()->hasStatut(Agence::STATUT_RESAWEB))
			{
				$this->setTrackPage('ferme');
				$track['callback1_agence'] = $track['agence_search'];
			}
			// pour les anciennes versions
			if (!$this->isSkin('v2'))
			{
				if (!$offres[0]['nb'] && $reservation->getAgence()->hasStatut(Agence::STATUT_RESAWEB))
				{
					$this->setTrackPage('indispo');
					$track['callback1_vehicule'] = $track['agence_search'];
				}
				// premi�re offre
				if ($offres[0])
					$track['pr_vu'] = $reservation['type'].'.cat'.str_replace('+',"'",$offres[0]['mnem']).'.demande';
			}
			// un upsell est-il propos� ?
			if (count($offres) > 1)
				$track['efficacite_up'] = 'upsell_affiche';
			$this->setTrackInfo($track);
			// et le cookie
			self::_setCookie('ADA001_TRACK_AGENCE', $track['agence_search']);
		}
		$this->_addMessage($msg);
		if ($url)
			Page::redirect($url);
		return;
	}
	// appell�e depuis reservation/resultat si la zone n'est pas ouverte � la r�servation
	public function telephoneAction()
	{
		$this->setTrackInfo('callback2', $_COOKIE['ADA001_TRACK_AGENCE']);
	}
	// appel�e depuis reservation/telephone
	public function appelAction()
	{
		// si l'e-mail n'est pas pass� on renvoie vers la page d'accueil
		if (!$_POST['email'])
			Page::redirect(Page::getURL());
		if ($reservation = Reservation::create($a = $_POST, $msg, $url))
		{
			foreach ($_POST as $k => $v)
				$_POST[$k] = htmlspecialchars(strip_tags($v));
			$agence = $reservation->getAgence('id,nom,adresse1,cp,ville,tel,fax');
			$categorie = $reservation->getCategorie('id,mnem,nom');
			$data = array(
				 'date'		=> date('d-m-Y � H:i')
				,'delai'	=> $_POST['delaiContact']
				,'civilite'	=> $_POST['civilite']
				,'prenom'	=> $_POST['prenom']
				,'nom'		=> $_POST['nom']
				,'tel'		=> $_POST['tel_client']
				,'email'	=> $_POST['email']
				,'agence'	=> $agence['nom'].'<br />Adresse : '.$agence->getHtmlAddress(' ','adresse1,cp,ville')
				,'tel_agence'=> $agence['tel']
				,'fax_agence'=> $agence['fax']
				,'depart'	=> 'du '.strtolower($reservation->getLongDate('debut')).' - '.$reservation->getDebut('H:i')
				,'retour'	=> 'au '.strtolower($reservation->getLongDate('fin')).' - '.$reservation->getFin('H:i')
				,'jour'		=> $reservation['duree']
				,'km'		=> $reservation['distance']
				,'vehicule'	=> 'Cat�gorie '.$categorie['mnem'].' - '.$categorie['nom']
				,'TTC'		=> $_POST['prix']
			);

			// email client
			$html = load_and_parse(BP.'/emails/em7.html', $data);
			send_mail ($html, EMAIL_RESA_TEL, SUBJECT_RESA_TEL, EMAIL_FROM, EMAIL_FROM_ADDR, '', BCC_EMAILS_TO);
		}
		if ($msg)
			$this->_addMessage($msg);
		if ($url)
			Page::redirect($url);
		// tracking
		$this->setTrackInfo('callback3', $_COOKIE['ADA001_TRACK_AGENCE']);
	}

	/**
	* Renvoie un tableau pour la navigation
	* pour les v�hicules sans permis (SP) et � l'heure (ML), on ne propose pas les assurances
	* De m�me les agences PointLoc ne permettent (ni options) ni assurances
	* @returns array
	*/
	public function getNavResa($force = false)
	{
		$a = array();
		if ($force || (/** @var Reservation */ $reservation = $this->getData('reservation')))
		{
			if ($this->isSkin('v3')) {
				$a = array(
					'resultat'		=> 'V�hicule et vos options',
					'options'		=> 'Assurances',
					'inscription' 	=> 'Inscription',
					'paiement'		=> 'Paiement',
					'validation'	=> 'Confirmation',
				);

				// pour les v�hicules sans permis (SP) et � l'heure (ML), on ne propose pas les assurances
				if ($reservation &&
					($reservation->isLocapass()
					|| in_array(strtolower($reservation['type']), array('sp','ml'))
					|| !Option::doesExsits($reservation->getAgence()->getData('zone'), $reservation['type'], 'assurances')
					|| $reservation->getAgence()->isPointLoc()
					)
				) {
					unset($a['options']);
				}
			} else if ($this->isSkin('mobile')) {
				$a = array(
					'index'		=> 'Votre demande',
					'vehicule'	=> 'Votre v�hicule',
					'resultat'	=> 'Options',
					'options'	=> 'Options',
					'inscription' => 'Vos infos',
					'paiement'	=> 'Paiement'
				);
				// on a 2 �tapes diff�rentes pour les options... il faut g�rer ce cas
				unset($a[($this->getAction() == 'options' ? 'resultat' : 'options')]);
			} else {
				$a['resultat'] = 'V�hicule';
				if ($reservation && strtolower($reservation['type'])=='vu' && $reservation->getAgence()->getData('zone')=='fr')
					$a['options'] = 'Mat�riel';
				$a['inscription'] = 'Inscription';
				$a['paiement'] = 'R�glement';
			}
			if (!$force && !$a[$this->getAction()])
				$a = array();
		}
		return $a;
	}

	// affiche des optoins compl�mentaires skin = vN
	public function optionsAction()
	{
		// cr�er la r�servation � partir de $_POST
		if ($reservation = Reservation::create($a = $_POST, $msg, $url))
		{
			$this->setData('reservation', $reservation);
			// pr�ciser le type dans le trackng
			$this->setTrackPage(strtolower($reservation['type']));
			return;
		}
		$this->_addMessage($msg);
		if ($url)
			Page::redirect($url);
		return;
	}
	// affiche le tarif des options (skin = www et iphone)
	public function tarifAction()
	{
		// cr�er la r�servation � partir de $_POST
		if ($reservation = Reservation::create($a = $_POST, $msg, $url))
		{
			// pr�ciser le type dans le trackng
			$this->setTrackPage(strtolower($reservation['type']));
			// on r�cup�re le forfait
			$forfait = Forfait::doUnserialize($_POST['forfait']);
			if (!$forfait)
				$this->_addMessage("Le forfait choisi n'est pas valide !");
			else
			{
				$this->setData('reservation', $reservation);
				$this->setData('forfait', $forfait);
				if ($forfait['categorie'] != $reservation['categorie'])
					$this->setTrackInfo('efficacite_up', 'upsell_clique');
			}
		}
		$this->_addMessage($msg);
		if ($url)
			Page::redirect($url);
		return;
	}
	// identification ou cr�ation d'un compte
	public function inscriptionAction()
	{
		// l'identifiant de la r�servation est pass�e, on le v�rifie
		// si �chec on est renvoy� vers reservation/index
		Page::checkKey($id = (int) $_GET['id']);
		$reservation = Reservation::factory($id);
		if (!$reservation)
			self::redirect(self::getURL());
		// pr�ciser le type dans le trackng
		$this->setTrackPage(strtolower($reservation['type']));
		if ($reservation['statut']!='C')
		{
			if ($reservation['statut']=='V' && $reservation['client_id'])
			{
				$this->_addMessage("La r�servation a d�j� �t� associ�e � un compte !");
				Page::redirect(Page::getURL(dirname($this->getId()).'/paiement.html?'.$reservation->getKeyParams(), null, USE_HTTPS));
			}
			else
			{
				// que fait-on l� ?
				Page::redirect();
			}
		}
		$this->setData('reservation', $reservation);
		// le tracking ... en �vitant le XSS
		if (!$reservation->isLocapass())
		{
			$this->setTrackInfo('pr_in', Tracking_SmartProfile::getPR($reservation));
			$efficacite_up = $_GET['efficacite_up'];
			if ($efficacite_up && preg_match('/^[a-z0-9\'\;\.\_\-]+$/i', $efficacite_up))
				$this->setTrackInfo('efficacite_up', $efficacite_up);
		}
	}
	/**
	* v�rification de l'identifiant et r�cup�ration des r�servations et clients pour les actions suivantes
	*
	* @param bool $trySetClient
	* @param bool $checkNotPaid
	* @returns bool
	*/
	protected function _checkReservation($trySetClient = false, $checkNotPaid = true)
	{
		// on v�rifie l'identifiant r�servation
		$id = (int) $_REQUEST['id'];
		Page::_checkKeyCustomer($id, 'reservation/index');
		// on v�rifie que le client est authentifi�, on le r�cup�re ainsi que la r�servation
		$client = Page::_checkKeyCustomer();
		$reservation = Reservation::factory($id);
		if (!$client) // le client diot �tre authentifi�
			$this->_addMessage("Vous devez �tre identifi� pour acc�der � cette page !");
		else if (!$reservation) // pas de r�servation
			$this->_addMessage("Le d�lai de validit� de votre r�servation a expir�, merci de la renouveler");
		else if ($checkNotPaid && $reservation->isInPayment())
			$this->_addMessage("Votre r�servation '".$reservation->getResIDClient()."' a d�j� �t� pay�e.<br/>Vous pouvez consulter votre espace client.");
		else if (!$reservation->checkClient($client, $trySetClient)) // la r�servation doit �tre associ�e � un client
			$this->_addMessage("La r�servation '$id' ne correspond pas au client indiqu�");
		else if (!$reservation->checkClientAge())
		{
			global $AGE;
			$this->_addMessage(sprintf("Votre �ge (%ld ans) ne vous permet pas de r�server ce v�hicule (%s).", $reservation->getClient()->getAge(), $AGE[$reservation->getCategorie()->getData('age')]));
			$this->setData('error_link_home', true);
		}
		// s'il y a des messages on s'arr�te l�
		if ($this->hasMessages())
			return false;
		// on conserve la r�seervation et le client pour la suite
		$this->setData('reservation',$reservation)
			 ->setData('client', $client);
		return true;
	}
	// association client / r�servation et affichage des diff�rents moyens de paiement
	public function paiementAction()
	{
		if (!$this->_checkReservation(true, true))
			return;
		/** @var Reservation */
		$reservation = $this->getData('reservation');
		// pr�ciser le type dans le trackng
		$this->setTrackPage(strtolower($reservation['type']));
		// si un commentaire est plac� dans le POST
		if (isset($_POST['commentaire']))
		{
			if ($commentaire = filter_var(trim(utf8_decode($_POST['commentaire'])), FILTER_SANITIZE_STRING))
			{
				if ($reservation)
				{
					$a = array('reservation'=>$reservation->getId(), 'commentaire'=>$commentaire);
					save_record('res_info', $a);
				}
			}
			die();
		}
		// code grillable : le r�cup�rer et le tester
		$msg = null; $mustReload = false;
		$code = $reservation->getPromotionCode();
		if (!$code && isset($_POST['grillable']))
			$code = PromotionCode::factory($_POST['grillable'], true, $msg);
		if ($code)
		{
			if ($code->isValidFor($reservation, $msg))
			{
				$this->setData('grillable', $code);
				$reservation->setData('total', $reservation->getPrixTotal() - $code['reduction']);
			}
		} else if (isset($_POST['grillable'])) {
			// ce n'est pas un code grillable, on essaie de voir s'il s'agit d'un codepromo (historiquement renseign� sur la page d'accueil)
			if ($coupon = PartenaireCoupon::factory($_POST['grillable'], true)) {
				// le code existe et est valide...
				$msg = '';
				$mustReload = $reservation->applyPartenaireCoupon($coupon, $msg);
			}
		}
		// renseigner le message si n�cessaire
		if ($msg)
			$this->setData('msg_grillable', $msg);
		// !ATTENTION : dans le cas d'un appel AJAX on renvoie en JSON
		if ($_POST['frm_grillable'])
		{
			$json = new Ada_Object();
			if ($msg) {
				$json['grillable_error'] = $msg;
				$json['must_reload'] = $mustReload;
			} else {
				$json['grillable_reduction'] = show_money($code['reduction']);
				$json['grillable_reduction_max'] = show_money($code['reduction_max']);
				$json['grillable_code'] = $code->toString();
			}
			die($json->toJson());
		}

		// informations statistiques
		/** @var Client */
		$client = $this->getData('client');
		if (!$reservation->isLocapass())
			$this->setTrackInfo('login', $client->getData('email'));
	}
	// pr�paration du paiement Paypal
	public function paypal_paiementAction()
	{
		// on v�rifie et on r�cup�re la r�servation et le client
		if (!$this->_checkReservation()) return;
		/*
		appelle PP_Checkout et renvoie sur le serveur paypal
		dans PP_Checkout on doit renseigner la description de ce qui est achet�, le montant eu euros, et le num�ro de r�servation.
		En cas d'abandon ou d'�chec on retourne sur index.php
		*/
		/** @var Reservation */
		$reservation = $this->getReservation();
		$token = Paypal::SetExpressCheckout(
			$reservation->toString(),
			$reservation['total'],
			$this->getBaseURL(),
			'paypal_recapitulatif.html?'.$reservation->getKeyParams(),
			'paiement.html?'.$reservation->getKeyParams(),
			$reservation->getResID()
		);
		// c'est Paypal qui prend la main
		if ($token)
			die();
	}
	public function paypal_recapitulatifAction()
	{
		// on v�rifie et on r�cup�re la r�servation et le client
		if (!$this->_checkReservation()) return;
		/** @var Reservation */
		$reservation = $this->getData('reservation');

		/**
		* page de confirmation
		* Fait apparaitre le r�sum� de l'op�ration en appelant PP_Review et demande � l'utilisateur de confirmer.
		* En cas d'abandon ou d'�chec on retourne sur paiement.html
		*/
		$res = Paypal::GetExpressCheckoutDetails($_GET['token']);
		if ($res['CUSTOM'] != $reservation->getResID())
			$this->_addMessage("La r�servation ".$reservation->getResIDClient()." ne correspond pas � la r�servation connue par PayPal (".$res['CUSTOM'].").");
		else
			$this->setData('PAYERID', $res['PAYERID']);
	}
	// validation du paiement paiement selon diff�rents modes, envoi des notifications
	public function validationAction()
	{
		if (!$this->_checkReservation(false, false)) return;
		/** @var Reservation */
		$reservation = $this->getData('reservation');
		// pr�ciser le type dans le trackng
		$this->setTrackPage(strtolower($reservation['type']));
		switch($reservation['statut'])
		{
			case 'C':
				$this->_addMessage("Impossible d'effectuer le paiement de cette r�servation, elle n'est pas valid�e");
				return;
			case 'X':
				$this->_addMessage("La r�servation est d�j� en cours de paiement! Veuillez consulter votre compte.");
				break;
			case 'P':
				if ($_REQUEST['mode']=='UNEURO')
					break;
				else
				{
					$this->_addMessage("Cette r�servation est d�j� en cours de r�glement.<br>Un e-mail de confirmation de r�servation vous a �t� envoy�.");
					break;
				}
			case 'A':
				$this->_addMessage("Impossible d'effectuer le paiement de cette r�servation, elle est annul�e.");
				return;
			case 'V':
				// on va effectuer le r�glement
				break;
			default: // ne doit pas arriver
				$this->_addMessage('Erreur lors du paiement, le statut est inconnu.');
				return;
		}
		// on ne fait le paiement que le sitatut est valide pour X ou P on ne laisse que le tracking
		if ($reservation['statut']=='V')
		{
			// on enregistre le commentaire si fourni
			$res_info = array();
			if ($commentaire = filter_var(trim($_POST['commentaire']), FILTER_SANITIZE_STRING)) {
				$res_info['commentaire'] = $commentaire;
			}
			if ($reservation->isLocapass() && preg_match('/^\d+$/', $_POST['conducteur'])) {
				$conducteur = (int) $_POST['conducteur'];
				$conducteurs = $reservation->getPartenaireCoupon()->getPartenaire()->getConducteurs();
				if ($conducteurs->offsetExists($conducteur)) {
					$res_info['conducteur'] = $conducteur;
				}
			}
			if (count($res_info)) {
				$res_info['reservation'] = $reservation->getId();
				save_record('res_info', $res_info);
			}
			// s'il y a des CGV � enregistrer
			if (isset($_POST['cgv']) && is_array($_POST['cgv']))
			{
				foreach($_POST['cgv'] as $cgv)
				{
					$a = array('client' => $reservation['client_id'], 'cgv' => filter_var($cgv, FILTER_SANITIZE_STRING));
					save_record('client_cgv', $a);
				}
			}
			// s'il y a des options on les rajoutes
			if (is_array($_POST['options']))
			{
				// on ajoute les options
				$reservation->addResOptions($_POST);
				// s'il y a un code de r�duction on recalcule sa valeur apr�s le rajout des options
				if ($code = $reservation->getPromotionCode())
				{
					if (!$code->isValidFor($reservation, $msg))
						$this->_addMessage($msg);
				}
			}

			// pour 1EURO, il faut rediriger vers le site de 1EURO
			if ($_POST['mode']=='UNEURO')
			{
				die(Paybox::get1EuroForm($reservation, 'E'));
			}

			// on cr�e la transaction associ�e au mod�le de paiement
			$x = Transaction::factory($_REQUEST['mode']);
			if (!$x)
			{
				$this->_addMessage("Le mode de paiement n'a pas pu �tre d�termin�");
				return;
			}
			$x->doPayment($reservation, $_POST);
			$this->_addMessage($x->getMessages());
			// si le paiement a �chou� on arr�te l�
			if ($reservation['statut'] != 'P')
				return;
		}

		// tracking
		if (!$reservation->isLocapass())
		{
			// tracking NSP
			$this->setTrackInfo(Tracking_SmartProfile::reservationValidation($reservation));
			Tracking_GoogleAnalytics::reservationValidation($this, $reservation);
			Tracking_ABTasty::reservationValidation($this, $reservation);
		}
	}
}
?>
