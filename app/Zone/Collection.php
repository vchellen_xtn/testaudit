<?

class Zone_Collection extends Ada_Collection
{
	/**
	* Cr�� une collection de zones
	*
	* @return Zone_Collection
	*/
	public static function factory()
	{
		// on cr�e la collection qui sera retourn�e
		$x = new Zone_Collection();
		// SQL pour les applications
		$sql = "SELECT z.id, z.nom, z.canon, COUNT(DISTINCT(aa.pdv)) agences\n";
		$sql.=" FROM zone z JOIN agence_alias aa ON aa.zone=z.id\n";
		$sql.=" WHERE (aa.statut & ".Agence::STATUT_VISIBLE.") = ".Agence::STATUT_VISIBLE."\n";
		$sql.=" GROUP BY z.id, z.nom, z.canon\n";
		$sql.=" ORDER BY z.id";
		$x->_loadFromSQL($sql);
		return $x;
	}
}
?>