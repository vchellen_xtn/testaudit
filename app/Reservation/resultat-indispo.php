<?
/**
* Traitement des diff�rents cas d'indisponibilit�s
* Param�tres :
* $reservation	Reservation
* $features		array	(
* 					'categories'	=> array('title'=>''), 
* 					'agences'		=> array('title'=>'', 'statut'=>'', ''max' => int, layout' => 'table | block'),
* 					'calendrier'	=> array('title',=>'', 'agence_search'=>'') : l'ordre donne l'ordre d'affichage
* $num			optionnel combien d'autres propsitions ont �t� faites
*/
/** @var Reservation */
$reservation = $this;
$fields = $reservation->getFields();
$hasDispo = false;

// pr�sentation d'autres cat�gories
if ($features['categories'])
{
	$categorie = $reservation->getCategorie();
	// parcourir la collection pour obtenir la cat�gorie avant et apr�s
	foreach(Categorie_Collection::factory('fr',$reservation['type']) as $x)
	{
		if ($x['id'] == $categorie['id'])
			$cat_prev = $y;
		else if ($cat_prev)
		{
			$cat_next = $x;
			break;
		}
		$y = $x;
	}
	// et afficher les deux cat�gories
	$otherCats = null;
	foreach(array($cat_prev, $cat_next) as $x)
	{
		// on n'affiche pas les cat�gories G et Z
		if ($x['mnem'] >= 'G' || $x['mnem']=='0') continue;
		// on v�rifie qu'il y a une offre
		$a = $fields;
		$a['categorie'] = $x['id'];
		$r = Reservation::create($a, $msg, $url, $reservation);
		if ($r)
			$o = $r->getOffres('v2');
		if (!$r || !$o || !$o->hasDispo())
			continue;
		// on affiche l'offre
		$otherCats.= '<div id="categorie_indispo_'.$x['id'].'" class="autres_indispo">';
		$otherCats.= '<img src="../'.$x->getImg('sml').'" alt=" "/>';
		$otherCats.= '<strong>'.$x['nom'].'</strong>';
		$otherCats.= '<input type="button" value="r�servez" onclick="doIndispo(\'categorie\','. $x['id'].');"/>';
		$otherCats.= "<div class=\"bottom\"></div>\n";
		$otherCats.= "</div>\n";
	}
	if ($otherCats)
	{
		$hasDispo = true;
		$features['categories']['html'] = $otherCats.'<div class="clear"></div>';
	}
}
// pr�sentation d'autres agences
if ($features['agences'])
{
	$agence = $reservation->getAgence();
	// parcourir les agences � proxilit� : pour la France on limite � 20km
	$agences = $agence->getAgencesAround(30, $features['agences']['statut'], ($agence['zone']=='fr' ? 20 : null));
	// si on a s�lectionn� les agences JEUNES on les mets dans ce groupe
	if ($features['agences']['statut'] & Agence::STATUT_JEUNES) {
		$jeunes = ZoneGroupe::factory('JEUNES', $agence['zone']);
	}
	// on limite � quelques agences
	$otherAgences = null;
	$nbDispo = 0;
	$maxDispo = min((isset($features['agences']['max']) ? (int) $features['agences']['max'] : 5), 5);
	$layout = (isset($features['agences']['layout']) && in_array($features['agences']['layout'], array('table','block')) ? $features['agences']['layout'] : 'table');
	$tDepart = $reservation->getDebut('U'); $tRetour = $reservation->getFin('U');
	$codeGroupes = array($agence['code_base'].'_'.$agence['code_groupe']); // en mode RESAS on ne veut pas regarder 2 fois dans le m�me groupe
	foreach($agences as /** @var Agence */ $a)
	{
		// pour les RESAS, on ne montre pas les agences du m�me "groupe"
		if ($reservation['forfait_type'] == 'RESAS') {
			$codeGroupe = $a['code_base'].'_'.$a['code_groupe'];
			// si l'agence est dans m�me groupe on a d�j� la disponibilit� des v�hicules
			if (in_array($codeGroupe, $codeGroupes)) continue;
			// OK... on cr�e le $resasUser pour interroger UNIPRO
			$codeGroupes[] = $codeGroupe;
			$resasUser = AccesAgence::createSpecial($a);
		}
		
		// si on a d�fini les JEUNES, on change l'agence de groupe
		if ($jeunes)
			$a->setData('groupe', $jeunes->getId());
		// est ce que l'agence est ouverte ?
		$msg = null;
		if ($tDepart != $a->adjustDateTime($tDepart) ||  $tRetour != $a->adjustDateTime($tRetour))
			$msg = 'FERME';
		// est ce qu'une offre est possible
		else
		{
			$o = Offre_Collection::factory($reservation, $a, 'fr', 1);
			if ($o && $resasUser && SITE_MODE=='PROD') {
				// en pr�-production il n'y a pas assez d'informtaions sur les agences...
				$o->loadTarifsUnipro($resasUser, false);
			}
			if (!$o || !$o->hasDispo())
				$msg = 'INDISPONIBLE';
		}
		// on n'affiche pas les agences FERME ou INDISPONIBLE, on garde le code pour une meilleure compr�hension de l'algorithme
		if ($msg) continue;
		$nbDispo++;
		
		// le bouton r�server
		$input = '<input type="button" value="%s" onclick="doIndispo(\'agence\', \'%s\');"/>';
		
		// pr�parer le HTML pour l'affichage sur le plan
		$a['html'] .= '<div class="btn_reserver">'.sprintf($input, 'reserver', $a['id']).'</div>';
		$jsonAgences[] = array_map(u8, $a->toArray(array('id','nom','lat','lon','html')));
		
		// on constitue le HTML pour les agences
		if ($layout == 'table') {
			$otherAgences.= '<tr id="agence_indispo_'.$a['id'].'">';
				$otherAgences.= '<td align="center"><p class="picto_indispo"><span>'.number_format($a['ortho'], 1, ',',' ').' km</span></p></td>';
				$otherAgences.= '<td>'.$a->getHtmlAddress('<br/>','nom,adresse1,cp_ville,tel').'</td>';
				$otherAgences.= '<td align="center">'.sprintf($input, 'reserver', $a['id']).'</td>';
			$otherAgences.= "</tr>\n";
		} else if ($layout == 'block') {
			$otherAgences.= '<div id="agence_indispo_'.$a['id'].'">';
				$otherAgences.= '<div class="agence_indispo_info">';
					$otherAgences.='<strong>'.$a['nom'].'</strong>';
					$otherAgences.= '<br/>'.$a->getHtmlAddress('<br/>','adresse1,adresse2,cp_ville,tel');
					$otherAgences.= $a->toHtml('block-horaires-vertical');
				$otherAgences.= '</div>';
				$otherAgences.= '<br/>'.sprintf($input, 'Je choisis cette agence', $a['id']);
			$otherAgences.= "</div>\n";
		}
		
		if ($nbDispo >= $maxDispo) break;
	}
	// enregistrer le html
	if ($nbDispo)
	{
		$hasDispo = true;
		
		$html = '<div id="map_indispo"></div>';
		if ($resasUser) {
			$html.= '<input type="hidden" name="agence_choisie" value=""/>';
		}
		if ($layout == 'table') {
			$html.= '<table cellspacing="0" cellpadding="0" border="0" id="agences_indispo"><colgroup><col width="99" /><col width="280" /><col width="120" /></colgroup>'."\n".$otherAgences."\n</table>\n";
		} else if ($layout == 'block') {
			$html.= '<div id="agences_indispo">'."\n".$otherAgences."\n</div>\n";
		}
		
		$features['agences']['html'] = $html;
		$center = $agences->getCenter();
	}
}
if ($features['calendrier'])
{
	$smartCalendar = SmartCalendar::factory($reservation, 3, $features['calendrier']['forfait_univers']);
	if ($smartCalendar->hasDispo())
		$hasDispo = true;
	$features['calendrier']['html'] = $smartCalendar->toHtml($features['calendrier']['agence_search']);
}

// afficher les diff�rentes propositinos
if ($hasDispo) :
?>
<form action="" method="post" name="frm_indispo" id="frm_indispo">
<?
	// les param�tres de la r�servation
	copy_form($fields);

	// on �crit les diff�rentes proposotions
	foreach($features as $k => $indispo) :
		if (!$indispo['html']) continue;
		if (!is_null($num)) $num++;
	?>
		<div id="div_<?=$k?>_indispo">
			<p><strong class="<? if ($num) echo 'num_'.strtolower($reservation['type']).' num_'.$num;?>"><?=$indispo['title']?></strong></p>
			<?=$indispo['html']?>
			<div class="bottom"></div>
		</div>
	<? endforeach; ?>
</form>
<script type="text/javascript">
//<![CDATA[ 
	function doIndispo(name, value)
	{
		var frm = document.forms['frm_indispo'];
		if (!value && typeof(name)=='object')
		{
			for(p in name)
			{
				if (frm[p] && frm[p].value)
					frm[p].value = name[p];
			}
		}
		else
		{
			if (name == "agence")
				sp_clic("N", frm.agence.value, "<?=strtolower($reservation['type'].'_'.$categorie['mnem'])?>", value);
			frm[name].value = value;
		}
		frm.submit();
	}
<? if ($features['calendrier']['html']) : ?>
	function updateForfait(f)
	{
		return doIndispo(f, null)
	}
<? endif; ?>
<? if (isset($features['agences']['html'])) : ?>
	$(window).load(function() {
		google.load("maps", "3", {other_params:'sensor=false<?=GOOGLE_API_KEY?>', callback: loadAgences});
	});
	var agences = <?= json_encode($jsonAgences); ?>;
	function loadAgences()
	{
		if (!agences) return;
		$('#map_indispo').css('height','230px').show();
		// d�finir le centre
		var latitude = <?= $center['latitude']; ?> ;
		var longitude = <?= $center['longitude']; ?>;
		var mylatlng = new google.maps.LatLng(latitude ,longitude);
		center = {"point": mylatlng, "title": "<?= htmlspecialchars($agence['nom']) ?>"};
		var mapOptions = {
			center: mylatlng,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			disableDoubleClickZoom : false,
			scrollwheel: false,
			draggable: true,
			keyboardShortcuts: true,
			mapTypeControl: false,
			panControl: true,
			zoomControl: true,
			scaleControl: false,
			streetViewControl: false
		};
		var map = new google.maps.Map(document.getElementById("map_indispo"), mapOptions);
		// aficher la liste des agences et r�cup�rer la zone ainsi d�limit�e
		bounds = createMarkersFromAgences(map, center, agences, function(marker)
		{
			$('#agences_indispo tr.current').removeClass('current');
			$('#agence_indispo_'+marker.agence_id).addClass('current');
		});
		map.setCenter(bounds.getCenter(), Math.min(map.fitBounds(bounds)));
	}
<? endif; // !agences ?>
// ]]>
</script>
<? endif; // !$hasDispo ?>