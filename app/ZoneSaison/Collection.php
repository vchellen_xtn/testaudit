<?
class ZoneSaison_Collection extends Ada_Collection
{
	/**
	* Cr�� une collection de saisons
	*
	* @return ZoneSaison_Collection
	*/
	static public function factory(Zone $zone)
	{
		// on cr�e la collection qui sera retourn�e
		$x = new ZoneSaison_Collection();
		// SQL pour les applications
		$sql = "SELECT * FROM zone_saison WHERE zone='".$zone->getId()."'";
		$x->_loadFromSQL($sql);
		return $x;
	}
	/**
	* Renvioe une ZoneSaison � partir d'une date
	* 
	* @param string $date
	* @returns ZoneSaison
	*/
	public function getSaison($jour, $mois)
	{
		// on cherche la saison
		foreach ($this->_items as /** var ZoneSaison */ $saison)
		{
			if ($saison->isIn($jour, $mois))
				return $saison['id'];
		}
		return null;
	}
}
?>
