<?php
/**
* Bloc pour l'insertion du code de r&eacute;duction Restopolitan dans les emails de confirmation de r&eacute;servation emails/em1.html
*/
?>
<table cellpadding="0" cellspacing="0" border="0" style="font-family:Arial, sans-serif;">
	<tr>
		<td colspan="2">
			<a href="http://www.ada.fr/offre/restopolitan.html"><img src="img/email/restopolitan/banniere.jpg" alt="ADA vous invite au restaurant en ce mois de Saint Valentin" style="vertical-align:bottom;">
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<p style="margin-top:5px; font-size:14px;">Voici votre code <img src="img/email/restopolitan/logo.jpg" alt="Restopolitan" style="vertical-align:middle;"> : <span style="color: #000; font-size:14px;"><?=$this->getData('code');?></span></p>
		</td>
	</tr>
</table>