<?php
class Visuel_Collection extends Ada_Collection
{
	protected $_position = null;
	
	/**
	* Renoive la liste des visuesl assoic�es � une page et une position
	* 
	* @param string $page
	* @param string $position
	* @return Visuel_Collection
	*/
	static public function factory($page, $position = 'left')
	{
		$x = new Visuel_Collection();
		if ($page == 'maintenance/index' || !in_array($position, array('top','left','right','bottom')))
			return $x;
		// charcher les visuels correspondants
		$sql = "SELECT v.id, v.src, v.libelle, v.href, v.hauteur, v.largeur, p.page, p.position";
		$sql.=" FROM visuel v JOIN visuel_page p ON p.visuel=v.id";
		$sql.=" WHERE v.publie=1 AND p.situation='".$position."' AND '".$page."' LIKE p.page";
		$sql.=" ORDER BY LENGTH(p.page) DESC, p.position ASC";
		
		// charger la collection
		$rs = sqlexec($sql);
		while($row = mysql_fetch_assoc($rs))
		{
			// on ne veut r�cup�rer que les informations provenant d'une page
			if (!$p) $p = $row['page'];
			if ($p != $row['page']) break;
			$pi = pathinfo($row['src']);
			$row['type'] = $pi['extension'];
			$x->add(new Visuel($row));
		}
		$x->_position = $position;
		return $x;
	}
	
	public function showVisuels($sp_from = 'fr_reservation_index')
	{
		if ($this->isEmpty()) return;
		// pr�parer prefix et suffix
		$prefix = '<div class="banniere-'.$this->_position.'">';
		$suffix = '</div>';
		// parcourir les banni�res
		foreach($this->_items as /** @var Visuel */ $visuel)
		{
			echo $prefix;
			$visuel->showVisuel($sp_from);
			echo $suffix;
		}
	}
}
?>
