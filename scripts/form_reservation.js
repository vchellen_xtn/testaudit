/**
* Les �v�nements suivant sont d�clench�s sur l'objet appADA :
* 	showAgence		[id]	identifiant de l'agence apr�s le chargement de l'agence et la mise � jour des horaires
* 	showCategories			affichage des cat�gories demand�es
* 	changedDate		[sens]	depart ou retour
* */
var appADA = appADA || {};
/**
* ATTENTION :
* 	doType() est appel�e depuis les boutons changeant de type
*/
// makeTime
function makeTime(tm)
{
	if (tm == null || !tm.toString().length)
		return null;
	a = tm.toString().split(":");
	return new Date(0, 0, 0, a[0], a[1], 0);
}

// doCheckNumber
function doCheckNumber(fld)
{
	if (!fld.value.match(/\d+/))
	{
		fld.style.backgroundColor = "#ff7777";
		fld.select();
	}
	else
	{
		fld.style.backgroundColor = "";
	}
}


/*
Gestion du formulaire de r�servation du site ADA poru g�rer les diff�rents cas
N�cessite jQuery et jQuery UI
*/
function formReservation(frm, initial, options)
{
	var that = this;
	this.frm = frm;
	this.is_loading = false; // est-on en train de charger une agence ?
	this.is_inited = false; // est ce que le formulaire a fini d'�tre initialis� ?

	/* les options pour le comportement du formulaire 
		notification_date faut-il indiquer le changement de dates ?
		statut		pour limiter la recherche des agences
		reseau		
		zone		
		jours_min	pour limiter la date de retour
		jours_max
		retour_fin	jour de retour au pire
		km_required	le kilom�trage est obligatoire
		delai_minimum faut-il v�rifier les d�lais minimum ?
	*/
	this.settings = {
		notification_date: true,
		km_required: false,
		delai_minimum: true,
		carrousel_size: "med"
	};
	if (options) { $.extend(this.settings, options); }
	// l'�tat du formulaire de la recherche courrante
	this.current = {
		zone: "fr",
		type: "",
		carrousel: "",
		categorie: "",
		h_depart: "",
		h_retour: "",
		duree: null
	};
	if (initial) { $.extend(this.current, initial); }
	this.current.carrousel = this.current.type;

	// la date de d�part, la date de retour et la premi�re date "possible"
	this.openDate = new Date();
	this.openDateTime = null;
	this.adaConfig = {
		vp : {iniOffset: 1, minOffset: 0, maxOffset: 21},
		vu : {iniOffset: 1, minOffset: 0, maxOffset: 28},
		sp : {iniOffset: 7, minOffset: 7, maxOffset: 28},
		ml : {iniOffset: 0, minOffset: 0, maxOffset: 1}
	};

	// le nombre d'heures minimum avant une r�servation
	this.delaiMinimum = { 
		mode: null, 
		vp: null, 
		vu: null, 
		sp: null, 
		ml: null 
	};

	// donn�es sur l'agence pour g�rer l'�tat � chaque changement d'agence
	this.agenceInfo = {
		daysClosed: null, 
		openingHours: null
	};
	this.setOptions = function(options)
	{
		if (options) { $.extend(this.settings, options); }
	}

	var numberOfMonthsConf = 2;
	if(appADA.skin == 'mobile') {
		numberOfMonthsConf = 1
	}
	/* initialisation du formulaire */
	this.init = function()
	{
		var datepickerDefaults = {
			// localisation
			closeText:"Fermer",
			prevText:"Pr�c�dent",
			nextText:"Suivant",
			currentText:"Aujourd'hui",
			dayNames:["dimanche","lundi","mardi","mercredi","jeudi","vendredi","samedi"],
			dayNamesShort:["dim.","lun.","mar.","mer.","jeu.","ven.","sam."],
			dayNamesMin: ['d','l','m','m','j','v','s'],
			monthNames: ['janvier','f�vrier','mars','avril','mai','juin','juillet','ao�t','septembre','octobre','novembre','d�cembre'],
			monthNamesShort:["janv.","f�vr.","mars","avril","mai","juin","juil.","ao�t","sept.","oct.","nov.","d�c."],
			// configuraiton
			minDate: 0,
			maxDate: "+12m",
			numberOfMonths: numberOfMonthsConf,
			changeMonth: false,
			dateFormat: 'dd-mm-yy',
			firstDay: 1,
			showOtherMonths : true,
			selectOtherMonths : true
		};
		// les calendriers
		$.datepicker.setDefaults(datepickerDefaults);
		$('#depart').datepicker( {onSelect: this.doCheckHoraireDepart, beforeShowDay: this.checkDateDepart} );
		$('#retour').datepicker( {onSelect: this.doCheckHoraireRetour, beforeShowDay: this.checkDateRetour} );
		$('#calendar img.btn_cal').css('cursor', 'pointer').click(function()
		{
			if (!that.agenceInfo.openingHours) return;
			$('#' + this.id.replace('btn_', '')).datepicker('show'); 
		});
		$("#h_depart").change(function(){
			that.doCheckHoraireRetour();
		});
		$('#calendar input').attr('autocomplete', 'off').attr('readonly', true);
		// v�rification du code promo
		$('#codepromo').change(function()
		{ 
			this.value = $.trim(this.value);
			if (!this.value.length) {
				$("#error_codepromo").html("");
			} else {
				$.post('../api/json/codepromo.check', $(this.form).serialize(), function(response)
				{
					if (response.message) {
						if ($("#error_codepromo").length) {
							$("#error_codepromo").html(response.message);
						} else {
							alert(response.message);
						}
					}
				},'json');
			}
			
		});
		// validation du formulaire
		$(frm).submit(function(){
			if (that.doCheck(this, false)) {
				$('<div class="loader-search"></div>').appendTo(document.body).addClass('ui-widget-overlay').css({width:$(document.body).width(), height:$(document.body).height(), 'z-index':'1000', 'background':'#000 url(../css/img/structure-2014/ajax-loader.gif) no-repeat center 600px', 'opacity':'0.5'});
				return true;
			}
			return false;
		});

		// agence / ville
		if ($.fn.tagdragon)
		{
			var url_agences = '../json.agences.php';
			var params = {};
			if (this.settings.zone) params.zone = this.settings.zone;
			if (this.settings.reseau) params.reseau = this.settings.reseau;
			if (this.settings.statut) params.statut = this.settings.statut;
			var q = $.param(params);
			if (q) url_agences += "?" + q;
			$('#autocomplete').tagdragon({
				'field': 'ville',
				'url': url_agences,
				'max': 40,
				'charMin': 3,
				'delay': 100,
				'onSelectedItem': function(val) { if (val.id) that.doAgence(val.id); },
				'onLoadedList': function(results) {
					len = (results && results.length > 0);
					$('#ville_ok').css('display', len ? "block" : "none");
					$('#ville_ko').css('display',!len ? "block" : "none");
				}
			});
		}
		
		// type : on force le type pour assurer la coh�rence en cas de rafraichissement de la page
		$('#radio_'+this.current.type).attr('checked','checked');

		// agence s�lectionn�e
		if (id = $('#agence').val())
		{
			this.doAgence(id);
		}
		else
		{
			this.enableCalendarControls(false);
			this.is_inited = true;
		}
		// v�rifier le bon affichage
		this.doType(this.current.type);
	};
	
	/* mise � jour suite � choix utilisateurs */
	// doType : change le type de v�hicule
	this.doType = function(val)
	{
		// on enl�ve la s�lection sur le label courant
		$('#recherche .menu_reservation label').removeClass('current').filter('[for=radio_'+val+']').addClass('current');
		// changer le type courant
		this.current.type = val;
		
		// modifier l'affichage pour le v�hicule sans permis
		$('#div_sp_info').css('display', (val=='sp' ? 'block' : 'none'));
		$('#span_h_depart, #span_h_retour').css('display', 'block'); /* (val=='sp' ? 'none' : 'block')); */
		$('#retour, #btn_retour').css('display', (val=='ml' ? 'none' : 'inline'));
		$('#span_ml_retour').css('display', (val=='ml' ? 'block' : 'none'))

		// re-v�rfiier les dates de d�part et de retour
		if (this.is_inited) this.is_loading = true;
		if ($('#depart').val() && $('#retour').val() && this.setOpenDateTime() && this.settings.notification_date)
			alert("D�sol�, les dates s�lectionn�es ne sont pas valides pour ce type de v�hicule.\nMerci de modifier votre s�lection.");
		if (this.is_inited) this.is_loading = false;
		
		// afficher les cat�gories correspondant au type de v�hicule
		this.showCategory();
	};
	
	// obtenir les informations sur une agence
	this.doAgence = function(id)
	{
		this.is_loading = true;
		$agence = $("#agence");
		if ($agence.get(0).type != "select-one") {
			$agence.empty();
		}
		this.enableCalendarControls(false, 'chargement en cours');
		if (!$.trim(id).length) return;
		
		// mettre � jour les calendriers
		$.get('../xmlagenceinfo.php?id=' + id, function(xml)
		{ 
			var e = xml.documentElement;
			if (!e || !e.getAttribute("id"))
			{
				that.enableCalendarControls(false, 'agence inconnue');
				return;
			}
			if ($agence.get(0).type != "select-one") {
				$agence.attr('value', e.getAttribute('id'));
			}
			if (that.current.zone != e.getAttribute('zone'))
			{
				that.current.zone = e.getAttribute('zone');
				that.showCategory();
			}
			if (that.current.zone=="gp") {
				that.adaConfig.vp.maxOffset = 45;
			} else if (that.current.zone.match(/^(fr|co|lu|gf|mu)$/)) {
				that.adaConfig.vp.maxOffset = 30;
			} else {
				that.adaConfig.vp.maxOffset = 21;
			}
			that.doHoraires(xml); 
			// v�rifier si l'agence fait bo�te aux lettres
			if (e && e.getAttribute('bal')=='1')
				$('#agence_bal').show();

			// v�rifier que les dates de d�part/retour sont toujours valides
			var err = [];
			$('#depart, #retour').each(function()
			{
				if ((d = $(this).datepicker('getDate')) && !that.isBusinessDay(d, this.id))
				{
					$(this).val(null); //datepicker('setDate',  null); 
					err.push('- la date de ' + this.id + ' s�lectionn�e n\'est pas valide pour cette agence');
				}
			});
			// puis demander l'affichage de l'agence
			that.is_loading = false;
			$(appADA).trigger("showAgence", [id]);
			if (!that.is_inited) {
				that.is_inited = true;
			}
			// et informer s'il y a une erreur
			if (err.length)
				alert('D�sol� :\n' + err.join('\n') + '\nMerci de modifier votre s�lection.');
		});
	};

	// met � jour l'affichage des cat�gories apr�s un changement de type
	// la cat�gorie peut �tre une liste, un champ input hidden avec un carrousel ou non
	this.showCategory = function()
	{
		$(appADA).trigger("showCategories");
		// m�moriser la valeur pr�c�dente
		var lst = this.frm.categorie;
		if (!lst) return;
		if (!lst.options)
		{
			if ($.fn.carrousel && $("#carrousel_categorie").length)
			{
				if (this.current.carrousel == this.current.type) return;
				this.current.carrousel = this.current.type;
				// V2 : mettre � jour le carousel
				/* dans certains cas on a un UL pour chaque type de v�hicule */
				$band = $("#carrousel_"+this.current.carrousel);
				if ($band.length) {
					/* si d�j� rempli inutile de recharger */
					if ($band.find("li").length) {
						$('#carrousel_categorie ul.carrousel-band').removeClass("carrousel-band");
						$band.addClass("carrousel-band");
						$('#carrousel_categorie').carrousel('update');
						return;
					}
				} else {
					$band = $("#carrousel_categorie ul.carrousel-band");
				}
				/* recharger le carrousel */
				$.get('../lib.categorie.php?skin=v2&zone=fr&type=' + this.current.type + "&size=" + this.settings.carrousel_size, function(html)
				{
					$band.html(html);
					if (!$band.hasClass("carrousel-band")) {
						$('#carrousel_categorie ul.carrousel-band').removeClass("carrousel-band");
						$band.addClass("carrousel-band");
					}
					$('#carrousel_categorie').carrousel('update');
				});
				return;
			}
			else
			{
				// on ne sait pas quoi faire...
				return;
			}
		}
		else
		{
			// utilis� pour le agences/liste et agences/agence
			var val = (lst.selectedIndex > 0) ? lst.options[lst.selectedIndex].value : this.current.categorie;
			$("#cat_" + val).hide();
			// vider le menu des cat�gories
			lst.options.length = 1;
			msg = "Choisissez d'abord une agence";
			if (this.current.type && this.current.zone)
			{
				// le remplir en fonction du type et de la zone
				var zone = this.current.zone;
				if (!mnuCategory[zone + '-' + this.current.type])
					zone = "fr";
				// remplir la liste des cat�gories
				for(var a=mnuCategory[zone + '-' + this.current.type], n=a?a.length:0, i=0; i<n; i++)
				{
					v = a[i].match(/^(\d+)\-(.+)/);
					lst.options.add (new Option(v[2], v[1], (v[1]==val), (v[1]==val)));
				}
				msg = (i > 0) ? "Choisissez un v�hicule" : "Erreur: pas de cat�gories pour cette zone";
			}
			lst.options[0].text = "--- " + msg + " ---";
		}
	};

	/* calendrier et horaires */
	// enableCalendarControls
	this.enableCalendarControls = function(enabled, info)
	{
		$('#calendar :input').attr('disabled', !enabled);
		if (info) $('#calendar img.btn_cal').attr('title', info).attr('alt', info);
		if (!enabled) $('#agence_bal').hide();
	};
	this.getDays = function()
	{
		var d = $('#depart').datepicker('getDate'),
			r = $('#retour').datepicker('getDate');
		// mettre � jour la dur�e de la r�servation
		this.current.duree = (r && d) ? (r.getTime() - d.getTime()) / (1000 * 60 * 60 * 24) : 0;
		return this.current.duree;
	}
	this.getIniOffsetDays = function()
	{
		if (this.settings.jours_max && this.current.type!="sp")
			return this.settings.jours_max;
		if (this.settings.jours_min)
			return this.settings.jours_min;
		if (typeof(this.current.duree) == "number")
			return this.current.duree;
		return this.adaConfig[this.current.type].iniOffset;
	}
	this.getMinOffsetDays = function()
	{
		return (this.settings.jours_min ? this.settings.jours_min : this.adaConfig[this.current.type].minOffset);
	}
	this.getMaxOffsetDays = function()
	{
		return (this.settings.jours_max ? this.settings.jours_max : this.adaConfig[this.current.type].maxOffset);
	}
	this.getLastMonday = function(d)
	{
		var j = d.getDay();
		if (j==0) j=7;
		var monday = new Date();
		monday.setTime(d.getTime() - (j-1)*3600*24000);
		return monday;
	}
	// checkDateDepart : v�rifie la validit� d'une date de d�part
	this.checkDateDepart = function(d)
	{
		return [(d.getTime() >= that.openDate.getTime() && that.isBusinessDay(d, 'depart')), ''];
	};
	// checkDateRetour : v�rifie la validit� d'une date de retour
	this.checkDateRetour = function(d)
	{
		var minOffset =  that.getMinOffsetDays() * 3600 * 24000;
		var maxOffset = that.getMaxOffsetDays() * 3600 * 24000;
		var dDepart = $('#depart').datepicker('getDate');
		if (!dDepart) {
			return [false, ''];
		}
		var tDepart = dDepart.getTime();
		var t = d.getTime();
		// v�rifier si d�part et retour doivent se faire dans la m�me semaine
		if (t > tDepart && that.settings.retour_fin && that.settings.retour_fin < 7 && that.getMaxOffsetDays() < 7)
		{
			// le d�part et le retour doivent se faire la m�me semaine
			if (that.getLastMonday(d) > that.getLastMonday(dDepart))
				return [false, ""];
		}
		return [((t >= minOffset + tDepart) && (t <= maxOffset + tDepart) && that.isBusinessDay(d, 'retour')), ''];
	};
	// doCheckHoraireRetour : appel� quand le champ texte contenant la date de retour change
	this.doCheckHoraireRetour = function()
	{
		that.setHoraireMenu (that.frm.h_retour, $('#retour').datepicker('getDate'));
		$(appADA).trigger("changedDate", "retour");
	};
	// doCheckHoraireDepart : la date de d�part change
	this.doCheckHoraireDepart = function()
	{
		var dateDepart = $('#depart').datepicker('getDate');
		that.setHoraireMenu (that.frm.h_depart, dateDepart);
		var dateRetour = $('#retour').datepicker('getDate');
		if (!dateRetour || !(that.checkDateRetour(dateRetour)[0]))
		{
			dateRetour = that.nextBusinessDay(dateDepart, that.getIniOffsetDays(), "retour");
			// si on a d�pass� le nombre de jours on r�-essaye avec le nombre de jours minimum
			if ((dateRetour.getTime() - dateDepart.getTime())/(1000*60*60*24) > that.getMaxOffsetDays() || !(that.checkDateRetour(dateRetour)[0]))
				dateRetour = that.nextBusinessDay(dateDepart, that.getMinOffsetDays(), "retour");
			$('#retour').datepicker('setDate', dateRetour);
		}
		that.doCheckHoraireRetour();
		$(appADA).trigger("changedDate", "depart");
		return false;
	};

	/* fonctions agenceInfo */

	// doHoraires : obtient les jours de fermeture et les heures d'ouvertures et d�termine la premi�re heure possible pour la location
	this.doHoraires = function(xml)
	{
		// obtenir les d�lais pour la zone 
		// sans objet pour les t�l�conseillers...
		if (this.settings.delai_minimum && document.cookie.indexOf("ADA001_TC_MODE=") == -1)
		{
			this.delaiMinimum.vp   = parseInt(xml.documentElement.getAttribute("vp_delai_min"));
			this.delaiMinimum.vu   = parseInt(xml.documentElement.getAttribute("vu_delai_min"));
			this.delaiMinimum.sp   = parseInt(xml.documentElement.getAttribute("sp_delai_min"));
			this.delaiMinimum.ml   = parseInt(xml.documentElement.getAttribute("ml_delai_min"));
			this.delaiMinimum.mode = xml.documentElement.getAttribute("delai_mode");
		}
		// jours de fermeture
		this.agenceInfo.daysClosed = new Array(12);
		for (var i=0, lst=xml.getElementsByTagName("fermeture"); i<lst.length; i++)
		{
			// apparemment parseInt("08") = 0 sur FF....
			m = lst[i].getAttribute("mois") - 1;
			if (!this.agenceInfo.daysClosed[m])
				this.agenceInfo.daysClosed[m] = new Array();
			this.agenceInfo.daysClosed[m].push(lst[i].getAttribute("jour"));
		}
		// heures d'ouverture
		this.agenceInfo.openingHours = null;
		for (var i=0, lst=xml.getElementsByTagName("horaire"); i<lst.length; i++)
		{
			if (!this.agenceInfo.openingHours) this.agenceInfo.openingHours = new Array(7);
			// le jour 1-7, convertit en ISO 0-6
			j = parseInt(lst[i].getAttribute("jour"));
			if (j == 7)
				j = 0;
			this.agenceInfo.openingHours[j] = new Array(
				  makeTime(lst[i].getAttribute("ouverture"))
				, makeTime(lst[i].getAttribute("fermeture"))
				, makeTime(lst[i].getAttribute("pause_debut"))
				, makeTime(lst[i].getAttribute("pause_fin"))
				);
		}
		if (this.agenceInfo.openingHours)
		{
			// v�rification des dates et des heures
			this.enableCalendarControls(true, "ouvrir le calendrier");
			this.setOpenDateTime();
		}
	};
	// setOpenDateTime : return 'true' si les dates changent
	this.setOpenDateTime = function()
	{
		var hasChanged = false;
		// on doit avoir les heures d'ouveture d'une agence
		if (!this.agenceInfo.openingHours || !this.agenceInfo.openingHours.length) 
			return hasChanged;
		this.openDateTime = this.getOpenDateTime();
		this.openDate = new Date(this.openDateTime);
		this.openDate.setHours(0,0,0,0);

		// s'il y a d�j� une date de d�part et qu'elle est avant openDate...
		if (!(depart = $('#depart').datepicker('getDate')) || !(this.checkDateDepart(depart)[0]))
		{
			depart = this.nextBusinessDay(this.openDate, 0, 'depart');
			$('#depart').datepicker('setDate', depart);
			hasChanged = true;
		}
		// ajuster le menu des horaires
		this.doCheckHoraireDepart();
		return hasChanged;
	};

	// getOpenDateTime : renvoie la premi�re date possible de r�servation
	this.getOpenDateTime = function()
	{
		// on arrondit � l'heure sup�rieure
		var dOpen = new Date();
		dOpen.setHours (dOpen.getHours() + 1, 0, 0, 0);

		// le d�lai minimum est soit en heures (C)alendaire soit en heures d'(O)uverture
		if (this.delaiMinimum.mode != 'O')
		{
			// pour les heures calendaires
			dOpen.setHours(dOpen.getHours() + this.delaiMinimum[this.current.type]);
			if 
			(
				!this.isBusinessDay(dOpen, 'depart')
				|| this.agenceInfo.openingHours[dOpen.getDay()][1].getHours() < dOpen.getHours()
				|| (this.current.type=='sp' && dOpen.getHours() > this.agenceInfo.openingHours[dOpen.getDay()][0].getHours()) // pour les sans permis on n'affiche pas l'heure, il faut que ce soti la premi�re heure
			)
			{
				dOpen = this.nextBusinessDay (dOpen, 1, 'depart');
				dOpen.setHours(this.agenceInfo.openingHours[dOpen.getDay()][0].getHours() );
			}
			return dOpen;
		}
		// pour les heures d'ouvertures
		var h = this.delaiMinimum[this.current.type];
		if (!h) h = 1;
		var offset = (this.isBusinessDay(dOpen, 'depart') && this.agenceInfo.openingHours[dOpen.getDay()][1].getHours() > dOpen.getHours() ) ? 0 : 1;
		while (h > 0)
		{
			// on passe au jour suivant (sauf pour la premi�re fois)
			if (offset)
			{
				dOpen = this.nextBusinessDay(dOpen, offset, 'depart');
				dOpen.setHours( this.agenceInfo.openingHours[dOpen.getDay()][0].getHours() );
			}
			else
				offset = 1;
			// on enl�ve le nombre d'heures ouvrables
			h -= this.agenceInfo.openingHours[dOpen.getDay()][1].getHours() - dOpen.getHours();
		}
		dOpen.setHours(this.agenceInfo.openingHours[dOpen.getDay()][1].getHours() + h, 0, 0, 0);
		return dOpen;
	};

	// nextBusinessDay
	this.nextBusinessDay = function(jour, offset_days, sens)
	{
		var d = new Date(jour), dayms = 24 * 3600 * 1000;
		d.setHours (3, 0, 0, 0);
		for(d.setTime(d.getTime() + offset_days * dayms);; d.setTime(d.getTime() + dayms))
		{
			if (this.isBusinessDay(d, sens))
				break;
		}
		d.setHours (0, 0, 0, 0);
		return d;
	};

	// renvoie true si d est ouvert
	this.isBusinessDay = function(d, sens)
	{
		// des jours de fermeture pour le mois indiqu� ?
		if (this.agenceInfo.daysClosed && this.agenceInfo.daysClosed[d.getMonth()] != null)
		{
			for (var x=0; x < this.agenceInfo.daysClosed[d.getMonth()].length; x++) 
			{
				if (this.agenceInfo.daysClosed[d.getMonth()][x] == d.getDate())
					return false;
			}
		}
		// s'il n'y a aucuen heure d'ouverture renseign�e, on arr�te une boucle infinie
		if (!this.agenceInfo.openingHours) 
			return true;
		// pas d'heures d'ouvertures ce jour-l�
		if (!this.agenceInfo.openingHours[d.getDay()])
			return false;
		// s'il y a des contraintes particuli�res pour le debut et la fin
		var debut = this.settings[sens + "_debut"], fin = this.settings[sens + "_fin"];
		if (debut && fin)
		{
			// on prend les parties enti�res du debut et de la fin
			debut = parseInt(debut); fin = parseInt(fin);
			var jour = d.getDay(); // entre 0 et 6
			if (jour==0) jour = 7;
			if (jour >= debut && jour <= fin)
				return true;
			else if (fin > 7 && (jour+7 <= fin))
				return true;
			else
				return false;
		}
		return true;
	};
	this.formatHHMM = function(h, m)
	{
		return (h.toString().length > 1 ? "" : "0") + h + ":" + (m.toString().length > 1 ? "" : "0") + m;
	}
	/* fonctions utilitaires */
	// setHoraireMenu: liste � remplir, jour de la semaine et date du calendrier
	this.setHoraireMenu = function(lst, jour, old_val)
	{
		// si nous sommes aujourd'hui il faut limiter les heures affich�es
		var d = new Date(jour);
		var sens = lst.name.substr(2); // enlever "h_" pour avoir "depart" ou "retour"
		d.setHours (0, 0, 0, 0);
		var h_end = "24:00", h_start = this.formatHHMM((this.openDate.getTime() == d.getTime()) ? this.openDateTime.getHours() : 0, 0);
		
		// s'il ne s'agit pas d'un champ "select"...
		if (!lst.options) return;

		// v�rifier s'il y a des conditions d'horaires sp�cifiques en cours
		var debut = this.settings[sens + "_debut"], fin = this.settings[sens + "_fin"];
		if (debut && fin)
		{
			var j = d.getDay();
			if (j==0) j = 7;
			if (j == parseInt(debut))
				h_start = this.formatHHMM(Math.max(parseInt(h_start), Math.round(100 * (debut - parseInt(debut)))), 0);
			if (j == parseInt(fin) || (fin >= 8 && (j+7)==parseInt(fin)))
				h_end = this.formatHHMM(Math.min(parseInt(h_end),  Math.round(100 * (fin - parseInt(fin)))), 0);
		}
		// on adatpte les heures si on est proche du maximum
		if (sens == "retour")
		{
			var days = this.getDays();
			var h_depart = frm.h_depart.value;
			if (days == this.getMaxOffsetDays())
				h_end = (h_end < h_depart ? h_end : h_depart);
			else if (days == this.getMinOffsetDays())
				h_start = (h_start > h_depart ? h_start : h_depart);
			else if (days > this.getMaxOffsetDays() || days < this.getMinOffsetDays())
			{
				h_start = h_end = "";
			}
		}

		if (!old_val || old_val=='undefined')
			old_val = (lst.selectedIndex >= 0) ? lst.options[lst.selectedIndex].value : "";
		if (!old_val.length)
			old_val = this.current[lst.name];
		lst.options.length = 1;
		if (!jour || !this.agenceInfo.openingHours || !(a = this.agenceInfo.openingHours[jour.getDay()]))
			return;

		// attention � faire une copie de l'objet date...
		for(var t=new Date(a[0]); t<=a[1]; t.setMinutes(t.getMinutes()+30))
		{
			// ajouter les heures valides
			if (!a[2] || !a[3] || t <= a[2] || t >= a[3])
			{
				if (sens == "retour" && t.getTime() == a[1].getTime()) {
					// pour la derni�re heure du retour on enl�ve 15mn
					t.setMinutes(t.getMinutes() - 15)
				}
				v = this.formatHHMM(t.getHours(), t.getMinutes());
				if (h_start && v < h_start) continue;
				if (h_end && v > h_end && (this.current.type!="sp" || lst.options.length > 1)) break;
				if (!old_val.length) old_val = v;
				lst.options[lst.options.length] = new Option (v, v, (v == old_val), (v == old_val));
			}
		}
		// s�lectionner automatiquement les horaires pour le type "sp"
		if (this.current.type == 'sp' && lst.options.length > 1)
		{
			if (sens == "depart" || (sens=="retour" && days > 0))
				lst.selectedIndex = 1;
			else
				lst.selectedIndex = lst.options.length-1;
		}
		// si aucun horaire n'est s�lectionn� on essaie de retrouver le plus proche
		if (lst.selectedIndex < 1 && old_val.length && lst.options.length > 1) {
			for(var i=1; i < lst.options.length;i++) {
				if (lst.options[i].value >= old_val) {
					lst.selectedIndex = i;
					break;
				}
			}
			if (lst.selectedIndex < 1) {
				lst.selectedIndex = lst.options.length - 1;
			}
		}
	};
	/* validation du formulaire */
	this.doCheck = function(frm, with_fix)
	{
		var err = "", 
			d = $('#depart').datepicker('getDate'),
			r = $('#retour').datepicker('getDate');
		if (!frm.agence.value.length)
			err += "\n- L'agence indiqu�e est invalide";
		if (frm.categorie && !frm.categorie.value.match(/^\d+$/))
			err += "\n- Vous devez indiquer la cat�gorie du v�hicule";
		if (!frm.h_depart || (frm.h_depart.options && !validateSelect(frm.h_depart)) || !frm.h_depart.value.match(/^\d{2}\:\d{2}$/))
			err += "\n- Vous devez indiquer l'heure de d�part";
		if (d && r && d.getTime() > r.getTime())
			err += "\n- La date de d�part ne peut �tre post�rieure � la date de retour";
		if (!frm.h_retour || (frm.h_retour.options && !validateSelect(frm.h_retour)) || !frm.h_retour.value.match(/^\d{2}\:\d{2}$/))
			err += "\n- Vous devez indiquer l'heure de retour";
		var days = (r && d) ? (r.getTime() - d.getTime()) / (1000 * 60 * 60 * 24) : 0, 
			max_days = this.getMaxOffsetDays();
		if (days > max_days && this.current.type != 'sp')
			err += "\n- La dur�e de la location demand�e est trop �lev�e pour une r�servation en ligne";
		if (this.current.type=='sp' && days < this.getMinOffsetDays())
			err += "\n- Pour une location d'un v�hicule sans permis inf�rieure � " + this.getMinOffsetDays() + " jours, merci de contacter directement votre agence";
		if (days == 0 && frm.h_retour.value <= frm.h_depart.value)
			err += "\n- L'heure de d�part doit �tre ant�rieure � l'heure du retour";
		if (days == max_days && frm.h_retour.value > frm.h_depart.value && this.current.type!="sp")
			err += "\n- L'heure de retour doit �tre avant " + frm.h_depart.value + " pour ne pas d�passer " + max_days + (max_days > 1 ? " jours" : " jour");
		if (frm.age && frm.age.options && !validateSelect(frm.age))
			err += "\n- Vous devez indiquer votre �ge";
		if (this.current.type != 'sp' && frm.permis && frm.permis.options && !validateSelect(frm.permis))
			err += "\n- Vous devez indiquer votre nombre d'ann�es de permis";
		if (frm.distance) {
			if (!frm.distance.value.match(/^\d*$/)) {
				err += "\n- La distance � parcourir est invalide";
			} else if (this.settings.km_required && !frm.distance.value.match(/^\d{2,4}$/)) {
				err += "\n- Vous devez indiquer la distance � parcourir";
			}
		}
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		// message d'avertissement avant modification des dates pour les v�hicules "sans permis"
		if (this.current.type == 'sp' && days > max_days && !with_fix)
		{
			alert("La dur�e de r�servation en ligne maximum est d'un mois, le tarif affich� correspond au premier mois de location, au del� le paiement s'effectue en agence");
			r = this.nextBusinessDay(d, 28); 
			$('#retour').datepicker('setDate', r);
			this.setHoraireMenu (this.frm.h_retour, r);
			return this.doCheck (frm, true);
		}
		frm.target = (this.current.zone == "gp" ? "_blank" : "_self");
		return true;
	}
	// ex�cuter l'initialisation
	this.init();
};

