<?php

define('OWM_API_ID', '842c3751400f98cc040b4359f03a35a8');

class AgenceMeteo extends Ada_Object
{
	const URL_ZIP  = 'http://api.openweathermap.org/data/2.5/weather?mode=json&units=metric&lang=fr&zip=%s,%s&appid=%s';
	const URL_CITY = 'http://api.openweathermap.org/data/2.5/weather?mode=json&units=metric&lang=fr&q=%s,%s&appid=%s';
	
    /**
     *
     * @author Fran�ois Lasselin flasselin@rnd.fr
     *
     *         This method return a weather html block
     *         It used a specific cache system to avoid requesting weather API at each page
     *         the openweather free API only allow 60 requests per minutes
     *         the cache is orgnised in the filesystem in cache/meteo/
     *         for each displayWeather request, it check if a file matching postal code and zone
     *         already exist. If it exist it check if it's not too old.If the data is fresh, it return the cache content
     *         Else, it will request the api by bufering the output (to put the content in cache)
     *         the api is not always successfull to recognize city with postale code. In cas of failure, it try the city name
     *         Finaly, it load the block-meteo wich content the html decoration
     *         Both json api output and html block render are saved to cache.
     *
     *         More information about API can be found in ADA021/Maintenance/references/Meteo/api-meteo.txt
     * @param Agence $agence agence pur laquelle on veut la m�t�o
     * @return string block HTML � afficher
     */
    static public function getWeather(Agence $agence)
    {
        $secondes_cache = 60 * 60;
        $dirMeteo = BP.'/cache/meteo';
        if (!is_dir($dirMeteo)) {
			mkdir($dirMeteo, 0755, true);
        }
        $fname = $dirMeteo.'/'.strtoupper(sprintf('%s-%s', $agence['zone'], preg_replace('/[^a-z0-9]/i', '', $agence['cp'])));
        $fnameHtml = $fname.'.html';
        $fnameJson = $fname.'.json';
        $ageFichier = (file_exists($filenamehtml)) ? filemtime($filenamehtml) : 0;
        if ($ageFichier > time() - $secondes_cache) {
            return file_get_contents($fnameHtml);
        } else {
            $url = sprintf(self::URL_ZIP, urlencode($agence['cp']), $agence['zone'], OWM_API_ID);
            $response = file_get_contents($url);
            $weather = json_decode($response, true);
            if ($weather["cod"] == "404") {
                $url = sprintf(self::URL_CITY, urlencode($agence['ville']), $agence['zone'], OWM_API_ID);
                $response = file_get_contents($url);
                $weather = json_decode($response, true);
            }
            if ($weather["cod"] != "404") {
            	$x = new AgenceMeteo();
                $block = $x->toHtml("block-meteo", array(
                    'ville' => $agence['ville'],
                    'zone' => $agence['zone'],
                    'weather' => $weather['weather'][0]['icon'],
                    'description' => utf8_decode($weather['weather'][0]['description']),
                    'temperature' => round($weather['main']['temp'])
                ));
                $fp = fopen($fnameHtml, 'w');
                if (flock($fp, LOCK_EX)) {
                    ftruncate($fp, 0);
                    fwrite($fp, $block);
                    fflush($fp);
                    flock($fp, LOCK_UN);
                }
                fclose($fp);
            }
        }
        $pointeur = fopen($fnameJson, 'w');
        fwrite($pointeur, $response);
        fclose($pointeur);
        return $block;
    }
}