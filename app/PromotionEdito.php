<?
/**
* G�n�re les textes editoriaux pour les promotions (nom, description, etc.)
* tables promotion_forfait, agence_promo_forfait
*/
class PromotionEdito 
{
	static public function createInterval($d1, $d2) {
		return self::_createInterval($d1, $d2);
	}
	
	static private function _createInterval($d1, $d2)
	{
		$t1 = strtotime(euro_iso($d1));
		$t2 = strtotime(euro_iso($d2));
		if ($t1 && $t2)
			return 'du '.date('d/m', $t1).' au '.date('d/m', $t2);
		else if ($t1)
			return 'apr�s le '.date('d/m', $t1);
		else if ($t2)
			return 'avant le '.date('d/m', $t2);
	}
	
	/**
	* Renvoie les textes �ditoriaux � partir des donn�es d'une promotion
	* @param Array $a les donn�es d'entr�e telles que agence, categories, etc
	* @return Array Les cl�s du tableau renvoy� sont : nom, slogan, slogan_iphone, description, mentions
	*/
	static public function getEdito($a)
	{
		/*
		*	$zone	nom de la zone
		* 	$what		montant de la promotion
		* 	$periode	"en semaine" ou "le week-end"
		* 	$for		type, cat�gorie
		* 	$where		agence, groupe ou zone
		*	$when		pr�cisions sur les dates
		*	$validite			conditions de date, nombre de jours de r�servation
		*/
		$token = array();
		
		// zone
		$zone = Zone::factory($_SESSION['ADA001_ADMIN_ZONE']);
		$zoneText = $zone['prefix'].$zone['nom'];
		if ($zone['id']=='fr') 
			$zoneText .= ' m�tropolitaine hors Corse';
		$token[] = strtoupper($zone['id']);

		// Grand Public ou Sp�cial
		$token[] = ($a['classe'] ? 'CODE' : 'TOUS');

		// localisation
		$where = ''; 
		$whereSlogan = '';
		if ($a['agence'] && (!is_array($a['agence']) || count($a['agence'])==1))
		{
			if (is_array($a['agence']))
				$a['agence'] = $a['agence'][0];
			$agence = Agence::factory(filter_var($a['agence'], FILTER_SANITIZE_STRING));
			$token[] = $agence['id'];
			$whereSlogan = $agence['nom'];
			$where = "dans l'agence ".$whereSlogan;
		}
		else if ($a['code_societe'] || is_array($a['agence']))
		{
			$token[] = '#agence_id#';
			$whereSlogan = '#agence_nom#';
			$where = "dans l'agence #agence_nom#";
		}
		else if ($a['groupe'])
		{
			$groupe = ZoneGroupe::factory((int)$a['groupe']);
			$whereSlogan = '';
			$where = 'dans les agences '.$groupe['nom'];
			$token[] = $groupe['nom'];
		}
		else if ($a['reseau'])
		{
			$whereSlogan = '';
			$where = 'dans les agences '.($a['reseau']=='ADA' ? 'ADA' : 'partenaires');
			$token[] = filter_var($a['reseau'], FILTER_SANITIZE_STRING);
		}
		else
		{
			$where = $zoneText;
			$token[] = strtoupper($zone['id']);
		}

		// type ou cat�gorie
		if (is_array($a['categorie']))
		{
			if (count($a['categorie']) > 1)
			{
				$for = '#categorie_nom#';
				$token[] = strtoupper(filter_var($a['type'], FILTER_SANITIZE_STRING)).' #categorie_mnem#';
			}
			else
			{
				$categorie = Categorie::factory($a['categorie'][0]);
				$for = $categorie['nom'];
				$token[] = strtoupper($categorie['type']). ' '.$categorie['mnem'];
			}
		}
		else if ($a['categories'])
		{
			// plusieurs cat�gories s�par�s par des vrigules (agence_promo_forfait)
			$cats = array();
			$categories = Categorie_Collection::factory('fr', $a['type']);
			$mnems = (is_array($a['categories']) ? $a['categories'] : explode(',', $a['categories']));
			foreach($mnems as $c) {
				$cats[] = $categories->getItem($c)->getData('nom');
			}
			$for = implode(', ', $cats);
			$token[] = strtoupper(filter_var($a['type'], FILTER_SANITIZE_STRING)).implode(',', $mnems);
			if (count($cats) > 1) {
				$forSlogan = (strtolower($a['type'])=='vp' ? 'certaines voitures' : 'certains utilitaires');
			}
		}
		else if ($a['type'])
		{
			$for = Categorie::$TYPE_LBL[$a['type']];
			if ($for)
				$token[] = strtoupper($a['type']);
		}
		else
		{
			$for = '';
			$token[] = 'Vx';
		}
		if (!$forSlogan) {
			$forSlogan = $for;
		}

		// semaine ou week-end
		$prefix = strtoupper(substr($a['forfait'] ? $a['forfait'] : $a['forfait_prefix'], 0, 2));
		if ($prefix)
		{
			$periode = ($prefix=='JC' || $prefix=='AD') ? 'en semaine' : 'le week-end';
			$token[] = filter_var($a['forfait'] ? $a['forfait'] : substr($a['forfait_prefix'], 0, -1), FILTER_SANITIZE_STRING);
		}
		
		// montant de la promotion
		$what = sprintf('%+ld', (int)$a['montant']).($a['mode']=='P' ? '%' : '�');
		$token[] = $what;

			
		// dates de d�part et conditions sur les jours dans le mois
		$when = '';
		if ($a['debut'] || $a['fin'])
		{
			$when = self::_createInterval($a['debut'], $a['fin']);
			$token[] = $when;
		}
		if (!empty($a['jour_mois_debut']) || !empty($a['jour_mois_fin'])) {
			if ($when) $when .=', ';
			$jour_mois_debut = ($a['jour_mois_debut'] == 1 ? '1er' : $a['jour_mois_debut']);
			if (!empty($a['jour_mois_debut']) && !empty($a['jour_mois_fin'])) {
				$when .= sprintf('entre le %s et le %s du mois', $jour_mois_debut, $a['jour_mois_fin']);
			} else if (!empty($a['jour_mois_debut'])) {
				$when .= sprintf('apr�s le %s du mois', $jour_mois_debut);
			} else if (!empty($a['jour_mois_fin'])) {
				$when .= sprintf('avant le %s du mois', $a['jour_mois_fin']);
			}
			$when .=', ';
		}

		// conditions (jours avant, depuis, jusqua, jours_min, jours_max)
		$validite = '';
		if ($a['jours_avant'])
			$validite = (int) $a['jours_avant'].' jours avant le d�part';
		if ($a['depuis'] || $a['jusqua'])
		{
			if ($validite) $validite .= ', ';
			$validite .= self::_createInterval($a['depuis'], $a['jusqua']);
		}
		if ($validite) 
			$validite = ', valable pour une r�servation pay�e '.$validite;
		// puis avec les jours_min et jours_mx
		$jours_min = $a['jours_min']; $jours_max = $a['jours_max'];
		if ($jours_max || $jours_min)
		{
			// adapter le texte de validit�
			if ($validite)
				$validite .= " et d'une dur�e ";
			else
				$validite = ", valable pour une r�servation d'une dur�e ";
			// ajouter le crit�re e dur�e
			if ($jours_min && $jours_max)
			{
				if ($jours_min == $jours_max)
				{
					if ($jours_min==1)
						$validite .= "d'une journ�e";
					else
						$validite .= "de $jours_min jours";
				}
				else
					$validite .= "de $jours_min � $jours_max jours";
			}
			else if ($jours_max)
			{
				if ($jours_max == 1)
					$validite .= "d'une journ�e";
				else
					$validite .= "de moins de $jours_max jours";
			}
			else if ($jours_min)
			{
				$validite .= "d'au moins $jours_min jours";
			}
		}
		// kilom�trage : km_min et km_mx
		$km_min = $a['km_min']; $km_max = $a['km_max'];
		if ($km_max || $km_min)
		{
			// adapter le texte de validit�
			if ($validite)
				$validite .= " et pour une distance ";
			else
				$validite = ", valable pour une r�servation d'une distance ";
			// ajouter le crit�re km
			if ($km_min && $km_max) {
				if ($km_min == $km_max) {
					$validite .= "de $km_min km";
				} else {
					$validite .= "entre $km_min et $km_max km";
				}
			} else if ($km_max) {
					$validite .= "de moins de $km_max km";
			} else if ($km_min) {
				$validite .= "d'au moins $km_min km";
			}
		}
		// puis avec age_min et age_mx
		$age_min = $a['age_min']; $age_max = $a['age_max'];
		if ($age_max || $age_min)
		{
			// adapter le texte de validit�
			if ($validite)
				$validite .= " et un conducteur ";
			else
				$validite = ", valable pour un conducteur ";
			// ajouter le crit�re �ge
			if ($age_min && $age_max)
			{
				if ($age_min == $age_max)
				{
					$validite .= "de $age_min ans";
					$token[] = $age_min;
				}
				else
				{
					$validite .= "entre $age_min et $age_max ans";
					$token[] = $age_min.'-'.$age_max;
				}
			}
			else if ($age_max)
			{
				$validite .= "de moins de $age_max ans";
				$token[] = '-'.$age_max;
			}
			else if ($age_min)
			{
				$validite .= "d'au moins $age_min ans";
				$token[] = '+'.$age_max;
			}
		}
		
		// constituer les donn�es
		$result = array();
		$result['nom'] = join(' ', $token);
		$result['slogan'] = "Promo $forSlogan $periode $when $whereSlogan";
		$result['slogan_iphone'] = "$what $forSlogan";
		if ($when) $when = ', d�part '.$when;
		if ($for) $for = 'de '.$for;
		$result['description'] = ($a['montant'] > 0 ? "Augmentation de " : "B�n�ficiez d'une r�duction de ").substr($what, 1)." sur toutes vos locations $for $periode $when $where$validite.";
		$result['mentions'] = "Offre valable uniquement $zoneText pour les locations effectu�es sur le site internet, dans la limite des stocks disponibles. Offre non cumulable. Offre hors ADA Premier Prix.";
		if ($a['classe'])
			$result['mentions'] .= " Offre r�serv�e aux titutlaires d'un code de r�duction et pouvant apporter les pi�ces justificatives si elles sont demand�es.";
			
		// retour
		return $result;
	}
	
}

?>
