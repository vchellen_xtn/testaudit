<?
	include ("secure.php");
	include ("lib.php");

	$section = $_GET['section'];
	if (!preg_match('/^[a-z]+$/', $section))
		$section = 'resa';
	
	// WriteButton
	function WriteButton($title, $url, $size, $last = false)
	{
		echo ("<input value=\"".$title."\" onclick=\"return goMain('".$url."',".$size.");\" type=\"button\" class=\"bouton\">");
		if (!$last) echo "\n";
	}
	function WriteButtonMain($title, $url, $last = false)
	{
		echo ("<input value=\"".$title."\" onclick=\"return go('".$url."');\" type=\"button\" class=\"bouton\">");
		if (!$last) echo "\n";
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css"/>
<script type="text/javascript">
// <![CDATA[ 

// goMain
function goMain(url, rows)
{
	go("main.php?rows=" + rows + "&url=" + escape(url));
	return false;
}
// go
function go(url)
{
	if (window.parent && window.parent.main) 
		window.parent.main.location.href = url;
	return false;
}
/* adapter le formulaire de recherche en fonction du contenu*/
function doSearch(frm)
{
	// r�cup�rer le contr�le
	var ctrl = frm.elements[0];
	var val = ctrl.value;
	// si c'est une r�servation
	if (val.match(/^\d+$/))
	{
		frm.method = "GET";
		frm.action = "reservation.php";
		ctrl.name = 'id';
		return true;
	}
	else if (val.length && !val.match(/^\s+$/))
	{
		frm.method = "POST";
		frm.action = "reservation_recherche.php";
		if (val.match(/^(FR|C|FRA)\d+[A-Z]$/i))
			ctrl.name = "pdv";
		else
			ctrl.name = "client";
		return true;
	}
	else
	{
		ctrl.value = "";
		ctrl.focus();
	}
	return false;
}
// ]]>
</script>
<style type="text/css">
	body{ background:url(img/menu_bg.gif) repeat-x 0 0; }
	td input{ display:block; margin:0 0 10px; min-width:110px; }
</style>
</head>
<body leftmargin="0" topmargin="0" marginheight="0" marginwidth="0">

<table width="100%" height="100%" cellpadding="0" cellspacing="0">
<tr>
	<td align="center" valign="top">
<?
// URLs � afficher pour ce menu
$goURL = "go('home.php')";
// R�servations et Clients
if ($section=='resa' && ($adminUser->hasRights(Acces::SB_GESTION) || $adminUser->hasRights(Acces::SB_SATISFACTION) || $adminUser->hasRights(Acces::SB_OPERATEUR)))
{
	if ($adminUser->hasRights(Acces::SB_GESTION) || $adminUser->hasRights(Acces::SB_OPERATEUR)) 
	{
		$goURL = "go('reservation_recherche.php')";
		WriteButtonMain ("R�servations", "reservation_recherche.php");
		echo '<form method="GET" action="reservation.php" target="main" onsubmit="return doSearch(this);">';
		echo '<input type="text" id="search" name="id" value="" size="8" maxlength="64"/>';
		echo '<input type="hidden" name="date" value="paiement"/>';
		echo '</form>';
	}
	if ($adminUser->hasRights(Acces::SB_ADMIN))
	{
		WriteButtonMain ("Stats", "stats_reservation.php");
		WriteButtonMain ("Facebook", "stats_facebook.php");
	}
	if ($adminUser->hasRights(Acces::SB_GESTION)) 
	{
		echo '<br/>';
		WriteButton ("Clients", "client.php", 400);
		WriteButton ("Prospects", "prospect.php", 260);
	}
	if ($adminUser->hasRights(Acces::SB_SATISFACTION) || $adminUser->hasRights(Acces::SB_ADMIN)) {
		echo '<br/>';
		WriteButtonMain ("Satisfactions", 'stats_questionnaire.php');
		if (!$adminUser->hasRights(Acces::SB_GESTION)) {
			$goURL = "go('stats_questionnaire.php')";
		}
	}
	if ($adminUser->hasRights(Acces::SB_ADMIN))
	{
		echo '<br/>';
		WriteButtonMain ("Exportations", "stats_export.php");
		
		// RESAS : outil d'aide � la vente
		echo '<br/><hr/>';
		echo '<br/><br/><a target="_blank" href="../resas/"><strong>Acc�s Resas</strong></a><br/>'."\n";
		WriteButtonMain ('Resas : Recherche', 'resas_recherche.php');
		WriteButtonMain ('Resas : Clients', 'resas_clients.php');
		WriteButtonMain ('Resas : Config.', 'resas_config.php');
		WriteButtonMain ('Resas : Yield', 'resas_yield.php');
		echo '<br/>',
		WriteButtonMain ('Resas : Bons Plans', 'resas_bonsplans.php');
		echo '<br/>',
		WriteButtonMain ('Resas : Unipro', 'resas_unipro.php');
	}
}
// Gestion et Offres
if ($section=='gestion' && $adminUser->getCurrentZoneID() && $adminUser->hasRights(Acces::SB_OFFRES))
{
	echo "<strong>".$adminUser->getCurrentZone()->getData('nom')."</strong><br /><br />";
	if ($adminUser->hasRights(Acces::SB_GESTION))
	{
		$goURL = "goMain('agence.php', 380)";
		
		// s�parer les agences ouvertes et ferm�es
		WriteButton ('Agences ouvertes', 'agence.php', 380);
		WriteButton ('Agences ferm�es', 'agence_fermee.php', 380);
	}
	// s'il y a des droits � g�rer les offres il faut pr�ciser la zone
	if ($adminUser->hasRights(Acces::SB_GESTION) || $adminUser->hasRights(Acces::SB_OFFRES))
	{
		WriteButton ("Fermetures", "fermeture.php?agence=%", 140);
		WriteButton ("Exceptions", "ouverture.php?agence=%", 140);
		echo "<br/>\n";
		
		WriteButton ("Stop Sell", "stopsell.php", 360);
		//WriteButton ("Promotions", "promotion_forfait.php", 480);
		WriteButtonMain ("Promo : cr��r", 'agence_promo_forfait.php');
	//	WriteButtonMain ("Promo : liste", "lists/promotion_forfait.php");
		WriteButtonMain ("Promo : recherche", 'promotion_recherche.php');
		WriteButton ("Promo : classe", "partenaire_classe.php", 420);
		echo "<br/>\n";
		if ($adminUser->hasRights(Acces::SB_ADMIN)) {
			WriteButton ("N�gociations", "promotion_negociation.php", 420);
			echo "<br/>\n";
		}
	}
	
	if ($adminUser->hasRights(Acces::SB_GESTION))
	{
		WriteButton ("Familles", "vehicule_famille.php", 270);
		WriteButton ("Cat�gories", "categorie.php", 490);
		if ($adminUser->hasRights(Acces::SB_ADMIN))
			WriteButtonMain ("Cat�gories Jeunes", "categorie_jeune.php");
		WriteButton ("Forfaits", "forfait.php", 340);
		if ($adminUser->hasRights(Acces::SB_ADMIN))
		{
			WriteButton ("Forfaits Contraintes", "forfait_univers.php", 380);
			WriteButton ("Options : Th�mes", "option_theme.php", 400);
		}
		WriteButton ("Options : D�finitions", "option.php", 400);
		WriteButtonMain ("Options : Tarifs", "lists/option_tarif.php");
		echo "<br/>\n";
	}
	if ($adminUser->hasRights(Acces::SB_GESTION) || $adminUser->hasRights(Acces::SB_OFFRES))
	{
		WriteButtonMain('Tarifs Forfaits', 'tarification_forfait.php');
	}
	if ($adminUser->hasRights(Acces::SB_ADMIN))
	{
		echo "<br/><br/>\n";
		WriteButton ("Motifs Annulation", "res_annulation_motif.php", 400);
		WriteButtonMain('Franchise', 'categorie_franchise.php');
	}
}
// Compte Pro
if ($section=='pro' && $adminUser->hasRights(Acces::SB_ADMIN)) 
{
	$goURL = "go('stats_partenaire.php')";

	WriteButtonMain('Stats', 'stats_partenaire.php');
	echo "<br/>\n";
	
	WriteButtonMain ("Partenaires : cr��r", "partenaire.php");
	WriteButtonMain ("Partenaires : liste", "lists/partenaire.php");
	WriteButtonMain ("Conducteurs : liste", "lists/partenaire_conducteur.php");
	echo "<br/>\n";
	WriteButtonMain ("Code R�duction : liste", "lists/partenaire_coupon.php");
	WriteButtonMain ("R�glements : liste", "lists/partenaire_reglement.php");
}
// Code Grillables
if ($section=='campagne' && $adminUser->hasRights(Acces::SB_ADMIN)) 
{
	$goURL = "go('stats_campagne.php')";

	WriteButtonMain('Stats', 'stats_campagne.php');
	echo "<br/>\n";
	
	WriteButtonMain ("Campagnes : cr��r", "promotion_campagne.php");
	WriteButtonMain ("Campagnes : liste", "lists/promotion_campagne.php");
	echo "<br/>\n";
	WriteButtonMain ("Code Grillable : liste", "lists/promotion_code.php");
	echo "<br/>\n";
	WriteButtonMain ("Service client", "promotion_serviceclient.php");
}
// Extractions comptables
if ($section=='compta' && $adminUser->hasRights(Acces::SB_ADMIN))
{
	$goURL = "goMain('extraction.php', 180)";
	WriteButton ("Extraction", "extraction.php", 180);
}
// Visuels, Banni�res
if ($section=='pub' && $adminUser->hasRights(Acces::SB_ADMIN))
{
	WriteButton ("Visuels", "visuel.php", 400);
	WriteButton ("Pages", "visuel_page.php", 400, true);
	WriteButton ("Visuels Promos", "visuel_promo.php", 250, true);
	WriteButton ("Visuels Moy. Dur�e", "visuel_moyenne_duree.php", 280, true);
}
// G�ogrpahie
if ($section=='geo' && $adminUser->hasRights(Acces::SB_ADMIN))
{
	$goURL = "goMain('zone.php', 180)";
	WriteButton ("Zones", "zone.php", 180);
	echo "<br/>\n";
	WriteButton ("Pays", "pays.php", 140);
	WriteButton ("D�partements", "dept.php", 140);
	WriteButton ("Agglo.", "agence_agglo.php", 200);
	WriteButton ("Villes", "ville.php", 380);
//	WriteButtonMain("Coord. Agence", 'agencemap.php')
}
if ($section=='seo' && $adminUser->hasRights(Acces::SB_EDITO))
{
	$goURL = "goMain('page.php', 270)";
	
	WriteButton ("Page", "page.php", 270);
	WriteButton ("Maps &amp; URLs", "url.php", 140);
	
	echo '<br/>';
	WriteButtonMain("Textes", 'stats_refnat.php');

}
// Utilisateurs et Droits
if ($section=='users' && $adminUser->hasRights(Acces::SB_ADMIN)) 
{
	$goURL = "goMain('acces.php', 270)";
	
	WriteButton ("Acc�s", "acces.php", 270);
	WriteButton ("Code origine", "origine.php", 270);
	WriteButton ("API : acc�s", "api_access.php", 270);
}
?>
	</td>
</tr>
</table>
<script type="text/javascript">
<?=$goURL;?>
</script>
</body>
</html>