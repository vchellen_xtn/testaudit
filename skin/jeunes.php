<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?
        $this->setData('titre', 'Location voiture pour jeune conducteur et �tudiants : ADA Jeunes');
    	$this->writeHead();
    ?>
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '216556905383141');
    fbq('track', "PageView");
    fbq('track', 'ViewContent');</script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=216556905383141&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->
</head>
<body class="<?=dirname($this->getId()).' '.basename($this->getId());?>">
	<div id="fb-root"></div>
    <div class="Slideout">
        <ul class="Slideout__list">
            <li class="Slideout__item">
                <a href="<?=Page::getURL('newsletter/index.html', null, USE_HTTPS)?>" class="Header__link" target="_blank">
                    <img src="<?=$baseurl?>css/jeunes-v2/letter-v2.png" alt="" class="Header__img">
                    <span class="Header__text">
                        Newsletter
                    </span>
                </a>
            </li>
            <li class="Slideout__item">
                <a href="https://www.facebook.com/ADALocationdevehicules" class="Header__link" target="_blank">
                    <img src="<?=$baseurl?>css/jeunes-v2/facebook-v2.png" alt="" class="Header__img">
                    <span class="Header__text">Je like</span>
                </a>
            </li>
            <li class="Slideout__item">
                <a href="<?=Page::getURL('');?>" class="Header__link" target="_blank">
                    <img src="<?=$baseurl?>css/jeunes-v2/at-v2.png" alt="" class="Header__img">
                    <span class="Header__text">ADA.FR</span>
                </a>
            </li>
            <li class="Slideout__item">
                <a href="<?=Page::getURL('client/index.html', SERVER_MOBI, USE_HTTPS)?>" class="Header__link Header__link--is-button"  target="_blank">
                    <img src="<?=$baseurl?>css/jeunes-v2/user-v2.png" alt="" class="Header__img">
                    <span class="Header__text">Espace client</span>
                </a>
            </li>
        </ul>
    </div>
    <div class="Header">
        <div class="container">
            <div class="row">
                <div class="col-xs-2 Header__logo-column">
                    <img src="<?=$baseurl?>css/jeunes-v2/logo-v2.png" alt="" class="Header__logo">
                </div>
                <div class="col-xs-10 Header__burger hidden-sm hidden-md hidden-lg">
                    <img src="<?=$baseurl?>css/jeunes-v2/burger.png" alt="" class="Header__burger-img">
                    <img src="<?=$baseurl?>css/jeunes-v2/close.png" alt="" class="Header__burger-close">
                </div>
                <div class="col-xs-3 hidden-xs Header__newsletter-column">
                    <a href="<?=Page::getURL('newsletter/index.html', null, USE_HTTPS)?>" class="Header__link" target="_blank">
                        <img src="<?=$baseurl?>css/jeunes-v2/letter-v2.png" alt="" class="Header__img">
                        <span class="Header__text">
                            <span class="hidden-sm hidden-md">Je m'inscris � la </span>newsletter
                        </span>
                    </a>
                </div>
                <div class="col-xs-2 hidden-xs Header__facebook-column">
                    <a href="https://www.facebook.com/ADALocationdevehicules" class="Header__link" target="_blank">
                        <img src="<?=$baseurl?>css/jeunes-v2/facebook-v2.png" alt="" class="Header__img">
                        <span class="Header__text">Je like</span>
                    </a>
                </div>
                <div class="col-xs-2 hidden-xs Header__ada-column">
                    <a href="<?=Page::getURL('');?>" class="Header__link" target="_blank">
                        <img src="<?=$baseurl?>css/jeunes-v2/at-v2.png" alt="" class="Header__img">
                        <span class="Header__text">ADA.FR</span>
                    </a>
                </div>
                <div class="col-xs-2 hidden-xs Header__account-column">
                    <a href="#" class="Header__link Header__link--is-button Header__login">
                        <img src="<?=$baseurl?>css/jeunes-v2/user-v2.png" alt="" class="Header__img">
                        <span class="Header__text">Espace client</span>
                    </a>
                    <div class="Login">
                        <? $client = $this->getConnectedClient(); ?>
                        <div id="Login__connected" <? if (!$client) echo ' style="display: none;"';?>>
                            <div class="Login__left-connected">
                                Bienvenue <span id="Login__name"><?=$client['prenom'];?></span>
                                <a rel="noindex,nofollow" href="<?=Page::getURL('?logout');?>" class="Login__logout">D�connexion</a>
                            </div>
                            <div class="Login__right-connected">
                                <a rel="noindex,nofollow" href="<?=Page::getURL('client/index.html', null, USE_HTTPS);?>" target="_blank" class="Login__account">Mon compte</a>
                            </div>
                        </div>
                        <? if (!$client) :?>
                        <form action="<?=Page::getURL('client/index.html', null, USE_HTTPS);?>" method="post" id="Login__form">
                            <input type="hidden" name="id" value="0" id="Login__id" />
                            <input type="hidden" name="key" value="<?=Page::getKey(0)?>" id="Login__key"/>
                            <input type="email" placeholder="e-mail" id="Login__email" name="email" />
                            <input type="password" placeholder="mot de passe" id="Login__mdp" name="pwd" />
                            <button type="submit" class="Login__submit">OK</button>
                            <a href="<?=Page::getURL('client/motdepasse.html', null, USE_HTTPS)?>" class="Login__password" target="_blank">Mot de passe oubli� ?</a>
                        </form>
                        <? endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

	<?
		// maintenance et t�l�-conseiller
		$this->writeInfo();
		// inclure la page courante
		include($this->getSkinnedPagePath());
	?>

    <div class="Footer-sup hidden-xs">
        <div class="container">
            <div class="row">
                <div class="col-xs-2">
					<a href="http://www.groupeg7.com" target="_blank">
                        <img src="<?=$baseurl?>css/img/structure-2014/logo-g7.png" alt="" class="img-responsive" />
                    </a>
                </div>
                <div class="col-xs-10">
                    <div class="row">
                        <div class="col-xs-2 Footer-sup__link">
                            <a href="http://www.groupeg7.com" target="_blank">Ada est une soci�t� du Groupe G7</a>
                        </div>
                        <div class="col-xs-2 Footer-sup__link">
        					<a href="<?=$here?>societe/index.html" title="Qui sommes nous ?" target="_blank">Qui sommes-nous ?</a>
                        </div>
                        <div class="col-xs-2 Footer-sup__link">
        					<a href="http://franchise.ada.fr/" target="_blank" title="Devenir franchis�" target="_blank">Devenir franchis�</a>
                        </div>
                        <div class="col-xs-2 Footer-sup__link">
        					<a href="<?=$here?>contact/index.html" title="Nous contacter" target="_blank">Nous contacter</a>
                        </div>
                        <div class="col-xs-2 Footer-sup__link">
        					<a href="<?=$here?>conditions/location.html" title="Conditions g�n�rales" target="_blank">Conditions g�n�rales</a>
                        </div>
                        <div class="col-xs-2 Footer-sup__link">
        					<a href="<?=$here?>conditions/assurance_complementaire.html" title="Conditions assurance compl�mentaire" target="_blank">Conditions assurance compl�mentaire</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="Footer-sub hidden-xs">
        <div class="container">
            <div class="row">
                <div class="col-xs-3">
                    <ul>
                        <li class="Footer-sub__title">
                            <a href="<?=$here?>">Retour sur ada.fr</a>
                        </li>
                        <li class="Footer-sub__title">
                            <a href="<?=$here?>agences/index.html" target="_blank">Agences de location de voiture</a>
                        </li>
                        <? if (false): ?>
                        <li class="Footer-sub__title">
                            <a href="<?=$here?>location-voiture-promo.html" target="_blank">Promotions location voiture</a>
                        </li>
                        <? endif; ?>
                        <li class="Footer-sub__title">
                            <a href="<?=$here?>jeunes/index.html" target="_blank">Location jeune conducteur</a>
                        </li>
                        <li class="Footer-sub__title">
                            <a href="<?=$here?>webzine/les-plus-belles-plages-de-france-mediterranee.html" target="_blank">Notre webzine</a>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-2">
                    <ul>
                        <li class="Footer-sub__title">Loisirs</li>
						<li class="Footer-sub__sub-title">
                            <a href="<?=$here?>vehicules/tourisme.html" target="_blank">Location de voiture de tourisme</a>
                        </li>
						<li class="Footer-sub__sub-title">
                            <a href="<?=$here?>offre/forfaits.html" target="_blank">Forfaits</a>
                        </li>
						<li class="Footer-sub__sub-title">
                            <a href="<?=$here?>vehicules/sans_permis.html" target="_blank">Location voiture sans permis</a>
                        </li>
						<li class="Footer-sub__sub-title">
                            <a href="<?=$here?>vehicules/deux_roues.html" target="_blank">Location deux roues</a>
                        </li>
					</ul>
                </div>
                <div class="col-xs-3">
    				<ul>
                        <li class="Footer-sub__title">
                            <a href="<?=$here?>demenagement/index.html" target="_blank">D�m�nagement</a>
                        </li>
    					<li class="Footer-sub__sub-title">
                            <a href="<?=$here?>demenagement/trouver-son-camion.html" target="_blank">Choisir son camion de d�m�nagement</a>
                        </li>
    					<li class="Footer-sub__sub-title">
                            <a href="<?=$here?>demenagement/materiel.html" target="_blank">Pr�voir son mat�riel de d�m�nagement</a>
                        </li>
    					<li class="Footer-sub__sub-title">
                            <a href="<?=$here?>demenagement/memo.html" target="_blank">Organiser son d�m�nagement</a>
                        </li>
    					<li class="Footer-sub__sub-title">
                            <a href="<?=$here?>demenagement/homebox.html" target="_blank">Louer un garde meuble</a>
                        </li>
    				</ul>
                </div>
                <div class="col-xs-2">
                    <ul>
						<li class="Footer-sub__title">Professionnels</li>
						<li class="Footer-sub__sub-title">
                            <a href="<?=$here?>pro/ada_ce.html" target="_blank">Comit�s d'Entreprises</a>
                        </li>
						<li class="Footer-sub__sub-title">
                            <a href="<?=$here?>pro/index.html" target="_blank">Entreprises</a>
                        </li>
                    </ul>
                    <ul>
						<li class="Footer-sub__title">Utilitaires</li>
						<li class="Footer-sub__sub-title">
                            <a href="<?=$here?>vehicules/utilitaires.html" rel="noindex, nofollow" target="_blank">V�hicules utilitaires</a>
                        </li>
						<li class="Footer-sub__sub-title">
                            <a href="<?=$here?>offre/forfaits.html" target="_blank">Forfaits</a>
                        </li>
					</ul>
                </div>
                <div class="col-xs-2">
                    <ul>
						<li class="Footer-sub__title">Vos services</li>
						<li class="Footer-sub__sub-title">
                            <a href="<?=$here?>aide/index.html" target="_blank">Aide en ligne</a>
                        </li>
						<li class="Footer-sub__sub-title">
                            <a href="<?=Page::getURL('client/index.html', null, USE_HTTPS);?>" target="_blank">Mon espace client</a>
                        </li>
						<li class="Footer-sub__sub-title">
                            <a href="<?=$here?>newsletter/index.html" target="_blank">Newsletter</a>
                        </li>
						<li class="Footer-sub__sub-title">
                            <a href="<?=$here?>faq/index.html" target="_blank">Questions fr�quentes</a>
                        </li>
						<li class="Footer-sub__sub-title">
                            <a href="<?=$here?>societe/mentions_legales.html" target="_blank">Mentions l�gales</a>
                        </li>
						<li class="Footer-sub__sub-title">
                            <a href="<?=$here?>juridique/regles_communiques.html" target="_blank">Informations r�glement�es</a>
                        </li>
					</ul>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-6 Footer-sub__payment">
                    Moyens de paiements accept�s <br>
					<img src="<?=$baseurl?>css/img/structure-2014/mode-paiement.png" alt="" />
                </div>
                <div class="col-xs-6 Footer-sub__tel">
                    <img src="../skin/v3/img/pave-audiotel.png" alt="" />
                </div>
            </div>
        </div>
    </div>

    <? if(! $this->isNoIndex() && ($this->hasData('footer') || $this->hasData('baseline'))): ?>
        <div class="Baseline hidden-xs">
            <div class="container">
                <? if($this->hasData('footer')): ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <ul>
                                <?
                    				if ($baseline = $this->getData('footer')) {
                    					$baseline = preg_replace("/href=\"(?!http)/" , 'href="'.$here, $baseline);
                    					echo '<li>'.join("</li>\n<li>", explode("\n", $baseline))."</li>\n";
                    				}
                        		?>
            				</ul>
                        </div>
                    </div>
                <? endif; ?>

                <? if($this->hasData('baseline')): ?>
                    <div class="row Baseline__content">
                        <div class="col-xs-12">
                            <?
                                echo $this->getData('baseline');
                            ?>
                        </div>
                    </div>
                <? endif; ?>
            </div>
        </div>
    <? endif; ?>

    <div class="Footer-mobile hidden-sm hidden-md hidden-lg">
        <div class="container">
            <div class="row">
                <div class="col-xs-9">
					<ul class="Footer-mobile__list">
						<li>
                            <a href="<?=$here?>conditions/location.html" title="Conditions g�n�rales" target="_blank">Conditions g�n�rales de vente</a>
                        </li>
						<li>
                            <a href="<?=$here?>juridique/regles_communiques.html" target="_blank">Informations r�glement�es</a>
                        </li>
						<li>
                            <a href="http://www.ada.fr" target="_blank">Acc�s au site ada.fr</a>
                        </li>
					</ul>
				</div>
				<div class="col-xs-3 Footer-mobile__fb">
					<a href="https://www.facebook.com/ADALocationdevehicules" target="_blank">
                        <img src="<?=$baseurl?>css/img/structure-mobile/btn-fb.png" alt="Facebook" />
                    </a>
				</div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function validateEmail($email) {
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            return emailReg.test( $email );
        }


        $(window).ready(function() {
            $('.Header__burger-img').click(function() {
                $('.Slideout').animate({
                    right : 0
                });

                $('.Header__burger-img').hide();
                $('.Header__burger-close').show();
            });

            $('.Header__burger-close').click(function() {
                $('.Slideout').animate({
                    right : -205
                });

                $('.Header__burger-close').hide();
                $('.Header__burger-img').show();
            });

            $('.Header__login').click(function(e) {
                e.preventDefault();
                $('.Login').is(":visible") ? $('.Login').fadeOut() : $('.Login').fadeIn();
            });

            $("#Login__form").submit(function(e) {
                e.preventDefault();
				var ok = true;
				if (! $('#Login__email').val().length || ! validateEmail($('#Login__email').val())) {
					$('#Login__email').addClass("error");
					ok = false;
				} else {
					$('#Login__email').removeClass("error");
				}
				if (! $('#Login__mdp').val().length) {
					$('#Login__mdp').addClass("error");
					ok = false;
				} else {
					$('#Login__mdp').removeClass("error");
				}
				if (!ok) return false;

                var frm = this;
				var url = this.action;
                var data = {
                    id: $('#Login__id').val(),
                    key: $('#Login__key').val(),
                    email: $('#Login__email').val(),
                    pwd: $('#Login__mdp').val()
                }
				$.post(url.replace('client/index.html', 'login.php?action=login').replace(/^https?\:/, window.location.protocol), data, function (xml) {
					if (xml) {
						$xml = $(xml);
						if ($xml.find("error").length) {
							alert($xml.find("error").text());
						} else {
							$('#Login__name').text($xml.find('prenom').text());
                            $('#Login__form').hide();
							$('#Login__connected').show();
						}
					}
				});
				return false;

			});
        });
    </script>

<?
	// informations de mise au point
	//$this->writeDebugInfo();
	$this->writeJSStats();
?>
</body>
</html>
