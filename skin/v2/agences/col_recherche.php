<div class="recherche-agence">
	<h2>Nos agences de location de voiture<br />en France</h2>
	
	<p>
		<strong>Recherchez les agences les plus proches<br />d'une ville ou d'une adresse</strong><br /><br />
		<em class="gris">ville, cp, adresse ou d�partement</em>
	</p>
	<form action="../agences/liste.html" method="POST" id="agence" onsubmit="return doCheck(this);">
		<div id="autocomplete">
			<input type="text" id="ville" name="ville" value=""/>
		</div>
		<input type="hidden" id="cp" name="cp"/>
		<p class="valid">
			<input type="submit" id="validez" title="lancez la recherche" />
		</p>
		<br clear="all" />
	</form>
	<? if ($page == 'agences/index') : ?>
	<div class="autres-destinations-internationales">
		<h2><img src="../css/img/titre-reservation-internat.jpg" alt="Nos autres destinations" /></h2>
		<ul class="link-international first">
			<li><a href="http://www.autoeurope.fr/ada/?pucode=DE" target="_blank" rel="noindex,nofollow">Allemagne</a></li>
			<li><a href="<?=$here?>location-voiture-belgique.html">Belgique</a></li>
			<li><a href="http://www.autoeurope.fr/ada/?pucode=BR" target="_blank" rel="noindex,nofollow">Br�sil</a></li>
			<li><a href="http://www.autoeurope.fr/ada/?pucode=CA" target="_blank" rel="noindex,nofollow">Canada</a></li>
			<li><a href="http://www.autoeurope.fr/ada/?pucode=US" target="_blank" rel="noindex,nofollow">Etats Unis</a></li>
			<li><a href="http://www.autoeurope.fr/ada/?pucode=GR" target="_blank" rel="noindex,nofollow">Gr�ce</a></li>
			<li><a href="<?=$here?>location-voiture-guadeloupe.html">Guadeloupe</a></li>
			<li><a href="<?=$here?>location-voiture-guyane.html">Guyane</a></li>
			<li><a href="<?=$here?>location-voiture-ile-maurice.html" rel="noindex,nofollow">Ile Maurice</a></li>
			<li><a href="http://www.autoeurope.fr/ada/?pucode=IE" target="_blank" rel="noindex,nofollow">Irlande</a></li>
		</ul>
		<ul class="link-international">
			<li><a href="http://www.autoeurope.fr/ada/?pucode=PT" target="_blank" rel="noindex,nofollow">Italie</a></li>
			<li><a href="<?=$here?>location-voiture-luxembourg.html">Luxembourg</a></li>
			<li><a href="<?=$here?>location-voiture-maroc.html">Maroc</a></li>
			<li><a href="<?=$here?>location-voiture-martinique.html">Martinique</a></li>
			<li><a href="<?=$here?>location-voiture-nouvelle-caledonie.html">Nouvelle Caledonie</a></li>
			<li><a href="http://www.autoeurope.fr/ada/?pucode=PT" target="_blank" rel="noindex,nofollow">Portugal</a></li>
			<li><a href="<?=$here?>location-voiture-republique-dominicaine.html">R�publique Dominicaine</a></li>
			<li><a href="<?=$here?>location-voiture-la-reunion.html">R�union</a></li>
			<li><a href="http://www.autoeurope.fr/ada/?pucode=UK" target="_blank" rel="noindex,nofollow">Royaume Uni</a></li>
			<li><a href="<?=$here?>location-voiture-senegal.html">S�n�gal</a></li>
		</ul>
	</div>
	<a href="http://www.autoeurope.fr/ada/" target="_blank" class="btn right">Nos autres destinations<span class="end"></span></a>
	<? endif; ?>
</div>
<script type="text/javascript" language="javascript">
//<![CDATA[ 
	// doCheck
	function doCheck(frm) 
	{
		return ((frm.cp.value.length > 0) || (frm.ville.value.length > 0));
	}
	// onload
	$(document).ready(function()
	{
		// recherche sur la ville
		$('#autocomplete').tagdragon({
			'field': 'ville',
			'url': '../json.ville.php',
			'max': 30,
			'charMin': 3,
			'delay': 100,
			'onSelectedItem': function(val) { $('#cp').attr('value', val.cp); }
		});
		$('#ville').focus();
		// toggles des d�partements
		$('#menu_dept a.toggle').each(function(i)
		{
			if ( i == 0 )
			{
				$(this.hash).show();
				$(this).addClass("current")
			} else {
				$(this.hash).hide();
			}
		}).click(function()
		{
			$('#menu_dept a.toggle').removeClass('current');
			$(this).addClass("current");
			$(this.hash).show().siblings('.dept').hide();
			return false;
		});
		//toggle destinations
		$('.titre-autres-destinations').click(function()
		{
			$('.autres-destinations').addClass('current');
			$('.titre-autres-destinations').addClass('current');
			$('.departements').removeClass('current');
			$('.titre-departements').removeClass('current');
			return false;
		});
		$('.titre-departements').click(function()
		{
			$('.departements').addClass('current');
			$('.titre-departements').addClass('current');
			$('.autres-destinations').removeClass('current');
			$('.titre-autres-destinations').removeClass('current');
			return false;
		});
	});
// ]]>
</script>