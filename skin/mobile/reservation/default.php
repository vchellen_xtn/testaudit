<div class="row booking-progression">
	<!--div class="univers_titre"><!--?=$this->getUniversTitre();?></div-->
	<? if ($navResa = $this->getNavResa(true)) : ?>
	<? 
		$i = 1; 
		$action = $this->getAction(); 
		$navHrefs = array(
			'vehicule' => array(
				'index' => '../',
			),
			'resultat' => array(
				'index' => '../',
				'vehicule' => 'vehicule.html',
			),
			'options' => array(
				'index' => '../',
				'vehicule' => 'vehicule.html',
			),
		);
	?>
	<ol id="nav-resa" class="list--booking-progression">
		<? foreach($navResa as $k => $title) : ?>
		<? $navHref = $navHrefs[$action][$k]; ?>
		<li class="<? if ($k == $action) echo 'current ';?>item-chemin-resa" id="<?php echo "nav".$i ?>">
			<? if ($navHref) : ?><a href="<?=$navHref;?>"><?endif; ?>
			<span class="number"><?=$i++?></span>
			<span class="title"><?=$title?></span>
			<? if ($navHref) : ?></a><?endif; ?>
		</li>
		<? endforeach; ?>
	</ol>
	<? endif; ?>
</div>
<div class="bg">
	<div class="content tpl_reservation">
	<?
		include($this->getPagePath());
	?>
		<div class="clear"></div>
	</div>
</div>