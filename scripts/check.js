<!-- hide from non-scriptable browsers

//
//	Form validation functions
//	by W.P.Dauchy @1996-2000
//

function IsEmpty(theField)
{
	return (theField.value == "" || theField.value == null);
}
// IsEmpty

function ValidateSelect(theMenu) 
{
	if (	theMenu == null ||
		theMenu.selectedIndex == -1 || 
		theMenu.selectedIndex == null || 
		theMenu.options[theMenu.selectedIndex].value == "")
		return false;
	return true;
}
// ValidateSelect

function ValidateRadio(theRadio, cnt) 
{
	if (cnt == null)
		cnt = (theRadio.length ? theRadio.length : 1)
	if (cnt == 1)
		return (theRadio.checked);	
	for (i=0; i<cnt; i++)
		if (theRadio[i].checked) return true;
	return false;
}
// ValidateRadio

function ValidateInteger(theField) 
{
	return TestChars (theField.value, "0123456789");
}
// ValidateInteger

function ValidateNum(theField) 
{
	return TestChars (theField.value, "0123456789,."+String.fromCharCode(160));
}
// ValidateInteger

function StringOfDigits(theString)
{
	return TestChars (theString, "0123456789");
} 
// StringOfDigits

function TestChars (theString, theChars)
{
	if (theString.length == 0)
		return false;	// blank entry is not a string of digits
	
	for(var i=0; i<theString.length; i++)	
	{
		var	thisChar = theString.charAt(i);	// this line necessary because of bug (?) (Mac only?)
		if (theChars.indexOf(thisChar) == -1)
			return false;
	}
	return true;
}

function ValidatePhone(theField)
{
	return theField.value.match(/^[0-9 \.]{10,14}$/);
}
// ValidatePhone

function ValidateEmail(what)
{
	var p1, p2, theString = (what.value ? what.value : what);
	var theChars = "@.-_";

	if (theString.length == 0)
		return false;	// blank entry is not a string of digits
	
	if ((p1 = theString.indexOf('@')) == -1)
		return false;

	if ((p2 = theString.lastIndexOf('.')) < p1 || p2 >= theString.length - 2 || p2 < theString.length - 4)
		return false;

	for(var i=0; i<theString.length; i++)	
	{
		var	thisChar = theString.charAt(i);	// this line necessary because of bug (?) (Mac only?)
		if ((thisChar >= 'a' && thisChar <= 'z') ||
			(thisChar >= 'A' && thisChar <= 'Z') ||
			(thisChar >= '0' && thisChar <= '9') ||
			(theChars.indexOf(thisChar) >= 0))
			continue;
		return false;
	}
	return true;
} 
// ValidateEmail

function ValidateDateTime(theElement)
{
	var strValue = theElement.value ? theElement.value : theElement;
	if (!strValue || !strValue.length)
		return false;
		
	dt = strValue.split(" ");
	
	return (ValidateDate(dt[0]) && (dt.length <= 1 || ValidateTime(dt[1])));
}

function ValidateDate(theElement, lg /* fran�ais par d�faut */)
{
	var	month	= 0;
	var	day		= 0;
	var	year	= 0;
	var	offset	= 0;
	var	testStr	= "";

	if (!theElement)
		return false;

	strValue = (theElement.value) ? theElement.value : theElement;
	if (strValue.length == 0)
		return false;	// blank entry is not a date

	// d�coupe en j/m/a ou m/j/a
	var now = new Date(), ard = strValue.split(/[\/\-]/);

	// v�rifier l'ann�e
	if (ard.length < 3)
		year = now.getYear();
	else
	{
		if (!StringOfDigits(ard[2]))
			return false;
		year = parseFloat(ard[2]);
		if (!year)
			return false;
		if (year < 50)
			year += 2000;
		if (year < 100)
			year += 1900;
	}

	// v�rifier le mois
	if (lg == 2)
	{
		month = ard[0];
		if (ard.length < 2)
			day = now.getDate();
		else
		{
			if (!StringOfDigits(ard[1]))
				return false;
			day = parseFloat(ard[1]);
		}
	}
	else
	{
		
		day = ard[0];
		if (ard.length < 2)
			month = 1 + now.getMonth();
		else
		{
			if (!StringOfDigits(ard[1]))
				return false;
			month = parseFloat(ard[1]);
		}
		
	}
		
	// v�rifie le jour
	if (day < 1 || day > 31 || isNaN(parseInt(day)))
		return false;

	// v�rifie le mois
	if (month < 1 || month > 12)
		return false;
	
	// ann�es bissextiles
	if (month == 2)	
	{
		if ((4 * Math.floor(year / 4)) == year)	
		{
			if (day > 29)
				return false;
		} 
		else	
		{
			if (day > 28)
				return false;
		}
	} 
	// mois de 30 jours
	else if ((month == 4) || (month == 6) || (month == 9) || (month == 11))	
	{
		if (day > 30)
			return false;
	}
	
	// reconstitue la valeur
	//theElement.value = ((lg == 2) ? (month + "/" + day) : (day + "/" + month)) + "/" + year.toString().substr(2,2);
	
	return true;
} 
// ValidateDate()

function ValidateTime(theElement)	
{
	// assume [H]H:MM[:SS][ AM|PM] format allow a time-format string as parameter in future
	var	hour	= 0;
	var	minute	= 0;
	var	second	= 0;
	var	testStr	= "";
	
	// vide : c'est une erreur
	strValue = (theElement.value) ? theElement.value : theElement;
	if (!strValue.length)
		return false;	
	
	// d�coupe en hh:mm:ss
	strValue = strValue.replace("h", ":");
	if (!TestChars(strValue, "0123456789:"))
		return false;
	var ard = strValue.split(":");
	if (ard.length < 1)
		return false;
		
	// v�rifie l'heure
	if (parseInt(ard[0]) < 0 || parseInt(ard[0]) > 23)
		return false;
	
	theElement.value = ard[0];
	if (ard.length < 2)
	{
		theElement.value += ":00";
		return true;
	}
	else
	{
		// v�rifie les minutes
		v = parseInt(ard[1]);
		if (v < 0 || v > 59)
			return false;
		if (v < 10)
			v = "0" + v;
		theElement.value += ":" + v;
	}

	if (ard.length >= 3)
	{
		// v�rifie les secondes
		v = parseInt(ard[2]);
		if (v < 0 || v > 59)
			return false;
		if (v < 10)
			v = "0" + v;
		theElement.value += ":" + v;
	}
	return true;
} 
// ValidateTime()

// end hiding -->

