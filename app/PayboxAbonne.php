<?php
class PayboxAbonne extends Ada_Object {
	/**
	* Cr�e un abonnement � partir d'un num�ro de carte
	* 
	* @param Reservation $reservation
	* @param string $libelle
	* @param string $numcarte
	* @param string $valid
	* @param string $cvv
	* @return PayboxAbonne
	*/
	static public function create(Reservation $reservation, $libelle, $numcarte, $valid, $cvv)
	{
		$client = $reservation->getClient();
		$refabonne = date('YmdHis').'ABT'.sprintf('%08d', $client['id']);
		$a = Paybox::paiement($reservation->getResID(), 1, $numcarte, $valid, $cvv, Paybox::TYPE_ABT_INSCRIPTION, null, $refabonne);
		if ($a['CODEREPONSE'] != '00000')
			return null;
		// cr�er l'enregistrement pour l'abonn�
		$data = array
		(
			'id'		=>'',
			'client'	=> $client['id'],
			'libelle'	=> filter_var($libelle, FILTER_SANITIZE_STRING),
			'validite'	=> preg_replace('/^(\d{2})(\d{2})$/','20$2-$1-01', $valid),
			'numcarte'	=> substr($numcarte, 0, 6).preg_replace('/\d/', 'X', substr(trim($numcarte,'X'), 6)),
			'CVV'		=> $cvv,
			'DATEVAL'	=> $valid,
			'PORTEUR'	=> $a['PORTEUR'],
			'TYPECARTE' => $a['TYPECARTE'],
			'PAYS'		=> $a['PAYS'],
			'REFABONNE'	=> $a['REFABONNE']
		);
		$x = new PayboxAbonne($data);
		$x->save();
		return $x;
	}
	
	/**
	* Renvoie les r�f�rences d'un abonn� en v�rifiant qu'il s'agit du m�me client
	* 
	* @param Client $client
	* @param int $id
	* @returns PayboxAbonne
	*/
	static public function factory(Client $client, $id)
	{
		$x = new PayboxAbonne();
		$x->load((int) $id);
		return ($x->_data['client']==$client->getId() ? $x : null);
	}
	
	/**
	* Renvoie si la carte est vlaide ?
	* @returns bool
	*/
	public function isValid()
	{
		// validit� de la carte : 1er jour du mois de validiit�
		return ($this->_data['validite'] >= date('Y-m-01'));
	}
	// supprime un abonnement
	public function remove(&$msg)
	{
		$a = Paybox::desabonnement($this->_data['REFABONNE'], $this->_data['PORTEUR'], $this->_data['DATEVAL'], $this->_data['CVV']);
		if ($a['CODEREPONSE']=='00000')
		{
			getsql("DELETE FROM paybox_abonne WHERE id=".(int)$this->getId());
			return true;
		}
		$msg = "Une erreur s'est produite lors de la suppression d'une carte : ".$a['COMMENTAIRE'];
		return false;
	}
};
?>
