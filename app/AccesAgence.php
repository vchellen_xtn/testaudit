<?php
/**
* G�re les droits d'acc�s � /resas/
* Contient les donn�es :
* 	login			identifiant
* 	droits			droits pour l'utilisateur (cf Acces)
* 	code_base		donne le droit � toutes les agences avec ce code_base
* 	code_societe	donne le droit � toutes lles agences avec ce code_societe
* 	agence			donen le droit � cette agence
* 	pdv				point de vente en cours de consultation
* 
*/
class AccesAgence extends Ada_Object
{
	const ROLE_ADMIN 	= 255;
	const ROLE_COMPTOIR	= 2;  // Acces::SB_TC 
	const ROLE_AGENCE	= 18; // Acces::SB_TC + Acces:SB_GESTION
	const ROLE_MANAGER	= 50; // Acces::SB_TC + Acces::SB_GESTION + Acces::SB_OFFRES
	
	static public $ROLES = array
	(
		'admin'		=> array(
			'droits'=> self::ROLE_ADMIN,
			'label'	=> 'Administrateur',
			'scope'	=> 'code_base,code_societe,agence,pdv',
		),
		'comptoir' => array(
			'droits'=> self::ROLE_COMPTOIR,
			'label'	=> 'Vendeur agence',
			'scope'	=> 'agence,pdv',
		),
		'chefagc' => array(
			'droits'=> self::ROLE_AGENCE,
			'label' => "Responsable d'agence",
			'scope'	=> 'agence,pdv',
		),
		'manager' => array(
			'droits'=> self::ROLE_MANAGER,
			'label' => "Gestionnaire soci�t�",
			'scope'	=> 'code_base,code_societe,agence,pdv',
		)
	);
	
	/** @var AgenceConfig_Collection */
	protected $_config = null;
	/** @var Agence_Collection */
	protected $_agences;
	/** @var Agence_Collection */
	protected $_pdvs;
	/** @var array */
	protected $_societes = array();
	/** @var string */
	protected $_code_base = null;
	/** @var array zones dans lesquelles sont pr�sentes les agences*/
	protected $_zones = array();
	/** @var array Groupes dans lesquels se trouvent les agence */
	protected $_groupes = array();
	
	/**
	* Pr�pare un nouveau AccesAgence
	* 
	* @param mixed $login
	* @param mixed $role
	* @param mixed $agence
	* @return AccesAgence
	*/
	static public function create($role, $login, $agenceID, $key, &$msg) {
		if (!self::$ROLES[$role]) {
			$msg = "Le r�le est incorrect !";
			return null;
		}
		if (!preg_match('/^[a-z0-9\.\-\_]+$/i', $login)) {
			$msg = "Le login n'est pas correct !";
			return null;
		}
		if (!Agence::isValidID($agenceID)) {
			$msg = "L'identifiant de l'agence n'est pas correct !";
			return null;
		}
		if ($role!='admin' && $key != self::getKey($role, $login, $agenceID)) {
			$msg = "La cl� d'authentification n'est pas correcte !";
			if (SITE_MODE != 'PROD')
				$msg.="<br/> La cl� correcte est : ".self::getKey($role, $login, $agenceID);
			return null;
		}
		
		// on cr�e l'agence
		$agence = Agence::factory($agenceID);
		if (!$agence) {
			$msg = "L'agence est incorrecte !";
			return null;
		}
		return self::createSpecial($agence, $role, $login);
	}
	/**
	* Cr�e un AccesAgence sans v�rifications !
	* Usage interne uniquement !
	* 
	* @param Agence $agence
	* @param string $role
	* @param string $login
	* @return AccesAgence
	*/
	static public function createSpecial(Agence $agence, $role = 'comptoir', $login = 'resas')
	{
		$a = array(
			'login'	=> $login,
			'droits'=> self::$ROLES[$role]['droits']
		);
		foreach(explode(',', self::$ROLES[$role]['scope']) as $k)
			$a[$k] = $agence[$k];
		$x = new AccesAgence($a);
		return $x;
	}
	
	/**
	* Cr�e un acc�s � partir d'un Cookie
	* 
	* @param string $cookie
	* @return AccesAgence
	*/
	static public function createFromCookie($cookieData)
	{
		if (!$cookieData) return null;
		if ($a = self::_decryptData($cookieData))
		{
			$x = new AccesAgence($a);
			return $x;
		}
		return null;
	}
	
	/**
	* Calcule une cl� pour v�rifier l'authentification
	* 
	* @param string $role
	* @param string $login
	* @param string $agence
	* @returns string
	*/
	static public function getKey($role, $login, $agence) {
		$secret = ($role != 'admin' ? 'St@tFranchises' : str_rot13('St@tFranchises'));
		return md5($login.date('Ymd').$role.$secret.$agence);
	}
	
	/**
	* Renvoie les donn�es devant �tre stock�es dans le cookie
	* @returns string
	*/
	public function getCookie()
	{
		return self::_encryptData($this->getData());
	}
		/**
	* V�rifie un droit
	* 
	* @param int $right
	* @returns bool
	*/
	public function hasRights($right)
	{
		return (($this->_data['droits'] & $right) == $right);
	}
	/**
	* Renvoie le r�le d'un utilisateur
	* @returns string
	*/
	public function getRole() {
		foreach(self::$ROLES as $role => $info) {
			if ($info['droits'] == $this->_data['droits']) {
				return $role;
			}
		}
	}
	/**
	* V�rifie si un utilisateur a le droit d'acc�s sur une agence
	* 
	* @param string $agence
	* @returns bool
	*/
	public function hasRightsOnAgence($agence) {
		if (is_array($agence)) {
			$pdvs = $this->getPointsDeVente();
			foreach($agence as $a) {
				if (!$pdvs->offsetExists($a)) {
					return false;
				}
			}
			return true;
		} else {
			return ($this->getPointsDeVente()->offsetExists($agence));
		}
	}
	/**
	* On remet � z�ro les configurations pour forcer le rechargement
	* Notamment apr�s une modification dans le back office
	* @returns AccesAgence
	*/
	public function resetConfig() {
		$this->_config = null;
		return $this;
	}
	
	/**
	* Renvoie une donn�e de configuration pour cet utilisateur / agence / soci�t� / code base
	* 
	* @param string $prop
	* @param strinb $scope  'code_base','code_societe", 'agence'
	* @param string $index	la valeur associ�e � 
	* @param bool $goUp v�rifie les droits au dessus
	* @return string
	*/
	public function getConfigData($prop, $scope = null, $index = null, $goUp = true) {
		// on charge les configurations si nc�cessaires
		if (!$this->_config) {
			$this->_config = AgenceConfig_Collection::factory($this);
		}
		// si le scope n'est pas d�fini, on commence par le pr�ciser
		if (!$scope && !$index) {
			foreach(array('code_base','code_societe','agence') as $k) {
				if ($this->_data[$k]) {
					$scope = $k;
					$index = $this->_data[$k];
					break;
				}
			}
		}
		// OK.. maintenant on cherche la r�ponse dans le scope courant
		if ($scope == 'code_base') {
			// le code_base est un cas particulier...
			if ($index == $this->_data['code_base']) {
				// on renvoie la premi�re valeur non nulle des soci�t�s
				foreach($this->getSocietes() as $k => $v) {
					if ($c = $this->_config[$k]) {
						if (!is_null($c[$prop])) {
							return $c[$prop];
						}
					}
				}
			}
		} else {
			// le cas g�n�ral
			if ($c = $this->_config[$index]) {
				if (!is_null($c[$prop])) {
					return $c[$prop];
				}
			}
		}
		// bon, on a rien trouv� � ce niveau on regarde au dessus
		if (!$goUp) {
			// on ne veut pas regarder les droits pr�c�dents
			return null;
		} else if ($scope == 'agence') {
			// on remonte au niveau soci�t�
			if ($agence = $this->getAgences()->getItem($index)) {
				return $this->getConfigData($prop, 'code_societe', $agence['code_societe']);
			}
		} else if ($scope == 'code_societe' || $scope == 'code_base') {
			// on remonte au niveau global
			return $this->getConfigData($prop, 'default', '%');
		}
		return null;
	}

	/**
	* Renvoie la liste des agences accessibles par cet utilisateur
	* @returns Agence_Collection
	*/
	public function getAgences() {
		if (!$this->_agences) {
			// pr�parer le SQL
			$agenceStatut = Agence::STATUT_VISIBLE + Agence::STATUT_RESAWEB;
			$sql = "select a.id, a.zone, a.code_base, a.code_societe, a.code_groupe, a.groupe, d.id dept, d.nom dept_nom, z.nom zone_nom, a.nom, a.societe\n";
			$sql.=" from agence a\n";
			$sql.=" join agence_pdv ap on ap.id=a.id\n";
			$sql.=" join zone z on z.id=a.zone\n";
			$sql.=" left join dept d on (a.zone='fr' and d.id=left(ap.cp,2))\n";
			$sql.=" where a.reseau='ADA' and (a.statut & $agenceStatut) = $agenceStatut\n";
			foreach(array('code_base','code_societe','agence') as $k) {
				if ($this->_data[$k]) {
					$sql.= ' AND a.'.($k=='agence' ? 'id' : $k)." = '".$this->_data[$k]."'";
					break;
				}
			}
			$sql.=" order by a.zone, d.id, a.id\n";
			// charger la collection
			$this->_agences = new Agence_Collection();
			$this->_agences->loadFromSQL($sql);
		}
		return $this->_agences;
	}
	public function getPointsDeVente() {
		if (!$this->_pdvs) {
			// pr�parer le SQL
			$agenceStatut = Agence::STATUT_VISIBLE + Agence::STATUT_RESAWEB;
			$sql = "select ap.id, a.zone, a.code_base, a.code_societe, a.code_groupe, a.groupe, d.id dept, d.nom dept_nom, z.nom zone_nom, ap.nom, a.societe\n";
			$sql.=" from agence a\n";
			$sql.=" join agence_pdv ap on ap.agence=a.id\n";
			$sql.=" join zone z on z.id=a.zone\n";
			$sql.=" left join dept d on (a.zone='fr' and d.id=left(ap.cp,2))\n";
			$sql.=" where a.reseau='ADA' and (a.statut & $agenceStatut) = $agenceStatut and ap.publie=1\n";
			foreach(array('code_base','code_societe','agence') as $k) {
				if ($this->_data[$k]) {
					$sql.= ' AND a.'.($k=='agence' ? 'id' : $k)." = '".$this->_data[$k]."'";
					break;
				}
			}
			$sql.=" order by a.zone, d.id, ap.id\n";
			// charger la collection
			$this->_pdvs = new Agence_Collection();
			$this->_pdvs->loadFromSQL($sql);
		}
		return $this->_pdvs;
	}
	/**
	* Renvoie la liste des soci�t�s accessibles par cet utilisateur
	* @returns array
	*/
	public function getSocietes() {
		if (!$this->_societes) {
			// remplir la laiste des soci�t�s
			foreach($this->getAgences() as $agence) {
				if (!isset($this->_societes[$agence['code_societe']])) {
					$this->_societes[$agence['code_societe']] = $agence['code_societe'].' - '.$agence['societe'];
				}
			}
		}
		return $this->_societes;
	}
	public function getCodeBase() {
		if (!$this->_code_base) {
			foreach($this->getAgences() as $agence) {
				$this->_code_base = $agence['code_base'];
				break;
			}
		}
		return $this->_code_base;
	}
	public function getZones() {
		if (!$this->_zones) {
			foreach($this->getAgences() as $agence) {
				if (!isset($this->_zones[$agence['zone']])) {
					$this->_zones[$agence['zone']] = $agence->getZone()->getData('nom');
				}
			}
		}
		return $this->_zones;
	}
	public function getGroupes() {
		if (!$this->_groupes) {
			foreach($this->getAgences() as $agence) {
				if (!is_null($agence['groupe']) && !isset($this->_groupes[$agence['groupe']])) {
					$this->_groupes[$agence['groupe']] = (string) (ZoneGroupe::factory($agence['groupe']));
				}
			}
		}
		return $this->_groupes;
	}

}
?>
