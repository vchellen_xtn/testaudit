<?
class VisuelPromo_Collection extends Ada_Collection
{
	/**
	* Cr�� une collection des visuels promo
	*
	* @return VisuelPromo_Collection
	* @param int $fromPosition
	* @param int $toPosition
	* @returns VisuelPromo_Collection
	*/
	static public function factory($random_order = false, $fromPosition = 0, $toPosition=99)
	{
		// on cr�e la collection qui sera retourn�e
		$x = new VisuelPromo_Collection();
		$sql = 'SELECT id, url_img, (duree*1000) AS time, commentaire, target, href';
		$sql.=' FROM visuel_promo';
		$sql.=sprintf(' WHERE publie = 1 AND position BETWEEN %ld AND %ld', $fromPosition, $toPosition);
		if ($random_order) {
			$sql .= ' ORDER BY RAND();';
		} else {
			$sql .= ' ORDER BY position ASC;';
		}
		$x->_loadFromSQL($sql);
		return $x;
	}
}
?>
