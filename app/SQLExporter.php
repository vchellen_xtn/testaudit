<?
class SQLExporter extends Ada_Object
{
	static private $_init = false;
	const FORM_NAME = '_sql_exporter';

	/**
		affiche le bouton d'export
		@param string $label	intitulé du bouton
		@param string $fname	nom du fichier (sans extension)
		@param string $sql		Requete SQL à exécuter
		@param bool $sqlpreseve	si false remplace le "select a,b.." par "select *"
	*/
	static public function getButton($label, $fname, $sql, $sqlpreseve = true)
	{
		$data = self::_encryptData(array('fname' => $fname, 'sql' => $sql, 'sqlpreserve'=>$sqlpreseve));
		$html = '';
		if (!self::$_init) 
		{
			trigger_error('SQLExporter not initialized; getForm() not called', E_USER_WARNING);
			return null;
		}
		$html .= '<input type="button" value="'.$label.'" onclick="return _doSQLExport(\''.$fname.'\', \''.$data.'\', \''.($sqlpreseve?1:0).'\');"/>';
		return $html;
	}
	
	/**
		chiffre les données de l'exporteur
		@param Array $data
	*/
	static protected function _getExportData($data)
	{
		self::_encryptData($data);
	}
	
	/**
		décrypte des données d'export et renvoie un tableau
		@param Array $stuff
	*/
	static public function doUnserialize($stuff)
	{
		if($data = self::_decryptData($stuff))
		{
			return new SQLExporter($data);
		}
		return null;
	}
	
	/**
		Renvoie le formulaire HTML nécessaire au fonctionnement de l'exporteur. Doit être appelé avant tout appel de getButton
	*/
	static public function getForm($formAction = 'lists/export.php')
	{
		if(self::$_init)
			return false;
		$html = '';
		$html .= '<form method="post" id="'.self::FORM_NAME.'" action="'.$formAction.'">';
		$html .= '<input type="hidden" name="sqlencrypted" value="1" />';
		$html .= '<input type="hidden" name="data" value="">';
		$html .= '<input type="hidden" name="fname" value="">';
		$html .= '<input type="hidden" name="sqlpreserve" value="">';
		$html .= '</form>';
		$html .= '
		<script type="text/javascript">
			function _doSQLExport(fname, data, sqlpreserve) {
				var formExport = document.forms.'.self::FORM_NAME.';
				formExport.data.value = data;
				formExport.fname.value = fname;
				formExport.sqlpreserve.value = sqlpreserve;
				formExport.submit();
			}
		</script>';
		self::$_init = true;
		return $html;
	}
	
	/**
		Execute la requete associée à l'objet SQLExporter et propose le téléchargement d'un fichier xls
		A partir de data['sql'] et data['fname']
	*/
	public function toExcel()
	{
		if ($sql = stripslashes($this->getData('sql')))
		{
			if (!$sql || !preg_match('/^select\s/i', $sql)) {
				die("Vous devez entrer une commande SQL");
			}
			// provoquer le dialogue de téléchargement
			header("content-Type: application/ms-excel");
			header("Content-Disposition: attachment; filename=".$this->getData("fname").".xls");

			// afficher toutes les colonnes
			if ($this->hasData('sqlpreserve') && !$this->getData('sqlpreserve'))
				$sql = preg_replace("/^select(.+)\sfrom\s/i", "select * from ", $sql);

			// pour ouvrir la connexion SQL
			sqlexec('select count(*) from categorie');
			
			// les entêtes de colonnes : on utilise mysql_unbuffered_query() pour limiter l'utilisation de la mémoire
			$rs = mysql_unbuffered_query($sql);
			for ($out="", $i=0; $i<mysql_num_fields($rs); $i++) 
			{
				$fld = mysql_fetch_field($rs, $i);
				$out.= $fld->name."\t";
			}
			$out.= "\n";
			echo $out; $out = null;
			// contenu du fichier
			while ($row=mysql_fetch_row($rs))
			{
				foreach ($row as $val)
					$out.= preg_replace("/(\s+)|(\")/", " ", $val)."\t";
				$out.= "\n";
				echo $out; $out = null;
			}
		}
	}
}
?>
