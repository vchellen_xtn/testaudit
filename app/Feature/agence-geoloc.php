<?php
/* Param�tres pouvant �tre pass� :
	$options		options � passer � api/json/agences c'est-�-dire � Agence_Collection::factory()
	$title			pour le titre de la popin
	$inside_page	pour �tre int�gr� � la page et non en popin
*/
	if (!$title) {
		$title = "Choisir votre agence";
	}
?>
<!-- popin recherche agence //-->
<div id="block_recherche_agence" class="bloc" title="<?= $title ?>" style="display:<?= $inside_page ? 'block' : 'none' ?>">
	<h3><?= $title ?></h3>
	<div id="results_recherche_agence">
		<div id="liste_recherche_agence" class="left"></div>
		<div id="carte_recherche_agence" class="left"></div>
		<div class="clear"></div>
	</div>
</div>
<script type="text/javascript" src="../scripts/agence-geoloc.js"></script>
<script type="text/javascript">
/* <![CDATA[  */
	appADA.searchOptions = <?=json_encode($options)?>;
	appADA.googleApiKey = "<?= GOOGLE_API_KEY ?>";
	$(document).ready(function() {
		appADA.geolocReady(<?= $inside_page ?>);
	});
/* ]]> */
</script>