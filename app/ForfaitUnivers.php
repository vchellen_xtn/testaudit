<?php
class ForfaitUnivers extends Ada_Object
{
	static public $UNIVERS = array('JEUNES' => 'JEUNES');
	
	/**
	* Renvoie un forfait jeune
	* 
	* @param int $id
	* @returns ForfaitUnivers
	*/
	static public function factory($id)
	{
		$x = new ForfaitUnivers();
		return $x->load((int)$id);
	}
	
	public function _construct()
	{
		$this->setTableName('forfait2_univers');
	}
	
	public function __toString()
	{
		return $this->_data['nom'];
	}
	
	/**
	* V�rifie si une r�servaiton r�pond aux crit�res du Forfait
	* 
	* @param Reservation $reservation
	* @returns bool
	*/
	public function checkReservation(Reservation $reservation)
	{
		// v�rification de la dur�e
		$duree = $reservation['duree'];
		if ($duree < $this->getData('jours_min') || $duree > $this->getData('jours_max'))
			return false;
		// v�rification du d�part
		$j1 = $reservation->getJ1();
		if ($j1 < $this->getData('depart_debut') || $j1 > $this->getData('depart_fin'))
			return false;
		// v�rificaiton du retour
		$j2 = $reservation->getJ2();
		if ($j2 < $this->getData('retour_debut') || $j2 > $this->getData('retour_fin'))
			return false;
		return true;
	}
};
?>
