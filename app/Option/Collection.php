<?
/**
* Renvoie une liste d'options assoici�e � un forfait (cat�gorie, duree, distance )
*/
class Option_Collection extends Ada_Collection
{
	/** @var string */
	protected $_type = null;
	/** @var int distance en km */
	protected $_distance;
	/** @var int d�lai avant la r�servation */
	protected $_delai = null;
	/** @var int nombre de jours de la r�servation */
	protected $_duree;
	/** @var array groupe d'options ? */
	protected $_optGroup = array();
	/** @var array pages o� afficher les options */
	protected $_pages = array();

	public function __construct(Option_Collection $clone = null)
	{
		parent::__construct();
		$this->_type = $clone->_type;
		$this->_duree = $clone->_duree;
		$this->_distance = $clone->_distance;
		$this->_delai = $clone->_delai;
	}
	/**
	* Renvoie une liste d'options � partir d'un forfait et d'une r�servation
	* (Si l'agence est de type Point Loc, il n'y a pas d'option ni d'assurance)
	*
	* @param Forfait $forfait
	* @param Reservation $reservation
	* @param bool fix_franchise ajoute les franchise rfv[pu] au montant afv[pu]
	* @return Option_Collection
	*/
	static public function factory(Forfait $forfait, Reservation $reservation, $en_avant = false)
	{

		// cri�tres pour la recherche
		$agence = $reservation->getAgence();
		$agence_statut = $agence['statut'];
		$zone = $agence['zone'];
		$reseau = $agence['reseau'];
		$type = strtolower($reservation['type']);
		$categorie = ($forfait['categorie'] ? $forfait['categorie'] : $reservation['categorie']);

		// crit�res pour la s�lection des options
		$duree = ($forfait['duree'] ? $forfait['duree'] : $reservation['duree']);
		$age = (int)$reservation['age'];
		$delai = ($reservation['debut'] ? $reservation->getHoursBeforeStart() : 24*21);

		// SQL
		$sql = "SELECT o.*, (CASE WHEN o.facturation='MATERIEL' THEN 1 ELSE 0 END) a_retirer, COALESCE(ao.paiement, t.paiement, o.paiement) paiement, COALESCE(o.position,9999) position\n";
		$sql.=", t.prix, t.jours,  t.prix_jour\n";
		$sql.=", th.nom theme_nom, th.page theme_page, th.ouvert theme_ouvert, th.popin theme_popin";
		$sql.=", tg.nom groupe_nom";
		$sql.=" FROM `option` o\n";
		$sql.=" JOIN option_tarif t ON (t.zone='".$zone."' and t.reseau='".$reseau."' and t.`option`=o.id and ".(int)$duree." BETWEEN IFNULL(t.jours,0) AND IFNULL(t.jours_max,999))\n";
		$sql.=" LEFT JOIN option_theme th ON th.id=o.theme\n";
		$sql.=" LEFT JOIN option_theme tg ON tg.id=o.groupe\n";
		$sql.=" LEFT JOIN categorie_option c ON c.`option` = o.id AND c.categorie = ".(int)$categorie."\n";
		$sql.=" LEFT JOIN agence_option ao ON (ao.agence='".$agence['agence']."' AND ao.`option`=o.id)\n";
		$sql.=" WHERE o.type = '".$type."'\n";
		// s�lection pour le rachat de franchise (staut agence et conducteur < 74 ans)
		$sql.= ' AND (o.agence_statut & '.$agence_statut.") != 0\n";
		$sql.= ' AND ('.$age." BETWEEN o.age_min AND o.age_max)\n";
		$sql.= " AND (IFNULL(c.filtre,0)=0)\n";
		if ($en_avant) {
			$sql.= ' ORDER BY COALESCE(o.en_avant,9999), COALESCE(o.position,9999) ASC, o.id ASC';
		} else {
			$sql.= ' ORDER BY COALESCE(o.position,9999) ASC, o.id ASC';
		}

		// cr�er la collection
		$x = new Option_Collection();
		$x->_type = $type;
		$x->_duree = $duree;;
		$x->_delai = $delai;
		$x->_distance = $reservation['distance'];
		if (!$x->_distance)
			$x->_distance = ($forfait['km'] ? $forfait['km'] : $reservation['km']);
		// charger la collection sauf pour les locapass qui n'ont pas d'options

		// pour les POINT LOC il n'y a pas d'options
		if($agence->isPointLoc()) {
			return $x;
		}
		if (!$reservation->isLocapass())
		{
			$x->_loadFromSQL($sql);
			/** @var Categorie : on met � jour les donn�es d�pendant de la cat�gorie */
			if ($type == 'vp' || $type=='vu')
			{
				$c = ($categorie == $reservation['categorie'] ? $reservation->getCategorie() : Categorie::factory($categorie));
				$search = array(); $replace = array();
				foreach(array('franchise','franchise_rf','franchise_af') as $k)
				{
					$search[] = '/(<span data-var="'.$k.'">)(<\/span>)/';
					$replace[] = '${1}'.show_money($c[$k]).'${2}';
				}
				foreach(array('nf','rf','pf','af') as $k)
				{
					if ($o = $x->_items[$k.$type])
						$o['commentaire'] = preg_replace($search, $replace, $o['commentaire']);
				}
			}
		}
		return $x;
	}
	/**
	* � l'affichage des tarifs, on doit additionner les prix de rfv[pu] � afv[pu]
	* @param Reservation $reservation la r�servation
	* @param bool $missingOptions est ce qu'on cherche � proposer des nouvelles optiosn ? les options existantes �tant act�es !
	* @returns Option_Collection
	*/
	public function fixFranchise(Reservation $reservation = null, $missingOptions = false)
	{
		$af = $this->_items['af'.$this->_type];	// ARF : Assurance Remboursement de Franchise
		$rf = $this->_items['rf'.$this->_type]; // GRF Seule : Garantie R�duction de Franchise
		$pf = $this->_items['pf'.$this->_type]; // GRF Pack : Garantie R�duction de Frnchise en pack avec l'ARF
		if (!$pf) $pf = $rf;
		if ($af && $pf && !$af['fix_franchise'])
		{
			// � la base, le tarif de l'ARF doit inclure la GRF quand on le propose en option
			$af['fix_franchise'] = true;
			$af['prix'] += $pf['prix'];
			if ($af['prix_jour'])
				$af['prix_jour'] += $pf['prix_jour'];

			// 2013-07-01 si la Reservation est pass�e
			// on modifie le montant de l'ARF en enlevant celui de la rfv[pu]
			if (($missingOptions && $reservation && $reservation->hasOption('rf'.$this->_type)) || ($reservation && $reservation->getData('grf_inclus')))
			{
				$af['prix'] -= $rf['prix'];
				if ($af['prix_jour'])
					$af['prix_jour'] -= $rf['prix_jour'];
				if ($reservation['forfait_type']=='RESAS') {
					if ($reservation->getData('grf_inclus')) {
						// on ne pr�sente que "af"
						unset($this->_items['rf'.$this->_type]);
						$this->_optGroup['franchise']['nb']--;
						unset($this->_items['nf'.$this->_type]);
						$this->_optGroup['franchise']['nb']--;
					}
				} else {
					// 2013-07-01 : et finalement on ne l'affiche pas pour ne pas avoir d'incoh�rences tarfaires
					unset($this->_items['af'.$this->_type]);
					$this->_optGroup['franchise']['nb']--;
				}
			}
			// sans la r�servation on supprime rfvu (page resultat)
			if ((!$reservation || $reservation['forfait_type']!='RESAS') && !$missingOptions)
			{
				// GEB: 2013-05-16: retirer rfvu de la pr�sentation
				/* GEB: 2015-09-07: on r�tablit rfvu
				if ($this->_type == 'vu')
				{
					unset($this->_items['rf'.$this->_type]);
					$this->_optGroup['franchise']['nb']--;
				}
				*/
			}

			// si $pf est le package on le supprime de la liste des options
			if ($pf['id'] == 'pf'.$this->_type)
			{
				unset($this->_items['pf'.$this->_type]);
				$this->_optGroup['franchise']['nb']--;
			}
		}
		return $this;
	}

	/**
	* Pour chaque option ajout�e
	* - on v�rifie son appartenance � un groupe
	* - on calcule son prix en fonction de la dur�e et de la distance
	*
	* @param Option $option
	* @return Ada_Collection
	*/
	public function add(Ada_Object $option)
	{
		// on n'ajoute pas les options de type 'N'
		if ($option['paiement']=='N')
			return $this;
		// hors d�lai on transforme l'option payable en ligne, � payable en agence
		if (!is_null($option['delai_min']) && $option['paiement']=='L' && $this->_delai < $option['delai_min'])
		{
			$option['paiement'] = 'A';
			// ... et finalement on ne pr�sente pas l'option hors d�lai
			return $this;
		}

		// compter les groupes
		if ($groupe = $option['groupe'])
		{
			// on augmente le compteur du groupe
			$this->_optGroup[$groupe]['nb']++;
			// on ne peut pas avoir plus d'une option initilias�e par groupe
			if ($option['init'] && !$this->_optGroup[$groupe]['init'])
				$this->_optGroup[$groupe]['init'] = 1;
			else
				$option['init'] = 0;
		}
		// calculer le prix de l'option en fonction de la distance et de la dur�e
		$prix = $option['prix'];
		if (!$option['fraction']) $option['fraction'] = 1;
		if ($option['tarification'] == 'K')
		{
			// on v�rifie qu'on peut effectivement calculer le prix kilom�trique
			if (!$this->_distance || !$option['fraction'])
				return null;
			$prix *= ($this->_distance / $option['fraction']);
			$prix = round($prix);
		}
		else if ($option['tarification'] == 'J')
		{
			if ($option['jours']) // s'il y a un nombre de jours, le prix correspond � ce nombre de jours, on se base sur prix_jour pour les jours suppl�mentaires
			{
				$prix = round($prix + (max(0, $this->_duree - $option['jours']) * $option['prix_jour']), 2);
				if ($option['prix_jour'] && $prix != round($this->_duree * $option['prix_jour'], 2))
					$option['prix_jour'] = null;
			}
			else
			{
				$option['prix_jour'] = $prix;
				$prix *= $this->_duree / $option['fraction'];
			}
		}
		$option['prix'] = ($option['paiement'] == 'L' ? $prix : 0);
		if ($groupe && !$option['prix'])
			$this->_optGroup[$groupe]['non'] = true;
		// renseigner la page
		if (!$this->_pages[$option['theme_page']])
			$this->_pages[$option['theme_page']]++;
		// ajouter l'option
		return parent::add($option);
	}
	public function isInGroup($groupe)
	{
		if ($groupe && $this->_optGroup[$groupe]['nb'] > 1)
			return true;
		return false;
	}
	public function getGroupCount($groupe)
	{
		return (int) $this->_optGroup[$groupe]['nb'];
	}
	public function hasPage($page)
	{
		return ($this->_pages[$page] > 0);
	}
	/**
	* Renvoyer les options associ�es � un th�me
	*
	* @param string $theme
	* @return Option_Collection
	*/
	public function getOptionsByTheme($theme, &$theme_nom)
	{
		$theme_nom = null;
		$x = new Option_Collection();
		foreach($this->_items as $o)
		{
			if ($o['theme'] == $theme)
			{
				if (!$theme_nom) $theme_nom = $o['theme_nom'];
				$x->_items[$o['id']] = $o;
			}
		}
		return $x;
	}
}
?>
