<?php
class AgenceBonPlan extends Ada_Object {
	
	// liste des statuts
	const STATUT_ATTENTE = 'attente';
	const STATUT_PUBLIC = 'public';
	const STATUT_ARCHIVE ='archive';
	
	// liste des tags autoris�s
	const ALLOWED_TAGS = '<a><b><br><br/><div><em><i><u><p><strong><span><ol><ul><li><sub><sup>';
	
	// informations sur les visuels
	const VISUEL_WIDTH_MAX = 180;
	const VISUEL_HEIGHT_MAX = 108;
	const VISUEL_UPLOAD = 'fichiers/bonsplans/';

	// e-mail d'alerte
	const EMAIL_BONPLAN_TPL = '/emails/em-agence-bonplan.html';
	const EMAIL_BONPLAN_SUBJECT = '[ADA][%s][%s] Bon plan en attente';
	
	protected $_doNotify = false;
	
	// tableau des statuts
	static public $STATUTS = array(
		self::STATUT_ATTENTE => 'en attente',
		self::STATUT_PUBLIC  => 'public',
		self::STATUT_ARCHIVE => 'archiv�',
	);
	
	/**
	* Renvoie une AgenceBonPlan
	* 
	* @param int $id
	* @return AgenceBonPlan
	*/
	static public function factory($id) {
		$x = new AgenceBonPlan();
		$x->load((int) $id);
		if ($x->isEmpty()) return null;
		return $x;
	}

	/**
	* Cr�e un nouveau bon plan
	* 
	* @param array $data  vient de $_POST
	* @param array $files  vient de $_FILES
	* @param array $errors
	* @return AgenceBonPlan
	*/
	static public function create(&$data, &$files, &$errors)
	{
		$x = new AgenceBonPlan();
		$fields = array_keys($x->_getValidatingRules());
		if ($x->validateDataEx($data, $files, $errors, $fields)) {
			// on enregistre les champs
			foreach($fields as $k) {
				if (isset($data[$k])) {
					$x->setData($k, $data[$k]);
				}
				$x->setData('statut', self::STATUT_ATTENTE);
			}
			$x->save();
			$x->_doNotify = true;
			$x->notifyBonPlan();
			return $x;
		}
		return null;
	}
	
	public function update(&$data, &$files, &$errors)
	{
		$fields = array_keys($this->_getValidatingRules());
		if ($this->validateDataEx($data, $files, $errors, $fields)) {
			$this->updateStatut($data['statut']);
			// on enregistre les champs
			foreach($fields as $k) {
				if (isset($data[$k])) {
					$this->setData($k, $data[$k]);
				}
			}
			$this->setData('modification', date('Y-m-d H:i:s'));
			$this->save();
			$this->notifyBonPlan();
			return $this;
		}
		
	}
	
	/**
	* Renvoie une date format�e
	* 
	* @param string $fld
	* @param string $format
	* @returns string
	*/
	public function getDate($fld, $format = 'd/m/Y') {
		if (isset($this->_data[$fld]) && !is_null($this->_data[$fld])) {
			return date($format, strtotime($this->_data[$fld]));
		}
		return null;
	}
	/**
	* Renvoie les dates
	* @returns string
	*/
	public function getInfoDates() {
		$debut = $this->getDate('debut', 'd/m/Y');
		$fin = $this->getDate('fin', 'd/m/Y');
		if ($debut && $fin) {
			return sprintf('Du %s au %s', $debut, $fin);
		} else if ($debut) {
			return sprintf('� partir du %s', $debut);
		} else if ($fin) {
			return sprintf("Jusqu'au %s", $fin);
		}
		return null;
	}
	public function getVisuel() {
		if ($this->_data['visuel'] && is_file(BP . '/' . $this->_data['visuel'])) {
			return $this->_data['visuel'];
		}
		return null;
	}
	
	/**
	* Modifie le statut du bon plan
	* 
	* @param string $statut
	* @param bool $doSave enregistre ou pas apr�s la mise � jour
	* @returns bool
	*/
	public function updateStatut($statut, $doSave = false) {
		if (!isset(self::$STATUTS[$statut]) || $statut == $this->_data['statut']) {
			return false;
		}
		if ($statut == self::STATUT_PUBLIC) {
			$fields['publication'] = date('Y-m-d H:i:s');
		} else if ($statut == self::STATUT_ATTENTE) {
			$this->_doNotify = true;
		}
		// finaliser la modification du statut
		$fields['statut'] = $statut;
		$fields['modification'] = date('Y-m-d H:i:s');
		// enregistrer les modifications
		foreach($fields as $k => $v) {
			$this->setData($k, $v);
		}
		if ($doSave) {
			$this->save(implode(',', array_keys($fields)));
		}
		return true;
	}
	
	/**
	* Pr�vient par e-mail de l'attente d'un bon plan � publier
	* @returns bool
	*/
	public function notifyBonPlan() {
		if (!$this->_doNotify) return false;
		
		// pr�parer les donn�es
		$agence = Agence::factory($this->_data['agence']);
		$data = array(
			// agence
			'code_societe' => $agence['code_societe'],
			'agence' => sprintf('%s - %s', $agence['id'], $agence['nom']),
			'url_agence' => $agence->getURLAgence(),
			'email'	=> $agence['email'],
			'telephone'	=> $agence['tel'],
			// bon plan
			'id'	=> $this->_data['id'],
			'titre' => $this->_data['titre'],
			'debut' => $this->_data['debut'],
			'fin'	=> $this->_data['fin'],
			'description' => $this->_data['description'],
			'visuel' => $this->_data['visuel'],
		);
		
		// pr�parer l'e-mail
		$html = load_and_parse(BP.self::EMAIL_BONPLAN_TPL, $data);
		$subject = sprintf(self::EMAIL_BONPLAN_SUBJECT, SITE_MODE, $this->_data['agence']);
		
		// envoyer l'e-mail
		send_mail($html, EMAIL_BONPLAN, $subject, EMAIL_FROM, EMAIL_FROM_ADDR, null, '');
		return true;
	}
	
	/**
	* Valide les donn�es et rajoute la v�rification de :
	* - description
	* - visuel
	* 
	* @param array $data
	* * @param array $files
	* @param array $errors
	* @param array $fields
	* @return bool
	*/
	public function validateDataEx(&$data, &$files, &$errors, &$fields = null)
	{
		// on valide les premiers champs comme habituellement
		if ($this->validateData($data, $errors, $fields)) {
			// puis on v�rifie la description...
			if (!isset($data['description']) || empty($data['description'])) {
				$errors[] = "Vous devez indiquer une description";
			} else {
				// on supprime certains tags
				$data['description'] = trim(strip_tags($data['description'], self::ALLOWED_TAGS));
				if (empty($data['description'])) {
					$errors[] = "Vous devez indiquer une description !";
				} else {
					// on ajoute � la liste des fields � enregistrer
					$fields[] = 'description';
				}
			}
			// calculer le champ canon
			if (!$this->_data['canon']) {
				$data['canon'] = canonize($data['titre']);
				$fields[] = 'canon';
			}
			// s'il y a des erreurs on s'arr�te l�
			if (!empty($errors)) {
				return false;
			}
			
			// ... et le visuel soit d�j� pr�sent ou il doit �tre fourni
			if (!$this->_data['visuel'] && !isset($files['visuel'])) {
				$errors[] = "Vous devez fournir un visuel !";
			} else if ($files['visuel']) {
				// v�rifier l'extension
				if (!preg_match('/\.(gif|png|jpg|jpeg)$/', $files['visuel']['name'], $m)) {
					$errors[] = "Vous devez uploader un visuel au format GIF, JPEG ou PNG";
				} else if (/** @var Image */ $image = new Image($files['visuel']['tmp_name'])) {
					// v�rification du redimensionnement
					$w = $image->getInfos(0); $h = $image->getInfos(1);
					if ($w > self::VISUEL_WIDTH_MAX || $h > self::VISUEL_HEIGHT_MAX) {
						if ($w > $h) {
							$image->resize(self::VISUEL_WIDTH_MAX, 0);
						} else {
							$image->resize(0, self::VISUEL_HEIGHT_MAX);
						}
					}
					
					// on calcule le nom de l'image
					$ext = $m[1];
					if ($ext == 'jpeg') $ext = 'jpg';
					$fname = self::VISUEL_UPLOAD.sprintf('%s_%s.%s', ($this->_data['agence'] ? $this->_data['agence'] : $data['agence']), ($this->_data['canon'] ? $this->_data['canon'] : $data['canon']), $ext);
					
					// on enregistre le visuel
					if ($image->save(BP.'/'.$fname)) {
						$data['visuel'] = $fname;
						$fields[] = 'visuel';
					} else {
						$errors[] = "Une erreur est survenue lors de l'enregistrement du fichier !";
					}
				} else {
					$errors[] = "Ce visuel n'est pas valide !";
				}
			}
		}
		return empty($errors);
	}
	
	/**
	* Renvoie le tableau des r�gles de validation
	*/
	protected function _getValidatingRules()
	{
		return array (
			'id'	=> $this->_addRuleInt(),
			'agence' => $this->_addRuleRegExp('/^FR[A-Z0-9]\d{2}[A-Z]$/', "Vous devez pr�ciser l'agence", "L'agence n'est pas valide !"),
			'statut' => $this->_addRuleRegExp('/^(attente|public|archive)$/', null, "Le statut n'est pas valide"),
			'titre'	=> $this->_addRuleString("Vous devez indiquer un titre"),
			'debut'	=> $this->_addRuleDate(null, "La date de d�but n'est pas valide"),
			'fin'	=> $this->_addRuleDate(null, "La date de fin n'est pas valide"),
			'createur' => $this->_addRuleString(),
		);
	}
}
?>
