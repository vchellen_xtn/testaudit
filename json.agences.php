<?
/**
* appel�e depuis reservation/index
*/
	include ('constant.php');

	// r�cup�rer les varialbes locales
	$statut = max(1, (int) $_REQUEST['statut']);
	if ($_REQUEST['reseau'] == 'ADA')
		$reseau = 'ADA';
	$zone = $_REQUEST['zone'];
	if (!preg_match('/^[a-z]{2}$/i', $zone))
		$zone = null;
	
	// nb d'items � afficher
	$cnt = 40; // non param�trable
	$sql = 'select id, recherche title'
		   .'  from agence_alias'
		   ." where (statut & $statut) = $statut";
	if ($zone) 
		$sql.=" and zone='$zone'";
	if ($reseau=='ADA')
		$sql.=" and id like 'FR%'";

	// trouver la ville ou l'agence correspondant au nom
	if (($ville = utf8_decode(urldecode($_REQUEST['tag']))) && ($len = strlen($ville)) )
	{
		if (preg_match('/^\d{2,5}$/', $ville))
			$sql.= " and left(cp, $len) = '$ville'";
		else
		{
			$empl = null; $emplLength = 0;
			$ville = filter_chars($ville);
			if (substr($ville, 0, 4) == 'gare')
			{
				$emplLength = 4;
				$empl = Agence::EMPL_GARE;
			}
			else if (substr($ville, 0, 8) == substr('aeroport', 0, $len))
			{
				$emplLength = 8;
				$empl = Agence::EMPL_AERO;
			}
			if ($empl)
			{
				$sql.= ' AND emplacement='.$empl;
				if ($len > $emplLength)
				{
					$ville = trim(substr($ville, $emplLength));
				}
				else
				{
					$ville = '';
				}
				$len = strlen($ville);
			}
			if ($ville)
			{
				$ville = mysql_escape_string($ville);
				$villes = "('$ville','".str_replace('-',' ', $ville)."')";
				$sql.=' and (';
				$sql.="LEFT(ville, $len) IN $villes OR LEFT(recherche, $len) IN $villes OR LEFT(agglo, $len) IN $villes";
				$sql.=')';
			}
		}
	}
	// recherche directe
	$sql.= ' order by nom limit '.$cnt;
	$rs = sqlexec($sql);
	if (mysql_num_rows($rs) == 0)
	{
		if (preg_match("/^\d{5}$/", $ville))
		{
			// trouver les agences les plus proches d'un code postal indiqu�
			$sql = "select latitude, longitude from ville where cp = '$ville'";
			if (list($latitude, $longitude) = getsql($sql))
			{
				$sql = "select a.id, a.recherche as title, 6378 * acos(cos($latitude) * cos(latitude) * cos(longitude - $longitude) + sin($latitude) * sin(latitude)) as ortho"
					  .' from agence_alias a join agence_pdv ap on (ap.id = a.pdv and ap.publie = 1)'
					  ." where (a.statut & $statut = $statut) ";
				$sql.=' and longitude is not null';
				if ($zone)
					$sql.= " and a.zone='$zone'";
				if ($reseau=='ADA')
					$sql.= " and a.id like 'FR%'";
				$sql.=' order by ortho limit '.$cnt;
				$rs = sqlexec($sql);
			}
		}
		else if ($len)
		{
			if ($str = canonize($ville))
			{
				$sql = "select aa.id, aa.recherche title from agence_alias aa join zone z on z.id=aa.zone";
				$sql.=" where z.canon regexp '(^| |\-)".str_replace('-','\\-',$str)."( |\-|\$)'";
				$sql.=" and (aa.statut & $statut = $statut)";
				$sql.=" order by aa.nom limit $cnt";
				$rs = sqlexec($sql);
			}
		}
	}

	$o = array();
	while ($row = mysql_fetch_assoc($rs))
		$o[] = '{"id":"'.$row['id'].'","tag":"'.$row['title'].'"}';

	header("Content-type: application/json; charset=iso-8859-1");
	header("X-Robots-Tag: noindex");
	echo "[".implode(",", $o)."]";
?>
