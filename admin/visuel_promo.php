<?
	require_once("secure.php");
	require_once("lib.php");

	function visuel_moveup($from, $to=null) {
		$sql="UPDATE visuel_promo SET position = position + 1"
			." WHERE 1 ";
		if ($from) $sql.=" AND position >= ".$from;
		if ($to) $sql.=" AND position < ".$to;
	//	echo $sql;
		sqlexec($sql);
	}
	function visuel_movedown($from, $to=null) {
		$sql="UPDATE visuel_promo SET position = position - 1"
			." WHERE 1";
		if ($from) $sql.=" AND position > ".$from;
		if ($to) $sql.=" AND position <= ".$to;
	//	echo $sql;
		sqlexec($sql);
	}

	// identifiant
	if((int) $_REQUEST['id']>0)
		$id = (int) $_REQUEST['id'];

	// suppression
	if($id && getarg("delete"))
	{
		$images = getsql("SELECT url_img FROM visuel_promo WHERE id = ".$id);
		foreach($images as $image)
		{
			if (!$image) continue;
			if(is_file('../'.$image))
				unlink('../'.$image);
		}
		sqlexec('DELETE FROM visuel_promo WHERE id = '.$id);
		unset($id);
		visuel_movedown( (int)$_POST['position']);
	}

	// modification, cr�ation
	if(getarg("update") || getarg("create"))
	{
		$count = getsql('SELECT COUNT(position) FROM visuel_promo');
		$position = (int) $_POST["position"];
		$old_position = (int) $_POST["old_position"];		
		// corriger la valeur de position si n�cessaire
	/* GEB: 2015-04-30 : on ne corrige pas la position automatiquement pour peremttre la coexistence v2/v3
		if($position) {
			if ($count == 0 && $position != 1)
				$position = 1;
			else if (!$old_position && $position > 1+$count)
				$position = $count + 1;
			else if ($old_position && $position > $count)
				$position = $count;
			$_POST['position'] = $position;
		}
		// modifier les autres positions
		if ($position && !$old_position)
			visuel_moveup($position, null);
		else if (!$position && $old_position)
			visuel_movedown($old_position, null);
		else if ($position < $old_position)
			visuel_moveup ($position, $old_position);
		else if ($position > $old_position)
			visuel_movedown ($old_position, $position);
	*/
		// enregistrer
		save_record("visuel_promo", $_POST);
		if(!$id)
			$id = mysql_insert_id();
		if(count($_FILES))
		{
			$sql = "UPDATE visuel_promo SET id = id";
			foreach ($_FILES as $key => $f)
			{
				if (preg_match('/\.(jpg|jpeg|gif|png|swf)$/i', $f['name'])) {
					$fichier = 'fichiers/visuel_promo/'.$id.'-'.str_replace('url_','',$key).strtolower(strrchr($f['name'],'.'));
					move_uploaded_file($f['tmp_name'], '../'.$fichier);
					$sql.= " ,".$key.' = "'.$fichier.'"';
				}
			}
			$sql.= ' WHERE id = '.$id;
			getsql($sql);
		}
	}
	if($id)
		$row = mysql_fetch_assoc(sqlexec("SELECT * FROM visuel_promo WHERE id = ".$id));
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script language="javascript" src="../scripts/check.js"></script>
	<script>
	<!-- //

	// doDelete
	function doDelete(frm)
	{
		return (confirm("Voulez-vous r�ellement supprimer cet �l�ment ?\nCette op�ration est irr�versible."));
	}

	// doSubmit
	function doSubmit(frm)
	{
		var err = "";
		if (!frm.position.value.length || !frm.position.value.match(/^\d{1,3}$/))
			err += "Vous devez indiquer une valeur num�rique pour la position!\n";
		else if (frm.position.value < 1)
			err += "Vous devez indiquer une valeur sup�rieure � 0 pour la position!\n";
		if(!ValidateSelect(frm.publie))
			err += "Vous devez indiquer la publication de l'image!\n";
		if (frm.url_img.value.length == 0)
			err += "Vous devez choisir une image en largeur!\n";
		if (frm.duree.value.length == 0 || !frm.duree.value.match(/^\d+([.,]\d+)*$/))
			err += "Vous devez indiquer la dur�e en secondes!\n";
		if(!ValidateSelect(frm.target))
			err += "Vous devez indiquer la cible !\n";
		//if (frm.href.value.length == 0)
		//	err += "Vous devez indiquer un lien!\n";
		if (frm.commentaire.value.length == 0)
			err += "Vous devez indiquer un commentaire!\n";
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		return true;
	}
	// -->
	</script>
</head>
<body>

<form action="" method="post" onsubmit="return doSubmit(this);" enctype="multipart/form-data">
<input type="hidden" name="id" value="<?=$id?>">
<input type="hidden" name="old_position" value="<?=$row['position']?>">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre">
		<img src="img/puce.gif" align="absmiddle" hspace="1">
		Visuel Promo
	</th>
</tr>
<tr>
	<td>
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="15%">
		<col width="32%">
		<col width="15%">
		<col width="32%">
		<tr>
			<td><label for="position">Position</label></td>
			<td><input type="text" maxlength="3" size="3" id="position" name="position" value="<?=$row['position']?>">
			<td><label for="publie">Publi�</label></td>
			<td>
				<? selectarray('publie', array('non','oui'), $row['publie'], '--'); ?>
			</td>
		</tr>
		<tr>
			<td>Promotion (en largeur)</td>
			<td colspan="2"><?=url_ctrl('url_img', $row['url_img'])?></td>
			<td>
				v2: (964 &times; 603), position &lt; 99<br/>
				v3: (1366 &times; 622), position &gt; 100<br/>
			</td>
		</tr>
		<tr>
			<td><label for="duree">Dur�e (sec)</label></td>
			<td><input type="text" id="duree" name="duree" value="<?=$row['duree']?>"></td>
			<td><label for="target">Cible</label></td>
			<td>
				<? selectarray('target', array('_blank'=>'_blank','_self'=>'_self'), $row['target'], '--'); ?>
			</td>
		</tr>
		<tr>
			<td><label for="href">Lien</label></td>
			<td><input type="text" id="href" name="href" value="<?=$row['href']?>"></td>
			<td><label for="commentaire">Commentaire</label></td>
			<td><input type="text" id="commentaire" name="commentaire" value="<?=$row['commentaire']?>"></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td colspan="3">doit commencer par "http://" pour un site externe, "/" pour un lien interne</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%">
		<tr>
			<td align="right">
				<input type="submit" name="<?=$id?"update":"create"?>" value="enregistrer" onclick="return doSubmit(this.form);">
			<?
				if($row)
					echo '<input type="submit" onclick="return doDelete(this.form);" name="delete" value="supprimer">';
			?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
</form>
</body>
</html>

