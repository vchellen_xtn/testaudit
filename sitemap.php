<?
	include('constant.php');
	
	// vérification des paramètres
	$f = $_GET['f'];
	if ($f == 'sitemap.xml')
		$f = (SERVER_NAME==SERVER_RESA ? 'sitemap_resa.xml' : 'sitemap_annu.xml');
	if (!preg_match('/^sitemap_[a-z\_]+.xml$/', $f) || !file_exists('cache/sitemap/'.$f))
	{
		return;
	}

	// on envie du XML compressé
	header('Content-Type: text/xml; charset=utf-8');
	readfile('cache/sitemap/'.$f);
?>