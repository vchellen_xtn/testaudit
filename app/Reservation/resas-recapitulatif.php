<?php
/**
* $this est Reservation
* $page page o� se trouve le bloc
* $kilometrages : tableau pour afficher une liste de param�trages
* $showTotal : afficher le total ou pas ?
* $showPrixOptions : affichr ou non le prix des options
*/
	/** @var Reservation */
	$reservation = $this;
	/** @var ResOption_Collection */
	$options = $reservation->getResOptions();
	/** @var Categorie */
	$categorie = $reservation->getCategorie();
	/** @var Agence */
	$agence = $reservation->getAgence();

	// d�terminer l'action du "retour"
	$action = 'resultat.html';
	if ($page == 'resas/inscription' && in_array(strtolower($reservation['type']), array('vu','vp')))
		$action = 'options.html';
?>
<!-- r�sum� de la r�servation -->
<form id="frm_resas_recapitulatif" name="reservation" method="post" action="<?=$action?>">
	<ul class="search-recap clearfix">
		<? if ($page != 'resas/resultat') : ?>
		<li>
			<input class="retour" type="submit" value="Retour"/>
		</li>
		<? endif; ?>
		<li class="fieldset--type-block size-30">
			<h2 class="title">Type de v�hicule</h2>
			<? echo Categorie::$TYPE_OAV[$reservation['type']];?> - <? echo $reservation->getCategorie(); ?><br/>
			<div class="container--img-recap">
				<img src="../<?=$reservation->getCategorie()->getImg('sml');?>" alt="" />
			</div>
		</li>
		<li class="fieldset--type-block size-40">
			<h2 class="title">Date de d�part et retour</h2>
			D�part le <?= $reservation->getDebut(); ?><br />
			Retour le <?= $reservation->getFin(); ?><br/>
			<?php
			// Dans le cas d'un locapass, on n'affiche aucun prix
			if (!$reservation->isLocapass()) {
			?>
			<? echo $reservation->getDuree(); ?>
			<? $franchise = $reservation->getMontantFranchise(); ?>
			<? if (!is_null($franchise)) : ?>
			<br/>Franchise <?=show_money($franchise);?> &euro;
			<? endif; ?>
			<br/><?=$reservation->getKmSupp('%s� le km supp.');?>
			<?php
			}
			?>
		</li>
		<li class="fieldset--type-block size-30">
			<h2 class="title">Kilom�trage</h2>
			<? if (is_array($kilometrages)) : ?>
				<br/>
				<? selectarray('l_distance', $kilometrages, $reservation['distance'], null, 'style="width: auto;"'); ?>
			<? elseif ($reservation->hasForfait()) : ?>
				<?=$reservation->getForfait()->getKm(); ?>
			<? elseif ($reservation['distance']) : ?>
				<?=$reservation['distance']; ?> km
			<? endif; ?>
		</li>
		<li>
			<a class="modifiez" href="index.html">Modifiez</a>
		</li>
	</ul>
	<?php
	foreach($reservation->getFields() as $k => $v)
	{
		if (!$v) continue;
		echo '<input type="hidden" name="'.$k.'" value="'.$v.'"/>';
	}
	if ($reservation->hasForfait()) {
		echo '<input type="hidden" name="forfait" value="'.$reservation->getForfait()->doSerialize().'"/>'."\n";
		echo '<input type="hidden" name="prix_forfait" value="'.$reservation->getForfait()->getPrix().'"/>'."\n";
		echo '<input type="hidden" name="unipro_vehicule" value="'.$reservation->getForfait()->getData('unipro_vehicule').'"/>'."\n";
		echo '<input type="hidden" name="operation" value="'.$reservation->getForfait()->getData('operation').'"/>'."\n";
	}
	$date_retrait = null;
	foreach($reservation->getResOptions() as $k => $option)
	{
		if ($option['quantite'])
			echo '<input type="hidden" name="options_u['.$k.']" value="'.$option['quantite'].'"/>';
		else
			echo '<input type="hidden" name="options[]" value="'.$k.'"/>';
		if (!$date_retrait && $option['retrait'])
			$date_retrait = $option['retrait'];
	}
	if ($date_retrait)
		echo '<input type="hidden" name="options_retrait" value="'.$date_retrait.'"/>';
?>
</form>

