<?
	$menu = array(
		'trouver-son-camion'	=> 'Choisir son camion<br />de déménagement',
		'materiel'				=> 'Prévoir son matériel<br />de déménagement',
		'memo'					=> 'Organiser<br />son déménagement',
		'homebox'				=> 'Louer<br />un garde meuble',
	);
	$action = basename($page);
	if(in_array($action, array('trouver-son-camion','choisir-son-camion','reserver', 'studio', '2-pieces', 'grand-volume')))
		$action = 'trouver-son-camion';
?>
<ul class="menu type-li-4">
	<? foreach($menu as $k => $lbl) : ?>
		<li <?if ($action==$k) echo ' class="current"'; ?>>
			<a href="<?=$k?>.html"<?if ($action==$k) echo ' class="current"'; ?>><?=$lbl;?></a>
		</li>
	<? endforeach; ?>
</ul>