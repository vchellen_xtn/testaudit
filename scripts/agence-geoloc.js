/**
 * Enrichissement de appADA avec les fonctionnalit�s de g�olocalisation
 * Responsable seulement du html de agence-geoloc.php, 
 * �met des $(appADA).trigger(**event**) pour le reste
 * 	showRechercheAgence		affiche la recherche d'agence
 * 	selectAgence			choix d'une agence dans la liste
 */
var appADA = appADA || {};
appADA.rechercheAgenceResults = {};
appADA.geolocReady = function() {
	google.load("maps", "3", {other_params:'sensor=false' + appADA.googleApiKey, callback: function() {}});
	// click sur le lien "Me g�olocaliser" dans le formulaire principal
	$('#trouvez_agence').click(function() {
		appADA.doSearch();
		$(appADA).trigger("showRechercheAgence");
		$('#block_recherche_agence').show();
		return false;
	});
	$('#liste_recherche_agence .btn, a.lien_agence').live('click', function() {
		$(appADA).trigger("selectAgence", [$(this).attr('data-id'), $(this).attr('data-nom')]);
		$('#block_recherche_agence').hide();
		return false;
	});
};
// initialise la Google Map
appADA.geolocInitMap = function() {
	appADA.doSearch();
};
// effectue la recherche
appADA.doSearch = function(search, p) {
	var args = {"method": "agences", "format": "json", "distance": 100, "limit": 10};
	var options = appADA.searchOptions;
	if (options) {
		$.extend(args, options);
	}
	if (search) {
		args.search = search;
	} else if ($('#ville').val().length) {
		args.search = $('#ville').val();
	} else if (p && p.latitude) {
		args.lat = deg2rad(p.latitude);
		args.lon = deg2rad(p.longitude);
	} else if (navigator.geolocation || (google.loader && google.loader.ClientLocation.longitude)) {
		appADA.doGeoLocation();
		return;
	} else {
		return false;
	}
	// lancer la recherche
	$.ajax({
		 url:'../api.php'
		,data: $.param(args)
		,dataType:'json'
		,success:function(json) {
			appADA.rechercheAgenceResults = json;
			appADA.rechercheDisplayList();
			appADA.rechercheMapLoaded();
			appADA.bodyScrollTo('#recherche');
		}
	});
};
// localisation par navigator.geolocation
appADA.doGeoLocation = function() {
	// sinon on recherche avec la g�olocalisation par le navigteur
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(
			function(position) {
				appADA.doSearch(null, position.coords);
			},
			function() {
				appADA.handleNoGeolocation(false);
			}
		);
	} else {
		// Browser doesn't support Geolocation
		appADA.handleNoGeolocation();
	}
};
// g�rer la localisation avec google.loader
appADA.handleNoGeolocation = function () {
	if (google.loader && google.loader.ClientLocation && google.loader.ClientLocation.longitude) {
		appADA.doSearch(null, google.loader.ClientLocation);
	}
};
// affiche la liste des agences
appADA.rechercheDisplayList = function () {
	var results = appADA.rechercheAgenceResults;
	if (!results.agences || !results.agences.length)
	{
		$('#liste_recherche_agence').html('<strong>Aucun r�sultat trouv�</strong>');
		return;
	}
	var agences = results.agences;
	$('#liste_recherche_agence').html('');
	for (var i = 0; i < agences.length; i++)
	{
		var texte = '<div class="recherche_agence" id="recherche_agence_'+agences[i].id+'">'+appADA.getHtmlAddress(agences[i])+'</div>';
		$('#liste_recherche_agence').append(texte);
	}
};
// affiche la carte GoogleMaps
appADA.rechercheMapLoaded = function () {
	$('#carte_recherche_agence').show();
	var mapOptions = {zoom: 5, mapTypeId: google.maps.MapTypeId.ROADMAP, scrollwheel: false, panControl: true, streetViewControl: false};
	var map = new google.maps.Map(document.getElementById('carte_recherche_agence'), mapOptions);
	var results = appADA.rechercheAgenceResults;
	if (!results || !results.agences || !results.agences.length)
	{
		map.setCenter(new google.maps.LatLng(46.22, 2.22), 5);
		return;
	}
	var center, agences = results.agences;

	// pr�parer les agences
	for (var i = 0; i < agences.length; i++)
	{
		agences[i].lat = rad2deg(agences[i].latitude);
		agences[i].lon = rad2deg(agences[i].longitude);
		agences[i].html = appADA.getHtmlAddress(agences[i], true);
	}
	// afficher la liste des agences
	if (results.center.latitude)
		center = {"point": new google.maps.LatLng(results.center.latitude, results.center.longitude), "title": results.search ? results.search : "Votre localisation"};
	var bounds = createMarkersFromAgences(map, center, agences, function(marker)
	{
		var agence_id = 'recherche_agence_' + marker.agence_id;
		$('.recherche_agence').removeClass('current').filter('#'+agence_id).addClass('current');
		// scroll dans la div
		$target = $('#'+agence_id);
		if ( $target && $target.length)
		{
			vposition = $('#liste_recherche_agence').scrollTop() + $target.position().top - $target.outerHeight();
			$('#liste_recherche_agence').animate({scrollTop: vposition}, 1000);
		}
	});
	map.setCenter(bounds.getCenter(), Math.min(map.fitBounds(bounds), 15));
};

appADA.getHtmlAddress = function (agence, court) {
	court = !!court;
	var output = '<' + 'a class="lien_agence" hr' + 'ef="'+agence.url_agence+'" data-id="'+agence.id+'" data-nom="'+agence.recherche +'">'+agence.nom+'<span class="end"></span></a>';
	if (agence.ortho) {
		output += '<span class="nb-km"> � ';
		if (agence.ortho < 1) {
			output += Math.round(agence.ortho * 1000) + "m";
		} else {
			output += (Math.round(agence.ortho * 10) / 10).toString().replace('.', ',') + "km";
		}
		output += "</span><br/>";
	}
	output += agence.adresse1 +'<br />'
		+( agence.adresse2 && agence.adresse2.length ? agence.adresse2+'<br />' : '' )
		+agence.cp+' '+agence.ville+'<br />';
	if ( court ) {
		return output;
	}
	if (agence.tel && agence.tel.length)
		output += 'T�l : '+agence.tel+'<br />';
	if (agence.fax && agence.fax.length)
		output += 'Fax : '+agence.fax+'<br />';
	output += '<' + 'a class="btn" hr' + 'ef="'+agence.url_resa+'" data-id="'+agence.id+'" data-nom="'+agence.recherche +'">choisir<span class="end"></span></a>';
	output += '<div class="clear"></div>';
	return output;
}
// peuvent rester dans le scope global
function deg2rad(deg) { return (deg *  Math.PI) / 180; }
function rad2deg(rad) { return (rad * 180) / Math.PI; }