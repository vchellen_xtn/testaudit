<?
	require_once('../constant.php');
	require_once("secure.php"); 
	require_once("lib.php");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script type="text/javascript" src="<?=Page::getProtocol()?>://www.google.com/jsapi"></script>
	<script type="text/javascript">
		google.load('jquery', '1.4');
	</script>
	<script type="text/javascript">
	// <![CDATA[
	// ]]>
	</script>
	<style type="text/css">
		select,input { width: inherit;}
		label { padding-left: 2px; }
		label.col1 { padding-left: 0; float: left; width: 65px;}
	</style>
</head>
<body>
<?
// afficher les satistiques
show_table('Partenaires : par type', 'select origine, sum(actif) actifs, count(*) partenaires from partenaire group by origine order by origine', array('origine'=>'stats_partenaire.php?origine=#origine#'));

// les ann�es limites
$toYear = date('Y');
$fromYear = $toYear - 1;

// afficher l'origine
$origine = $_GET['origine'];
if ($origine && Partenaire::$ORIGINES[$origine])
{
	// r�cup�rer le coupon si pr�cis�
	$coupon = (int) $_GET['coupon'];
	
	// afficher la liste des partenaires et des coupons
	$title = "Partenaires / Codes pour ".Partenaire::$ORIGINES[$origine];
	$href = array('code' => "stats_partenaire.php?origine=$origine&coupon=#coupon#", 'partenaire'=>'partenaire.php?id=#id#');
	$format = array();
	
	$sql = "select p.id, p.nom partenaire";
	$sql.=", pc.id coupon, concat(pc.code,coalesce(concat(' (',pc.locapass,')'),'')) code";
	$sql.=", pc.echeance, (case when p.actif * pc.actif then 'O' else 'N' end) actif";
	for($year=$toYear;$year>=$fromYear;$year--)
	{
		// nombre et CA des r�servations pour cette ann�e
		$sql.= ", sum(case when year(paiement)='$year' and statut='P' then 1 else 0 end) `$year (P)`";
		$sql.= ", sum(case when year(paiement)='$year' and statut='A' then 1 else 0 end) `$year (A)`";
		$sql.= ", sum(case when year(paiement)='$year' and forfait_type!='LOCAPASS' then total-ifnull(rembourse,0) else 0 end) `$year (CA)`";
		$format["$year (CA)"] = '%01.2f �';
	}
	$sql.=" from partenaire p\n";
	$sql.=" left join partenaire_coupon pc on pc.partenaire=p.id\n";
	$sql.=" left join reservation r on (r.coupon=pc.id and r.paiement >= '$fromYear-01-01')\n";
	$sql.=" where p.origine='".$origine."'\n";
	if ($coupon)
		$sql.=" and pc.id=$coupon\n";
	$sql.=" group by p.nom, p.actif, pc.actif, pc.echeance, pc.id, pc.code, pc.locapass";
	$sql.=" order by p.nom, pc.code\n";
//echo "<pre>$sql</pre>";
	// liste des partenaires / coupons
	show_table($title, $sql, $href, $format, array('coupon','id'));
	
	// afficher les donn�es pour un code
	if ($coupon)
	{
		// on affiche un bouton pour l'extraction des r�servations
		echo '<h2>Export des r�servations</h2>';
		echo '<form method="post" action="reservation_recherche.php">'."\n";
		echo '<input type="hidden" name="coupon" value="'.getsql("select code from partenaire_coupon where id=$coupon").'"/>'."\n";
		echo '<input type="hidden" name="date_from" value="01-01-'.$fromYear.'"/>'."\n";
		echo '<input type="hidden" name="date_to" value="31-12-'.$toYear.'"/>'."\n";
		echo '<input type="submit" name="search" value="toutes les r�sas"/>'."\n";
		echo '</form>'."\n";
		
		// pour chaque type de v�hicule
		// cat�gorie, nombre de r�sas, CA
		foreach(array('VP','VU','SP','EL') as $type)
		{
			$title = "$type : par cat�gorie";
			$format = array();
			
			$sql = "select c.mnem `cat�gorie`\n";
			for($year=$toYear;$year>=$fromYear;$year--)
			{
				// nombre et CA des r�servations pour cette ann�e
				$sql.= ", sum(case when year(paiement)='$year' and statut='P' then 1 else 0 end) `$year (P)`";
				$sql.= ", sum(case when year(paiement)='$year' and statut='A' then 1 else 0 end) `$year (A)`";
				$sql.= ", sum(case when year(paiement)='$year' and forfait_type!='LOCAPASS' then total-ifnull(rembourse,0) else 0 end) `$year (CA)`";
				$format["$year (CA)"] = '%01.2f �';
			}
			$sql.=" from reservation r\n";
			$sql.=" join categorie c on c.id=r.categorie\n";
			$sql.=" where r.coupon=$coupon and r.type='$type' and r.paiement>='$fromYear-01-01'\n";
			$sql.=" group by c.mnem, c.position";
			$sql.=" order by c.position";
			
			if (show_table($title, $sql, null, $format))
			{
				// r�partition par ann�e - mois
				$title = "$type : par mois";
				$format["CA"] = '%01.2f �';
				
				$sql = "select concat(year(r.paiement),'-',lpad(month(r.paiement), 2, '0')) mois\n";
				$sql.= ", sum(case when statut='P' then 1 else 0 end) `P`";
				$sql.= ", sum(case when statut='A' then 1 else 0 end) `A`";
				$sql.= ", sum(case when forfait_type!='LOCAPASS' then total-ifnull(rembourse,0) else 0 end) `CA`";
				$sql.=" from reservation r\n";
				$sql.=" where r.coupon=$coupon and r.type='$type' and r.paiement>='$fromYear-01-01'\n";
				$sql.=" group by year(r.paiement), month(r.paiement)";
				$sql.=" order by mois desc";
			
				show_table($title, $sql, null, $format);
			}
		}
	}
}
// sqlprofile();
?>
</body>
</html>