<?
class AgenceHoraire extends Ada_Object
{
	const DELTA_HOURS = 3;
	
	public function _construct()
	{
		$this->setIdFieldName('jour');
	}
	/**
	* Si une heure est dans les heures d'ouverture
	* 
	* @param string $h HH:MM 
	* @returns bool
	*/
	public function isOpen($h)
	{
		if ($h >= $this->_data['ouverture'] && $h <= $this->_data['fermeture'])
		{
			if ($this->_data['pause_debut'] && $this->_data['pause_fin'] && $this->_data['pause_debut']!=$this->_data['pause_fin'])
			{
				if ($h > $this->_data['pause_debut'] && $h < $this->_data['pause_fin'])
					return false;
			}
			return true;
		}
		return false;
	}
	/**
	* Renvoie un horaire "amn�ag�"
	* 
	* @param string $h	HH:mm
	* @param string $sens null, depart ou retour
	*/
	public function adjustTime($h, $sens)
	{
		// si c'est ferm�, on arr�te l�
		if ($this->isClosed())
			return null;
		// si c'est ouvert aux heures indiqu�s, on arr�te l�
		if ($this->isOpen($h))
			return $h;
		// si c'est avatn l'heure d'ouveture on la renvoie
		if ($h < $this->_data['ouverture'])
			return $this->_data['ouverture'];
		// si c'est apr�s l'heure de fermeture on la renvoie
		if ($h > $this->_data['fermeture'])
			return $this->_data['fermeture'];
		// sinon on est dans la pause et cela d�pen du sens
		if ($sens == 'depart')
			return $this->_data['pause_debut'];
		return $this->_data['pause_fin'];
	}
	/**
	* Est-ce ouvret ce jour-l� ?
	* @param int $hour optionnel - indique si l'agence est ouverte � cette heure-l�, si avant ouverture et � plus de self::DELTA_HOURS on consid�re que c'est ferm�
	* @returns bool
	*/
	public function isClosed($hour = null)
	{
		if (!$this->hasData('ouverture'))
			return true;
		if ($hour && $hour < (int)$this->_data['ouverture'] && ($hour + self::DELTA_HOURS) < (int)$this->_data['ouverture']) {
			return true;
		}
		return false;
	}
}
?>