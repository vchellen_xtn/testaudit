<?php
class Transaction_Locapass extends Transaction
{
	
	// pour le Locapass, le paiement est toujours accept�
	protected function _doPayment(Reservation $reservation, $args)
	{
		if (!$reservation->isLocapass())
		{
			$this->_addMessage("La r�servation n'est pas associ�e � un LOCAPASS");
			return false;
		}
		return true;
	}
	// et l'annulation �galement
	protected function _doRefund(Reservation $reservation, $montant)
	{
		if (!$reservation->isLocapass())
		{
			$this->_addMessage("La r�servation n'est pas associ�e � un LOCAPASS");
			return false;
		}
		return true;
	}
	protected function _getModalitesRemboursementTotal()
	{
		return "Votre r�servation LOCAPASS a bien �t� annul�e.";
	}
	protected function _getModalitesRemboursementPartiel($rembourse)
	{
		return '';
	}
}
?>
