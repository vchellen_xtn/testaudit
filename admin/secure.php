<?
	session_start();
	require_once(dirname(dirname(__file__)).'/constant.php');
	require_once(BP.'/lib/lib.sql.php');
	require_once(BP.'/lib/lib.util.php');
	
	// forcer le passage en HTTPS si posible
	if (SERVER_NAME!=SERVER_ANNU && USE_HTTPS && $_SERVER['HTTPS']!='on')
	{
		header('Location: https://'.SERVER_NAME.$_SERVER['SCRIPT_NAME'].($_SERVER['QUERY_STRING'] ? '?'.$_SERVER['QUERY_STRING'] : ''));
	}
	
	$adminUser = Acces::createFromCookie($_COOKIE['ADA001_ADMIN_AUTH']);
	if (!$adminUser)
	{
		$url = 'login.php?url='.urlencode($_SERVER['SCRIPT_URL'].'&'.$_SERVER['QUERY_STRING']);
		header('location: '.$url);
		exit();
	}
	if ($_SESSION['ADA001_ADMIN_ZONE'] != $adminUser->getCurrentZoneID())
	{
		$_SESSION['ADA001_ADMIN_ZONE'] = $adminUser->getCurrentZoneID();
	}
	// on v�rifie que la page est bien auoris�e pour ce r�le
	include_once(dirname(__file__).'/secure.pages.php');
	$p = basename($_SERVER['SCRIPT_NAME']);
	if (!$adminUser->hasRights($ACCES_PAGES[$p])) {
		die("Vous n'avez pas le droit d'acc�der � cette fonctionnalit� !");
	}
?>