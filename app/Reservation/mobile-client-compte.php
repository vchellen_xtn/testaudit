<?php
/*
Usage:
	$reservation->toHtml('mobile-client-compte', array('compteurReservation' => 1, 'infoClient' => ''));
*/
	/** @var Reservation */ $reservation = $this;
	$compteurReservation;	// compteur pour affichage de la r�servation dans une liste
	$infoClient;			// afficher une information client
?>
<div class="container-booking">
	<div class="row type-table title-booking">
		<div class="col-auto first"><?=$compteurReservation?></div>
		<div class="col-auto second">
			<span class="semi-bold">R�servation n�<?=$reservation->getResIDClient();?></span> - <?=$reservation->getStatut();?>
			<br />
			<span class="red">D�part: <?=$reservation->getDebut('d/m/Y H:i')?></span>
			<?  if (!empty($infoClient)) : ?>
			<br />
			<span class="red"><?=$infoClient; ?></span>
			<? endif; ?>
		</div>
	</div>
	
	<table class="detail-booking">
		<tr>
			<th>Votre v�hicule</th>
			<td>
				<?=$reservation['categorie_nom']?><br />
				<?=$reservation['categorie_description']?>
			</td>
		</tr>
		<tr>
			<th>Agence ADA</th>
			<td><?=$reservation['agence_nom']?></td>
		</tr>
		<tr>
			<th>Dur�e</th>
			<td><?=$reservation->getDuree()?></td>
		</tr>
		<tr>
			<th>Km inclus</th>
			<td><?if ($reservation['forfait_type']!='LOCAPASS') echo (is_null($reservation['km']) ? 'km illimit�' : $reservation['km'].' km')?></td>
		</tr>
		<tr>
			<th>Tarifs TTC</th>
			<td><?if ($reservation['forfait_type']!='LOCAPASS') echo $reservation['total'].' &euro;'?></td>
		</tr>
	</table>
</div>

<div class="row">
	<ul class="inner-menu">
		<?php foreach($reservation->getLinks() as $link) : ?>
		<li><a href="<?=$link['href'];?>"><?=$link['title'];?></a></li>
		<?php endforeach; ?>
	</ul>
</div>

