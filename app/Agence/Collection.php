<?

class Agence_Collection extends Ada_Collection
{
	// modes d'affichage correspondant aux diff�rents SELECT
	const MODE_AGENCES_LISTE = 1;
	const MODE_RESERVATION_INDEX = 2;
	
	// champs sp�cifiques � la collection
	protected $_libelle;
	protected $_search;
	protected $_msg;
	protected $_promoInfo = false;	 // contr�le le chargement des informations sur les promotions
	protected $_afterLoad = false;	//  contr�le l'ex�cution des enrichisssements apr�s le chargement SQL
	
	/** @var Ada_Object  : autour de quoi est la liste */
	protected $_what = null;
	
	// pour l'affichage de Google Map
	protected $_center = array(); // (longitude, latitude) en degr�s
	protected $_zoom = null;
	protected $_polygon = null;
	protected $_showCenter  = false;
	

	/**
	* Cr�e une collection en fonction des arguments pass�s
	* Les param�tres pouvant �tre pass� dans $args
	* 	zone			les agences d'une zone
	* 	agence			les agences autour d'une autre agence
	* 	code_societe	les agences d'une soci�t�
	* 	emplacement		toutes les agences pour un emplacement (Gare, A�roport)
	* 	agglo			toutes les agences d'une agglom�ration
	* 	cp				recherche � partir d'un code postal
	*	dept			recherche � partir d'un d�partement
	* 	lat, lon		recherche autour d'un point latitude, longitude en radians
	* 	ville			recherche sur le nom de la ville
	* 	search			recherche en texte int�grale
	* 	malin			restriction aux agences � l'heure
	* 	statut			filtre sur le agence.statut (int) Agence::STATUT_XX
	* 	vehicules		selon type de v�hicule : Agence::TYPE_XX
	* 	limit			nombre de r�sultats attendues
	* 	distance		� quelle distance recherche-t-on ?
	* 	zone_limit		limiter la recherche � une zone
	* 	reseau			limiter � agence.reseau (ADA, CITER)
	* 
	* @param array $args
	* @return Agence_Collection
	*/
	static public function factory($args, $mode = self::MODE_AGENCES_LISTE, $promoInfo = true)
	{
		// on cr�e la collection qui sera retourn�e
		$agences = new Agence_Collection();
		$agences->_promoInfo = $promoInfo;
		$agences->_afterLoad = true;
		// on pr�pare les arguemnts
		foreach (array('cp','dept','ville','search') as $k)
		{
			if (!$args[$k]) continue;
			$search = strip_tags(trim($args[$k]));
			break;
		}
		$agences->_setSearch($search);
		$malin = array_key_exists('malin', $args);
		$statut = ((int)$args['statut'] > 0  ? (int) $args['statut'] : Agence::STATUT_VISIBLE);
		
		// limite et distance
		$limit = min(($args['limit'] ? (int) $args['limit'] : 10), 50);
		$distance = min(($args['distance'] ? (int) $args['distance'] : 70), 70);
		
		// on g�re le cas de la recherche � partir d'une agence
		if (!$search)
		{
			if ($args['agence'] && Agence::isValidID($args['agence']))
				$around = Agence::factory($args['agence']);
			else if ($args['emplacement'])
				$emplacement = AgenceEmplacement::factory($args['emplacement']);
			else if ($args['agglo'])
				$agglo = AgenceAgglo::factory($args['agglo']);
		}


		// SQL pour les agences
		$fld_agence = 'aa.id, aa.pdv, aa.agence, a.reseau, a.code_societe, a.code_base, a.code_groupe, a.type, a.livraison_km, aa.nom, ap.nom nom_pdv, aa.canon, aa.recherche, aa.emplacement, ap.j7, ap.adresse1, ap.adresse2, ap.cp, ap.ville, aa.zone, a.tel, a.fax, a.statut, ap.latitude, ap.longitude';
		$fld_agence.=", ap.satisfaction_reponses, ap.satisfaction_vehicule, ap.satisfaction_commentaires, ap.satisfaction_agence";
		$sql_agence ="\n from agence a \n join agence_pdv ap on (ap.agence=a.id and ap.publie=1) \n join agence_alias aa on aa.pdv=ap.id ";
		if ($around)
			$sql_agence.="\n join agence_distance ad on ad.autour=aa.id";
		$sql_agence.="\n where (aa.statut & $statut) = $statut and aa.pageweb=1";
		if (isset($args['vehicules']) && preg_match('/^\d+$/', $args['vehicules'])) {
			$sql_agence.=' and (a.type & '.(int)$args['vehicules'].') = '.(int)$args['vehicules'];
		}
		if (in_array($args['reseau'], array('ADA','CITER'))) // // limiter � un r�seau
			$sql_agence.=" and a.reseau='".$args['reseau']."'";
		if (preg_match('/^[a-z]{2}$/', $args['zone_limit'])) // limiter � une zone
			$sql_agence.=" and aa.zone='".$args['zone_limit']."'";
		$sql_pdv    = "\n AND aa.pdv=aa.id"; // montrer les points de vente uniquement
		if ($malin)
			$sql_agence.= ' and (a.type & '.Agence::TYPE_ML.') = '.Agence::TYPE_ML;

		// rechercher autour d'une agence
		if ($around)
		{
			// construire le SQL
			$fld_agence .= ", ad.distance ortho";
			$sql = 'select '.$fld_agence;
			$sql_agence .= " AND ad.agence='".$around['pdv']."'";
			$sql.= $sql_agence.$sql_pdv;
			if ($around['zone']=='fr') {
				$sql.= "\nHAVING ortho < $distance";
			}
			$sql.= "\n ORDER BY distance";
			$sql.= " LIMIT $limit";

			// construire la collection
			return $agences->_setWhat($around)
							->_loadFromSQL($sql);
		}
		else if ($emplacement)
		{
			$agences->_setWhat($emplacement);
			$sql = $sql_agence." AND aa.zone IN ('fr','co') AND aa.emplacement=".$emplacement['id'];
		}
		else if ($agglo)
		{
			$agences->_setWhat($agglo);
			$sql = $sql_agence." AND aa.zone IN ('fr','co') AND (aa.ville='".addslashes($agglo['nom'])."' OR aa.agglo='".addslashes($agglo['nom'])."')";
		}
		// d�partement, code postal, ville agence, ville en france puis adresse
		else if (preg_match('/^[0-9]{2}$/', $search) || preg_match('/^2[AB]$/i',$search))
		{
			$dept = Dept::factory($search);
			$agences->_setWhat($dept);
			if (in_array($search, array('20','2A','2B')))
				$sql = $sql_agence." AND (aa.zone='co' OR (aa.zone='fr' AND LEFT(aa.dept,2) IN ('20','2A','2B')) )\n";
			else
			{
				$sql = $sql_agence." AND aa.zone IN ('fr','co') AND aa.dept = '".addslashes($search)."'";
				$agences->_setPolygon($dept['id']);
			}
		}
		//else if (preg_match('/^[0-9]{5}$/', $search))
		//	$sql = $sql_agence." AND aa.zone IN ('fr','co') AND aa.cp='".addslashes($search)."'".$sql_pdv;
		else if ($search && !preg_match('/^[0-9]{5}$/', $search))
			$sql = $sql_agence." AND aa.zone IN ('fr','co') AND ".where_fulltext('aa.nom, aa.ville', $search).$sql_pdv;
		else if ($zoneID = strip_tags(trim($args['zone'])))
		{
			$zone = Zone::factory($zoneID);
			$agences->_setWhat($zone);
			if ($zoneID == 'co')
				$sql = $sql_agence." AND (aa.zone='co' OR (aa.zone='fr' AND LEFT(aa.dept,2) IN ('20','2A','2B')))";
			else
				$sql = $sql_agence." AND aa.zone='".addslashes($zoneID)."'";
		}
		else if ($code_societe = filter_var($args['code_societe'], FILTER_SANITIZE_STRING))
		{
			if (preg_match('/^[A-Z0-9]{2}$/', $code_societe))
				$sql = $sql_agence." AND a.code_societe='".addslashes($code_societe)."'";
		}
		// on recherche les agences avec ces premi�res possibilit�s
		if ($sql)
			$agences->_loadFromSQL('select distinct '.$fld_agence.$sql."\n ORDER BY aa.nom");
		// si on a rien trouv� on tente autre chose
		if ($agences->isEmpty())
		{
			if (isset($args['lat']) && isset($args['lon'])
			   && preg_match('/^\-?\d+(\.\d+)?$/', $args['lat'])
			   && preg_match('/^\-?\d+(\.\d+)?$/', $args['lon'])
			)
			{
				$longitude = $args['lon']; 
				$latitude = $args['lat'];
				$agences->_setLibelle('pr�s de votre position'); //.round(rad2deg($latitude),2).', '.round(rad2deg($longitude), 2));
				$agences->_showCenter = true;
			}
			else if ($search && strlen($search) > 2)
			{
				// on recherche dans la table des villes
				$str = strtoupper(str_replace(array('-',"'",'"'), ' ', $search));
				$row = mysql_fetch_assoc(sqlexec("select nom, cp, latitude, longitude from ville where '".addslashes($str)."' IN (cp, REPLACE(REPLACE(UCASE(nom),'-',' '),'\'', ' '))"));
				if ($row)
				{
					//echo '<pre>Ville: '; print_r($row); echo '</pre>';
					$agences->_setLibelle($row['nom'].' ('.$row['cp'].')');
					$longitude = $row['longitude']; $latitude = $row['latitude'];
				}
				else
				{
					// on recherche par GoogleMaps
					$x = Geocoding::factory(utf8_encode($str));
					if ($x->isOK())
					{
						$locatinoType = $x->getLocation($latitude, $longitude);
						//echo '<pre>GMap: '; echo"$search | $longitude | $latitude"; echo '</pre>';
						$longitude = deg2rad($longitude); $latitude = deg2rad($latitude);
						$agences->_setLibelle('')
								->_setSearch(utf8_decode($x->getFormattedAddress()));
						$agences->_showCenter = true;
					}
				}
			}
			if ($latitude && $longitude)
			{
				$fld_agence .= ", 6378 * acos(cos($latitude) * cos(ap.latitude) * cos(ap.longitude - $longitude) + sin($latitude) * sin(ap.latitude)) as ortho";
				$sql = 'select '.$fld_agence;
				$sql.= $sql_agence.$sql_pdv;
				//$sql.= "  and ap.zone IN ('fr','co')\n";
				$sql.= "  and (ap.longitude is not null";
				if (preg_match('/^[0-9]{5}$/', $search))
					$sql.=" or aa.cp='".$search."'";
				$sql.= ")\n";
				$sql.=" HAVING ortho < $distance\n";
				$sql.= " ORDER BY ortho";
				$sql.= " LIMIT $limit";

				$agences->_setCenter(rad2deg($longitude), rad2deg($latitude))
						->_setZoom(11)
						->_loadFromSQL($sql);
			}
		}
		if ($agences->isEmpty())
			$agences->_setMsg("Il n'y a pas d'agence correspondant � votre recherche : &laquo;".$search."&raquo;");
		return $agences;
	}
	/* =============================
	**	FONCTIONS MEMBRES	
	============================= */
	/**
	* Indique l'origine de la liste
	* 
	* @param Ada_Object $what (Agence, Dept, Zone)
	* @returns Agence_Collection
	*/
	protected function _setWhat(Ada_Object $what)
	{
		$this->_what = $what;
		if ($what['longitude'] && $what['latitude'])
			$this->_setCenter(rad2deg($what['longitude']), rad2deg($what['latitude']));
		$this->_setSearch($what['nom']);
		$this->_setLibelle($what['nom']);
		if ($what['zoom'])
			$this->_setZoom($what['zoom']);
		return $this;
	}
	/** @returns Ada_Object */
	public function getWhat() { return $this->_what; }
	/**
	* put your comment there...
	* 
	* @param string $libelle
	* @return Agence_Collection
	*/
	protected function _setLibelle($libelle) { $this->_libelle = $libelle; return $this; }
	public function getLibelle()
	{ 
		return $this->_libelle;
	}
	/**
	* @param mixed $search
	* @return Agences_Collection
	*/
	protected function _setSearch($search) { $this->_search = $search; return $this; }
	public function getSearch() { return $this->_search; }
	/** @param string $msg @return Agences_Collection */
	protected function _setMsg($msg) { $this->_msg = $msg; return $this; }
	public function getMsg() { return $this->_msg; }
	/**
	* Initialise le centre en degr�es
	* 
	* @param double $longitude
	* @param double $latitude
	* @return Agences_Collection
	*/
	protected function _setCenter($longitude, $latitude) { 
		$this->_center = array('longitude' => (string) $longitude, 'latitude' => (string) $latitude); 
		return $this;
	}
	public function getCenter() { return $this->_center; }
	public function getCenterX() { return $this->_center['longitude']; }
	public function getCenterY() { return $this->_center['latitude']; }
	/**
	* @param int $zoom
	* @return Agences_Collection
	*/
	protected function _setZoom($zoom) { $this->_zoom = $zoom; return $this; }
	public function getZoom() { return $this->_zoom; }
	/**
	* @param string $polygon
	* @return Agences_Collection
	*/
	protected function _setPolygon($polygon) { $this->_polygon = $polygon; return $this; }
	public function getPolygon() { return $this->_polygon; }
	// showCenter
	public function showCenter() { return $this->_showCenter; }
	/**
	* Enrichit les donn�es des agences
	* 	- zoom dynamique
	* 	- url pour chaque agence
	* 	- promotions ?
	*/
	protected function _afterLoad()
	{
		if (!$this->_afterLoad) return $this;
		// si on affiche un plan ?
		$useMap = true; //!empty($this->_center);
		
		// faire la liste des agences avec une promotion
		if ($this->count() && $this->_promoInfo)
		{
			$promo = array();
			$rs = sqlexec('SELECT pf.agence, count(pf.id) nb_promo FROM agence_promo_forfait pf LEFT JOIN agence_config ac ON ac.agence=pf.code_societe  WHERE '.PromotionForfait_Collection::createSQLCondition(array('agence' => $this->getIds())).' GROUP BY pf.agence');
			if (mysql_num_rows($rs))
			{
				while ($row = mysql_fetch_assoc($rs))
					$promo[$row['agence']] = $row['nb_promo'];
			}
		}

		// 1er parcours
		$max_ortho = 0;
		foreach ($this->_items as /** @var Agence */ $item)
		{
			// on cache les coordonn�es des partenaires
			$item->hidePartner();
			
			// les urls
			$item['url_resa'] = $item->getURLResa();
			$item['url_agence'] = $item->getURLAgence();

			// pour les promotions
			if ($nb = $promo[$item['pdv']])
				$item['nb_promo'] = $nb;

			// si on affiche un plan
			if ($useMap)
			{
				$item['lat'] = rad2deg($item['latitude']);
				$item['lon'] = rad2deg($item['longitude']);
				
				$html  = '<a href="'.$item['url_agence'].'"><strong class="green">'.$item["nom"].'</strong></a><br />';
				$html .= '<div style="margin: 0 0 15px 15px;">';
				$html .= $item->getHtmlAddress('<br/>', null, 'fax');
				//$html .= '<br/><a href="'.$item['url_agence'].'">&raquo; En savoir plus</a>';
				$item['html'] = $html;
			}
			
			// et la plus grande distance
			$ortho = $item['ortho'];
			if ($ortho > $max_ortho)
				$max_ortho = $ortho;
		}
		// calcule le zoom � partir du max_ortho
		if ($max_ortho)
		{
			if ($max_ortho < 5)
				$zoom = 12;
			else if ($max_ortho < 10)
				$zoom = 11;
			else if ($max_ortho < 20)
				$zoom = 10;
			else if ($max_ortho < 40)
				$zoom = 9;
			else if ($max_ortho < 80)
				$zoom = 8;
			else
				$zoom = 7;
			$this->_setZoom($zoom);
		}
		return $this;
	}
	/**
	* Convert object attributes to JSON
	*
	* @param  array $arrAttributes array of required attributes
	* @return string
	*/
	protected function __toJson(array $arrAttributes = array())
	{
		$json = array();
		$json['center'] = $this->getCenter();
		$json['search'] = u8($this->getSearch());
		foreach ($this->_items as /** @var Ada_Object */ $item)
			$json['agences'][] = array_map('u8', $item->toArray($arrAttributes));
		return json_encode($json);
	}
}
?>
