<?
/**
Dans $_SESSION
	'api_offres' = array(
		'time'	=> array
		(
			'last_request' => time,
			'nb_requests'  => int
		),
	);
*/
class ApiToken {
	const SESSION_KEY = 'api_token'; // nom de la cl� dans la session
	const MAX_REQUESTS = 60; // nombre de requ�tes au maximum
	const MAX_REQUESTS_BY_MINUTES = 10; // nombre maximum de requ�tes en moyenne par minute
	const MAX_TOKENS = 2;	 // nombre maximum de token distribu� pour cette session (
	const MAX_TIMEOUT = 10; // nombre de minutes de validit� du token

	/* $_SERVER['REMOTE_ADDR'] */
	static public function getToken($ip) {
		if ($ip == '::1') $ip = '127.0.0.1';
		$t = time();
		$h = dechex($t);
		$token = ($h.dechex(ip2long($ip)).md5($h.$ip.PRIVATE_KEY));
		// on enregistre dans la session
		if (!is_array($_SESSION[self::SESSION_KEY])) {
			$_SESSION[self::SESSION_KEY] = array();
		} else if (count($_SESSION[self::SESSION_KEY]) >= self::MAX_TOKENS) {
			// on ne garde que derniers
			$_SESSION[self::SESSION_KEY] = array_slice($_SESSION[self::SESSION_KEY], 1 - self::MAX_TOKENS, null, true);
		}
		$_SESSION[self::SESSION_KEY][$t] = array(
			'last_request' => $t,
			'nb_requests' => 0,
		);
		return $token;
	}
	static public function isThatTokenValid($token, $ip) {
		if ($ip == '::1') $ip = '127.0.0.1';
		// on v�rifie le token
		if (($h = substr($token, 0, 8)) && ($ip == long2ip(hexdec(substr($token, 8, 8)))) && substr($token, 16) == md5($h.$ip.PRIVATE_KEY))
		{
			// on r�cup�re le temps initial
			$t = hexdec($h);
			// on v�rifie si c'est un token "attendu"
			if (!isset($_SESSION[self::SESSION_KEY][$t])) return false;
			$current = time();
			
			// si le nombre de requ�tes maximum est d�pass�
			if ($_SESSION[self::SESSION_KEY][$t]['nb_requests'] >= self::MAX_REQUESTS) {
				unset($_SESSION[self::SESSION_KEY][$t]);
				return false;
			}
			// si le temps est d�pass�, on arr�te l�...
			if ($current > ($t + self::MAX_TIMEOUT * 60)) {
				unset($_SESSION[self::SESSION_KEY][$t]);
				return false;
			}
			// on note la derni�re requ�te et on renvoie
			$_SESSION[self::SESSION_KEY][$t]['last_request'] = $current;
			$_SESSION[self::SESSION_KEY][$t]['nb_requests']++;
			$nb_requests = $_SESSION[self::SESSION_KEY][$t]['nb_requests'];
			// les premi�res on ne tient pas compte du temps
			if ($nb_requests > 3) {
				// puis on v�rifie que le rythme de requ�tes n'est pas trop �lev�
				if ($nb_requests * 60 > (self::MAX_REQUESTS_BY_MINUTES * ($current - $t)) ) {
					// et s'il est vraiment trop �lev� on invalide le token
					if ($nb_requests * 60 > (3 * self::MAX_REQUESTS_BY_MINUTES * ($current - $t)) ) {
						unset($_SESSION[self::SESSION_KEY][$t]);
					}
					return false;
				}
			}
			return true;
		}
		return false;
	}

};
?>