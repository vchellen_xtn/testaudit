<?
	require_once('../constant.php');
	require_once("secure.php"); 
	require_once("lib.php");
	if (!$adminUser->hasRights(Acces::SB_ADMIN) && !$adminUser->hasRights(Acces::SB_EDITO))
		die("Vous n'avez pas les droits pour voir cette information !");

	// lien et format
	$href = array();
	$format = array();
	if (!count($_GET)) $_GET['zone'] = '';
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script type="text/javascript" src="<?=Page::getProtocol()?>://www.google.com/jsapi"></script>
	<script type="text/javascript">
		google.load('jquery', '1.4');
	</script>
	<script type="text/javascript">
	// <![CDATA[
	// ]]>
	</script>
	<style type="text/css">
		select,input { width: inherit;}
		label { padding-left: 2px; }
		label.col1 { padding-left: 0; float: left; width: 65px;}
	</style>
</head>
<body>
<h1>Textes R�f Nat</h1>
<p>
<?
	$tbl = null;
	foreach(RefnatCanon::$TYPES as $k => $info)
	{
		if (isset($_GET[$k]))
		{
			$tbl = $k;
			$mode = $info;
			$h1 = $info['title'];
			echo '<strong>';
		}
		else
			echo '<a href="'.basename(__file__).'?'.$k.'">';
		echo $info['title'];
		echo (isset($_GET[$k]) ? '</strong>' : '</a>').str_repeat('&nbsp;', 4);
	}
?>
</p>
<?
if ($mode)
{
	$href = array('id' => "refnat.php?tbl=$tbl&id=#id#");
	if (RefnatCanon::isGeographic($tbl))
		$href['canon'] = '../location-voiture-#canon#.html';
	$sql = "select id, nom, canon";
	$sql.=", case when refnat is null then '' else 'X' end texte";
	if ($mode['footer'])
		$sql.=", case when footer is null then '' else 'X' end footer";
	$sql.=" from ".$mode['tbl'];
	if (isset($mode['cond']))
		$sql.=' where '.$mode['cond'];
	$sql.=" order by id";
	show_table($h1, $sql, $href);
}
?>
</body>
</html>