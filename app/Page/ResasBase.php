<?php
/**
* Classe utilis�e par Page_Resas et Page_Resas_Admin pour assurer l'authentification
*/

class Page_ResasBase extends Page
{
	/** @var AccesAgence */
	protected $_resasUser = null;
	/** @var Agence */
	protected $_agence;
	
	/** Renvoie si la page a besoin de HTTPS @returns bool */
	protected function isThatPageNeedsHttps()
	{
		return true;
	}

	
	/**
	* V�rifier les param�tres et l'identification
	* 
	*/
	protected function checkParameters()
	{
		// se d�connecter de /resas/ ... mais pas de l'admin
		if (isset($_GET['logout'])) {
			$this->setResasUser(null);
		}
		// les v�rifications de Page
		if (!parent::checkParameters())
			return false;

		// est ce que l'utilisateur est associ�e � une agence ?
		// s'il y a un param�tre agence, on le valide
		if (isset($_REQUEST['agence']) && isset($_REQUEST['login']) && isset($_REQUEST['role']) && isset($_REQUEST['key'])) {
			$msg = '';
			$this->setResasUser(AccesAgence::create($_REQUEST['role'], $_REQUEST['login'], $_REQUEST['agence'], $_REQUEST['key'], $msg));
			if ($msg) {
				$this->_addMessage($msg);
			}
		} else if (isset($_COOKIE['ADA001_RESAS_AGENCE'])) {
			$this->setResasUser(AccesAgence::createFromCookie($_COOKIE['ADA001_RESAS_AGENCE']));
		}
		// si l'utilisateur est connect� on arr�te l�
		if ($this->_resasUser) {
			// on v�rifie que l'OAV est ouverte pour cette agence
			if ($this->_agence) {
				return true;
			} else {
				$this->_addMessage("Votre agence n'est pas autoris� � acc�der � l'OAV !");
			}
		}

		// sinon est-il identifi� dans l'admin ?
		if (!$this->_adminUser)
		{
			// sinon il faut s'authentifier
			$this->setId('resas/login');
			return false;
		}
		// OK on est conect� et il faut choisir l'agence
		$this->setId('resas/agence');
		return false;
	}
	
	/**
	* Renvoie l'agence courante
	* 
	*/
	public function getAgence()
	{
		return $this->_agence;
	}
	/**
	* Essaie de changer d'agence pour l'utilisateur courant
	* 
	* @param string $agenceID
	* @param string $msg
	* @returns bool
	*/
	protected function changeAgence($agenceID, &$msg) {
		if ($agenceID && Agence::isValidID($agenceID)) {
			// il faut valider le login, le r�le et l'agence
			$agence = '';
			$adminUser = $this->getAdminUser();
			$resasUser = $this->getResasUser();
			if ($adminUser && ($adminUser->hasRights(Acces::SB_ADMIN) || $adminUser->hasRights(Acces::SB_TC))) {
				$agence = $agenceID;
			} else if ($resasUser && $resasUser->hasRightsOnAgence($agenceID) && $resasUser->getConfigData('oav_acces', 'agence', $agenceID)) {
				$agence = $agenceID;
			} else {
				$msg = "Vous n'avez pas le droit d'acc�der � cette agence.";
			}
			// d�terminer le login et le r�le
			$login = ($resasUser ? $resasUser['login'] : $adminUser['login']);
			$role = '';
			if ($resasUser) {
				$role = $resasUser->getRole();
			} else if ($adminUser->hasRights(Acces::SB_ADMIN)) {
				$role = 'admin';
			} else if ($adminUser->hasRights(Acces::SB_TC)) {
				$role = 'comptoir';
			} else {
				$msg = "Vous n'avez pas le droit d'acc�der � cet outil.";
			}
			if ($login && $role && $agence) {
				if ($x = AccesAgence::create($role, $login, $agence, AccesAgence::getKey($role, $login, $agence), $msg)) {
					$this->setResasUser($x);
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	* Renvoie l'utilisateur courant
	* @returns AccesAgence
	*/
	public function getResasUser() {
		return $this->_resasUser;
	}
	/**
	* Change le ResasUser courant
	* 
	* @param AccesAgence $resasUser
	* @return Page_ResasBase
	*/
	public function setResasUser(AccesAgence $resasUser = null) {
		$this->_resasUser = $resasUser;
		if ($this->_resasUser) {
			self::setCookie('ADA001_RESAS_AGENCE', $this->_resasUser->getCookie());
			if ($this->_adminUser || $this->_resasUser->getConfigData('oav_acces')) {
				$this->_agence = Agence::factory($this->_resasUser['pdv']);
			}
		} else {
			self::setCookie('ADA001_RESAS_AGENCE', '', time() - 24*3600);
		}
		return $this;
	}
}
?>
