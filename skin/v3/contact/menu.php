<?
	$menu = array(
		'index'			=> 'Nous contacter',
		'recommandez'	=> 'Recommandez ce site'
	);
	$action = basename($page);
?>
<ul class="menu type-li-2">
	<? foreach($menu as $k => $lbl) : ?>
		<li <?if ($action==$k) echo ' class="current"'; ?>>
			<a href="<?=$k?>.html"<?if ($action==$k) echo ' class="current"'; ?>><?=$lbl;?></a>
		</li>
	<? endforeach; ?>
</ul>