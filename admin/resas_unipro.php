<?
	set_time_limit(0);
	session_start();
	require_once('secure.php'); 
	require_once('lib.php');
	// remettre � z�ro
	if ($_POST['reset'])
	{
		$_POST = array();
		$_SESSION['admin_resas_unipro'] = array();
	}
	// faire une recherche
	if (count($_POST))
		$_SESSION['admin_resas_unipro'] = $_POST;
	else if ($_SESSION['admin_resas_unipro'])
	{
		foreach($_SESSION['admin_resas_unipro'] as $k => $v)
			$_POST[$k] = $v;
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script type="text/javascript" src="<?=Page::getProtocol()?>://www.google.com/jsapi"></script>
	<script type="text/javascript">
		google.load('jquery', '1.4');
	</script>
	<link rel="stylesheet" type="text/css" href="../css/anytime.min.css" />
	<script type="text/javascript" src="../scripts/anytime.min.js"></script>
	<script type="text/javascript" src="admin.js"></script>
	<style type="text/css">
		select,input { width: inherit;}
		label { padding-left: 2px; }
		label.col1 { padding-left: 0; float: left; width: 65px;}
	</style>
</head>
<body>
<form action="" method="post">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre">
		<img src="img/puce.gif" align="absmiddle" hspace="1">
		Recherche Unipro Tarifs et Dispos
	</th>
</tr>
<tr>
	<td>
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<tr><td>
		<fieldset><legend>D�finition</legend>
			<p>
				<label class="col1" for="agence">Agence</label>
				<input type="text" value="<?=htmlspecialchars($_POST['agence'])?>" id="agence" name="agence" size="7" maxlength="6"/>
			</p>
			<p>
				<label class="col1" for="type">Type</label>
				<? selectarray('type', array('VP' => 'VP', 'VU' => 'VU'), $_POST['type']); ?>
			</p>
			<p>
				<label class="col1" for="categories">Cat�gories</label>
				<input type="text" value="<?=$_POST['categories']?>" id="categories" name="categories" size="32" maxlength="32"/>
				<em>cat�gories s�par�s par des virgules sans espaces, ex: <code>D+,E,E'</code></em>
			</p>
			<p>
				<label class="col1" for="depart">D�part</label><input type="text" value="<?=($_POST['depart'] ? htmlspecialchars($_POST['depart']) : date('Y-m-d 17:00', strtotime('+7 days')))?>" id="depart" name="depart" size="16" maxlength="16"/>
				<em>AAAA-MM-DD HH:MM</em>
			</p>
			<p>
				<label class="col1" for="retour">Retour</label><input type="text" value="<?=($_POST['retour'] ? htmlspecialchars($_POST['retour']) : date('Y-m-d 17:00', strtotime('+9 days')))?>" id="retour" name="retour" size="16" maxlength="16"/>
				<em>AAAA-MM-DD HH:MM</em>
			</p>
			<p>
				<label class="col1" for="km">Km</label><input type="text" value="<?=($_POST['km'] ? (int)$_POST['km'] : 150)?>" id="km" name="km" size="4" maxlength="4"/>
			</p>
		</fieldset>
		<fieldset><legend>Choix API</legend>
			<? foreach(array('tarifs', 'dispos') as $k) : ?>
			<p>
				<label for="method_<?=$k?>"><?=$k?></label>
				<input type="radio" name="method" id="method_<?=$k?>" value="<?=$k?>" <? if ($_POST['method']==$k || (!isset($_POST['method']) && $k=='tarifs')) echo ' checked'; ?>/>
			</p>
			<? endforeach; ?>
		</fieldset>
		</td></tr>
		</table>
	</td>
</tr>
<tr>
	<td align="right">
		<input type="submit" name="search" value="rechercher">
		&nbsp;<input type="submit" name="reset" value="effacer">
	</td>
</tr>
<tr>
	<td>
	<? 
	if (count($_SESSION['admin_resas_unipro'])) {
		$unipro = new Unipro();
		$type = $_POST['type'];
		$agence = $_POST['agence'];
		$depart = $_POST['depart'];
		$retour = $_POST['retour'];
		$km = (int) $_POST['km'];
		$categories = preg_replace('/[^a-z0-9\+\'\,]/i', '', $_POST['categories']);
		echo "Agence: $agence<br/>Type: $type<br/>D�part: $depart<br/>Retour: $retour<br/>Km: $km<br/>\n";
		if ($categories) {
			echo "Cat�gories: $categories\n";
		}
		if ($_POST['method']=='tarifs') {
			$tarifications = array('NAT','LOC');
			if ($type == 'VU') {
				$tarifications[] = 'VUHS';
			}
			foreach($tarifications as $tarification) {
				echo' <h2>'.$type.' / '.$tarification.'</h2>'."\n";
				echo date('H:i:s').'<br/>';
				$tarifs = $unipro->getTarifs($agence, $type, $categories, $depart, $retour, $km, $tarification);
				echo date('H:i:s').'<br/>';
				$unipro->debug($tarifs);
			}
		} else if ($_POST['method'] == 'dispos') {
			echo' <h2>'.$type.'</h2>'."\n";
			echo date('H:i:s').'<br/>';
			list($dispos, $parcs) = $unipro->getDispos($agence, $type, $categories, $depart, $retour);
			echo date('H:i:s').'<br/>';
			$unipro->debug($dispos);
			$unipro->debug($parcs);
		}
	}
	?>
	</td>
</tr>
</table>
</form>
</body>
</html>
