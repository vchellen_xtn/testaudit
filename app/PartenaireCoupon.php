<?

class PartenaireCoupon extends Ada_Object
{
	// destinataire des pi�ces justificatives
	const PF_CLIENT = 1;
	const PF_AGENCE = 2;
	
	/**
	* Cr�er le coupon et le r�glement associ�e � partir d'un Partenaire et d'un Tarif
	* 
	* @param Partenaire $partenaire
	* @param Client $client
	* @param PartenaireTarif $tarif
	* @return PartenaireCoupon
	*/
	static public function create(Partenaire $partenaire, Client $client, PartenaireTarif $tarif)
	{
		$data = array(
			'id'			=> '',
			'partenaire'	=> $partenaire['id'],
			'code'			=> sprintf('%s%07ld', preg_replace('/[^A-Z]/','', Partenaire::$ORIGINES[$partenaire['origine']]), $client['id']),
			'nom'			=> $partenaire['nom'],
			'classe'		=> $tarif['classe'],
			'echeance'		=> date('Y-m-d', strtotime('+'.$tarif['duree'].' months'))
		);
		$x = new PartenaireCoupon($data);
		$x->save();
		$x->setId(mysql_insert_id());
		return $x;
	}
	
	/** @var Patenaire */
	protected $_partenaire = null;
	/** @var PartenaireReglement */
	protected $_reglement = null;
	protected $_createFromCode = false;
	
	/**
	* Cr�e un coupon � partir d'un identifiant
	* 
	* @param string $id
	* @return PartenaireCoupon
	*/
	static public function factory($id, $idIsCode = false)
	{
		$x = new PartenaireCoupon();
		if ($idIsCode)
		{
			// on supprime les espaces et les 0 au d�but
			$id = preg_replace('/((^0+)|\s)/', '', $id);
			// puis on ne garde que les caract�res alphab�tiques, les tirets, les points
			$id = preg_replace('/[^a-z0-9\.\-\_]/i', '', $id);
			$x->_createFromCode = true;
		}
		if ($x->load($id)->isEmpty())
			return null;
		return $x;
	}
	protected function _prepareSQL($id, $cols='*', $data=null)
	{
		$sql = 'select pc.*, p.origine, p.nom partenaire_nom';
		$sql.=' from partenaire_coupon pc join partenaire p on p.id=pc.partenaire';
		$sql.=' where ';
		if ($this->_createFromCode)
			$sql.=" pc.code='".mysql_real_escape_string($id)."' and pc.actif=1";
		else
			$sql.=' pc.id='.((int)$id);
		return $sql;
	}
	/**
	* V�rifie la validit� du coupon pour  une r�servation
	* 
	* @param Reservation $reservation
	* @param string $msg message d'erreur
	* @returns bool
	*/
	public function isValidFor(Reservation $reservation, &$msg)
	{
		$msg = null;
		if ($this->_data['locapass'] && !in_array(strtolower($reservation['type']), array('vp','vu')))
			$this->setData('locapass', null);
		if ($this->_data['echeance'])
		{
			// on v�rifie l'�ch�ance
			$code = $this->_data['code'];
			$echeance = strtotime($this->_data['echeance']);
			if ($echeance < time())
				$msg = "Le code <strong>$code</strong> n'est plus valable depuis le ".date('d/m/Y', $echeance).".";
			else if ($reservation && $echeance < $reservation->getDebut('U'))
				$msg = "Votre code <strong>$code</strong> n'est pas valable apr�s le ".date('d/m/Y', $echeance).".";
			// s'il y a un probl�me d'�ch�ance
			if ($msg)
			{
				if ($this->_data['origine']=='webpro')
				{
					// on renouvelle automatiquement le code promotionnel
					// en trouvant le tarif correspondant
					if ($tarif = PartenaireTarif::factory($this->getPartenaire(), $this->_data['echeance']))
					{
						// on r�cup�re le dernier r�gleemnt existant ?
						$reglement = PartenaireReglement::factory($this->getId());
						if (!$reglement || $reglement['reservation'])
						{
							// s'il a �t� pay�e on en cr�e un nouveau on cr�e une nouveua r�glement et on en informe le client
							$reglement = PartenaireReglement::create($this, $tarif);
						}
						if ($reglement['prix_ht'] > 0)
						{
							$msg.= sprintf("<br/>Le renouvellement de votre code promotionnel sera inclus dans votre prochaine r�servation pour un montant de %.2f � HT pour une dur�e de %d mois.", $reglement['prix_ht'], $reglement['duree']);
						}
						else
						{
							$msg.= sprintf("<br/>Le renouvellement de votre code promotionnel vous est offert et sera effectif � l'issue de votre prochaine r�servation.");
						}
					}
				}
				else
				{
					// comment prolonger l'�ch�ance autrement ?
					if (in_array($this->_data['origine'], array('ce','webce','locapass')))
						$msg .= "<br/>Vous pouvez le renouveler en nous contactant via l'adresse suivante : <a href=\"mailto:commercial@ada.fr\">commercial@ada.fr</a>";
					return false;
				}
			}
		}
		// v�rification de date de d�but
		if ($this->_data['depuis'])
		{
			// on v�rifie la validit�
			$code = $this->_data['code'];
			$depuis = strtotime($this->_data['depuis']);
			if ($depuis > time())
			{
				$msg = "Le code <strong>$code</strong> n'est pas encore valable, il le sera � partir du ".date('d/m/Y', $depuis).".";
				return false;
			}
		}
		// v�rification locapass
		if ($this->_data['locapass'] && $reservation->hasData('agence') && $reservation->getAgence()->getReseau()!='ADA')
		{
			$msg = "Ce service est op�r� par une agence partenaire du r�seau ADA : vous pr�senterez au comptoir votre voucher internet, mais pas votre bon locapass. Il vous sera demand� un d�p�t de garantie par carte bancaire, afin de r�gler au retour d��ventuels dommages au v�hicule. Si vous souscrivez � des options compl�mentaires (second conducteur, assurances compl�mentaires, GPS...), celles-ci seront � r�gler � l'agence.";
		}
		return true;
	}
	/**
	* Renvoie les r�glements d'un coupon
	* @returns PartenaireReglement_Collection 
	*/
	public function getPartenaireReglements()
	{
		return PartenaireReglement_Collection::factory($this);
	}
	
	/**
	* Revnoie le dernier r�glement associ�e avec le coupon
	* @returns PartenaireReglement
	*/
	public function getPartenaireReglement()
	{
		if ($this->_data['origine']=='webpro' && !$this->_reglement)
		{
			$this->_reglement = PartenaireReglement::factory($this->getId());
		}
		return $this->_reglement;
	}
	public function getPrixHT()
	{
		// pour les WebPro il peut y avoir un 
		if ($r = $this->getPartenaireReglement())
		{
			// y-a-il un r�glement en attente ?
			if (!$r['reglement'])
				return $r['prix_ht'];
		}
		return null;
	}
	
	public function getEntreprise()
	{
		return $this->_data['nom'];
	}
	/**
	* Paie le coupon si n�cessaire et ajsute l'�ch�ance en fonction
	* 
	* @param Reservation $reservation
	* @returns bool
	*/
	public function isPaidBy(Reservation $reservation)
	{
		if ($reglement = $this->getPartenaireReglement())
		{
			// si le r�glement est effectif on augmente l'�ch�ance
			if ($reglement->isPaidBy($reservation))
			{
				$this->setData('echeance', date('Y-m-d', strtotime('+'.$reglement['duree'].' months')));
				$this->save('echeance');
				return true;
			}
		}
		return false;
	}
	

	/**
	* Renvoie le Partenaire associ�
	* @return Partenaire
	*/
	public function getPartenaire()
	{
		if (!$this->_partenaire)
			$this->_partenaire = Partenaire::factory($this->getData('partenaire'));
		return $this->_partenaire;
	}
	
	// Renvoie une phrase pour les pi�ces justificatives � fournir
	public function getPiecesJustificatives($destinataire = PartenaireCoupon::PF_CLIENT)
	{
		// s'il s'agit d'un locapass
		if ($this->getData('locapass'))
		{
			if ($destinataire == self::PF_CLIENT)
				return "Pr�senter en agence votre bon locapass ainsi qu'une copie de votre derni�re fiche de paie.";
			else
				return "Doit pr�senter un bon locapass et une copie de sa derni�re fiche de paie (".$this->getPartenaire()->getNom().").";
		}
		// puis sinon selon le type de partenaire
		$origine = $this->getPartenaire()->getOrigine();
		if($origine == 'ce' || $origine == 'webce')
		{
			if ($destinataire == self::PF_CLIENT)
				return "Pr�senter ce bon en agence avec le tampon de l'entreprise et la signature du g�rant, ainsi qu'une fiche de paie ou de votre carte CE.";
			else
				return "Doit pr�senter un bon avec le tampon de l'entreprise et la signature du g�rant et une fiche de paie ou sa carte CE (".$this->getPartenaire()->getNom().").";
		}
		return null;
	}
	/**
	* Renvoie un tableau de statistques : MOIS, VP, VU
	* 
	* @param string $date
	* @returns array
	*/
	public function getStatsSince($date = null)
	{
		$sql = "select concat(year(paiement),'-',month(paiement)) MOIS, sum(case when type='VP' then 1 else 0 end) VP, sum(case when type='VU' then 1 else 0 end) VU";
		$sql.=" from reservation";
		$sql.=" where coupon=".$this->getId()." and statut IN ('A','P')";
		if ($date)
			$sql .= " and paiement >= $date";
		$sql.=" group by year(paiement), month(paiement)";
		$rs = sqlexec($sql);
		$a = array();
		while ($row = mysql_fetch_assoc($rs))
		{
			if (!$a) $a[] = array_keys($row);
			$a[] = $row;
		}
		return $a;
	}
	/**
	* Renvoie les r�servations assoc�es � ce coupon
	* 
	* @param string $fromDate : depuis la date
	* @param int $limit : nombre maximum attendu
	* @param bool $loadFacturation r�cup�rer les facturations ?
	* @return Reservation_Collection
	*/
	public function getReservations($fromDate = null, $limit = null, $loadFacturation = true)
	{
		return Reservation_Collection::factory($this, $fromDate, $limit, $loadFacturation);
	}

}
?>
