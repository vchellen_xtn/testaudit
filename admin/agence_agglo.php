<?
	require_once('secure.php'); 
	require_once('lib.php');

	// identifiant
	$id = (int) getarg('id');
	
	// suppression
	if ($id && getarg('delete')) 
	{
		$sql = "delete from agence_agglo where id = '".$id."'";
		getsql($sql);
		unset($id);
	}
	
	// modification, création
	if (getarg("update") || getarg("create"))
	{
		$build_map = false;
		if (!$_POST['canon'])
		{
			$build_map = true;
			$_POST['canon'] = canonize($_POST['nom']);
		}
		save_record('agence_agglo', $_POST);
		if (!$id)
		{
			$id = mysql_insert_id();
			$_POST['id'] = $id;
		}
		if ($build_map)
		{
			$_POST['type'] = 'agglo';
			save_record('refnat_canon', $_POST);
			include('map.php');
		}
	}
	if ($id)
	{
		$rs = sqlexec("select * from agence_agglo where id = '".$id."' ");
		if (!$rs || !($row = mysql_fetch_assoc($rs)))
			die("Aucun enregistrement correspondant.");
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script language="javascript" src="../scripts/check.js"></script>
	<script>
	<!-- //
	// doDelete
	function doDelete(frm)
	{
		return (confirm("Voulez-vous réellement supprimer cet élément ?\nCette opération est irréversible."));
	}
	
	// doSubmit
	function doSubmit(frm)
	{
		var err = "";
		if(!frm.nom.value.length)
			err+= "Veuillez indiquer le nom\n";
		if (!frm.dept.value.match(/^\d{2,3}$/))
			err+= "Le département n'est pas valide\n";
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		return true;
	}
	// -->
	</script>
</head>
<body>
<form action="" method="post" onsubmit="return doSubmit(this);">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre">
	<img src="img/puce.gif" align="absmiddle" hspace="1">
	Agglomération
	</th>
</tr>
<tr>
	<td>
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="15%">
		<col width="15%">
		<col width="15%">
		<col width="55%">
		<tr>
			<td><label for="id">ID</label></td>
			<td>
				<? if ($id) echo $id; ?>
				<input type="hidden" name="id" value="<? if ($id) echo $id; ?>"/>
			</td>
			<td><label for="en_avant">En avant</label></td>
			<td><? selectarray('en_avant', array('non','oui'), $row['en_avant']);?></td>
		</tr>
		<tr>
			<td><label for="nom">Nom</label></td>
			<td><input type="text" id="nom" name="nom" value="<?=$row['nom']?>" maxlength="64"/></td>
			<td><label for="canon">Canon</label></td>
			<td>
				<input type="text" id="canon" name="canon" value="<?=$row['canon']?>" maxlength="64" readonly/>
				<br/><a href="#" onclick="document.getElementById('canon').value=''; return false;">effacer</a>
			</td>
		</tr>
		<tr>
			<td><label for="dept">Département</label></td>
			<td><input type="text" name="dept" id="dept" value="<?=$row['dept']?>" maxlength="3"></td>
			<td colspan="2">&nbsp;</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td align="right">
		<input type="submit" name="<?=$id ? "update" : "create"?>" value="enregistrer">
		<? if ($id) : ?>
		&nbsp;
		<input type="submit" onclick="return doDelete(this.form);" name="delete" value="supprimer">
		<? endif; ?>
	</td>
</tr>
</table>
</form>
</body>
</html>
