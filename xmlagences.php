<?
	// utilis� par les partenaires ADA pour avoir la liste des agences
	// xmlagences.php
	include "xml.php";
	$base = 'http://'.$_SERVER["SERVER_NAME"].dirname($_SERVER["SCRIPT_NAME"]);
	if (substr($base,-1) != '/') 
		$base.='/';
	$co = htmlspecialchars(strip_tags($_REQUEST['co']));
	if ($co) $co = '?co='.rawurlencode($co);

	$element = "agence";
	$sql = "SELECT aa.id, aa.nom, ap.adresse1, ap.adresse2, ap.cp, ap.ville, a.tel, a.fax, DEGREES(ap.latitude) latitude, DEGREES(ap.longitude) longitude";
	$sql.= ", ap.photo";
	$sql.= ", CONCAT('location-voiture-',aa.canon,'.html') AS agence";
	$sql.= ", CONCAT('location-voiture-',d.canon,'.html') AS departement";
	$sql.= ", CONCAT('recherche-location-voiture-',aa.canon,'.html') AS reservation";
	$sql.= " FROM agence a JOIN agence_pdv ap ON (ap.agence=a.id and ap.publie=1)";
	$sql.="  JOIN agence_alias aa ON aa.pdv=ap.id";
	$sql.= " JOIN dept d ON (d.zone='fr' AND d.id=aa.dept)";
	$sql.= " WHERE a.zone='fr' AND (a.statut & 1) = 1";
	$sql.= " ORDER BY aa.nom";
	$attrs = mysql_fetch_assoc(sqlexec("SELECT NOW() AS date"));
	xml_from_sql($element, $sql, null, do_agence_attr);

// on d�finit une fonction pour interpr�ter les attributs
function do_agence_attr($k, $v) {
	global $base, $co;
	if($k == 'agence' || $k == 'departement')
		$v = $base.$v.$co;
	else if ($k == 'reservation')
		$v = $base.$v.$co;
	else if ($k == 'photo' && $v)
		$v = $base.$v;
	return $v;
}
?>
