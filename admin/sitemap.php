<?
	include_once ("secure.php");
	include_once ("lib.php");

	// pour �crie le sitemap
	function sitemap_open ($fname)
	{
		$f = fopen($fname, 'wt');
		fwrite($f, '<?xml version="1.0" encoding="UTF-8"?>'."\n");
		fwrite($f, '<!-- Le '.date("Y-m-d H:i:s", time())." -->\n");
		fwrite($f, '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'."\n");
		return $f;
	}
	function sitemap_write ($f, $url, $frequency, $priority)
	{
		fwrite($f, "\t<url><loc>$url</loc><changefreq>$frequency</changefreq><priority>$priority</priority></url>\n");
	}
	function sitemap_close ($f)
	{
		fwrite($f, '</urlset>');
		fclose($f);
	}
	function sitemap_annuaire ($f, $base, &$refnat, $sql, $priority='0.5', $frequency='monthly')
	{
		$cnt = 0; $rs = sqlexec($sql);
		while ($row = mysql_fetch_assoc($rs))
		{
			sitemap_write($f, $base.gu_annuaire($row, $refnat), $frequency, $priority);
			$cnt++;
		}
		return $cnt;
	}	

	set_time_limit(0);
	$output = (basename(__file__) == basename($_SERVER['SCRIPT_NAME']));
	if ($output)
	{
?>
<html>
<head>
	<title>G�n�ration des fichiers SITEMAP</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
</head>
<body>
<?
	}

	// on peut continuer � g�n�rer un RewriteMap
	$base = 'http://'.SERVER_RESA.'/';
	$f = sitemap_open('../cache/sitemap/sitemap_resa.xml');
	$cnt=0; $rs = sqlexec("select type, id, canon from refnat_canon where type in ('agence','zone','dept','emplacement','agglo') order by type desc, canon asc");
	while ($row = mysql_fetch_assoc($rs))
	{
		sitemap_write($f, $base.gu_agence($row), 'weekly', '1.0')	;
		$cnt++;
	}
	sitemap_close($f);
	if ($output) {
		echo "Sitemap RESA : $cnt zones<br>\n";
	}
	
	// sitemaps pour l'annuaire
	$base = 'http://'.SERVER_ANNU.'/';
	$sitemaps = array();
	
	// initialiser le cache des noms canoniques
	$rs = sqlexec('select type, id, canon from refnat_canon order by type');
	while ($row = mysql_fetch_assoc($rs)) {
		$refnat[$type][$row['id']] = $row;
	}

	// annuaire : type, marque, modele
	$fname = '../cache/sitemap/sitemap_annu_type.xml';
	$cnt = 0; $sitemaps[] = basename($fname);
	$f = sitemap_open($fname);
	$cnt += sitemap_annuaire ($f, $base, $refnat, "select t.id type from vehicule_type t where t.id in ('vp','vu') order by t.id");
	$cnt += sitemap_annuaire ($f, $base, $refnat, "select distinct type, marque from vehicule_modele where publie=1");
	$cnt += sitemap_annuaire ($f, $base, $refnat, "select distinct type, marque, id modele from vehicule_modele where publie=1");
	sitemap_close($f);
	if ($output) echo "SITEMAP ".basename($fname).": $cnt<br>\n";

	// �crire le sitemap index
	$f = fopen('../cache/sitemap/sitemap_annu.xml', 'wt');
	fwrite ($f, '<?xml version="1.0" encoding="UTF-8"?>'."\n");
	fwrite ($f, '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'."\n");
	foreach($sitemaps as $v)
   		fwrite ($f, "\t<sitemap><loc>$base$v</loc></sitemap>\n");
	fwrite($f, '</sitemapindex>');
	if ($output)
	{
		echo "SITEMAPINDEX: ".count($sitemaps)."<br>\n";
?>
</body>
</html>
<?
	}
?>
