<?
	require_once("secure.php"); 
	require_once("lib.php");

	// identifiant
	$email = filter_var(getarg("email"), FILTER_VALIDATE_EMAIL);
	
	// suppression
	if ($email && getarg("delete")) 
	{
		$sql = "delete from prospect where email = '".$email."'";
		getsql($sql);
		unset($email);
	}
	
	// modification
	if (getarg("update") || getarg("create"))
		save_record("prospect", $_POST);

	if ($email)
	{
		$rs = sqlexec("select * from prospect where email = '".$email."'");
		if (!$rs || !($row = mysql_fetch_assoc($rs)))
			die("Aucun enregistrement correspondant.");
	}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script language="javascript" src="../scripts/check.js"></script>
	<script>
	<!-- //

	// doDelete
	function doDelete(frm)
	{
		return (confirm("Voulez-vous r�ellement supprimer cet �l�ment ?\nCette op�ration est irr�versible."));
	}
	
	// doSubmit
	function doSubmit(frm)
	{
		var err = "";
		
		if(!ValidateEmail(frm.email))
			err += "L'adresse email est incorrecte\n";
		
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		return true;
	}
	
	// -->
	</script>
</head>

<body>

<form action="" method="post">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre">
	<img src="img/puce.gif" align="absmiddle" hspace="1">
	<?="Prospect";?>
	</th>
</tr>
<tr>
	<td>
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="15%">
		<col width="35%">
		<col width="15%">
		<col width="35%">
		<tr>
			<td><label for="email">Email</label></td>
			<td colspan="3">
				<?=$email;?>
				<input type="<?=($email ? "hidden" : "text")?>" id="email" name="email" value="<?=$email?>"/>
			</td>
		<tr>
			<td><label for="origine">Origine</label></td>
			<td><? selectarray('origine', array('ADAFR'=>'ADAFR','JEUNES'=>'JEUNES'), $row['origine'])?></td>
			<td><label for="desabo">Statut</label></td>
			<td><? selectarray("desabo", array("0" => "Actif", "1" => "Inactif"), $row["desabo"]); ?></td>
		</tr>
		<tr>
			<td><label for="titre">Civilit�</label>
			<td><? selectarray("titre", $CIVILITE, $row['titre'], '--'); ?></td>
			<td><label for="naissance">Naissance</label></td>
			<td><input type="text" id="naissance" name="naissance" value="<?=euro_iso($row['naissance'])?>" maxlength="10"/></td>
		</tr>
		<tr>
			<td><label for="prenom">Pr�nom</label></td>
			<td><input type="text" name="prenom" id="prenom" value="<?=stripslashes($row['prenom'])?>" maxlength="64"></td>
			<td><label for="nom">Nom</label></td>
			<td><input type="text" name="nom" id="nom" value="<?=stripslashes($row['nom'])?>" maxlength="64"></td>
		</tr>
		<tr>
			<td><label for="cp">Code postal</label></td>
			<td><input type="text" id="cp" name="cp" value="<?=$row['cp']?>" maxlength="12"/></td>
			<td colspan="2"></td>
		</tr>
		<tr>
			<td>Cr�ation</td>
			<td><?=euro_iso($row["creation"])?></td>
			<td>Modification</td>
			<td><?=euro_iso($row["modification"])?></td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td align="right">
		<input type="submit" name="<?=$email ? "update" : "create"?>" value="enregistrer" onclick="return doSubmit(this.form);">
<?
	if ($email) 
	{
?>
		&nbsp;
		<input type="submit" onclick="return doDelete(this.form);" name="delete" value="supprimer">
<?
	}
?>
	</td>
</tr>
</table>
</form>

</body>
</html>
