<?
	if (basename(__file__)==basename($_SERVER['SCRIPT_NAME'])) {
		require_once ("../secure.php");
	}
	
	// decrypter le sql ou le recevoir en clair en GET/POST
	if($_POST['sqlencrypted']) {
		$exporter = SQLExporter::doUnserialize($_POST['data']);
	} else {
		die("Exportaion impossible");
		//$exporter = new SQLExporter(array('sql'=>$_REQUEST['sql'], 'fname'=>$_REQUEST['fname'], 'sqlpreserve'=>$_REQUEST['sqlpreserve']));
	}
	
	// fichier excel
	if ($exporter && !$exporter->isEmpty()) {
		$exporter->toExcel();
	}
?>
