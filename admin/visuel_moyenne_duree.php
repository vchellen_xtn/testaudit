<?
	require_once("secure.php"); 
	require_once("lib.php");

	// identifiant
	$id = getarg('id', '\d*');
	
	// suppression
	if ($id && getarg("delete")) 
	{
		$sql = "delete from visuel_moyenne_duree where id = '".$id."'";
		getsql($sql);
		unset($id);
	}
	
	// modification, cr�ation
	if (getarg("update") || getarg("create"))
	{
		save_files($_POST, array('url_img' => '../fichiers/visuel_moyenne_duree'));
		$_POST['url_img'] = preg_replace('/^\.\.\/(.+)/', '$1', $_POST['url_img']);
		save_record('visuel_moyenne_duree', $_POST);
		if (!$id) $id = mysql_insert_id();
	}
	// afficher le visuel
	if ($id)
	{
		$rs = sqlexec('select * from visuel_moyenne_duree where id = "'.addslashes($id).'"');
		if (!$rs || !($row = mysql_fetch_assoc($rs)))
			die("Aucun enregistrement correspondant.");
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script>
	<!-- //
	// doDelete
	function doDelete(frm)
	{
		return (confirm("Voulez-vous r�ellement supprimer cet �l�ment ?\nCette op�ration est irr�versible."));
	}
	
	// doSubmit
	function doSubmit(frm)
	{
		var err = "";
		if(!frm.libelle.value.length)
			err += "Veuillez indiquer le nom\n";
		if(!frm.type.value.length)
			err += "Veuillez indiquer le type de v�hicule\n";
		if (!frm.position.value.match(/^\d+$/))
			err += "Veuillez indiquer la position\n";
		if(!frm.description.value.length)
			err += "Veuillez indiquer la description\n";
		if(!frm.url_img.value.length)
			err += "Veuillez indiquer l'url du visuel\n";
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		return true;
	}
	// -->
	</script>
</head>
<body>
<form action="" method="post" onsubmit="return doSubmit(this);" enctype="multipart/form-data">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre">
	<img src="img/puce.gif" align="absmiddle" hspace="1">
<? 
	echo "Visuel";
?>
	</th>
</tr>
<tr>
	<td>
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="15%">
		<col width="35%">
		<col width="15%">
		<col width="35%">
		<tr>
			<td>ID</td>
			<td>
				<strong><?=$id?></strong>
				<input type="hidden" name="id" value="<?=$id?>">
			</td>
			<td><label for="publie">Publi�</label></td>
			<td>
				<input type="checkbox" id="publie" name="publie[]" value="1" <? if(!$id || $row['publie']) echo 'checked'; ?>>
				<input type="hidden" name="publie[]" value="0">
			</td>
		</tr>
		<tr>
			<td><label for="type">Type</label></td>
			<td><? selectarray('type', array('vp'=>'voiture','vu'=>'utilitaire'), $row['type'], '--');?></td>
			<td><label for="position">Position</label></td>
			<td><input type="text" class="l" id="position" name="position" value="<?=$row['position']?>" maxlength="5" size="5"/></td>
		</tr>
		<tr>
			<td><label for="libelle">Libell�</label></td>
			<td colspan="3"><input type="text" class="l" id="libelle" name="libelle" value="<?=$row['libelle']?>" maxlength="64" size="64"/></td>
		</tr>
		<tr>
			<td><label for="description">Description</label></td>
			<td colspan="3"><input type="text" class="l" id="description" name="description" value="<?=$row['description']?>" maxlength="64" size="64"/></td>
		</tr>
		<tr>
			<td><label for="url_img">Visuel</label></td>
			<td colspan="3"><?=url_ctrl('url_img', $row['url_img'])?></td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%">
		<tr>
			<td>
			<? if ($id) : ?>
				<input type="button" value="pages" onclick="parent.list.location.href='lists/visuel_page.php?visuel=<?=$id?>'; return false;">
			<? endif; ?>
			</td>
			<td align="right">
				<input type="submit" name="<?=$id ? "update" : "create"?>" value="enregistrer">
				<? if ($id) : ?>
				&nbsp;
				<input type="submit" onclick="return doDelete(this.form);" name="delete" value="supprimer">
				<? endif; ?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
</form>
</body>
</html>
