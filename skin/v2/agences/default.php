		<? if ($this->hasData('html_breadcrumb')) : ?>
			<? echo $this->getData('html_breadcrumb');?>
		<? endif; ?>
		<div class="bg">
			<div class="header">
				<?
					if($this->getId() == 'agences/liste' && !$this->getAgences()->getLibelle()) {
						echo '<h1>'.$this->getUniversTitre()."</h1>\n";
					} elseif ($this->getId() == 'agences/agence') {
						echo '<h1>'.$this->getUniversTitre()."</h1>\n";
					} elseif($this->getId() == 'agences/malin') {
						echo '<div class="univers_titre">'.$this->getUniversTitre().'</div>';
					} 
					else {
						echo hn(1, $this->getUniversTitre(), "000000", "16");
					}
				?>
			</div>
			<div class="content">
				<div id="page">
				<?
					include($this->getPagePath());
				?>
				</div>
				<div class="clear"></div>
				<? if ($this->hasData('html_links_around')) : ?>
					<? echo $this->getData('html_links_around');?>
				<? endif; ?>
				<? if ($this->hasData('html_links_zoom')) : ?>
					<? echo $this->getData('html_links_zoom');?>
				<? endif; ?>
				<div class="clear"></div>
			</div>
		</div>
