<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?
	$this->addCSS($this->getProtocol().'://fonts.googleapis.com/css?family=Raleway:700,600,500,400');
	$this->writeHead();
?>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0" />
<? if (USE_ABTASTY) : /* ABTesting : https://app.abtasty.com/ */ ?>
	<script type="text/javascript" src="//try.abtasty.com/a1b0a7dd1812a39ae90024dcf53a9ae9.js"></script>
<? endif; ?>
<? if (USE_WEB2ROI) : /* Web2ROI : http://rnd.web2roi.com/ */ ?>
	<script type="text/javascript">
	/* <![CDATA[  */ 
	(function() {
		window.sy = {
			uid : "adafr"
			,usid : ""
		};
		var w2r = document.createElement('script'); w2r.type = 'text/javascript'; w2r.async = true;
		w2r.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 't.sytsem.com/v2/tracker.js';
		var sw2r = document.getElementsByTagName('script')[0]; sw2r.parentNode.insertBefore(w2r, sw2r);
	})();
	/* ]]> */
	</script>
<? endif; ?>
	<script type="text/javascript">
	// <![CDATA[
	var appADA = appADA || {};
	appADA.baseUrl = '<?=Page::getURL(null, null, (self::getProtocol()=='https'));?>';
	appADA.skin = '<?=Page::getSkin();?>';

	if (typeof $ !== "undefined")
	{
		$(document).ready(function()
		{
			//main nav & toggle
			$('.dropdown-trigger').click(function(){
				var dropdownTarget = $(this).attr('data-target');
				$(this).toggleClass('opened');
				$(dropdownTarget).stop().slideToggle();
				return false;
			});
			
			
			// le formulaire de connexion
			$("#login-form--header").submit(function() {
				// on v�rifie les 2 champs
				var ok = true;
				if (!validateEmail(this.email)) {
					$(this.email).addClass("error");
					ok = false;
				} else {
					$(this.email).removeClass("error");
				}
				if (!this.pwd.value.length) {
					$(this.pwd).addClass("error");
					ok =false;
				} else {
					$(this.pwd).removeClass("error");
				}
				if (!ok) return false;
				// soumettre le formulaire en AJEX
				var frm = this;
				var url = this.action;
				$.post(url.replace('client/index.html', 'login.php?action=login').replace(/^https?\:/, window.location.protocol), $(this).serialize(), function (xml) { 
					if (xml) {
						$xml = $(xml);
						if ($xml.find("error").length) {
							alert($xml.find("error").text());
						} else {
							// remplir la zone de connexion
							$auth = $("#customer_connected");
							$auth.find("span.prenom").text($xml.find("prenom").text());
							$auth.find("span.nom").text($xml.find("nom").text());
							$auth.find("a.logout").attr("title", "Vous �tes " + $xml.find("email").text());
							// montrer la connexion et cacher le formulaire
							$auth.show();
							$(frm).hide();
						}
					}
				});
				return false;
			});
		});
	}
	// ]]>
	</script>
	<meta property="og:image" content="http://www.ada.fr/css/img/structure-2015/logo-ada.jpg" />
</head>
<body class="<?=dirname($this->getId()).' '.basename($this->getId());?>">
	<div id="fb-root"></div>
	<script>
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.5";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
	<header>
		<nav class="navbar navbar-default">
			<div class="nav navbar-nav">
				<div class="navbar-header align-center">
					<button type="button" class="navbar-toggle dropdown-trigger" data-target="#main-nav">
						<span class="sr-only">Navigation</span>
					</button>
					<? if ($page == 'reservation/index') echo '<h1>'; ?>
					<a href="<?=$here?>"><img src="<?=$baseurl?>css/img/structure-2015/logo-ada-small.gif" alt="location de voiture et utilitaire - ADA.fr" class="logo" /></a>
					<? if ($page == 'reservation/index') echo '</h1>'; ?>
				</div>
				<div id="main-nav" class="dropdown-target">
					<ul class="nav">
						<li><a href="<?=$here?>">Ma r�servation</a></li>
						<li><a href="<?=Page::getURL('client/index.html', null, USE_HTTPS);?>">Mon espace client</a></li>
						<li><a href="<?=$here?>agences/index.html" class="trouver-agence">Agences</a></li>
						<li><a href="<?=$here?>annuaire/index.html">Annuaire</a></li>
						<li><a href="<?=$here?>vehicules/index.html">V�hicules</a></li>
						<li><a href="<?=$here?>offre/index.html">Offres</a></li>
						<li><a href="<?=$here?>demenagement/index.html">D�m�nagement</a></li>
						<li><a href="<?=$here;?>faq/index.html">Questions</a></li>
						<li><a href="<?=$here?>contact/index.html" title="Nous contacter">Nous contacter</a></li>
						<? if ($this->getConnectedClient()) : ?>
						<li><a href="<?=$here?>?logout">Me d�connecter</a></li>
						<? endif; ?>
					</ul>
				</div>
			</div>
		</nav>
	</header>
	
	<div id="content" class="container-fluid">
		<?
			// maintenance et t�l�-conseiller
			$this->writeInfo();
			// inclure la page courante
			include($this->getSkinnedPagePath());
		?>
	</div>
	
	<footer class="footer">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-9">
					<ul class="list--chevron">
						<li><a href="<?=$here?>conditions/location.html" title="Conditions g�n�rales">Conditions g�n�rales de vente</a></li>
						<li><a href="<?=$here?>juridique/regles_communiques.html">Informations r�glement�es</a></li>
						<li><a href="http://www.ada.fr" target="_blank">Acc�s au site ada.fr</a></li>
					</ul>
				</div>
				<div class="col-xs-3 text-right">
					<a href="https://www.facebook.com/ADALocationdevehicules" target="_blank"><img src="<?=$baseurl?>css/img/structure-mobile/btn-fb.png" alt="Facebook" class="btn-fb" /></a>
				</div>
			</div>
		</div>
	</footer>

	<div class="content--baseline">
		<?
			if (!$this->isNoIndex() && $this->hasData('footer')) {
				// r�cup�rer le texte de la baseline en corrigeant les liens
				if ($baseline = $this->getData('footer')) {
					$baseline = preg_replace("/href=\"(?!http)/" , 'href="'.$here, $baseline);
					echo '<ul class="baseline">'."\n";
					echo '<li>'.join("</li>\n<li>", explode("\n", $baseline))."</li>\n";
					echo '</ul>'."\n";
				}
			}
			if (!$this->isNoIndex() && $this->hasData('baseline')) {
				echo $this->getData('baseline');
			}
		?>
	</div>
	<div id="dialog-popin" title="">
		<div id="dialog-popin-msg"></div>
	</div>
<?
	// informations de mise au point
	$this->writeDebugInfo();
	$this->writeJSStats();
?>
</body>
</html>