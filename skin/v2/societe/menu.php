<?
	$menu = array(
		'index'				=> 'Historique',
		'partenaires'		=> 'Partenaires',
		'reseau'			=> 'R�seau',
		'chiffres'			=> 'Chiffres',
		'mentions_legales'	=> 'Mentions l�gales'
	);
	$action = basename($page);
?>
<ul class="menu">
	<? foreach($menu as $k => $lbl) : ?>
		<li>
			<a href="<?=$k?>.html"<?if ($action==$k) echo ' class="current"'; ?>><?=$lbl;?></a>
		</li>
	<? endforeach; ?>
</ul>