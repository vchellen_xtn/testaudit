<?
	// les tags autoris�s dans les �ditions HTML
	define('HTML_TAGS_ALLOWED', '<a><b><br><em><h2><h3><hr><hr/><i><li><ol><p><span><sub><sup><strong><ul>');

	/*
	** fonctions utilitaires
	*/

	function stats_franchise_key($code_societe)
	{
		return md5(date('Ymd').'St@tFranchises'.$code_societe);
	}
	
	function money($x)
	{
		if ($x > 0) 
			return number_format($x, 2, ',', ' ').'&nbsp;�';
	}

	// sumarg
	function sumarg($a)
	{
		$x = $_REQUEST[$a];
		if (is_array($x))
		{
			for($v=0, $i=0; $i<count($x); $i++)
				$v += $x[$i];
		}
		else
		{
			$v = 0 + $x;
		}
		return $v;
	}
	
	// url_ctrl
	function url_ctrl ($name, $url)
	{
		global $script_written;
		if (!$script_written)
		{
?>
<script>
	// url_control
	function url_control(name, url)
	{
		box = document.getElementById("div_" + name);
		if (!box)
			return;
		if (url != undefined && url.length)
		{
			html = url;
			html+= '&nbsp;&nbsp;<a href="javascript:void(0)" onclick="window.open(\'../' + url + '\')">[voir]</a>';
			html+= '<a href="#" onclick="url_control(\'' + name + '\'); return false;">[supprimer]</a>';
			html+= '<input type="hidden" name="' + name + '" value="' + url + '">';
		}
		else
		{
			html = '<input type="file" name="' + name + '" value="">';
		}
		box.innerHTML = html;
	}
</script>
<?
			$script_written = true;
		}
		echo '<div id="div_'.$name.'"></div>';
		echo '<script>url_control("'.$name.'", "'.$url.'");</script>';
	}
	
function show_table($title, $sql, $href = array(), $format = array(), $hide = array())
{
	$rs = sqlexec($sql);
	echo "<h2>$title</h2>\n";
	$cnt = mysql_num_rows($rs);
	if (!$rs || !$cnt)
	{
		echo "<p>aucune donn�e</p>\n";
		return $cnt;
	}
	echo '<table border="0" cellpadding="3" cellspacing="0" class="framed stats">'."\n";;
	while ($row = mysql_fetch_assoc($rs))
	{
		if ($href)
		{
			$search = $replace = array();
			foreach($row as $k => $v)
			{
				$search[] = "#$k#";
				$replace[] = $v;
			}
		}
		if (!$first)
		{
			$first = true;
			echo '<tr style="background: url(img/bg_list.gif) repeat-x 0% 100%;"><th>'.join('</th><th>', array_diff(array_keys($row), $hide)).'</th></tr>'."\n";
		}
		echo '<tr>';
		foreach($row as $k => $v)
		{
			if (in_array($k, $hide)) continue;
			echo '<td';
			if (is_numeric($v))
				echo ' align="right"';
			else if (strlen($v) < 2)
				echo ' align="center"';
			echo '>';
			if ($href[$k])
				echo '<a href="'.str_replace($search, $replace, $href[$k]).'">';
			if ($format[$k])
			{
				if ($format[$k]=='money')
					$v = number_format($v, 2, ".", " ").' �';
				else
					$v = sprintf($format[$k], $v);
			}
			echo isset($v) ? $v : '&nbsp';
			if ($href[$k])
				echo '</a>';
			echo '</td>';
		}
		echo "</tr>\n";
	}
	echo '</table>'."\n";
	return $cnt;
}

?>