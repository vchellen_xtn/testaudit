<?
class Page_Contact extends Page
{
	protected $_motifContact = array
	(
		 'Identification (adresse, bon de location, r�f�rence, client,...)'
		,'Prix/Quantit� (dur�e de la location, tarif, nombre de kilom�tres,...)'
		,'V�hicule indisponible'
		,'Carburant'
		,'Probl�mes de r�glement (caution d�bit�e, r�servation pr�pay�e non utilis�e, double r�glement)'
		,'Probl�mes techniques'
		,'Commentaires et suggestions'
	);

	/** @returns array */
	public function getMotifs()
	{
		return $this->_motifContact;
	}
	
	public function getUniversTitre() {
		return "Contactez-nous";
	}

	// contact/satisfaction
	public function satisfactionAction()
	{
		// obtenir la r�servation correspondante
		foreach(array($_GET['key'], $_COOKIE['ADA001_QUESTION_USER_KEY']) as $k)
		{
			if ($k)
			{
				$userKey = filter_var($k, FILTER_SANITIZE_STRING);
				break;
			}
		}
		if (!$userKey) return;
		
		// cr�ation ou r�cup�ration d'un QuestionUser
		$questionUser = QuestionUser::factory($userKey);
		if (!$questionUser) return;
		$this->_setCookie('ADA001_QUESTION_USER_KEY', $userKey);
		$this->setData('question_user', $questionUser);
		// obtenir le questionnaire
		$questionnaire = QuestionQuestionnaire::factory(1);
		$this->setData('questionnaire', $questionnaire);
		
		// enregistement des r�ponses
		if ($_POST['questionnaire'])
		{
			// TODO : traiter les erreurs
			if ($save = $questionnaire->saveReponses($questionUser, $_POST, $errors))
				$this->setData('is_form_valid', true);
			else
			{
				$this->setData('is_form_valid', false);
				$this->_addMessage($errors);
			}
		}
	}

	// contact/message
	public function messageAction()
	{
		if(!$_POST['valid_contact_x'] && !$_POST['valid_contact'])
			return;
		// on v�rifie la pr�sence des champs obligatoires
		// et on remplit le tableau pour l'envoi de l'e-mail
		$data = array('date_jour' => date('d-m-Y � h:m:s'));
		foreach (explode(',','email,motif,civilite,prenom,nom,tel,msg,mobile') as $k)
		{
			$v = $_POST[$k];
			if (!$v)
			{
				$this->_addMessage('Veuillez compl�ter les champs obligatoires');
				return;
			}
			if ($k == 'motif')
				$v = $this->_motifContact[(int)$v];
			$data[$k] = htmlentities(strip_tags($v));
		}
		// email contact
		$html = load_and_parse(BP.'/emails/em8.html', $data);
		send_mail ($html, EMAIL_CONTACT, SUBJECT_CONTACT, EMAIL_FROM, EMAIL_FROM_ADDR, '', BCC_EMAILS_TO);
		self::redirect(self::getURL('contact/confirmation.html'));
	}

	public function recommandezAction()
	{
		define('LIB_PATH_HTTP', '../lib');
		include_once 'lib/lib.captcha.php';

		if ($_POST['frm_parrainage'])
		{
			// r�gles de validation
			$rules = array(
				'exp_email' => $this->_addRuleEmail("L'adresse email de l'exp�diteur est obligatoire", "L'adresse email de l'exp�diteur n'est pas valide"),
				'dest_email' => $this->_addRuleEmail("L'adresse email du destinataire est obligatoire", "L'adresse email du destinataire n'est pas valide"),
				'exp_nom' => $this->_addRuleString(),
				'exp_prenom' => $this->_addRuleString(),
				'message' => $this->_addRuleString(),
			);
			$fields = array_keys($rules);
			$data = $_POST;
			$errors = array();
			foreach ($fields as $k) {
				if (isset($_POST[$k]) || $rules[$k]['empty'])
					$data[$k] = $this->_validateRule($data[$k], $rules[$k], $errors);
			}
			if (!captcha_check()) {
				$errors[] = "le cryptogramme visuel est incorrect";
			}
			// il y a des erreurs
			if ($errors) {
				// tableau des donn�es filtr�es
				if ( is_array($data) ) {
					$this->setData('post_data', $data);
				}
				$this->setData('is_form_valid', false);
				$this->_addMessage($errors);
			} else {
				$html = load_and_parse(TPL_EMAIL_PARRAINAGE, $data);
				send_mail($html, $data['dest_email'], SUBJECT_PARRAINAGE, EMAIL_FROM, EMAIL_FROM_ADDR);
				$this->setData('is_form_valid', true);
				$this->setData('parrainage_dest_email', $data['dest_email']);
			}
		}
	}
}
?>