<?
	function write_radio(&$row, $name, $value, $label, $default = false)
	{
		$id = $name.'_'.$value;
		echo '<input type="radio" id="'.$id.'" name="'.$name.'" value="'.$value.'"';
		if ($row[$name]==$value || (!$row && $default)) echo ' checked ';
		echo '><label for="'.$id.'">&nbsp;'.$label.'</label>';
	}
	function write_checkbox(&$row, $name, $id, $value, $label, $title = null)
	{
		echo '<input type="checkbox" name="'.$name.'[]" value="'.$value.'" id="'.$name.'_'.$id.'"';
		if ($row[$name] & $value) echo ' checked="checked"';
		echo '><label for="'.$name.'_'.$id.'" title="'.$title.'">&nbsp;'.$label.'</label>';
	}
	
	// enregistre un alias � partir des informations de l'alias ou du point de vente
	/**
	* enregistre dans agence_alias depuis un point de vente ou un alias
	*/
	function save_alias($row, $do_map = true)	// le row d'un point de vente
	{
		if (!$row['agence']) die("Vous devez pr�ciser l'agence pour cr�er un alias");
		// on obtient un identifiant si n�cessiare
		if (!$row['id'])
		{
			// il s'agit d'une cr�ation
			list ($row['zone'], $row['statut'], $row['emplacement']) = getsql("select zone, statut, emplacement from agence_pdv where id='".$row['agence']."'");
			$row['id'] = agence_get_id($row['agence']);
			$row['pageweb'] = 1;
		}
		// s'il s'agit de l'alias li� � un point de vente
		if (!$row['pdv'])
			$row['pdv'] = $row['id'];
		// champs trait�s
		if ($row['canon'])
			$do_map = false;
		else
			$row['canon'] = canonize($row['nom'], 'agence_alias');
		$row['recherche'] = trim($row['nom']).' ('
			.( $row['zone']=='fr' ? substr($row['cp'], 0, 2) : getsql("SELECT nom FROM zone WHERE id='".$row['zone']."'") )
			.')';
		if($row['zone']=='fr')
			$row['dept'] = substr($row['cp'], 0, 2);
		save_record('agence_alias', $row);
		if ($row['pageweb']) // on n'enregistre pas les alias sans page web
			save_record('refnat_canon', $a = array('type'=>'agence','id'=>$row['id'], 'canon'=>$row['canon'], 'nom'=>$row['nom']));
		if ($do_map)
		{
			// r��crire map.txt
			include 'map.php';
		}
		return $row['id'];
	}
	
	/* renvioe un nouvel identifiant pour point de vente ou alias */
	function agence_get_id($agence)
	{
		// quelle lettre pour le denier pdv ?
		$last_pdv = getsql('SELECT MAX(RIGHT(id,1)) FROM agence_alias WHERE agence = "'.$agence.'"');
		if(!$last_pdv) // c'est un nouveau pdv
			return $agence;
		// il existe deja un pdv pour l'agence
		// on renvoie l'identifiant suivant
		return substr($agence, 0,strlen($agence)-1).chr(ord($last_pdv)+1);
	}
	
	// met � jour les tatut
	function agence_update_statut ($agence)
	{
		sqlexec("UPDATE agence_pdv ap JOIN agence a ON a.id=ap.agence SET ap.statut = ap.publie * a.statut WHERE a.id LIKE '".addslashes($agence)."'");
		sqlexec("UPDATE agence_alias aa JOIN agence_pdv ap ON aa.pdv=ap.id SET aa.statut=ap.statut WHERE ap.agence LIKE '".addslashes($agence)."'");
		
	}
	
	// affiche la liste des points de vente et des alias
	function show_pdv($agence, $pdv=null, $alias=null)
	{
		if (!$agence) return;
	?>
		<table class="framed" border="0" cellspacing="0" cellpadding="4" style="float:left; margin-left:5px; width:300px;">
		<tr>
		<th class="bg_titre">
			<img src="img/puce.gif" align="absmiddle" hspace="1"> Points de vente / Alias
		</th>
		</tr>
		<tr>
		<td>
		<?
		echo '<ul class="pdv_list">';
		$sql  = 'SELECT id, agence, nom'
				.'	FROM agence_pdv '
				.'	WHERE agence = "'.$agence.'"'
				.'	ORDER BY id';
		$rs = sqlexec($sql);
		while($rs && $row = mysql_fetch_assoc($rs))
		{
			echo '<li><span';
			if($row['id']==$pdv)
				echo ' class="actif">'.$row['nom'];
			else
				echo '><a href="agence_pdv.php?id='.$row['id'].'&agence='.$row['agence'].'">'.$row['nom'].'</a>';
			echo '</span>';
			$sql  = 'SELECT id, nom, ville'
					.'	FROM agence_alias '
					.'	WHERE pdv LIKE "'.$row['id'].'"'
					.'	ORDER BY id';
			$rs2 = sqlexec($sql);
			echo '<ul>';
			if(mysql_num_rows($rs2))
			{
				while($rs2 && $row2 = mysql_fetch_assoc($rs2))
				{
					echo '<li><span';
					if($row2['id']==$alias)
						echo ' class="actif">'.$row2['nom'].' ('.$row2['ville'].')';
					else
						echo '><a href="agence_alias.php?id='.$row2['id'].'">'.$row2['nom'].' ('.$row2['ville'].')</a>';
					echo '</span></li>';
				}
			}
			echo '<li><span><a href="agence_alias.php?agence='.$agence.'&pdv='.$row['id'].'">nouvau alias</a></span></li>';
			echo '</ul>';
			echo '</li>';
		}
		echo '<li><span><a href="agence_pdv.php?agence='.$agence.'">nouveau point de vente</a></span></li>';
		echo '</ul>';
		?>
		</td>
		</tr>
		<tr>
			<? list($code_base, $code_societe) = getsql("select code_base, code_societe from agence where id='$agence'"); ?>
			<td><a target="_blank" href="../stats_franchises/?code_societe=<?=$code_societe?>&key=<?=stats_franchise_key($code_societe)?><? if ($pdv) echo '&agence='.$pdv?>">Stats Franchis�</a></td>
		</tr>
		<?php
		global $adminUser;
		if ($adminUser) {
			foreach(AccesAgence::$ROLES as $role => $info) {
				if ($role == 'admin') continue;
				$lbl = 'Resas '.$info['label'];
				$url = ( $role=='manager' ? 'resas/admin/index.html' : 'resas/');
				$key = AccesAgence::getKey($role, $adminUser['login'], $agence);
				echo '<tr><td>';
				$href = sprintf($url.'?role=%s&login=%s&agence=%s&key=%s', $role, $adminUser['login'], $agence, $key);
				echo '<a target="_blenk" href="../'.$href.'"/>'.$lbl.'</a>';
				echo '</td></tr>';
			}
		}
		?>
		</table>
		<?
	}
	
	/**
	* �crire le fichier maptxt
	* @return array ce qui a �t� produit
	*/
	function make_maptxt()
	{
		// on peut continuer � g�n�rer un RewriteMap
		$fname = dirname(dirname(__file__)).'/cache/map/map.txt';
		$f = fopen($fname, 'wt');

		// zones
		$cnt = 0; $rs = sqlexec("select id, canon from zone where id!='fr'");
		while ($row = mysql_fetch_assoc($rs))
		{
			$cnt++; fwrite($f, gu_agence($row)."\tindex.php?page=agences/liste&zone=".$row['id']."\n");
		}
		$output[] = "$cnt zones";
		
		// emplacement
		$cnt = 0; $rs = sqlexec('select id, canon from agence_emplacement where id > 1');
		while ($row = mysql_fetch_assoc($rs))
		{
			$cnt++; fwrite($f, gu_agence($row)."\tindex.php?page=agences/liste&emplacement=".$row['id']."\n");
		}
		$output[] = "$cnt emplacements";
		
		// agglom�rations
		$cnt = 0; $rs = sqlexec('select id, canon from agence_agglo');
		while ($row = mysql_fetch_assoc($rs))
		{
			$cnt++; fwrite($f, gu_agence($row)."\tindex.php?page=agences/liste&agglo=".$row['id']."\n");
		}
		$output[] = "$cnt agglom�rations";

		// la page promo/index
		fwrite($f, gu_promo (null)."\tindex.php?page=promo/index\n");
		fwrite($f, gu_promo (null, 'vu')."\tindex.php?page=promo/index&type=vu\n");

		// d�partement
		$cnt = 0; $rs = sqlexec("select id, canon from dept where zone in('fr','co')");
		while ($row = mysql_fetch_assoc($rs))
		{
			fwrite($f, gu_agence($row)."\tindex.php?page=agences/liste&dept=".$row['id']."\n");
			fwrite($f, gu_promo ($row)."\tindex.php?page=promo/index&dept=".$row['id']."\n");
			fwrite($f, gu_promo ($row, 'vu')."\tindex.php?page=promo/index&type=vu&dept=".$row['id']."\n");
			$cnt++;
		}
		$output[] = "$cnt d�partements";
	
		// agences
		$cnt = 0; $rs = sqlexec("select id, canon from agence_alias where (statut & 1)=1");
		while ($row = mysql_fetch_assoc($rs))
		{
			fwrite($f, gu_agence($row)."\tindex.php?page=agences/agence&agence=".$row['id']."\n");
			fwrite($f, gu_promo($row)."\tindex.php?page=promo/index&agence=".$row['id']."\n");
			fwrite($f, gu_promo($row, 'vu')."\tindex.php?page=promo/index&type=vu&agence=".$row['id']."\n");
			fwrite($f, gu_recherche($row, 'vp')."\tindex.php?agence=".$row['id']."&type=vp\n");
			fwrite($f, gu_recherche($row, 'vu')."\tindex.php?agence=".$row['id']."&type=vu\n");
			fwrite($f, gu_recherche($row, 'sp')."\tindex.php?agence=".$row['id']."&type=sp\n");
			fwrite($f, gu_recherche($row, 'ml')."\tindex.php?agence=".$row['id']."&type=el\n");
			$cnt++;
		}
		$output[] = "$cnt agences";
		// c'est termin� !
		fclose($f);
		return $output;
	}
?>
