<?php
/**
* Affiche un forfait
*/
	/** @var Forfait */
	$forfait = $this;
	$categorie = $forfait->getCategorie();
	$type = strtolower($categorie['type']);
	if ($forfait->getButtons())
	{
		$form_action = ($forfait->isLocapass() ? '../reservation.php' : '../reservation/tarif.html');
		echo "\t".'<form action="'.$form_action.'" method="post">'."\n";
		echo "\t".'<input type="hidden" name="forfait" value="'.htmlentities($forfait->doSerialize()).'">'."\n";
		if ($forfait->isLocapass())
			echo "\t".'<input type="hidden" name="key" value="'.Page::getKey('reservation').'">'."\n";
		copy_form($_POST);
	}
?>
	<div class="offre">
		<div class="offre_vehicule">
			<span class="ong_categorie">Cat�gorie</span>
			<div class="apercu">
				<img src="../<?=$categorie->getImg('med')?>" title="<?=$categorie['nom']?>" alt="<?=$categorie['nom']?>" />
			</div>
			<p>
				<strong>Cat�gorie <?=$categorie['mnem']?><br />
				<?=$categorie['nom']?></strong>
				<?=$categorie['description']?>
			</p>
			<div class="clear"></div>
		</div>

		<table cellpadding="0" cellspacing="0" border="0" style="float:left">
			<tr>
				<td valign="top">
					<!-- onglet dur�e -->
					<span class="ong_duree">Dur�e</span>
					<p class="dureekm"><?=$forfait->getDuree()?></p>
 				</td>
				<? if (!$forfait->isLocapass()) { ?>
				<td valign="top">
					<!-- onglet km inclus -->
					<span class="ong_km">Km inclus</span>
					<p class="dureekm"><?=$forfait->getKm();?></p>
					<?
					if ($forfait->hasPromo())
						echo "\t".'<p class="txtpromo">au lieu de</p>'."\n";
					?>
				</td>
				<td valign="top" align="center">
					<!-- onglet tarif TTC -->
					<span class="ong_tarif">Tarif TTC</span>
					<p class="prixrouge" style="margin-top:5px;">
						<?=$forfait->getPrix().'<span class="eurorouge">&euro;</span>';?>
					</p>
					<?
					if ($forfait->hasPromo()) 
						echo "\t".'<p class="prixbarre">'.$forfait->getPrixBarre().'<span>&euro;</span></p>'."\n";
					if ($kmSupp = $forfait->getPrixKmSupp())
						echo show_money($kmSupp).'&euro; le km supp.<br />'."\n";
					echo '<a href="../popup.php?page=reservation/details&agence='.$forfait->getAgence()->getId().'&km_sup='.$kmSupp.'" onclick="return wOpen(this.href, 570, 551)"><img src="../img/bt_details2.gif" alt="D�tails" title="D�tails" /></a>'."\n";
					?>
				</td>
				<? } else { // !$isLocapass
				?>
				<td width="180" align="center">
					<img src="../img/logo_locapass.gif" alt="locapass" /><br />
				</td>
				<? } ?>
			</tr>
		</table>
		<div class="clear"></div>
		<div class="offre_specs">
		<?
			echo $categorie->getSpecifications().'<br clear="all" />';
			if ($surcharge = $forfait->getSurcharge())
				echo '<p class="agence_surcharge">Ce tarif ne comprend pas <strong>la '.strtolower($forfait->getAgence()->getSurchargeLabel()).'</strong> de <span class="prixrouge">'.$surcharge.' �</span>, applicable dans cette agence.</p>';
		?>
		</div>
		<div class="bottom"></div>
	</div>
	
	<!-- boutons -->
<?
	if ($forfait->hasButton(Forfait::BTN_CHANGE))
	{
?>
	<div class="gauche impr" style="margin:0 0 20px;"><a href="../" id="bt_modifiez2"><img border="0" src="../img/bt_modifiez2.gif" alt="Modifiez votre demande" title="Modifiez votre demande" /></a></div>
<?
	}
	if ($forfait->hasButton(Forfait::BTN_BOOK))
	{
?>
	<div class="droite impr" style="margin-right: 5px;"><input type="image" name="reserver" src="../img/bt_reservez.gif" alt="R�servez" title="R�servez" /></div>
<?
	}
?>
	<p class="clear"></p>
<?
		if ($forfait->getButtons())
			echo "</form>";
?>