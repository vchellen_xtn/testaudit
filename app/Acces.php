<?php
class Acces extends Ada_Object
{
	const SB_ADMIN		= 128;
	const SB_OPERATEUR  = 64; 	// pour rechercher et annuler des r�servations
	const SB_OFFRES		= 32;
	const SB_GESTION	= 16;
	const SB_SATISFACTION = 8;
	const SB_EDITO		= 4;
	const SB_TC			= 2;
	const SB_LOGIN		= 1;
	
	const ROLE_ADMIN		= 255;	// acc�s complet
	const ROLE_TC			= 2;	// ne peut pas se connecter � l'admin
	const ROLE_SATISFACTION	= 9;	// Acces::SB_LOGIN + Acces::SB_SATISFACTION
	const ROLE_EDITO 		= 5;	// Acces::SB_LOGIN + Acces::SB_EDITO 
	const ROLE_GESTION		= 17;	// Acces::SB_LOGIN + Acces::SB_GESTION
	const ROLE_ZONE			= 33;	// Acces::SB_LOGIN + Acces::SB_OFFRES
	const ROLE_OPERATEUR	= 65;	// Acces::SB_LOGIN + Acces::SB_OPERATEUR

	static public $ROLES = array
	(
		Acces::ROLE_ADMIN	=> "Administrateur", 
		Acces::ROLE_TC		=> "T�l� Conseiller", 
		Acces::ROLE_SATISFACTION => "Enqu�te satisfaction",
		Acces::ROLE_EDITO 	=> "Editeur", 
		Acces::ROLE_GESTION => "Gestionnaire", 
		Acces::ROLE_ZONE	=> "Gestionnaie zone",
		Acces::ROLE_OPERATEUR => "Op�rateur R�servation",
	);


	/** @var Zone_Collection */
	protected $_zones;
	/** @var Zone  */
	protected $_currentZone; // identifiant de la zone courante
	
	/**
	* Cr�er l'acc�s associ�e � un login
	* 
	* @param mixed $login
	* @returns Acces
	*/
	static public function factory($login)
	{
		// on filtre les caract�res possibles
		$login = preg_replace('/[^a-z0-9\-\_\.]/i', '', $login);
		$x = new Acces();
		if ($x->load($login)->isEmpty())
			return null;
		return $x;
	}
	/**
	* Cr�e un acc�s � partir d'un Cookie
	* 
	* @param string $cookie
	* @return Acces
	*/
	static public function createFromCookie($cookie)
	{
		if (!$cookie) return null;
		list($login, $zone) = self::_decryptData($cookie);
		if ($login)
		{
			$x = Acces::factory($login);
			if ($x && $zone)
				$x->setCurrentZone($zone);
			return $x;
		}
	}
	/**
	* Renvoie les donn�es devant �tre stock�es dans le cookie
	* @returns string
	*/
	public function getCookie()
	{
		return self::_encryptData(array($this->getId(), $this->getCurrentZoneID()));
	}
	/**
	* Constructeur appel� par Ada_Object
	* on red�finit la cl� primaire sur la table acces
	*/
	public function _construct()
	{
		$this->setIdFieldName('login');
	}
	protected function _prepareSQL($id, $cols='*', $data=null)
	{
		return parent::_prepareSQL($id, $cols, $data).' and statut=1';
	}

	/**
	* Renvoie la lise des zones associ�es � un acc�s
	* @returns Zone_Collection
	*/
	public function getZones()
	{
		if (!$this->_zones && !$this->isEmpty())
		{
			$this->_zones = new Zone_Collection();
			$sql = "select z.* from zone z join acces_zone az on az.zone=z.id where az.acces='".$this->getId()."' order by z.nom";
			$this->_zones->loadFromSQL($sql);
		}
		return $this->_zones;
	}
	/**
	* Renvoie la zone couranate
	* @returns Zone
	*/
	public function getCurrentZone()
	{
		return $this->_currentZone;
	}
	public function getCurrentZoneID()
	{
		if ($this->_currentZone)
			return $this->_currentZone->getId();
	}
	/**
	* Modifie la zone courante
	* 
	* @param string $zone
	* @returns Acces
	*/
	public function setCurrentZone($zone)
	{
		// on change la zone si elle est dans la liste des zones autoris�es
		$zones = $this->getZones();
		if ($zones)
			$this->_currentZone = $zones[$zone];
		return $this;
	}
	// v�rifier le mot de passe
	public function checkPassword($pwd)
	{
		if ($this->isEmpty()) return false;
		$a = getsql("select password('".addslashes($pwd)."'), old_password('".addslashes($pwd)."')");
		if (is_array($a) && in_array($this->_data['password'], $a))
			return true;
		return false;
	}
	/**
	* V�rifie un droit
	* 
	* @param mixed $right	un droit ou un tableau de droits dont au moins un doit �tre valide
	* @returns bool
	*/
	public function hasRights($right)
	{
		if (is_null($right)) {
			return false;
		}
		if (is_array($right)) {
			foreach($right as $r) {
				if ($this->hasRights($r)) {
					return true;
				}
			}
			return false;
		}
		return (($this->_data['droits'] & $right) == $right);
	}
	/**
	* V�rification d'un r�le en particulier
	* 
	* @param int $role
	* @returns bool
	*/
	public function isRole($role) {
		return ($this->_data['droits'] == $role);
	}
	/**
	* Renvoie si l'administrateur auhtentifi� est un t�l� conseiller
	* @returns bool
	*/
	public function isTeleConseiller() {
		if (true && SITE_MODE == 'DEV') {
			return true;
		}
		return $this->isRole(self::ROLE_TC);
	}
	
	/**
	* V�rifie si l'utilisateur a le droit sur une agence
	* 
	* @param int $right
	* @param string $agence
	* @returns bool
	*/
	public function hasRightsOnAgence($right, $agence)
	{
		// les administrateurs ont tous les droits
		if ($this->_data['droits'] & self::SB_ADMIN)
			return true;
		return false;
	}
	
	/**
	* Ecrire le menu Admin
	* 
	* @param string $page
	* @param string $args
	*/
	public function writeEditAdmin($page, $args)
	{
		if (!$this->hasRights(self::SB_EDITO) && !$this->hasRights(self::SB_ADMIN))
			return;
		echo '<p style="position: absolute; left: 1em; top: 1em; z-index: 1000; background: white; border: 1px dotted red; padding: 0.5em;">';
		if ($this->hasRights(self::SB_EDITO))
		{
			echo '<strong><a href="../admin/page.php?id='.$page.'" target="_blank">Editer informations page '.$page.'</a></strong><br/>';
			if (true || $page == 'annuaire/index')
			{
				foreach (RefnatCanon::$TYPES as $k => $info)
				{
					if ($args[$k])
					{
						//echo '<strong><a href="../admin/refnat.php?tbl=refnat&id='.$k.'" target="_blank">Editer texte pour '.$k.' [par d�faut]</a></strong><br/>';
						echo '<strong><a href="../admin/refnat.php?tbl='.$k.'&id='.urlencode($args[$k]).'" target="_blank">Editer texte pour '.$k.' ['.htmlspecialchars($args[$k]).']</a></strong><br/>';
					}
				}
				echo "<br/>";
			}
		}
		if ($this->hasRights(self::SB_ADMIN))
		{
			if (($page == 'agences/agence' || $page=='annuaire/index') && $args['agence'])
				echo '<strong><a href="../admin/agence_alias.php?id='.urlencode($args['id'] ? $args['id'] : $args['agence']).'" target="_blank">Editer informations agence</a></strong><br/>';
			if (($page == 'agences/liste' || $page == 'annuaire/index') && $args['dept'])
				echo '<strong><a href="../admin/dept.php?id='.urlencode($args['dept']).'" target="_blank">Editer informations d�partement</a></strong><br/>';
		}
		echo '</p>';
	}
}
?>
