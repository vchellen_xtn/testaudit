<div id="slides">
	<div class="slides_container">
	<? foreach (VisuelPromo_Collection::factory(false, 100, 199) as /** @var VisuelPromo */ $row): ?>
		<div class="slide" title="<?= $row['commentaire'] ?>" data-time="<?= $row['time'] ?>">
			<?= $row->showVisuel($this->getBaseURL(false),'',''); ?>
		</div>
	<? endforeach; ?>
	</div>
	<div class="bg">
		<? include ($this->getPagePath()); ?>
	</div>
	<div class="bannieres-bottom">
		<?php $this->showBannieres('bottom'); ?>
		<div style="min-height:1px; width:226px; display:inline-block;" class="banniere-bottom"></div>
		<div style="min-height:1px; width:226px; display:inline-block;" class="banniere-bottom"></div>
		<div style="min-height:1px; width:226px; display:inline-block;" class="banniere-bottom"></div>
	</div>
</div>

<div class="align-center" style="margin-bottom:15px;">
	<div class="fb-like" data-href="http://www.ada.fr/" data-layout="standard" data-action="like" data-show-faces="false" data-share="false"></div>
</div>

<?
if (!$this->isNoIndex() && !isset($_GET['agence']) && !isset($_GET['ville']) && !isset($_GET['type']) && !isset($_GET['categorie']))
{
	$googleplus_url = Agence::getADAGooglePlusURL();
	$baseline = <<<BASELINE
				<ul class="baseline">
					<li>ADA est le leader fran�ais de la location de voiture et utilitaire de proximit� en France, nous poss�dons la plus importante s�lection de v�hicules de tourisme en m�tropole, partag�s dans un r�seau de plus de 480 agences de location automobile en France et o� nous vous pr�sentons de nombreuses offres et promotions sur vos locations de voiture pas ch�re. Nos agences ADA sont visibles dans l�ensemble des grands a�roports, les gares et importantes villes de tout l'hexagone et aux DOM-TOM.</li>
					<li>Une r�servation claire et simple de votre location de voiture et utilitaire en France avec de nombreuses offres pour une <a href="{$here}location-voiture-paris.html">location de voiture � Paris</a>, une location auto � Nice, une location de voiture � Marseille, une <a href="{$here}location-voiture-bordeaux.html">location voiture � Bordeaux</a> ou un leasing � Lyon.</li>
					<li>Pour les <a href="{$here}demenagement/index.html">d�m�nagements</a>, les livraisons, le transport de mat�riaux lourds et encombrants, ADA vous propose �galement la location utilitaire. Afin de pouvoir transporter tout type de volumes, quels qu'ils soient, ADA met � votre disposition une large s�lection de v�hicule pas cher. Petite fourgonnette, camion-benne ou fourgon, des professionnels vous guident. Notre offre ADA 1er prix vous permettra une location voiture ou une location utilitaire d�s 18 ans � 29 euros la journ�e voiture et 39 euros la journ�e utilitaire, elle comprend une assurance et une assistance de 50km.</li>
					<li>ADA est �galement pr�sent � l'international et vous permet aussi louer � l'�tranger. Pour une <a href="{$here}location-voiture-guadeloupe.html">location voiture Guadeloupe</a>, en Martinique, en <a href="{$here}location-voiture-corse.html">Corse</a> ou � la R�union, ADA propose aussi ses services. Pour tout forfait de 1 � 21 jours, la r�servation peut se faire facilement et rapidement sur internet, au-del�, il est n�cessaire de se rapprocher de l'agence ADA la plus proche afin d'obtenir davantage de renseignements. Vous pouvez �galement <a href="{$googleplus_url}" target="_blank">nous retrouver sur Google+</a></li>
					<li>Sp�cialis� dans la location voiture et la location utilitaire, ADA pr�sente <a href="{$here}vehicules/tourisme.html">une large gamme de v�hicules</a> de grandes marques, parfaits pour tous les d�placements. De la petite citadine pour les vacances � deux au monospace pour les longs s�jours en famille, de nombreux mod�les sont disponibles. Ces derniers s'adaptent � tous les besoins et � toutes les envies. Afin de r�pondre � tous les budgets, ADA offre un vaste choix allant du cabriolet au 4x4 en passant par les compactes, des petites voitures et des v�hicules haut de gamme, des voitures sans permis et des deux-roues ainsi que des utilitaires pour vos d�placements, vos d�m�nagements ou pour votre travail au quotidien.</li>
				</ul>
BASELINE;
	$this->setData('baseline', $baseline);
}
?>
<script type="text/javascript">
// <![CDATA[
	if (typeof $ !== "undefined")
	{
		// gestion de la nav principale
		$(document).ready(function() {
			// les publicit�s
			if ($.fn.slides)
			{
				$('#slides').slides({
					preload: false,
					preloadImage: '../img/ajax-loader.gif',
					play: 3000,
					effect: 'fade',
					generateNextPrev: true,
					generatePagination: false
				});
			}
		});
	}
	// ]]>
</script>
