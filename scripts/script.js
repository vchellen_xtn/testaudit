// wOpen
function wOpen(href, width, height){
	w = window.open(href, 'target', 'toolbar=0, location=0, directories=0, status=1, scrollbars=1, resizable=1, copyhistory=0, menuBar=1, width='+width+', height='+height+', left=75, top=75')
	w.focus();
	return false;
}

// popupflash
function popup(url, titre, largeur, hauteur, scroll){
	window.open(url, titre, 'width=' + largeur + ', height=' + hauteur + ', scrollbars=' + scroll);
}

function addLoadEvent(func)
{
	var oldonload = window.onload;
	if (typeof window.onload != 'function') {
		window.onload = func;
	}
	else {
		window.onload = function() {
			oldonload();
			func();
		}
	}
}

// pour redimmensionner le bloc central en fonction des colonnes lat�rales
function redimensionner()
{
	var columnHeight;
	var columnsCenter = document.getElementById("cellule");	// bloc central

	if(!columnsCenter)
		return;

	if(document.getElementById("column1"))
		columnHeight = document.getElementById("column1").offsetHeight;
	else if(document.getElementById("column3_1"))
		columnHeight = document.getElementById("column3_1").offsetHeight;

	if(!columnHeight)
		return;

	if (columnHeight > columnsCenter.offsetHeight)
	{
		// modif WD le 22/04/09 pour �viter le r�dimensionnement inopportun des promotions
		//columnsCenter.style.height= (columnHeight-27)+"px";
	}
	else if(columnHeight < columnsCenter.offsetHeight)
	{
		// le petit coin bas gauche de la colonne centrale
		var footerCorner = document.getElementById("footerCorner");
		if(footerCorner && footerCorner.className)
			footerCorner.className = footerCorner.className.replace('gauche corner_bottom_left_red2', 'gauche corner_bottom_left_red');
	}
}

// populateMenu
function populateMenu(xml, ctrl, def_val)
{
	if (!ctrl)
		return;
	// m�morise la valeur active
	val = (ctrl.selectedIndex >= 0) ? ctrl.options[ctrl.selectedIndex].value : "";
	if (!val || !val.length)
		val = def_val;
	// ne garde que la premi�re option si value=""
	ctrl.options.length = (ctrl.options.length > 0 && (ctrl.options[0].value=='' || ctrl.options[0].value=='%')) ? 1 : 0;
	// xml vide => ne rien faire
	if (!xml)
		return;
	// recr�er les �l�ments dans l'ordre
	for (var i=0, lst=xml.getElementsByTagName("option"); i<lst.length; i++)
	{
		selected = ((i == 0 && ctrl.name == "h1") || (lst[i].getAttribute("val") == val));
		ctrl.options.add(new Option(lst[i].getAttribute("title"), lst[i].getAttribute("val"), false, selected));
	}
}

// validateSelect
function validateSelect(lst) {
	return (lst && lst.selectedIndex >= 0 && lst.options[lst.selectedIndex].value.length > 0);
}
// validateEmail
function validateEmail (what) {
	var s = (what.value ? what.value : what);
	return (s && s.match && s.match(/^[a-z0-9\.\-\_]+@([a-z0-9\-]+\.)+[a-z]{2,4}$/i));
}
// validatePhone
function validatePhone (what) {
	var s = (what.value ? what.value : what);
	return (s && s.match && s.match(/^\+?[0-9 \.\-]{10,18}$/));
}
// validateDate
function validateDate (what) {
	var s = (what.value ? what.value : what);
	if (!s || !s.split) return false;
	var a = s.split(/[\/\-\:]/), d = new Date(a[2], a[1]-1, a[0]);
	return (!isNaN( d ) && a[2] > 0 && d.getYear()%100==a[2]%100 && d.getMonth()==a[1]-1 && d.getDate()==a[0]);
}

// validateChecked : checkbox ou radio
function validateChecked(c, cnt)
{
	if (cnt == null)
		cnt = (c.length ? c.length : 1)
	if (cnt == 1)
		return (c.checked);
	for (i=0; i<cnt; i++)
		if (c[i].checked) return true;
	return false;
}

function validateNum(what)
{
	var s = (what.value ? what.value : what);
	return s.match(/^\d+(\.\d{2})?$/);
}


var appADA = appADA || {};

if (typeof jQuery != 'undefined') {
	(function ($) {
		appADA.showPopIn = function (popin, args, options) {
			if (!options || typeof(options) == 'undefined') {
				options = {};
			}
			options = $.extend({
				dialogClass: "popin",
				draggable: false,
				resizable: false,
				width: 766,
				modal: true,
				closeText: "x"
			}, options);
			if (typeof($("#dialog-popin").dialog('instance')) !== 'undefined') {
				$("#dialog-popin").dialog('destroy');
			}
			$.post('../popin/' + popin + '.html', args, function (data) {
				$("#dialog-popin-msg").html(data);
				$("#dialog-popin").dialog(options);
			}, 'html');
			$(appADA).trigger("showPopin", [popin, args]);
			return false;
		}
		appADA.show360 = function (categorie, page, type, mnem) {
			this.showPopIn('categorie_360', {"categorie": categorie}, {
				dialogClass: 'popin-360',
				draggable: false,
				resizable: false,
				width: 700,
				height: 650
			});
			if (typeof(sp_clic) == "function") {
				sp_clic('N', 'vehicule_360', page, type + "_" + mnem.replace('+', "'"));
			}
			return false;
		}
		// scroll du body
		appADA.bodyScrollTo = function (selector, duration) {
			if (typeof($) === 'undefined') {
				return;
			}
			var targetTop = 0;
			if ($(selector).length) {
				targetTop = $(selector).offset().top;
			} else if ($.isNumeric(selector)) {
				targetTop = selector;
			}
			if (!$.isNumeric(duration)) {
				duration = 700;
			}
			$('body,html').animate({scrollTop: targetTop}, duration);
		}
		appADA.showOverlayLoader = function () {
			$('<div class="loader-search"></div>').appendTo(document.body).addClass('ui-widget-overlay').css({
				width: $(document.body).width(),
				height: $(document.body).height(),
				'z-index': '1000',
				'background': '#000 url(../css/img/structure-2014/ajax-loader.gif) no-repeat center 600px',
				'opacity': '0.5'
			});
		}
		appADA.hideOverlayLoader = function () {
			$('div.loader-search').remove();
		}
		// afficher le dialog
		appADA.showDialog = function (selector, msg, options) {
			options = $.extend({
				resizable: false,
				minHeight: 20,
				width: 400,
				modal: true,
				buttons: {
					'OK': function () {
						$(this).dialog('close');
					}
				}
			}, options);
			if (typeof($(selector).dialog('instance')) != 'undefined') {
				$(selector).dialog('destroy');
			}
			$(selector + '-msg').html(msg);
			$(selector).dialog(options);
			// ajout de classes sur les boutons
			$.each($(selector).dialog('option', 'buttons'), function (i) {
				$(selector + ' + .ui-dialog-buttonpane button:contains("' + i + '")').addClass(i.toLowerCase().replace(/[^a-z0-9]+/g, '_'));
			});
		}
		// initialisation des comportements
		appADA.init = function () {
			/* gestion des toggle : control�e par data-target et data-speed */
			$(".trigger--toggle--block").click(function () {
				var speed = $(this).data("speed");
				if (!speed) speed = "slow";
				var $target = $(this).parents(".toggle--wrapper:first").find(".toggle--block");
				if ($(this).data("target")) {
					$target = $($(this).data("target"));
				}
				// activer / d�sactiver les trigger
				$triggers = $(this).parents(".toggle--wrapper:first").find(".trigger--toggle--block");
				if ($target.is(":visible")) {
					$triggers.removeClass("active");
				} else {
					$triggers.addClass("active");
				}
				$target.slideToggle(speed);
				return false;
			});
			// si le hash indique un �l�ment d'un triiger, on l'ouvre
			if (window.location.hash) {
				$hash = $(window.location.hash);
				if ($hash.is(":hidden")) {
					// on essaye d'ouvrir
					$hash.parents(".toggle--wrapper:first").find(".trigger--toggle--block:first").click();
				}
			}
			// cookieCNIL
			if (typeof(cookieCNIL) == "object") {
				cookieCNIL.init({
					text: "ADA utilise des cookies pour vous proposer la meilleure exp�rience en ligne possible.<br>En continuant la navigation sur notre site, vous accepterez l'utilisation des cookies.",
					url: "../conditions/internet.html#article-cookie",
					css: {
						background: '#000',
						color: '#FFF',
						position: 'fixed',
						border: '0px',
						padding: '1%',
						bottom: '0',
						width: '98%'
					}
				});
			}
		};
		$(document).ready(appADA.init);
	})(jQuery);
}