<?
	require_once("secure.php"); 
	require_once("lib.php");

	// identifiant
	$id = getarg("id");
	
	// suppression
	if ($id && getarg("delete")) 
	{
		$sql = "delete from vehicule_famille where id = '".$id."'";
		getsql($sql);
		unset($id);
	}
	
	// modification, cr�ation
	if (getarg("update") || getarg("create"))
	{
		save_record('vehicule_famille', $_POST);
		if (!$id) $id = mysql_insert_id();
	}
	if ($id)
	{
		$rs = sqlexec("select * from vehicule_famille where id = '".$id."' ");
		if (!$rs || !($row = mysql_fetch_assoc($rs)))
			die("Aucun enregistrement correspondant.");
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script>
	<!-- //
	// doDelete
	function doDelete(frm)
	{
		return (confirm("Voulez-vous r�ellement supprimer cet �l�ment ?\nCette op�ration est irr�versible."));
	}
	
	// doSubmit
	function doSubmit(frm)
	{
		var err = "";
		if(!frm.type.options[frm.type.selectedIndex].value)
			err += "Veuillez indiquer un type\n";
		if(!frm.nom.value.length)
			err+= "Veuillez indiquer le nom du mod�le\n";
		if (!frm.position.value.match(/^\d+$/))
			err+= "Veuillez indiquer une position valide\n";
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		return true;
	}
	// -->
	</script>
</head>
<body>
<div style="float: right; padding: 1em; border: 1px dotted grey; margin: 2em;">
</div>
<form action="" method="post" onsubmit="return doSubmit(this);">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre" colspan="2">
	<img src="img/puce.gif" align="absmiddle" hspace="1">
<?
	echo "Famille";
?>
	</th>
</tr>
<tr>
	<td colspan="2">
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="15%">
		<col width="35%">
		<col width="15%">
		<col width="35%">
		<tr>
			<td><label for="id">Id</label></td>
			<td>
				<?
				echo $id;
				echo '<input type="hidden" name="id" value="'.$id.'">';
				?>
			</td>
			<td><label for="type">Type</label></td>
			<td>
			<? 
				$sql = 'select id, nom from vehicule_type';
				if ($row['type']) $sql.=" where id='".$row['type']."'";
				$sql.=' order by id';
				selectsql('type', $sql, $row['type'], '--'); 
			?>
			</td>
		</tr>
		<tr>
			<td><label for="nom">Nom</label></td>
			<td><input type="text" name="nom" id="nom" value="<?=$row['nom']?>" maxlength="48" size="48"></td>
			<td><label for="categorie_defaut">Cat. par d�faut</label></td>
			<td><? selectsql('categorie_defaut', "select id, concat(mnem,' - ',nom) from categorie where famille='".$id."' order by mnem", $row['categorie_defaut'], '--'); ?></td>
		</tr>
		<tr>
			<td><label for="position">Position</label></td>
			<td><input type="text" id="position" name="position" value="<?=$row['position']?>" maxlength="2" size="2"/>
			<td><label for="suivant_1">Afficher suivant</label></td>
			<td>
				<input type="hidden" name="suivant[]" value="0"/>
				<input type="checkbox" name="suivant" id="suivant_1" value="1" <? if ($row['suivant']) echo ' checked';?>/>
			</td>
		</tr>
		<tr>
			<td><label for="description">Description</label></td>
			<td colspan="3"><textarea id="description" name="description" rows="2" style="width: 100%;"><?=$row['description'];?></textarea></td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td align="left">
	<? if ($id) { ?>
		<input type="button" onclick="parent.list.location.href='lists/categorie.php?famille=<?=$id?>'" value="cat�gorie">
	<? } ?>
	</td>
	<td align="right">
		<input type="submit" name="<?=$id ? "update" : "create"?>" value="enregistrer">
<?
	if ($id && !getsql("select count(*) from categorie where famille='".addslashes($id)."'") )
	{
?>
		&nbsp;
		<input type="submit" onclick="return doDelete(this.form);" name="delete" value="supprimer">
<?
	}
?>
	</td>
</tr>
</table>
</form>
</body>
</html>
