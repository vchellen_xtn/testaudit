<?
	include ('constant.php');
function xml_from_sql($element, $sql, $attrs = null, $fn = null) {
	header("Content-type: text/xml; charset=UTF-8");
	header("X-Robots-Tag: noindex");
	$rs = sqlexec(stripslashes($sql));

	echo '<'.'?xml version="1.0" encoding="utf-8"?'.'>'."\n";
	echo '<data count="'.mysql_num_rows($rs).'"';
	if ($attrs && is_array($attrs)) {
		foreach ($attrs as $k => $v)
			echo ' '.$k.'="'.$v.'"';
	}
	echo ">\n";
	while ($row = mysql_fetch_assoc($rs))
	{
		echo '<'.$element;
		foreach ($row as $key => $val) {
			if ($fn)
				$val = $fn($key, $val);
			echo ' '.$key.'="'.htmlspecialchars(utf8_encode($val)).'"';
		}
		echo '/>'."\n";
	}
	echo '</data>'."\n";
}
?>