<?php
class Page_Jeunes extends Page_Reservation
{
	protected function _getCustomScripts()
	{
		$action = $this->getAction();
		$files = array('js'=>array(), 'css'=>array());
		if ($action =='newsletter')
			$files['js'][] = 'scripts/jquery.validate.min.js';
		if ($action =='offre' || ($action=='resultat' && $this->isSkin('v2')))
		{
			$files['js'][] = 'scripts/form_reservation.js';
			$files['js'][] = 'scripts/agence_liste.js';
			if ($action == 'offre')
			{
				$files['js'][] = 'scripts/jquery.tagdragon.min.js';
			}
		}
		return $files;
	}
	
	/**
	* Renvoie un tableau pour la navigation
	* @returns array
	*/
	public function getNavResa($force = false)
	{
		if ($a = parent::getNavResa($this->getAction()=='offre'))
		{
			$a['resultat'] = 'Options';
			$a = array_merge(array('offre' => 'Offre'), $a);
		}
		return $a;
	}

	
	public function offreAction()
	{
		// v�rifiication du type de v�hicules renseign�s
		if (!Categorie::$TYPE[$_GET['type']])
			$_GET['type'] = 'vp';
	}
	
	public function resultatAction()
	{
		// appeler le r�sultat de Page_Reservation
		parent::resultatAction();
		// on change dynamiquement l'agence de groupe et la r�servation de forfait_type
		if (/** @var Reservation */ $reservation = $this->getData('reservation'))
		{
			// on r�cup�re les offres et on interdit les promotions
			if ($offres  = $reservation->getOffres())
			{
				$offres->setPromoRestrictions($reservation->getPartenaireCoupon() ? Forfait_Collection::PROMO_COUPON : Forfait_Collection::PROMO_NONE);
				// pr�ciser si le v�hicule est indisponible
				if (($offre = $offres->getOffreByCategory($reservation['categorie'])) && ($offre['nb'] <= 0))
					$this->setData('indispo', true);
			}
		}
	}
	
	// enregistre le formulaire de newsletter/index
	public function newsletterAction()
	{
		if (!count($_POST)) return;
		
		// on essaie de cr�er un prospect moyenne dur�e
		$errors = array();
		$_POST['origine'] = 'JEUNES';
		$x = Prospect::create($_POST, $errors);
		if ($x)
		{
			$this->setData('is_form_valid', true);
			$this->setData('lien_reservation', 'jeunes/offre.html');
			$this->setTrackPage('confirmation', true);
		}
		else if (count($errors))
			$this->_addMessage($errors);
	}
}
?>
