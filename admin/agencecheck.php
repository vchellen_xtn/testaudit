<?
require_once "secure.php";
require_once "lib.php";
require_once "../lib/lib.tcp.php";

set_time_limit(0);
define("TOLERANCE",0.002);

die("� reprendre avec la novuelle API");

$sql = "SELECT id, adresse1, adresse2, cp, ville, latitude, longitude FROM agence_pdv WHERE zone='fr' AND (longitude+latitude is not null) and statut!=0";
$rs = sqlexec($sql);
?>
<html>
<head>
	<title>Mise � jour des coordonn�es des agences</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
</head>
<body>
<table border="1" cellpadding="2" cellspacing="0">
<caption>Mise � jour des coordonn�es</caption>
<tr><th>Nb</th><th>agence</th><th>adresse</th><th>longitude</th><th>latitude</th></tr>
<?
$count = 0;
while ($row = mysql_fetch_assoc($rs)) {
	$a = array();
	// on enl�ve le CEDEX s'il existe
	$ville = preg_replace('/\scedex\s*$/i','', $row['ville']);
	// l'adrese de base
	$a[] = $row['adresse1'].' '.$row['adresse2'].' '.$row['cp'].' '.$ville.' France';
	// on peut essayer d'autres adresses
	if ($row['adresse2']) {
		$a[] = $row['adresse1'].' '.$row['cp'].' '.$ville.' France';
		$a[] = $row['adresse2'].' '.$row['cp'].' '.$ville.' France';
	}
	// ... sans le code postal
	$a[] = $row['adresse1'].' '.$row['adresse2'].' '.$ville.' France';
	if ($row['adresse2']) {
		$a[] = $row['adresse1'].' '.$ville.' France';
		$a[] = $row['adresse2'].' '.$ville.' France';
	}
	// puis on essaye ces diff�rentes adresses
	$lat = $lng = null;
	foreach ($a as $adresse) {
		if (GMapConvert($adresse, $lat, $lng)) {
			break;
		}
	}
	if (!$lat || !$lng) continue;
	if (abs(rad2deg($row['latitude'])-$lat) <= TOLERANCE && abs(rad2deg($row['longitude'])-$lng) <= TOLERANCE)
		continue;
	echo '<tr><td>'.++$count.'</td><td><a href="main.php?rows=460&url='.urlencode('agence.php?id='.$row['id']).'">'.$row['id'].'</a></td>';
	echo '<td>'.$adresse.'</td><td>'.$lng.' / '.rad2deg($row['longitude']).'</td><td>'.$lat.' / '.rad2deg($row['latitude']).'</td>';
	echo "</tr>\n";
	$row['latitude'] = deg2rad($lat);
	$row['longitude'] = deg2rad($lng);
	save_record('agence_pdv', $row);
}
?>
</table>
</body>
</html>