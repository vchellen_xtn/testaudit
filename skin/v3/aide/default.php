		<style type="text/css">
			.content_bottom{width:964px; background:#FFF; margin:0 auto; padding:0px; font-family:'Raleway', Arial; font-size:14px;}
		</style>
		<div class="bg-new">
			<div class="header">
				<div class="univers_titre"><?=$this->getUniversTitre();?></div>
			</div>
			<div class="content">
				<div id="col" class="box-sizing">
				<?
					// répertoire spécifique
					$d = dirname(__file__).'/';
					if (is_file($f = $d.'/menu.php'))
						include ($f);
					$this->showBannieres('left');
				?>
				</div>
				
				<div id="page">
				<?
					include($this->getPagePath());
				?>
				</div>
				
				<div class="clear"></div>
			</div>
		</div>