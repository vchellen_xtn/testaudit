<?php
class PromotionCampagne_Collection extends Ada_Collection
{
	static public function factory($serviceClientOnly = false, $dateUntil = null)
	{
		$x = new PromotionCampagne_Collection();
		// pr�parer le SQL
		$sql = "select * from promotion_campagne where 1";
		if ($serviceClientOnly)
			$sql.=" and serviceclient=1"; 
		if ($dateUntil)
			$sql.=" and '$dateUntil' <= ifnull(fin,'$dateUntil')";
		if ($serviceClientOnly)
			$sql.=" order by slogan, nom";
		else
			$sql.=" order by nom";
		return $x->loadFromSQL($sql);
	}
}
?>
