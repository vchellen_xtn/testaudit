<?php
class AgenceConfig_Collection extends Ada_Collection {
	
	/** @var array codes soci�t�s pr�sents */
	protected $_societes = array();

	/**
	* Cr�e la liste des configurations pour un utilisateur
	* 
	* @param AccesAgence $resaUser
	* @return AgenceConfig_Collection
	*/
	static public function factory(AccesAgence $resaUser) {
		
		// liste des agences
		$agences = array_merge
		(
			array('%'), // le param�trage par d�faut
			$resaUser->getAgences()->getIds(),	// les agences autoris�es
			array_keys($resaUser->getSocietes()) // les codes soci�t�s autoris�es
		);
		
		// pr�parer le SQL
		$sql = "select c.* \n";
		$sql.=" from agence_config c\n";
		$sql.=" where c.agence IN ('".implode("','", $agences)."')\n"; // pour avoir la configuration par d�faut
		
		// pr�parer le SQL
		$x = new AgenceConfig_Collection();
		$x->loadFromSQL($sql);
		return $x;
	}
}
?>
