<?php


class Transaction_UnEuro extends Transaction
{
	/**
	* On appelle le parent si les arguments sont fournies
	* Sinon on regarde dans le $_GET pour valider UnEuro sans enregistrer
	* Cela permet de simuler la r�servation
	* 
	* @param Reservation $reservation
	* @param array $args
	*/
	public function doPayment(Reservation $reservation, $args)
	{
		if (count($args))
			return parent::doPayment($reservation, $args);
		// on v�rifie les informationss sp�cifiques � Paybox et 1EURO
		if (!Paybox::checkSignature())
		{
			$this->_addMessage("La signature est incorrecte !");
			return false;
		}
		$a = $_GET;
		if ($a['MONTANT'] != (100 * $reservation['total']))
		{
			$this->_addMessage("Le montant est incorrect");
			return false;
		}
		// v�rifier le num�ro d'autorisation en production
		if (SITE_MODE=='PROD' && preg_match('/^X+$/', $a['AUTORISATION']))
		{
			$this->_addMessage("Le num�ro d'autorisation n'est pas correct");
			return false;
		}
		// le CODEREPONSE doit �tre 00000 et l'autorisation doit �tre pr�sente
		if ($a['CODEREPONSE']!='00000')
		{
			$this->_addMessage("Erreur lors du paiement : ".Paybox::getMessage($a['CODEREPONSE'],'UN'));
			return false;
		}
		if (!$a['AUTORISATION'])
		{
			$this->_addMessage("Le num�ro d'autorisation n'est pas fournie");
			return false;
		}
		// on simule le "paiement" en modifiant le statut
		// mais SANS L'ENREGISTRER
		// l'appel d�initif se fait par /paybox_confirm.php
		$reservation['statut'] = 'P';
	}
	
	protected function _doPayment(Reservation $reservation, $a)
	{
		// on enregistrele retour
		$reservation->setData('numerotrans', $a['NUMTRANS'])
					->setData('numappel', $a['NUMAPPEL'])
					->setData('code_reponse', $a['CODEREPONSE'])
					->setData('autorisation', $a['AUTORISATION'])
					->save('numerotrans,numappel,code_reponse,autorisation');
		// on enregistre la transaction
		Paybox::logger($reservation, $this->_mode, $a);
		// on renvoie un message d'erreur si besoin
		if ($a['CODEREPONSE'] != '00000')
		{
			$this->_addMessage(Paybox::getMessage($a['CODEREPONSE'],'UN').'<br/>'.$a['COMMENTAIRE']);
			return false;
		}
		return true;
	}
	/**
	* Annule une r�servation
	* 
	* @param Reservation $reservation
	* @param double $montant
	*/
	protected function _doRefund(Reservation $reservation, $montant)
	{
		// on ne peut pas annuler avec UN EURO
		return false;
	}
	protected function _getModalitesRemboursementTotal()
	{
		return "Le remboursement n'est pas possible par UN EURO!";
	}
	protected function _getModalitesRemboursementPartiel($rembourse)
	{
		return "Le remboursement n'est pas possible par UN EURO!";;
	}
}
?>
