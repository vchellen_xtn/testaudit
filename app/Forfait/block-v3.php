<?
	/** @var Forfait */ $forfait = $this;
	$categorie = $forfait->getCategorie();
?>

<div class="fam_cat">
	<h2><?=$categorie['nom']?></h2>
	<p id="forfait_categorie_commentaire">
		<?=$categorie['commentaire']?>
	</p>
	<p id="forfait_categorie_description">
		<?=$categorie['description']?>
	</p>
</div>
<div id="forfait_categorie_specs">
	<ul class="options">
		<?=$categorie->getSpecifications('li');?>
	</ul>
	<div class="clear"></div>
</div>
<? if ($categorie['izmocar'] && strtolower($categorie['type'])=='vp'): ?>
	<a href="#" onclick="return appADA.show360(0<?='+'.(int)$categorie['id'];?>, 'reservation_resultat', '<?=strtolower($categorie['type']);?>', '<?=addslashes($categorie['mnem']);?>');">
		<img src="../<?=$categorie->getImg('big', 'categories3');?>" id="forfait_categorie_img" class="bg-360" alt="" />
		<span id="forfait_categorie_vp_txt">Cliquez sur la voiture<br />pour la d�couvrir sous<br />tous les angles</span>
	</a>
<? elseif ($categorie['izmocar'] && strtolower($categorie['type'])=='vu'): ?>
	<a href="#" onclick="var img = document.getElementById('forfait_categorie_img'); if (!img) {return false;} if (img.src.match(/-big\.png$/)) { img.src = img.src.replace('-big.png', '-big-rear.png'); $('#forfait_categorie_txt').html('Voir l\'ext�rieur'); } else { img.src = img.src.replace('-big-rear.png', '-big.png'); $('#forfait_categorie_txt').html('Voir l\'int�rieur'); } return false;">
		<img src="../<?=$categorie->getImg('big', 'categories3');?>" id="forfait_categorie_img" alt=""/>
		<span id="forfait_categorie_txt">Voir l'int�rieur</span>
	</a>
<? else : ?>
	<img src="../<?=$categorie->getImg('big', 'categories3');?>" id="forfait_categorie_img" alt="" />
<? endif; ?>

<div class="promo_prix">
	<? if ($forfait->isLocapass()) : ?>
		<img src="../css/img/locapass.png" alt="Locapass" />
	<? else : ?>
	<div class="prix_total">
		<span id="forfait_t_prix" class="digit-<?=strlen($forfait->getPrix());?>"><?=$forfait->getPrix();?>�</span>
		<div class="right">
			<? if ($forfait->hasPromo()) : ?>
			<p id="forfait_prix">au lieu de <span><?=$forfait->getPrixBarre()?>�</span></p>
			<? endif; ?>
			<a href="#" onclick="if (appADA && appADA.showPopIn) { appADA.showPopIn('forfait_details', {'agence': '<?=$forfait->getAgence()->getId()?>', 'categorie': '<?=$categorie['id']?>', 'km_sup': '<?=$forfait->getPrixKmSupp()?>'})};  return false;">D�tails et devis</a>
		</div>
	</div>
	<? endif; ?>
	<? if ($surcharge = $forfait->getSurcharge()) : ?>
	<div class="forfait_surcharge">
		<span class="forfait_surcharge_label"><?=$forfait->getAgence()->getSurchargeLabel()?></span>
		<span class="forfait_surcharge_prix"><?=$surcharge;?> &euro;</span>
	</div>
	<? endif; ?>
	<? if ($forfait->hasNego()) : ?>
	<div class="forfait_nego">
		<a id="btn_negociation" href="#" onclick="if (appADA && appADA.showPopIn) { appADA.showPopIn('forfait_negociation', {'forfait': '<?=$forfait->doSerialize();?>'}, {dialogClass: 'popin popin-negociation', position: {my: 'right bottom', at:'left center', of: '#btn_negociation' }}); }  return false;">N�gociez votre prix ici !</a>
	</div>
	<? endif; ?>
</div>



