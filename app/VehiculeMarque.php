<?php
class VehiculeMarque extends Ada_Object
{
	/**
	* Cr�e un v�hicule marque
	* 
	* @param string $id
	* @return VehiculeMarque
	*/
	static public function factory($id)
	{
		$x = new VehiculeMarque();
		if (preg_match('/^[a-z\-]+$/i', $id)) {
			$x->load($id);
		}
		if ($x->isEmpty()) return null;
		return $x;
	}
	public function __toString() {
		return $this->_data['nom'];
	}
}
?>
