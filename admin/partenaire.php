<?
	require_once("secure.php"); 
	require_once("lib.php");

	// identifiant
	$id = getarg("id");
	
	// suppression
	if ($id && getarg("delete")) 
	{
		$sql = "DELETE FROM partenaire WHERE id = '".$id."'";
		getsql($sql);
		unset($id);
	}
	
	// modification
	if (getarg("update") || getarg("create"))
	{
		save_record("partenaire", $_POST);
		if(!$id)
		{
			$id = mysql_insert_id();
			if ($_POST['code'])
			{
				$pc = PartenaireCoupon::factory($_POST['code'], true);
				if ($pc && !$pc->isEmpty())
					$msg = "Le code de r�duction {$pc['code']} est d�j� utilis� (#{$pc['id']}) par le partenaire {$pc['partenaire']}.";
				else
				{
					$pc = array('id'=>'','partenaire'=>$id, 'nom'=>$_POST['nom']);
					foreach(array('code','depuis','echeance','classe','locapass') as $k)
						$pc[$k] = $_POST[$k];
					save_record('partenaire_coupon', $pc);
				}
			}
		}
	}

	if ($id)
	{
		$rs = sqlexec("SELECT * FROM partenaire WHERE id = '".$id."'");
		if (!$rs || !($row = mysql_fetch_assoc($rs)))
			die("Aucun enregistrement correspondant.");
		// cr�er les objests
		$partenaire = new Partenaire($row);
		$coupons = PartenaireCoupon_Collection::factory($partenaire);
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script type="text/javascript" src="../scripts/script.js"></script>
	<script type="text/javascript" src="admin.js"></script>
	<script type="text/javascript">
	// <![CDATA[
	<? if ($msg) echo 'alert("'.$msg.'");'; ?>
	// doDelete
	function doDelete(frm)
	{
		return (confirm("Voulez-vous r�ellement supprimer cet �l�ment ?\nCette op�ration est irr�versible."));
	}

	// doSubmit
	function doSubmit(frm)
	{
		var err = "";
		if(!validateSelect(frm.origine))
			err += "Veuillez pr�ciser l'origine\n";
		if(!frm.nom.value.length)
			err += "Veuillez indiquer le nom\n";
		if(!validateSelect(frm.actif))
			err += "Veuillez indiquer le statut\n";
		if (!frm.annee.value.match(/^((18|19|20)\d{2})?$/))
			err+="Veuillez indiquer une ann�e de cr�ation valide\n"
		if (frm.email.value.length && !validateEmail(frm.email))
			err += "Veuilez indiquer un e-mail valide\n"
		// v�rification du code de r�duction
		if(frm.code && frm.code.value.length)
		{
			if (frm.depuis.value.length && !validateDate(frm.depuis))
				err += "Veuillez rentrer une date de d�but valide\n";
			if (frm.echeance.value.length && !validateDate(frm.echeance))
				err += "Veuillez rentrer une date d'�ch�ance valide\n";
		}
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		return true;
	}
	
	// ]]>
	</script>
</head>
<body>
<form action="" method="post" onsubmit="return doSubmit(this);" enctype="multipart/form-data">
<table class="framed" border="0" cellspacing="0" cellpadding="4" style="float: left;">
<tr>
	<th class="bg_titre">
		<img src="img/puce.gif" align="absmiddle" hspace="1"> Partenaire
	</th>
</tr>

<tr>
	<td>
	<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="15%">
		<col width="32%">
		<col width="15%">
		<col width="33%">
		<tr>
			<td><label for="id">Id</label></td>
			<td><? echo $id.'<input type="hidden" id="id" name="id" value="'.$id.'" maxlength="6">'; ?></td>
			<td><label for="origine">Origine</label></td>
			<td><? selectarray('origine', Partenaire::$ORIGINES, $row['origine'], '--'); ?></td>
		</tr>
		<tr>
			<td><label for="nom">Nom</label></td>
			<td><input type="text" id="nom" name="nom" value="<?=$row["nom"]?>" maxlength="64"></td>
			<td><label for="actif">Actif</label></td>
			<td><? selectarray("actif", array('1'=>'oui','0'=>'non'), $row['actif']); ?></td>
		</tr>
		<tr>
			<td><label for="siret">Siret</label></td>
			<td><input type="text" id="siret" name="siret" value="<?=$row["siret"]?>" size="14" maxlength="14" /></td>
			<td><label for="naf">NAF</label></td>
			<td><input type="text" id="naf" name="naf" value="<?=$row["naf"]?>" maxlength="5"></td>
		</tr>
		<tr>
			<td><label for="gerant">G�rant</label></td>
			<td colspan="3"><input type="text" id="gerant" name="gerant" value="<?=$row["gerant"]?>" maxlength="96"></td>
		</tr>
		<tr>
			<td><label for="annee">Ann�e cr�ation</label></td>
			<td><input type="text" id="annee" name="annee" value="<?=$row["annee"]?>" maxlength="4"></td>
			<td><label for="salaries">Salari�s</label></td>
			<td><? selectarray('salaries', Partenaire::$SALARIES, $row['salaries']);?></td>
		</tr>
		<tr>
			<td><label for="interlocuteur">Interlocuteur</label></td>
			<td><input type="text" id="interlocuteur" name="interlocuteur" value="<?=$row["interlocuteur"]?>" maxlength="96"></td>
			<td><label for="email">Email</label></td>
			<td><input type="text" id="email" name="email" value="<?=$row["email"]?>" maxlength="128"></td>
		</tr>
		<tr>
			<td><label for="adresse1">Adresse 1</label></td>
			<td><input type="text" id="adresse1" name="adresse1" value="<?=$row["adresse1"]?>" maxlength="32"></td>
			<td><label for="adresse2">Adresse 2</label></td>
			<td><input type="text" id="adresse2" name="adresse2" value="<?=$row["adresse2"]?>" maxlength="32"></td>
		</tr>
		<tr>
			<td><label for="cp">CP</label></td>
			<td><input type="text" id="cp" name="cp" value="<?=$row["cp"]?>" maxlength="5"></td>
			<td><label for="ville">Ville</label></td>
			<td><input type="text" id="ville" name="ville" value="<?=$row["ville"]?>" maxlength="64"></td>
		</tr>
		<tr>
			<td><label for="pays">Pays</label></td>
			<td><? selectsql("Pays", "SELECT id, nom FROM pays", ($row['pays']?$row['pays']:'fr')) ?></td>
			<td colspan="2"></td>
		</tr>
		<tr>
			<td><label for="tel">Tel</label></td>
			<td><input type="text" id="tel" name="tel" value="<?=$row["tel"]?>" maxlength="16"></td>
			<td><label for="fax">Fax</label></td>
			<td><input type="text" id="fax" name="fax" value="<?=$row["fax"]?>" maxlength="16"></td>
		</tr>
		<? if (!$id) : ?>
		<tr><th colspan="4">Code de r�duction</th></tr>
		<tr>
			<td><label for="code">Code</label></td>
			<td><input type="text" name="code" maxlength="32"/></td>
			<td colspan="2"></td>
		</tr>
		<tr>
			<td><label for="depuis">Depuis</label></td>
			<td><input type="text" id="depuis" name="depuis" maxlength="10"/></td>
			<td><label for="echeance">Ech�ance</label></td>
			<td><input type="text" id="echeance" name="echeance" maxlength="10"/></td>
		</tr>
		<tr>
			<td><label for="classe">Promotions</label></td>
			<td><? selectsql('classe', 'SELECT id, nom FROM partenaire_classe ORDER BY nom', $row['classe'], '--');?></td>
			<td><label for="locapass">Locapss</label></td>
			<td><input type="text" id="locapass" name="locapass" value="<?=$row['locapass']?>" maxlength="6"></td>
		</tr>
		<? endif; // cr�ation ?>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%">
		<tr>
			<td align="left">
				<input type="button" onclick="go_frame(parent.list, 'lists/partenaire_conducteur.php?partenaire=<?=$id?>')" value="conducteurs"/>
			</td>
			<td align="right" valign="top">
				<input type="submit" name="<?=$id ? "update" : "create"?>" value="enregistrer">
				<? if ($id && !$coupons->count()) : ?>
				&nbsp;
				<input type="submit" onclick="return doDelete(this.form);" name="delete" value="supprimer">
				<? endif; ?>
			</td>
		</tr>
		</table>
	</td>
</tr>
<? if ($id) : ?>
<tr>
	<td>
	<table width="100%">
	<tr>
		<th colspan="6">Codes r�ductions</th>
	</tr>
	<tr>
		<th>Code</th>
		<th>Depuis</th>
		<th>Echeance</th>
		<th>Actif</th>
		<th>Locapass</th>
		<th>Promotions</th>
		<th>Stats</th>
	</tr>
	<? foreach($coupons as $coupon) : ?>
	<tr>
		<td><a href="partenaire_coupon.php?id=<?=$coupon['id']?>"><?=$coupon['code']?></a></td>
		<td><?=euro_iso($coupon['depuis'])?></td>
		<td><?=euro_iso($coupon['echeance'])?></td>
		<td><?=$coupon['actif'] ? 'oui' : 'non' ?></td>
		<td><?=$coupon['locapass']?></td>
		<td><?=$coupon['classe_nom']?></td>
		<td><a href="stats_partenaire.php?origine=<?=$partenaire['origine']?>&coupon=<?=$coupon['id']?>">Stats</a></td>
	</tr>
	<? endforeach; ?>
	<tr>
		<td colspan="6"><input type="button" value="nouveau code de r�duction" onclick="window.location.href='partenaire_coupon.php?partenaire=<?=$partenaire['id']?>'"/></td>
	</tr>
	</table>
	</td>
</tr>
<? endif; ?>
</table>
<?
if ($row)
{
?>
<table class="framed" border="0" cellspacing="0" cellpadding="4" style="float:left; margin-left:5px; width:300px;">
	<tr><th class="bg_titre"><img src="img/puce.gif" align="absmiddle" hspace="1">Clients associ�s</th></tr>
	<tr>
		<td>
		<?
			$clients = Client_Collection::factory($partenaire);
			if ($clients->isEmpty())
				echo 'aucun client associ�e';
			else
			{
				foreach($clients as $client)
					echo '<a href="client.php?id='.$client['id'].'">'.$client['prenom'].' '.$client['nom'].' - '.$client['email'].'</a><br/>';
			}
		?>
		</td>
	</tr>
</table>
<?
}
?>
</form>
</body>
</html>
