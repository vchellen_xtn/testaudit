<?
	if (!$_GET['option']) die("Vous devez pr�ciser une option !");
	session_start();
	include('../../constant.php');

	$title 	 = "Agences Option";
	$url 	 = "../agence_option.php";
	$key	 = "a.id";
	$order	 = "";

	$option = '';
	if (isset($_GET['option']) && preg_match('/^[a-z0-9]{4}$/i', $_GET['option'])) {
		$option = $_GET['option'];
	}
	
	$sql = "select a.id agence, a.code_societe as code, a.reseau, a.nom";
	$sql.= " from agence_option ao join agence a on ao.agence=a.id";
	$sql.= sprintf(" where a.zone='%s' and ao.`option`='%s'", $_SESSION["ADA001_ADMIN_ZONE"], $option);
	$sql.= sprintf(" and (a.statut & %ld = %ld)", Agence::STATUT_VISIBLE, Agence::STATUT_VISIBLE);

	$out_url = "list.php?sql=" . urlencode($sql);
	$out_url.= "&title=" . urlencode($title);
	$out_url.= "&url=" . urlencode($url);
	$out_url.= "&color=4587ba";
	$out_url.= "&key=".$key."&order=".$order."&nb_rows=30";
	header("location: ".$out_url);
?>
