<?php


class Transaction_Paybox extends Transaction
{
	protected $_payboxCollected = true;
	
	/**
	* Utiilie-t-on ou pas 3DSecure
	* Dans le cas o� l'on revient de 3DSecure $bin est vide
	* 
	* @param Reservation $reservation
	* @param PayboxBin $bin
	* @return bool
	*/
	protected function _use3DSecure(Reservation $reservation, PayboxBin $bin = null, $abonnement = false)
	{
		// faut-il utiliser 3D Secure ?
		if (USE_3DS)
		{
			// pour les abonnements on force 3D Secure
			if ($abonnement)
				return true;
			// pour les cartes non issues de FRANCE
			if ($bin['PAYS']!='FRA')
				return true;
			// et pour les montants sup�rieurs � un seill d�fini
			return ($reservation['total'] >= USE_3DS);
		}
		return false;
	}
	
	/**
	* Obtenir l'identification 3D Secure
	* 
	* @param Reservation $reservation
	* @param string $numcarte
	* @param string $valid
	* @param string $cvv
	* @return PayboxSecure
	*/
	protected function _get3DSecure(Reservation $reservation, $numcarte, $valid, $cvv, $libelle_1clic=null)
	{
		// tout d'abord avons nous un identifiant 3DSecure pour cette r�servation
		$secures = PayboxSecure_Collection::factory($reservation);
		$secure = $secures->findOK();
		// 1. pas d'identification 3D Secure et nous avons un num�ro de carte : nous demandons l'identification 3D Secure
		if (($secures->isEmpty() || !$secure) && $numcarte)
		{
			// remettre le statut � la validation
			$reservation->setData('statut','V')->save('statut');
			// et renvoyer vers l'authentification 3DSecure
			die(Paybox::get3DSecureForm($reservation, $numcarte, $valid, $cvv, $libelle_1clic));
		}
		// 2. pas d'autentification 3D Secure
		if (!$secure)
			$this->_addMessage("L'authentification 3D Secure a �chou�e.");
		else if ($numcarte && $secure['CCNumber']!=$numcarte)
		{
			$this->_addMessage("L'authentification 3D Secure a �t� faite pour une autre carte");
			$secure = null;
		}
		return $secure;
	}
	
	/**
	* Effectuer le paiement d'une r�servation
	* 
	* @param Reservation $reservation
	* @param array $args
	*/
	protected function _doPayment(Reservation $reservation, $args)
	{
		// on valide les arguments re�us dans $args si pr�sent
		// NB : ils sont absents en cas de retour de 3D Secure
		$rules = array (
			'an'		=> '/^\d{4}$/',
			'mois'		=> '/^\d{1,2}$/',
			'valid' 	=> '/^\d{4}$/',
			'numcarte'	=> '/^\d{15,16}$/',
			'cvv'		=> '/^\d{3}$/',
			'chk_1clic'	=> '/^\d$/',
			'abonnement'	=> '/^\d+$/',
		);
		foreach($rules as $k => $regexp)
		{
			if (!$args[$k]) continue;
			$args[$k] = preg_replace('/[^\d]/', '', $args[$k]);
			if (!preg_match($regexp, $args[$k]))
			{
				$this->_addMessage("Le paiement ne peut pas �tre effectu� : un arguement n'est pas correct ($k)");
				return false;
			}
		}
		// date de validit�
		$args['valid'] = sprintf('%04s', ($args['mois']) * 100 + $args['an'] % 100);
		
		// si un abonnement a �t� pass� on le r�cup�re
		if ($args['abonnement'])
		{
			$abonnement = PayboxAbonne::factory($reservation->getClient(), $args['abonnement']);
			if (!$abonnement)
			{
				$this->_addMessage("Le paiement en 1 clic ne peut pas �tre effectu� : les informations n'ont pas �t� trouv�es !");
				return false;
			}
			if (!$abonnement->isValid())
			{
				$this->_addMessage("Le paiement en 1 clic ne peut pas �tre effectu� : la date de validation est expir�e !");
				return false;
			}
			// on renseigne les informations en provenance de l'abonnement
			$args['cvv'] = $abonnement['CVV'];
			$args['valid'] = $abonnement['DATEVAL'];
			$args['numcarte'] = $abonnement['PORTEUR'];
			$args['abonnement'] = $abonnement['REFABONNE'];
		}
		else
		{
			// obtenir le TYPECARTE et PAYS pour ce num�ro de carte
			// lorsqu'on revient de 3DSecure on n'a pas ces informations qui serontobtenues par $this->_get3DSecure
			$bin = PayboxBin::factory($reservation, $args['numcarte'], $args['valid'], $args['cvv']);
			
			// 3D-Secure : si les conditions sont remplies on doit obtenir l'autorisation 3D Secure
			if ($bin->isEmpty() || $this->_use3DSecure($reservation, $bin, ($args['chk_1clic'] && $args['libelle'])))
			{
				$secure = $this->_get3DSecure($reservation, $args['numcarte'], $args['valid'], $args['cvv'], $args['libelle']);
				if (!$secure) return false;
				// il faut initialiser les diff�rents arguemnts
				$args['valid'] = $secure['CCExpDate'];
				$args['numcarte'] = $secure['CCNumber'];
				$args['cvv'] = $secure['CVVCode'];
				$args['ID3D'] = $secure['ID3D'];
				if ($secure['libelle_1clic'])
				{
					$args['libelle'] = $secure['libelle_1clic'];
					$args['chk_1clic'] = 1;
				}
			}
		
			// on re-v�rifie que les param�tres sont pr�ts pour aller plus loins
			unset($rules['an'],$rules['mois'], $rules['chk_1clic'], $rules['abonnement']);
			foreach ($rules as $k => $regexp)
			{
				if (!$args[$k])
				{
					$this->_addMessage("Le paiement ne peut pas �tre effectu� : un arguement est manquant ($k)");
					return false;
				}
			}
		}

		// pr�parer le type et le montant pour l'appel � PAYBOX...
		$type = Paybox::TYPE_PAIEMENT;
		$montant = $reservation['total'];
		// ... avec les codes grillables la r�servation peut �tre � 0�... on demande quand m�me l'autorisation pour 1� ...
		if ($montant <= 0)
		{
			$type = Paybox::TYPE_AUTORISATION;
			$montant = 1;
		}
		// ... en cas d'abonnement, il faut utilier les types paybox::TYPE_ABT_AUTORISATION Paybox::TYPE_ABT_PAIEMENT (+50)...
		if ($args['abonnement'])
			$type += 50;
		// ... appeler Paybox
		$a = Paybox::paiement($reservation->getResID(), $montant, $args['numcarte'], $args['valid'], $args['cvv'], $type, $args['ID3D'], $args['abonnement']);

		// on enregistrele retour
		$reservation->setData('numerotrans', $a['NUMTRANS'])
					->setData('numappel', $a['NUMAPPEL'])
					->setData('code_reponse', $a['CODEREPONSE'])
					->setData('autorisation', $a['AUTORISATION'])
					->setData('valid', $args['valid'])
					->save('numerotrans,numappel,code_reponse,autorisation,valid');
		// on enregistre le retour
		Paybox::logger($reservation, $this->_mode, $a);
		// on renvoie un message d'erreur si besoin
		if ($a['CODEREPONSE'] != '00000')
		{
			// obtenir un message d'explications
			$msg = Paybox::getMessage($a['CODEREPONSE']);
			$this->_addMessage($msg ? $msg : $a['COMMENTAIRE']);
			return false;
		}
		
		// on cache les num�ros de carte bancaire dans paybox_secure
		sqlexec("UPDATE paybox_secure SET CCNumber = RPAD(LEFT(CCNumber,6),LENGTH(CCNumber),'X') WHERE reservation='".$reservation->getId()."' OR creation < '".date('Y-m-d H:i:s', strtotime('-10 minutes'))."'");

		// cr�er l'abonnement si c'est demand�
		if ($args['chk_1clic'] && $args['libelle'] && !$args['abonnement'])
		{
			if ($abonnement = PayboxAbonne::create($reservation, $args['libelle'], $args['numcarte'], $args['valid'], $args['cvv']))
				$this->_addMessage("Votre carte a �t� enregistr� avec succ�s pour un prochain paiement en 1 clic.");
			else
				$this->_addMessage("Une erreur s'est produite lors de l'enregistrement de votre carte pour le paiement en 1 clic.");
		}
		
		// on conserve le num�ro de carte puor le passer � UNIPRO
		// mais on ne l'enregistre pas dans la base
		if ($abonnement)
			$numero_carte = str_replace('X','-', $abonnement['numcarte']);
		else
		{
			$numero_carte = preg_replace('/[^\d]/', '', $args['numcarte']);
			$numero_carte = preg_replace('/\d/', '-', $numero_carte, strlen($numero_carte)-4);
		}
		$reservation->setData('numero_carte', $numero_carte);
		return true;
	}
	/**
	* Annule une r�servation
	* 
	* @param Reservation $reservation
	* @param double $montant
	*/
	protected function _doRefund(Reservation $reservation, $montant)
	{
		// s'il n'y a rien � rembourser on arr�te tout de suite
		if (!$montant) return true;
		// on tente l'annulation si le montant � rembourser est �gale au total de la r�servation
		if ($montant == $reservation->getTotal())
		{
			$a = Paybox::annulation($reservation->getResID(), $montant, $reservation['numappel'], $reservation['numerotrans']);
			// on enregistre le retour
			if ($a)
				Paybox::logger($reservation, $this->_mode, $a);
			if ($a && $a['CODEREPONSE'] == '00000')
			{
				$this->_payboxCollected = false;
				return true;
			}
		}
		// si le montant a �t� imput� des frais de dossier
		// ou si le code retour indique un �chec car une t�l� collecte a eu lieu il faut rembourser...
		if (!$a || $a['CODEREPONSE'] == '00015')
		{
			$a = Paybox::remboursement($reservation->getResID(), $montant, $reservation['numappel'], $reservation['numerotrans']);
			// on enregistre le retour
			if ($a)
				Paybox::logger($reservation, $this->_mode, $a);
		}
		// on note s'il y a une erreur
		if ($a['CODEREPONSE'] != '00000')
		{
			$this->_addMessage("Une erreur s'est produite : ".Paybox::getMessage($a['CODEREPONSE']).'<br>'.$a['COMMENTAIRE']);
			return false;
		}
		return true;
	}
	protected function _getModalitesRemboursementTotal()
	{
		// message diff�rent s'il y a eu t�l�-collecte ou non
		if ($this->_payboxCollected)
			return "Votre compte bancaire sera re-cr�dit� du montant de votre r�servation.";
		else
			return "Votre compte bancaire ne sera pas d�bit� du montant de cette r�servation.";
	}
	protected function _getModalitesRemboursementPartiel($rembourse)
	{
		if ($rembourse > 0) {
			return "Votre compte bancaire va donc �tre re-cr�dit� d'un montant de ".$rembourse."� TTC selon les d�lais de traitement de votre banque. Ce d�lai n'exc�dera pas 72h.";
		}
	}
}
?>
