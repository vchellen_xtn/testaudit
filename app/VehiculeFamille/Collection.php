<?php
class VehiculeFamille_Collection extends Ada_Collection
{
	/**
	* Renvoie une liste de famille de v�hicule
	* 
	* @param string $zone
	* @param string $type
	* @return VehiculeFamille_Collection
	*/
	static public function factory($zone, $type)
	{
		$x = new VehiculeFamille_Collection();
		$sql = "select c.id, c.mnem, c.famille, f.defaut, f.nom, coalesce(f.description, c.commentaire) description";
		$sql.=" from vehicule_famille f";
		$sql.=" join categorie c on c.id=f.categorie_defaut";
		$sql.=" where f.type='".addslashes($type)."'";
		if ($zone != 'fr')
			$sql.=" and f.id in (SELECT DISTINCT c.famille FROM agence a JOIN agence_dispo d on (d.agence=a.id and d.type='$type') JOIN categorie c on c.id=d.categorie WHERE a.zone='$zone')";
		$sql.=" order by f.position";
		$x->loadFromSQL($sql);
		return $x;
	}
	
	public function checkCategorie($catID)
	{
		if ($catID && !isset($this->_items[$catID]))
			$catID = getsql('select categorie_defaut from vehicule_famille f join categorie c on c.famille=f.id where c.id='. (int)$catID);
		return $catID;
	}
	
	/**
	* Renvoie l'ID de la cat�gorie par d�faut
	* 
	*/
	public function getDefaultID()
	{
		foreach($this->_items as $x)
		{
			if ($x['defaut'])
				return $x->getId();
		}
	}
	
}
?>
