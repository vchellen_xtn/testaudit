<?php
define("IMAGE_FLIP_HORIZONTAL",	1);
define("IMAGE_FLIP_VERTICAL",	2);
define("IMAGE_FLIP_BOTH",		3);

/**
 * A Wrapper for GD library in PHP. GD must be installed in your system for this to work.
 * Example: $img = new Image('wheel.png');
 * 			$img->flip(1)->resize(120, 0)->save('wheel.jpg');
 */
class Image {
	private $file_name;
	private $info;
	private $width;
	private $height;
	private $image;
	private $org_image;

	/**
	 * Constructor -
	 * Arguments : Image Filepath
	 */
	function Image($image_file) {
		if(!function_exists('imagecreatefrompng')) return; //GD not available
		if(!file_exists($image_file) or !is_readable($image_file)) return;

		$this->file_name = $image_file;
		$img = getimagesize($image_file);

		//Create the image depending on what kind of file it is.
		switch($img['mime']) {
			case 'image/png' : $image = imagecreatefrompng($image_file); break;
			case 'image/jpeg': $image = imagecreatefromjpeg($image_file); break;
			case 'image/gif' :
				$old_id = imagecreatefromgif($image_file);
				$image  = imagecreatetruecolor($img[0],$img[1]);
				imagecopy($image,$old_id,0,0,0,0,$img[0],$img[1]);
				break;
			default: break;
		}
		$this->info		= $img;
		$this->org_image = $image;
		$this->_setImage($image);
	}

	protected function _setImage($image)
	{
		if ($this->image && $this->image != $this->org_image && $this->image != $image)
			imagedestroy($this->image);
		$this->image  = $image;
		$this->width  = $this->info[0] = imagesx($image);
		$this->height = $this->info[1] = imagesy($image);
		return $this;
	}


	/**
	 * Rotates the image to any direction using the given angle.
	 * Arguments: $angle - The rotation angle, in degrees.
	 * Example: $img = new Image("file.png"); $img->rotate(180); $img->show(); // Turn the image upside down.
	 */
	function rotate($angle, $background=0) {
		if(!$this->image) return false;
		if(!$angle) return $this;

		$this->_setImage(imagerotate($this->image, $angle, $background));
		return $this;
	}

	/**
	 * Mirrors the given image in the desired way.
	 * Arguments : $type - Direction of mirroring. This can be 1(Horizondal Flip), 2(Vertical Flip) or 3(Both Horizondal and Vertical Flip)
	 * Example: $img = new Image("file.png"); $img->flip(2); $img->show();
	 */
	function flip($type) {
		if(!$this->image) return false;
		if(!$type) return false;

		$imgdest= imagecreatetruecolor($this->width, $this->height);
		$imgsrc	= $this->image;
		$height	= $this->height;
		$width	= $this->width;

		switch( $type ) {
			//Mirroring direction
			case IMAGE_FLIP_HORIZONTAL:
			case 'h':
				for( $x=0 ; $x<$width ; $x++ )
					imagecopy($imgdest, $imgsrc, $width-$x-1, 0, $x, 0, 1, $height);
				break;

			case IMAGE_FLIP_VERTICAL:
			case 'v':
				for( $y=0 ; $y<$height ; $y++ )
					imagecopy($imgdest, $imgsrc, 0, $height-$y-1, 0, $y, $width, 1);
				break;

			case IMAGE_FLIP_BOTH:
			case 'b':
				for( $x=0 ; $x<$width ; $x++ )
					imagecopy($imgdest, $imgsrc, $width-$x-1, 0, $x, 0, 1, $height);

				$rowBuffer = imagecreatetruecolor($width, 1);
				for( $y=0 ; $y<($height/2) ; $y++ ) {
					imagecopy($rowBuffer, $imgdest  , 0, 0, 0, $height-$y-1, $width, 1);
					imagecopy($imgdest  , $imgdest  , 0, $height-$y-1, 0, $y, $width, 1);
					imagecopy($imgdest  , $rowBuffer, 0, $y, 0, 0, $width, 1);
				}

				imagedestroy( $rowBuffer );
				break;
			}

		$this->_setImage($imgdest);
		return $this;
	}

	/**
	 * Resize the image to an new size. Size can be specified in the arugments.
	 * Agruments :
	 *  @param int $new_width - The width of the desired image. If 0, the function will automatically calculate the width using the height ratio.
	 *	@param int $new_height - The height of the desired image. If 0, the function will automatically calculate the value using the width ratio.
	 *	@param bool $use_resize- If true, the function uses imagecopyresized() function instead of imagecopyresampled().
	 *					Resize is faster but poduces poorer quality image. Resample on the other hand is slower - but makes better images.
	 * @returns Image
	 * Example: $img -> resize(60, 0, false); // Better quality image created using width ratio
	 * 			$img -> resize(120, 300);
	 */
	function resize($new_width,$new_height, $use_resize = true) {
		if(!$this->image) return false;
		if(!$new_height and !$new_width) return false; //Both width and height is 0

		$height = $this->height;
		$width  = $this->width;

		//If the width or height is give as 0, find the correct ratio using the other value
		if(!$new_height and $new_width) $new_height = $height * $new_width / $width; //Get the new height in the correct ratio
		if($new_height and !$new_width) $new_width	= $width  * $new_height/ $height;//Get the new width in the correct ratio

		//Create the image
		$new_image = imagecreatetruecolor($new_width,$new_height);
		// gestion de l'alpha
		if (true) // modif GEB 2011-08-16
		{
			imagesavealpha($new_image, true);
			if( $this->info['mime']=='image/png')
				imagefill($new_image, 0, 0, imagecolorallocatealpha($new_image, 255, 255, 255, 127));
			else if($this->info['mime']=='image/jpeg')    // sinon artefacts sur l'alpha lors du resample
				imagefill($new_image, 0, 0, imagecolorallocate($new_image, 255, 255, 255));
		}
		else
			imagealphablending($new_image, false);
		// redimenssionnement
		if($use_resize)
			imagecopyresized($new_image, $this->image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
		else
			imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

		$this->_setImage($new_image);
		return $this;
	}

	/**
	 * Crops the image from the center and resizes it to an new size
	 * Agruments :
	 *  @param int $target_width - The width of the desired image.
	 *	@param int $target_height - The height of the desired image.
	 * @returns Image
	 * Example: $img -> resizeCenterCrop(200, 200);
	 */
	function resizeCenterCrop($target_width, $target_height, $use_resize = true) {
		$img_size = $this->info;
		// landscape
		if ($img_size[0] > $img_size[1]) {
			$new_w = $img_size[0] * $target_height / $img_size[1];
			if ($new_w < $target_width) {
				$h = 0;
				$w = $new_w = $target_width;
			} else {
				$h = $target_height;
				$w = 0;
			}
			// crop start position
			$x = (int) floor($new_w / 2 -  $target_width / 2);
			$y = 0;
		// portrait
		} else {
			$new_h = $img_size[1] * $target_width / $img_size[0];
			if ($new_h < $target_height) {
				$h = $new_h = $target_height;
				$w = 0;
			} else {
				$h = 0;
				$w = $target_width;
			}
			// crop start position
			$x = 0;
			$y = (int) floor($new_h / 2 -  $target_height / 2);
		}
		$this->resize($w, $h, $use_resize)->crop($x, $y, $x + $target_width, $y + $target_height);
		return $this;
	}

	/**
	* Resize by a ratio
	*
	* @param float $ratio
	* @param bool $use_resize
	* @returns Image
	*/
	function stretch($ratio, $use_resize)
	{
		$w = round($this->width * $ratio);
		$h = round($this->height * $ratio);
		return $this->resize($w, $h, $use_resize);
	}

	/**
	 * Crops the given image from the ($from_x,$from_y) point to the ($to_x,$to_y) point.
	 * Arguments :$from_x - X coordinate from where the crop should start
	 *			  $from_y - Y coordinate from where the crop should start
	 *			  $to_x   - X coordinate from where the crop should end
	 *			  $to_y   - Y coordinate from where the crop should end
	 * Example: $img -> crop(250,200,400,250);
	 */
	function crop($from_x,$from_y,$to_x,$to_y) {
		if(!$this->image) return false;

		$height = $this->height;
		$width  = $this->width;

		$new_width  = $to_x - $from_x;
		$new_height = $to_y - $from_y;

		//Create the image
		$new_image = imagecreatetruecolor($new_width, $new_height);

		imagealphablending($new_image, false);
		imagecopy($new_image, $this->image, 0,0, $from_x,$from_y, $new_width, $new_height);
		$this->_setImage($new_image);
		return $this;
	}

	/**
	 * Expands the given image to a new size, without resizing. Allows to create borders
	 * Arguments :
	 *  @param int $new_width - The width of the desired image.
	 *	@param int $new_height - The height of the desired image.
	 *	@param string $fill_color - The fill color.
	 * @returns Image
	 * Example: $img -> expand(600, 600, 'ff0000');
	 * 			$img -> expand(120, 300); // default fill color to white
	 */
	function expand($new_width, $new_height, $fill_color = "ffffff") {
		if(!$this->image) return false;

		$height = $this->height;
		$width  = $this->width;

		$x_origin = 0;
		$y_origin = 0;

		// no expanding necessary
		if ( $new_height < $height || $new_width < $width ) {
			return $this;
		};

		// Create the image
		$new_image = imagecreatetruecolor($new_width, $new_height);

		// Center in width
		if ( $new_width > $width ) {
			$x_origin = (int) (( $new_width - $width ) / 2);
			$new_width = $width;
		}
		// Center in height
		if ( $new_height > $height ) {
			$y_origin = (int) (( $new_height - $height ) / 2);
			$new_height = $height;
		}
		// Fill new image with $fill_color
		list($color_r, $color_g, $color_b) = sscanf(sprintf("%06s", $fill_color), "%2x%2x%2x");
		imagefill($new_image, 0, 0, imagecolorallocate($new_image, $color_r, $color_g, $color_b));

		imagealphablending($new_image, false);
		imagecopy($new_image, $this->image, $x_origin,$y_origin, $from_x,$from_y, $new_width, $new_height);
		$this->_setImage($new_image);
		return $this;
	}

	/**
	 * Shrinks the given image to a new size, without resizing. Allows to create borders
	 * Arguments :
	 *  @param int $new_width - The width of the desired image.
	 *	@param int $new_height - The height of the desired image.
	 *	@param string $fill_color - The fill color.
	 * @returns Image
	 * Example: $img -> expand(600, 600, 'ff0000');
	 * 			$img -> expand(120, 300); // default fill color to white
	 */
	function shrink($new_width, $new_height, $fill_color = "ffffff") {
		if(!$this->image) return false;

		$height = $this->height;
		$width  = $this->width;

		$x_origin = 0;
		$y_origin = 0;

		// no shrinking necessary
		if ( $new_height > $height && $new_width > $width ) {
			return $this;
		};
		
		$img_size = $this->info;
		// landscape
		if ($img_size[0] > $img_size[1]) {
			$w = $new_width;
			$h = 0;
		// portrait
		} else {
			$w = 0;
			$h = $new_height;
		}
		$this->resize($w, $h, false)->expand($new_width, $new_height, $fill_color);
		return $this;
	}

	/**
	 * Save the image to the given file. You can use this function to convert image types to. Just specify the image format you want as the extension.
	 * Argument:
	 * 	@param string $file_name - the file name to which the image should be saved to
	 *  @param bool $destroy
	 * @returns bool false if save operation fails.
	 * Example: $img->save("image.png");
	 * 			$image->save('file.jpg');
	 */
	function save($file_name, $destroy = true) {
		if(!$this->image) return false;

		$extension = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));

		switch($extension) {
			case 'png' : return imagepng($this->image, $file_name); break;
			case 'jpeg':
			case 'jpg' : return imagejpeg($this->image, $file_name); break;
			case 'gif' : return imagegif($this->image, $file_name); break;
			default: break;
		}
		if($destroy) $this->destroy();
		return false;
	}

	/**
	 * Display the image and then destroy it.
	 * Example: $img->show();
	 */
	function show($destroy = true) {
		if(!$this->image) return false;

		header("Content-type: ".$this->info['mime']);
		switch($this->info['mime']) {
			case 'image/png' : imagepng($this->image); break;
			case 'image/jpeg': imagejpeg($this->image); break;
			case 'image/gif' : imagegif($this->image); break;
			default: break;
		}
		if($destroy) $this->destroy();

		return $this;
	}

	/**
	 * Discard any changes made to the image and restore the original state
	 */
	function restore() {
		$this->_setImage($this->org_image);
		return $this;
	}



	/**
	 * Destroy the image to save the memory. Do this after all operations are complete.
	 */
	function destroy() {
		 imagedestroy($this->image);
		 imagedestroy($this->org_image);
	}

	/**
	 * Image infos
	 */
	function getInfos($k = null) {
		if (!is_null($k) && array_key_exists($k, $this->info)) {
			return $this->info[$k];
		}
		return $this->info;
	}

	/**
	 * renvoi la ressource Image
	 */
	function getImage() {
		if (!$this->image) {
			return false;
		}
		$x = imagesx($this->image);
		$y = imagesy($this->image);
		return $this->image;
	}
}
