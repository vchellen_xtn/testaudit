<?php
/**
* Gestion des négociations
*/

class PromotionNegociation_Collection extends Ada_Collection {
	
	/**
	* Renvoyer les négociations correspondant ŕ l'offre
	* 
	* @param Offre_Collection $offres
	* @return PromotionNegociation_Collection
	*/
	static public function factory(Offre_Collection $offres) {
		// les dates
		$date = date('Y-m-d');
		$jour = date('N');
		
		// la liste des négociations
		$sql = 'select distinct n.*';
		$sql.=' from promotion_negociation n';
		$sql.=' join categorie c on c.type=n.type';
		$sql.=' where c.id IN ('.implode(',', $offres->getCategories()).')';
		$sql.=" and n.jour=$jour and n.actif=1";
		$sql.=" and (n.depuis is null or n.depuis <= '$date')";
		$sql.=" and (n.jusqua is null or n.jusqua >= '$date')";
		$sql.=' and find_in_set(c.mnem, n.categories)';
		$sql.=' order by n.id';
		
		// renvoyer les négociations
		$x = new PromotionNegociation_Collection();
		$x->loadFromSQL($sql);
		return $x;
	}
}
?>
