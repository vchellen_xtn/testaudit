<?
/**
** PgKm.php - v�rification de l'inscription au programme Kilom'Etre
** Requiert :
**	XmlRpc.php
**	XMLRPC_PGKM_URL
**		(localhost)		http://localhost/Ada/ADA030-CRM/html/xmlrpc.php
**	(localhost)		http://localhost/Ada/ADA030-CRM/html/xmlrpc.php
**	(pr�-prod)		http://pgkm.ada.lbn.fr/xmlrpc.php
**	(production)	http://www.programmekilometre.fr/xmlrpc.php
*/

if (SITE_MODE == 'DEV')
{
	define('PGKM_URL', 'http://localhost/Ada/ADA030-CRM/html/');
	define('PGKM_SOAP_LOGIN', 'gdubourguet');
	define('PGKM_SOAP_PWD', 'pomme');
} else {
	if (SITE_MODE == 'PROD') {
		define('PGKM_URL', 'http://www.programmekilometre.fr/');
	} else {
		define('PGKM_URL', 'http://pgkm.ada.lbn.fr/');
	}
	define('PGKM_SOAP_LOGIN', 'unipro');
	define('PGKM_SOAP_PWD', 'I53u549R6S');
}
define('PGKM_SOAP_URL', PGKM_URL.'services/soap?wsdl');

// inclure XML_RPC
include_once('XmlRpc.php');


class PgKm
{
	const CALC_PRIVATE_KEY = 'a0dc9a87790b715e5b5f5c35ddf1f578';
	static public $TYPE_PERMIS = array(
									 'A' => 'A - Motocyclettes'
									,'AL'=> 'AL - Motocyclettes l�g�res'
									,'AT'=> 'AT - Tricycles et quadri. � moteur'
									,'B' => "B - V�hicules de moins de 10 places"
									,'C' => "C - V�hicules de transport de marchandises de plus de  3,5 tonnes"
									,'D' => "D - V�hicules de plus de 9 places"
									,'E' => "Permis poids lourds"

	);
	/* Serveus SOAP et XML*/
	static protected $_xmlrpc = null; // xmlrpc_client
	static protected $_soapSvr = null;
	static protected $_soapID = null;
	
	/**
	* Renvoie l'URL PgKM
	* 
	*/
	static public function getPgKmURL() { return PGKM_URL; }
	
	/**
	* v�rifie si un e-mail est membre du programme kilom'�tre
	* 
	* @param string $email
	* @return bool
	*/
	static public function checkEmail($email, $autofix = false)
	{
		$key = md5($email.self::CALC_PRIVATE_KEY.$email);
		if ($xmlrpc_resp = self::_callXmlRpc('calculometre.checkEmail', array($email, $key)))
		{
			if ($autofix && $xmlrpc_resp->value())
				sqlexec("UPDATE client SET pgkm=1 WHERE email='".addslashes($email)."'");
			return $xmlrpc_resp->value();
		}
		return false;
	}
	
	/**
	* envoie un e-amil par programme kilom'�tre
	* 
	* @param string $email
	*/
	static public function sendMail($email)
	{
		if ($xmlrpc_resp = self::_callXmlRpc('calculometre.sendMail', array($email)))
			return $xmlrpc_resp->value();
	}
	/**
	* Lien pour s'enrgistrer sur le programme kilom'�tre
	* 
	* @param string $email
	* @return string url pour isncription
	*/
	static public function getURLRegister($email)
	{
		return PGKM_URL.'user/register?keymail='.base64_encode($email);
	}
	
	/**
	* Appel XMLRPC 
	* 
	* @param string $method - nom de la m�thode
	* @param array $args - tableau des arguments
	* @return xmlrpcresp
	*/
	static protected function _callXmlRpc($method, $args)
	{
		if (!self::$_xmlrpc)
			self::$_xmlrpc = new xmlrpc_client(PGKM_URL.'xmlrpc.php');
		self::$_xmlrpc->return_type = 'phpvals';
		// param�tres � passer
		if (!$args || !is_array($args))
			$args = array($args);
		$x_args = array();
		foreach ($args as $v)
			$x_args[] = new xmlrpcval($v);
		$xmlrpc_msg = new xmlrpcmsg($method, $x_args);
		$xmlrpc_resp = self::$_xmlrpc->send($xmlrpc_msg);
		if ($xmlrpc_resp->faultCode())
		{
			$a['id'] = '';
			$a['methode'] = $method;
			$a['args'] = join(', ', $args);
			$a['erreur_code'] = $xmlrpc_resp->faultCode();
			$a['erreur_message'] = $xmlrpc_resp->faultString();
			save_record('log_pgkm', $a);
			return null;
		}
		return $xmlrpc_resp;
	}
	/**
	* Enregistrer l'utilisateur dans PgKm avec ses informations de permis
	* 
	* @param Client $client
	* @param string $permis_type
	* @param string $permis_numero
	* @param string $permis_date
	* @param string $permis_lieu
	*/
	static public function registerUser(Client $client, $permis_type, $permis_numero, $permis_date, $permis_lieu)
	{
		// obtenir l'agence de la derni�re r�servation
		foreach($client->getReservations(null, 1) as /** @var Reservation */ $reservation)
		{
			$agence = $reservation->getAgence();
			if ($agence['reseau']=='ADA')
				break;
			$agence = null;
		}
		if (!$agence) return "Vous devez avoir fait au moins une r�servation dans une agence ADA.";
		
		// calculometre.registerUser($email, $nom, $prenom, $adresse1, $adresse2, $codepostal, $ville, $telephone, $naissance)
		$a = array (
			 'email'	=> $client['email']
			,'nom'		=> utf8_encode($client['nom'])
			,'prenom'	=> utf8_encode($client['prenom'])
			,'adresse1'	=> utf8_encode($client['adresse'])
			,'adresse2'	=> ''
			,'codepostal'=> utf8_encode($client['cp'])
			,'ville'	=> utf8_encode($client['ville'])
			,'telephone'=> preg_replace('/[^0-9]/','',$client['tel'])
			,'naissance'=> $client['naissance']
			,'permis_type'=> $permis_type
			,'permis_numero'=> $permis_numero
			,'permis_date'	=> $permis_date
			,'permis_lieu'	=> utf8_encode($permis_lieu)
			,'unipro_origine'	=> $agence['code_societe'].$agence['id']
			,'unipro_categorie'	=> 'A'
		);
		return self::_callSoap('calculometre_saveUser', $a);
	}
	static public function searchUser($email)
	{
		return self::_callSoap('calculometre_searchUser', array('search'=>$email));
	}
	
	/**
	* Appelle le serveur SOAP de Progreamme Kilometre
	* 
	* @param string $method la m�thode � appeler
	* @param array $args les arguemnts
	*/
	static protected function _callSoap($method, $args)
	{
		try
		{
			// cr�er le client SOAP
			if (!self::$_soapSvr)
				self::$_soapSvr = new SoapClient(PGKM_SOAP_URL, array('compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP));
			if (!self::$_soapSvr) return false;

			// obtenir un identifiant de session
			if (!self::$_soapID)
			{
				$sessionID = self::$_soapSvr->__soapCall('calculometre_connect', array('123456789'));
				// puis se connecter
				self::$_soapID = self::$_soapSvr->__soapCall('calculometre_login', array('sessid'=>$sessionID,'username'=> PGKM_SOAP_LOGIN, 'password'=>PGKM_SOAP_PWD)); 
			}
			if (!self::$_soapID) return false;
		
			// appeler la m�thode que l'on souhaite appeler
			return self::$_soapSvr->__soapCall($method, array_merge(array('sessid'=>self::$_soapID), $args));
		}
		catch(Exception $e) 
		{
			$msg = (string) $e;
		};
		return false;
	}
}
?>
