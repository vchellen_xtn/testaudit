<?php
class ProspectPro extends Ada_Object
{
	const COURTE_DUREE = 'courte_duree';
	const MOYENNE_DUREE = 'moyenne_duree';
	const OFFRE_HYUNDAI = 'offre_hyundai';
	
	static public $LBL_ORIGINE = array(
		self::COURTE_DUREE => 'courte dur�e',
		self::MOYENNE_DUREE => 'moyenne dur�e',
		self::OFFRE_HYUNDAI => 'offre Hyundai'
	);
	
	protected $_origine = null;
	
	/**
	* R�cup�rer un prospect
	* 
	* @param int $id
	* @returns ProspectPro
	*/
	static public function factory($id)
	{
		$x = new ProspectPro();
		$x->load($id);
		$x->_origine = $x->getData('origine');
		return $x;
	}
	
	/**
	* Cr�ee un prospect
	* 
	* @param string $origine COURTE_DUREE ou MOYENNE_DUREE
	* @param array $data
	* @param array $errors
	* @return ProspectPro
	*/
	static public function create($origine, &$data, &$errors)
	{
		if (!in_array($origine, array(self::COURTE_DUREE, self::MOYENNE_DUREE, self::OFFRE_HYUNDAI))) return;
		$x = new ProspectPro();
		$x->_origine = $origine;
		if (isset($data['agence']) && Agence::isValidID($data['agence'])) {
			// pour le formulaire moyenne dur�e on pr�cise l'agence
			$agence = Agence::factory($data['agence']);
			if (!$data['agence_actuelle']) {
				$data['agence_actuelle'] = sprintf('%s - %s', $agence['id'], $agence['nom']);
			}
		}
		if ($x->validateData($data, $errors))
		{
			// faire la liste des v�hicules
			if (is_array($data['vehicules']))
			{
				$vehicules = array();
				foreach($data['vehicules'] as $k)
				{
					if ($v = VisuelMoyenneDuree::factory((int) $k))
						$vehicules[] = strtoupper($v['type']).' - '.$v['libelle'].' - '.$v['description'];
				}
				$data['vehicules'] = join("\n", $vehicules);
			}
			// s'il n'y a pas d'erreurs � ce stade on :
			// - cr�e le prospect
			// - envoie un e-mail � ADA
			if (!count($errors))
			{
				$data['id'] = '';
				$data['origine'] = $x->_origine;
				$x->setData($data)->save();
				$x = self::factory($x->getId());
				
				// envoyer le mail de confirmation
				$html = '<table border="1" cellpadding="2" cellspacing="0">';
				if ($agence) {
					$html.= '<tr><th>Envoy&eacute; &agrave;</th><td>'.$agence['email'].'</td></tr>'."\n";
				}
				foreach($x->getData() as $k => $v)
				{
					if ($k=='modification' || is_null($v)) continue;
					if ($k == 'origine') $v = self::$LBL_ORIGINE[$v];
					$k = str_replace('_', ' ', $k);
					$v = nl2br($v);
					$html.= "<tr><th>$k</th><td>$v</td></tr>\n";
				}
				$html.='</table>';
				$html = load_and_parse(BP.'/emails/em-moyenne-duree-commercial.html', array('data'=>$html));
				// destinataire : le si�ge sauf si l'agence est pr�cis�e
				$to = EMAIL_MOYENNEDUREE;
				$cc = '';
				if ($agence && SITE_MODE == 'PROD') {
					$cc = $to;
					$to = $agence['email'];
				}
				send_mail($html, $to, SUBJECT_PRO.' '.self::$LBL_ORIGINE[$x->getData('origine')], EMAIL_FROM, EMAIL_FROM_ADDR, $cc, '');

				// renvoyer le partenaire
				return $x;
			}
		}
		return null;
	}
	
	/**
	* Renvoie le tableau des r�gles de validation
	*/
	protected function _getValidatingRules()
	{
		$rules = array (
			'id'		=> $this->_addRuleInt(),
			'raison_sociale'		=> $this->_addRuleString('Vous devez pr�ciser le nom de la soci�t�'),
			'siret'		=> $this->_addRuleRegExp('/^\d{14}$/', 'Vous devez pr�ciser le num�ro SIRET', "Le num�ro SIRET %s n'est pas valide"),
			'naf'		=> $this->_addRuleRegExp('/^\d{3,4}[A-Z]$/i', "Vous devez pr�ciser le code NAF", "Le code NAF n'est pas valide"),
			'nom'	=> $this->_addRuleString("Vous devez pr�ciser votre nom"),
			'prenom'	=> $this->_addRuleString("Vous devez pr�ciser votre pr�nom"),
			'email'		=> $this->_addRuleEmail("Vous devez pr�ciser un e-mail", "L'e-mail %s n'est pas valide"),
			'tel'		=> $this->_addRulePhone("Vous devez pr�ciser le t�l�phone", "Le num�ro de t�l�phone n'est pas valide"),
			'adresse1'	=> $this->_addRuleString("Vous devez pr�ciser l'adresse"),
			'adresse2'	=> $this->_addRuleString(),
			'cp'		=> $this->_addRuleRegExp('/^\d{5}$/', "Vous devez pr�ciser le code postal", "Le code postal n'est pas valide"),
			'ville'		=> $this->_addRuleString("Vous devez pr�ciser la ville"),
			'commentaires'		=> $this->_addRuleString(),
		);
		if ($this->_origine == self::MOYENNE_DUREE || $this->_origine == self::OFFRE_HYUNDAI)
		{
			$rules['agence_actuelle']	= $this->_addRuleString();
			$rules['nombre_vehicules'] = $this->_addRuleInt("Vous devez pr�ciser le nombre de v�hicules", "Le nombre de v�hicules n'est pas correct");
			$rules['periode'] = $this->_addRuleString("Vous devez pr�ciser la p�riode de location");
			$rules['duree'] = $this->_addRuleString("Vous devez pr�ciser la dur�e de location");
			$rules['vehicules'] = $this->_addRuleString();
		}
		return $rules;
	}
	
	/**
	* Valider les donn�es puis v�rifications compl�mentaire
	* 
	* @param array $data
	* @param array $errors
	* @param mixed $fields
	* @returns boolean
	*/
	public function validateData(&$data, &$errors, $fields = null)
	{
		if (parent::validateData($data, $errors, $fields))
		{
			if ($data['siret'] && !Partenaire::validateSIRET($data['siret']))
			{
				$errors[] = "Le num�ro SIRET ".$data['siret']." n'est pas valide.";
				$data['siret'] = null;
				return false;
			}
			return true;
		}
		return false;
	}
}
?>
