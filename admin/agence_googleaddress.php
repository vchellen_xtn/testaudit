<?
	require_once ("secure.php");
	require_once ("../lib/lib.sql.php");

	// pour �cire les fichiers UNICODE
	define ("BEBOM", pack('CC', 0xfe, 0xff));
	define ("LEBOM", pack('CC', 0xff, 0xfe));

	/**
	* Documentation
	* 	https://support.google.com/business/answer/3370250?p=primary_category&hl=fr&rd=1
	*/
	
	// TODO : enregistrer au format UNICODE
	// provoquer le dialogue de t�l�chargement
	header("Content-type: text/plain");
	header("Content-Disposition: attachment; filename=ada_google_".date('Y-m-d').".txt");
	
	// indiquer � Google que l'on fait un export complet
	$out[]= 'Complete';
	// exporter les agences
	$sql = "select ap.id store_code, 'Agence de location de voitures' primary_category, 'ADA' name, ap.adresse1 address_line_1, ap.adresse2 address_line_2, ap.ville city, NULL state, ap.cp postal_code, 'FR' country_code, a.tel main_phone, a.fax fax_phone, aa.canon home_page, null hours, DEGREES(ap.latitude) `c:latitude:latitude`, DEGREES(ap.longitude) `c:longitude:longitude`";
	$sql.=" from agence_pdv ap";
	$sql.=" join agence a on a.id=ap.agence";
	$sql.=" join agence_alias aa on aa.id=ap.id";
	$sql.=" where a.zone in ('fr','co','gf','gp','mq','nc','re') AND a.reseau='ADA' and (a.statut&1)=1 AND ap.publie=1 AND LENGTH(ap.cp)=5";
	// on ne soirt que les points de vente principaux
	$sql.="   and ap.agence=ap.id";
	// limiter aux agences qui ne se sont pas inscrites individuellement
//	$sql.=" and ap.id not in ('FR600A')";
	$sql.=" order by ap.id";


	// renvoyer l'ensemble des informations
	$rs = sqlexec($sql);
	while($row = mysql_fetch_assoc($rs))
	{
		if (!$first)
		{
			$first = true;
			$out[] = join("\t", array_keys($row));
		}
		// quelques ajustements
		$row['name'] = ucwords(strtolower($row['name']));
		$row['city'] = ucwords(strtolower($row['city']));
		if (!preg_match('/[a-z]/', $row['address_line_1']))
			$row['address_line_1'] = ucwords(strtolower($row['address_line_1']));
		$row['main_phone'] = str_replace('.',' ', $row['main_phone']);
		$row['fax_phone'] = str_replace('.',' ', $row['fax_phone']);
		$row['home_page'] = 'http://www.ada.fr/location-voiture-'.$row['home_page'].'.html';
		
		// construire les heures
		$hours = array();
		$horaires = sqlexec("select h.jour, h.ouverture, h.fermeture from agence_horaire h where h.agence='".$row['store_code']."' order by h.jour");
		while ($h = mysql_fetch_assoc($horaires))
			$hours[] = ($h['jour']==7 ? 1 : (1+$h['jour'])).':'.substr($h['ouverture'],0,5).':'.substr($h['fermeture'],0,5);
		$row['hours'] = join(',', $hours);
		// sortir la ligne
		$out[] = join("\t", $row);
	}
	// fin du transfert
	$out[] = 'END';
	
	// tout renvoyer en UNICODE
	$txt = iconv('ISO-8859-1', 'UCS-2LE', join("\r\n", $out));
	if (($bom=substr($txt, 0, 2)) == BEBOM || $bom == LEBOM)
		$txt = substr($content, 2);
	echo LEBOM.$txt;
?>
