<?
class PartenaireTarif extends Ada_Object {
	/**
	* Renvoie le tarif � proposer � un partenaire
	* 
	* @param Partenaire $partenaire
	* @returns PartenaireTarif
	*/
	static public function factory(Partenaire $partenaire, $date_from = null)
	{
		/** Les tarifs d�pendent de :
		* 	de l'origine (webce, webpro)
		* 	du nombre de r�servations effecut�es dans un certain d�lai
		*/
		// 1. on s�lectionne tous les tarifs pour ce type de partenaire
		if (!$date_from)
			$date_from = date('Y-m-d');
		$rs = sqlexec(sprintf("select * from partenaire_tarif where origine='%s' order by prix_ht asc, duree desc", $partenaire['origine']));
		while ($x = new PartenaireTarif(mysql_fetch_assoc($rs)))
		{
			if ($x['depuis'])
			{
				if ($partenaire->getReservationsBetween(date('Y-m-d', strtotime('-'.$x['depuis'].' months', strtotime($date_from))), $date_from) >= $x['reservations'])
					return $x;
				continue;
			}
			// sinon on renvoie le premier qui n'a pas de conditions
			return $x;
		}
	}
};
?>
