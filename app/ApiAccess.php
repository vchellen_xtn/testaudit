<?php

class ApiAccess extends Ada_Object
{
	const MAX_MONTHS = 9;
	
	// authentification
	const ERR_LOGIN		= 101;
	const ERR_PWD		= 102;
	const ERR_IP		= 103;
	// arguments
	const ERR_ORIGINE	= 201;
	const ERR_TYPE		= 202;
	const ERR_DATE		= 203;
	const ERR_DELAY		= 204;
	const ERR_LOCALISATION 	= 205;
	// recherche agence
	const ERR_AGENCE = 301;
	// r�servation
	const ERR_RESERVATION = 401;
	const ERR_CLOSED_START = 402;
	const ERR_CLOSED_RETURN = 403;
	
	/** 
	*	Cr�e un acc�s � l'API en v�rifiant login, mot de passe et IP
	**	@param string $login identifiant � l'API
	**	@param string $pwd	 mot de passe
	**	@param string $ip	 IP de l'appelant
	**	@returns ApiAccess
	*/
	static public function factory($login, $pwd, $ip)
	{
		$x = new ApiAccess();
		$x->load($login, '*', $pwd);
		if ($x->isEmpty())
			return $x->_error(self::ERR_LOGIN, "L'identifiant est incorrect!");
		else if (!$x['logged_in'])
			return $x->_error(self::ERR_PWD, "Le mot de passe est incorrect !");
		else if (!IS_RND && !IS_ADA && $x['authorized_ips'] && !in_array($ip, explode(',', $x['authorized_ips'])))
			return $x->_error(self::ERR_IP, "L'IP $ip non valide !");
		$x['ip'] = $ip;
		return $x;
	}
	protected function _construct()
	{
		$this->setIdFieldName('login');
		return $this;
	}
	protected function _prepareSQL($id, $cols='*', $pwd=null)
	{
		if (!is_numeric($id))
			$id = "'".addslashes($id)."'";
		return "select $cols, (case when password('".addslashes($pwd)."')=pwd then 1 else 0 end) as logged_in from {$this->getTableName()} where {$this->getIdFieldName()}=$id";
	}

	/** Renvoie la liste des offres pour la requ�te effectu�e
	* 	Les arguments sont :
			origine	code � inscrire dans les URLs vers le site ADA
			type		vp | vu
			localisation recherche agence (code agence, d�partement, ville, adresse, code a�roport, coordonn�es longitude / latitude)
			debut	YYYY-MM-DD HH:MM
			fin		YYYY-MM-DD HH:MM
		@param array $args
		@returns Ada_Object
	*/
	public function getOffres($args)
	{
		/* v�rification des arugments */
		// code origine si pr�sent dans notre base, sinon on prendra celui �ventuellement d�fini pour ce login
		if ($args['origine'])
		{
			if (preg_match('/^[a-z0-9\-\_]+$/', $args['origine']) && getsql("select id from origine where id='".$args['origine']."'"))
				$this->_data['origine'] = $args['origine'];
			else
				return $this->_error(self::ERR_ORIGINE, "Le code origine n'est pas correct !");
		}
		// v�rifier le type de v�hicule
		if ($type && !in_array($args['type'], array('vp','vu')))
			return $this->_error(self::ERR_TYPE, "Le type n'est pas correct !");
		// oclaisation
		if (!trim($args['localisation']))
			return $this->_error(self::ERR_LOCALISATION, "La localisation est manquante !");
		// v�rifier les dates
		foreach(array('debut' => $args['debut'], 'fin' => $args['fin']) as $k => $v)
		{
			$t = strtotime($v);
			if ($v != date('Y-m-d H:i', $t))
				return $this->_error(self::ERR_DATE, "La date $k n'est pas correcte !");
			// on v�rifie le d�lai
			if ($t < time() || $t > strtotime('+'.self::MAX_MONTHS.' months'))
				return $this->_error(self::ERR_DELAY, "Pas d'informations disponibles.");
		}
		// pr�ciser la distance
		if (!preg_match('/^\d{2,4}$/', $args['distance']))
			$args['distance'] = null;
		/* il faut maintenant trouver une agence */
		$args['agence '] = null; 
		if (Agence::isValidID($args['localisation']) && ($agence = Agence::factory($args['localisation']))) {
			$args['agence'] = $agence['id'];
		} else {
			$agences = Agence_Collection::factory(array('search' => $args['localisation']));
			foreach($agences as /** @var Agence */ $agence)
			{
				$args['agence'] = $agence['id'];
				break;
			}
		}
		// on enregistre
		ApiLog::create($this, $args);
		if (!$agence)
			return $this->_error(self::ERR_AGENCE, "Aucuen agence ne correspond !");
		/* OK... on a tout ce qu'il faut pour renvoyer les donn�es */
		// cr�er la r�servation
		$tDepart = strtotime($args['debut']); 
		$tRetour = strtotime($args['fin']);
		$a = array(
			'agence'	=> $args['agence'],
			'type'		=> $args['type'],
			'categorie' => ($args['type']=='vp' ? 71 : 11),
			'depart'	=> date('d-m-Y', $tDepart),
			'h_depart'	=> date('H:i', $tDepart),
			'retour'	=> date('d-m-Y', $tRetour),
			'h_retour'	=> date('H:i', $tRetour),
			'distance'	=> $args['distance'],
			'age'	=> 25,
			'permis'=> 5
		);
		$reservation = Reservation::create($a, $msg, $url_redirect);
		if (!$reservation)
			return $this->_error(self::ERR_RESERVATION, $msg);
	/* la v�rification des horaires est � terminer : il y a actuellement un DELTA qui n'est pas correct
		// on v�rifie les horaires de d�part
		if ($agence->isClosed(date('Y-m-d', $tDepart), date('N', $tDepart), date('G', $tDepart))) {
			return $this->_error(self::ERR_CLOSED_START, "L'agence n'est pas ouverte pour votre d�part.");
		}
		if ($agence->isClosed(date('Y-m-d', $tRetour), date('N', $tRetour), date('G', $tRetour))) {
			return $this->_error(self::ERR_CLOSED_RETURN, "L'agence n'est pas ouverte pour votre retour.");
		}
	*/
		
		// faire la liste des offres
		$categories = Categorie_Collection::factory('fr',$a['type']);
		
		// pr�parer l'URL de base
		unset($a['categorie']);
		if ($this->_data['origine'])
			$a['co'] = $this->_data['origine'];
		$url = Page::getURL('?'.http_build_query($a).'&categorie=');
		// construire le tableau des ovvres
		$offres = array();
		foreach(Offre_Collection::factory($reservation, null, null, -1) as /** @var Offre */ $offre)
		{
			if ($forfait = $offre->getForfait())
			{
				$categorie = $categories[$offre['categorie']];
				$offres[] = array(
					'mnem' => $offre['mnem'],
					'acriss'=> $categorie['acriss'],
					'categorie' => $categorie['nom'],
					'modele' => $categorie['description'],
					'img'	 => Page::getURL($categorie->getImg('sml')),
					'img_med'	=> Page::getURL($categorie->getImg('med')),
					'img_big'	=> Page::getURL($categorie->getImg('big')),
					'km'	=> $forfait['km'],
					'prix' => $forfait->getPrix(),
					'stock'=> ($offre['nb'] > 0 ? 1 : 0),
					'url'  => $url.$offre['categorie']
				);
			}
		}
		// on renvioe les donn�es
		$data = array('status' => 'OK');
		$data['agence'] = $agence->toArray(array('id','nom','adresse1','adresse2','cp','ville'));
		$data['agence']['url'] = $agence->getURLAgence();
		$data['offres'] = $offres;
		return new Ada_Object($data);
	}
	/**
	* Renvoie un message d'erreur lors d'un appel API
	* 
	* @param int $code
	* @param string $msg
	* @return Ada_Object
	*/
	protected function _error($code, $msg)
	{
		return new Ada_Object(array('status' => 'KO', 'erreurr_code' => $code, 'erreur_message' => $msg));
	}
}