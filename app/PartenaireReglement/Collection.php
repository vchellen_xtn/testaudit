<?
class PartenaireReglement_Collection extends Ada_Collection
{
	/**
	* Renvoie les r�glements associ�es � un coupon
	* 
	* @param PartenaireCoupon $coupon
	* @return PartenaireReglement_Collection
	*/
	static public function factory(PartenaireCoupon $coupon)
	{
		$x = new PartenaireReglement_Collection();
		if ($coupon)
			$x->_loadFromSQL('select * from partenaire_reglement where coupon='.$coupon->getId().' order by id desc');
		return $x;
	}
}
?>
