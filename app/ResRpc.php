<?
class ResRpc extends Ada_Object
{
	/**
	* Acc�de aux informations du syst�me de gestion associ�e � la r�servation
	* 
	* @param Reservation $reservation
	*/
	static public function factory(Reservation $reservation)
	{
		if (!$reservation) return;
		$agence = $reservation->getAgence();
		if (!$agence) return;
		$key = strtolower($agence['reseau']);
		if ($key == 'ada') $key = 'unipro';
		if ($x = new ResRpc())
		{
			$x->setTableName("res_$key");
			return $x->load($reservation->getId());
		}
	}
	
	public function _construct()
	{
		$this->setIdFieldName('reservation');
	}
	
	/**
	* Renvoie 'citer' ou 'unipro'
	* @returns string
	*/
	public function getKey()
	{
		return substr($this->getTableName(), 4);
	}
	public function getSystem()
	{
		return strtoupper($this->getKey());
	}
	public function getCode()
	{
		return $this->_data[$this->getKey()];
	}
	public function doReset()
	{
		if ($this->isEmpty()) return false;
		$this->setData('validation', '')
			 ->setData('annulation', '')
			 ->setData($this->getKey(), '');
		$this->save();
		return true;
	}
}
?>
