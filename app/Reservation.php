<?

class Reservation extends Ada_Object
{
	static protected $FIELDS = array('type','categorie','forfait_univers','age','permis','agence','depart','h_depart','retour','h_retour','distance','illimite','codepromo','comptepro');

	/** @var Client */
	protected $_client;
	/** @var Agence */
	protected $_agence;
	/** @var Categorie */
	protected $_categorie;
	/** @var ResOption_Collection */
	protected $_options;
	/** @var Option_Collection */
	protected $_allOptions;
	/** @var PartenaireCoupon */
	protected $_coupon;
	/** @var PartenaireCoupon */
	protected $_origineCoupon;
	/** @var PromotionCode */
	protected $_promotion_code;
	/** @var Facturation_Collection */
	protected $_facturations;
	/** @var Offre_Collection */
	protected $_offres;
	/** @var Forfait */
	protected $_forfait;
	/** @var ConditionsAnnulation */
	protected $_conditionsAnnulation;

	/** @var ResInfo */
	protected $_resInfo;

	/**
	* Cr�e un objet � partir d'un identifiant
	*
	* @param int $id
	* @param bool $tryArchive
	* @return Reservation
	*/
	static public function factory($id, $tryArchive = false)
	{
		if (!$id) return null;
		$x = new Reservation();
		if ($x->load($id)->isEmpty())
		{
			// on essaye dans les archives
			$x->setTableName('_archive_reservation');
			if (!$tryArchive || $x->load($id)->isEmpty())
				return null;
			$x->setData('archive', 1);
		}
		return $x;
	}
	/**
	* Renvoie un tableau avec les crit�res de recherche � partir du COOKIE et du GET
	*
	* @param array $default valeur par d�faut
	* @return array
	*/
	static public function createSearchFromCookieAndGet($default = array())
	{
		// recherche m�moris�e avec priorit� au GET, et le valider en passant par la r�servation
		$search = isset($_COOKIE['ADA001_LAST_SEARCH']) ? array_merge(unserialize($_COOKIE['ADA001_LAST_SEARCH']), $_GET) : $_GET;
		if ($search && $criteria = Reservation::create($search, $msg, $url)) {
			$search = $criteria->getFields();
			$search['is_complete'] = true;
		} else if (!empty($default)) {
			return $default;
		} else {
			// si ce n'est pas valide (tous les arguments ne sont peut-�tres pas fournis) on essaie de r�cup�rer certains �l�ments
			$a = array('type' => 'vp');
			if (isset($search['type']) && isset(Categorie::$TYPE[strtolower($search['type'])])) {
				$a['type'] = strtolower($search['type']);
			}
			if (preg_match('/^\d*$/', $search['categorie'])) {
				$a['categorie'] = $search['categorie'];
			}
			if (isset($search['agence']) && Agence::isValidID($search['agence'])) {
				$a['agence'] = $search['agence'];
				$a['zone'] = getsql(sprintf("select zone from agence_alias where id = '%s'", $a['agence']));
			}
			if (isset($search['codepromo'])) {
				$a['codepromo'] = preg_replace('/[^a-z0-9]/i', '', $search['codepromo']);
			}
			$search = $a;
		}
		return $search;
	}

	/**
	* Cr�ation d'une r�servation � partir d'un tableau
		type		vp | vu | sp | ml
		categorie	identifiant de la cat�gorie
		age			�ge du conducteur sup�rieur �
		permis		nombre d'ann�es de permis sup�rieur �
		agence		identifiant alias
		ville		texte recherch� pour trouver l'agence
		depart		d�part date(dd-mm-yyyy)
		h_depart	d�part heure (HH:mm)
		retour		retour date (dd-mm-YYYY)
		h_retour	retour heure (HH:mm)
		distance	nombre de kilom�tres
		illimite	1 pour indiquer un forfait illimit�
		codepromo	code promotion
		comptepro	identifiant code professionnnel
	* @param array $a tableau d�finissant la r�servation
	* @param array $msg tableau des messages d'erreurs
	* @param string $url rediriger vers l'url pour les explications
	* @returns Reservation
	*/
	static public function create($data, &$msg, &$url, Reservation $clone=null)
	{
		$msg = array(); $a = array();
		foreach(self::$FIELDS as $k)
		{
			if (isset($data[$k]) && trim($data[$k])!='')
				$a[$k] = $data[$k];
		}

		// v�rification des param�tres
		if (!count($a))
		{
			$url = Page::getURL();
			return;
		}
		// premi�res v�rifications
		if (!$a['type'] || !Categorie::$TYPE[$a['type']])
			$msg[] = "Vous devez indiquer le type de v�hicule";
		if (!isset($a['categorie']) || empty($a['categorie'])) {
			$a['categorie'] = Categorie::getCatIDByDefaultFromType($a['type']);
		} else if (!is_numeric($a['categorie'])) {
			$msg[] = "Vous devez indiquer la categorie de v�hicule";
		}
		if (!$a['age'] || !is_numeric($a['age']))
			$msg[] = "Vous devez indiquer votre age";
		if (!isset($a['permis']) || !is_numeric($a['permis']))
			$msg[] = "Vous devez indiquer votre nombre d'ann�es de permis de conduire";
		if (!$a['agence'])
			$msg[] = "Vous devez pr�ciser l'agence";
		if (!$a['depart'] || !preg_match('/^\d{2}-\d{2}-\d{4}$/', $a['depart']))
			$msg[] = "Vous devez indiquer la date de d�part";
		if (!$a['h_depart'] || !preg_match('/^\d{2}:\d{2}$/', $a['h_depart']))
			$msg[] = "Vous devez indiquer l'heure de d�part";
		if (!$a['retour'] || !preg_match('/^\d{2}-\d{2}-\d{4}$/', $a['retour']))
			$msg[] = "Vous devez indiquer la date de retour";
		if ($a['type']=='ml' && $a['depart']!=$a['retour'])
			$msg[] = "La date de d�part et de retour doivent �tre identiques";
		if (!$a['h_retour'] || !preg_match('/^\d{2}:\d{2}$/', $a['h_retour']))
			$msg[] = "Vous devez indiquer l'heure de retour";
		if ($a['distance'] && !preg_match('/^\d{2,4}$/', $a['distance']))
			$msg[] = "La distance indiqu�e n'est pas correcte.";
		if ($a['illimite'])
			$a['illimite'] = ($a['illimite'] == '1');
		if (count($msg)) return;

		// on v�rifie l'agence
		$agence = ($clone && $clone['agence']==$a['agence'] ? $clone->getAgence() : Agence::factory($a['agence'], true));
		if (!$agence || $agence->isEmpty())
		{
			$msg[] = "Vous devez indiquer l'agence choisie";
			return;
		}
		// on r�cup�re la cat�gorie
		$categorie = ($clone && $clone['categorie']==$a['categorie'] ? $clone->getCategorie() : Categorie::factory($a['categorie']));
		if (!$categorie || strtolower($categorie['type'])!=$a['type'])
		{
			$msg[] = "La cat�gorie indiqu�e n'existe pas";
			return;
		}
		// v�rification sur la cat�gorie et le profil
		// on v�rifiera l'�ge et le permis dans la constituion des offres
		$a['age'] = (int) $a['age'];
		$a['permis'] = (int) $a['permis'];
		if ($a['forfait_univers'])
			$a['forfait_univers'] = (int) $a['forfait_univers'];
		/*
		if (($categorie['age'] > (int)$a['age']) || ($categorie['permis'] > (int)$a['permis']))
		{
			$url = Page::getURL('reservation/profil.html?categorie='.(int)$a['categorie']);
			return;
		}
		*/
		// on cr�e la r�servation que l'on va compl�ter
		$a['statut'] = 'C';
		$a['agence'] = $agence['agence'];
		$a['pdv'] = $agence['pdv'];
		$a['alias'] = $agence['id'];
		$a['debut'] = euro_iso($a['depart']).' '.$a['h_depart'].':00';
		$a['fin'] = euro_iso($a['retour']).' '.$a['h_retour'].':00';
		$a['surcharge'] = $agence->getSurcharge($a['type']);
		$x = new Reservation($a);
		$x->_agence = $agence;
		$x->_categorie = $categorie;

		// on calcule la "dur�e" de la r�servation
		$t1 = $x->getDebut('U');
		$t2 = $x->getFin('U');
		// nuit offerte
		if (NUIT_OFFERTE || $a['type']=='sp')
		{
			// on r�cup�re la partie hh.mm de J2 (jj.hhmmm)
			$n2 = (($x->getJ2() * 10000) % 10000)/100;
			if ($a['type']=='sp' || $n2 > HEURE_DEBUT_NUIT || $n2 <= HEURE_FIN_NUIT)
				$t2 -= 3600 * (($n2 - HEURE_DEBUT_NUIT + 24) % 24);
		}
		// on tient compte du changement d'heure (DST)
		$t2 += 3600 * (date('I', $t2) - date('I', $t1));
		// dur�e en jours (moins la franchise)
		if ($a['type']=='ml') // dur�e en heures
			$duree = ($t2 - $t1) / 3600;
		else // dur�e en jours
			$duree = ($t2 - $t1 - FRANCHISE_MINUTES * 60) / (24 * 3600);
		$x->setData('duree', ($duree < 1 ? 1 : ceil($duree)));

		// et la saison
		$x->setData('saison', $agence->getZone()->getSaison($t1));

		// le code pro, code promo ou coupon de r�duction
		$code = $a['codepromo'] ? $a['codepromo'] : $a['comptepro'];
		if ($code)
		{
			// on note la saisie utilisateur pour l'affichage
			$code = htmlspecialchars(strip_tags($code));
			/** @var PartenaireCoupon */
			$coupon = ($clone ? $clone->_coupon  : PartenaireCoupon::factory($code, true));
			if (!$coupon || !$coupon['id'])
				$msg[] = "Le code <strong>$code</strong> n'est pas valide.";
			else
			{
				if (!$coupon->isValidFor($x, $explications))
					$coupon = null;
				if ($explications)
					$msg[] = $explications;
			}
		}
		if ($coupon)
		{
			$x->_coupon = $coupon;
			$x->setData('coupon', $coupon['id']);
		}
		$x->setData('forfait_type', ($coupon && $coupon['locapass'] ? 'LOCAPASS' : 'ADAFR'));
		// si le forfait et les options sont renseign�es
		if (isset($data['forfait']))
		{
			/** @var Forfait on r�cup�re le forfait */
			$forfait = Forfait::doUnserialize($_POST['forfait']);
			if (!$forfait)
			{
				$msg[] = "Le forfait n'est pas valide !";
				return null;
			}
			$x->_forfait = $forfait;
		}
		// r�cup�rer les options et parcourir les options pour calculer le total et pr�paere le SQL d'insertion
		$prix_options = $x->getPrixSurcharge() + $x->getPrixCoupon();
		if ($options = $x->buildResOptions($forfait, $data))
		{
			$x->_options = $options;
			$prix_options += $options->getTotal();
		}
		if ($forfait)
		{
			$x['forfait'] = $forfait['id'];
			$x['forfait_type'] = $forfait['forfait_type'];
			if ($x['forfait_type']=='JEUNES')
				$x->_categorie->loadJeunes();
			$x['categorie'] = $forfait['categorie'];
			// aucun tarf pour LOCAPASS
			if ($x['forfait_type']=='LOCAPASS') {
				$x['surcharge'] = null;
				$forfait->setData('unipro_vehicule', filter_var($data['unipro_vehicule'], FILTER_SANITIZE_STRING));
				if (AgenceResa::$OPERATION_LABEL[$data['operation']]) {
					$forfait->setData('operation', $data['operation']);
				}
			} else {
				if ($x['forfait_type']=='RESAS') {
					// le prix doit �tre indiqu�e dans $data
					if (!preg_match('/^\d+(\.\d{1,2})?$/', $data['prix_forfait']))
					{
						$msg[] = "Le prix du forfait n'est pas indiqu� !";
						return null;
					}
					// pour RESAS on g�re la surcharge sur la page "resultat"
					if ($x['surcharge']) {
						$prix_options -= $x['surcharge'];
						$x['surcharge'] = null;
					}
					// on r�cup�re le prix pass� en param�tre
					$forfait->setData('resas_tarif_ttc', $data['prix_forfait']);
					$forfait->setData('unipro_vehicule', filter_var($data['unipro_vehicule'], FILTER_SANITIZE_STRING));
					if (AgenceResa::$OPERATION_LABEL[$data['operation']]) {
						$forfait->setData('operation', $data['operation']);
					}
				} else {
					// dans le cas ADAFR et JEUNES on conserve les informations de promotion, r�duction et yield
					// elles ne doivent pas �tre conserv�es pour les RESAS car elles feraient doublon avec le tarif qui incluent d�j� cela [resas_tarif_ttc] d�finie ci-dessus
					if (isset($forfait['p_id'])) {
						$x['promotion'] = $forfait['p_id'];
					}
					if (isset($forfait['reduction'])) {
						$x['reduction'] = $forfait['reduction'];
					}
					if (isset($forfait['yield'])) {
						$x['yield'] = $forfait['yield'];
					}
					if (isset($forfait['nego']) && $forfait['nego'] > 0) {
						$x['nego'] = $forfait['nego'];
					}
				}
				$x['tarif'] = $forfait->getPrixBase();
				$x['total'] = $forfait->getPrix() + $prix_options;
			}
			if (!is_null($forfait['km']))
				$x['km'] = (int) $forfait['km'];
			$x['jours'] = (int) $forfait['jours'];
			$x['duree'] = (int) $forfait['duree'];
		}
		// renvoyer la r�servation maintenant compl�te
		return $x;
	}

	/**
	* Essaie d'appliquer un coupon � une r�servation
	*
	* @param PartenaireCoupon $reservation
	* @param string msg message d'erreur
	* @returns bool : si modif� ou non
	*/
	public function applyPartenaireCoupon(PartenaireCoupon $coupon, &$msg) {
		// le code existe et est valide...
		if (!$this->getId()) {
			$msg .= "La r�servation n'est pas valid�e.";
			return false;
		} else if ($this->isInPayment()) {
			// si la r�servation est en cours de paiement, on ne la modifie pas
			$msg .= "La r�servation est d�j� en cours de r�glement.";
			return false;
		} else if (!is_null($this->_data['coupon'])) {
			// il y a d�j� un code de r�duction utilis�...
			$msg .= "Vous avez d�j� utilis� un code promotionnel !";
			return false;
		} else if (!$coupon->isValidFor($this, $msg)) {
			if (!$msg) {
				$msg = "Ce code n'est pas valide !";
			}
			return false;
		}
		// le message peut alors contenir un message explicatif...
		if ($msg) $msg .= '<br/>';
		if (!is_null($coupon['locapass'])) {
			// on n'utilise pas le LOCAPASS sur cette page
			$msg .= "Vous ne pouvez pas utiliser ce code LOCAPASS � cette �tape.";
			return false;
		} else if ($this->_data['forfait_type'] != 'ADAFR') {
			// pas de code de r�duction pour les parcours autre que ADAFR
			$msg .= "Vous ne pouvez pas utiliser ce code � cette �tape.";
			return false;
		}
		// OK... on va maintenant associer la r�servation � ce coupon
		$this->setData('coupon', $coupon->getId());
		$this->_coupon = $coupon;
		$variation = 0;
		$prix_coupon = $this->getPrixCoupon();

		// puis onn recalcule un forfait avec ce coupon
		if ($offres = Offre_Collection::factory($this, null, 'fr', 1)) {
			if ($forfait = $offres->getForfait($this->_data['categorie'])) {
				// on va maintenant comparer le prix obtenu avec le coupon et le pirx actuel de la r�servation
				// � condition que le km et la dur�e soit bien comparable...
				if ($this->getPrixForfait() > $forfait->getPrix()
				&& $this->_data['duree'] <= $forfait['duree'] && ($this->_data['km'] <= $forfait['km'] || is_null($forfait['km'])))
				{
					$msg .= "Votre code de r�duction a bien �t� pris en compte.";
					$variation = round($forfait->getPrix() - $this->getPrixForfait(), 2);
					// modifier les informations li�es au forfait
					$this->_data['forfait'] = $forfait['id'];
					if (isset($forfait['p_id'])) {
						$this->_data['promotion'] = $forfait['p_id'];
					}
					if (isset($forfait['reduction'])) {
						$this->_data['reduction'] = $forfait['reduction'];
					}
					if (isset($forfait['yield'])) {
						$this->_data['yield'] = $forfait['yield'];
					}
					$this->_data['tarif'] = $forfait->getPrixBase();
					if (!is_null($forfait['km']))
						$this->_data['km'] = (int) $forfait['km'];
					$this->_data['jours'] = (int) $forfait['jours'];
					$this->_data['duree'] = (int) $forfait['duree'];
				} else if ($this->_data['reduction'] > 0) {
					// les prix sont identiques et il y a d�j� une r�duction � la r�servation
					if (!$prix_coupon) { // si on paye le coupon, on ne veut pas brouiller le message...
						$msg .= "Vous b�n�ficiez d�j� d'une promotion plus importante !";
					}
				} else {
					// aucune promotion m�me avec le code de r�duction
					$msg .= "Ce code ne vous permet pas d'obtenir une r�duction avec vos conditions de location";;
				}
			}
		}
		if (!$forfait) {
			$msg .= "Aucune promotion n'a �t� trouv�e pour vos conditions de r�servation !";
		}

		if ($prix_coupon || $variation) {
			$this->_data['total'] = round($this->_data['total'] + $prix_coupon + $variation, 2);
			// on enregistre les modifications effectu�es
			$this->save();
			return true;
		}
		return false;
	}

	/**
	* Renvoie le forfait assoic� � la r�servation
	* @returns Forfait
	*/
	public function getForfait()
	{
		return $this->_forfait;
	}
	/** Un forfait est-il d�finir @returns bool */
	public function hasForfait()
	{
		return ($this->_forfait != null);
	}
	/** Enregistre la r�servation */
	public function save($cols = '*')
	{
		if (!$this->getId())
		{
			$this->setData('id', '');
			parent::save();
			if ($this->_options)
				$this->_options->setReservation($this);
			// on essaie d'enregistrer le r�s�ultat de la n�gociation
			ResNego::create($this);
		}
		else
		{
			parent::save($cols);
		}
	}
	/**
	* REnvoie les chamsp de la r�servaiton
	*
	*/
	public function getFields()
	{
		$a = $this->toArray(self::$FIELDS);
		$a['zone'] = $this->_agence['zone'];
		if ($this->_data['alias'])
			$a['agence'] = $this->_data['alias'];
		if (!$this->getId())
			return $a;
		else
		{
			$a['depart'] = $this->getDebut('d-m-Y');
			$a['h_depart'] = $this->getDebut('H:i');
			$a['retour'] = $this->getFin('d-m-Y');
			$a['h_retour'] = $this->getFin('H:i');
			if (!$this->_data['km'])
				$a['illimite'] = 1;
			if ($x = $this->getPartenaireCoupon())
				$a['codepromo'] = $x->getData('code');
			return $a;
		}
	}
	// fonctions utilitaires
	public function __toString()
	{
		return 'Location '.$this->getResIDClient().' du '.$this->getDebut()." au ".$this->getFin();
	}
	public function toString($format='') { return $this->__toString(); }
	public function getResID()
	{
		return resid($this->_data);
	}
	public function getResIDClient()
	{
		return resid_client($this->_data);
	}
	/**
	* renvoie les offres possibles pour cette r�servaiton
	* @param string skin - modifie le comportement de getOffres
	* @returns Offre_Collection
	*/
	public function getOffres($skin = null)
	{
		if (!$this->_offres && $this->isBookableOnline())
		{
			if ($skin == Offre_Collection::OFFRE_ALTERNATIVES || $skin == Offre_Collection::OFFRE_TOUTES) {
				$this->_offres = Offre_Collection::factory($this, $this->getAgence(), 'fr', $skin);
			} else if ($skin=='v2') {
				$this->_offres = Offre_Collection::factory($this, $this->getAgence(), 'fr', $this->_data['type']=='vp' ? Offre_Collection::OFFRE_TOUTES : 1);
			} else if ($skin=='v3' || $skin=='v4') {
				$this->_offres = Offre_Collection::factory($this, $this->getAgence(), 'fr', in_array($this->_data['type'], array('vp','vu')) ? Offre_Collection::OFFRE_ALTERNATIVES : 1);
			} else if ($skin == 'jeunes' || $skin=='1') {
				$this->_offres = Offre_Collection::factory($this, $this->getAgence(), 'fr', 1);
			} else if ($skin == 'resas') {
				$this->_offres = Offre_Collection::factory($this, $this->getAgence(), 'fr', (in_array($this->_data['type'], array('vp','vu')) ? Offre_Collection::OFFRE_ALTERNATIVES : 1));
			} else {
				$this->_offres = Offre_Collection::factory($this);
			}
		}
		return $this->_offres;
	}
	// v�rifications
	public function isBookableOnline() { return $this->getAgence()->isBookableOnline(); }
	public function isTooLate($agence = null)
	{
		if (!$agence) $agence = $this->getAgence();
		$delai = $agence->getDelaiMin($this->getData('type'));
		if ($agence->getDelaiMode() != 'O')
			return ($this->getHoursBeforeStart() < $delai);
		else
		{
			// TODO: on s'int�resse aux heures d'ouvetures
			return ($this->getHoursBeforeStart() < $delai);
		}
	}
	/**
	* Renvoie les informations RPC (UNIPRO ou CITER)
	* @returns ResRpc
	*/
	public function getResRpc()
	{
		if ($this->_data['paiement'])
			return ResRpc::factory($this);
	}
	/**
	* Renvoie les LogPaybox pour cette R�servation
	* @returns LogPaybox_Collection
	*/
	public function getLogPaybox()
	{
		return LogPaybox_Collection::factory($this);
	}
	public function isLocapass()
	{
		return ($this->_data['forfait_type'] == 'LOCAPASS');
	}
	/**
	* Faut-il proposer UN EURO ?
	*
	* @param int $prix montant
	*/
	public function show1Euro($top=0, $left=0, $prix = null)
	{
		// pas locapass
		if ($this->_data['forfait_type']=='ADAFR' && !$this->isInPayment())
		{
			// le prix doit �tre spu�rieur � USE_1EURO
			if (!$prix) $prix = $this->getPrixTotal();
			if ($prix >= USE_1EURO)
			{
				// soit on ne connait pas le client, sinon il doit �tre en France
				$client = $this->getClient();
				if (!$client || $client['pays']=='fr')
				{
					$html = '<div class="un_euro">';
					$html.= '<script language="javascript" src="http://flux.effiliation.com/new1euro/11835874?montant='.$prix.'&type=3X5X10X20X&top='.$top.'&bottom=0&right=0&left='.$left.'"></script>';
					$html.= '</div>';
					return $html;
				}
			}
		}
		return null;
	}
	/**
	* La r�servation est-elle annulable
	*
	* @param string $msg contient le mmessage d'explicaiton en cas d'impossibilit� d'annulation
	* @returns bool annulable ou pas ?
	*/
	public function isCancellable(&$msg)
	{
		// la r�servation doit �tre pay�e
		$statut = $this->_data['statut'];
		if ($statut!='P')
		{
			if ($statut == 'A')
				$msg = 'La r�servation a d�j� �t� annul�e.';
			else if ($statut == 'X')
				$msg = "Une erreur s'est produite lors ".($this->_data['paiement'] ? "de l'annulation" : 'du paiement').'.';
			else
				$msg = "La r�servation n'a pas �t� pay�e.";
			return false;
		}
		// dans combien de temps le d�part est-il pr�vu ?
		$preavis = $this->getHoursBeforeStart();
		if ($preavis <= 0)
		{
			$msg = "Vous ne pouvez pas annuler apr�s la date de d�part.";
			return false;
		}
		// s'il y a d�j� eu un remboursement
		if ($this->_data['remboursement'])
		{
			$msg = "Un remboursement partiel a d�j� �t� effectu� pour cette r�servation";
			return false;
		}
		// message selon les conditions d'annulations
		if (!$this->_getConditionsAnnulation()->isCancellable($preavis, $msg))
			return false;

		// On teste le d�lai Paybox : on ne peut plus annuler apr�s 2 mois..
		if ($this->getData('paiement_mode')=='UN' || ($this->getData('paiement_mode')=='PB' && !$this->isCancellableByPaybox()))
		{
			// on supprime le message pr�c�dent qui peut laisser penser que l'annulation a �t� faite...
			$msg = "<strong>Cette r�servation ne peut pas �tre annul�e en ligne.</strong>";
			$msg.= '<br/>Pour demander un remboursement, merci d\'adresser un email � <a href="mailto:reservation@ada.fr">reservation@ada.fr</a>';
			$msg.= '<br/>en pr�cisant le n� de la r�servation concern�e : <strong>'.$this->getResIDClient().'</strong>.';
			$msg.= "<br/><br/>Nous vous confirmerons l'annulation dans les plus brefs d�lais.";
			return false;
		}
		return true;
	}
	/**
	* La r�servation peut-elle �tre annul�e ?
	* @returns bool
	*/
	public function isRefundable() {
		return $this->_getConditionsAnnulation()->isRefundable($this->getHoursBeforeStart());
	}
	
	// renvoie le nobmre d'heurs avant le d�part
	public function getHoursBeforeStart($name = null)
	{
		if ($name)
			$t = $this->_getTimestamp($name);
		if (!$t)
			$t = time();
		return ceil(($this->_getTimestamp('debut') - $t) / 3600);
	}
	/**
	* Si annulable par Paybox
	* @returns bool
	*/
	public function isCancellableByPaybox()
	{
		// on dispose d'un d�lai de 2 mois pour annuler un paiement Paybox apr�s les donn�es ne sont plus disponibles
		if ($this->_data['paiement_mode'] != 'PB') return false;
		return (time() < strtotime('+2 month', $this->_getTimestamp('paiement')));
	}
	/** renvoie le montant des frais d'annulation
	* @param int $preavis nombre d'heures avant le d�part
	* @return double frais d'annulation
	*/
	public function getCancellationFees($preavis = null)
	{
		if (is_null($preavis))
			$preavis = $this->getHoursBeforeStart('annulation');
		return $this->_getConditionsAnnulation()->getCancellationFees($preavis);
	}
	/** renvoie le montant des frais d'annulation pour l'agence
	* @param int $preavis nombre d'heures avant le d�part
	* @return double frais d'annulation
	*/
	public function getCancellationFeesSiege($preavis = null)
	{
		if (is_null($preavis))
			$preavis = $this->getHoursBeforeStart('annulation');
		return $this->_getConditionsAnnulation()->getCancellationFeesSiege($preavis);
	}
	// est ce en cours de paiement, pay� ou annul�e
	public function isInPayment()
	{
		return (strstr('APX', $this->getData('statut')) ? true : false);
	}
	// obtient la date de d�but format�e
	public function getDate($name, $format=null)
	{
		if (!$format) $format = 'd/m/Y H:i';
		return date($format, $this->_getTimestamp($name));
	}
	public function getLongDate($name)
	{
		global $JOURS, $MOIS;
		$t = $this->_getTimestamp($name);
		return $JOURS[date('N', $t)].' '.date('j', $t).' '.$MOIS[date('n', $t)];
	}
	public function getDebut($format = null) { return $this->getDate('debut', $format ? $format : 'd/m/Y \� H:i'); }
	public function getFin($format = null)   { return $this->getDate('fin',   $format ? $format : 'd/m/Y \� H:i'); }
	public function getFinAvecVerificationHoraires()
	{
		$t = $this->_getTimestamp('fin');
		$format = 'd/m/Y \� H:i';
		if ($h = $this->getAgence()->getHoraires(date('N', $t)))
		{
			$str = date('H:i', $t);
			if ($str >= $h['fermeture'] || ($h['pause_debut'] && $h['pause_debut']!=$h['pause_fin'] && $str >= $h['pause_debut'] && $str < $h['pause_fin']))
				$format = 'd/m/Y \a\v\a\n\t H:i';
		}
		return $this->getDate('fin', $format);
	}
	/**
	* J1 et J2 sont les bornes de d�but et de fin
	* transform�s en d�cimales sur un intervalle de 15 jours
	* pour effectuer la recherche dans les forfaits
	* Ils permettent de d�terminer le jour/heure de d�part et de retour autoris�s
	*
	*/
	public function getJ1() { return $this->_getJ('debut'); }
	public function getJ2()
	{
		$j2 = $this->_getJ('fin');
		// attention � l'intervalle
		if ($j2 < $this->getJ1())
			$j2 += 7;
		return $j2;
	}
	public function getDuree()
	{
		$duree = $this->getData('duree');
		$lbl = ($this->_data['type']=='ml' ? ' heure' : ' jour');
		if ($duree > 1) $lbl .= 's';
		return $duree.$lbl;
	}
	/**
	* Renvoie le nombre de kilom�trage de la r�servation
	*
	* @param bool $withKmSupp
	* @return string
	*/
	public function getKm($withKmSupp = false)
	{
		if (is_null($this->_data['km']))
			return 'km illimit�';
		else
		{
			$str = $this->_data['km'].' km';
			if ($withKmSupp) {
				$str.= $this->getKmSupp(' (%s� le km supp.)');
			}
			return $str;
		}
	}
	/**
	* Renvoie le kilom�trage suppl�mentaire pour la r�servation
	*
	* @param string $format	foramt pour sprintf()
	* @returns string
	*/
	public function getKmSupp($format = '%s') {
		$str = '';
		$kmSupp = ($this->hasForfait() ? $this->getForfait()->getData('km_sup') : $this->getCategorie()->loadZone($this->getAgence()->getData('zone'))->getData('km_sup'));
		if ($kmSupp > 0) {
			$str = sprintf($format, show_money($kmSupp));
		}
		return $str;
	}
	public function TTC2HT($prix, $t = null)
	{
		if (!$t && $this->hasData('paiement'))
			$t = strtotime($this->getData('paiement'));
		return round($prix/(1+Tva::getRate($t)), 2);
	}
	public function HT2TTC($prix, $t = null)
	{
		if (!$t && $this->hasData('paiement'))
			$t = strtotime($this->getData('paiement'));
		return round($prix*(1+Tva::getRate($t)), 2);
	}
	public function getPrixTotal() { return $this->_data['total']; }
	public function getPrixTotalHT()
	{
		// certaines options n'ont pas de TVA
		$courtage = $this->getResOptions()->getTotal('COURTAGE');
		return $this->TTC2HT($this->getPrixTotal() - $courtage) + $courtage;
	}
	public function getPrixSurcharge() { return ((double) $this->_data['surcharge']); }
	public function getPrixCouponHT()
	{
		if (!isset($this->_data['coupon_ht']))
		{
			$this->_data['coupon_ht'] = 0.0;
			if ($p = $this->getPartenaireCoupon())
				$this->_data['coupon_ht'] = (double) $p->getPrixHT();
		}
		return $this->_data['coupon_ht'];
	}
	public function getPrixCoupon() { return $this->HT2TTC($this->getPrixCouponHT()); }
	public function getPrixOptionsHT()
	{
		// les asurances sont sans TVA
		$courtage = $this->getResOptions()->getTotal('COURTAGE');
		return $this->TTC2HT($this->getPrixOptions() - $courtage) + $courtage;
	}
	public function getPrixOptions()
	{
		return $this->getResOptions()->getTotal();
		//return $this->getPrixTotal() - $this->getPrixForfait() - $this->getPrixSurcharge();
	}
	public function getPrixForfaitHT()
	{
		return $this->TTC2HT($this->getPrixForfait());
	}
	public function getPrixForfait()
	{
		return round($this->_data['tarif'] - (double) $this->_data['reduction'] - (double) $this->_data['nego'] + (double) $this->_data['yield'],2);
	}
	public function getPrixFraisDossier() { return $this->_data['fraisdossier']; }
	public function getPrixFraisDossierHT() { return $this->TTC2HT($this->getPrixFraisDossier()); }
	/**
	* Renvoie la variation en % du Yield
	* @returns ffoat
	*/
	public function getYieldVariation() {
		$var = 0;
		if ($this->_data['yield']) {
			$t_prix = $this->_data['tarif'] - $this->_data['reduction'];
			$y_prix = $t_prix + $this->_data['yield'];
			$var = round((($y_prix - $t_prix) * 100) / $t_prix, 2);
		}
		return $var;
	}

	// cr�er une version timestmap de la date
	protected function _getTimestamp($name)
	{
		if (!$this->_data['t_'.$name] && $this->_data[$name])
			$this->_data['t_'.$name] = strtotime($this->_data[$name]);
		return $this->_data['t_'.$name];
	}
	// transforme un temps en un chiffre d�cimal
	protected function _getJ($name)
	{
		if (!$this->_data['j_'.$name])
		{
			$t = $this->_getTimestamp($name);
			// heure au format num�rique hh,mm
			$n = date('H',$t) + (date('i', $t) / 60.0);
			// repr�sentation num�rique jj,hh
			$this->_data['j_'.$name] = date('N', $t) + $n / 100;
		}
		return $this->_data['j_'.$name];
	}
	public function getStatut()
	{
		switch($this->_data['statut'])
		{
			case 'C': return 'Cr�ation';
			case 'V': return 'Valid�e';
			case 'P': return 'Pay�e';
			case 'A': return 'Annul�e';
			case 'X': return 'En �chec';
		}
		return $this->_data['statut'];
	}
	/** la r�servation a-t-elel d�marr� ?*/
	public function isStarted() { return (time() > $this->_getTimestamp('debut')); }
	public function isFinished(){ return (time() > $this->_getTimestamp('fin')); }
	public function getKey() { return Page::getKeyCustomer($this->getId()); }
	public function getKeyParams()
	{
		return 'id='.$this->getId().'&key='.$this->getKey();
	}
	/** obtnier les liens de gestions */
	public function getLinks()
	{
		$links = array();
		// identifiant et statut
		$id = $this->_data['id'];
		$statut = $this->_data['statut'];
		$params = '?'.$this->getKeyParams();
		if (!$id || ($statut != 'A' && $statut != 'P'))
			return $links;
		// on cr�e les liens selon les conditions de la r�servation
		if ('P' == $statut)
		{
			$links['impression'] = $this->_getLink('client/impression.html', 'Confirmation', $params);
			if (SITE_MODE!='PROD' && $this->isFinished())
				$links['satisfaction'] = $this->_getLink(QuestionQuestionnaire::getURL($this->getId()), 'Satisfaction', null);
		}
		if (!$this->isLocapass()) // pas de facture et d'avoir pour les locapass
		{
			$links['facture_ada'] = $this->_getLink('client/facture.html', 'Facture', $params);
			if (($this->_data['rembourse'] > 0) && (('A'==$statut && $this->_data['paiement']) || ('P'==$statut && $this->_data['remboursement'])))
				$links['avoir_ada'] = $this->_getLink('client/avoir.html', 'Avoir', $params);
			if ($this->_data['facture_materiel'])
				$links['facture_materiel'] = $this->_getLink('client/facture_materiel.html', 'Facture mat�riel', $params);
			if ($this->_data['avoir_materiel'])
				$links['avoir_materiel'] = $this->_getLink('client/avoir_materiel.html', 'Avoir mat�riel', $params);
			if ($this->_data['facture_courtage'])
				$links['facture_courtage'] = $this->_getLink('client/facture_courtage.html', 'Facture assurances', $params);
			if ($this->_data['avoir_courtage'])
				$links['avoir_courtage'] = $this->_getLink('client/avoir_courtage.html', 'Avoir assurances', $params);
			if ($this->_data['facture_pro'])
				$links['facture_pro'] = $this->_getLink('client/facture_pro.html', "Facture frais d'adh�sion", $params);
		}
		// annuler la r�servation
		if ($this->_data['type']!='ml' && 'P'==$statut && !$this->isStarted() && !$this->_data['remboursement'])
		{
			// on ne veut pas pouvoir annuler pour les LOCAPASS vente priv�e
			if ($this->isLocapass())
				$coupon = $this->getPartenaireCoupon();
			if (!$coupon || !stristr($coupon['nom'], 'vente priv�e'))
				$links['annulation'] = $this->_getLink('client/annulation.html', 'Annulez votre r�servation', $params);
		}
		return $links;
	}
	protected function _getLink($page, $title, $params = null)
	{
		return array(
			'title' => $title,
			'href' => Page::getURL($page, null, USE_HTTPS).$params
		);
	}
	/**
	* Renvoie les conditions d'annulation en fonction de la r�servation
	* @returns Reservation_ConditionsAnnulation
	*/
	protected function _getConditionsAnnulation()
	{
		if (!$this->_conditionsAnnulation) {
			$this->_conditionsAnnulation = Reservation_ConditionsAnnulation::factory($this);
		}
		return $this->_conditionsAnnulation;
	}
	/**
	* Renvoie les explications sur les conditions d'annulations
	* @returns string
	*/
	public function getConditionsAnnulation()
	{
		return $this->_getConditionsAnnulation()->getConditions();
	}
	/**
	* Renvoie les informations sur les conditions d'annulation
	* @returns string
	*/
	public function getInfosAnnulation()
	{
		return $this->_getConditionsAnnulation()->getInfos();
	}

	/**
	* Renvoie l'agence associ�e
	* @returns Agence
	*/
	public function getAgence($cols = '*')
	{
		if (!$this->_agence)
		{
			// on montre le partenaire uniquement si la r�servation est pay�e ou annul�e ?
			$hidePartner = !$this->isInPayment();
			// on recherche parmi les diff�rentes possibilit�s
			foreach(array('alias','pdv','agence') as $k)
			{
				if (!$this->_data[$k]) continue;
				$x = Agence::factory($this->_data[$k], $hidePartner, $cols, ($this->isInPayment() ? null : Agence::STATUT_VISIBLE));
				if (!$x || $x->isEmpty()) continue;
				$this->_agence = $x;
				break;
			}
		}
		return $this->_agence;
	}
	/**
	* Renvoie un PartenaireCoupon
	* @returns PartenaireCoupon
	*/
	public function getPartenaireCoupon()
	{
		if (!$this->_coupon && $this->_data['coupon'])
			$this->_coupon = PartenaireCoupon::factory($this->_data['coupon']);
		return $this->_coupon;
	}
	/**
	* R�cup�rer le coupon de r�duction associ�e � un "code origine" pass� dans l'URL
	* @returns PartenaireCoupon
	*/
	public function getOrigineCoupon()
	{
		// s'il y a un code origine
		if ($co = preg_replace('/[^a-z0-9\.\-\_]/i', '', $_COOKIE['ADA001_ORIGINE']))
		{
			// on recherche s'il y a un coupon
			if ($code = getsql(sprintf("select coupon from origine where id='%s'", $co)))
			{
				$tis->_origineCoupon = PartenaireCoupon::factory($code, true);
			}
		}
		return $tis->_origineCoupon;
	}
	/**
	* Renvoie un code grillable associ�e � une r�servation si elle existe
	*
	* @returns PromotionCode
	*/
	public function getPromotionCode()
	{
		if (!$this->_promotion_code)
		{
			// il faut que la r�servation ait un identifiant pour faire cette v�rification
			if ($this->getId()) {
				$this->_promotion_code = PromotionCode::factory($this, false, $msg);
			}
			// pour ne pas refaire de requ�tes SQL par la suite s'il n'y avait pas de codes de promotions, on en cr�e un vide
			if (!$this->_promotion_code)
				$this->_promotion_code = new PromotionCode();
		}
		return $this->_promotion_code->isEmpty() ? null : $this->_promotion_code;
	}
	/**
	* Renvoie ResInfo associ�e � cette r�servation
	* @return ResInfo
	*/
	public function getResInfo()
	{
		if (!$this->_resInfo)
			$this->_resInfo = ResInfo::factory($this);
		return $this->_resInfo;
	}
	/**
	* Renvoie le d�tail d'une n�gociation
	* @rreturns ResNego
	*/
	public function getResNego() {
		return ResNego::factory($this);
	}
	/**
	* Renvoie un tableeau avec les infos du conducteur
	* @returns array
	*/
	public function getConducteur() {
		if (strstr('AP', $this->_data['statut'])) {
			$fields = array('email','titre','prenom','nom','naissance','tel','gsm');
			if ($this->isLocapass()) {
				if ($conducteur = $this->getResInfo()->getConducteur()) {
					return $conducteur->toArray($fields);
				}
			}
			return $this->getClient()->toArray($fields);
		}
		return array();
	}
	/**
	* renvoie la cat�gorie associ�e � la recherche
	* @return Categorie
	*/
	public function getCategorie($cols = '*')
	{
		if (!$this->_categorie && $this->_data['categorie'])
			$this->_categorie = Categorie::factory($this->_data['categorie'], $cols, $this->_data['forfait_type']=='JEUNES');
		return $this->_categorie;
	}
	/**
	* Renvoie le client assoic�e � une r�servation
	* @returns Client
	*/
	public function getClient()
	{
		if (!$this->_client && ($clientID = $this->getData('client_id')))
			$this->_client = Client::factory($clientID);
		return $this->_client;
	}
	/**
	* Retourne les otpions
	*
	* @returns ResOption_Collection
	*/
	public function getResOptions()
	{
		if (!$this->_options && $this->getId())
			$this->_options = ResOption_Collection::factory($this);
		return ($this->_options ? $this->_options : new ResOption_Collection());;
	}
	/** V�rifier si une option est pr�sente dans la r�servation
	*
	* @param string $id
	* @return bool
	*/
	public function hasOption($id)
	{
		if ($options = $this->getResOptions())
			return $options->hasOption($id);
		return false;
	}
	/**
	* Renvoie la liste de toutes les options
	*
	* @param Forfait $forfait le forfait permet de d�finir le nombre de kilom�tres
	* @param bool $en_avant : s�lectionne uniquement les options � mettre en avant
	* $param bool $refresh	remet � jour les options
	* @return Option_Collection
	*
	* $refresh est utilis�e pour afficher la liste des options, il y a un probl�me dans le cas suivant :
	* 	resultat	choix de l'ARF
	* 	inscription	modifier la r�servation
	* 	resultat	buildResOptions() a mis la GRF Pack � la place de la GRF Seule est tout est fauss�e
	*				cela est fait pour que addResOptions() puisse faire les bonnes op�rations quand l'ARF est ajout�e sur la page de paiement...
	*
	*/
	public function getAllOptions(Forfait $forfait = null, $en_avant = false, $refresh = false)
	{
		if (!$this->_allOptions || $refresh)
		{
			if (!$forfait)
				$forfait = new Forfait($this->toArray(array('categorie','km')));
			$forfait->setAgence($this->getAgence());
			$this->_allOptions = Option_Collection::factory($forfait, $this, $en_avant);
		}
		return $this->_allOptions;
	}
	/**
	* Ajoute des options � la liste courante
	*
	* @param array $args
	* @returns Reservation
	*/
	public function addResOptions($args = array())
	{
		/* il faut maintenant :
			1. enregistrer ces nouvelles options
			2. ajouter ces options � la liste courantes
			3. si l'ARF (afv[pu]) ets ajout�e et que la GRF Seule l'�tait d�j� et que la GRF Pack existe, modifier le montant de la GRF de la diff�rence
			4. modifier le total de la r�servation
		*/
		// 1. enregistrer les nouvelles options
		if ($addOptions = $this->buildResOptions(null, $args))
		{
			// 2. les ajouter � la liste courange puis les enregistrer
			$type = strtolower($this->getData('type'));
			/** @var ResOption_Collection : les options de la r�servation*/
			$options = $this->getResOptions();
			/** @var ResOption : l'option rachat de franchise */
			$rf = $options['rf'.$type];
			foreach($addOptions as $k => $o)
			{
				if ($options->hasOption($k))
					unset($addOptions[$k]);
				else if (($k == 'pf'.$type) && $rf)
				{
					// pour la GRF Pack, il faut modifier le montant de l'option courante
					$this->setData('total', round($this->getData('total') + $o['prix'] - $rf['prix'],  2));
					$rf->updatePrix($o['prix']);
					unset($addOptions[$k]);
				}
				else
				{
					$options->add($o);
				}
			}
			$addOptions->setReservation($this);
			$options->sortByPosition();
			// 3. modifier le total de la r�servation
			$this->setData('total', round($this->getData('total') + $addOptions->getTotal(), 2));
			$this->save('total');
		}
		return $this;
	}
	/**
	* Construit une liste des ResOptions � partir d'un $_POST
	* On ne le cr�e que si la r�servation n'a pas d�j� �t� cr��
	*
	* @param Forfait $forfait
	* @param array $args array('options' => array(), 'options_u' => array(), 'options_retrait')
	* @return ResOption_Collection
	*/
	public function buildResOptions(Forfait $forfait = null, $args = array())
	{
		if (!is_array($args['options']) && !is_array($args['options_u']))
			return null;
		$allOptions = $this->getAllOptions($forfait);
		$retrait = $args['options_retrait'];
		if (!preg_match('/^201[3-9]\-[0-1][1-9]-[0-3][0-9]$/', $retrait))
			$retrait = null;
		// les opttions avec tarification F, J et K
		$options = new ResOption_Collection();
		if (is_array($args['options']))
		{
			/*
			* Gestion de l'ARF (afv[pu]) et GRF Seule (rfv[pu] ou en Pack (pfv[pu]))
			* Si afv[pu] est s�lectionn�e :
			* 1. si rfv[pu] n'est pas d�j� s�lectionn� :
			* 	- on remplace dans les options rfv[pu] par pfv[pu] s'il existe
			* 	- on choisit automaituqmeent rfv[pu]
			* 2. sinon on ajoute pfv[pu] s'il existe et on laisse addResOptions() faire l'ajustement
			*/
			$type = strtolower($this->_data['type']);
			if (in_array('af'.$type, $args['options']))
			{
				// ARF n�cessite la GRF Pack si elle existe, sinon la GRF Seule
				$pf = $allOptions['pf'.$type];
				if (!$this->hasOption('rf'.$type))
				{
					// si la GRF Pack existe, elle doit remplacer la GRF Seule
					if ($pf)
					{
						$allOptions['rf'.$type] = $pf->setId('rf'.$type);
						unset($allOptions['pf'.$type]);
					}
					if (!in_array('rf'.$type, $args['options']))
						$args['options'][] = 'rf'.$type;
				}
				else if ($pf && !in_array('pf'.$type, $args['options']))
				{
					// si l'option GRF Seule est d�j� selectionn� on laisse addResOptions() faire la diff�rence
					$args['options'][] = 'pf'.$type;
				}
			}
			// parcourir les options
			foreach ($args['options'] as $k)
			{
				$option = $allOptions[$k];
				if (!$option) continue;
				$o = array('option' => $option['id']);
				if ($option['prix'] > 0)
					$o['prix'] = $option['prix'];
				if ($option['a_retirer'])
					$o['retrait'] = $retrait;
				foreach(array('paiement','facturation','position','nom','theme','groupe','theme_nom') as $v)
					$o[$v] = $option[$v];
				$options->add(new ResOption($o));
			}
		}
		// les opttions avec tarification U
		if (is_array($args['options_u']))
		{
			foreach ($args['options_u'] as $k => $qte)
			{
				$option = $allOptions[$k];
				$qte = (int) $qte;
				if (!$option || $qte < 1)
					continue;
				$o = array('option' => $option['id']);
				if ($option['paiement']=='L' && $option['prix'] > 0)
					$o['prix'] = round($option['prix'] * $qte, 2);
				$o['quantite'] = $qte;
				if ($option['a_retirer'])
					$o['retrait'] = $retrait;
				foreach(array('paiement','facturation','position','nom','theme','groupe','theme_nom') as $v)
					$o[$v] = $option[$v];
				$options->add(new ResOption($o));
			}
		}
		return $options->sortByPosition();
	}

	/**
	* Renvoie les facturations associ�es � la r�servation
	* @param bool $reload recharche les facturations
	* @returns Facturation_Collection
	*/
	public function getFacturations($reload = false)
	{
		if (!$this->_facturations || $reload)
			$this->_facturations = Facturation_Collection::factory($this);
		return $this->_facturations;
	}
	/**
	* Pour rappeler � l'agence les optiosn manquantes
	* @returns Option_Collection
	*/
	public function getMissingOptions($en_avant = false, $toArray = true)
	{
		// la liste des options de la r�servation
		$ids = $this->getResOptions()->getIds();
		$type = strtolower($this->getData('type'));
		// faire la liste des options manuantes
		$allOptions = $this->getAllOptions(null, $en_avant)->fixFranchise($this, true);
		$options = new Option_Collection($allOptions);
		foreach ($allOptions as $o)
		{
			if (is_null($o['en_avant'])) break;
			if ($o['paiement']=='L' && !in_array($o['id'], $ids))
				$options[$o['id']] = $o;
		}
		if ($toArray)
		{
			$x = array();
			foreach($options as $o)
				$x[$o['id']] = '- '.$o['nom'];
			return $x;
		}
		return $options;
	}
	/**
	* Renvoie le montant de la franchise en fonction de la cat�gorie et des options
	* @returns int
	*/
	public function getMontantFranchise() {
		$categorie = $this->getCategorie();
		$type = strtolower($categorie['type']);
		if ($type!='vp' && $type!='vu') {
			return null;
		}
		if ($franchise = $categorie['franchise']) {
			foreach(array('af','rf') as $k) {
				if ($this->hasOption($k.$type)) {
					$franchise = $categorie['franchise_'.$k];
					break;
				}
			}
		}
		return $franchise;
	}
	/**
	* Associe un client � une r�servation, renvoie false si d�j� associ�e � un autre client
	*
	* @param Client $client
	* @param bool	$setClient indique le client sin non renseign�
	* @returns bool
	*/
	public function checkClient(Client $client, $setClient = false)
	{
		$clientID = $this->getData('client_id');
		// mettre � jour la r�servation
		if (!$clientID && $setClient)
		{
			$clientID = $client->getId();
			$this->setData('client_id', $clientID)
				 ->setData('client_email', $client->getData('email'))
				 ->setData('statut', 'V')
				 ->setData('validation', date('Y-m-d H:i:s'));
			$this->save('client_id,client_email,statut,validation');
		}
		if ($clientID != $client->getId())
			return false;
		// r�cup�rer le client au niveau de la r�servation si n�cessiare
		if (!$this->_client)
			$this->_client = $client;
		return true;
	}
	/**
	* V�rifier si le client respecte l'�ge d�clar� dans la r�servation et poru la cat�gorie choisie
	*
	*/
	public function checkClientAge()
	{
		// si on conna�t le client
		if ($client = $this->getClient())
		{
			if ($clientAge = $client->getAge(substr($this->_data['validation'], 0, 10)))
			{
				// r�cup�rer les �ges, les trier et mettre une borne sup�rieure
				global $AGE; // cf declaration de $AGE dans constant.php
				$ages = array_keys($AGE);
				sort($ages);
				$ages[] = 120;
				// 2015-06-15 : l'�ge par d�faut est 25 ans
				if ($this->getId() && $this->_data['age']==25 && $clientAge < 25)
				{
					foreach($ages as $k => $v)
					{
						if ($k > 0 && $clientAge < $v)
						{
							$this->setData('age', $ages[$k-1])->save('age');
							break;
						}
					}
				}

				// on doit avoir $ages = [18, 21, 23, 25, 120]
				$age = max(18, $this->_data['age']);
				foreach($ages as $k => $v)
				{
					if ($age < $v)
					{
						// l'�ge du client diot �tre entre cette �ge et l'�ge inf�rieure
						if ($clientAge >= $ages[$k-1] && $clientAge < $ages[$k]) {
							// et l'�ge doit �tre coh�rent avec la cat�gorie demand�e
							$categorie = $this->getCategorie()->loadZone($this->getAgence()->getData('zone'));
							return ($clientAge >= $categorie['age']);
						}
					}
				}
			}
		}
		return true;
	}

}
?>
