<?php
/**
* E-Commerce
* 	https://developers.google.com/analytics/devguides/collection/gajs/gaTrackingEcommerce?hl=fr
* 	https://developers.google.com/analytics/devguides/collection/gajs/methods/gaJSApiEcommerce?hl=fr#_gat.GA_Tracker_._addTrans
* Custom Varaibles
* 	http://cutroni.com/blog/2011/05/18/mastering-google-analytics-custom-variables/
* Custom Variables and e-commerce
* 	http://cutroni.com/blog/2011/06/14/5-google-analytics-custom-variables-for-ecommerce/
* Universal Analytics
* 	Migrating
* 		 http://misterphilip.com/universal-analytics/migration/config
* 	Documenation
* 		https://developers.google.com/analytics/devguides/collection/analyticsjs/
*		https://developers.google.com/analytics/devguides/collection/analyticsjs/cookies-user-id#user_id
*		https://developers.google.com/analytics/devguides/collection/analyticsjs/ecommerce
* 		https://developers.google.com/analytics/devguides/collection/analyticsjs/enhanced-ecommerce
*/
class Tracking_GoogleAnalytics
{

	static public function doTracking(Page $page)
	{
		//Google Analytics
		if (defined('GA_ACCOUNT') && GA_ACCOUNT) :
?>
		<script type="text/javascript">
		// <![CDATA[
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/<?= SITE_MODE == 'PROD' ? 'analytics' : 'analytics_debug' ?>.js','ga');

		<? if ($client = $page->getConnectedClient()) : ?>
		ga('create', '<?= GA_ACCOUNT ?>', 'ada.fr', {'userId': '<?=$client->getId()?>'});
		<? else : ?>
		ga('create', '<?= GA_ACCOUNT ?>', 'ada.fr');
		<? endif; ?>
<? foreach($page->getGATracking() as $variables) : ?>
		ga(<?=trim(json_encode($variables), '[]');?>);
<?php endforeach; ?>
		ga('send', 'pageview');
		// ]]>
		</script>
<?		endif;
		//!GoogleAnalytics
	}

	/**
	* Renoive le tracking pour Google Analytics
	*
	* @param Page $page
	* @param Reservation $reservation
	*/
	static public function reservationValidation(Page $page, Reservation $reservation)
	{
		// r�cup�rer les varaibles principales
		$agence = $reservation->getAgence();
		$dept_nom = '';
		if ($dept = $agence->getDept()) {
			$dept_nom = filter_chars(sprintf('%s %s', $dept['id'], $dept['nom']));
		}
		$categorie = $reservation->getCategorie();
		
		// cr�er la transaction
		$page->addGATracking('require', 'ecommerce');
		$page->addGATracking
		(
			'ecommerce:addTransaction', 
			array(
				'id'	=> $reservation['id'],				// transaction ID - required
				'affiliation' => filter_chars(sprintf('%s %s', $agence['id'], $agence['nom'])),	// affiliation or store name
				'revenue' => $reservation->getPrixTotal(), 	// total - required; Shown as "Revenue" in the Transactions report. Does not include Tax and Shipping.
				'tax'	=> '0.00',		// tax
				'shipping' => '0.00'	// shipping
			)
		);
		// ...puis on pr�cise le forfait...
		$type = strtoupper($categorie['type']);
		$mnem = $categorie['mnem'];
		if ($mnem == 'D+') $mnem = "D'";
		$sku = sprintf('%s/%s', $type, $mnem);
		$page->addGATracking
		(
			'ecommerce:addItem', 
			array(
				'id' =>  $reservation['id'], 	// identifiant de la transaction
				'sku' => $sku,		// SKU/code - required
				'name' => $categorie['canon'], // product name - necessary to associate revenue with product
				'category' => 'forfait', 			// category or variation
				'price' => $reservation->getPrixForfait(),// unit price - required
				'quantity' => '1'					// quantity - required
			)
		);

		// ...puis les diff�rentes options...
		foreach($reservation->getResOptions() as $optionID => $option) {
			$qty = max(1, $option['quantite']);
			$page->addGATracking(
				'ecommerce:addItem', 
				array(
					'id' => $reservation['id'],			// identifiant de la transaction
					'sku' => $optionID, 					// SKU/code - required
					'name' => filter_chars($option['nom']), // product name - necessary to associate revenue with product
					'category' => filter_chars($option['theme']), // category or variation
					'price' => round($option['prix'] / $qty, 2),			// unit price - required
					'quantity' => $qty	// quantity - required
				)
			);
		}

		// ... on finit par quelques variables personnalis�es...
		$page->addGATracking('set', 'TypeVehicule', $type);
		$page->addGATracking('set', 'Duree', $reservation['duree']);
		$page->addGATracking('set', 'JoursAvant', ceil($reservation->getHoursBeforeStart() / 24));
		$page->addGATracking('set', 'Ville', filter_chars($agence['ville']));
		$page->addGATracking('set', 'Departement', $dept_nom);
		$page->addGATracking('set', 'Zone', strtoupper($agence['zone']));

		$forfait = mysql_fetch_assoc(sqlexec(sprintf("select * from forfait2 where id='%s'", $reservation['forfait'])));
		if ($forfait && isset($forfait['prefix'])) {
			$page->addGATracking('set', 'Forfait', $forfait['prefix']);
		}
		// envoyer les donnn�es
		$page->addGATracking('ecommerce:send');
	}
}

