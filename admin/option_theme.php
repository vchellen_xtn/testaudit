<?
	require_once("secure.php"); 
	require_once("lib.php");

	// identifiant
	$id = getarg("id");
	
	// suppression
	if ($id && getarg("delete")) 
	{
		$sql = "delete from option_theme where id = '".addslashes($id)."'";
		getsql($sql);
		unset($id);
	}
	
	// modification, cr�ation
	if (getarg("update") || getarg("create"))
		save_record('option_theme', $_POST);
	if ($id)
	{
		$rs = sqlexec("select * from option_theme where id = '".addslashes($id)."' ");
		if (!$rs || !($row = mysql_fetch_assoc($rs)))
			die("Aucun enregistrement correspondant.");
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script language="javascript" src="../scripts/script.js"></script>
	<script language="javascript" src="admin.js"></script>
	<script>
	<!-- //

	// doDelete
	function doDelete(frm)
	{
		return (confirm("Voulez-vous r�ellement supprimer cet �l�ment ?\nCette op�ration est irr�versible."));
	}
	
	// doSubmit
	function doSubmit(frm)
	{
		var err = "";
		if(!frm.id.value.length)
			err+= "Veuillez indiquer un Id\n";
		if(!frm.nom.value.length)
			err+= "Veuillez indiquer le nom\n";
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		return true;
	}
	// -->
	</script>
</head>
<body>
<form action="" method="post">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre">
		<img src="img/puce.gif" align="absmiddle" hspace="1">
		Th�me Option
	</th>
</tr>
<tr>
	<td>
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="15%">
		<col width="35%">
		<col width="15%">
		<col width="35%">
		<tr>
			<td><label for="id">ID</label></td>
			<td>
				<?=$id?>
				<input type="<?=($id ? 'hidden' : 'text')?>" maxlength="16" name="id" id="id" value="<?=$id?>"/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td><label for="page">Page</label></td>
			<td>
			<?
				$a = array('resultat'=>'resultat','options'=>'options','paiement' => 'paiement');
				selectarray('page', $a, $row['page'], null, null);
			?>
			</td>
			<td><label for="ouvert">Ouvert</label></td>
			<td>
			<?
				$a = array('non','oui');
				selectarray('ouvert', $a, $row['ouvert'], null, null);
			?>
			</td>
		</tr>
		<tr>
			<td><label for="nom">Nom</label></td>
			<td colspan="3"><input type="text" id="nom" name="nom" value="<?=$row["nom"]?>" maxlength="64" size="64"></td>
		</tr>
		<tr>
			<td><label for="popin">Popin</label></td>
			<td>
			<?
				$a = array();
				foreach(glob(BP.'/skin/popin/popin/*.htm') as $f)
				{
					$f = substr(basename($f), 0, -4);
					if ($f == 'index') continue;
					$a[$f] = $f;
				}
				selectarray('popin', $a, $row['popin'], '--');
			?>
			</td>
			<td colspan="2"></td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%">
		<tr>
			<td align="left">
			<? if ($id) :  ?>
				<input type="button" onclick="return go_frame(parent.list, 'lists/option.php?theme=<?=$id?>');" value="options"/>
			<? endif; ?>
			</td>
			<td align="right">
				<input type="submit" name="<?=$id ? "update" : "create"?>" value="enregistrer" onclick="return doSubmit(this.form);">
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
</form>
</body>
</html>
