<?php
class PayboxSecure extends Ada_Object
{
	const STATUT_3DS_OK = "Autorisation � faire";

	public function _construct()
	{
		parent::_construct();
		$this->setIdFieldName('IdSession');
	}
	
	/**
	* Retourne un enregistrement si le statut est correct et a moins de 5 minutes
	* 
	*/
	public function isOK()
	{
		if ($this->_data['StatusPBX'] == self::STATUT_3DS_OK)
		{
			// U: Erreur lors de la demande � la banque
			if ($this->_data['3DENROLLED']!='U') {
				// il y a un timeout 3D Secure
				if (strtotime($this->_data['creation']) > strtotime('-5 minutes')) {
					// si l'ID3d a �t� utilis�e, il faut en redemander un
					$used = getsql(sprintf("SELECT COUNT(*) FROM paybox WHERE reservation=%ld AND ID3D='%s'", $this->_data['reservation'], $this->_data['ID3D']));
					if (!$used) {
						return true;
					}
				}
			}
		}
		return false;
	}
}
?>
