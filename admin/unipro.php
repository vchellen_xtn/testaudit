<?
// si la page est appel�e directement il faut inclure les librairies
if (basename($_SERVER['SCRIPT_NAME']) == basename(__file__))
{
	include_once dirname(__file__).'/../constant.php';
}

// si la page est appel�e directement, on corrige les r�servations en cours non abouties
if ((SITE_MODE!='PROD' || in_array($_SERVER['REMOTE_ADDR'],array(IP_RND, IP_ADA))) && basename($_SERVER['SCRIPT_NAME']) == basename(__file__))
{
	set_time_limit(0);
	$unipro = new Unipro();
	if ($_GET['agence'])
	{
		$agences = $unipro->getAgences($_GET['agence']);
		$unipro->debug ($agences);
	}
	else if ($_GET['jauge'])
	{
		$jauges = $unipro->getJauges($_GET['jauge']);
		$unipro->debug($jauges);
	}
	else if ($_GET['horaire'])
	{
		$horaires = $unipro->getHoraires();
		$unipro->debug($horaires);
	}
	else if ($_GET['fermeture'])
	{
		$fermetures = $unipro->getFermetures($_GET['fermeture']);
		$unipro->debug($fermetures);
	}
	else if (isset($_GET['tarifs']))
	{
		$agence = 'FRA71A'; // // 'FR390A'; // 'FR513A';
		$depart = date('Y-m-d 09:00', strtotime('+7 days'));
		$retour = date('Y-m-d 17:00', strtotime('+9 days'));
		$km = 150;
		echo "Agence: $agence<br/>D�part: $depart<br/>Retour: $retour<br/>Km: $km<br/>\n";
		foreach(array('VP/NAT','VP/LOC','VU/NAT','VU/LOC','VU/VUHS') as $k) {
			list($type, $tarification) = explode('/', $k);
			echo' <h2>'.$type.' / '.$tarification.'</h2>'."\n";
			echo date('H:i:s').'<br/>';
			$tarifs = $unipro->getTarifs($agence, $type, '', $depart, $retour, $km, $tarification);
			echo date('H:i:s').'<br/>';
			$unipro->debug($tarifs);
		}
	}
	else if (isset($_GET['dispos']))
	{
		$agence = 'FRA71A'; // // 'FR390A'; // 'FR513A';
		$depart = date('Y-m-d 09:00', strtotime('+7 days'));
		$retour = date('Y-m-d 17:00', strtotime('+9 days'));
		echo "Agence: $agence<br/>D�part: $depart<br/>Retour: $retour<br/>\n";
		foreach(array('VP','VU') as $type) {
			echo' <h2>'.$type.'</h2>'."\n";
			echo date('H:i:s').'<br/>';
			list($dispos, $parcs) = $unipro->getDispos($agence, $type, '', $depart, $retour);
			echo date('H:i:s').'<br/>';
			$unipro->debug($dispos);
			$unipro->debug($parcs);
		}
	}
	else if ($_GET['searchuser'])
	{
		echo $_GET['idagence']." - ".$_GET['searchuser']." - ".$_GET['nom'];
		$check = $unipro->searchuser($_GET['idagence'],$_GET['searchuser'],$_GET['prenom'],$_GET['ville'],$_GET['telephone']);
		$unipro->debug($check);
	}
	else
	{
	?>
	<ul>
		<li><a href="?agence=TOUTES">liste des agences</a></li>
		<li><a href="?horaire=1">liste des horaires</a></li>
		<li><a href="?jauge=TOUTES">liste des jauges</a></li>
		<li><a href="?fermeture=TOUTES">liste des fermetures</a></li>
		<li><a href="?tarifs">tarifs</a></li>
		<li><a href="?dispos">dispos</a></li>
	</ul>
	<?
	}
	$unipro = null;
}
?>
