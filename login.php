<?
	session_start();
	include 'constant.php';

	// respond to CORS preflights
	// return only the headers and not the content
	// only allow CORS if we meet requirements
/* 2015-01-12 : ne fonctionne pas e pr�-production la m�thode OPTIONS ne semble pas �tre autoris�e
	if (preg_match('/^https?:\/\/(pprod\.ada\.lbn\.fr|www\.ada\.fr)$/', $_SERVER['HTTP_ORIGIN']))
	{
		header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
		header('Access-Control-Allow-Origin: '.$_SERVER['HTTP_ORIGIN']);
		header('Access-Control-Allow-Headers: X-Requested-With');
		header('Access-Control-Max-Age: 3600');
		header('Access-Control-Allow-Credentials: true');
	}
	if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
		exit();
	}
*/
	// identifiant de la r�servation
	$id = (int) $_REQUEST['id'];
	if (!Page::checkKey((string)$id, 'key', null))
		$err = 'Op�ration non autoris�e';
	else if ($_REQUEST['action']!='facebook' && !filter_var($_REQUEST['email'], FILTER_VALIDATE_EMAIL))
	{
		$err = "Cette adresse e-mail n'est pas valide : ".htmlspecialchars(strip_tags($_REQUEST['email']));
		$field = 'email';
	}
	else
	{
		// si connexion depuis Facebook,on cherche � r�cup�rer l'enregistrement
		if ($_REQUEST['action']=='facebook')
		{
			if ($clientFB = ClientFacebook::createFromSignedRequest($_COOKIE['fbsr_'.FB_APP_ID]))
				$client = $clientFB->getClient();
		}
		else
		{
			// on r�cup�re l'e-mail de l'utilisateur et on le cherche dans la base
			$email = filter_var($_REQUEST['email'], FILTER_SANITIZE_EMAIL);
			$client = Client::factory($email);
		}
		// puis selon le type de formulaire
		if ($_REQUEST['action'] == 'facebook')
		{
			if (!$clientFB)
				$err = "Vous devez �tre connect� � Facebook et avoir accept� de partager votre e-mail avec ADA !";
			else if (!$client)
				$err = "Nous n'avons pas pu rapprocher votre e-mail Facebook ".$clientFB['facebook_email']." d'un compte ADA";
			else
				$email = $client['email'];
		}
		else if ($_REQUEST['action'] == 'login')
		{
			if (!$client)
			{
				$err = "Cette adresse e-mail ".$email." est inconnue. Merci de la corriger ou de vous inscrire !";
				$field = 'email';
			}
			else if (!$_COOKIE['ADA001_TC_MODE'] && !$client->checkPassword(iconv('UTF-8', 'WINDOWS-1252//TRANSLIT', $_REQUEST['pwd'])))
			{
				$err = "Le mot de passe n'est pas correct. Vous pouvez r�-essayer ou r�-inialiser votre mot de passe.";
				$field = 'pwd';
			}
		}
		else
		{
			if ($client)
			{
				$err = "Vous avez d�j� r�serv� sur ADA.fr avec l'e-mail ".$email;
				$field = 'email';
			}
			else if (!Client::create($_REQUEST, $errors, true))
			{
				$err = join('<br/>', $errors);
			}
			else if ($_COOKIE['fbsr_'.FB_APP_ID])
			{
				// si on a cr�� le client et qu'il y a une connexion facebook on cr�e l'association maintenant
				$clientFB = ClientFacebook::createFromSignedRequest($_COOKIE['fbsr_'.FB_APP_ID]);
			}
		}
	}
	
	header('Content-type: text/xml;charset=utf-8');
	if (!$err && $email)
	{
		// authentification
		Page::setKeyCustomer($email);
		// on v�rifie le prgramme kilom�tre
		if (!$client['pgkm'])
			PgKm::checkEmail($email, true);
		// envoyer de l'information sur le programme kilom'�tre si c'est demand�
		if ($_REQUEST['kilom_etre'])
			PgKm::sendMail($email);
	}

	echo '<statut>'."\n";
	echo '<key>'.Page::getKeyCustomer($id).'</key>'."\n";
	echo '<email>'.$email.'</email>'."\n";
	if ($client) {
		echo '<prenom>'.utf8_encode($client['prenom']).'</prenom>'."\n";
		echo '<nom>'.utf8_encode($client['nom']).'</nom>'."\n";
	}
	if ($err)
	{
		echo '<error';
		if ($field) echo ' field="'.$field.'"';
		echo '><![CDATA['.utf8_encode($err).']]></error>'."\n";
	}
	echo '</statut>'."\n";
?>
