<?
/**
* Conditions d'annulation pour remboursement jusqu'avant le d�part  (2015-10-20) 
	- annulation possible 48h avant sans remboursement
	- il y a toujours des frais de dossier
*/
class Reservation_ConditionsAnnulation04 extends Reservation_ConditionsAnnulation
{
	const FRAIS_ANNULATION = 27;
	const FRAIS_ANNULATION_SIEGE = 20;
	
	public function isCancellable($preavis, &$msg)
	{
		if ($preavis <= 1) {
			$msg = "L'annulation doit �tre faite  avant votre d�part.";
			$msg .= '<br /><span class="rouge">Vous ne pouvez plus annuler cette r�servation.</span>';
			return false;
		}
		if ($preavis <= 48) {
			$msg = "Votre annulation intervient moins de 48h avant votre d�part.";
			$msg .= '<br /><strong class="rouge">Le montant de votre r�servation ne vous sera pas rembours�.</strong>';
			return true;
		}
		// on d�tailles les conditions d'annulation selon le d�lai
		$msg = "Votre annulation intervient plus de 48 heures avant votre d�part.";
		$msg.='<br/>Le montant de votre r�servation vous est rembours�, avec une <strong class="rouge">d�duction de %ld � TTC de frais de dossier.</strong>';
		$msg = sprintf($msg, $this->getCancellationFees($preavis));
		return true;
	}
	/**
	* Renvoie si la r�servation peut �tre rembours�e
	* 
	* @param int $preavis nombre d'heures avant le d�part
	* @returns bool
	*/
	public function isRefundable($preavis) {
		if ($preavis <= 48 ) {
			return false;
		}
		return true;
	}
	public function getCancellationFees($preavis)
	{
		return self::FRAIS_ANNULATION;
	}
	public function getCancellationFeesSiege($preavis)
	{
		// la part agence des frais de dossier
		return self::FRAIS_ANNULATION_SIEGE;
	}
	
	public function getConditions()
	{
		$str = <<<EOT
		<p style="margin-bottom:10px;">
			L'annulation doit �tre faite au moins 48 heures avant le jour de votre d�part. A moins de 48h, le montant de votre r�servation ne vous sera pas rembours�.<br />
			<br />
			Si votre annulation intervient :
		</p>
		<ul class="tiret">
			<li><span class="rouge gras">plus de 48h avant votre d�part</span> : le montant de votre r�servation vous est rembours�, avec une d�duction de {$this->getCancellationFees(72)} � TTC de frais de dossier 
			<li><span class="rouge gras">moins de 48h avant votre d�part</span> : le montant de votre r�servation ne vous sera pas rembours�</li>
			<li><span class="rouge gras">2 mois apr�s votre r�glement initial (et avant votre d�part)</span> : Cette r�servation ne peut pas �tre annul�e en ligne. Pour demander un remboursement, merci d'adresser un email � <a href="mailto:reservation@ada.fr">reservation@ada.fr</a> en pr�cisant le n� de la r�servation concern�e. Nous vous confirmerons l'annulation dans les plus brefs d�lais.</li>
		</ul>
		<p style="margin-bottom:10px;">
			<strong>Il n'y a pas lieu � remboursement :</strong>
		</p>
		<ul class="tiret">
			<li>Pour toute location plus courte que la dur�e pr�vue dans la r�servation</li>
			<li>Si le Locataire ne se pr�sente pas pour la prise du v�hicule</li>
			<li>En cas de retard dans la prise du v�hicule</li>
			<li>En cas d'annulation faite moins de 48h avant votre d�part</li>
			<li>Dans le cas d'une location � l'heure (location type ADA Malin)</li>
			<li>Dans le cas o� le client est dans l'impossibilit� de pr�senter la carte bancaire ayant servi � effectuer le paiement en ligne</li>
			<li>Dans le cas o� le client est dans l'impossibilit� d'effectuer le d�p�t de garantie</li>
			<li>&Agrave; toutes personnes ne pr�sentant pas sa carte d'�tudiant en court de validit� et pour un jeune actif un justificatif de domicile dans le cadre d'Ada Jeunes</li>
		</ul>
		<p style="margin-bottom:10px;">
			<strong>Remboursement du mat�riel achet� en ligne et droit de r�tractation :</strong>
			<br/><br/>Le Client dispose d'un droit de r�tractation jusqu'� la date de retrait des Biens indiqu�e lors de la commande, par cons�quent :
		</p>
		<ul class="tiret">
			<li>si le client souhaite annuler ses achats avant la date de demande de retrait indiqu� lors de ses achats, le client pourra prendre contact avec le service client � l'adresse <a href="mailto:reservation@ada.fr">reservation@ada.fr</a> pour demander le remboursement du mat�riel achet� sans frais</li>
			<li>si le client refuse le mat�riel remis en agence, l'agence informera reservation@ada.fr pour informer de la d�cision du client et demander le remboursement</li>
			<li>apr�s la date de retrait, aucun remboursement ne pourra �tre effectu� sans que la demande ne soit faite par l'agence</li>
			<li>si le client souhaite annuler sa commande apr�s le retrait du mat�riel en agence, aucun remboursement du mat�riel ne sera effectu�</li>
		</ul>
EOT;
		return $str;
	}
	public function getInfos()
	{
		$str = <<<EINFO
		Vous pouvez annuler votre r�servation jusqu'� 48h avant la prise du v�hicule.<br />
		Des frais de dossier de {$this->getCancellationFees(72)} euros TTC vous seront factur�s pour toute annulation au-del� de 48h avant votre d�part. Moins de 48h avant votre d�part : le montant de votre r�servation ne vous sera pas rembours�.<br/>
EINFO;
		return $str;
	}
}
?>
