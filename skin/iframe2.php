<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<?
	$this->writeHead();
?>
	<style type="text/css" media="print">
		img { border:0px; }
	</style>
	<style type="text/css" media="screen">
		body {
			margin: 0;
			padding: 0;
			font: 12px/1.5 verdana, arial, helvetica, sans-serif;
			background:white;
		}
	</style>
</head>
<body>
<?
	include ($this->getPagePath());
	$this->writeJSStats();
?>
</body>
</html>
