<?
require_once "secure.php";

set_time_limit(0);
// remettre � NULL
sqlexec("UPDATE agence_pdv SET latitude=NULL, longitude=NULL WHERE  latitude=0 AND longitude=0");

// s�lectionner les agences � mettre � jour
$sql = "SELECT ap.id, ap.adresse1, ap.adresse2, ap.cp, ap.ville, z.nom zone_nom FROM agence_pdv ap JOIN zone z ON z.id=ap.zone WHERE (ap.longitude+ap.latitude is null) and statut&1=1";
$rs = sqlexec($sql);
?>
<html>
<head>
	<title>Mise � jour des coordonn�es des agences</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
</head>
<body>
<table border="1" cellpadding="2" cellspacing="0">
<caption>Mise � jour des coordonn�es</caption>
<tr><th>Nb</th><th>agence</th><th>adresse</th><th>longitude</th><th>latitude</th></tr>
<?
$count = 0;
while ($row = mysql_fetch_assoc($rs)) {
	$a = array();
	// on enl�ve le CEDEX s'il existe
	$ville = preg_replace('/\scedex\s*$/i','', $row['ville']);
	if ($row['cp']=='...') 
		$row['cp'] = '';
	// l'adrese de base
	$a[] = $row['adresse1'].' '.$row['adresse2'].' '.$row['cp'].' '.$ville.' '.$row['zone_nom'];
	// on peut essayer d'autres adresses
	if ($row['adresse2'])
	{
		$a[] = $row['adresse1'].' '.$row['cp'].' '.$ville.' '.$row['zone_nom'];
		$a[] = $row['adresse2'].' '.$row['cp'].' '.$ville.' '.$row['zone_nom'];
	}
	// puis sans le code postal
	$a[] = $row['adresse1'].' '.$row['adresse2'].' '.$ville.' '.$row['zone_nom'];
	if ($row['adresse2'])
	{
		$a[] = $row['adresse1'].' '.$ville.' '.$row['zone_nom'];
		$a[] = $row['adresse2'].' '.$ville.' '.$row['zone_nom'];
	}
	// on essaye pour chaque adresse
	echo '<tr><td>'.(++$count).'</td><td><a href="main.php?rows=460&url='.urlencode('agence_pdv.php?id='.$row['id']).'">'.$row['id'].'</a></td>';
	echo '<td>';
	$saved = false;
	foreach ($a as $adresse)
	{
		$geometry = "KO"; $lat = null; $lng = null;
		if ($x = Geocoding::factory($adresse))
		{
			if ($x->isOK())
			{
				$geometry = $x->getLocation($lat, $lng);
				if (!$saved && in_array($geometry, array('ROOFTOP')))
				{
					$row['latitude'] = deg2rad($lat);
					$row['longitude'] = deg2rad($lng);
					save_record('agence_pdv', $row);
					$saved = true;
					$geometry = '['.$geometry.']';
				}
			}
		}
		echo sprintf("%s : %s (%f, %f)<br/>", $geometry, $adresse, $lat, $lng);
	}
	echo '</td>';
	echo '<td>'.rad2deg($row['longitude']).'</td><td>'.rad2deg($row['latitude']).'</td>';
	echo "</tr>\n";
}
?>
</table>
</body>
</html>