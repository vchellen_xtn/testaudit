<?
	/** @var Forfait */
	$forfait = $this;
	$categorie = $forfait->getCategorie();
?>
	<div class="col-wrapper">
		<div class="col-1-4 left">
			<img src="../skin/jeunes/jeunes/img/picto-exclu-web.png" alt="exclusivit� web" />
		</div>
		<div class="col-1-2 left">
			<h3><?=$categorie['nom']?></h3>
		</div>
		<div class="col-1-4 left">
			<span class="promo_prix">
				<span id="forfait_t_prix"><?=$forfait->getPrix();?>�</span>
				<? if ($forfait->hasPromo()) : ?>
					<span id="forfait_prix"><?=($forfait->hasPromo() ? 'au lieu de <span>'.$forfait->getPrixBarre() .'</span>' : '')?>�</span><br/>
				<? endif; ?>
				<span id="forfait_kmsup"><? if ($kmSupp = $forfait->getPrixKmSupp()) echo '<span>'.show_money($kmSupp).'�</span> le km supp.';?></span> 
				<a href="../popup.php?page=reservation/details&agence=<?=$forfait->getAgence()->getId()?>&km_sup=<?=$kmSupp?>" onclick="return wOpen(this.href, 400, 470)">D�tails</a>
			</span>
		</div>
		<div class="clear"></div>
	</div>
	
	<br/>
	
	<p class="promo_infos">
		<?=str_replace(',<br/>',', ', $forfait->getPromoDescription());?>
	</p>
	
	<div class="col-wrapper">
		<div class="col-2-3 left">
			<img src="../<?=$categorie->getImg('big');?>" id="forfait_categorie_img" alt="" />
		</div>
		<div class="col-1-3 left ">
			<div class="duree-km">
				<p>Dur�e<span><?=$forfait->getDuree();?></span></p>
				<p>KM inclus<span><?=$forfait->getKm()?></span></p>
			</div>
			<p class="type-vehicule">Type : <strong><?=$categorie['description']?></strong></p>
		</div>
		<div class="clear"></div>
	</div>
	
	<ul class="specs">
		<?=$categorie->getSpecifications('li');?>
	</ul>
	<div class="clear"></div>
