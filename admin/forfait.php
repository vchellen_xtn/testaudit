<?
	require_once("secure.php"); 
	require_once("lib.php");

	// identifiant
	$id = getarg("id");
	
	// suppression
	if ($id && getarg("delete")) 
	{
		$sql = "delete from forfait2 where id = '".addslashes($id)."'";
		getsql($sql);
		$sql = "delete from forfait2_tarif where forfait = '".addslashes($id)."'";
		getsql($sql);
		unset($id);
	}
	
	// modification, cr�ation
	if (getarg("update") || getarg("create"))
	{
		if ($_POST['illimite']) $_POST['km'] = '';
		if ($_POST["depart_fin"] < $_POST["depart_debut"])
			$_POST["depart_fin"] += 7;
		if ($_POST["retour_fin"] < $_POST["retour_debut"])
			$_POST["retour_fin"] += 7;
		save_record("forfait2", $_POST);
	}
	
	if ($id)
	{
		$rs = sqlexec('select * from forfait2 where id = "'.addslashes($id).'"');
		if (!$rs || !($row = mysql_fetch_assoc($rs)))
			die("Aucun enregistrement correspondant ($id).");
	}
	$unites = (strtolower($row['type'])=='ml' ? 'heures' : 'jours');

	for ($h=0; $h<=23; $h++)
		$HOURS[$h] = sprintf("%02d:00", $h);
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script>
	<!-- //

	// doDelete
	function doDelete(frm)
	{
		return (confirm("Voulez-vous r�ellement supprimer cet �l�ment ?\nCette op�ration est irr�versible."));
	}
	
	// date_from_control
	function date_from_control(ctrl_j, ctrl_h)
	{
		var j = ctrl_j.selectedIndex >= 0 ? ctrl_j.options[ctrl_j.selectedIndex].value : null,
			h = ctrl_h.selectedIndex >= 0 ? ctrl_h.options[ctrl_h.selectedIndex].value : null;
			v = parseInt(j) + parseInt(h) / 100;
		return isNaN(v) ? "" : v;
	}
	
	// doCheck
	function doSubmit(frm)
	{
		var err = "";
		if (!frm.id.value.length)
			err += "Veuillez indiquer un identifiant\n";
		if (frm.type && !frm.type.value.length)
			err+="Veuillez indiquer un type\n";
		if (frm.prefix && !frm.prefix.value.length)
			err+="Veuillez indiquer un pr�fixe\n";
		if(!frm.nom.value.length)
			err+= "Veuillez indiquer le nom\n";
		if(!frm.jours.value.length || isNaN(frm.jours.value))
			err+= "Veuillez indiquer le nombre de <?=$unites?>\n";
		if(!frm.illimite.checked && (!frm.km.value.length || isNaN(frm.km.value)))
			err+= "Veuillez indiquer le km\n";
		if(!frm.jours_min.value.match(/^\d+$/))
			err+= "Veuillez indiquer le nombre de <?=$unites?> minimum\n";
		if(!frm.jours_max.value.match(/^\d+$/))
			err+= "Veuillez indiquer le nombre de <?=$unites?> maximum\n";
		if (!frm.km_max.value.match(/^\d*$/))
			err+= "Veuillez indiquer une valeur correcte pour le kilom�trage maximum\n";
		if(!frm.km_jour.value.length || isNaN(frm.km_jour.value))
			err+= "Veuillez indiquer le km_jour\n";

		// format condens� pour les bornes
		frm.depart_debut.value = date_from_control(frm.depart_debut_j, frm.depart_debut_h);
		frm.depart_fin.value = date_from_control(frm.depart_fin_j, frm.depart_fin_h);
		frm.retour_debut.value = date_from_control(frm.retour_debut_j, frm.retour_debut_h);
		frm.retour_fin.value = date_from_control(frm.retour_fin_j, frm.retour_fin_h);

		// affiche le message d'erreur
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		return true;
	}
	function doIllimite (c) {
		var t = c.form.km;
		t.disabled = c.checked;
		return true;
	}
	
	// -->
	</script>
</head>

<body>

<form action="" method="post" onsubmit="return doSubmit(this);">
<input type="hidden" name="depart_debut" value="<?=$row["depart_debut"]?>">
<input type="hidden" name="depart_fin" value="<?=$row["depart_fin"]?>">
<input type="hidden" name="retour_debut" value="<?=$row["retour_debut"]?>">
<input type="hidden" name="retour_fin" value="<?=$row["retour_fin"]?>">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre" colspan="2">
	<img src="img/puce.gif" align="absmiddle" hspace="1">
<? 
	echo "Forfait";
?>
	</th>
</tr>
<tr>
	<td colspan="2">
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="15%">
		<col width="35%">
		<col width="15%">
		<col width="35%">
		<tr>
			<td>ID</td>
			<td><? echo $id; echo '<input type="'.($id ? "hidden" : "text").'" name="id" value="'.$id.'" maxlength="16">';?></td>
			<td colspan="2">
				<input type="hidden" name="nopromo[]" value="0"/>
				<input type="checkbox" name="nopromo[]" value="1" id="nopromo_1" style="width: inherit;"<? if ($row['nopromo']) echo ' checked';?>/>
				<label for="nopromo_1">Exclure de toute promotion</label>
			</td>
		</tr>
		<tr>
			<td><label for="type">Type</label></td>
			<td>
			<?
				if ($row['type']) {
					echo strtoupper($row['type']);
				} else {
					selectarray('type', Categorie::$TYPE, strtolower($row['type']), '--'); 
				}
			?>
			</td>
			<td><label for="prefix">Pr�fixe</label></td>
			<td>
			<?
				$forfaits = AgencePromoForfait::getForfaits();
				if ($row['type']) {
					$a = (strtolower($row['type'])=='vu' ? $forfaits['vu'] : $forfaits['vp']);
				} else {
					$a = array_merge($forfaits['vp'], $forfaits['vu']);
				}
				selectarray('prefix', array_combine($a, $a), $row['prefix'], '--');
			?>
			</td>
		</tr>
		<tr>
			<td><label for="nom">Nom</label></td>
			<td colspan="3">
				<input type="text" id="nom" name="nom" value="<?=stripslashes($row["nom"])?>" size="64" maxlength="128"/></td>
			</td>
		</tr>
		<tr><th colspan="4">Forfait comprend :</th></tr>
		<tr>
			<td><label for="jours"><?=ucwords($unites)?> Inclus</label></td>
			<td><input type="text" class="l" id="jours" name="jours" value="<?=$row['jours']?>" maxlength="2" size="3">&nbsp;<?=ucwords($unites)?></td>
			<td colspan="2"></td>
		</tr>
		<tr>
			<td><label for="km">Distance Inclus</label></td>
			<td>
				<input type="text" class="l" id="km" name="km" value="<?=$row['km']?>" <?=(is_null($row['km']) ? 'disabled="disabled"' : '');?> size="6" maxlength="5">&nbsp;km
				<input type="checkbox" id="illimite" name="illimite" value="1" onclick="return doIllimite(this);" <?=(is_null($row['km']) ? 'checked="checked"' : '');?>><label for="illimite">Illimit�</label>
			</td>
			<td><label for="km_jour">Km / Jour Supp.</label></td>
			<td><input type="text" class="l" id="km_jour" name="km_jour" value="<?=$row['km_jour']?>" size="6" maxlength="5">&nbsp;km</td>
		</tr>
		<tr><th colspan="4">Crit�res de s�lection</th></tr>
		<tr>
			<td><label for="jours_min">Dur�e : entre</label></td>
			<td>
				<input type="text" class="l" id="jours_min" name="jours_min" value="<?=$row['jours_min']?>" maxlenth="2" size="3">
				<label for="jours_max">&nbsp;et&nbsp;</label>
				<input type="text" class="l" id="jours_max" name="jours_max" value="<?=$row['jours_max']?>" maxlength="2" size="3">&nbsp;<?=$unites?>
			</td>
			<td><label for="km_max">Km maximum</label></td>
			<td><input type="text" class="l" id="km_max" name="km_max" value="<?=$row['km_max']?>" maxlength="5" size="6">&nbsp;km</td>
		</tr>
		<tr>
			<td>D�part entre</td>
			<td><? day_time_control($row, 'depart_debut'); ?></td>
			<td>et</td>
			<td><? day_time_control($row, 'depart_fin'); ?></td>
		</tr>
		<tr>
			<td>Retour entre</td>
			<td><? day_time_control($row, "retour_debut"); ?></td>
			<td>et</td>
			<td><? day_time_control($row, "retour_fin"); ?></td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td align="left">
	<? if ($id) { ?>
		<input type="button" value="tarifs" onclick="parent.list.location.href='lists/forfait_tarif.php?forfait=<?=$id?>'; return false;">
		<input type="button" value="stop sell" onclick="parent.list.location.href='lists/stopsell.php?forfait=<?=$id?>'; return false;">
	<? } ?>
	</td>
	<td align="right">
		<input type="submit" name="<?=$id ? "update" : "create"?>" value="enregistrer">
<?
	if ($id) 
	{
?>
		&nbsp;
		<input type="submit" onclick="return doDelete(this.form);" name="delete" value="supprimer">
<?
	}
?>
	</td>
</tr>
</table>
</form>

</body>
</html>

<?
	// day_time_control
	function day_time_control($row, $a)
	{
		global $JOURS, $HOURS;
		$x = $row[$a];
		if ($x >= 8)
			$x -= 7;
		selectarray($a."_j", $JOURS, ($x != null) ? floor($x): null, '--', " style='width:60%'");
		selectarray($a."_h", $HOURS, ($x != null) ? round(100 * ($x - floor($x))) : null, '--', " style='width:37%'");
	}
?>