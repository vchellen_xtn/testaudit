<?php /** @var PromotionCode */ $code = $this; ?>
<table width="510" bgcolor="#d10600" style="color:white; font-size:11px; font-family:arial;" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="4">
			<img src="img/email/code-reduction-top.gif" alt="">
		</td>
	</tr>
	<tr>
		<td width="15" rowspan="3">
			&nbsp;
		</td>
		<td colspan="2">
			En compensation, ADA vous offre une r&eacute;duction sur le montant de votre prochaine r&eacute;servation.<br>
			<br>
		</td>
		<td width="15" rowspan="3">
			&nbsp;
		</td>
	</tr>
	<tr>
		<td align="right" width="220">
			<span style="font-size:38px;"><?=str_replace('�', '&euro;', htmlentities($code['campagne_slogan']))?></span>&nbsp;&nbsp;&nbsp;&nbsp;	
		</td>
		<td width="290">
			<span style="font-size:14px; font-weight:bold;">avec le code <?=$code['code']?></span>
			<br>valable jusqu'au <?=$code->getExpirationDate('d/m/Y');?>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<br>
			Lors de votre prochaine r&eacute;servation saisissez le code <?=$code['code']?> au niveau de la page de paiement.
			<br>
			Votre r&eacute;duction sera automatiquement appliqu&eacute;. Valable uniquement sur Internet
		</td>
	</tr>
	<tr>
		<td colspan="4">
			<img src="img/email/code-reduction-bottom.gif" alt="">
		</td>
	</tr>
</table>
