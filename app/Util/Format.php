<?php
class Util_Format
{
	static $JOURS = array('1'=>'lundi', '2'=>'mardi', '3'=>'mercredi', '4'=>'jeudi', '5'=>'vendredi', '6'=>'samedi', '7'=>'dimanche');
	static $MOIS  = array('1'=>'janvier', '2'=>'f�vrier', '3'=>'mars', '4'=>'avril', '5'=>'mai', '6'=>'juin', '7'=>'juillet', '8'=>'ao�t', '9'=>'septembre', '10'=>'octobre', '11'=>'novembre', '12'=>'d�cembre');

	static public function money($x)
	{
		$prec = (round($x) == $x) ? 0 : 2;
		return number_format($x, $prec, ",", " ");
	}
	
	static function dateFR($format, $t = null)
	{
		if (is_null($t)) {
			$t = time();
		}
		$output = array();
		// liste des formats pris en compte par date()
		$formats = 'AaBcdDeFgGHhiIjlLMmnNoOPrSsTtUuWwYyZz';
		foreach (str_split($format) as $part) {
			if ($part == 'l') {
				$output[] = Util_Format::$JOURS[date('N', $t)];
			} else if ($part == 'F') {
				$output[] = Util_Format::$MOIS[date('n', $t)];
			} else if (strpos($formats, $part) > -1) {
				$date_element = date($part, $t);
				$output[] = $date_element;
			} else {
				$output[] = $part;
			}
		}
		return implode('', $output);
	}
}