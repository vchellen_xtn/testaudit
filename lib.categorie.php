<?
if (basename(__file__)==basename($_SERVER['SCRIPT_NAME']))
{
	require_once ('constant.php');

	header("Content-Type: text/html;charset=windows-1252");
	header("X-Robots-Tag: noindex");
	show_categories($_GET['skin'], $_GET['zone'], $_GET['type'], (int) $_GET['categorie'], $_GET['size']);
}
// show_categories
function show_categories($skin, $zone, $type, $catID=null, $size='med')
{
	// v�rification des arguemnts
	if (!in_array($skin, array('v2')))
		$skin = 'v2';
	if (!in_array($size, array('med','big')))
		$size = 'med';
	$type = strtolower($type);
	if (!Categorie::$TYPE[$type]) {
		$type = 'vp';
	}
	$zone = 'fr';
	$catID = (int) $catID;
	if ($skin == 'v2')
	{
		// cr�er la collection souhait�e
		if ($type != 'vp')
			$collection = Categorie_Collection::factory($zone, $type);
		else
		{
			$collection = VehiculeFamille_Collection::factory($zone, $type);
			$catID = $collection->checkCategorie($catID);
		}
		// afficher la collection
		$i = 0; $max = $collection->count() - 1;
		if (!$collection[$catID])
			$catID = null;
		foreach($collection as $categorie)
		{
			$nom = $categorie['nom'];
			$class = ($i == 0 ? 'first' : ($i == $max ? 'last' : ''));
			$i++;
			if ($categorie['id'] == $catID || (!$catID && $categorie['defaut'])) $class .= ' current';
			echo '<li';
			if ($class) echo ' class="'.trim($class).'"';
			echo '>';
			echo '<div class="container--slide-carrousel">';
			echo '<strong>'.$nom.'</strong>';
			if ($type=='vu') {
				if (!$here) {
					$here = Page::getURL(null, null, false);
				}
				echo '<a class="help" href="'.$here.'popup.php?page=demenagement/categorie&categorie='.$categorie['id'].'" onclick="return wOpen(this.href, 570, 450);"><img src="../css/img/options_details.gif" alt="" /></a>';
			}
			echo '<img class="cat_img" id="cat-'.$categorie['id'].'" src="../fichiers/categories/'.$type.'/'.$categorie['mnem'].'-'.$size.'.png" alt="'.$nom.'" />';
			echo '<p>'.nl2br($categorie['commentaire'] ? $categorie['commentaire'] : $categorie['description']).'</p>';
			echo '</div>';
			echo '</li>'."\n";
		}
		return;
	}

}
?>