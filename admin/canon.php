<?
	include_once ("secure.php");
	include_once ("lib.php");
?>
<html>
<head>
	<title>Mise � jour des noms canoniques</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
</head>
<body>

<h1>Mise � jour des noms canoniques</h1>
<?
	if ($_GET['reset'])
	{
	//	getsql("update agence_alias set canon = null");
		getsql("update region set canon = null");
		getsql("update dept set canon = null");
		getsql("update zone set canon = null");
		getsql("update categorie set canon = null");
		getsql("update vehicule_marque set canon = null");
		getsql("update vehicule_modele set canon = null");
	}

	// canonize_sql
	function canonize_sql($sql, $tbl)
	{
		// parcourir les canons � cr�er
		for ($j=0, $cnt=0, $rs=sqlexec($sql); $row=mysql_fetch_assoc($rs); $cnt++)
		{
			// r�cup�rer la liste des canons sur la premi�re occurence
			if (!$cnt)
			{
				$all_canon = array();
				for ($rs2 = sqlexec("select canon from $tbl where canon is not null"); $row2 = mysql_fetch_row($rs2);)
					$all_canon[] = $row2[0];
			}
			// tester les diff�rents canons
			$canon = canonize($row['nom']);
			for($j=1, $row['canon']=$canon; in_array($row['canon'], $all_canon); $j++)
			{
				$row['canon'] = $canon."-".$j;
				echo $row['canon']."<br>";
			}
			$all_canon[] = $row['canon'];
			unset($row['nom']);
			save_record($tbl, $row);
		}
		if ($cnt)
			echo "<p>$cnt $tbl(s) mise(s) � jour !</p>\n";
	}
	
	// agences
	canonize_sql("SELECT id, nom FROM agence_alias WHERE NULLIF(canon,'') IS NULL AND (statut & 1) = 1 ORDER BY nom", "agence_alias");
	
	// zone
	canonize_sql("SELECT id, nom FROM zone WHERE canon IS NULL", "zone");
	
	// r�gion
	canonize_sql("SELECT id, nom FROM region WHERE canon IS NULL", "region");

	// d�partement
	canonize_sql("SELECT id, nom FROM dept WHERE canon IS NULL", "dept");
	
	// cat�gories
	canonize_sql("SELECT id, nom FROM categorie WHERE canon IS NULL", "categorie");

	// vehicule_marque
	canonize_sql("SELECT id, id nom FROM vehicule_marque WHERE canon IS NULL", "vehicule_marque");

	// vehicule_modele
	canonize_sql("SELECT id, CONCAT(marque,' ',nom) nom FROM vehicule_modele WHERE canon IS NULL", "vehicule_modele");

	// mettre � jour la table refnat_canon
	echo "refnat_canon: suppression<br>";
	getsql('TRUNCATE TABLE refnat_canon');
	getsql("INSERT INTO refnat_canon SELECT DISTINCT 'zone', z.id, z.canon, z.nom FROM zone z JOIN agence_alias aa ON aa.zone=z.id AND (aa.statut&1)=1");;
	getsql("INSERT INTO refnat_canon SELECT 'region', id, canon, nom FROM region WHERE zone='fr';");
	getsql("INSERT INTO refnat_canon SELECT 'dept', id, canon, nom FROM dept WHERE zone='fr';");
	getsql("INSERT INTO refnat_canon SELECT 'agence', id, canon, nom FROM agence_alias WHERE (statut & 1)=1 AND pageweb=1;");
	getsql("INSERT INTO refnat_canon SELECT 'agglo', id, canon, nom FROM agence_agglo");
	getsql("INSERT INTO refnat_canon SELECT 'emplacement', id, canon, nom FROM agence_emplacement WHERE id > 1");
	getsql("INSERT INTO refnat_canon SELECT 'categorie', c.id, c.canon, concat(ifnull(concat(nullif(f.nom,c.nom),' '),''), c.nom) nom FROM categorie c LEFT JOIN vehicule_famille f ON f.id=c.famille WHERE publie=1;");
	getsql("INSERT INTO refnat_canon SELECT 'type', id, canon, nom FROM vehicule_type;");
	getsql("INSERT INTO refnat_canon SELECT DISTINCT 'marque', vm.id, vm.canon, vm.nom FROM vehicule_marque vm JOIN vehicule_modele vmc ON vm.id=vmc.marque and vmc.publie=1;");
	getsql("INSERT INTO refnat_canon SELECT DISTINCT 'modele', vm.id, vm.canon, vm.nom FROM vehicule_modele vm WHERE vm.publie=1;");
	echo "refnat_canon: mis � jour<br>";
?>

<h1>Agence : v�rification du nom canonique</h1>
<?
	$rs = sqlexec("SELECT canon FROM agence_alias GROUP BY canon HAVING COUNT(*) > 1");
	if ($rs && mysql_num_rows($rs) > 0)
	{
		echo "\t".'<table border="1" cellpadding="2" cellspacing="0">'."\n";
		echo "\t<caption>".mysql_num_rows($rs)." noms canoniques en double</caption>\n";
		echo "\t<tr><th>Canon</th><th>Agences</th></tr>\n";
		while ($row = mysql_fetch_assoc($rs))
		{
			echo "\t<tr><td>".$row['canon']."</td><td>";
			$rs2 = sqlexec("select id, nom, cp, ville, statut from agence_alias where canon='".$row['canon']."'");
			while ($row2 = mysql_fetch_assoc($rs2))
				echo '<a target="blank" href="agence_alias.php?id='.$row2['id'].'">'.$row2['id'].'</a> '.$row2['nom'].' ('.$row2['ville'].', '.$row2['cp'].') - '.(($row2['statut']&1)==1 ? 'publi�' : 'non publi�').'<br>';
			echo "</td></tr>\n";
		}
		echo "\t</table>\n";
	}
?>
<h1>V�rification du nom canonique</h1>
<?
	$rs = sqlexec("SELECT canon FROM refnat_canon GROUP BY canon HAVING COUNT(*) > 1");
	if ($rs && mysql_num_rows($rs) > 0)
	{
		echo "\t".'<table border="1" cellpadding="2" cellspacing="0">'."\n";
		echo "\t<caption>".mysql_num_rows($rs)." noms canoniques multiples</caption>\n";
		echo "\t<tr><th>Canon</th><th>Valeurs</th></tr>\n";
		while ($row = mysql_fetch_assoc($rs))
		{
			echo "\t<tr><td>".$row['canon']."</td><td>";
			$rs2 = sqlexec("select type, id, nom from refnat_canon where canon='".$row['canon']."'");
			while ($row2 = mysql_fetch_assoc($rs2))
				echo $row2['type'].':&nbsp;<a target="_blank" href="'.$row2['type'].'.php?id='.$row2['id'].'">'.$row2['id'].' - '.$row2['nom'].'</a><br>';
			echo "</td></tr>\n";
		}
		echo "\t</table>\n";
	}
?>
</body>
</html>
