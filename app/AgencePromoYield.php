<?
class AgencePromoYield extends Ada_Object
{
	/**
	* factory
	* 
	* @param int $id
	* @return AgencePromoYield
	*/
	static public function factory($id)
	{
		$x = new AgencePromoYield();
		if ($x->load($id)->isEmpty())
			return null;
		return $x;
	}
	/**
	* Renvoie la variation pour un groupe, type, cat�gorie, jauge
	* 
	* @param string $code_base
	* @param string $code_groupe
	* @param string $type
	* @param string $categorie
	* @param int $jauge
	* @return AgencePromoYield
	*/
	static public function find($code_base, $code_groupe, $type, $categorie, $jauge) {
		$sql = 'select * from agence_promo_yield where code_base="%s" and code_groupe="%s" and type="%s" and categorie="%s" and %ld between jauge_min and jauge_max';
		if ($rs = sqlexec(sprintf($sql, $code_base, $code_groupe, $type, $categorie, $jauge))) {
			if ($row = mysql_fetch_assoc($rs)) {
				return new AgencePromoYield($row);
			}
		}
		return null;
	}
	
	/**
	* Modifie en base l'ensemble des donn�es pour une cat�gories
	* 
	* @param array $a
	* @param array $errors
	* @returns bool
	*/
	static public function create(&$a, &$errors)
	{
		// champs erron�s
		$fieldsError = array();
		
		// champs obligatoires
		if(!self::_checkField($a['code_base'], '/^[a-z0-9]{2}$/i'))
			$fieldsError[] = 'code_base';
		if(!self::_checkField($a['code_groupe'], '/^[a-z0-9\-\_\s]+$/i'))
			$fieldsError[] = 'code_groupe';
		if(!self::_checkField($a['type'], '/^v[pu]/i', true))
			$fieldsError[] = 'type';
		// categories (au moins une de selectionn�e)
		if(!empty($a['categories']) && is_array($a['categories']))
		{
			foreach($a['categories'] as $c)
			{
				if(!self::_checkField($c, '/^[^\s]+$/i'))
				{
					$fieldsError[] = 'categories';
					break;
				}
			}
		}
		else
		{
			$errors[] = 'Vous devez s�lectionner au moins une cat�gorie';
		}
		
		// grille jauges/montants
		if(empty($a['jauge_min']) || empty($a['jauge_max']) || empty($a['montant']) 
		|| count($a['jauge_min']) != count($a['jauge_max']) || count($a['jauge_min']) != count($a['montant']))
		{
			$errors[] = 'Valeurs de minimum, maximum, et/ou pourcentage manquantes';
		}
		
		// erreurs
		foreach($fieldsError as $f)
		{
			$errors[] = 'Le champ '.$f.' doit �tre correctement renseign�.';
		}
		
		// enregistrement
		if(empty($errors))
		{
			// suppressions entr�es pour ce groupe /ces categories
			$sql  = 'DELETE FROM agence_promo_yield';
			$sql .= ' WHERE code_base="'.$a['code_base'].'" AND code_groupe="'.$a['code_groupe'].'"';
			$sql .= ' AND categorie IN (';
			$sql .= '"'.implode('","', $a['categories']).'"';
			$sql .= ' )';
			sqlexec($sql);
			// creation des nouvelles entr�es
			$creation = date('Y-m-d H:i:s');
			$entries = array();
			$sql  = ' INSERT INTO agence_promo_yield(code_base, code_groupe, type, categorie, jauge_min, jauge_max, montant, creation, modification) VALUES';
			// une ligne par categorie et par valeurs de jauge
			foreach($a['categories'] as $c)
			{
				for($i = 0; $i < count($a['jauge_min']); $i++)	// NB : on a deja verifi� que count(jauge_min)==count(jauge_max)==count(montant)
				{
					$entries[] = ' ("'.$a['code_base'].'", "'.$a['code_groupe'].'", "'.$a['type'].'", "'.$c.'", '.(int)$a['jauge_min'][$i].', '.(int)$a['jauge_max'][$i].', '.(int)$a['montant'][$i].', "'.$creation.'", "'.$creation.'")';
				}
			}
			$sql .= implode(',', $entries);
			sqlexec($sql);
			return true;
		}
		return false;
	}
	
	/**
	* V�rifie le champ
	* NB : renvoie true si champ optionnel vide. Renvoie false si champ optionnel rempli incorrectement.
	* 
	* @param mixed $field
	* @param mixed $value
	* @param mixed $regex
	* @param mixed $required
	*/
	static private function _checkField($value, $regex, $required = false)
	{
		// champ OK si : (requis && regex) || (!requis && (vide || regex))
		return ($required && preg_match($regex, $value)) || (!$required && (empty($value) || preg_match($regex, $value)));
	}
	/**
	* Renvoie le prix avec la variation Yield
	* 
	* @param float $ttc
	* @returns float
	*/
	public function getPrixYield($ttc) {
		// le montant peut �tre positif (augmentation) ou n�gatif (r�duction)
		$ttc += ($this->_data['montant'] * $ttc)/100;
		if ($ttc < 0) $ttc = 0;
		return round($ttc, 0);
	}
	/**
	* Renvoie une description
	* @resturns strring yield_#jauge_min#_#jauge_max#
	*/
	public function getBornes() {
		return sprintf('yield_%ld_%ld',  $this->_data['jauge_min'], $this->_data['jauge_max']);
	}
	/**
	* Renvoie une description de la r�gle de Yield
	* @returns string Yield_50_100 +10%
	*/
	public function getDescription() {
		return ucfirst(sprintf('%s %+.2f', $this->getBornes(), $this->_data['montant']));
	}
}
?>