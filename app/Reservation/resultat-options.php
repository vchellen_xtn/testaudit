<?php
/**
* $this est Reservation
* $forfait doit �tre pass� en arguemnt
* $page action de la page courante pour s�lectionner les options
* $track_page : pour le suivi des popins
* $formAction	page suivante
* $labelTotal : libell� pour le total
* $display1Euro : commande l'affichage de 1Euro
* $themes tableau des th�mes � afficher sans tenir compte de la page indiqu�e
* 	'theme' => array
* 			(
* 				'ouvert' => 1 | 0	fieldset ouvert ou non par d�faut
* 				'toggle' => 1 | 0	pouvoir supprimer le toogle (uniquement si ouvert)
* 				'visuels' => 1 | 0	afficher les vieuels
* 				'presentation'	=> 'ligne2'|'ligne3'	une pr�sentation diff�rente...
* 			)
*/
if (!$labelTotal) {
	$labelTotal = 'Total TTC';
}
if (empty($themes)) {
	$themes = array();
}
if (is_null($display1Euro)) {
	$display1Euro = true;
}
/** @var Reservation */
$reservation = $this;
/** @var Forfait */
$forfait = $forfait;
if (!$forfait) return;
/** @var Option_Collection : r�cup�rer la liste des options */
$options = $reservation->getAllOptions($forfait, false, true)->fixFranchise($reservation);
$type = strtolower($reservation['type']);

// d�finir l'action du formauliare
if (!$formAction) {
	$formAction = ($reservation['forfait_type']=='RESAS' ? 'inscription.html' : '../reservation.php');
	if ($page == 'resultat' && $options->hasPage('options'))
		$formAction = 'options.html';
}
/* DEBUG options anormales
if (isset($options['pfvp']) || isset($options['pfvu'])) {
	$txt = $page."\r\n";
	$txt.= print_r($_POST, true)."\n";
	foreach($options as $option) {
		$txt.= $option['theme'].' - '.$option['id'].' - '.$option['nom']."\r\n";
	}
	send_mail($txt, 'gdubourguet@rnd.fr', '[ADA] Debug '.date('Y-m-d H:i:s'), EMAIL_FROM, EMAIL_FROM_ADDR, '', '');
	$txt = null;
}
/* */
?>
<!-- afficher les tarifs -->
<div id="offre_options"<? if (!$forfait['id']) echo ' style="display: none;"'?> class="js-accordion">
	<form action="<?=$formAction?>" method="post" onsubmit="return doCheckReservationTarif(this);" id="frm_reservation_tarif">
		<input type="hidden" name="key" value="<?=Page::getKey('reservation')?>"/>
		<input type="hidden" name="forfait" value="<?=$forfait->doSerialize();?>"/>
		<? if ($reservation['forfait_type']=='RESAS') : ?>
		<input type="hidden" name="unipro_vehicule" value="<?=$forfait->getData('unipro_vehicule');?>"/>
		<input type="hidden" name="operation" value="<?=$forfait->getData('operation');?>"/>
		<? endif; ?>
		<?
		copy_form($reservation->getFields());
		
		// afficher les frais d'adh�sion et la surcharge si n�cessaire
		$theme_avec_prix = array();
		if ($prix_coupon = $reservation->getPrixCoupon()) 
			$theme_avec_prix['prix_coupon'] = array('theme_nom'=>"Frais d'adh�sion compte pro", 'theme_prix' => $prix_coupon);
		if (!$reservation->isLocapass())
			$surcharge = round($reservation->getPrixSurcharge());
		if($surcharge > 0)
			$theme_avec_prix['surcharge'] = array('theme_nom'=>$reservation->getAgence()->getSurchargeLabel(), 'theme_prix' => $surcharge);
		// on ne l'affiche que pour la page resultats
		if ($page != 'resultat')
			$theme_avec_prix = array();
		foreach($theme_avec_prix as $k => $theme) :
			if (!empty($themes) && !isset($themes[$k])) continue;
?>
		<fieldset class="option_theme theme_avec_prix">
			<legend>
				<strong><?=$theme['theme_nom']?></strong>
				<span class="theme_span_prix">
					<?=show_money($theme['theme_prix'])?> �
					<input type="hidden" name="<?=$k?>" value="<?=$theme['theme_prix']?>" />
				</span>
			</legend>
		</fieldset>
<?
		endforeach;
		// si RESAS, on change l'option par d�faut
		if ($reservation['forfait_type']=='RESAS') {
			foreach(array('nf','rf','af') as $k) {
				if (isset($options[$k.$type])) {
					$options[$k.$type]['init'] = ($k == 'nf' ? 1 : 0);
				}
			}
		}
		
		// afficher la liste des options en comparant aux options d�j� s�lectionn�es
		$resOptions = $reservation->getResOptions();
		$prix_options = $surcharge + $prix_coupon;
		$theme = null; $closeTags = array();
		$a_retirer = false; $date_retrait = null;
		
		foreach ($options as /** @var Option */ $option) :
			// l'option est-elle s�lectionn�e ?
			$optionID = $option['id'];
			if (($option['init'] && (!$resOptions || $resOptions->isEmpty())) || $resOptions[$optionID])
			{
				// cas particulier du rachat de franchie 'rfv[pu]' qui est aussi pris en charge dans 'afv[pu]'
				if ($optionID == 'rf'.$type && $resOptions['af'.$type])
					$option['init'] = 0;
				else
				{
					$option['init'] = ($option['quantite_max'] > 1 && $resOptions[$optionID]) ? (int) $resOptions[$optionID]['quantite'] : 1;
					// sur la page r�sultat on ne compte pas les options de la page suivante
					if ($page == 'options' || (empty($themes) && $option['theme_page'] == $page) || (!empty($themes) && isset($themes[$option['theme']])))
						$prix_options += $option['prix'] * $option['init'];
				}
			}
			else
			{
				$option['init'] = 0;
			}
			// on n'affiche pas les options qui ne sont pas sur la bonne page
			// mais on les enregistre dans le formulaire pour les passer � la page suivante
			if (!$date_retrait && $option['a_retirer'] && $resOptions[$optionID]['retrait'])
				$date_retrait = $resOptions[$optionID]['retrait'];
			// on n'affiche que les options de la page sauf pour RESAS...
			if (((empty($themes) && $option['theme_page'] != $page) || (!empty($themes) && !isset($themes[$option['theme']]))) && $reservation['forfait_type']!='RESAS')
			{
				if ($option['init'])
				{
					if ($option['quantite_max'] > 1)
						echo '<input type="hidden" name="options_u['.$optionID.']" value="'.$option['init'].'"/>';
					else
						echo '<input type="hidden" name="options[]" value="'.$optionID.'"/>';
				}
				continue;
			}
			// faut-il montrer la liste des dates
			if (!$a_retirer && $option['a_retirer'])
				$a_retirer = true;
			// v�rifier la pr�sentation
			$theme_presentation = $option['presentation'];
			if (isset($themes[$option['theme']]) && isset($themes[$option['theme']]['presentation']) && in_array($themes[$option['theme']]['presentation'], array('ligne2','ligne3'))) {
				$theme_presentation = $themes[$option['theme']]['presentation'];
			}
			// pr�sentation de l'option
			if ($option['theme_nom'] != $theme)
			{
				echo join("\n", array_reverse($closeTags));
				$closeTags = array();
				$groupe = null;
				$theme = $option['theme_nom'];
				$theme_toggle = ($option['theme_ouvert'] ? 'theme_ouvert' : 'theme_ferme');
				$theme_avec_toggle = 'theme_avec_toggle';
				if (isset($themes[$option['theme']]) && isset($themes[$option['theme']]['ouvert'])) {
					$theme_toggle = ($themes[$option['theme']]['ouvert'] ? 'theme_ouvert' : 'theme_ferme');
					if ($theme_toggle == 'theme_ouvert' && isset($themes[$option['theme']]['toggle']) && !$themes[$option['theme']]['toggle']) {
						$theme_avec_toggle = '';
					}
				}
				if ($reservation['forfait_type']=='RESAS') {
					// pour RESAS on ouvre les th�mes de la page "resultat" et on ferme les autres...
					$theme_toggle = ($option['theme_page']=='resultat' ? 'theme_ouvert' : 'theme_ferme');
					// ... et un "clear" pour regrouper les th�mes ensemble
					if ($option['theme_page'] == 'options' && !$clearThemes) {
						$clearThemes = true;
						echo '<div class="clear"></div>';
					}
				}
				?>
				<fieldset id="fieldset_theme_<?=$option['theme'];?>" class="option_theme <?=$theme_avec_toggle;?> <?=$theme_toggle;?>">
					<legend>
						<strong>
							<?=$theme?>
							<? if (false && $option['theme_popin']) : /* si on affiche le lien, au clic cela d�clenche l'ouverture / fermeture du fieldset... */ ?>
							<a class="_theme_popin" style="cursor:pointer;" onclick="return showPopIn('<?=$option['theme_popin']?>');"><img src="../css/img/options_details.png" alt="" class="trigger-option-popin" /></a>
							<? endif; ?>
						</strong>
						<span class="theme_span_toggle"></span>
					</legend>
					<table border="0" cellpadding="0" cellspacing="0">
					<colgroup>
						<? if ($theme_presentation == 'ligne2') : ?>
						<? $nbCols = 3; ?>
						<col class="forth-col" width="20"/>
						<col class="third-col" width="40"/>
						<col class="second-col" width="434"/>
						<? elseif ($theme_presentation == 'ligne3') : ?>
						<? $nbCols = 3; ?>
						<col class="forth-col" width="20"/>
						<col class="second-col"/>
						<col class="third-col" width="40"/>
						<? else : ?>
						<? $nbCols = 4; ?>
						<col class="first-col" width="40"/>
						<col class="second-col" width="304"/>
						<col class="third-col" width="90"/>
						<col class="forth-col" width="50"/>
						<? endif; ?>
					</colgroup>
				<?
				$closeTags[] = '</table></fieldset>';
				// puor la pr�sentation en grille, on ouvre tout de suite une ligne
				if ($option['presentation']=='grille') : ?>
					<tr id="theme_<?=$option['theme'];?>" class="option_theme_grille">
						<td colspan="<?=$nbCols;?>">
							<ul>
				<?
					$closeTags[] = '</ul><div class="clear"></div></td></tr>';
				endif;
			}
			// pour un groupe �crire la ligne pr�sentant le groupe
			if ($option['groupe'] != $groupe) :
				if ($groupe)
					echo array_pop($closeTags);
				$groupe = ($option['groupe'] && $options->isInGroup($option['groupe'])) ? $option['groupe'] : null;
				if (!$groupe)
					$option['groupe'] = $groupe;
				if ($groupe):
					if ($option['presentation']=='grille') :
					?>
						<li id="options_<?=$groupe?>">
							<table border="0" cellpadding="0" cellspacing="0" id="groupe_<?=$groupe?>">
							<tr>
								<td colspan="3">
									<label class="option_theme_grille-label"><img src="../css/options/<?=$groupe?>-sml.png" alt=" " class="left" /><?=$option['groupe_nom']?></label>
								</td>
							</tr>
					<?
						$closeTags[] = '</table></li>';
					else :
					?>
						<tr id="groupe_<?=$groupe?>">
							<? if ($theme_presentation == 'ligne2') : ?>
							<td colspan="2">&nbsp;</td>
							<td>
								<label><?=$option['groupe_nom']?></label>
								<input type="hidden" name="options[]" id="opt_groupe_<?=$groupe?>" autocomplete="off" value=""/>
							</td>
							<? else : ?>
							<td><img src="../css/options/<?=$groupe?>-sml.png" alt=" "/></td>
							<td>
								<label><?=$option['groupe_nom']?></label>
								<input type="hidden" name="options[]" id="opt_groupe_<?=$groupe?>" autocomplete="off" value=""/>
							</td>
							<td colspan="2">&nbsp;</td>
							<? endif; ?>
						</tr>
						<? if ($option['presentation'] == 'liste') : ?>
						<? if ($theme_presentation == 'ligne2' && $groupe == 'franchise') : ?>
						<tr class="option_theme_franchise" id="options_<?=$groupe?>_titre">
							<td></td>
							<td colspan="2">Ma franchise</td>
						</tr>
						<? endif; ?>
						<tr class="option_theme_liste" id="options_<?=$groupe?>">
							<? if ($theme_presentation == 'ligne2' && $groupe == 'franchise') : ?>
							<td></td>
							<td colspan="2">
							<ul class="option_theme_liste count_liste_<?=$options->getGroupCount($groupe)?>">
							<? $closeTags[] = '</ul><p id="error_groupe_franchise" style="display: none;"></p></td></tr>'; ; ?>
							<? else : ?>
							<td colspan="<?=$nbCols;?>">
							<ul class="option_theme_liste count_liste_<?=$options->getGroupCount($groupe)?>">
							<? $closeTags[] = '</ul></td></tr>'; ; ?>
							<? endif;
						else :
							$closeTags[] = '';
						endif;
					endif;
				endif;
			endif;
			// affichage de l'option soit sous forme de liste soit sous forme de ligne du tableau
			if ($groupe && $option['presentation']=='liste') : ?>
				<li id="option_<?=$optionID?>">
					<? if ($theme_presentation == 'ligne2') : ?>
					<? if ($option['popin']) : ?>
						<a style="cursor:pointer;" onclick="return showPopIn('<?=$option['popin']?>');"><img src="../css/img/options_details.png" alt="" class="trigger-option-popin" /></a>
					<? endif; ?>
					<label for="<?=$optionID?>">
						<span class="option_choice"><? echo $option->toInput();  ?></span>
						<?
							$accroche = null; $franchise = null; $commentaire = null;
							switch(substr($option['id'], 0, 2)) {
								case 'af':
									$accroche = 'Annulez votre franchise pour '.$option->getPrix();
									$franchise = $reservation->getCategorie()->getData('franchise_af');
									$commentaire = "Protection maximale\nD�p�t de garantie r�duit";
									break;
								case 'rf':
									$accroche = 'R�duisez votre franchise pour '.$option->getPrix();
									$franchise = $franchise = $reservation->getCategorie()->getData('franchise_rf');
									$commentaire = "D�p�t de garantie r�duit";
									break;
								case 'nf':
									$accroche = 'Franchise totale';
									$franchise = $franchise = $reservation->getCategorie()->getData('franchise');
									break;
							}
						?>
						<span class="option_franchise_montant"><? echo show_money($franchise); ?>&nbsp;&euro;</span>
						<div class="option_franchise_edito">
							<p class="option_franchise_accroche"><?=$accroche;?></p>
							<? if ($commentaire) : ?>
								<ul class="option_commentaire_<?=$option['presentation']?>">
									<li><? echo implode('</li><li>', preg_split('/[\r\n]+/', $commentaire));?></li>
								</ul>
							<? endif; ?>
						</div>
					</label>
					<? else : ?>
					<img src="../css/options/<?=$optionID?>-sml.png" alt=" "/>
					<? if ($option['popin']) : ?>
						<a style="cursor:pointer;" onclick="return showPopIn('<?=$option['popin']?>');"><img src="../css/img/options_details.png" alt="" class="trigger-option-popin" /></a>
					<? endif; ?>
					<br/>
					<label for="<?=$optionID?>">
						<? if ($reservation['forfait_type']=='RESAS') : ?>
							<? echo $option['nom']; ?>
						<? else: ?>
							<div class="option_franchise_accroche">
								<?=($option['accroche'] ? $option['accroche'] : $option['nom'])?>
							</div>
							<? if ($option['commentaire']) : ?>
								<div class="option_commentaire_<?=$option['presentation']?>">
									<?=$option['commentaire']?>
								</div>
							<? endif; ?>
						<? endif; ?>
						<span class="option_prix"><?=$option->getPrix();?></span>
						<span class="option_choice"><? echo $option->toInput();  ?></span>
						<? if ($reservation['forfait_type']=='RESAS' && $option['commentaire']) : ?>
							<div class="clear option_commentaire_<?=$option['presentation']?>">
								<?=str_replace(array('<br/>', '<br />', '<br>'), '', $option['commentaire']);?>
							</div>
						<? endif; ?>
					</label>
					<? endif; ?>
				</li>
			<? elseif ($groupe && $option['presentation']=='grille') :?>
				<tr id="option_<?=$optionID?>" class="option_theme_<?=$option['presentation']?>">
					<td class="option_label">
						<label for="<?=$optionID?>"><?=$option['nom']?></label>
						<? if ($option['popin']) : ?>
							<a style="cursor:pointer;" onclick="return showPopIn('<?=$option['popin']?>');"><img src="../css/img/options_details.png" alt="" /></a>
						<? endif; ?>
						<? if ($reservation['forfait_type']!='RESAS' && !empty($option['accroche'])) : ?>
							<br><span class="option_accroche"><?=$option['accroche'];?></span>
						<? endif; ?>
						<? if ($reservation['forfait_type']!='RESAS' && !empty($option['commentaire'])) : ?>
							<div class="option_commenatire_<?=$option['presentation']?>"><?=$option['commentaire']?></div>
						<? endif; ?>
					</td>
					<td class="option_prix">
						<?=$option->getPrix();?>
					</td>
					<td class="option_choice">
						<? echo $option->toInput(); ?>
					</td>
				</tr>
			<? else : /* presentation ligne ou blic */?>
				<tr id="option_<?=$optionID?>" class="option_theme_<?=$option['presentation']?>">
					<? if ($theme_presentation == 'ligne2') : ?>
					<td class="option_choice">
						<? echo $option->toInput(); ?>
					</td>
					<td class="option_prix">
						<?=$option->getPrix();?>
					</td>
					<td class="option_label">
						<label for="<?=$optionID?>"><?=$option['nom']?></label>
						<? if ($option['popin']) : ?>
							<a style="cursor:pointer;" onclick="return showPopIn('<?=$option['popin']?>');"><img src="../css/img/options_details.png" alt="" /></a>
						<? endif; ?>
						<? if ($reservation['forfait_type']!='RESAS') : ?>
							<? if ($option['accroche']) : ?>
								<br/><span class="option_accroche"><?=$option['accroche']?></span>
							<? endif; ?>
							<? if ($option['commentaire']) : ?>
								<div class="option_commenatire_<?=$option['presentation']?>"><?=$option['commentaire']?></div>
							<? endif; ?>
						<? endif; ?>
					</td>
					<? elseif ($theme_presentation == 'ligne3') : ?>
					<td class="option_choice">
						<? echo $option->toInput(); ?>
					</td>
					<td class="option_label">
						<label for="<?=$optionID?>"><?=$option['nom']?></label>
						<? if ($option['popin']) : ?>
							<a style="cursor:pointer;" onclick="return showPopIn('<?=$option['popin']?>');"><img src="../css/img/options_details.png" alt="" /></a>
						<? endif; ?>
						<? if ($reservation['forfait_type']!='RESAS') : ?>
							<? if ($option['accroche']) : ?>
								<br/><span class="option_accroche"><?=$option['accroche']?></span>
							<? endif; ?>
							<? if ($option['commentaire']) : ?>
								<div class="option_commenatire_<?=$option['presentation']?>"><?=$option['commentaire']?></div>
							<? endif; ?>
						<? endif; ?>
					</td>
					<td class="option_prix">
						<?=$option->getPrix();?>
					</td>
					<? else : ?>
					<td>
						<? if (!isset($themes[$option['theme']]) || !isset($themes[$option['theme']]['visuels']) || $themes[$option['theme']]['visuels']) : ?>
						<img src="../css/options/<?=$optionID?>-sml.png" alt=" "/>
						<? endif; ?>
					</td>
					<td class="option_label">
						<label for="<?=$optionID?>"><?=$option['nom']?></label>
						<? if ($option['popin']) : ?>
							<a style="cursor:pointer;" onclick="return showPopIn('<?=$option['popin']?>');"><img src="../css/img/options_details.png" alt="" /></a>
						<? endif; ?>
						<? if ($reservation['forfait_type']!='RESAS') : ?>
							<? if ($option['accroche']) : ?>
								<br/><span class="option_accroche"><?=$option['accroche']?></span>
							<? endif; ?>
							<? if ($option['commentaire']) : ?>
								<div class="option_commenatire_<?=$option['presentation']?>"><?=$option['commentaire']?></div>
							<? endif; ?>
						<? endif; ?>
					</td>
					<td class="option_prix">
						<?=$option->getPrix();?>
					</td>
					<td class="option_choice">
						<? echo $option->toInput(); ?>
					</td>
					<? endif; ?>
				</tr>
			<? endif; ?>
		<? endforeach; ?>
		<? echo join("\n", array_reverse($closeTags)); ?>
		<? if ($a_retirer) : // afficher les dates de retarit possibles : 2 semaines avant la r�servation, au moins 72h apr�s le paiement ?>
		<?
			$agence = $reservation->getAgence(); 
			$tEnd = strtotime($reservation->getDebut('Y-m-d')); 
			$tStart = min($tEnd, max(strtotime('+3 days'), strtotime('-2 weeks', $tEnd)));
			$tStart = strtotime(date('Y-m-d', $tStart));
			$jours = array();
			for($t=$tStart, $jour = date('Y-m-d', $t); $t <= $tEnd; $t = strtotime('+1 day', $t), $jour = date('Y-m-d', $t))
			{
				if ($agence->isClosed($jour, date('N', $t))) continue;
				$jours[$jour] = Util_Format::dateFR('l d F Y', $t);
			}
		?>
		<? if ($reservation['forfait_type']=='RESAS') : ?>
		<div class="clear"></div>
		<? endif; ?>
		<fieldset id="fieldset_retrait" class="option_theme theme_avec_prix option_theme_date" <? if (!$date_retrait) echo ' style="display: none;"';?>>
			<strong>Choisir la date de retrait en agence de vos achats</strong>
			<span class="theme_span_prix">
				<select name="options_retrait" id="options_retrait" style="width: inherit;">
					<option value="">-- choisir une date --</option>
					<? foreach($jours as $k => $v) : ?>
					<option value="<?=$k?>" <? if ($k==$date_retrait) echo ' selected="selected"';?> ><?=$v?></option>
					<? endforeach; ?>
				</select>
			</span><br/>
			<? if (count($jours) < 2) : ?>
				<strong class="warning">Confirmation de votre commande sous 24h</Strong>
			<? endif; ?>
		</fieldset>
		<? elseif ($date_retrait) : ?>
			<input type="hidden" name="options_retrait" value="<?=$date_retrait?>"/>
		<? endif; ?>
		<div class="offre_bottom">
		<? if (!$reservation->isLocapass()) : ?>
			<? $prixTotal = $forfait->getPrix() + $prix_options; ?>
			<div class="offre_total">
				<p>
					<span class="total"><?=$labelTotal;?></span>
					<span class="prix_total" id="prix_total"><?=show_money($prixTotal)?></span>
					<span class="euro">�</span>
					<span class="end"></span>
				</p>
				<? if ($reservation['forfait_type']!='RESAS') : ?>
				<span class="paiement_immediat">tarif pour un paiement imm�diat</span>
				<? endif; ?>
			</div>
			<div class="clear"></div>
			<? if ($display1Euro) : ?>
			<? echo $reservation->show1Euro(-54, -315, $prixTotal); ?>
			<? endif; ?>
			<div class="clear"></div>
		<? endif; ?>
			<p class="valid">
				<input type="submit" value="Continuez" class="default-btn" />
			</p>
			<input type="hidden" autocomplete="off" name="prix_forfait" value="<?=round($forfait->getPrix(),2);?>"/>
			<input type="hidden" autocomplete="off" name="prix_options" value="<?=$prix_options?>"/>
			<div class="clear"></div>
		</div>
	</form>
</div>
<div id="dialog-options" title="">
	<div id="dialog-options-msg"></div>
</div>
<script type="text/javascript">
// <![CDATA[
	var reservation_resultat_initial = true;
	<? if ($resOptions && !$resOptions->isEmpty()) : ?>
	reservation_resultat_initial = false;
	<? endif; ?>
	$(document).ready(function()
	{
		// initialise les th�mes
		$(".theme_avec_toggle").accordion({ 
			header: 'legend',
			autoHeight:false, 
			active:true, 
			collapsible:true
		});
		// ouvrir le th�ems ouverts ou avec des cases d�j� coch�s
		$("#frm_reservation_tarif").hide();
		$(".theme_ouvert legend, .theme_ferme:has(:checkbox:checked) legend, .theme_ferme:has(:selected[value!='0']) legend").click();
		$("#frm_reservation_tarif").show();

		// initialiser les options s�lectionn�es par les bouton radio
		$("#frm_reservation_tarif input:radio:checked").each(function() {
			var m = this.name.match(/^(.+)_(.+)$/);
			$("#frm_reservation_tarif input#opt_groupe_" + m[2]).val(this.value);
		});
		// ajouter la clsse "checked" pour les case coh�es
		$("#frm_reservation_tarif :checkbox:checked, #frm_reservation_tarif :selected[value!='0']").each(function() {
			// pour ceux qui sont dans  un tableau
			if ($(this).parent("td").length) {
				$(this).parents("tr:first").find("td.option_label, td.option_prix").addClass("checked");
			}
		});
	});
	function showPopIn(popin)
	{
		options = {dialogClass: "popin-options", draggable: false, resizable: false, width: 766, modal: true, closeText:"x"};
		if (typeof($("#dialog-options").dialog('instance')) != 'undefined') {
			$("#dialog-options").dialog('destroy');
		}
		$.post('../popin/' + popin + '.html', $("#frm_reservation_tarif").serialize(), function(data) {
			$("#dialog-options-msg").html(data);
			$("#dialog-options").dialog(options);
		}, 'html');
		sp_clic('N','popin', '<?=$track_page?>','fr_popin_' + popin);
		return false;
	}
	
	function doCheckReservationTarif (f)
	{
		var err = '', is_valid = true;
		<? if ($reservation->getAgence()->hasStatut(Agence::STATUT_LIVRAISON_SEULEMENT)) : ?>
		// v�rification livraison � domicile
		// une des 2 cases doit �tre coch�e
		la = document.getElementById('la<?=strtolower($forfait['type']);?>');
		lr = document.getElementById('lr<?=strtolower($forfait['type']);?>');
		if (la && lr && !la.checked && !lr.checked)
			err += "Vous devez s�lectionner au moins la livraison ou la reprise du v�hicule  !";
		<? endif; ?>
		// les options de franchise doivent �tre choisies si pr�sent
		if (f.group_franchise && f.group_franchise.length) {
			if (!$(f.group_franchise).filter(":checked").length) {
				$error_franchise = $("#error_groupe_franchise");
				if ($error_franchise.length) {
					$("#error_groupe_franchise").html("Veuillez nous indiquer quelle assurance vous souhaitez en cas de dommage ou vol").show();
					is_valid = false;
					if (appADA.bodyScrollTo) {
						appADA.bodyScrollTo("#error_groupe_franchise");
					}
				} else {
					err += "Veuillez nous indiquer quelle assurance vous souhaitez en cas de dommage ou vol";
				}
			}
		}
		// la date de retrait doit �tre s�lectionn�e si elle est propos�e
		if (f.options_retrait && $(".a_retirer :checkbox:checked, .a_retirer :selected[value!='0']", f).length && f.options_retrait.options.length > 1 && !f.options_retrait.value.length)
			err += "Vous devez pr�ciser la date de retrait du mat�riel !";
		if (err.length)
		{
			alert (err);
			is_valid = false;
		}
		return is_valid;
	}
	// updateTotal
	function updateTotal(box, montant, groupe)
	{
		var elt, frm = box.form;
		if (box.type=='select-one')
		{
			var prix = $(box).attr('data-prix');
			$(box).attr('data-prix', montant);
			montant = (Math.round((montant - prix)*100))/100;
		}
		else if (box.type!='hidden' && !box.checked)
			montant = -montant;
		else if (groupe)
		{
			// si un groupe on effectue le calcul pour l'option d�coch�e
			var eltGroupe = document.getElementById('opt_groupe_' + groupe);
			if (eltGroupe)
			{
				if (eltGroupe.value)
				{
					elt = document.getElementById(eltGroupe.value);
					if (elt.id == box.id) return;
					if (elt.onclick)
						elt.onclick();
				}
				eltGroupe.value = box.id;
			}
		}
		// le prix total des options		
		var prix_options = (Math.round(parseFloat(frm.prix_options.value)*100)/100) + montant;
		frm.prix_options.value = prix_options;
		// ajuster le total en fonction des options choisies
		elt = document.getElementById('prix_total');
		if (elt) 
		{
			var s = (Math.round((parseFloat(frm.prix_forfait.value) + prix_options)*100)/100);
			elt.innerHTML = s.toString().replace('.', ',').replace(/\,(\d)$/,',$10');
		}
		// visualiser l'activation
		if (box.type!='hidden') {
			var selector = "#option_" + box.id + " td.option_prix, #option_" + box.id + " td.option_label";
			if ($(box).is(":checked") || (box.type=='select-one' && box.selectedIndex > 0)) {
				$(selector).addClass("checked");
			} else {
				$(selector).removeClass("checked");
			}
		}
		// s'il faut montrer la date de retreit
		if ($(box).hasClass("a_retirer")) {
			if (frm.options_retrait) {
				if ($(".a_retirer :checkbox:checked, .a_retirer :selected[value!='0']", frm).length > 0) {
					$("#fieldset_retrait").slideDown();
				} else {
					$("#fieldset_retrait").slideUp();
				}
			}
		}
	}
// ]]>
</script>
