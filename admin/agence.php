<?
	require_once("secure.php"); 
	require_once("lib.php");
	require_once("lib.agence.php");

	// identifiant
	$id = getarg("id");
	if ($id && !Agence::isValidID($id)) {
		die("identifiant incorrect: $id");
	}

	// suppression
	if ($id && getarg("delete")) 
	{
		// supprimer les pdv, les alias et dans refnat_canon
		sqlexec("DELETE refnat_canon FROM refnat_canon JOIN agence_alias ON (agence_alias.id=refnat_canon.id AND refnat_canon.type='agence') WHERE agence_alias.agence='$id'");
		foreach(array('agence_horaire','promotion_forfait','stop_sell') as $k)
			sqlexec("DELETE $k FROM $k JOIN agence_pdv ON agence_pdv.id=$k.agence WHERE agence_pdv.agence = '$id'");
		foreach (array('agence_alias','agence_pdv','agence_dispo','agence_fermeture','agence_ouverture','agence_horaire','agence_option','promotion_forfait','agence_promo_forfait','stop_sell') as $k)
			sqlexec("DELETE FROM $k WHERE agence = '$id'");
		sqlexec("DELETE FROM agence WHERE id='$id'");
		unset($id);
	}

	// modification
	if (getarg("update") || getarg("create"))
	{
		save_record("agence", $_POST);
		agence_update_statut($id);
		// mettre � jour le statut dans agence_pdv et agence_alias
		// pour une cr�ation d'agence on initialise...
		if (getarg("create"))
		{
			// ...les disponibilit�s
			$sql = 'INSERT INTO agence_dispo (agence, categorie, type, nb)';
			$sql.=" SELECT '".$id."', id, type, 0 FROM categorie WHERE zone='".$_POST['zone']."'";
			sqlexec($sql);
		}
	}
	// r�cup�rer l'enregistrement
	if ($id)
	{
		$rs = sqlexec("SELECT * FROM agence WHERE id = '".$id."'");
		if (!$rs || !($row = mysql_fetch_assoc($rs)))
			die("Aucun enregistrement correspondant.");
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script language="javascript" src="../scripts/script.js"></script>
	<script>
	<!--

	// doDelete
	function doDelete(frm)
	{
		return (confirm("Voulez-vous r�ellement supprimer cet �l�ment ?\nCette op�ration est irr�versible."));
	}

	// doSubmit
	function doSubmit(frm)
	{
		var err = "";
		if(!frm.id.value.length)
			err += "Veuillez indiquer le code agence\n";
		if(!frm.type_vu.checked && !frm.type_vp.checked && !frm.type_2r.checked && !frm.type_sp.checked)
			err += "Veuillez indiquer au moins un type d'activit�\n";
		if(!frm.nom.value.length)
			err += "Veuillez indiquer le nom\n";
		if(!frm.societe.value.length)
			err += "Veuillez indiquer la societe\n";
		if(!frm.franchise.value.length)
			err += "Veuillez indiquer le nom du franchis�\n";
		if (!validatePhone(frm.tel))
			err += "Le num�ro de t�l�phone est incorrect\n";
		if (frm.fax && frm.fax.value.length && !validatePhone(frm.fax))
			err += "Le num�ro de fax est incorrect\n";
		if(!validateEmail(frm.email))
			err += "L'adresse email est incorrecte\n";
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		return true;
	}
	
	//-->
	</script>
</head>

<body>

<?= SQLExporter::getForm(); ?>
<form action="" method="post" onsubmit="return doSubmit(this);" enctype="multipart/form-data">
<table class="framed" border="0" cellspacing="0" cellpadding="4" style="float:left">
<tr>
	<th class="bg_titre">
	<img src="img/puce.gif" align="absmiddle" hspace="1">
<? 
	echo "Agence";
	if ($row['nom']) {
		$t = array();
		foreach(array('nom','code_region','code_base','code_groupe') as $k) {
			if ($row[$k]) {
				$t[] = $row[$k];
			}
		}
		echo ' : '.implode(' / ', $t);
		// puis les informations tarifaires
		if ($agence = new Agence($row)) {
			echo ' / Tarifs Web VP: '.$agence->getTarifsWeb('vp').', Tarifs Web VU: '.$agence->getTarifsWeb('vu');
		}
	}
?>
	</th>
</tr>

<tr>
	<td>
	<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="15%">
		<col width="35%">
		<col width="15%">
		<col width="35%">
		<tr>
			<td><label for="id">identifiant</label></td>
			<td>
		<? 
			echo $id;
			echo '<input type="'.($id ? "hidden" : "text").'" id="id" name="id" value="'.$id.'" maxlength="6">';
		?>
			</td>
			<td><label for="reseau">R�seau</label></td>
			<td>
			<?
				if ($row['reseau']) echo '<strong>'.$row['reseau'].'</strong>';
				else
				{
					write_radio($row, 'reseau', 'ADA', 'ADA', true);
					write_radio($row, 'reseau', 'CITER', 'CITER');
				}
	
			?>
			</td>
		</tr>
		<tr>
			<td>Activit�</td>
			<td>
				<input type="hidden" name="type[]" value="0">
				<?
				foreach(Agence::$VEHICULES_TYPE as $k => $v) {
					write_checkbox($row, 'type', $k, $v, strtoupper($k), Agence::$VEHICULES_TYPE_LBL[$k]);
				}
				?>
			</td>
			<td><label for="groupe">Groupe</label></td>
			<td>
			<?
				if ($row['id'])
					selectsql('groupe', "select id, nom from zone_groupe where zone='".$row['zone']."'", $row['groupe'], '-- par d�faut --'); 
				else
					echo '-- par d�faut --';
			?>
			</td>
		</tr>
		<tr>
			<td><label for="nom">Nom</label></td>
			<td><input type="text" id="nom" name="nom" value="<?=$row["nom"]?>"></td>
			<td><label for="zone">Zone</label></td>
			<td><? selectsql('zone', "SELECT id, nom FROM zone WHERE id='".$_SESSION['ADA001_ADMIN_ZONE']."'", $row["zone"], "--") ?></td>
		</tr>
		<tr>
			<td>Code / Soci�t�</td>
			<td>
				<input type="text" name="code_societe" value="<?=$row["code_societe"]?>" size="2" maxlength="2" style="width: inherit;">
				<input type="text" name="societe" value="<?=$row["societe"]?>" size="30" maxlength="64">
			</td>
			<td>Franchis�</td>
			<td><input type="text" name="franchise" value="<?=$row["franchise"]?>"></td>
		</tr>
		<tr>
			<td>Tel</td>
			<td><input type="text" name="tel" value="<?=$row['tel']?>" maxlength="16"></td>
			<td>Fax</td>
			<td><input type="text" name="fax" value="<?=$row['fax']?>" maxlength="16"></td>
		</tr>
		<tr>
			<td>Email</td>
			<td colspan="3"><input type="text" name="email" value="<?=$row["email"]?>" maxlength="96"></td>
		</tr>
		<tr>
			<td>Statut</td>
			<td colspan="3">
			<?
				write_checkbox ($row, 'statut', 'visible', Agence::STATUT_VISIBLE, 'Visible');
				write_checkbox ($row, 'statut', 'resa', Agence::STATUT_RESAWEB, 'Resa. en ligne');
				write_checkbox ($row, 'statut', 'carpentier',Agence::STATUT_ASSURANCES, 'Carpentier');
				write_checkbox ($row, 'statut', 'livr', Agence::STATUT_LIVRAISON_SEULEMENT, 'Livraison uniquement');
				echo '<br>';
				write_checkbox ($row, 'statut', 'jeunes', Agence::STATUT_JEUNES, 'Jeunes');
				write_checkbox ($row, 'statut', 'abandon', Agence::STATUT_ABANDONNISTES, 'Abandonnistes');
				write_checkbox ($row, 'statut', 'stop1erprix', Agence::STATUT_STOP_PREMIER_PRIX, 'Stop 1<sup>er</sup> Prix');
				write_checkbox ($row, 'statut', 'stopnego', Agence::STATUT_STOP_NEGO, 'Stop N�gociations');
				write_checkbox ($row, 'statut', 'tel_client',Agence::STATUT_TEL_CLIENT, 'T�l. Client');
			?>
				<input type="hidden" name="statut[]" value="0">
			</td>
		</tr>
		<tr>
			<td>Notification</td>
			<td>
			<?
				write_radio($row, 'notification', 'rpc', 'Automatique');
				write_radio($row, 'notification', 'email', 'E-mail', true);
			?>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%">
		<tr>
			<td valign="top">
		<?
			if ($id) 
			{
		?>
				<input type="button" onclick="parent.list.location.href='lists/dispo.php?agence=<?=$id?>'" value="dispo">
				<input type="button" onclick="parent.list.location.href='lists/stopsell.php?agence=<?=$id?>'" value="stop-sell">
				<input type="button" onclick="parent.list.location.href='lists/reservation.php?agence=<?=$id?>'" value="r�servations">
				<? 
					$sql_export = 'select distinct c.id, c.email, c.titre, c.prenom, c.nom, c.naissance, c.adresse, c.adresse2, c.cp, c.ville, c.pays, c.tel, c.gsm, c.promo, c.actif, c.creation, c.modification';
					$sql_export.=' from client c';
					$sql_export.=' join reservation r on c.id=r.client_id';
					$sql_export.=" where r.agence='%s'";
  					$sql_export.=" and year(r.paiement) >= '%ld'";
  					$sql_export.=" and c.promo='O' and c.actif=1";
  					
  					$sql_export = sprintf($sql_export, $id, 2010);
				?>
				<?= SQLExporter::getButton('export optins', 'client_optin_'.$id, $sql_export); ?>
				<br>
				<input type="button" onclick="parent.list.location.href='lists/fermeture.php?agence=<?=$id?>'" value="fermetures">
				<input type="button" onclick="parent.list.location.href='lists/ouverture.php?agence=<?=$id?>'" value="exceptions">
				<input type="button" onclick="window.location.href='agence_option.php?agence=<?=$id?>'" value="options">
		<?
			}
		?>
			</td>
			<td align="right" valign="top">
				<input type="submit" name="<?=$id ? "update" : "create"?>" value="enregistrer">
			<?
			if ($id && !getsql("SELECT id FROM reservation WHERE agence='".$id."' LIMIT 1")) 
			{
			?>
				&nbsp;
				<input type="submit" onclick="return doDelete(this.form);" name="delete" value="supprimer">
			<?
			}
			?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<? show_pdv($id); ?>
</form>
</body>
</html>
