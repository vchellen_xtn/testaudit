<?
	define("DEBUG", 0);
	require_once ("../secure.php");
	require_once ("../../lib/lib.sql.php");

	// page et taille de page
	$nb_rows = (int) $_GET["nb_rows"];
	if (!$nb_rows) $nb_rows = 20;

	$from_row = (int) $_GET["from_row"];
	if (!($from_row > 0)) $from_row = 1;

	$sql = $_GET['sql'];
	if (!$sql || !preg_match('/^select\s/i', $sql)) {
		die("Vous devez entrer une commande SQL");
	}
	$sql = stripslashes($sql);

	// ligne de totaux ?
	$has_total = ($_GET["total"] != "");

	// autres param�tres
	$sort  = preg_replace("/^(?!case|coalesce|year)(.+\.)*([^`][^\.]+)$/", "$1`$2`", $_GET["key"]);
	
	$order = $_GET["order"];
	$url   = $_GET["url"];
	$color = $_GET["color"];
	if (!$color) 
		$color = "FCEFBA";
	header("Content-Type: text/html;charset=iso-8859-1");
?>
<html>
<head>
<title>Liste</title>
<meta http-equiv="Content-Type" value="text/html;charset=iso-8859-1">
<link rel="stylesheet" href="../style.css">
<script language="javascript">
	<!-- hide from non-scriptable browsers
	function goEdit(a)
	{
		var w = (window.parent.edit ? window.parent.edit : window);
		w.location.href = a.href ? a.href : a;
		return false;
	}
	
	// doSort
	function doSort(key, order)
	{
		var frm = document.forms['frm_list'];
		frm.key.value = key;
		frm.order.value = order;
		frm.submit();
	}
	
	// doDisplay
	function doDisplay(first)
	{
		var frm = document.forms['frm_list'];
		frm.from_row.value = first;
		frm.submit();
	}
	
	// showAll
	function showAll()
	{
		document.forms['frm_list'].nb_rows.value = "-1";
		document.forms['frm_list'].submit();		
	}
	
	function doSearchChange(fld)
	{
		fld.form.submit();
	}
	
	// end hiding -->
</script>
<style type="text/css">
	a{ color:#000; }
</style>
</head>
<body bgcolor="FFFFFF" link="000000" vlink="000000" alink="000000">

<?= SQLExporter::getForm('export.php'); ?>
<form id="frm_list" name="frm_list" action="list.php" method="get">
<input type="hidden" name="sql" value="<?=$sql ?>">
<input type="hidden" name="key" value="<?=$sort ?>">
<input type="hidden" name="order" value="<?=$order ?>">
<input type="hidden" name="from_row" value="<?=$from_row ?>">
<input type="hidden" name="title" value="<?=$_GET["title"] ?>">
<input type="hidden" name="url" value="<?=$_GET["url"] ?>">
<input type="hidden" name="color" value="<?=$color ?>">
<input type="hidden" name="new_url" value="<?=$_GET["new_url"] ?>">
<input type="hidden" name="pk" value="<?=$_GET["pk"] ?>">


<table class="framed" cellspacing="0" cellpadding="0">
<? 
	$title = $_GET["title"];
	
	if ($title != "") 
	{
?>
<tr class="bg_titre">
	<td>
		<table border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr>
				<th colspan="2"><?=$title?></th>
				<th width="10" align="right"><a title="mise � jour" href="javascript:location.reload();"><img border="0" src="../img/reload.gif"></a></th>
				<th width="10" align="right"><a title="retour page pr�c�dente" href="javascript:history.back();"><img border="0" src="../img/back.gif"></a></th>
			</tr>
		</table>
	</td>
</tr>
<? 
	} 
?>
<tr>
	<th style="padding:0">
		<table border="0" cellspacing="0" cellpadding="3" width="100%">
		<tr style="background: url(../img/bg_list.gif) repeat-x 0% 100%;">
<? 
	// colonnes de la requete
	$query = parse_query($sql);
	$cols = $query['cols'];
// print_r($cols);
	foreach ($_GET as $key => $val)
	{
		if (trim($val) && substr($key, 0, 5) == "srch_")
		{
			$has_search = true;
			$sql.= (strstr($sql, " where ") ? " and " : " where ");
			$sql.= "(".$cols[substr($key, 5)].") like '".str_replace('*', '%', addslashes(trim($val)))."%'";
		}
	}
	
 	// ordre de tri
	if ($sort != "") 
		$sql = $sql." order by (".$sort.") ".$order;
// echo $sql;
	$rs = sqlexec($sql);
	if ($rs && $row = mysql_fetch_array($rs, MYSQL_ASSOC))
	{
		foreach ($row as $fld => $val)
		{
			if (!strchr($fld, "_"))
			{
				echo '<td';
				if (is_numeric($val)) echo ' align="right"';
				echo ' nowrap><a href="javascript:doSort(\''.addslashes($cols[$fld]).'\',\'';
				$is_sorted = (str_replace('`', '', $cols[$fld]) == str_replace('`', '', $sort));
				if ($is_sorted && $order == "") 
					echo 'desc';
				echo '\');"';
				if ($is_sorted) echo ' class="hilite"';
				echo '><b>'.$fld;
				if ($is_sorted) 
				{
					echo '<img align="absmiddle" src="../img/';
					echo ($order == "") ? 'down' : 'up';
					echo '.gif" border="0">';
				}
				echo '</a></td>'."\n";
			}
		}
		echo "</tr>\n";
		
		// recherche
		echo '<tr class="search">'."\n";
		foreach ($cols as $fld => $val)
		{
			echo '<td><input type="text" class="search" name="srch_'.$fld.'" value="'.$_GET["srch_".$fld].'" onchange="doSearchChange(this)"></td>'."\n";
		}
		
		$num = 0;
		$total = array();
	} 
	else
	{
		echo '<td width="100%" align="center">Il n\'y a pas de r�sultat';
		if ($has_search)
			echo '<input type="button" style="margin: 2px 0px 0px 6px" value="retour" onclick="history.back()">';
		echo '</td>';
	}

	echo "</tr>\n";

	// en cas de cl�s primaires multiples
	if ($_GET['pk']) $pk = explode(',',$_GET['pk']);

	// on affiche les donn�es
	if ($rs && (mysql_num_rows($rs) > 0) && mysql_data_seek ($rs, $from_row - 1))
	{
		while (($row = mysql_fetch_array($rs, MYSQL_ASSOC)) && ($nb_rows < 0 || $num < $nb_rows))
		{
			$num = $num + 1;
			echo '<tr bgcolor="#'.(($num % 2 == 0) ? "e8e8e8" : "ffffff").'">'."\n";

			$cond = null;
			foreach ($row as $fld => $val)
			{
				if (!$cond || ($pk && in_array($fld, $pk)))
				{
					$cond_fld = $fld; //$cols[$fld];
					if ($p = strpos($cond_fld, "."))
						$cond_fld = substr($cond_fld, $p + 1);
					if ($cond) $cond .= '&';
					$cond.= $cond_fld;
					$cond.= "=".urlencode($val);
				}
				if (!strchr($fld, "_"))
				{
					echo '<td height="12" valign="top"';
					if (is_numeric($val))
					{
						echo ' align="right"';
						$total[$fld] += $val;
						echo '>'.$val.'&nbsp;</td>'."\n";
					}
					else
					{
						$total[$fld] = "&nbsp;";
						echo '><a onclick="return goEdit(this);" href="'.$url.'?'.$cond.'">'.$val;
						echo '&nbsp;</a></td>'."\n";
					}
				}
			}
			echo '</tr>'."\n";
		}
	}	
	
	// Pr�sence d'une ligne de totaux
	if ($has_total && count($total) > 0)
	{
		echo ('<tr class="total">'."\n");
		foreach ($total as $fld => $val)
		{
			if (!strchr($fld, "_"))
				echo ("<td align=\"right\"><b>".$val."</td>\n");
		}
		echo ("</tr>\n");
	}
?>
		</table>
		</th>
	</tr>
	</table>
	
	<table width="700" cellspacing="0" cellpadding="0" border="0">
	<tr style="background:url(../img/bg_footer.gif) no-repeat; height:39px;">
		<td>
			<table height="20" width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr>
					<td width="60">
					<? if ($from_row > 1) { ?>
						<a title="page pr�c�dente" href="javascript:doDisplay(<?=$from_row-$nb_rows?>)">
						<img src="../img/left.gif" border="0"></a>
					<? } ?>
					</td>
					<td align="center" style="font-size:10px" >&nbsp;
					<? if ($from_row > 1 || $row) { ?>
						 lignes par page : <input maxlength="2" style="width:20px; text-align:center; font-size:10px" type="text" name="nb_rows" value="<?=$nb_rows?>" onchange="this.form.from_row.value=''; this.form.submit();">
						 <a href="javascript:showAll();"><b>Tous</a>
					<? } ?>
					</td>
					<td width="60" align="right">
					<? if ($row) { ?>
						<a title="page suivante" href="javascript:doDisplay(<?=$from_row+$nb_rows?>)">
						<img src="../img/right.gif" border="0"></a>
					<? } ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<table width="700" cellspacing="0" cellpadding="0" border="0">
<tr>
	<td width="100%" align="right">
		<?= SQLExporter::getButton('exporter', $_GET["title"], $sql, false); ?>
<? 
	$url = $_GET["new_url"];
	if ($url) 
	{ 
?>
		<input onclick="goEdit('<?=str_replace("'", "\'", $url) ?>');" type="button" value="ajouter">
<? 	
	} 
?>
	</td>
</tr>
</table>

</form>
</body>
</html>

<?
	@mysql_free_result($rs);
?>
