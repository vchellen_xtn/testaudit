<? 
if ( is_dir(dirname(__file__).'/'.dirname($this->getId())) ) : ?>
	<style type="text/css">
		.content_bottom{width:964px; background:#FFF; margin:0 auto; padding:0px; font-family:'Raleway', Arial; font-size:14px;}
	</style>
		<div class="bg-new">
			<div class="content">
				<div id="rubrique">
				<?
					// répertoire spécifique
					$d = dirname(__file__).'/'.dirname($this->getId());
					if (is_file($f = $d.'/menu.php'))
						include ($f);
					$this->showBannieres('left');
				?>
				</div>
				
				<div id="page">
				<?
					include($this->getPagePath());
				?>
				</div>
				
				<div class="clear"></div>
			</div>
		</div>
<? else :
	// pour tous les fichiers .php on prend celui de /v2/
	include ($this->getSkinnedPagePath('v2'));
endif;