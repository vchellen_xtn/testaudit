<?
	session_start();
	include ('../secure.php');
	if (!$_SESSION['ADA001_ADMIN_ZONE'])
		die("Vous devez choisir une zone de travail !");
	$title 	 = "Promotions Forfait";
	$url 	 = "../agence_promo_forfait.php";
	$key	 = "p.id";
	$order	 = "";

	$sql = "select p.id, ";
	$sql.= "	   case p.actif when 1 then 'oui' else 'non' end as actif,";
	$sql.= "	   case when p.classe is null then 'Tous' else 'R�serv�' end as classe,";
	$sql.= "	   coalesce(p.reseau, 'Tous') as reseau,";
	$sql.= "	   coalesce(zg.nom, 'Tous') as groupe,";
	$sql.= "	   coalesce(p.code_societe, 'Tous') as code,";
	$sql.= "	   coalesce(p.agence, 'Toutes') as agence, ";
	$sql.= "	   coalesce(ucase(p.type), 'Tous') as type, ";
	$sql.= "	   p.categories, ";
	$sql.= "	   p.forfait,";
	$sql.= "	   p.debut, p.fin, p.nom";
	$sql.= "  from agence_promo_forfait p";
	$sql.=" left join zone_groupe zg on zg.id=p.groupe";
	$sql.= " where p.zone='".$_SESSION['ADA001_ADMIN_ZONE']."' and p.perimetre='ADAFR'";
	
	if ($_GET['agence'])
	{
		$a = mysql_fetch_assoc(sqlexec("select ap.id, ap.agence, a.code_societe, a.groupe, a.reseau from agence a join agence_pdv ap on ap.agence=a.id where ap.id='".filter_var($_GET['agence'], FILTER_SANITIZE_STRING)."'"));
		$sql.= " and (p.agence is null or p.agence='".$a['id']."')";
		if ($a['code_societe'])
			$sql.= " and (p.code_societe is null or p.code_societe='".$a['code_societe']."')";
		if ($a['groupe'])
			$sql.= " and (p.groupe is null or p.groupe='".$a['groupe']."')";
		$sql.= " and (p.reseau is null or p.reseau='".$a['reseau']."')";
	}
	if ($_GET['type'] && Categorie::$TYPE[$_GET['type']])
		$sql.= " and (p.type is null or p.type='".addslashes($_GET['type'])."'";
	if ($_GET['categorie']) {
		if ($categorie = Categorie::factory((int)$_GET['categorie']))
			$sql.= ' and (p.categories is null or FIND_IN_SET("'.$categorie['mnem'].'", p.categories)';
	}
	if ($_GET['forfait'])
		$sql.= sprintf(" and (p.forfait is null or p.forfait='%s')", filter_var($_GET['forfait'], FILTER_SANITIZE_STRING));
	if ($_GET['classe'])
		$sql.= " and p.classe=".(int) $_GET['classe'];
	
	$out_url = "list.php?sql=" . urlencode($sql);
	$out_url.= "&title=" . urlencode($title);
	$out_url.= "&url=" . urlencode($url);
	$out_url.= "&new_url=" . urlencode($url . "?" . $_SERVER['QUERY_STRING']);
	$out_url.= "&color=4587ba";
	$out_url.= "&key=".$key."&order=".$order."&nb_rows=30";
	header("location: ".$out_url);
?>
