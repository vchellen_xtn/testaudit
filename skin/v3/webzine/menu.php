<?
	$menu = array(
		'les-plus-belles-plages-de-france-mediterranee'		=> 'Les plus belles plages de France (Mer M�diterran�e)',
		'les-plus-belles-plages-de-france-atlantique'	=> 'Les plus belles plages de France (C�te Atlantique)',
		'les-plus-belles-plages-de-france-manche'	=> 'Les plus belles plages de France (Manche)',
		'les-3-meilleures-destinations-pour-faire-du-ski-en-france'		=> 'Les 3 meilleures destinations pour faire du ski en France',
		'decouvrez-les-stations-de-ski-proches-de-toulouse'		=> 'D�couvrez les stations de ski proches de Toulouse',
		'decouvrir-les-legendes-et-les-splendeurs-de-la-bretagne-en-voiture'	=> 'D�couvrir les l�gendes et les splendeurs de la Bretagne en voiture',
		'prenez-le-volant-pour-visiter-les-environs-de-la-rochelle'		=> 'Prenez le volant pour visiter les environs de la Rochelle !',
		'location-de-voiture-pour-les-jeunes-conducteurs'		=> 'Location de voiture pour les jeunes conducteurs',
		'les-3-meilleures-destinations-de-france-a-petit-budget'		=> 'Les 3 meilleures destinations de France � petit budget',
		'louer-sa-voiture-de-replacement-pour-aller-au-travail'		=> 'Louer sa voiture de replacement pour aller au travail',
		'infos-pratiques-pour-utiliser-au-mieux-le-site-ada'		=> 'Infos pratiques pour utiliser au mieux le site Ada.fr',
		'comment-choisir-la-bonne-categorie-de-voiture'		=> 'Comment choisir la bonne cat�gorie de voiture ?',
		'road-trip-sur-les-plus-belles-routes-de-france'		=> 'Road trip sur les plus belles routes de France',
		'louer-un-utilitaire-en-aller-simple'		=> 'Louer un utilitaire en aller simple, la meilleure solution pratique et �conomique',
		'attitudes-interdites-au-volant-en-2015'		=> 'Attitudes interdites au volant en 2015 ! Il n�y a pas que le t�l�phone qui peut poser probl�me...',
		'en-voiture-de-location-avec-son-bebe'		=> 'En voiture de location avec son b�b�, quelles pr�cautions prendre ?',
		'faire-la-route-des-vins-d-alsace'		=> 'Faire la Route des vins d\'Alsace',
		'nos-conseils-pour-bien-louer-un-utilitaire'		=> 'Nos conseils pour bien louer un utilitaire',
		'nos-astuces-a-suivre-pour-les-longs-trajets-en-voiture-cet-ete'		=> 'Nos astuces � suivre pour les longs trajets en voiture cet �t�',
		'comment-reduire-votre-temps-d-attente-au-guichet-de-nos-agences'		=> 'Comment r�duire votre temps d\'attente au guichet de nos agences ADA ?',
		'que-faire-lorsqu-on-me-vole-ma-voiture-de-location'		=> 'Que faire lorsqu\'on me vole ma voiture de location ?',
		'est-il-difficile-de-louer-une-voiture-lorsqu-on-a-plus-de-70-ans'		=> 'Est-il difficile de louer une voiture lorsqu\'on a plus de 70 ans ?',
		'j-ai-perdu-les-clefs-de-ma-voiture'		=> 'J\'ai perdu les clefs de ma voiture de location que faire ?',
		'pourquoi-reserver-une-categorie-de-vehicule'		=> 'Pourquoi r�server une cat�gorie de v�hicule et non pas un mod�le bien pr�cis ?',
	);
	$action = basename($page);
?>
<ul class="menu type-li-4">
	<? foreach($menu as $k => $lbl) : ?>
		<li <?if ($action==$k) echo ' class="current"'; ?>>
			<a href="<?=$k?>.html"<?if ($action==$k) echo ' class="current"'; ?>><?=$lbl;?></a>
		</li>
	<? endforeach; ?>
</ul>