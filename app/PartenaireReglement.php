<?
class PartenaireReglement extends Ada_Object {
	
	/**
	* Cr�e un r�gelemnt
	* 
	* @param PartenaireCoupon $coupon
	* @param PartenaireTarif $tarif
	* @return PartenaireReglement
	*/
	static public function create(PartenaireCoupon $coupon, PartenaireTarif $tarif)
	{
		$data = array (
			'id'		=> '',
			'coupon'	=> $coupon['id'],
			'duree'		=> $tarif['duree'],
			'prix_ht'	=> $tarif['prix_ht']
		);
		$x = new PartenaireReglement($data);
		$x->save();
		$x->setId(mysql_insert_id());
		return $x;
	}
	/**
	* Renvoie le dernier r�glement associ�e � un coupon
	* 
	* @param int $couponID
	* @return PartenaireReglement
	*/
	static public function factory($couponID)
	{
		$x = new PartenaireReglement();
		if ($x->load($couponID)->isEmpty())
			return null;
		return $x;
	}
	/**
	* Renvoie le SQL pour retrouver le r�glement le plus r�cent
	* 
	* @param mixed $couponID
	* @param mixed $cols
	* @param mixed $data
	*/
	protected function _prepareSQL($couponID, $cols='*', $data=null)
	{
		$sql = 'select *';
		$sql.=' from '.$this->getTableName();
		$sql.=' where coupon='.$couponID;
		$sql.=' order by creation desc limit 1';
		return $sql;
	}
	/**
	* Indique que le r�gelemnt est pay�
	* 
	* @param Reservation $reservation
	* @returns bool
	*/
	public function isPaidBy(Reservation $reservation)
	{
		if (!$this->_data['reglement'] && !$this->_data['reservation'])
		{
			$this->setData('reservation', $reservation->getId())
				->setData('reglement', date('Y-m-d'))
				->save();
			return true;
		}
		return false;
	}
	/**
	* Renvoie la r�servation ayant pay� le r�gleemnt
	* @return Reservation
	*/
	public function getReservation()
	{
		if ($this->_data['reservation'])
		{
			return Reservation::factory($this->_data['reservation']);
		}
		return null;
	}
	public function getPrixHT() 
	{
		return $this->_data['prix_ht'];
	}
	public function getPrixTTC() 
	{
		return round((1+Tva::getRate(strtotime($this->getData('reglement'))))*$this->getPrixHT(), 2);
	}

};
?>
