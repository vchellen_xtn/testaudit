<?

class ResAnnulationMotif extends Ada_Object
{
	protected $_adminUser = null;
	protected $_sendEmail = true;
	protected $_adminMontant = array();
	/** @var PromotionCampagne */
	protected $_promotionCampagne = null;
	/** @var PromotionCode */
	protected $_promotionCode = null;
	
	/**
	* Cr�e un objet � partir d'un identifiant (optionnel)
	* et permet de passer les param�tres de l'annulation
	* 
	* @param string $id
	* @param bool $sendMail - un mail doit-il �tre envoy�
	* $param PromotionCampagne $campagne campagne pour cr�er le code
	* @return ResAnnulationMotif
	*/
	static public function factory($id = null, $sendEmail = true, PromotionCampagne $campagne = null)
	{
		$x = new ResAnnulationMotif();
		if ($id)
			$x->load($id);
		$x->_sendEmail = $sendEmail;
		$x->_promotionCampagne = $campagne;
		return $x;
	}
	public function __toString()
	{
		if ($x = $this->_data['libelle'])
			return $x;
		return parent::__toString();
	}
	
	/**
	* L'annulation est-elle un remboursement partiel ?
	* @returns bool
	*/
	public function isPartiel()
	{
		return ($this->_data['partiel'] > 0);
	}
	/**
	* Renvoie un tableau contenant les montant � rembouser fix� lors de l'annulation dans l'admin
	* 	ada		xxxx (�)
	*   courage xxxx (�)
	* 
	* @returns array
	*/
	public function getAdminMontant()
	{
		return $this->_adminMontant;
	}
	public function mustSendEmail()
	{
		return $this->_sendEmail;
	}
	/**
	* Renvoie le code de r�duction associ�e au motif d'annulation
	* @param Reservation $reservation
	* @returns PromotionCodde
	*/
	public function getPromotionCode(Reservation $reservation = null)
	{
		if (!$this->_promotionCode && $this->_promotionCampagne && $this->_adminUser && $reservation)
		{
			$this->_promotionCode = $this->_promotionCampagne->getOneCode($this->_adminUser, $reservation->getClient(), $reservation);
		}
		return $this->_promotionCode;
	}
	public function isAdmin()
	{
		return ($this->_adminUser != null);
	}
	
	/**
	* Renseigne les montants � rembourser
	* 
	* @param string $facturation
	* @param string $montant
	* @returns ResAnnulationMotif
	*/
	public function setAdminMontant($facturation, $montant)
	{
		if ($montant)
			$this->_adminMontant[$facturation] = $montant;
		return $this;
	}
	public function setAdminUser($login)
	{
		$this->_adminUser = $login;
		return $this;
	}
}
?>