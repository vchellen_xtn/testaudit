<?php
/* Inclus depuis agences/liste et agences/index */
	/** @var Page_Agences */
	$that = $this;

	// recherche m�moris�e avec priorit� au GET, et le valider en passant par la r�servation
	$search = Reservation::createSearchFromCookieAndGet();
	// contr�le sp�cifique � l'agence
	if (!$search['type'] || ($search['type'] && $agence && !$agence->hasVehiculeType($search['type'])))
		$search['type'] = 'vp';
	// forcer les informations sur l'agence
	if (!is_null($agence)) {
		// contr�le sp�cifique � l'agence
		list($search['agence'], $search['zone']) = array($agence['id'], $agence['zone']);
	} else {
		// si la recherche est g�olocalis�e, on choisit la premi�re agence
		if (!is_null($agences) && is_object($agences) && $agences->showCenter()) {
			$ids = $agences->getIds();
			$search['agence'] = reset($ids);
		}
		// si une agence est indiqu�e et qu'elle est dans la liste on la s�lectionne
		if ($search['agence']) {
			if (!is_null($agences) && isset($agences[$search['agence']])) {
				$agence = $agences[$search['agence']];
				$search['zone'] = $agence['zone'];
				$agence = null;
			} else {
				$search['zone'] = 'fr';
				$search['agence'] = '';
			}
		}
	}
?>
<!-- formulaire de r�servation -->
<form method="post" action="<?=Page::getURL('reservation/resultat.html')?>" name="form_res_agence" id="form_res_agence" class="clearfix">
	<input type="hidden" name="age" value="<?=key($AGE);?>"/>
	<input type="hidden" name="permis" value="<?=key($PERMIS);?>"/>

	<? if (!is_null($agence) && $agence['zone']!='fr' && $agence['zone']!='co') : /* que des VP ailleurs qu'en FR */ ?>
	<input type="hidden" name="type" id="type" value="vp"/>
	<? else : ?>
	<div id="familles" class="align-center form-inline form-group">
		<? foreach(Categorie::$TYPE as $k => $v) : ?>
		<? if ((!is_null($agence) && !$agence->hasVehiculeType($k)) || (is_null($agence) && $k == 'sp')) continue; /* afficher soit les types de l'agence, soit toos les types sauf sans permis */?>
		<div class="inblock relative">
			<input type="radio" name="type" id="radio_<?=$k?>" value="<?=$k?>" <? if ($k=='vp') echo ' checked="checked"';?> class="radio-custom" />
			<label for="radio_<?=$k?>"><?=$v?></label>
		</div>
		<? endforeach; ?>
	</div>
	<? endif; ?>
	<div class="form-mixed">
		<div class="form-group select-mixed">
			<label for="categorie">Choisissez votre cat�gorie :</label>
			<?
				$collection = VehiculeFamille_Collection::factory('fr', 'vp');
				selectarray('categorie', $collection, $collection->getDefaultID(), '--- Choisissez un v�hicule ---');
				// pr�parer le menu des cat�gories
				$mnu = array();
				foreach($collection as $x) {
					$mnu['fr-vp'][] = $x['id'].'-'.$x['nom'];
				}
				// puis les autres cat�gories pour FR
				foreach(array('vu','ml','sp') as $type) {
					foreach(Categorie_Collection::factory('fr', $type) as $x) {
						$mnu['fr-'.$type][] = $x['id'].'-'.$x['nom'];
					}
				}
			?>
		</div>
		<? if (is_null($agences)) : ?>
		<input type="hidden" id="agence" name="agence" value="<?=$agence['id']?>"/>
		<? else : ?>
		<div class="form-group select-mixed">
			<label for="agence">Choisissez votre agence :</label>
			<? selectarray('agence', $agences, $search['agence'], '-- Choisissez une agence --'); ?>
		</div>
		<? endif; ?>
	</div>

	<div id="calendar">
		<div class="select-inline form-group clearfix">
			<div class="col-60">
				<label for="depart">D�part :</label>
				<input type="text" name="depart" id="depart" value="<?=$search['depart'];?>" />
			</div>
			<div class="col-40">
				<span id="span_h_depart" <?=($search['type']=='sp' ? 'style="display:none;"' : '');?>>
					<label for="h_depart" class="align-right">�</label>
					<select name="h_depart" id="h_depart"><option value="">--</option></select>
				</span>
			</div>
		</div>
		<div class="select-inline form-group clearfix">
			<div class="col-60">
				<label for="retour">Retour :</label>
				<span id="span_ml_retour"<? if ($type!='ml') echo ' style="display: none; "';?>>le m�me jour</span>
				<input type="text" name="retour" id="retour" value="<?=$search['retour']?>" />
			</div>
			<div class="col-40">
				<span id="span_h_retour" <?=($search['type']=='sp' ? 'style="display:none;"' : '');?>>
					<label for="h_retour" class="align-right">�</label>
					<select name="h_retour" id="h_retour"><option value="">--</option></select>
				</span>
			</div>
		</div>

		<p id="div_sp_info" class="form-note" style="<?=($search['type']!='sp' ? 'display:none;' : 'clear:both;');?>">
			Vous pouvez r�server un v�hicule sans permis pour une p�riode de 7 jours � un mois. Au del� d'un mois, le renouvellement de la location se fait directement en agence.
		</p>
	</div>
	<div class="clearfix submit-mixed">
		<input type="submit" name="valider" class="btn-default right big" value="R�server" />
		<a href="<?=$here?>agences/index.html" class="left link-return">Retour</a>
	</div>
</form>
<script type="text/javascript">
// <![CDATA[
<?
	// le menu JS des cat�gories
	echo "\tmnuCategory = new Array();\n";
	foreach ($mnu as $k => $v)
		echo "\t".'mnuCategory["'.$k.'"] = new Array("'.implode('","', $v).'");'."\n";
?>
	// formulaire de r�servation
	var frmReservation = null;
	$(document).ready(function()
	{
		// initialiser le formulaire de r�servation
		frmReservation = new formReservation(document.getElementById("form_res_agence"), <?=json_encode($search);?>, {"notification_date": false});
		$("#familles input:radio").click(function() {
			frmReservation.doType(this.value);
		});
		$("#agence").change(function() {
			frmReservation.doAgence(this.value);
		});
	});
// ]]>
</script>
