<?
/**
* Option
*/
class Option extends Ada_Object
{
	static public $MODE_PAIEMENT = array('L' => 'En ligne', 'A' => 'En agence', 'N'=>'Non pr�sent�');
	
	/**
	* V�rifie l'existence d'options dans un th�e donn� pour une zone et un typ�
	* 
	* @param string $zone
	* @param string $type
	* @param string $theme
	* @returns bool
	*/
	static public function doesExsits($zone, $type, $theme) {
		if (!preg_match('/^[a-z]{2}$/i', $zone) || !preg_match('/^[a-z]{2}$/i', $type) || !preg_match('/^[a-z]+$/i', $theme)) {
			return false;
		}
		$cnt = getsql(sprintf("select count(distinct(o.id)) nb from `option` o join option_tarif t on t.`option`=o.id where o.theme = '%s' and o.type='%s' and t.zone='%s'", $theme, $type, $zone));
		return ($cnt > 0);
	}
	
	public function __toString()
	{
		return $this->_data['nom'];
	}
	
	public function getPrix()
	{
		$prix = $this->_data['prix'];
		if ($this->_data['paiement'] == 'A')
			return 'en agence';
		else if ($prix > 0)
		{
			if ($this->_data['tarification'] == 'J' && $this->_data['prix_jour'] > 0)
				return show_money($this->_data['prix_jour']).' �/j';
			else if ($this->_data['tarification'] == 'U')
				return show_money($prix).'�/u';
			else
				return show_money($prix).' �';
		}
		else
			return 'Inclus';
	}
	
	/** @returns string champ Input pour l'Option */
	public function toInput()
	{
		$optionID = $this->getId();
		$classes = array();
		if ($this->_data['a_retirer'])
			$classes[] = 'a_retirer';
		$classes[] = $this->_data['facturation'];
		$max = $this->_data['quantite_max'];
		if ($max > 1)
		{
			$input  = '<select autocomplete="off" class="'.join(' ', $classes).'" id="'.$optionID.'" name="options_u['.$optionID.']" style="width: inherit;" data-prix="'.round($this->_data['prix']*(int)$this->_data['init'], 2).'"  onchange="updateTotal(this, '.$this->_data['prix'].'*parseInt(this.value), null);">';
			$input.='<option value="0">--</option>';
			for($i=1; $i <= $max; $i++)
			{
				$input.='<option';
				if ($i == (int) $this->_data['init'])
					$input.=' selected="selected"';
				$input.=' value="'.$i.'">'.$i.'</option>';
			}
			$input.='</select>';
		}
		else
		{
			$input = '<input autocomplete="off" class="'.join(' ', $classes).'" id="'.$optionID.'" value="'.$optionID.'"';
			if ($this->_data['init'])
				$input.=' checked="checked"';
			if ($this->_data['groupe'] && $this->_data['theme']!='voiturier')
				$input.= ' type="radio" name="group_'.$this->_data['groupe'].'" onclick="updateTotal(this, '.$this->_data['prix'].',\''.$this->_data['groupe'].'\');"';
			else
			{
				if ($this->_data['prix'] > 0 || $this->_data['paiement']=='A')
					$input.= ' type="checkbox" name="options[]" onclick="updateTotal(this, '.$this->_data['prix'].', null);"';
				else
				{
					$input = '&#10003;'.$input;
					$input.= ' type="hidden" name="options[]"';
				}
			}
			$input.= '/>';
		}
		//$input.= '<input type="hidden" name="px_opt_'.$optionID.'" value="'.$this->_data['prix'].'"/>';
		return $input;
	}
}
?>
