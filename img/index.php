<?
	// param�tres
	if (!($color = $_GET['c']))
		$color = "000000";

	if (!($size = $_GET['s']))
		$size = 16;

	if (!($text = $_GET['t']))
		$text = "Hello world";

	if (!($width = $_GET['w']))
		$width = -1;
	// ajustements entre version de FreeType :
	// local : 2.1.9 , pprod : 2.3.7
	if ($_SERVER['SERVER_NAME'] != 'localhost')
	{
		define('START_Y', -2);
		$size = round($size * 0.9375);
	}
    else
    {
        define('START_Y', 0);
    }
	define('START_X', 0);


	// chercher d'abord dans le cache
	header("Content-type: image/png");
	header('Last-modified: '.date('r'));
	header('Cache-Control: max-age=604800');
	header('Expires: '.date('r', strtotime('+1 week', time())));

	$dir = dirname(dirname(__file__)).'/cache/img';
	if (!is_dir($dir))
		mkdir($dir);
	$fname = $dir.'/'.md5($color.$size.$text).".png";
	if (is_file($fname))
	{
		// pour tirer parti du cache client (d�sactiv� en local..)
		$t = filemtime($fname);
		if ($_SERVER['SERVER_NAME'] != 'localhost' && array_key_exists('HTTP_IF_MODIFIED_SINCE', $_SERVER))
		{
			$ims = strtotime(preg_replace('/;.*$/', '', $_SERVER['HTTP_IF_MODIFIED_SINCE']));
			if ($ims + 600 >= time() && $ims > $t)
				die(header($_SERVER['SERVER_PROTOCOL'].' 304 Not Modified'));
		}
		// renvoyer le contenu
		die(file_get_contents($fname));
	}
	define("ALLER",  "fonts/Aller_Bd.ttf");
	define("ARIAL", "./fonts/arial.ttf");
	define("USE_FONT", ALLER);
	define("USE_FONT_BOLD", ALLER);

	// calculer la taille du texte
	list ($w,) = gettextsize(strip_tags($text), $size, USE_FONT_BOLD);
	list (,$h) = gettextsize("pb", $size);
	list (,$y) = gettextsize("b", $size);
	$h++;

	if(!function_exists('imageantialias'))
	{
		// doubler les tailles pour l'antialiasing
		$size = $size*2;
		$width = $width*2;
		$w = $w*2;
		$h = $h*2;
		$y = $y*2;
	}

	// calculer le nombre de lignes
	$lines[] = $text;
	if ($width > 0 && $w > $width)
	{
		$lines = getlines($text, $size, $width);
		$w = $width;
	}

	// cr�e une image 32-bits
	$img = imagecreatetruecolor($w + 2, count($lines) * $h + 1);
	imagesavealpha($img, true);
	if(function_exists('imageantialias'))
		imageantialias($img, true);

	// .. sur fond de couleur indiqu�e (=> transparent)
	$hbg = imagecolorallocatealpha($img, 200, 200, 200, 127);
	imagecolortransparent($img, $hbg);
	imagefill($img, 1, 1, $hbg);

	// la couleur
	list($r, $g, $b) = sscanf($color, "%2s%2s%2s");
	$hcol = imagecolorallocate($img, hexdec($r), hexdec($g), hexdec($b));

	// �crire le texte
	for($i=0; $i<count($lines); $i++)
		writeline($img, $size, $y + ($h * $i), $hcol, $lines[$i]);

	if(!function_exists('imageantialias'))
	{
		// cr�er l'image finale, taille r�elle
		$ioW = $w/2 + 2;
		$ioH = count($lines) * $h/2 + 1;
		$imgOutput = imagecreatetruecolor($ioW, $ioH);
		imagesavealpha($imgOutput, true);
		$hbg2 = imagecolorallocatealpha($imgOutput, 200, 200, 200, 127);
		imagecolortransparent($imgOutput, $hbg2);
		imagefill($imgOutput, 1, 1, $hbg2);
		// copier la grande dans la normale
		imagecopyresampled($imgOutput,$img,0,0,0,0,$ioW,$ioH,$w + 2,count($lines) * $h + 1);
		imagedestroy($img);
		$img = $imgOutput;
	}
	// et afficher l'image
	imagepng($img, $fname);
	imagepng($img);
	imagedestroy($img);

	// gettextsize
	function gettextsize($text, $size, $font=USE_FONT)
	{
		list($x1,,$x2,$y2,,$y1,,) = imagettfbbox ($size, 0, $font, $text);
		return array($x2 - $x1, $y2 - $y1);
	}

	// getlines
	function getlines($text, $size, $width)
	{
		// suppr les attributs des tags
		$text = preg_replace("/<([a-z][a-z0-9]*)[^>]*>([^<]+)<\/\\1>/", '<\\1>\\2</\\1>', $text);
		// match les elements entre tags
		preg_match_all("/<([a-z][a-z0-9]*)[^>]*>([^<]+)<\/\\1>/", $text, $m);

		for ($i=0; $i<count($m[0]); $i++)
		{
			if(!is_int(strpos($m[0][$i], ' '))) // que les chaione contenant un espace sont trait�es
				continue;
			$tagged = '<'.$m[1][$i].'>'
				.str_replace(' ', '</'.$m[1][$i].'> <'.$m[1][$i].'>', $m[2][$i])
				.'</'.$m[1][$i].'>';
			$text = str_replace($m[0][$i], $tagged, $text);
		}

		foreach (explode(" ", $text) as $word)
		{
			list($x, ) = gettextsize(strip_tags($ln.$word), $size);
			if ($x > $width)
			{
				$lines[] = trim($ln);
				$ln = '';
			}
			$ln .= $word.' ';
		}
		$lines[] = trim($ln);
		return $lines;
	}

	// writeline
	function writeline ($img, $size, $y, $hcol, $text)
	{
		preg_match_all("/(?:<([a-z][a-z0-9]*)[^>]*>)?([^<]+)(?:<\/\1>)?/", $text, $m);

		if(!count($m))
		{
			imagettftext($img, $size, START_X, START_Y, $y, $hcol, USE_FONT, $text);
			return;
		}
		// le preg_match_all ne renvoie pas les chaines comme on voudrait mais de la forme "/eot>text text", on nettoie
		array_walk($m[2], create_function('&$v, $k', '$v = preg_replace("/^\/?[^>]+>/", "", $v);'));

		list(,$h1) = gettextsize("b", $size);
		list($spw,) = gettextsize(" ", $size); // largeur d'un espace dans la taille demand�e

		for ($i=0, $x=0; $i<count($m[0]); $i++)
		{

			$tag = $m[1][$i];
			// les espaces sont r�duits � une seul, d�j� ajout� � la fin du mot pr�c�dent
			$s = trim($m[2][$i]);
			if(!$s)
				continue;

			switch($tag)
			{
				case 'sup':
					$supsize = $size / 2;
					list($w2, $h2) = gettextsize($s, $supsize);
					imagettftext($img, $supsize, 0, START_X+$x-$spw/2, START_Y+$y - $h1 + $h2, $hcol, USE_FONT, $s);
					$x += $w2;
				    break;
				case 'strong':
				case 'b':
					list($w1,) = gettextsize($s, $size, USE_FONT_BOLD);
					imagettftext($img, $size, 0, START_X+$x, START_Y+$y, $hcol, USE_FONT_BOLD, $s);
					$x += $w1 + $spw;
				    break;
				case 'u':
					list($w1,) = gettextsize($s, $size);
					imagettftext($img, $size, 0, $x, $y, $hcol, USE_FONT, $s);
					imagefilledrectangle($img, START_X+$x, START_Y+round($y+($h1*0.1)), $x+$w1, round($y+($h1*0.15)), $hcol);
					$x += $w1 + $spw;
				    break;
				default:
					list($w1,) = gettextsize($s, $size);
					imagettftext($img, $size, 0, START_X+$x, START_Y+$y, $hcol, USE_FONT, $s);
					$x += $w1 + $spw;
				    break;
			}
		}
		return;
	}
?>