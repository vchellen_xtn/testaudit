<?

class ZoneGroupe extends Ada_Object
{
	
	/**
	* 
	* @param int $id
	* @param string zone identifiant de la zone
	* @return ZoneGroupe
	*/
	static public function factory($id, $zone = null)
	{
		$x = new ZoneGroupe();
		if (!is_numeric($id))
			$x->setIdFieldName('nom');
		$x->load($id, '*', array('zone' => $zone));
		$x->setIdFieldName('id');
		return $x;
	}
	public function __toString()
	{
		return $this->_data['nom'];
	}
	
	/**
	* Charge la zone_groupe en tenant compte éventuellement de la zone dans $data
	* 
	* @param mixed $id
	* @param mixed $cols
	* @param array $data
	*/
	protected function _prepareSQL($id, $cols='*', $data=null)
	{
		$sql = parent::_prepareSQL($id, $cols, $data);
		if (!empty($data['zone']) && preg_match('/^[a-z]{2}$/', $data['zone'])) {
			$sql.= sprintf(" and zone='%s'", $data['zone']);
		}
		return $sql;
	}
}
?>