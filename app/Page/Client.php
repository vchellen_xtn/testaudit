<?

class Page_Client extends Page
{
	/** @var Reservation */
	protected $_reservation = null;
	
	/** @returns Client */
	public function getClient() { return $this->_client; }
	public function getReservation() { return $this->_reservation; }
	
	/** La page supporte-t-elle le HTTPS ? @returns bool */
	protected function isThatPageNeedsHttps()
	{
		return true;
	}
	// v�rification des param�tres appel�e depuis Page::factory()
	protected function checkParameters()
	{
		// les v�rifications g�n�rales
		// va authentifier le client s'il existe
		if (!parent::checkParameters())
			return false;
		$action = $this->getAction();
		if ($this->_client && $action == 'index')
			$this->redirect('../client/compte.html');
		if (!$this->_client && !in_array($action, array('index','motdepasse','changer-mot-de-passe')) && !($this->_adminUser && preg_match('/^(avoir|facture)/', $action)))
			$this->redirect('../client/index.html');
		// s'il y a un identifiant on r�cup�re la r�servation assoic�e
		if ($id = $_GET['id'])
		{ 
			if (Page::_checkKeyCustomer($id))
			{
				$this->_reservation = Reservation::factory((int) $id);
				if ($this->_client)
					$this->_reservation->checkClient($this->_client);
			}
		}
	}
	
	public function getUniversTitre() {
		return "Espace client";
	}
	
	// authentification de l'utilsiateur
	public function indexAction()
	{
		$isLoggedIn = false;
		// authentificatin facebook
		if (isset($_POST['fb_login']) && isset($_COOKIE['fbsr_'.FB_APP_ID]))
		{
			if ($clientFB = ClientFacebook::createFromSignedRequest($_COOKIE['fbsr_'.FB_APP_ID]))
			{
				if ($client = $clientFB->getClient())
				{
					$email = $client['email'];
					$this->_client = $client;
					$isLoggedIn = true;
				}
			}
			
		}
		// sinon v�rifie l'authentification
		else if ($email = filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
		{
			$this->_client = Client::factory($email);
			if (!$this->_client)
				$this->_addMessage("Cette adresse e-mail ne figure pas dans notre base client.");
			else if ($_GET['resend'])
			{
				$this->_client->sendPassword();
				$this->_addMessage("L'email vous permettant de modifier votre mot de passe vous a �t� adress�.");
			}
			else if (!$this->_client->checkPassword($_POST['pwd']))
				$this->_addMessage("La connexion � votre espace client n�a pu aboutir.<p/>Nous vous invitons � v�rifier votre e-mail et votre mot de passe.");
			else
			{
				$isLoggedIn = true;
			}
		}
		
		// si l'utilisateur est identifi� on le redireige
		if ($isLoggedIn)
		{
			// le login s'est bien pass�
			$this->setTrackInfo('login', $email);
			self::setKeyCustomer($email);
			self::redirect('../client/compte.html');
		}
	}
	public function changer_mot_de_passeAction()
	{
		// on r�cup�re le param�tre
		if (!($x = $_GET['_x']))
			self::redirect('../client/index.html');
		// on extrait les informations de x
		if (! ($client = Client::checkKey($x, $msg)))
		{
			$this->_addMessage($msg);
			return;
		}
		// si les mots de passe sont fournies
		if ($_POST['pwd'] && $_POST['rpwd'] && $_POST['pwd']==$_POST['rpwd'])
		{
			$client->changePassword($_POST['pwd']);
			$this->_addMessage("Votre mot de passe a �t� modifi�.");
			$this->_addMessage('Vous pouvez maintenant <a href="'.self::getURL('client/index.html?email='.htmlspecialchars(strip_tags($client['email']))).'">vous connecter</a>');
			return;
		}
		$this->_client = $client;
	}
	/**
	* Pr�pare les donn�es pour le compte pro
	* 
	*/
	public function compte_proAction()
	{
		// on r�cup�re le client et on v�rifie s'il s'agit d'un compte Pro
		$client = $this->getClient();
		$partenaire = $client->getPartenaire();
		if (!$partenaire)
			self::redirect(self::getURL('client/compte.html'));
	}
	public function partenaireAction()
	{
		// on v�rifie qu'il s'agit bien d'un compte pro
		$this->compte_proAction();
		// si des donn�es sont pass�es dans le $_POST
		if ($_POST)
		{
			$fields = 'adresse1,adresse2,cp,ville,tel,fax';
			$partenaire = $this->getClient()->getPartenaire();
			if ($partenaire->validateData($_POST, $errors, $fields))
			{
				foreach(explode(',', $fields) as $k)
					$partenaire->setData($k, $_POST[$k]);
				$partenaire->save();
				self::redirect(self::getURL('client/validation.html'));
			}
			$this->_addMessage($errors);
		}
		return;
	}
	
	// enregistre les modifications du compte client
	public function profilAction()
	{
		// on v�rifie l'inscription au Programme Kilom'�tre sur la page client/profil
		if (!$this->_client['pgkm'] && PgKm::checkEmail($this->_client['email'], true))
			$this->_client['pgkm'] = 1;

		// uniquement si des donn�es sont envoy�es
		if (!$_POST['valid_profil'] && !$_POST['valid_profil_x'])
			return;
		// si l'e-mail a chang� on v�rifie qu'il soit bien unique
		$email = filter_var($_POST['email'], FILTER_VALIDATE_EMAIL);
		if ($email && $email != $this->_client['email'])
		{
			if (getsql("SELECT email FROM client WHERE email='".addslashes($email)."'"))
			{
				$this->_addMessage("L'adresse email $email figure d�j� dans notre fichier client");
				return;
			}
		}
		// on modifie quelques valeurs
		$_POST['promo'] = ($_POST['promo'] ? 'O' : 'N');
		$_POST['partenaire'] = ($_POST['partenaire'] ? 'O' : 'N');
		
		// enregistrement et redirection
		$_POST['id'] = $this->_client['id'];
		if (!$_POST['email'])
			$_POST['email'] = $this->_client['email'];
		if ($this->_client->validateData($_POST, $errors))
		{
			save_record('client', $_POST);
			// si l'e-mail a chang� on modifie les cookies
			if ($email)
				self::setKeyCustomer($email);
		}
		else
		{
			$this->_addMessage($errors);
		}

		// puis on supprime les abonnements 1 clic
		if (is_array($_POST['abonnements']))
		{
			foreach($_POST['abonnements'] as $k)
			{
				if ($abonnement = PayboxAbonne::factory($this->_client, $k))
				{
					if (!$abonnement->remove($msg = ''))
						$this->_addMessage($msg);
				}
			}
		}
		// s'il y a des messages on reste sur la m�me page
		if ($this->hasMessages())
			return;
		// puis on redirigie vers une page de confirmation
		self::redirect('../client/validation.html');
	}
	public function programme_kilometreAction()
	{
		// on v�rifie le client
		$client = $this->getClient();
		if (!$client) self::redirect('../client/index.html');
		
		// ajouter FormChecker
		$this->addJS('scripts/form_checker.js');
		
		// on v�rifie les donn�es dans le $_POST
		if (count($_POST))
		{
			foreach($_POST as $k => $v)
				$_POST[$k] = filter_var($v, FILTER_SANITIZE_STRING);
			$permis_type = $_POST['permis_type'];
			if (!PgKm::$TYPE_PERMIS[$permis_type])
				$this->_addMessage("Le type de permis n'est pas correct !");
			$permis_numero = $_POST['permis_numero'];
			if (!preg_match('/^\d{6,20}$/', $permis_numero))
				$this->_addMessage("Le num�ro de permis ne semble pas valide !");
			$permis_date = euro_iso($_POST['permis_date']);
			if (!preg_match('/^(19|20)\d{2}-[01]\d-[0123]\d$/', $permis_date) && date('Y-m-d', strtotime($permis_date))!=$permis_date)
				$this->_addMessage("La date indqu�e n'est pas correcte!");
			else
			{
				$tPermis = strtotime($permis_date);
				if ($client['naissance']) $tNaissance = strtotime(euro_iso($client['naissance']));
				if ($tPermis > time())
					$this->_addMessage("La date de permis ne peut pas �tre dans le futur !");
				if ($tNaissance && $tPermis < strtotime('+18 years', $tNaissance))
					$this->_addMessage("Il n'y a pas 18 ans d'�cart entre votre date de naissance (".date('d/m/Y', $tNaissance).") et votre date de permis (".date('d/m/Y', $tPermis).")");
			}
			$permis_lieu = trim($_POST['permis_lieu']);
			if (!($l = strlen($permis_lieu)) || $l > 20 || $l < 2)
				$this->_addMessage("Le lieu du permis n'est pas vllide");
			// si les param�trs ont bien �t� valid�es on envoie les donn�es au programme kilom'�tre
			if (!$this->hasMessages())
			{
				$x = PgKm::registerUser($client, $permis_type, $permis_numero, $permis_date, $permis_lieu);
				if ($x != 'OK')
				{
					$this->_addMessage("Une erreur s'est produite lors de l'inscription au Programme Kilom'�tre.<br/>$x");
				}
			}
		}
		return;
	}
	
	// confirmation de l'nanulation
	public function confirmationAction()
	{
		// on v�rifie qu'on a la r�servation et qu'elle peut �tre annul�e
		if (!$this->_reservation) return;
		if (!$this->_reservation->isCancellable($msg))
		{
			$this->_addMessage($msg);
			return;
		}
		// on cr�e la transaction associ�e au mode de paiement puor annuler
		$x = Transaction::factory($this->_reservation->getData('paiement_mode'));
		if (!$x) 
		{
			$this->_addMessage("Le mode de paiement n'a pas pu �tre d�termin�");
			return;
		}
		$x->doCancel($this->_reservation);
		$this->_addMessage($x->getMessages());
		// si l'annulation a �chou� on arr�te l�
		if ($this->_reservation['statut'] != 'A')
			return;
		// tracking pour l'annulation
		$track['agence_annulee'] = $this->_reservation['agence'];
		$track['ecart_annulation'] = ceil($this->_reservation->getHoursBeforeStart()/24);
		$track['pr_out'] = Tracking_SmartProfile::getPR($this->_reservation);
		$this->setTrackInfo($track);
	}
	
	/*
	** AFFICHAGE DES FACTURES ET DES AVOIRS
	*/
	/**
	* V�rifie les param�tres pour la facturation
	* 
	* @param string $facturation ada|courtage
	* @param string $type F|A
	*/
	protected function _checkFacturation($facturation, $type)
	{
		if (!$this->_reservation) return;
		$x = $this->_reservation->getFacturations()->find($facturation, $type);
		if (!$x)
		{
			$this->_addMessage("Ce document n'est pas disponible");
			return;
		}
		$this->setData('facturation', $x);
	}
	public function factureAction() { $this->_checkFacturation('ada', 'F'); }
	public function facture_courtageAction() { $this->_checkFacturation('courtage', 'F'); }
	public function facture_materielAction() { $this->_checkFacturation('materiel', 'F'); }
	public function facture_proAction() { $this->_checkFacturation('pro', 'F'); }
	public function avoirAction() { $this->_checkFacturation('ada', 'A'); }
	public function avoir_courtageAction() { $this->_checkFacturation('courtage', 'A'); }
	public function avoir_materielAction() { $this->_checkFacturation('materiel', 'A'); }

}
?>
