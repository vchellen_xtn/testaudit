<?
	/** @var Page_Client */ $that = $this;
	/** @var Client */  $client = $that->getClient();
	$action = basename($page);
	$menu = array(
		'compte'	=> 'Votre compte client',
		'compte_pro'	=> 'Votre compte pro',
	);
	// on ne garde pas le compte pro dans tous les cas
	if (!$client || !$client->hasComptePro()) {
		unset($menu['compte_pro']);
	}
	// dans certains cans, on ne veut pas de menu
	if (preg_match('/^(avoir|facture|confirmation)/', $action)) {
		$menu = array();
	}
	$menuCurrent = ($action == 'compte_pro' ? 'compte_pro' : 'compte');
?>
<? if (count($menu) > 0) : ?>
<ul class="menu type-li-<?=count($menu);?>">
	<? foreach($menu as $k => $lbl) : ?>
		<li <?if ($menuCurrent == $k) echo ' class="current"'; ?>>
			<a href="<?=$k?>.html"<?if ($menuCurrent == $k) echo ' class="current"'; ?>><?=$lbl;?></a>
		</li>
	<? endforeach; ?>
</ul>
<? endif; ?>
