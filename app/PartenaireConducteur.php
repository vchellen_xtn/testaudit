<?php

class PartenaireConducteur extends Ada_Object {
	/**
	* Renvoie un conducteur
	* 
	* @param int $id
	* @return PartenaireConducteur
	*/
	static public function factory($id) {
		$x = new PartenaireConducteur();
		$x->load((int)$id);
		if ($x->isEmpty()) return null;
		return $x;
	}
	
	/**
	* Renvoie une cha�ne de caract�res pour identifier un conducteur
	* @returns string
	*/
	public function __toString() {
		return sprintf('%s %s - %s', $this->_data['nom'], $this->_data['prenom'], $this->_data['email']);
	}
}
?>
