<?
	require_once("secure.php"); 
	require_once("lib.php");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script type="text/javascript" src="<?=Page::getProtocol()?>://www.google.com/jsapi"></script>
	<script type="text/javascript">
		google.load('jquery', '1.4');
	</script>
	<script type="text/javascript">
	// <![CDATA[
	// ]]>
	</script>
	<style type="text/css">
		select,input { width: inherit;}
		label { padding-left: 2px; }
		label.col1 { padding-left: 0; float: left; width: 65px;}
	</style>
</head>
<body>
<h1>Service Client : Code Grillable</h1>
<form action="" method="POST">
<p>
	<label for="campagne"><strong>Sélectionnez la campagne pour laquelle vous souhaitez obtenir un code grillable :</strong></label>
	<br/><? selectarray('campagne', PromotionCampagne_Collection::factory(true, date('Y-m-d')), $_POST['campagne'], '-- choisissez une campagne service client--', 'onchange="this.form.submit()"'); ?>
	<br/>Ce code grillable sera alors marqué comme distribué : il est de votre responsabiltié de le communiquer.
</p>
</form>
<? if ($campagne = (int) $_POST['campagne']) : ?>
<? $campagne = PromotionCampagne::factory($campagne); ?>
<h2>Campagne #<?=$campagne['id']?> - <?=$campagne['nom']?></h2>
<p>Vous devez maintenant distribuer le code grillable ci-dessous :</p>
<p style="margin: 10px;"><strong style="padding: 5px; border: 1px dotted #CCCCCC;"><? echo $campagne->getOneCode($adminUser['login'])->getData('code'); ?></strong></p>
<? endif; ?>
</body>
</html>