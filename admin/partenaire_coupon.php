<?
	require_once("secure.php");
	require_once("lib.php");

	// identifiant
	$id = (int) getarg('id');
	if (!$id) $id = (int) getarg('coupon');
	$partenaire = (int) $_GET['partenaire'];
	if (!$id && !$partenaire)
		die("Vous devez partir d'un partenaire our cr�er un nouveau code de r�duction !");

	// suppression
	if ($id && getarg("delete"))
	{
		$partenaire = getsql("select partenaire from partenaire_coupon where id=$id");
		$sql = "DELETE FROM partenaire_coupon WHERE id = '".$id."'";
		getsql($sql);
		unset($id);
		header("Location: partenaire.php?id=$partenaire");
		exit();
	}

	// modification
	if (getarg("update") || getarg("create"))
	{
		// on v�rifie que le code n'exiiste pas dj��
		if (!$id)
		{
			$pc = PartenaireCoupon::factory($_POST['code'], true);
			if ($pc && !$pc->isEmpty())
			{
				echo 'Le code '.$pc['code'].' est d�j� utilis� par le partenaire <a href="partenaire.php?id='.$pc['partenaire'].'">'.$pc['partenaire_nom'].'</a>';
				echo '<br/>Voir le code r�duciton <a href="partenaire_coupon.php?id='.$pc['id'].'">#'.$pc['id'].' - '.$pc['nom'].'</a>';
				exit();
			}
		}
		save_record("partenaire_coupon", $_POST);
		if(!$id)
			$id = mysql_insert_id();
	}

	if ($id)
	{
		$rs = sqlexec("SELECT * FROM partenaire_coupon WHERE id = '".$id."'");
		if (!$rs || !($row = mysql_fetch_assoc($rs)))
			die("Aucun enregistrement correspondant.");
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script type="text/javascript" src="../scripts/script.js"></script>
	<script type="text/javascript">
	<!--
	// doDelete
	function doDelete(frm)
	{
		return (confirm("Voulez-vous r�ellement supprimer cet �l�ment ?\nCette op�ration est irr�versible."));
	}

	// doSubmit
	function doSubmit(frm)
	{
		var err = "";
		if(!frm.code.value.length)
			err += "Veuillez indiquer le code\n";
		if(!frm.nom.value.length)
			err += "Veuillez indiquer le nom\n";
		if(!validateSelect(frm.actif))
			err += "Veuillez indiquer le statut\n";
		if(frm.partenaire && !validateSelect(frm.partenaire))
			err += "Veuillez indiquer le partenaire\n";
		if (frm.depuis.value.length && !validateDate(frm.depuis))
			err += "Veuillez rentrer une date de d�but valide\n";
		if (frm.echeance.value.length && !validateDate(frm.echeance))
			err += "Veuillez rentrer une date d'�ch�ance valide\n";
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		return true;
	}
	//-->
	</script>
</head>
<body>
<form action="" method="post" onsubmit="return doSubmit(this);" enctype="multipart/form-data">
<table class="framed" border="0" cellspacing="0" cellpadding="4" style="float: left;">
<tr>
	<th class="bg_titre">
		<img src="img/puce.gif" align="absmiddle" hspace="1"> Code R�duction
	</th>
</tr>
<tr>
	<td>
	<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="15%">
		<col width="35%">
		<col width="15%">
		<col width="35%">
		<tr>
			<td><label for="id">Id</label></td>
			<td>
			<?
				if ($id) echo $id;
				echo '<input type="hidden" id="id" name="id" value="'.$id.'">'
			?>
			</td>
			<td colspan="2"></td>
		</tr>
		<tr>
			<td>Partenaire</td>
			<td colspan="3">
			<?
				if($row["partenaire"])
					echo '<a href="partenaire.php?id='.$row['partenaire'].'">'.getsql('SELECT nom FROM partenaire WHERE id ='.$row['partenaire']).'</a>';
				else
					selectsql('partenaire', 'SELECT id, nom FROM partenaire WHERE id='.$partenaire.' ORDER BY nom', $partenaire, '--');
			?>
			</td>
			<td colspan="2"></td>
		</tr>
		<tr>
			<td><label for="nom">Nom</label></td>
			<td colspan="3"><input type="text" id="nom" name="nom" value="<?=$row ? $row['nom'] : getsql("select nom from partenaire where id=$partenaire")?>" maxlength="64" size="64"/></td>
		</tr>
		<tr>
			<td></td>
			<td colspan="3">pour diff�rencier si plusieurs codes r�ductions pour un m�me partenaire</td>
		</tr>
		<tr><th colspan="4">Code � saisir par le client</th></tr>
		<tr>
			<td><label for="code">Code</label></td>
			<td><input type="text" id="code" name="code" value="<?=$row["code"]?>" maxlength="32" size="32"></td>
			<td><label for="actif">Actif</label></td>
			<td><? selectarray("actif", array('1'=>'actif','0'=>'inactif'), $row['actif']); ?></td>
		</tr>
		<tr><th colspan="4">Validit� du code</th></tr>
		<tr>
			<td><label for="depuis">Depuis</label></td>
			<td><input type="text" id="depuis" name="depuis" value="<?=euro_iso($row['depuis'])?>" maxlength="10" size="12"></td>
			<td><label for="echeance">Ech�ance</label></td>
			<td><input type="text" id="echeance" name="echeance" value="<?=euro_iso($row["echeance"])?>" maxlength="10" size="12"></td>
		</tr>
		<tr><th colspan="4">Promotions et/ou Locapass associ�s � ce code de r�duction</th></tr>
		<tr>
			<td><label for="classe">Classe de promotions</label></td>
			<td><? selectsql('classe', 'SELECT id, nom FROM partenaire_classe ORDER BY nom', $row['classe'], '--');?></td>
			<td><label for="locapass">Locapass</td>
			<td><input type="text" id="locapass" name="locapass" value="<?=$row["locapass"]?>" maxlength="6" size="8"></td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%">
		<tr>
			<td valign="top"></td>
			<td align="right" valign="top">
				<input type="submit" name="<?=$id ? "update" : "create"?>" value="enregistrer">
<?
	if ($id)
	{
?>
				&nbsp;
				<input type="submit" onclick="return doDelete(this.form);" name="delete" value="supprimer">
<?
	}
?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<?
if ($row)
{
	$coupon = new PartenaireCoupon($row);
?>
<table class="framed" border="0" cellspacing="0" cellpadding="4" style="float:left; margin-left:5px; width:300px;">
	<tr>
		<td><a href="stats_partenaire.php?coupon=<?=$coupon['id']?>&origine=<?=getsql('select origine from partenaire where id='.$coupon['partenaire']);?>">Stats</a></td>
	</tr>
	<?
	$reglements = PartenaireReglement_Collection::factory($coupon);
	if (!$reglements->isEmpty())
	{
		echo '<tr><th class="bg_titre"><img src="img/puce.gif" align="absmiddle" hspace="1">R�glements</th></tr>';
		foreach($reglements as /** @var PartenaireReglement */ $reglement)
		{
			echo '<tr><td>';
			echo 'R�glement cr�� le '.euro_iso($reglement['creation']);
			echo '<br/>'.$reglement['prix_ht'].'� HT pour une dur�e de '.$reglement['duree'].' mois';
			if ($reglement['reservation'])
			{
				$reservation = $reglement->getReservation();
				echo '<br/>pay� le '.euro_iso($reglement['reglement']).' par la r�servation <a href="reservation.php?id='.$reglement['reservation'].'">#'.$reglement['reservation'].'</a>';
				echo '<br/><a href="../'.$reservation->getFacturations()->find('pro', 'F')->getLink().'" target="_blank">voir la facture</a>';
			}
			echo '</td></tr>';
		}
	}
?>
</table>
<? } ?>
</form>
</body>
</html>
