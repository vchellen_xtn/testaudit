<?
class ResOption extends Ada_Object
{
	public function _construct()
	{
		$this->setIdFieldName('option');
	}
	public function getPrix($show = true)
	{
		$prix = $this->_data['prix'];
		if ($show && $this->_data['paiement'] == 'L')
		{
			if ($prix > 0)
				return sprintf('%01.2f', $prix).'&nbsp;�';
			return 'inclus';
		}
		return;
	}
	public function __toString()
	{
		$out = '';
		if ($this->_data['quantite'])
			$out = $this->_data['quantite'].' ';
		$out.= $this->_data['nom'];
		return $out;
	}
	public function updatePrix($prix)
	{
		$this->_data['prix'] = $prix;
		if ($this->_data['reservation'])
			sqlexec(sprintf("UPDATE res_option SET prix='%s' WHERE reservation=%ld AND `option`='%s'", round($prix, 2), $this->_data['reservation'], $this->_data['option']));
	}
}
?>