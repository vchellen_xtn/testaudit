<?php
class Page_Faq extends Page {
	static protected $_FAQ_RUBRIQUES = array(
		'louer'			=> "Louer un v�hicule Ada",
		'vehicules'		=> 'V�hicules',
		'jeunes'		=> "Ada Jeunes",
		'demenagement'	=> 'D�m�nagement',
		'reservation'	=> "Ma r�servation",
		'carburant'		=> "Carburant",
		'etranger'		=> "Location hors fronti�res",
	);
	
	/** stocke la liste des questions par rubrique */
	protected $_questions = array();
	
	public function getUniversTitre() {
		return "FAQ";
	}
	
	/**
	* Renvoie le tableau des rubriques de la FAQ
	* @returns array
	*/
	public function getFAQRubriques() {
		return self::$_FAQ_RUBRIQUES;
	}
	/**
	* Renvoie la rubrique de la FAQ en se basant sur la premi�re partie de l'action avant le premier tiret
	* @returns string
	*/
	public function getFAQRubrique($page = null) {
		if (!$page) {
			$page = $this->getId();
		}
		if (preg_match('/^faq\/([^\-]+)/', $page, $m)) {
			return $m[1];
		}
		return '';
	}
	
	/**
	* Renvoie le menu pour la page courante
	* 
	*/
	public function getQuestions($rubrique = null) {
		// r�cup�rer la rubrique courante si elle n'est pas indiqu�e
		if (!$rubrique) {
			$rubrique = $this->getFAQRubrique();
		}
		// charger les informations depuis la base de donn�es
		if ($rubrique == 'index' || !$this->_questions[$rubrique]) {
			$search = 'faq/';
			if ($rubrique != 'index') {
				$search.= $rubrique.'-';
			}
			$rs = sqlexec(sprintf("SELECT id, url, titre, h1 FROM page WHERE id LIKE '%s%%' ORDER BY h1", $search));
			while($row = mysql_fetch_assoc($rs)) {
				if ($row['id'] == 'faq/index') continue;
				$this->_questions[$this->getFAQRubrique($row['id'])][basename($row['id'])] = $row;
			}
		}
		// pr�parer le HTML
		$output = '';
		if ($rubrique == 'index') {
			foreach(self::$_FAQ_RUBRIQUES as $k => $lbl) {
				$output.= '<h2>'.$lbl.'</h2>'."\n";
				$output.= $this->getQuestions($k);
			}
		} else if (isset($this->_questions[$rubrique])) {
			$output.='<ul class="bg-titre-faq">'."\n";
			foreach($this->_questions[$rubrique] as $href => $question) {
				if ($href == $rubrique) continue;
				$output.=sprintf('<li><a href="%s.html">%s</a></li>', $href, $question['h1'])."\n";
			}
			$output.='</ul>'."\n";
		}
		return $output;;
	}
}
?>
