<?
	session_start();
	define('SKIN_DEFAULT', 'v2');
	include ('constant.php');

	// header
	header("X-Robots-Tag: noindex");
    header('Content-Type: text/html; charset=utf-8');

	// cr�er la page et l'afficher
	$x = Page::factory('popin', $_GET['page'], 'popin/index');
	if (!$x) die('Erreur interne');
	$x->noIndex();
	$x->render();
?>