<?php
class FaultDetail {
  public $errorMessage; // string
  public $requestID; // string
}

class getStationsADA_listeStationsRow {
  public $cSystemID; // string
  public $cCodeStation; // string
  public $cCodeStationIATA; // string
  public $cCodePays; // string
  public $cCodeRegion; // string
  public $cNomStation; // string
  public $cNomVille; // string
  public $cCodePostal; // string
  public $cVilleRattachement; // string
  public $cCodePostalVilleRattachement; // string
  public $cAdresse1; // string
  public $cAdresse2; // string
  public $cTypeStation; // string
  public $cTelephone; // string
  public $cFax; // string
  public $cEmail; // string
  public $cCritere; // string
  public $cLatitude; // string
  public $cLongitude; // string
  public $cTelephoneAstreinte; // string
}

class getHorairesStationsADA_listeHorairesRow {
  public $cSystemID; // string
  public $cCodeStation; // string
  public $cTypeHoraire; // string
  public $cDateDebut; // string
  public $cDateFin; // string
  public $cJourSemaine; // string
  public $cNomVille; // string
  public $cHeureDebut; // string
  public $cHeureFin; // string
  public $cAfterHours; // string
}

class getCategoriesVehicules_listeCategoriesRow {
  public $cSystemID; // string
  public $cCodePays; // string
  public $cCodeStation; // string
  public $cCodeRegion; // string
  public $cCarCategory; // string
  public $cCarGroup; // string
  public $cText; // string
  public $cImage; // string
  public $cTypeVehicule; // string
}

class getEquipementsLocation_listeEquipementsRow {
  public $cSystemID; // string
  public $cCountryID; // string
  public $cStationID; // string
  public $cEquipementID; // string
  public $cName; // string
  public $iQteMax; // int
}

class getPrestationsLocation_listePrestationsRow {
  public $cSystemID; // string
  public $cCountryID; // string
  public $cStationID; // string
  public $cPrestationID; // string
  public $cName; // string
}

class putReservationSimple_listeEquipementsRow {
  public $cEquipementID; // string
  public $iNbEquipement; // int
  public $cName; // string
}

class putReservationSimple_listePrestationsInclusesRow {
  public $cCodePrestation; // string
  public $cPrixPrestation; // string
}

class putReservationSimple_listePrestationsExcluesRow {
  public $cCodePrestation; // string
  public $cPrixPrestation; // string
}

class putReservationSimple_listeCategoriesDisponiblesRow {
  public $cCarCategory; // string
  public $cStatutDispVehicule; // boolean
}

class getReservationSimple_listeEquipementsRow {
  public $cEquipementID; // string
  public $iNbEquipement; // int
  public $cName; // string
}

class getReservationSimple_listePrestationsInclusesRow {
  public $cCodePrestation; // string
  public $cPrixPrestation; // string
}

class getReservationSimple_listePrestationsExcluesRow {
  public $cCodePrestation; // string
  public $cPrixPrestation; // string
}

class getReservationSimple_listeCategoriesDisponiblesRow {
  public $cCarCategory; // string
  public $cStatutDispVehicule; // boolean
}

class getReservationSimpleByNrVoucher_listeEquipementsRow {
  public $cEquipementID; // string
  public $iNbEquipement; // int
  public $cName; // string
}

class getReservationSimpleByNrVoucher_listePrestationsInclusesRow {
  public $cCodePrestation; // string
  public $cPrixPrestation; // string
}

class getReservationSimpleByNrVoucher_listePrestationsExcluesRow {
  public $cCodePrestation; // string
  public $cPrixPrestation; // string
}

class getReservationSimpleByNrVoucher_listeCategoriesDisponiblesRow {
  public $cCarCategory; // string
  public $cStatutDispVehicule; // boolean
}

class postReservationSimple_listeEquipementsRow {
  public $cEquipementID; // string
  public $iNbEquipement; // int
  public $cName; // string
}

class postReservationSimple_listePrestationsInclusesRow {
  public $cCodePrestation; // string
  public $cPrixPrestation; // string
}

class postReservationSimple_listePrestationsExcluesRow {
  public $cCodePrestation; // string
  public $cPrixPrestation; // string
}

class postReservationSimple_listeCategoriesDisponiblesRow {
  public $cCarCategory; // string
  public $cStatutDispVehicule; // boolean
}

class getDisponibiliteVehicule_listeDisponibilityRow {
  public $cCategory; // string
  public $lStatus; // boolean
}

class VerifierSiUserExiste_TAB_SocietesRow {
  public $COD_Societe; // string
  public $LIB_Societe; // string
}

class VerifierSiUserExiste_TAB_AgencesRow {
  public $COD_Societe; // string
  public $COD_Agence; // string
  public $LIB_Agence; // string
}

class VerifierSiUserExiste_TAB_AlertesRow {
  public $COD_Alerte; // string
  public $LIB_Alerte; // string
  public $DAT_HeureDebut; // string
  public $DAT_HeureFin; // string
  public $FLG_Active; // string
  public $FLG_AfficherPortail; // string
}

class VerifierSiUserExisteV2_TAB_SocietesRow {
  public $COD_Societe; // string
  public $LIB_Societe; // string
}

class VerifierSiUserExisteV2_TAB_AgencesRow {
  public $COD_Societe; // string
  public $COD_Agence; // string
  public $LIB_Agence; // string
}

class VerifierSiUserExisteV2_TAB_AlertesRow {
  public $COD_Alerte; // string
  public $LIB_Alerte; // string
  public $DAT_HeureDebut; // string
  public $DAT_HeureFin; // string
  public $FLG_Active; // string
  public $FLG_AfficherPortail; // string
}

class ListerLesAgences_TAB_AgencesRow {
  public $COD_Societe; // string
  public $COD_Agence; // string
  public $LIB_Agence; // string
}

class ListerLesCategories_TAB_CategorieRow {
  public $COD_categorie; // string
  public $LIB_categorie; // string
}

class ListerLesGenres_TAB_GenreRow {
  public $COD_genre; // string
  public $LIB_genre; // string
}

class ListerLesMarques_TAB_MarquesRow {
  public $COD_marque; // string
  public $LIB_marque; // string
}

class ListerLesModeles_TAB_ModelesRow {
  public $COD_marque; // string
  public $COD_modele; // string
  public $LIB_modele; // string
  public $LIB_energie; // string
}

class ListerLesEnergies_TAB_EnergieRow {
  public $COD_energie; // string
  public $LIB_energie; // string
}

class CompterLesReservationsExternes_TAB_ReservationRow {
  public $COD_type; // string
  public $NBR_ParType; // int
}

class ListerLesReservationsExternes_TAB_ReservationRow {
  public $NUM_Resa; // string
  public $NUM_Voucher; // string
  public $DAT_Depart; // string
  public $DAT_Retour; // string
  public $NOM_Agence; // string
  public $COD_Categorie; // string
  public $COD_type; // string
  public $VAL_Voucher; // decimal
  public $NOM_Conducteur; // string
  public $ADR_Conducteur; // string
  public $AGE_Conducteur; // int
  public $DAT_Optention_Permis; // string
  public $ETAT_Operation; // string
  public $FLG_Lu; // int
}

class ListerLesReservationsExternes_TAB_OPTRow {
  public $NUM_Resa; // string
  public $COD_type; // string
  public $COD_Option; // string
  public $LIB_Option; // string
  public $LIB_type_Opt; // string
  public $FLG_Opt_Inclus; // string
  public $VAL_Prix; // decimal
  public $QTE; // int
  public $VAL_Montant; // decimal
}

class getAgencesAdafr_listeAgencesAdafrRow {
  public $cCodeBase; // string
  public $cCodeAgence; // string
  public $cCodeAgenceIATA; // string
  public $cCodePays; // string
  public $cCodeRegion; // string
  public $cNomAgence; // string
  public $cNomVille; // string
  public $cCodePostal; // string
  public $cAdresse1; // string
  public $cAdresse2; // string
  public $cTypeStation; // string
  public $cTelephone; // string
  public $cFax; // string
  public $cEmail; // string
  public $cLatitude; // string
  public $cLongitude; // string
  public $cCodeSociete; // string
  public $cNomSociete; // string
  public $cResponsableLegal; // string
  public $cResponsableAgence; // string
  public $cDateDebutAffectation; // string
  public $cGroupepAgence; // string
  public $cCodeResponsableRegion; // string
}

class getJaugesVariablesAdafr_listeJaugesVariablesAdafrRow {
  public $cCodeAgence; // string
  public $cCategorie; // string
  public $cDateDebut; // string
  public $cHeureDebut; // string
  public $cDateFin; // string
  public $cHeureFin; // string
  public $cValJauge; // string
  public $lStatusJaugeExiste; // boolean
}

class getFermetureCialeAgence_listeFermetureCialeAgenceRow {
  public $cCodeAgence; // string
  public $cDateDebut; // string
  public $cDateFin; // string
}

class putReservationAdafrv5_lstPrestRow {
  public $cCodePrestation; // string
  public $cPrixPrestation; // string
  public $cTypePrestation; // string
  public $cPrixUnitairePrestation; // string
  public $cQuantitePrestation; // string
}

class putReservationAdafrv2_lstPrestRow {
  public $cCodePrestation; // string
  public $cPrixPrestation; // string
  public $cTypePrestation; // string
}

class putReservationAdafrv3_lstPrestRow {
  public $cCodePrestation; // string
  public $cPrixPrestation; // string
  public $cTypePrestation; // string
}

class putReservationAdafrv4_lstPrestRow {
  public $cCodePrestation; // string
  public $cPrixPrestation; // string
  public $cTypePrestation; // string
  public $cPrixUnitairePrestation; // string
  public $cQuantitePrestation; // string
  public $dateEnlevement; // string
}

class getDateEnlevementOptionsAdafr_lstPrestDemenagementRow {
  public $cCodePrestation; // string
  public $lEnleve; // boolean
  public $dateEnlevement; // date
}

class updateOptionsAdafr_lstPrestRow {
  public $cCodePrestation; // string
}

class EnregistrerEnlevementColis_TAB_PrestRow {
  public $COD_Prestation; // string
  public $DAT_Enlevement; // string
}

class ListerLesVehiculesProlongeables_TAB_VehiculesRow {
  public $COD_Agence; // string
  public $COD_Genre; // string
  public $COD_Categorie; // string
  public $COD_Marque; // string
  public $COD_Modele; // string
  public $LIB_Modele; // string
  public $LIB_Immat; // string
  public $DAT_Sortieprevue; // string
}

class LireEnteteCmdsVehiculesEnCours_TAB_EnteteCmdsRow {
  public $NUM_Cmds; // string
  public $DAT_Cmds; // string
  public $COD_Societe; // string
  public $COD_Etat; // string
  public $LIB_Presentation; // string
}

class LireDetailCmdsVehiculesEnCours_TAB_CmdsRow {
  public $NUM_Cmd; // string
  public $COD_Agence; // string
  public $COD_Genre; // string
  public $COD_Categorie; // string
  public $COD_Acriss; // string
  public $COD_Marque; // string
  public $COD_Gamme; // string
  public $COD_Modele; // string
  public $LIB_Modele; // string
  public $DAT_Precmd; // string
  public $FLG_CmdConfirme; // string
  public $DAT_LivraisonSouhaite; // string
  public $DAT_LivraisonPrevue; // string
  public $VAL_Loyer; // decimal
  public $VAL_Km; // int
  public $VAL_KmSup; // decimal
  public $FLG_ContratSigne; // string
  public $FLG_Immatricule; // string
  public $FLG_Livre; // string
  public $DAT_Sortieprev; // string
}

class LireUneAgence_TAB_FermeturesRow {
  public $VAL_Id; // int
  public $DAT_Debut; // string
  public $DAT_Fin; // string
  public $LIB_Raison; // string
}

class LireUneAgence_TAB_JoursRow {
  public $NUM_JourSemaine; // string
  public $COD_Demijournee; // string
  public $DAT_Ouverture; // string
  public $DAT_Fermeture; // string
}

class LireLesStopSales_TAB_StopSaleRow {
  public $COD_Agence; // string
  public $COD_Genre; // string
  public $COD_Categorie; // string
  public $DAT_Debut; // string
  public $DAT_Fin; // string
  public $LIB_Type; // string
  public $VAL_StopSale; // int
}

class LireCAAgence_TAB_ChifaffaireRow {
  public $CODE_AGENCE; // string
  public $NOM_VILLE; // string
  public $ANNEE; // int
  public $MOIS; // int
  public $CODE_GENRE; // string
  public $VAL_CA; // decimal
  public $PARCMOY; // int
  public $CUMNBCTRAT; // int
  public $CUMNBJOURS; // int
  public $CUMNBKMS; // int
  public $DATECREA; // string
  public $CODE_UTILIS_CREA; // string
  public $NOM_SOCIETE; // string
}

class ListerLesUtilisateurs_TAB_UtilisateurRow {
  public $COD_Utilisateur; // string
  public $LIB_Utilisateur; // string
  public $COD_Group; // string
}

class LireDetailReservationsExternes_TAB_RemarquesRow {
  public $LIB_Remarque; // string
}

class LireDetailReservationsExternes_TAB_OPTRow {
  public $NUM_Resa; // string
  public $COD_type; // string
  public $COD_Option; // string
  public $LIB_Option; // string
  public $LIB_type_Opt; // string
  public $FLG_Forfait; // string
  public $VAL_Prix; // decimal
  public $QTE; // int
  public $VAL_Montant; // decimal
  public $VAL_MontantTTC; // decimal
}

class LireEnteteReservationsExternes_TAB_ReservationRow {
  public $NUM_Resa; // string
  public $NUM_Voucher; // string
  public $DAT_Depart; // string
  public $DAT_Retour; // string
  public $COD_agence_resa; // string
  public $NOM_Agence; // string
  public $COD_Categorie; // string
  public $COD_type; // string
  public $VAL_Voucher; // decimal
  public $ETAT_Operation; // string
  public $FLG_Lu; // int
  public $NB_JLOC; // decimal
  public $NB_KM; // int
}

class ValiderCAAgences_TAB_ChifaffaireRow {
  public $CODE_SOCIETE; // string
  public $CODE_AGENCE; // string
  public $NOM_VILLE; // string
  public $ANNEE; // int
  public $MOIS; // int
  public $CODE_GENRE; // string
  public $VAL_CA; // decimal
  public $PARCMOY; // int
  public $CUMNBCTRAT; // int
  public $CUMNBJOURS; // int
  public $CUMNBKMS; // int
  public $CODE_UTILIS_CREA; // string
  public $LIB_COMMENTAIRE; // string
}

class getAgencesAda_listeAgencesAdaRow {
  public $cCodeAgence; // string
  public $cCodeAgenceIATA; // string
  public $cCodePays; // string
  public $cCodeRegion; // string
  public $cNomAgence; // string
  public $cNomVille; // string
  public $cCodePostal; // string
  public $cAdresse1; // string
  public $cAdresse2; // string
  public $cTypeStation; // string
  public $cTelephone; // string
  public $cFax; // string
  public $cEmail; // string
  public $cLatitude; // string
  public $cLongitude; // string
  public $cCodeSociete; // string
  public $cNomSociete; // string
  public $cResponsableLegal; // string
  public $cResponsableAgence; // string
  public $cDateDebutAffectation; // string
  public $cTypeContrat; // string
}

class CalculerCaAgences_TAB_Facture_NonValideeRow {
  public $NUM_FACTURE; // string
}

class CalculerCaAgences_TAB_ChifaffaireRow {
  public $CODE_AGENCE; // string
  public $NOM_VILLE; // string
  public $ANNEE; // int
  public $MOIS; // int
  public $CODE_GENRE; // string
  public $VAL_CA; // decimal
  public $PARCMOY; // int
  public $CUMNBCTRAT; // int
  public $CUMNBJOURS; // int
  public $CUMNBKMS; // int
}

class LireTotalCAAgence_TAB_ChifaffaireRow {
  public $CODE_SOCIETE; // string
  public $CODE_AGENCE; // string
  public $NOM_VILLE; // string
  public $ANNEE; // int
  public $MOIS; // int
  public $CODE_GENRE; // string
  public $VAL_CA; // decimal
  public $PARCMOY; // int
  public $CUMNBCTRAT; // int
  public $CUMNBJOURS; // int
  public $CUMNBKMS; // int
  public $CODE_UTILIS_CREA; // string
  public $LIB_COMMENTAIRE; // string
}

class LireFacturesNonValidees_TAB_Facture_NonValideeRow {
  public $DAT_FACTURE; // string
  public $NUM_FACTURE; // string
  public $NOM_CLIENT; // string
  public $MONTANT; // decimal
}

class getOptionsAda_listeOptionsAdaRow {
  public $cSystemID; // string
  public $cCountryID; // string
  public $cStationID; // string
  public $cOptionID; // string
  public $cName; // string
  public $iQteMax; // int
}

class DeclarerRachatFranchisev3_TAB_ConductAdditionnelRow {
  public $LIB_NomCdtAdd; // string
  public $LIB_PrenomCdtAdd; // string
  public $LIB_SexeCdtAdd; // string
  public $DAT_PermisCdtadd; // string
  public $NUM_PermisCdtlAdd; // string
}

class DeclarerRachatFranchisev2_TAB_ConductAdditionnelRow {
  public $LIB_NomCdtAdd; // string
  public $LIB_PrenomCdtAdd; // string
  public $LIB_SexeCdtAdd; // string
  public $DAT_PermisCdtadd; // string
  public $NUM_PermisCdtlAdd; // string
}

class DeclarerRachatFranchiseCarpv2_TAB_ConductAdditionnelRow {
  public $LIB_NomCdtAdd; // string
  public $LIB_PrenomCdtAdd; // string
  public $LIB_SexeCdtAdd; // string
  public $DAT_PermisCdtadd; // string
  public $NUM_PermisCdtlAdd; // string
}

class DeclarerRachatFranchise_TAB_ConductAdditionnelRow {
  public $LIB_NomCdtAdd; // string
  public $LIB_PrenomCdtAdd; // string
  public $LIB_SexeCdtAdd; // string
  public $DAT_PermisCdtadd; // string
  public $NUM_PermisCdtlAdd; // string
}

class LireEtatParc_TAB_VehiculesRow {
  public $COD_Societe; // string
  public $LIB_RaisonSociale; // string
  public $LIB_OptionDeclaration; // string
  public $COD_AgenceAffec; // string
  public $LIB_Agence; // string
  public $COD_Parc; // string
  public $LIB_Immat; // string
  public $COD_Genre; // string
  public $COD_Categorie; // string
  public $LIB_SituAdmin; // string
  public $COD_Marque; // string
  public $LIB_Marque; // string
  public $COD_Modele; // string
  public $LIB_Modele; // string
  public $DAT_Entree; // string
  public $DAT_SortieReelle; // string
  public $DAT_Sortieprevue; // string
  public $DAT_DerniereDecl; // string
  public $Prix; // string
  public $NUM_Serie; // string
  public $DAT_PREMCIR; // string
}

class ValiderEtatParc_TAB_VehiculesRow {
  public $COD_Parc; // string
  public $LIB_Immat; // string
}

class getPolicesARF_listePolicesRow {
  public $COD_Societe; // string
  public $COD_AgenceLoc; // string
  public $COD_Canal; // string
  public $NUM_Police; // string
  public $COD_Genre; // string
  public $DAT_DebutLoc; // string
  public $HRS_DebutLoc; // string
  public $DAT_FinLoc; // string
  public $HRS_FinLoc; // string
  public $NBR_JourLoc; // string
  public $COD_ProduitAss; // string
  public $PRIX_CLI; // string
  public $LIB_Immat; // string
  public $LIB_NomCdtPrinc; // string
  public $LIB_PrenomCdtPrinc; // string
  public $LIB_SexeCdtPrinc; // string
  public $NB_jourloc; // int
  public $NB_jourfac; // int
  public $NB_jourlocfac; // int
  public $NB_jassfac; // int
  public $cdprestqte; // int
  public $cTypePrest; // string
  public $cCDPRESTcodePrest; // string
  public $cCDPRESTmontant; // string
  public $cCDPRESTchoix; // string
  public $cStatut; // string
  public $DAT_DebutLoc_ctl; // int
  public $DAT_FinLoc_ctl; // int
  public $cPointDeFacturation; // int
  public $NUM_Contrat; // string
  public $COD_Canal2; // string
  public $COD_PREST; // string
  public $LIB_PREST; // string
  public $BLOC_NUMVOUCHER; // string
  public $NUM_RESA; // string
}

class getTarifCd_lstTarifCdRow {
  public $cMontant; // string
  public $cCodeDevise; // string
  public $cCodeTarif; // string
  public $cCodeDuree; // string
  public $cCattarif; // string
  public $cTauxRemiseMaxi; // string
}

class getClientsB1_listeClientRow {
  public $cCode; // string
  public $cNom; // string
  public $cPrenom; // string
  public $cAdr1; // string
  public $cAdr2; // string
  public $cCodePostale; // string
  public $cVille; // string
  public $cTelephone; // string
  public $cAlert; // string
  public $cType; // string
}

class LireOpeEtatParcEda_TAB_VehiculesRow {
  public $COD_AgenceAffec; // string
  public $COD_Parc; // string
  public $LIB_Immat; // string
  public $COD_Marque; // string
  public $COD_Modele; // string
  public $COD_Genre; // string
  public $COD_Categorie; // string
  public $DAT_Entree; // string
  public $DAT_SortieReelle; // string
  public $DAT_Sortieprevue; // string
}

class LireOpeEtatParcAgence_TAB_VehiculesRow {
  public $COD_AgenceAffec; // string
  public $COD_Parc; // string
  public $LIB_Immat; // string
  public $COD_Marque; // string
  public $COD_Modele; // string
  public $COD_Genre; // string
  public $COD_Categorie; // string
  public $DAT_Entree; // string
  public $DAT_SortieReelle; // string
  public $DAT_Sortieprevue; // string
}

class getPlanningCdDispo_WPLANNINGCDRow {
  public $groupe; // string
  public $cat; // string
  public $dispo; // int
  public $total; // int
}

class getPlanningCdDispo_WDISPOPARCSRow {
  public $agence; // string
  public $cat; // string
  public $marquemodele; // string
  public $immat; // string
}


/**
 * WSTKRPCEncodedService class
 *
 *
 *
 * @author    {author}
 * @copyright {copyright}
 * @package   {package}
 */
class WSTKRPCEncodedService extends SoapClient {

  private static $classmap = array(
                                    'FaultDetail' => 'FaultDetail',
                                    'getStationsADA_listeStationsRow' => 'getStationsADA_listeStationsRow',
                                    'getHorairesStationsADA_listeHorairesRow' => 'getHorairesStationsADA_listeHorairesRow',
                                    'getCategoriesVehicules_listeCategoriesRow' => 'getCategoriesVehicules_listeCategoriesRow',
                                    'getEquipementsLocation_listeEquipementsRow' => 'getEquipementsLocation_listeEquipementsRow',
                                    'getPrestationsLocation_listePrestationsRow' => 'getPrestationsLocation_listePrestationsRow',
                                    'putReservationSimple_listeEquipementsRow' => 'putReservationSimple_listeEquipementsRow',
                                    'putReservationSimple_listePrestationsInclusesRow' => 'putReservationSimple_listePrestationsInclusesRow',
                                    'putReservationSimple_listePrestationsExcluesRow' => 'putReservationSimple_listePrestationsExcluesRow',
                                    'putReservationSimple_listeCategoriesDisponiblesRow' => 'putReservationSimple_listeCategoriesDisponiblesRow',
                                    'getReservationSimple_listeEquipementsRow' => 'getReservationSimple_listeEquipementsRow',
                                    'getReservationSimple_listePrestationsInclusesRow' => 'getReservationSimple_listePrestationsInclusesRow',
                                    'getReservationSimple_listePrestationsExcluesRow' => 'getReservationSimple_listePrestationsExcluesRow',
                                    'getReservationSimple_listeCategoriesDisponiblesRow' => 'getReservationSimple_listeCategoriesDisponiblesRow',
                                    'getReservationSimpleByNrVoucher_listeEquipementsRow' => 'getReservationSimpleByNrVoucher_listeEquipementsRow',
                                    'getReservationSimpleByNrVoucher_listePrestationsInclusesRow' => 'getReservationSimpleByNrVoucher_listePrestationsInclusesRow',
                                    'getReservationSimpleByNrVoucher_listePrestationsExcluesRow' => 'getReservationSimpleByNrVoucher_listePrestationsExcluesRow',
                                    'getReservationSimpleByNrVoucher_listeCategoriesDisponiblesRow' => 'getReservationSimpleByNrVoucher_listeCategoriesDisponiblesRow',
                                    'postReservationSimple_listeEquipementsRow' => 'postReservationSimple_listeEquipementsRow',
                                    'postReservationSimple_listePrestationsInclusesRow' => 'postReservationSimple_listePrestationsInclusesRow',
                                    'postReservationSimple_listePrestationsExcluesRow' => 'postReservationSimple_listePrestationsExcluesRow',
                                    'postReservationSimple_listeCategoriesDisponiblesRow' => 'postReservationSimple_listeCategoriesDisponiblesRow',
                                    'getDisponibiliteVehicule_listeDisponibilityRow' => 'getDisponibiliteVehicule_listeDisponibilityRow',
                                    'VerifierSiUserExiste_TAB_SocietesRow' => 'VerifierSiUserExiste_TAB_SocietesRow',
                                    'VerifierSiUserExiste_TAB_AgencesRow' => 'VerifierSiUserExiste_TAB_AgencesRow',
                                    'VerifierSiUserExiste_TAB_AlertesRow' => 'VerifierSiUserExiste_TAB_AlertesRow',
                                    'VerifierSiUserExisteV2_TAB_SocietesRow' => 'VerifierSiUserExisteV2_TAB_SocietesRow',
                                    'VerifierSiUserExisteV2_TAB_AgencesRow' => 'VerifierSiUserExisteV2_TAB_AgencesRow',
                                    'VerifierSiUserExisteV2_TAB_AlertesRow' => 'VerifierSiUserExisteV2_TAB_AlertesRow',
                                    'ListerLesAgences_TAB_AgencesRow' => 'ListerLesAgences_TAB_AgencesRow',
                                    'ListerLesCategories_TAB_CategorieRow' => 'ListerLesCategories_TAB_CategorieRow',
                                    'ListerLesGenres_TAB_GenreRow' => 'ListerLesGenres_TAB_GenreRow',
                                    'ListerLesMarques_TAB_MarquesRow' => 'ListerLesMarques_TAB_MarquesRow',
                                    'ListerLesModeles_TAB_ModelesRow' => 'ListerLesModeles_TAB_ModelesRow',
                                    'ListerLesEnergies_TAB_EnergieRow' => 'ListerLesEnergies_TAB_EnergieRow',
                                    'CompterLesReservationsExternes_TAB_ReservationRow' => 'CompterLesReservationsExternes_TAB_ReservationRow',
                                    'ListerLesReservationsExternes_TAB_ReservationRow' => 'ListerLesReservationsExternes_TAB_ReservationRow',
                                    'ListerLesReservationsExternes_TAB_OPTRow' => 'ListerLesReservationsExternes_TAB_OPTRow',
                                    'getAgencesAdafr_listeAgencesAdafrRow' => 'getAgencesAdafr_listeAgencesAdafrRow',
                                    'getJaugesVariablesAdafr_listeJaugesVariablesAdafrRow' => 'getJaugesVariablesAdafr_listeJaugesVariablesAdafrRow',
                                    'getFermetureCialeAgence_listeFermetureCialeAgenceRow' => 'getFermetureCialeAgence_listeFermetureCialeAgenceRow',
                                    'putReservationAdafrv5_lstPrestRow' => 'putReservationAdafrv5_lstPrestRow',
                                    'putReservationAdafrv2_lstPrestRow' => 'putReservationAdafrv2_lstPrestRow',
                                    'putReservationAdafrv3_lstPrestRow' => 'putReservationAdafrv3_lstPrestRow',
                                    'putReservationAdafrv4_lstPrestRow' => 'putReservationAdafrv4_lstPrestRow',
                                    'getDateEnlevementOptionsAdafr_lstPrestDemenagementRow' => 'getDateEnlevementOptionsAdafr_lstPrestDemenagementRow',
                                    'updateOptionsAdafr_lstPrestRow' => 'updateOptionsAdafr_lstPrestRow',
                                    'EnregistrerEnlevementColis_TAB_PrestRow' => 'EnregistrerEnlevementColis_TAB_PrestRow',
                                    'ListerLesVehiculesProlongeables_TAB_VehiculesRow' => 'ListerLesVehiculesProlongeables_TAB_VehiculesRow',
                                    'LireEnteteCmdsVehiculesEnCours_TAB_EnteteCmdsRow' => 'LireEnteteCmdsVehiculesEnCours_TAB_EnteteCmdsRow',
                                    'LireDetailCmdsVehiculesEnCours_TAB_CmdsRow' => 'LireDetailCmdsVehiculesEnCours_TAB_CmdsRow',
                                    'LireUneAgence_TAB_FermeturesRow' => 'LireUneAgence_TAB_FermeturesRow',
                                    'LireUneAgence_TAB_JoursRow' => 'LireUneAgence_TAB_JoursRow',
                                    'LireLesStopSales_TAB_StopSaleRow' => 'LireLesStopSales_TAB_StopSaleRow',
                                    'LireCAAgence_TAB_ChifaffaireRow' => 'LireCAAgence_TAB_ChifaffaireRow',
                                    'ListerLesUtilisateurs_TAB_UtilisateurRow' => 'ListerLesUtilisateurs_TAB_UtilisateurRow',
                                    'LireDetailReservationsExternes_TAB_RemarquesRow' => 'LireDetailReservationsExternes_TAB_RemarquesRow',
                                    'LireDetailReservationsExternes_TAB_OPTRow' => 'LireDetailReservationsExternes_TAB_OPTRow',
                                    'LireEnteteReservationsExternes_TAB_ReservationRow' => 'LireEnteteReservationsExternes_TAB_ReservationRow',
                                    'ValiderCAAgences_TAB_ChifaffaireRow' => 'ValiderCAAgences_TAB_ChifaffaireRow',
                                    'getAgencesAda_listeAgencesAdaRow' => 'getAgencesAda_listeAgencesAdaRow',
                                    'CalculerCaAgences_TAB_Facture_NonValideeRow' => 'CalculerCaAgences_TAB_Facture_NonValideeRow',
                                    'CalculerCaAgences_TAB_ChifaffaireRow' => 'CalculerCaAgences_TAB_ChifaffaireRow',
                                    'LireTotalCAAgence_TAB_ChifaffaireRow' => 'LireTotalCAAgence_TAB_ChifaffaireRow',
                                    'LireFacturesNonValidees_TAB_Facture_NonValideeRow' => 'LireFacturesNonValidees_TAB_Facture_NonValideeRow',
                                    'getOptionsAda_listeOptionsAdaRow' => 'getOptionsAda_listeOptionsAdaRow',
                                    'DeclarerRachatFranchisev3_TAB_ConductAdditionnelRow' => 'DeclarerRachatFranchisev3_TAB_ConductAdditionnelRow',
                                    'DeclarerRachatFranchisev2_TAB_ConductAdditionnelRow' => 'DeclarerRachatFranchisev2_TAB_ConductAdditionnelRow',
                                    'DeclarerRachatFranchiseCarpv2_TAB_ConductAdditionnelRow' => 'DeclarerRachatFranchiseCarpv2_TAB_ConductAdditionnelRow',
                                    'DeclarerRachatFranchise_TAB_ConductAdditionnelRow' => 'DeclarerRachatFranchise_TAB_ConductAdditionnelRow',
                                    'LireEtatParc_TAB_VehiculesRow' => 'LireEtatParc_TAB_VehiculesRow',
                                    'ValiderEtatParc_TAB_VehiculesRow' => 'ValiderEtatParc_TAB_VehiculesRow',
                                    'getPolicesARF_listePolicesRow' => 'getPolicesARF_listePolicesRow',
                                    'getTarifCd_lstTarifCdRow' => 'getTarifCd_lstTarifCdRow',
                                    'getClientsB1_listeClientRow' => 'getClientsB1_listeClientRow',
                                    'LireOpeEtatParcEda_TAB_VehiculesRow' => 'LireOpeEtatParcEda_TAB_VehiculesRow',
                                    'LireOpeEtatParcAgence_TAB_VehiculesRow' => 'LireOpeEtatParcAgence_TAB_VehiculesRow',
                                    'getPlanningCdDispo_WPLANNINGCDRow' => 'getPlanningCdDispo_WPLANNINGCDRow',
                                    'getPlanningCdDispo_WDISPOPARCSRow' => 'getPlanningCdDispo_WDISPOPARCSRow',
                                   );

  public function WSTKRPCEncodedService($wsdl = "http://sa01cy.ada.fr:25011/wsa/wsa1/wsdl?targetURI=urn:prgs:RPCEncoded", $options = array()) {
    foreach(self::$classmap as $key => $value) {
      if(!isset($options['classmap'][$key])) {
        $options['classmap'][$key] = $value;
      }
    }
    parent::__construct($wsdl, $options);
  }

  /**
   *
   *
   * @param
   * @return string
   */
  public function ping() {
    return $this->__soapCall('ping', array(),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUserAccount
   * @param string $cUserPwd
   * @return string
   */
  public function loginUNIPRO($cUserAccount, $cUserPwd) {
    return $this->__soapCall('loginUNIPRO', array($cUserAccount, $cUserPwd),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @return list(ArrayOfgetStationsADA_listeStationsRow $listeStations, string $cErrorCode, string $cErrorMessage)
   */
  public function getStationsADA($cUUID) {
    return $this->__soapCall('getStationsADA', array($cUUID),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @return list(ArrayOfgetHorairesStationsADA_listeHorairesRow $listeHoraires, string $cErrorCode, string $cErrorMessage)
   */
  public function getHorairesStationsADA($cUUID) {
    return $this->__soapCall('getHorairesStationsADA', array($cUUID),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @return list(ArrayOfgetCategoriesVehicules_listeCategoriesRow $listeCategories, string $cErrorCode, string $cErrorMessage)
   */
  public function getCategoriesVehicules($cUUID) {
    return $this->__soapCall('getCategoriesVehicules', array($cUUID),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @return list(ArrayOfgetEquipementsLocation_listeEquipementsRow $listeEquipements, string $cErrorCode, string $cErrorMessage)
   */
  public function getEquipementsLocation($cUUID) {
    return $this->__soapCall('getEquipementsLocation', array($cUUID),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @return list(ArrayOfgetPrestationsLocation_listePrestationsRow $listePrestations, string $cErrorCode, string $cErrorMessage)
   */
  public function getPrestationsLocation($cUUID) {
    return $this->__soapCall('getPrestationsLocation', array($cUUID),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param boolean $bRequest
   * @param string $cLastName
   * @param string $cFirstName
   * @param string $cClientID
   * @param string $cPickUpStationID
   * @param string $cPickupDate
   * @param string $cPickUpTime
   * @param string $cReturnStationID
   * @param string $cReturnDate
   * @param string $cReturnTime
   * @param string $cCarCategory
   * @param ArrayOfputReservationSimple_listeEquipementsRow $listeEquipements
   * @param string $cPhone
   * @param string $cContact
   * @param string $cCorporateNr
   * @param string $cRateCode
   * @param string $cAgenceID
   * @param string $cAccountNr
   * @param string $cMop
   * @param string $cFlightNr
   * @param string $cFrquentTraveller
   * @param string $cReference
   * @param string $cRemarks
   * @param string $cAmountValue
   * @param string $cAmountCurrency
   * @param string $cVoucherNr
   * @return list(boolean $bRequest, string $cLastName, string $cFirstName, string $cClientID, string $cPickUpStationID, string $cPickupDate, string $cPickUpTime, string $cReturnStationID, string $cReturnDate, string $cReturnTime, string $cCarCategory, ArrayOfputReservationSimple_listeEquipementsRow $listeEquipements, string $cPhone, string $cContact, string $cCorporateNr, string $cRateCode, string $cAgenceID, string $cAccountNr, string $cMop, string $cFlightNr, string $cFrquentTraveller, string $cReference, string $cRemarks, string $cAmountValue, string $cAmountCurrency, string $cVoucherNr, string $cStatutReservation, string $cReservationNr, string $cWarningCode, string $cWarningMessage, string $cErrorCode, string $cErrorMessage, string $cPrice, string $cDuration, ArrayOfputReservationSimple_listePrestationsInclusesRow $listePrestationsIncluses, ArrayOfputReservationSimple_listePrestationsExcluesRow $listePrestationsExclues, ArrayOfputReservationSimple_listeCategoriesDisponiblesRow $listeCategoriesDisponibles)
   */
  public function putReservationSimple($cUUID, $bRequest, $cLastName, $cFirstName, $cClientID, $cPickUpStationID, $cPickupDate, $cPickUpTime, $cReturnStationID, $cReturnDate, $cReturnTime, $cCarCategory, $listeEquipements, $cPhone, $cContact, $cCorporateNr, $cRateCode, $cAgenceID, $cAccountNr, $cMop, $cFlightNr, $cFrquentTraveller, $cReference, $cRemarks, $cAmountValue, $cAmountCurrency, $cVoucherNr) {
    return $this->__soapCall('putReservationSimple', array($cUUID, $bRequest, $cLastName, $cFirstName, $cClientID, $cPickUpStationID, $cPickupDate, $cPickUpTime, $cReturnStationID, $cReturnDate, $cReturnTime, $cCarCategory, $listeEquipements, $cPhone, $cContact, $cCorporateNr, $cRateCode, $cAgenceID, $cAccountNr, $cMop, $cFlightNr, $cFrquentTraveller, $cReference, $cRemarks, $cAmountValue, $cAmountCurrency, $cVoucherNr),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $cReservationNr
   * @return list(string $cReservationNr, string $cLastName, string $cFirstName, string $cClientID, string $cPickUpStationID, string $cPickupDate, string $cPickUpTime, string $cReturnStationID, string $cReturnDate, string $cReturnTime, string $cCarCategory, ArrayOfgetReservationSimple_listeEquipementsRow $listeEquipements, string $cPhone, string $cContact, string $cCorporateNr, string $cRateCode, string $cAgenceID, string $cAccountNr, string $cMop, string $cFlightNr, string $cFrquentTraveller, string $cReference, string $cRemarks, string $cAmountValue, string $cAmountCurrency, string $cVoucherNr, string $cStatutReservation, string $cWarningCode, string $cWarningMessage, string $cErrorCode, string $cErrorMessage, string $cPrice, string $cDuration, ArrayOfgetReservationSimple_listePrestationsInclusesRow $listePrestationsIncluses, ArrayOfgetReservationSimple_listePrestationsExcluesRow $listePrestationsExclues, ArrayOfgetReservationSimple_listeCategoriesDisponiblesRow $listeCategoriesDisponibles)
   */
  public function getReservationSimple($cUUID, $cReservationNr) {
    return $this->__soapCall('getReservationSimple', array($cUUID, $cReservationNr),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $cVoucherNr
   * @return list(string $cVoucherNr, string $cReservationNr, string $cLastName, string $cFirstName, string $cClientID, string $cPickUpStationID, string $cPickupDate, string $cPickUpTime, string $cReturnStationID, string $cReturnDate, string $cReturnTime, string $cCarCategory, ArrayOfgetReservationSimpleByNrVoucher_listeEquipementsRow $listeEquipements, string $cPhone, string $cContact, string $cCorporateNr, string $cRateCode, string $cAgenceID, string $cAccountNr, string $cMop, string $cFlightNr, string $cFrquentTraveller, string $cReference, string $cRemarks, string $cAmountValue, string $cAmountCurrency, string $cStatutReservation, string $cWarningCode, string $cWarningMessage, string $cErrorCode, string $cErrorMessage, string $cPrice, string $cDuration, ArrayOfgetReservationSimpleByNrVoucher_listePrestationsInclusesRow $listePrestationsIncluses, ArrayOfgetReservationSimpleByNrVoucher_listePrestationsExcluesRow $listePrestationsExclues, ArrayOfgetReservationSimpleByNrVoucher_listeCategoriesDisponiblesRow $listeCategoriesDisponibles)
   */
  public function getReservationSimpleByNrVoucher($cUUID, $cVoucherNr) {
    return $this->__soapCall('getReservationSimpleByNrVoucher', array($cUUID, $cVoucherNr),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $cReservationNr
   * @return list(string $cReservationNr, string $cStatutReservation, string $cErrorCode, string $cErrorMessage)
   */
  public function delReservationSimple($cUUID, $cReservationNr) {
    return $this->__soapCall('delReservationSimple', array($cUUID, $cReservationNr),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $cReservationNr
   * @param string $cLastName
   * @param string $cFirstName
   * @param string $cClientID
   * @param string $cPickUpStationID
   * @param string $cPickupDate
   * @param string $cPickUpTime
   * @param string $cReturnStationID
   * @param string $cReturnDate
   * @param string $cReturnTime
   * @param string $cCarCategory
   * @param ArrayOfpostReservationSimple_listeEquipementsRow $listeEquipements
   * @param string $cPhone
   * @param string $cContact
   * @param string $cCorporateNr
   * @param string $cRateCode
   * @param string $cAgenceID
   * @param string $cAccountNr
   * @param string $cMop
   * @param string $cFlightNr
   * @param string $cFrquentTraveller
   * @param string $cReference
   * @param string $cRemarks
   * @param string $cAmountValue
   * @param string $cAmountCurrency
   * @param string $cVoucherNr
   * @return list(string $cReservationNr, string $cLastName, string $cFirstName, string $cClientID, string $cPickUpStationID, string $cPickupDate, string $cPickUpTime, string $cReturnStationID, string $cReturnDate, string $cReturnTime, string $cCarCategory, ArrayOfpostReservationSimple_listeEquipementsRow $listeEquipements, string $cPhone, string $cContact, string $cCorporateNr, string $cRateCode, string $cAgenceID, string $cAccountNr, string $cMop, string $cFlightNr, string $cFrquentTraveller, string $cReference, string $cRemarks, string $cAmountValue, string $cAmountCurrency, string $cVoucherNr, string $cStatutReservation, string $cWarningCode, string $cWarningMessage, string $cErrorCode, string $cErrorMessage, string $cPrice, string $cDuration, ArrayOfpostReservationSimple_listePrestationsInclusesRow $listePrestationsIncluses, ArrayOfpostReservationSimple_listePrestationsExcluesRow $listePrestationsExclues, ArrayOfpostReservationSimple_listeCategoriesDisponiblesRow $listeCategoriesDisponibles)
   */
  public function postReservationSimple($cUUID, $cReservationNr, $cLastName, $cFirstName, $cClientID, $cPickUpStationID, $cPickupDate, $cPickUpTime, $cReturnStationID, $cReturnDate, $cReturnTime, $cCarCategory, $listeEquipements, $cPhone, $cContact, $cCorporateNr, $cRateCode, $cAgenceID, $cAccountNr, $cMop, $cFlightNr, $cFrquentTraveller, $cReference, $cRemarks, $cAmountValue, $cAmountCurrency, $cVoucherNr) {
    return $this->__soapCall('postReservationSimple', array($cUUID, $cReservationNr, $cLastName, $cFirstName, $cClientID, $cPickUpStationID, $cPickupDate, $cPickUpTime, $cReturnStationID, $cReturnDate, $cReturnTime, $cCarCategory, $listeEquipements, $cPhone, $cContact, $cCorporateNr, $cRateCode, $cAgenceID, $cAccountNr, $cMop, $cFlightNr, $cFrquentTraveller, $cReference, $cRemarks, $cAmountValue, $cAmountCurrency, $cVoucherNr),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $cPickUpStationID
   * @param string $cPickupDate
   * @param string $cPickUpTime
   * @param string $cReturnStationID
   * @param string $cReturnDate
   * @param string $cReturnTime
   * @return list(string $cPickUpStationID, string $cPickupDate, string $cPickUpTime, string $cReturnStationID, string $cReturnDate, string $cReturnTime, string $cErrorCode, string $cErrorMessage, ArrayOfgetDisponibiliteVehicule_listeDisponibilityRow $listeDisponibility)
   */
  public function getDisponibiliteVehicule($cUUID, $cPickUpStationID, $cPickupDate, $cPickUpTime, $cReturnStationID, $cReturnDate, $cReturnTime) {
    return $this->__soapCall('getDisponibiliteVehicule', array($cUUID, $cPickUpStationID, $cPickupDate, $cPickUpTime, $cReturnStationID, $cReturnDate, $cReturnTime),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Agence
   * @param string $IDT_Utilisateur
   * @param string $LIB_MotDePasse
   * @return list(int $CodRetour, string $LibelleRetour, string $LIB_Nom, int $NBR_Societes, ArrayOfVerifierSiUserExiste_TAB_SocietesRow $TAB_Societes, int $NBR_Lignes, ArrayOfVerifierSiUserExiste_TAB_AgencesRow $TAB_Agences, string $COD_Societe_Agent, string $COD_base, string $COD_Groupe, int $NBR_Alertes, ArrayOfVerifierSiUserExiste_TAB_AlertesRow $TAB_Alertes, string $COD_AgenceSiege)
   */
  public function VerifierSiUserExiste($cUUID, $COD_Agence, $IDT_Utilisateur, $LIB_MotDePasse) {
    return $this->__soapCall('VerifierSiUserExiste', array($cUUID, $COD_Agence, $IDT_Utilisateur, $LIB_MotDePasse),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Agence
   * @param string $IDT_Utilisateur
   * @param string $LIB_MotDePasse
   * @return list(int $CodRetour, string $LibelleRetour, string $LIB_Nom, int $NBR_Societes, ArrayOfVerifierSiUserExisteV2_TAB_SocietesRow $TAB_Societes, int $NBR_Lignes, ArrayOfVerifierSiUserExisteV2_TAB_AgencesRow $TAB_Agences, string $COD_Societe_Agent, string $COD_base, string $COD_Groupe, int $NBR_Alertes, ArrayOfVerifierSiUserExisteV2_TAB_AlertesRow $TAB_Alertes, string $COD_AgenceSiege)
   */
  public function VerifierSiUserExisteV2($cUUID, $COD_Agence, $IDT_Utilisateur, $LIB_MotDePasse) {
    return $this->__soapCall('VerifierSiUserExisteV2', array($cUUID, $COD_Agence, $IDT_Utilisateur, $LIB_MotDePasse),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Base
   * @param string $COD_Societe
   * @return list(int $CodRetour, string $LibelleRetour, int $NBR_Lignes, ArrayOfListerLesAgences_TAB_AgencesRow $TAB_Agences)
   */
  public function ListerLesAgences($cUUID, $COD_Base, $COD_Societe) {
    return $this->__soapCall('ListerLesAgences', array($cUUID, $COD_Base, $COD_Societe),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Base
   * @param string $COD_Genre
   * @return list(int $CodRetour, string $LibelleRetour, int $NBR_Lignes, ArrayOfListerLesCategories_TAB_CategorieRow $TAB_Categorie)
   */
  public function ListerLesCategories($cUUID, $COD_Base, $COD_Genre) {
    return $this->__soapCall('ListerLesCategories', array($cUUID, $COD_Base, $COD_Genre),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Base
   * @return list(int $CodRetour, string $LibelleRetour, int $NBR_Lignes, ArrayOfListerLesGenres_TAB_GenreRow $TAB_Genre)
   */
  public function ListerLesGenres($cUUID, $COD_Base) {
    return $this->__soapCall('ListerLesGenres', array($cUUID, $COD_Base),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Base
   * @return list(int $CodRetour, string $LibelleRetour, int $NBR_Lignes, ArrayOfListerLesMarques_TAB_MarquesRow $TAB_Marques)
   */
  public function ListerLesMarques($cUUID, $COD_Base) {
    return $this->__soapCall('ListerLesMarques', array($cUUID, $COD_Base),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Base
   * @param string $COD_marque
   * @return list(int $CodRetour, string $LibelleRetour, int $NBR_Lignes, ArrayOfListerLesModeles_TAB_ModelesRow $TAB_Modeles)
   */
  public function ListerLesModeles($cUUID, $COD_Base, $COD_marque) {
    return $this->__soapCall('ListerLesModeles', array($cUUID, $COD_Base, $COD_marque),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Base
   * @return list(int $CodRetour, string $LibelleRetour, int $NBR_Lignes, ArrayOfListerLesEnergies_TAB_EnergieRow $TAB_Energie)
   */
  public function ListerLesEnergies($cUUID, $COD_Base) {
    return $this->__soapCall('ListerLesEnergies', array($cUUID, $COD_Base),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Base
   * @param string $COD_Societe
   * @param string $LIB_immat
   * @return list(int $CodRetour, string $LibelleRetour)
   */
  public function VerifierUneImmat($cUUID, $COD_Base, $COD_Societe, $LIB_immat) {
    return $this->__soapCall('VerifierUneImmat', array($cUUID, $COD_Base, $COD_Societe, $LIB_immat),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Base
   * @param string $COD_societe
   * @param string $COD_agence
   * @param string $DAT_Debut
   * @param string $DAT_Fin
   * @return list(int $CodRetour, string $LibelleRetour, int $NBR_Lignes, ArrayOfCompterLesReservationsExternes_TAB_ReservationRow $TAB_Reservation)
   */
  public function CompterLesReservationsExternes($cUUID, $COD_Base, $COD_societe, $COD_agence, $DAT_Debut, $DAT_Fin) {
    return $this->__soapCall('CompterLesReservationsExternes', array($cUUID, $COD_Base, $COD_societe, $COD_agence, $DAT_Debut, $DAT_Fin),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Base
   * @param string $COD_societe
   * @param string $COD_agence
   * @param string $COD_Type
   * @return list(int $CodRetour, string $LibelleRetour, int $NBR_Lignes, ArrayOfListerLesReservationsExternes_TAB_ReservationRow $TAB_Reservation, int $NBR_Opt, ArrayOfListerLesReservationsExternes_TAB_OPTRow $TAB_OPT)
   */
  public function ListerLesReservationsExternes($cUUID, $COD_Base, $COD_societe, $COD_agence, $COD_Type) {
    return $this->__soapCall('ListerLesReservationsExternes', array($cUUID, $COD_Base, $COD_societe, $COD_agence, $COD_Type),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Base
   * @param string $COD_societe
   * @param string $COD_agence
   * @param string $NUM_Resa
   * @return list(int $CodRetour, string $LibelleRetour)
   */
  public function PasserLaReservationExterneLu($cUUID, $COD_Base, $COD_societe, $COD_agence, $NUM_Resa) {
    return $this->__soapCall('PasserLaReservationExterneLu', array($cUUID, $COD_Base, $COD_societe, $COD_agence, $NUM_Resa),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $cCodeAgence
   * @return list(ArrayOfgetAgencesAdafr_listeAgencesAdafrRow $listeAgencesAdafr, string $cErrorCode, string $cErrorMessage)
   */
  public function getAgencesAdafr($cUUID, $cCodeAgence) {
    return $this->__soapCall('getAgencesAdafr', array($cUUID, $cCodeAgence),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $cCodeAgence
   * @param string $cCategorie
   * @return list(ArrayOfgetJaugesVariablesAdafr_listeJaugesVariablesAdafrRow $listeJaugesVariablesAdafr, string $cErrorCode, string $cErrorMessage)
   */
  public function getJaugesVariablesAdafr($cUUID, $cCodeAgence, $cCategorie) {
    return $this->__soapCall('getJaugesVariablesAdafr', array($cUUID, $cCodeAgence, $cCategorie),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $cCodeAgence
   * @return list(ArrayOfgetFermetureCialeAgence_listeFermetureCialeAgenceRow $listeFermetureCialeAgence, string $cErrorCode, string $cErrorMessage)
   */
  public function getFermetureCialeAgence($cUUID, $cCodeAgence) {
    return $this->__soapCall('getFermetureCialeAgence', array($cUUID, $cCodeAgence),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $cCodeAgence
   * @param string $cDateDepart
   * @param string $cHeureDepart
   * @param string $cDateRetour
   * @param string $cHeureRetour
   * @param string $cCategorie
   * @param string $cKmInclus
   * @param string $cNom
   * @param string $cPrenom
   * @param string $cSexe
   * @param string $cDateNaissance
   * @param string $cLieuNaissance
   * @param string $cTelephone
   * @param string $cTelPortable
   * @param string $cAdr1
   * @param string $cAdr2
   * @param string $cVille
   * @param string $cCodepostal
   * @param string $cPays
   * @param string $cMontant
   * @param string $cCodeDevise
   * @param string $cNumPermis
   * @param string $cTypePermis
   * @param string $cDatePermis
   * @param string $cLieuPermis
   * @param string $cNumResaAdafr
   * @param string $cCodeClient
   * @param string $cRemarks
   * @return list(string $cCodeAgence, string $cDateDepart, string $cHeureDepart, string $cDateRetour, string $cHeureRetour, string $cCategorie, string $cKmInclus, string $cNom, string $cPrenom, string $cSexe, string $cDateNaissance, string $cLieuNaissance, string $cTelephone, string $cTelPortable, string $cAdr1, string $cAdr2, string $cVille, string $cCodepostal, string $cPays, string $cMontant, string $cCodeDevise, string $cNumPermis, string $cTypePermis, string $cDatePermis, string $cLieuPermis, string $cNumResaAdafr, string $cCodeClient, string $cRemarks, string $cReservationNr, string $cErrorCode, string $cErrorMessage)
   */
  public function putReservationAdafr($cUUID, $cCodeAgence, $cDateDepart, $cHeureDepart, $cDateRetour, $cHeureRetour, $cCategorie, $cKmInclus, $cNom, $cPrenom, $cSexe, $cDateNaissance, $cLieuNaissance, $cTelephone, $cTelPortable, $cAdr1, $cAdr2, $cVille, $cCodepostal, $cPays, $cMontant, $cCodeDevise, $cNumPermis, $cTypePermis, $cDatePermis, $cLieuPermis, $cNumResaAdafr, $cCodeClient, $cRemarks) {
    return $this->__soapCall('putReservationAdafr', array($cUUID, $cCodeAgence, $cDateDepart, $cHeureDepart, $cDateRetour, $cHeureRetour, $cCategorie, $cKmInclus, $cNom, $cPrenom, $cSexe, $cDateNaissance, $cLieuNaissance, $cTelephone, $cTelPortable, $cAdr1, $cAdr2, $cVille, $cCodepostal, $cPays, $cMontant, $cCodeDevise, $cNumPermis, $cTypePermis, $cDatePermis, $cLieuPermis, $cNumResaAdafr, $cCodeClient, $cRemarks),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $cCodeAgence
   * @param string $cDateDepart
   * @param string $cHeureDepart
   * @param string $cDateRetour
   * @param string $cHeureRetour
   * @param string $cCategorie
   * @param string $cKmInclus
   * @param string $cNom
   * @param string $cPrenom
   * @param string $cSexe
   * @param string $cDateNaissance
   * @param string $cLieuNaissance
   * @param string $cTelephone
   * @param string $cTelPortable
   * @param string $cAdr1
   * @param string $cAdr2
   * @param string $cVille
   * @param string $cCodepostal
   * @param string $cPays
   * @param string $cMontant
   * @param string $cCodeDevise
   * @param string $cNumPermis
   * @param string $cTypePermis
   * @param string $cDatePermis
   * @param string $cLieuPermis
   * @param string $cEmail
   * @param string $cNumResaAdafr
   * @param string $cCodeClient
   * @param string $cRemarks
   * @param string $cReprise
   * @param string $cNomClient
   * @param string $cTypeClient
   * @param string $cReglementEnLigne
   * @param ArrayOfputReservationAdafrv5_lstPrestRow $lstPrest
   * @param string $cDureeResa
   * @return list(string $cCodeAgence, string $cDateDepart, string $cHeureDepart, string $cDateRetour, string $cHeureRetour, string $cCategorie, string $cKmInclus, string $cNom, string $cPrenom, string $cSexe, string $cDateNaissance, string $cLieuNaissance, string $cTelephone, string $cTelPortable, string $cAdr1, string $cAdr2, string $cVille, string $cCodepostal, string $cPays, string $cMontant, string $cCodeDevise, string $cNumPermis, string $cTypePermis, string $cDatePermis, string $cLieuPermis, string $cEmail, string $cNumResaAdafr, string $cCodeClient, string $cRemarks, string $cReprise, string $cNomClient, string $cTypeClient, string $cReglementEnLigne, ArrayOfputReservationAdafrv5_lstPrestRow $lstPrest, string $cDureeResa, string $cReservationNr, string $cErrorCode, string $cErrorMessage)
   */
  public function putReservationAdafrv5($cUUID, $cCodeAgence, $cDateDepart, $cHeureDepart, $cDateRetour, $cHeureRetour, $cCategorie, $cKmInclus, $cNom, $cPrenom, $cSexe, $cDateNaissance, $cLieuNaissance, $cTelephone, $cTelPortable, $cAdr1, $cAdr2, $cVille, $cCodepostal, $cPays, $cMontant, $cCodeDevise, $cNumPermis, $cTypePermis, $cDatePermis, $cLieuPermis, $cEmail, $cNumResaAdafr, $cCodeClient, $cRemarks, $cReprise, $cNomClient, $cTypeClient, $cReglementEnLigne, $lstPrest, $cDureeResa) {
    return $this->__soapCall('putReservationAdafrv5', array($cUUID, $cCodeAgence, $cDateDepart, $cHeureDepart, $cDateRetour, $cHeureRetour, $cCategorie, $cKmInclus, $cNom, $cPrenom, $cSexe, $cDateNaissance, $cLieuNaissance, $cTelephone, $cTelPortable, $cAdr1, $cAdr2, $cVille, $cCodepostal, $cPays, $cMontant, $cCodeDevise, $cNumPermis, $cTypePermis, $cDatePermis, $cLieuPermis, $cEmail, $cNumResaAdafr, $cCodeClient, $cRemarks, $cReprise, $cNomClient, $cTypeClient, $cReglementEnLigne, $lstPrest, $cDureeResa),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $cCodeAgence
   * @param string $cDateDepart
   * @param string $cHeureDepart
   * @param string $cDateRetour
   * @param string $cHeureRetour
   * @param string $cCategorie
   * @param string $cKmInclus
   * @param string $cNom
   * @param string $cPrenom
   * @param string $cSexe
   * @param string $cDateNaissance
   * @param string $cLieuNaissance
   * @param string $cTelephone
   * @param string $cTelPortable
   * @param string $cAdr1
   * @param string $cAdr2
   * @param string $cVille
   * @param string $cCodepostal
   * @param string $cPays
   * @param string $cMontant
   * @param string $cCodeDevise
   * @param string $cNumPermis
   * @param string $cTypePermis
   * @param string $cDatePermis
   * @param string $cLieuPermis
   * @param string $cNumResaAdafr
   * @param string $cCodeClient
   * @param string $cRemarks
   * @param string $cReprise
   * @param string $cNomClient
   * @param string $cTypeClient
   * @param string $cReglementEnLigne
   * @param ArrayOfputReservationAdafrv2_lstPrestRow $lstPrest
   * @return list(string $cCodeAgence, string $cDateDepart, string $cHeureDepart, string $cDateRetour, string $cHeureRetour, string $cCategorie, string $cKmInclus, string $cNom, string $cPrenom, string $cSexe, string $cDateNaissance, string $cLieuNaissance, string $cTelephone, string $cTelPortable, string $cAdr1, string $cAdr2, string $cVille, string $cCodepostal, string $cPays, string $cMontant, string $cCodeDevise, string $cNumPermis, string $cTypePermis, string $cDatePermis, string $cLieuPermis, string $cNumResaAdafr, string $cCodeClient, string $cRemarks, string $cReprise, string $cNomClient, string $cTypeClient, string $cReglementEnLigne, ArrayOfputReservationAdafrv2_lstPrestRow $lstPrest, string $cReservationNr, string $cErrorCode, string $cErrorMessage)
   */
  public function putReservationAdafrv2($cUUID, $cCodeAgence, $cDateDepart, $cHeureDepart, $cDateRetour, $cHeureRetour, $cCategorie, $cKmInclus, $cNom, $cPrenom, $cSexe, $cDateNaissance, $cLieuNaissance, $cTelephone, $cTelPortable, $cAdr1, $cAdr2, $cVille, $cCodepostal, $cPays, $cMontant, $cCodeDevise, $cNumPermis, $cTypePermis, $cDatePermis, $cLieuPermis, $cNumResaAdafr, $cCodeClient, $cRemarks, $cReprise, $cNomClient, $cTypeClient, $cReglementEnLigne, $lstPrest) {
    return $this->__soapCall('putReservationAdafrv2', array($cUUID, $cCodeAgence, $cDateDepart, $cHeureDepart, $cDateRetour, $cHeureRetour, $cCategorie, $cKmInclus, $cNom, $cPrenom, $cSexe, $cDateNaissance, $cLieuNaissance, $cTelephone, $cTelPortable, $cAdr1, $cAdr2, $cVille, $cCodepostal, $cPays, $cMontant, $cCodeDevise, $cNumPermis, $cTypePermis, $cDatePermis, $cLieuPermis, $cNumResaAdafr, $cCodeClient, $cRemarks, $cReprise, $cNomClient, $cTypeClient, $cReglementEnLigne, $lstPrest),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $cCodeAgence
   * @param string $cDateDepart
   * @param string $cHeureDepart
   * @param string $cDateRetour
   * @param string $cHeureRetour
   * @param string $cCategorie
   * @param string $cKmInclus
   * @param string $cNom
   * @param string $cPrenom
   * @param string $cSexe
   * @param string $cDateNaissance
   * @param string $cLieuNaissance
   * @param string $cTelephone
   * @param string $cTelPortable
   * @param string $cAdr1
   * @param string $cAdr2
   * @param string $cVille
   * @param string $cCodepostal
   * @param string $cPays
   * @param string $cMontant
   * @param string $cCodeDevise
   * @param string $cNumPermis
   * @param string $cTypePermis
   * @param string $cDatePermis
   * @param string $cLieuPermis
   * @param string $cNumResaAdafr
   * @param string $cCodeClient
   * @param string $cRemarks
   * @param string $cReprise
   * @param string $cNomClient
   * @param string $cTypeClient
   * @param string $cReglementEnLigne
   * @param ArrayOfputReservationAdafrv3_lstPrestRow $lstPrest
   * @param string $cDureeResa
   * @return list(string $cCodeAgence, string $cDateDepart, string $cHeureDepart, string $cDateRetour, string $cHeureRetour, string $cCategorie, string $cKmInclus, string $cNom, string $cPrenom, string $cSexe, string $cDateNaissance, string $cLieuNaissance, string $cTelephone, string $cTelPortable, string $cAdr1, string $cAdr2, string $cVille, string $cCodepostal, string $cPays, string $cMontant, string $cCodeDevise, string $cNumPermis, string $cTypePermis, string $cDatePermis, string $cLieuPermis, string $cNumResaAdafr, string $cCodeClient, string $cRemarks, string $cReprise, string $cNomClient, string $cTypeClient, string $cReglementEnLigne, ArrayOfputReservationAdafrv3_lstPrestRow $lstPrest, string $cDureeResa, string $cReservationNr, string $cErrorCode, string $cErrorMessage)
   */
  public function putReservationAdafrv3($cUUID, $cCodeAgence, $cDateDepart, $cHeureDepart, $cDateRetour, $cHeureRetour, $cCategorie, $cKmInclus, $cNom, $cPrenom, $cSexe, $cDateNaissance, $cLieuNaissance, $cTelephone, $cTelPortable, $cAdr1, $cAdr2, $cVille, $cCodepostal, $cPays, $cMontant, $cCodeDevise, $cNumPermis, $cTypePermis, $cDatePermis, $cLieuPermis, $cNumResaAdafr, $cCodeClient, $cRemarks, $cReprise, $cNomClient, $cTypeClient, $cReglementEnLigne, $lstPrest, $cDureeResa) {
    return $this->__soapCall('putReservationAdafrv3', array($cUUID, $cCodeAgence, $cDateDepart, $cHeureDepart, $cDateRetour, $cHeureRetour, $cCategorie, $cKmInclus, $cNom, $cPrenom, $cSexe, $cDateNaissance, $cLieuNaissance, $cTelephone, $cTelPortable, $cAdr1, $cAdr2, $cVille, $cCodepostal, $cPays, $cMontant, $cCodeDevise, $cNumPermis, $cTypePermis, $cDatePermis, $cLieuPermis, $cNumResaAdafr, $cCodeClient, $cRemarks, $cReprise, $cNomClient, $cTypeClient, $cReglementEnLigne, $lstPrest, $cDureeResa),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $cCodeAgence
   * @param string $cDateDepart
   * @param string $cHeureDepart
   * @param string $cDateRetour
   * @param string $cHeureRetour
   * @param string $cCategorie
   * @param string $cKmInclus
   * @param string $cNom
   * @param string $cPrenom
   * @param string $cSexe
   * @param string $cDateNaissance
   * @param string $cLieuNaissance
   * @param string $cTelephone
   * @param string $cTelPortable
   * @param string $cAdr1
   * @param string $cAdr2
   * @param string $cVille
   * @param string $cCodepostal
   * @param string $cPays
   * @param string $cMontant
   * @param string $cCodeDevise
   * @param string $cNumPermis
   * @param string $cTypePermis
   * @param string $cDatePermis
   * @param string $cLieuPermis
   * @param string $cEmail
   * @param string $cNumResaAdafr
   * @param string $cCodeClient
   * @param string $cRemarks
   * @param string $cReprise
   * @param string $cNomClient
   * @param string $cTypeClient
   * @param string $cReglementEnLigne
   * @param ArrayOfputReservationAdafrv4_lstPrestRow $lstPrest
   * @param string $cDureeResa
   * @param string $cOperationClient
   * @param string $cCodeTarif
   * @param string $cCodeDuree
   * @param string $cImmat
   * @return list(string $cCodeAgence, string $cDateDepart, string $cHeureDepart, string $cDateRetour, string $cHeureRetour, string $cCategorie, string $cKmInclus, string $cNom, string $cPrenom, string $cSexe, string $cDateNaissance, string $cLieuNaissance, string $cTelephone, string $cTelPortable, string $cAdr1, string $cAdr2, string $cVille, string $cCodepostal, string $cPays, string $cMontant, string $cCodeDevise, string $cNumPermis, string $cTypePermis, string $cDatePermis, string $cLieuPermis, string $cEmail, string $cNumResaAdafr, string $cCodeClient, string $cRemarks, string $cReprise, string $cNomClient, string $cTypeClient, string $cReglementEnLigne, ArrayOfputReservationAdafrv4_lstPrestRow $lstPrest, string $cDureeResa, string $cOperationClient, string $cCodeTarif, string $cCodeDuree, string $cImmat, string $cReservationNr, string $cErrorCode, string $cErrorMessage)
   */
  public function putReservationAdafrv4($cUUID, $cCodeAgence, $cDateDepart, $cHeureDepart, $cDateRetour, $cHeureRetour, $cCategorie, $cKmInclus, $cNom, $cPrenom, $cSexe, $cDateNaissance, $cLieuNaissance, $cTelephone, $cTelPortable, $cAdr1, $cAdr2, $cVille, $cCodepostal, $cPays, $cMontant, $cCodeDevise, $cNumPermis, $cTypePermis, $cDatePermis, $cLieuPermis, $cEmail, $cNumResaAdafr, $cCodeClient, $cRemarks, $cReprise, $cNomClient, $cTypeClient, $cReglementEnLigne, $lstPrest, $cDureeResa, $cOperationClient, $cCodeTarif, $cCodeDuree, $cImmat) {
    return $this->__soapCall('putReservationAdafrv4', array($cUUID, $cCodeAgence, $cDateDepart, $cHeureDepart, $cDateRetour, $cHeureRetour, $cCategorie, $cKmInclus, $cNom, $cPrenom, $cSexe, $cDateNaissance, $cLieuNaissance, $cTelephone, $cTelPortable, $cAdr1, $cAdr2, $cVille, $cCodepostal, $cPays, $cMontant, $cCodeDevise, $cNumPermis, $cTypePermis, $cDatePermis, $cLieuPermis, $cEmail, $cNumResaAdafr, $cCodeClient, $cRemarks, $cReprise, $cNomClient, $cTypeClient, $cReglementEnLigne, $lstPrest, $cDureeResa, $cOperationClient, $cCodeTarif, $cCodeDuree, $cImmat),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $cReservationNr
   * @return list(ArrayOfgetDateEnlevementOptionsAdafr_lstPrestDemenagementRow $lstPrestDemenagement, string $cErrorCode, string $cErrorMessage)
   */
  public function getDateEnlevementOptionsAdafr($cUUID, $cReservationNr) {
    return $this->__soapCall('getDateEnlevementOptionsAdafr', array($cUUID, $cReservationNr),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $cReservationNr
   * @param string $cMontant
   * @param string $cCodeDevise
   * @param ArrayOfupdateOptionsAdafr_lstPrestRow $lstPrest
   * @return list(string $cErrorCode, string $cErrorMessage)
   */
  public function updateOptionsAdafr($cUUID, $cReservationNr, $cMontant, $cCodeDevise, $lstPrest) {
    return $this->__soapCall('updateOptionsAdafr', array($cUUID, $cReservationNr, $cMontant, $cCodeDevise, $lstPrest),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Base
   * @param string $COD_Societe
   * @param string $COD_Agence
   * @param string $NUM_Resa
   * @param ArrayOfEnregistrerEnlevementColis_TAB_PrestRow $TAB_Prest
   * @return list(int $CodRetour, string $LibelleRetour)
   */
  public function EnregistrerEnlevementColis($cUUID, $COD_Base, $COD_Societe, $COD_Agence, $NUM_Resa, $TAB_Prest) {
    return $this->__soapCall('EnregistrerEnlevementColis', array($cUUID, $COD_Base, $COD_Societe, $COD_Agence, $NUM_Resa, $TAB_Prest),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $cReservationNr
   * @return list(string $cReservationNr, string $cErrorCode, string $cErrorMessage)
   */
  public function cancelReservationAdafr($cUUID, $cReservationNr) {
    return $this->__soapCall('cancelReservationAdafr', array($cUUID, $cReservationNr),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $cReservationNr
   * @param string $cMontantFraisDossier
   * @param string $cCodeDevise
   * @return list(string $cReservationNr, string $cMontantFraisDossier, string $cCodeDevise, string $cErrorCode, string $cErrorMessage)
   */
  public function cancelReservationAdafrv2($cUUID, $cReservationNr, $cMontantFraisDossier, $cCodeDevise) {
    return $this->__soapCall('cancelReservationAdafrv2', array($cUUID, $cReservationNr, $cMontantFraisDossier, $cCodeDevise),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Base
   * @param string $COD_Societe
   * @param string $COD_Agence
   * @param string $DAT_Recherche
   * @param string $LIB_IMMAT
   * @return list(int $CodRetour, string $LibelleRetour, int $NBR_Lignes, ArrayOfListerLesVehiculesProlongeables_TAB_VehiculesRow $TAB_Vehicules)
   */
  public function ListerLesVehiculesProlongeables($cUUID, $COD_Base, $COD_Societe, $COD_Agence, $DAT_Recherche, $LIB_IMMAT) {
    return $this->__soapCall('ListerLesVehiculesProlongeables', array($cUUID, $COD_Base, $COD_Societe, $COD_Agence, $DAT_Recherche, $LIB_IMMAT),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Base
   * @param string $COD_Societe
   * @param string $COD_Agence
   * @return list(int $CodRetour, string $LibelleRetour, int $NBR_Lignes, ArrayOfLireEnteteCmdsVehiculesEnCours_TAB_EnteteCmdsRow $TAB_EnteteCmds)
   */
  public function LireEnteteCmdsVehiculesEnCours($cUUID, $COD_Base, $COD_Societe, $COD_Agence) {
    return $this->__soapCall('LireEnteteCmdsVehiculesEnCours', array($cUUID, $COD_Base, $COD_Societe, $COD_Agence),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Base
   * @param string $COD_Societe
   * @param string $NUM_Cmd
   * @return list(int $CodRetour, string $LibelleRetour, int $NBR_Lignes, ArrayOfLireDetailCmdsVehiculesEnCours_TAB_CmdsRow $TAB_Cmds)
   */
  public function LireDetailCmdsVehiculesEnCours($cUUID, $COD_Base, $COD_Societe, $NUM_Cmd) {
    return $this->__soapCall('LireDetailCmdsVehiculesEnCours', array($cUUID, $COD_Base, $COD_Societe, $NUM_Cmd),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Base
   * @param string $COD_Societe
   * @param string $COD_Agence
   * @return list(int $CodRetour, string $LibelleRetour, string $LIB_NomResponsable, string $LIB_Adresse_1, string $LIB_Adresse_2, string $LIB_Codepostal, string $LIB_Ville, string $LIB_Pays, string $NUM_Telephone, string $NUM_Fax, int $NBR_Fermeture, ArrayOfLireUneAgence_TAB_FermeturesRow $TAB_Fermetures, int $NBR_Jours, ArrayOfLireUneAgence_TAB_JoursRow $TAB_Jours)
   */
  public function LireUneAgence($cUUID, $COD_Base, $COD_Societe, $COD_Agence) {
    return $this->__soapCall('LireUneAgence', array($cUUID, $COD_Base, $COD_Societe, $COD_Agence),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Base
   * @param string $COD_Societe
   * @param string $COD_Agence
   * @param string $LIB_NomResponsable
   * @param string $LIB_Adresse_1
   * @param string $LIB_Adresse_2
   * @param string $LIB_Codepostal
   * @param string $LIB_Ville
   * @param string $LIB_Pays
   * @param string $NUM_Telephone
   * @param string $NUM_Fax
   * @return list(int $CodRetour, string $LibelleRetour)
   */
  public function EnregistrerUneAgence($cUUID, $COD_Base, $COD_Societe, $COD_Agence, $LIB_NomResponsable, $LIB_Adresse_1, $LIB_Adresse_2, $LIB_Codepostal, $LIB_Ville, $LIB_Pays, $NUM_Telephone, $NUM_Fax) {
    return $this->__soapCall('EnregistrerUneAgence', array($cUUID, $COD_Base, $COD_Societe, $COD_Agence, $LIB_NomResponsable, $LIB_Adresse_1, $LIB_Adresse_2, $LIB_Codepostal, $LIB_Ville, $LIB_Pays, $NUM_Telephone, $NUM_Fax),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Base
   * @param string $COD_Societe
   * @param string $COD_Agence
   * @return list(int $CodRetour, string $LibelleRetour, int $NBR_Lignes, ArrayOfLireLesStopSales_TAB_StopSaleRow $TAB_StopSale)
   */
  public function LireLesStopSales($cUUID, $COD_Base, $COD_Societe, $COD_Agence) {
    return $this->__soapCall('LireLesStopSales', array($cUUID, $COD_Base, $COD_Societe, $COD_Agence),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Base
   * @param string $COD_Societe
   * @param string $COD_Action
   * @param string $COD_Agence
   * @param string $COD_Categorie
   * @param string $DAT_Debut
   * @param string $DAT_Fin
   * @param string $LIB_Type
   * @return list(int $CodRetour, string $LibelleRetour)
   */
  public function EnregistrerUnStopSale($cUUID, $COD_Base, $COD_Societe, $COD_Action, $COD_Agence, $COD_Categorie, $DAT_Debut, $DAT_Fin, $LIB_Type) {
    return $this->__soapCall('EnregistrerUnStopSale', array($cUUID, $COD_Base, $COD_Societe, $COD_Action, $COD_Agence, $COD_Categorie, $DAT_Debut, $DAT_Fin, $LIB_Type),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Base
   * @param string $COD_Societe
   * @param string $COD_Agence
   * @param string $LIB_Ouverture_1_AM
   * @param string $LIB_Ouverture_1_PM
   * @param string $LIB_Ouverture_2_AM
   * @param string $LIB_Ouverture_2_PM
   * @param string $LIB_Ouverture_3_AM
   * @param string $LIB_Ouverture_3_PM
   * @param string $LIB_Ouverture_4_AM
   * @param string $LIB_Ouverture_4_PM
   * @param string $LIB_Ouverture_5_AM
   * @param string $LIB_Ouverture_5_PM
   * @param string $LIB_Ouverture_6_AM
   * @param string $LIB_Ouverture_6_PM
   * @param string $LIB_Ouverture_7_AM
   * @param string $LIB_Ouverture_7_PM
   * @return list(int $CodRetour, string $LibelleRetour)
   */
  public function EnregistrerHorairesOuverturesAgence($cUUID, $COD_Base, $COD_Societe, $COD_Agence, $LIB_Ouverture_1_AM, $LIB_Ouverture_1_PM, $LIB_Ouverture_2_AM, $LIB_Ouverture_2_PM, $LIB_Ouverture_3_AM, $LIB_Ouverture_3_PM, $LIB_Ouverture_4_AM, $LIB_Ouverture_4_PM, $LIB_Ouverture_5_AM, $LIB_Ouverture_5_PM, $LIB_Ouverture_6_AM, $LIB_Ouverture_6_PM, $LIB_Ouverture_7_AM, $LIB_Ouverture_7_PM) {
    return $this->__soapCall('EnregistrerHorairesOuverturesAgence', array($cUUID, $COD_Base, $COD_Societe, $COD_Agence, $LIB_Ouverture_1_AM, $LIB_Ouverture_1_PM, $LIB_Ouverture_2_AM, $LIB_Ouverture_2_PM, $LIB_Ouverture_3_AM, $LIB_Ouverture_3_PM, $LIB_Ouverture_4_AM, $LIB_Ouverture_4_PM, $LIB_Ouverture_5_AM, $LIB_Ouverture_5_PM, $LIB_Ouverture_6_AM, $LIB_Ouverture_6_PM, $LIB_Ouverture_7_AM, $LIB_Ouverture_7_PM),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Base
   * @param string $COD_Societe
   * @param string $COD_Agence
   * @param string $COD_Action
   * @param string $VAL_Id
   * @param string $DAT_Debut
   * @param string $DAT_Fin
   * @param string $LIB_Raison
   * @return list(int $CodRetour, string $LibelleRetour)
   */
  public function EnregistrerPeriodeFermetureAgence($cUUID, $COD_Base, $COD_Societe, $COD_Agence, $COD_Action, $VAL_Id, $DAT_Debut, $DAT_Fin, $LIB_Raison) {
    return $this->__soapCall('EnregistrerPeriodeFermetureAgence', array($cUUID, $COD_Base, $COD_Societe, $COD_Agence, $COD_Action, $VAL_Id, $DAT_Debut, $DAT_Fin, $LIB_Raison),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Base
   * @param string $COD_Societe
   * @param string $COD_Agence
   * @param int $ANNEE
   * @param int $MOIS
   * @return list(int $CodRetour, string $LibelleRetour, int $NBR_Lignes, ArrayOfLireCAAgence_TAB_ChifaffaireRow $TAB_Chifaffaire)
   */
  public function LireCAAgence($cUUID, $COD_Base, $COD_Societe, $COD_Agence, $ANNEE, $MOIS) {
    return $this->__soapCall('LireCAAgence', array($cUUID, $COD_Base, $COD_Societe, $COD_Agence, $ANNEE, $MOIS),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Base
   * @param string $COD_Societe
   * @param string $COD_Agence
   * @param string $COD_GROUP
   * @return list(int $CodRetour, string $LibelleRetour, int $NBR_Lignes, ArrayOfListerLesUtilisateurs_TAB_UtilisateurRow $TAB_Utilisateur)
   */
  public function ListerLesUtilisateurs($cUUID, $COD_Base, $COD_Societe, $COD_Agence, $COD_GROUP) {
    return $this->__soapCall('ListerLesUtilisateurs', array($cUUID, $COD_Base, $COD_Societe, $COD_Agence, $COD_GROUP),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Base
   * @param string $COD_societe
   * @param string $COD_agence
   * @param string $NUM_Resa
   * @return list(int $CodRetour, string $LibelleRetour, string $NOM_Agence, string $COD_Categorie, string $COD_type, decimal $VAL_Voucher, string $NUM_Voucher, string $DAT_Depart, string $DAT_Retour, string $ETAT_Operation, int $FLG_Lu, decimal $NB_JLOC, int $NB_KM, string $NOM_Conducteur, string $PRENOM_Conducteur, string $ADR_Conducteur, int $AGE_Conducteur, string $DAT_Optention_Permis, string $COD_ClientPayeur, string $NOM_CLientPayeur, int $NBR_Remarques, ArrayOfLireDetailReservationsExternes_TAB_RemarquesRow $TAB_Remarques, int $NBR_Opt, ArrayOfLireDetailReservationsExternes_TAB_OPTRow $TAB_OPT)
   */
  public function LireDetailReservationsExternes($cUUID, $COD_Base, $COD_societe, $COD_agence, $NUM_Resa) {
    return $this->__soapCall('LireDetailReservationsExternes', array($cUUID, $COD_Base, $COD_societe, $COD_agence, $NUM_Resa),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Base
   * @param string $COD_Societe
   * @param string $COD_Agence
   * @return list(int $CodRetour, string $LibelleRetour, int $FLG_Astreinte, string $NUM_Tel, int $FLG_Sms)
   */
  public function LireUneAstreinteAgence($cUUID, $COD_Base, $COD_Societe, $COD_Agence) {
    return $this->__soapCall('LireUneAstreinteAgence', array($cUUID, $COD_Base, $COD_Societe, $COD_Agence),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Base
   * @param string $COD_Societe
   * @param string $COD_Agence
   * @param int $FLG_Astreinte
   * @param string $NUM_Tel
   * @param int $FLG_Sms
   * @return list(int $CodRetour, string $LibelleRetour)
   */
  public function EnregistrerUneAstreinteAgence($cUUID, $COD_Base, $COD_Societe, $COD_Agence, $FLG_Astreinte, $NUM_Tel, $FLG_Sms) {
    return $this->__soapCall('EnregistrerUneAstreinteAgence', array($cUUID, $COD_Base, $COD_Societe, $COD_Agence, $FLG_Astreinte, $NUM_Tel, $FLG_Sms),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Base
   * @param string $COD_societe
   * @param string $COD_agence
   * @param string $COD_Type
   * @param string $DAT_Debut
   * @param string $DAT_Fin
   * @return list(int $CodRetour, string $LibelleRetour, int $NBR_Lignes, ArrayOfLireEnteteReservationsExternes_TAB_ReservationRow $TAB_Reservation)
   */
  public function LireEnteteReservationsExternes($cUUID, $COD_Base, $COD_societe, $COD_agence, $COD_Type, $DAT_Debut, $DAT_Fin) {
    return $this->__soapCall('LireEnteteReservationsExternes', array($cUUID, $COD_Base, $COD_societe, $COD_agence, $COD_Type, $DAT_Debut, $DAT_Fin),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Base
   * @param string $COD_Marque
   * @param string $COD_Categorie
   * @return list(int $CodRetour, string $LibelleRetour)
   */
  public function ValiderUneMarque($cUUID, $COD_Base, $COD_Marque, $COD_Categorie) {
    return $this->__soapCall('ValiderUneMarque', array($cUUID, $COD_Base, $COD_Marque, $COD_Categorie),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Base
   * @param string $COD_Societe
   * @param string $COD_Agence
   * @return list(int $CodRetour, string $LibelleRetour, string $LIB_Agence, string $LIB_RaisonSociale)
   */
  public function LireRaisonSocialeAgence($cUUID, $COD_Base, $COD_Societe, $COD_Agence) {
    return $this->__soapCall('LireRaisonSocialeAgence', array($cUUID, $COD_Base, $COD_Societe, $COD_Agence),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Base
   * @param int $ANNEE
   * @param int $MOIS
   * @param ArrayOfValiderCAAgences_TAB_ChifaffaireRow $TAB_Chifaffaire
   * @return list(int $CodRetour, string $LibelleRetour)
   */
  public function ValiderCAAgences($cUUID, $COD_Base, $ANNEE, $MOIS, $TAB_Chifaffaire) {
    return $this->__soapCall('ValiderCAAgences', array($cUUID, $COD_Base, $ANNEE, $MOIS, $TAB_Chifaffaire),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Base
   * @param int $ANNEE
   * @param int $MOIS
   * @return list(int $CodRetour, string $LibelleRetour, decimal $VAL_CA, int $COD_STATUSDECL, string $LIB_STATUSDECL)
   */
  public function LireTotalCA($cUUID, $COD_Base, $ANNEE, $MOIS) {
    return $this->__soapCall('LireTotalCA', array($cUUID, $COD_Base, $ANNEE, $MOIS),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $cCodeAgence
   * @return list(ArrayOfgetAgencesAda_listeAgencesAdaRow $listeAgencesAda, string $cErrorCode, string $cErrorMessage)
   */
  public function getAgencesAda($cUUID, $cCodeAgence) {
    return $this->__soapCall('getAgencesAda', array($cUUID, $cCodeAgence),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Base
   * @param int $ANNEE
   * @param int $MOIS
   * @return list(int $CodRetour, string $LibelleRetour, int $NBR_LignesFac, ArrayOfCalculerCaAgences_TAB_Facture_NonValideeRow $TAB_Facture_NonValidee, int $NBR_Lignes, ArrayOfCalculerCaAgences_TAB_ChifaffaireRow $TAB_Chifaffaire, decimal $VAL_CA, string $DAT_Limit)
   */
  public function CalculerCaAgences($cUUID, $COD_Base, $ANNEE, $MOIS) {
    return $this->__soapCall('CalculerCaAgences', array($cUUID, $COD_Base, $ANNEE, $MOIS),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Base
   * @param int $ANNEE
   * @param int $MOIS
   * @return list(int $CodRetour, string $LibelleRetour)
   */
  public function DemanderCAAgences($cUUID, $COD_Base, $ANNEE, $MOIS) {
    return $this->__soapCall('DemanderCAAgences', array($cUUID, $COD_Base, $ANNEE, $MOIS),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Base
   * @param int $ANNEE
   * @param int $MOIS
   * @return list(int $CodRetour, string $LibelleRetour, int $NBR_Lignes, ArrayOfLireTotalCAAgence_TAB_ChifaffaireRow $TAB_Chifaffaire)
   */
  public function LireTotalCAAgence($cUUID, $COD_Base, $ANNEE, $MOIS) {
    return $this->__soapCall('LireTotalCAAgence', array($cUUID, $COD_Base, $ANNEE, $MOIS),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Base
   * @param int $ANNEE
   * @param int $MOIS
   * @return list(int $CodRetour, string $LibelleRetour, int $NBR_Lignes, ArrayOfLireFacturesNonValidees_TAB_Facture_NonValideeRow $TAB_Facture_NonValidee)
   */
  public function LireFacturesNonValidees($cUUID, $COD_Base, $ANNEE, $MOIS) {
    return $this->__soapCall('LireFacturesNonValidees', array($cUUID, $COD_Base, $ANNEE, $MOIS),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @return list(ArrayOfgetOptionsAda_listeOptionsAdaRow $listeOptionsAda, string $cErrorCode, string $cErrorMessage)
   */
  public function getOptionsAda($cUUID) {
    return $this->__soapCall('getOptionsAda', array($cUUID),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Action
   * @param string $COD_Societe
   * @param string $COD_AgencePrin
   * @param string $COD_AgenceLoc
   * @param string $COD_Canal
   * @param string $NUM_Contrat
   * @param string $COD_Genre
   * @param string $COD_Categorie
   * @param string $DAT_DebutLoc
   * @param string $HRS_DebutLoc
   * @param string $DAT_FinLoc
   * @param string $HRS_FinLoc
   * @param int $NBR_JourLoc
   * @param string $COD_ProduitAss
   * @param string $PRIX_CLI
   * @param string $LIB_Immat
   * @param string $LIB_NomCdtPrinc
   * @param string $LIB_PrenomCdtPrinc
   * @param string $LIB_SexeCdtPrinc
   * @param string $DAT_PermisCdPrinc
   * @param string $NUM_PermisCdPrinc
   * @param string $DAT_Resa
   * @param string $PRIX_Franchise
   * @param int $NBR_ConductAdditionnel
   * @param ArrayOfDeclarerRachatFranchisev3_TAB_ConductAdditionnelRow $TAB_ConductAdditionnel
   * @return list(string $COD_MsgVespiren, string $LIB_MsgVespiren, string $TYP_MsgVespiren, string $NUM_Police_ADA, string $NUM_Police_VESP, string $DAT_Trt_VESP, int $CodRetour, string $LibelleRetour)
   */
  public function DeclarerRachatFranchisev3($cUUID, $COD_Action, $COD_Societe, $COD_AgencePrin, $COD_AgenceLoc, $COD_Canal, $NUM_Contrat, $COD_Genre, $COD_Categorie, $DAT_DebutLoc, $HRS_DebutLoc, $DAT_FinLoc, $HRS_FinLoc, $NBR_JourLoc, $COD_ProduitAss, $PRIX_CLI, $LIB_Immat, $LIB_NomCdtPrinc, $LIB_PrenomCdtPrinc, $LIB_SexeCdtPrinc, $DAT_PermisCdPrinc, $NUM_PermisCdPrinc, $DAT_Resa, $PRIX_Franchise, $NBR_ConductAdditionnel, $TAB_ConductAdditionnel) {
    return $this->__soapCall('DeclarerRachatFranchisev3', array($cUUID, $COD_Action, $COD_Societe, $COD_AgencePrin, $COD_AgenceLoc, $COD_Canal, $NUM_Contrat, $COD_Genre, $COD_Categorie, $DAT_DebutLoc, $HRS_DebutLoc, $DAT_FinLoc, $HRS_FinLoc, $NBR_JourLoc, $COD_ProduitAss, $PRIX_CLI, $LIB_Immat, $LIB_NomCdtPrinc, $LIB_PrenomCdtPrinc, $LIB_SexeCdtPrinc, $DAT_PermisCdPrinc, $NUM_PermisCdPrinc, $DAT_Resa, $PRIX_Franchise, $NBR_ConductAdditionnel, $TAB_ConductAdditionnel),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Action
   * @param string $COD_Societe
   * @param string $COD_AgencePrin
   * @param string $COD_AgenceLoc
   * @param string $COD_Canal
   * @param string $NUM_Contrat
   * @param string $COD_Genre
   * @param string $COD_Categorie
   * @param string $DAT_DebutLoc
   * @param string $HRS_DebutLoc
   * @param string $DAT_FinLoc
   * @param string $HRS_FinLoc
   * @param int $NBR_JourLoc
   * @param string $COD_ProduitAss
   * @param string $PRIX_CLI
   * @param string $LIB_Immat
   * @param string $LIB_NomCdtPrinc
   * @param string $LIB_PrenomCdtPrinc
   * @param string $LIB_SexeCdtPrinc
   * @param string $DAT_PermisCdPrinc
   * @param string $NUM_PermisCdPrinc
   * @param int $NBR_ConductAdditionnel
   * @param ArrayOfDeclarerRachatFranchisev2_TAB_ConductAdditionnelRow $TAB_ConductAdditionnel
   * @return list(string $COD_MsgVespiren, string $LIB_MsgVespiren, string $TYP_MsgVespiren, string $NUM_Police_ADA, string $NUM_Police_VESP, string $DAT_Trt_VESP, int $CodRetour, string $LibelleRetour)
   */
  public function DeclarerRachatFranchisev2($cUUID, $COD_Action, $COD_Societe, $COD_AgencePrin, $COD_AgenceLoc, $COD_Canal, $NUM_Contrat, $COD_Genre, $COD_Categorie, $DAT_DebutLoc, $HRS_DebutLoc, $DAT_FinLoc, $HRS_FinLoc, $NBR_JourLoc, $COD_ProduitAss, $PRIX_CLI, $LIB_Immat, $LIB_NomCdtPrinc, $LIB_PrenomCdtPrinc, $LIB_SexeCdtPrinc, $DAT_PermisCdPrinc, $NUM_PermisCdPrinc, $NBR_ConductAdditionnel, $TAB_ConductAdditionnel) {
    return $this->__soapCall('DeclarerRachatFranchisev2', array($cUUID, $COD_Action, $COD_Societe, $COD_AgencePrin, $COD_AgenceLoc, $COD_Canal, $NUM_Contrat, $COD_Genre, $COD_Categorie, $DAT_DebutLoc, $HRS_DebutLoc, $DAT_FinLoc, $HRS_FinLoc, $NBR_JourLoc, $COD_ProduitAss, $PRIX_CLI, $LIB_Immat, $LIB_NomCdtPrinc, $LIB_PrenomCdtPrinc, $LIB_SexeCdtPrinc, $DAT_PermisCdPrinc, $NUM_PermisCdPrinc, $NBR_ConductAdditionnel, $TAB_ConductAdditionnel),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Action
   * @param string $COD_Societe
   * @param string $COD_AgencePrin
   * @param string $COD_AgenceLoc
   * @param string $COD_Canal
   * @param string $NUM_Contrat
   * @param string $COD_Genre
   * @param string $COD_Categorie
   * @param string $DAT_DebutLoc
   * @param string $HRS_DebutLoc
   * @param string $DAT_FinLoc
   * @param string $HRS_FinLoc
   * @param int $NBR_JourLoc
   * @param string $COD_ProduitAss
   * @param string $PRIX_CLI
   * @param string $LIB_Immat
   * @param string $LIB_NomCdtPrinc
   * @param string $LIB_PrenomCdtPrinc
   * @param string $LIB_SexeCdtPrinc
   * @param string $DAT_PermisCdPrinc
   * @param string $NUM_PermisCdPrinc
   * @param int $NBR_ConductAdditionnel
   * @param ArrayOfDeclarerRachatFranchiseCarpv2_TAB_ConductAdditionnelRow $TAB_ConductAdditionnel
   * @return list(string $COD_MsgVespiren, string $LIB_MsgVespiren, string $TYP_MsgVespiren, string $NUM_Police_ADA, string $NUM_Police_VESP, string $DAT_Trt_VESP, int $CodRetour, string $LibelleRetour)
   */
  public function DeclarerRachatFranchiseCarpv2($cUUID, $COD_Action, $COD_Societe, $COD_AgencePrin, $COD_AgenceLoc, $COD_Canal, $NUM_Contrat, $COD_Genre, $COD_Categorie, $DAT_DebutLoc, $HRS_DebutLoc, $DAT_FinLoc, $HRS_FinLoc, $NBR_JourLoc, $COD_ProduitAss, $PRIX_CLI, $LIB_Immat, $LIB_NomCdtPrinc, $LIB_PrenomCdtPrinc, $LIB_SexeCdtPrinc, $DAT_PermisCdPrinc, $NUM_PermisCdPrinc, $NBR_ConductAdditionnel, $TAB_ConductAdditionnel) {
    return $this->__soapCall('DeclarerRachatFranchiseCarpv2', array($cUUID, $COD_Action, $COD_Societe, $COD_AgencePrin, $COD_AgenceLoc, $COD_Canal, $NUM_Contrat, $COD_Genre, $COD_Categorie, $DAT_DebutLoc, $HRS_DebutLoc, $DAT_FinLoc, $HRS_FinLoc, $NBR_JourLoc, $COD_ProduitAss, $PRIX_CLI, $LIB_Immat, $LIB_NomCdtPrinc, $LIB_PrenomCdtPrinc, $LIB_SexeCdtPrinc, $DAT_PermisCdPrinc, $NUM_PermisCdPrinc, $NBR_ConductAdditionnel, $TAB_ConductAdditionnel),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Action
   * @param string $COD_Societe
   * @param string $COD_AgencePrin
   * @param string $COD_AgenceLoc
   * @param string $COD_Canal
   * @param string $NUM_Contrat
   * @param string $COD_Genre
   * @param string $COD_Categorie
   * @param string $DAT_DebutLoc
   * @param string $HRS_DebutLoc
   * @param string $DAT_FinLoc
   * @param string $HRS_FinLoc
   * @param int $NBR_JourLoc
   * @param string $COD_ProduitAss
   * @param string $PRIX_CLI
   * @param string $LIB_Immat
   * @param string $LIB_NomCdtPrinc
   * @param string $LIB_PrenomCdtPrinc
   * @param string $LIB_SexeCdtPrinc
   * @param string $DAT_PermisCdPrinc
   * @param string $NUM_PermisCdPrinc
   * @param int $NBR_ConductAdditionnel
   * @param ArrayOfDeclarerRachatFranchise_TAB_ConductAdditionnelRow $TAB_ConductAdditionnel
   * @return list(string $COD_MsgVespiren, string $LIB_MsgVespiren, string $TYP_MsgVespiren, int $CodRetour, string $LibelleRetour)
   */
  public function DeclarerRachatFranchise($cUUID, $COD_Action, $COD_Societe, $COD_AgencePrin, $COD_AgenceLoc, $COD_Canal, $NUM_Contrat, $COD_Genre, $COD_Categorie, $DAT_DebutLoc, $HRS_DebutLoc, $DAT_FinLoc, $HRS_FinLoc, $NBR_JourLoc, $COD_ProduitAss, $PRIX_CLI, $LIB_Immat, $LIB_NomCdtPrinc, $LIB_PrenomCdtPrinc, $LIB_SexeCdtPrinc, $DAT_PermisCdPrinc, $NUM_PermisCdPrinc, $NBR_ConductAdditionnel, $TAB_ConductAdditionnel) {
    return $this->__soapCall('DeclarerRachatFranchise', array($cUUID, $COD_Action, $COD_Societe, $COD_AgencePrin, $COD_AgenceLoc, $COD_Canal, $NUM_Contrat, $COD_Genre, $COD_Categorie, $DAT_DebutLoc, $HRS_DebutLoc, $DAT_FinLoc, $HRS_FinLoc, $NBR_JourLoc, $COD_ProduitAss, $PRIX_CLI, $LIB_Immat, $LIB_NomCdtPrinc, $LIB_PrenomCdtPrinc, $LIB_SexeCdtPrinc, $DAT_PermisCdPrinc, $NUM_PermisCdPrinc, $NBR_ConductAdditionnel, $TAB_ConductAdditionnel),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Base
   * @param string $COD_Societe
   * @param string $COD_Agence
   * @param int $ANNEE
   * @param int $MOIS
   * @return list(int $CodRetour, string $LibelleRetour, int $NBR_Lignes)
   */
  public function LireTotalEtatParc($cUUID, $COD_Base, $COD_Societe, $COD_Agence, $ANNEE, $MOIS) {
    return $this->__soapCall('LireTotalEtatParc', array($cUUID, $COD_Base, $COD_Societe, $COD_Agence, $ANNEE, $MOIS),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Base
   * @param string $COD_Societe
   * @param string $COD_Agence
   * @param int $ANNEE
   * @param int $MOIS
   * @return list(int $CodRetour, string $LibelleRetour, int $NBR_Lignes, ArrayOfLireEtatParc_TAB_VehiculesRow $TAB_Vehicules)
   */
  public function LireEtatParc($cUUID, $COD_Base, $COD_Societe, $COD_Agence, $ANNEE, $MOIS) {
    return $this->__soapCall('LireEtatParc', array($cUUID, $COD_Base, $COD_Societe, $COD_Agence, $ANNEE, $MOIS),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Base
   * @param int $ANNEE
   * @param int $MOIS
   * @param int $NBR_Lignes
   * @param ArrayOfValiderEtatParc_TAB_VehiculesRow $TAB_Vehicules
   * @return list(int $CodRetour, string $LibelleRetour)
   */
  public function ValiderEtatParc($cUUID, $COD_Base, $ANNEE, $MOIS, $NBR_Lignes, $TAB_Vehicules) {
    return $this->__soapCall('ValiderEtatParc', array($cUUID, $COD_Base, $ANNEE, $MOIS, $NBR_Lignes, $TAB_Vehicules),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $cCodeSociete
   * @param string $COD_Agence
   * @param string $cNumeroPolice
   * @param boolean $lEstFacture
   * @param string $cAnneeMois
   * @return list(ArrayOfgetPolicesARF_listePolicesRow $listePolices, string $cErrorCode, string $cErrorMessage)
   */
  public function getPolicesARF($cUUID, $cCodeSociete, $COD_Agence, $cNumeroPolice, $lEstFacture, $cAnneeMois) {
    return $this->__soapCall('getPolicesARF', array($cUUID, $cCodeSociete, $COD_Agence, $cNumeroPolice, $lEstFacture, $cAnneeMois),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUid
   * @param string $cCodeAgence
   * @param string $cDateDepart
   * @param string $cHeureDepart
   * @param string $cDateRetour
   * @param string $cHeureRetour
   * @param string $cCodeGenre
   * @param string $lCategorie
   * @param string $cKmInclus
   * @param string $cNatureTarif
   * @return list(ArrayOfgetTarifCd_lstTarifCdRow $lstTarifCd, int $CodRetour, string $LibelleRetour)
   */
  public function getTarifCd($cUUid, $cCodeAgence, $cDateDepart, $cHeureDepart, $cDateRetour, $cHeureRetour, $cCodeGenre, $lCategorie, $cKmInclus, $cNatureTarif) {
    return $this->__soapCall('getTarifCd', array($cUUid, $cCodeAgence, $cDateDepart, $cHeureDepart, $cDateRetour, $cHeureRetour, $cCodeGenre, $lCategorie, $cKmInclus, $cNatureTarif),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUid
   * @param string $cCodeAgence
   * @param string $cNom
   * @param string $cPrenom
   * @param string $cVille
   * @param string $cTelephone
   * @return list(ArrayOfgetClientsB1_listeClientRow $listeClient, int $CodRetour, string $LibelleRetour)
   */
  public function getClientsB1($cUUid, $cCodeAgence, $cNom, $cPrenom, $cVille, $cTelephone) {
    return $this->__soapCall('getClientsB1', array($cUUid, $cCodeAgence, $cNom, $cPrenom, $cVille, $cTelephone),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Agence
   * @return list(string $cErrorCode, string $cErrorMessage, int $NBR_Lignes, ArrayOfLireOpeEtatParcEda_TAB_VehiculesRow $TAB_Vehicules)
   */
  public function LireOpeEtatParcEda($cUUID, $COD_Agence) {
    return $this->__soapCall('LireOpeEtatParcEda', array($cUUID, $COD_Agence),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @param string $COD_Agence
   * @return list(string $cErrorCode, string $cErrorMessage, int $NBR_Lignes, ArrayOfLireOpeEtatParcAgence_TAB_VehiculesRow $TAB_Vehicules)
   */
  public function LireOpeEtatParcAgence($cUUID, $COD_Agence) {
    return $this->__soapCall('LireOpeEtatParcAgence', array($cUUID, $COD_Agence),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUid
   * @param string $cCodeAgence
   * @param string $cCodeGenre
   * @param string $lCattarif
   * @param string $cDateDebut
   * @param string $cHeureDebut
   * @param string $cDateFin
   * @param string $cHeureFin
   * @param string $cDetail
   * @return list(ArrayOfgetPlanningCdDispo_WPLANNINGCDRow $WPLANNINGCD, ArrayOfgetPlanningCdDispo_WDISPOPARCSRow $WDISPOPARCS, int $CodRetour, string $LibelleRetour)
   */
  public function getPlanningCdDispo($cUUid, $cCodeAgence, $cCodeGenre, $lCattarif, $cDateDebut, $cHeureDebut, $cDateFin, $cHeureFin, $cDetail) {
    return $this->__soapCall('getPlanningCdDispo', array($cUUid, $cCodeAgence, $cCodeGenre, $lCattarif, $cDateDebut, $cHeureDebut, $cDateFin, $cHeureFin, $cDetail),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

  /**
   *
   *
   * @param string $cUUID
   * @return void
   */
  public function logoutUNIPRO($cUUID) {
    return $this->__soapCall('logoutUNIPRO', array($cUUID),       array(
            'uri' => 'urn:prgs:RPCEncoded',
            'soapaction' => ''
           )
      );
  }

}

?>