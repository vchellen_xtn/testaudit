<?
	session_start();
	require_once("secure.php"); 
	require_once("lib.php");
	// remettre � z�ro
	if ($_POST['reset'])
	{
		$_POST = array();
		$_SESSION['admin_search'] = array();
	}
	// faire une recherche
	if (count($_POST))
		$_SESSION['admin_search'] = $_POST;
	else if ($_SESSION['admin_search'])
	{
		foreach($_SESSION['admin_search'] as $k => $v)
		$_POST[$k] = $v;
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script type="text/javascript" src="<?=Page::getProtocol()?>://www.google.com/jsapi"></script>
	<script type="text/javascript">
		google.load('jquery', '1.4');
	</script>
	<link rel="stylesheet" type="text/css" href="../css/anytime.min.css" />
	<script type="text/javascript" src="../scripts/anytime.min.js"></script>
	<script type="text/javascript" src="admin.js"></script>
	<script type="text/javascript">
		function doSort(key, order)
		{
			var frm = document.forms['frm_search'];
			frm.key.value = key;
			frm.order.value = order;
			return doDisplay(frm, 1);
		}
		function doDisplay(frm, from)
		{
			if (frm.from_row)
				frm.from_row.value = from;
			frm.submit();
			return false;
		}
		// onload
		var dateFormat = '%d-%m-%Y';
		var dateConverter = new AnyTime.Converter({format: dateFormat});
		var dateOptions = getOptionsAnyTime(dateFormat, "depuis");

		// onload
		$(document).ready(function()
		{
			AnyTime.picker('date_from', dateOptions);
			dateOptions.labelTitle = "jusqu'�";
			AnyTime.picker('date_to', dateOptions);
		});
	</script>
	<style type="text/css">
		select,input { width: inherit;}
		label { padding-left: 2px; }
		label.col1 { padding-left: 0; float: left; width: 65px;}
	</style>
</head>
<body>
<?= SQLExporter::getForm(); ?>
<form id="frm_search" name="frm_search" action="" method="post">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre">
		<img src="img/puce.gif" align="absmiddle" hspace="1">
		Recherche r�servation
	</th>
</tr>
<tr>
	<td>
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<tr><td>
		<fieldset><legend>Identification</legend>
			<p>
				<label class="col1" for="id">R�servation</label><input type="text" value="<?=htmlspecialchars($_POST['id'])?>" id="id" name="id" size="8" maxlength="7"/>
				<label for="client">Client</label><input type="text" value="<?=htmlspecialchars($_POST['client'])?>" id="client" name="client" size="48" maxlength="128"/>
			</p>
			<p>
				<label class="col1" for="pdv">Pt de vente</label><input type="text" value="<?=htmlspecialchars($_POST['pdv'])?>" id="pdv" name="pdv" size="7" maxlength="6"/>
				<label for="coupon">Coupon</label><input type="text" value="<?=htmlspecialchars($_POST['coupon'])?>" id="coupon" name="coupon" size="16" maxlength="24"/>
				<label for="canal">Canal</label><? selectarray('canal', array('WEB'=>'WEB','TEL'=>'TEL'), $_POST['canal'], 'Tous'); ?>
				<label for="agent">Agent</label><input type="text" value="<?=htmlspecialchars($_POST['agent'])?>" id="agent" name="agent" size="16" maxlength="64"/>
			</p>
			<p>
				<label class="col1" for="campagne">Campagne</label>
				<? selectsql('campagne', "select id, concat(nom,' (#',id,' - ',ifnull(date_format(debut,'%d/%m/%y'),''),' - ',ifnull(date_format(fin,'%d/%m/%y'),''),')') nom from promotion_campagne order by nom", $_POST['campagne'], '--'); ?>
			</p>
		</fieldset>
		<fieldset><legend>Conditions</legend>
			<p>
				<label class="col1" for="forfait_type">Type de forfait</label>
				<? selectarray('forfait_type', array('ADAFR'=>'ADAFR','JEUNES'=>'JEUNES','LOCAPASS'=>'LOCAPASS'), $_POST['forfait_type'], '(Tous)')?>
			</p>
			<p>
				<? 
					$flds_date = array('paiement','annulation','creation','debut','fin'); 
					$flds_date = array_combine($flds_date, $flds_date); 
					foreach(array('date_from' => date('01-01-Y'),'date_to'=>date('31-12-Y')) as $k => $v)
					{
						if (!$_POST[$k])
							$_POST[$k] = $v;
					}
				?>
				<label class="col1" for="date">Date</label><? selectarray('date', $flds_date, $_POST['date']); ?>
				<? 
				foreach(array('date_from'=>'depuis','date_to'=>"jusqu'�") as $k => $v)
				{
					echo '<label for="'.$k.'">'.$v.'</label>';
					echo '<input type="text" id="'.$k.'" name="'.$k.'" size="10" maxlength="10" value="'.$_POST[$k].'">';
				}
				?>
			</p>
			<p>
				<label class="col1" for="statut">Statut</label><? selectarray('statut', array('P'=>'Pay�e (P)','A'=>'Annul�e (A)','X'=>'En erreur (X)','V'=>'Valid�e (V)'), $_POST['statut'], '(P, A et X)');?>
				<label for="total">Total</label><input type="text" id="total" name="total" size="7" mexlength="7" value="<?=$_POST['total']?>"/>&nbsp;�
			</p>
		</fieldset>
		<fieldset><legend>Localisation</legend>
			<p>
				<label class="col1" for="zone">Zone</label><? selectsql('zone', "select id, nom from zone order by nom", $_POST['zone'], '---');?>
				<label for="groupe">Groupe</label><? selectsql('groupe', "select id, UCASE(CONCAT(zone, ' - ', nom)) from zone_groupe where nom NOT IN ('CITER','JEUNES') order by zone, nom", $_POST['groupe'], '---');?>
				<label for="code_societe">Soci�t�</label><? selectsql('code_societe', "select distinct code_societe, concat(code_societe, ' - ', societe) from agence where nullif(code_societe,'') is not null order by code_societe", $_POST['code_societe'], '---');?>
			</p>
			<p>
				<label class="col1" for="agence">Agence</label>
				<? selectsql('agence', "select id, concat(id,' - ', nom) from agence order by id", $_POST['agence'], '---');?>
			</p>
		</fieldset>
		<fieldset><legend>V�hicule</legend>
			<p>
				<label class="col1" for="type">Type</label><? selectsql('type', "select distinct type, UCASE(type) from categorie order by type", $_POST['type'], '---');?>
				<label for="categorie">Cat�gorie</label><? selectsql('categorie', "select id, CONCAT(UCASE(type),' - ',mnem,' - ', nom) from categorie where publie=1 order by type, position, mnem", $_POST['categorie'], '---');?>
			</p>
		</fieldset>
		</td></tr>
		</table>
	</td>
</tr>
<tr>
	<td align="right">
		<input type="submit" name="search" value="rechercher" onclick="return doDisplay(this.form, 1);">
		&nbsp;<input type="submit" name="reset" value="effacer">
	</td>
</tr>
<tr>
	<td>
	<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
	<?
		// champ des agences
		$flds_agence = array('zone','groupe','code_societe');
		$flds_form = array('reset','search','date_from','date_to','nb_rows','from_row','key','order');
	
		if (!$_POST['key']) $_POST['key'] = 'id';
		if (!$_POST['order']) $_POST['order'] = 'asc';
		// cr�er le sql
		$sql = "SELECT r.id, r.agence, r.statut, r.client_email, r.total, date(r.paiement) paiement, date(r.debut) debut, date(r.fin) fin";
		$sql.=" FROM ".($_POST['statut']=='V' ? '_archive_reservation' : 'reservation')." r";
		$sql.=" JOIN categorie c ON c.id=r.categorie";
		$sql.=" JOIN client u ON u.id=r.client_id";
		$sql.=" JOIN agence a ON a.id=r.agence";
		$sql.=" LEFT JOIN partenaire_coupon pc ON pc.id=r.coupon";
		$sql.=" LEFT JOIN promotion_code g on g.reservation=r.id";
		$sql.=" LEFT JOIN promotion_campagne gc on gc.id=g.campagne";
		$sql.=' WHERE '.count($_SESSION['admin_search']);
		foreach ($_POST as $k => $v)
		{
			if (in_array($k, $flds_form) || !$v) continue;
			if (!preg_match('/^[a-z0-9_]+$/i', $k)) continue;
			// pour les dates
			if ($k == 'date')
			{
				if (!in_array($v, $flds_date)) continue;
				foreach(array('date_from'=>'>=','date_to'=>'<=') as $type => $op)
				{
					if ($d = $_POST[$type])
					{
						if ($type == 'date_to') $d .= ' 23:59:59';
						$sql .= " AND r.{$_POST['date']} $op '".mysql_escape_string(euro_iso($d))."'";
					}
				}
			}
			// pour les agences
			else if (in_array($k, $flds_agence)) {
				$sql.= " AND a.$k = '".mysql_escape_string($v)."'";
			} else if ($k == 'client') {
				if (is_numeric($v)) {
					$sql .= " AND r.client_id='$v'";
				} else {
					$v = mysql_escape_string($v).'%';
					$sql.=" AND (r.client_email LIKE '$v' OR u.email LIKE '$v' OR u.nom LIKE '$v')";
				}
			} else if ($k == 'coupon') {
				$v = preg_replace('/((^0+)|\s)/', '', $v);
				$sql.=" AND (pc.code='".mysql_escape_string($v)."' OR g.code='".mysql_escape_string($v)."')" ;
			} else if ($k=='campagne') {
				$sql.=' AND g.campagne='.(int) $_POST['campagne'];
			} else if ($k == 'agent') {
				$sql.=" AND r.agent LIKE '".mysql_escape_string($v)."%'";
			} else {
				$sql.=" AND r.$k='".mysql_escape_string($v)."'";
			}
		}
		if (!$_POST['statut'])
			$sql.=" AND r.statut IN ('A','P','X')";
		$sql .= ' ORDER BY r.'.$_POST['key'].' '.$_POST['order'];
//echo '<pre>'.$sql.'</pre>';
		// requ�te SQL
		$rs = sqlexec($sql);
		$cnt = mysql_num_rows($rs);
		echo "<caption>$cnt r�servations</caption>";
		// pagination
		$range_rows = range(20,200,20);
		$nb_rows = $_POST['nb_rows'];
		if (!$nb_rows) $nb_rows = $range_rows[0];
		$from_row = $_POST["from_row"];
		$from_row = floor($from_row / $nb_rows) * $nb_rows + 1;
		if (!($from_row > 0)) $from_row = 1;
		
		// se d�placer dans les donn�es si c'est possible
		if ($cnt > $from_row)
			mysql_data_seek ($rs, $from_row - 1);
		else
			$from_row = 1;
		// afficher les r�sultats
		$i = 0;
		while ($row = mysql_fetch_assoc($rs))
		{
			$i++;
			if ($i > $nb_rows) break;
			if (!$first)
			{
				$first = true;
				
				// export des r�servation
				$fld_export = "r.id, r.forfait, r.forfait_type, r.age, r.permis, r.coupon, r.canal, r.agent, pc.code, gc.nom campagne, u.id as id_client, u.nom as nom_client, u.prenom as prenom_client, r.client_email email, r.alias, a.zone, a.reseau";
				$fld_export.="\n, r.type, c.mnem, r.debut, r.fin, r.promotion, r.tarif, r.reduction promo_reduction, r.yield, r.nego, r.surcharge, g.reduction code_reduction, g.promotion code_promotion, r.total, r.statut, r.distance, r.km, r.jours, r.duree, r.creation, r.paiement, r.paiement_mode, r.annulation, r.annulation_motif, u.pgkm, u.naissance, ru.unipro";
				$fld_export.="\n, orf.prix option_grf_montant";
				$fld_export.="\n, faf.id facture_ada_id, faf.mode facture_ada_mode, faf.montant facture_ada_montant, faa.id avoir_ada_id, faa.mode avoir_ada_mode, faa.montant avoir_ada_montant";
				$fld_export.="\n, fcf.id facture_ass_id, fcf.mode facture_ass_mode, fcf.montant facture_ass_montant, fca.id avoir_ass_id, fca.mode avoir_ass_mode, fca.montant avoir_ass_montant";
				$fld_export.="\n, fmf.id facture_mat_id, fmf.mode facture_mat_mode, fmf.montant facture_mat_montant, fma.id avoir_mat_id, fma.mode avoir_mat_mode, fma.montant avoir_mat_montant";
				$fld_export.="\n, fpf.id facture_pro_id, fpf.mode facture_pro_mode, fpf.montant facture_pro_montant";
				$fld_export.="\n, month(r.debut) mois_loc, year(r.debut) annee_loc, month(r.paiement) mois_paiement, year(r.paiement) annee_paiement, mid(yearweek(r.paiement, 3), 5, 2) semaine_paiement, a.code_societe, a.code_region";
				
				$tbl_export = "\nleft join res_option orf on (orf.reservation=r.id and orf.`option`=concat('rf',lcase(r.type)))";
				$tbl_export.= "\nleft join facturation_ada faf on (faf.reservation=r.id and faf.type='F')";
				$tbl_export.= "\nleft join facturation_ada faa on (faa.reservation=r.id and faa.type='A')";
				$tbl_export.= "\nleft join facturation_courtage fcf on (fcf.reservation=r.id and fcf.type='F')";
				$tbl_export.= "\nleft join facturation_courtage fca on (fca.reservation=r.id and fca.type='A')";
				$tbl_export.= "\nleft join facturation_materiel fmf on (fmf.reservation=r.id and fmf.type='F')";
				$tbl_export.= "\nleft join facturation_materiel fma on (fma.reservation=r.id and fma.type='A')";
				$tbl_export.= "\nleft join facturation_pro fpf on (fpf.reservation=r.id and fpf.type='F')";
				$tbl_export.= "\nleft join res_unipro ru on (ru.reservation=r.id)";
				
				$sql_resa_export = preg_replace("/^select\s+(.+)from\s+(.+)\s+where /i", "select $fld_export\n from \\2 $tbl_export\nwhere ", $sql);
				
				// l'export des options
				$fld_export = "ro.reservation, r.forfait_type, r.age, r.permis, r.alias, a.zone, a.reseau, r.type, c.mnem, r.debut, r.fin, r.total, r.statut, r.duree, r.validation";
				$fld_export.="\n, o.theme, ro.`option`, ro.quantite, ro.prix, ro.facturation, ro.retrait, ro.paiement, coalesce(o.nom,ro.nom) nom";
				
				$tbl_export = "\n join res_option ro on ro.reservation=r.id";
				$tbl_export.="\n left join `option` o ON o.id=ro.`option`";

				$sql_opts_export = preg_replace('/^select\s+(.+)from\s+(.+)\s+where\s+(.+)\s+order by .+$/i', "select $fld_export\n from \\2 $tbl_export\nwhere \\3 order by ro.reservation, ro.position", $sql);
				
				// les boutons pour exporter
				echo '<tr><td colspan="'.count($row).'" align="right">';
					echo SQLExporter::getButton('export options', 'options', $sql_opts_export);
					echo SQLExporter::getButton('export r�sas', 'reservation', $sql_resa_export);
				echo '</td></tr>';
				echo '<tr><td align="left">';
				if ($from_row > 1)
					echo '<a href="#" onclick="doDisplay(document.forms[\'frm_search\'], '.(int)($from_row-$nb_rows).')"><img src="img/left.gif" border="0"></a>';
				echo '</td><td colspan="'.(count($row)-2).'" align="center">';
				echo '<input type="hidden" name="from_row" value="'.$from_row.'"/>';
				echo '<input type="hidden" name="key" value="'.$_POST['key'].'"/>';
				echo '<input type="hidden" name="order" value="'.$_POST['order'].'"/>';
				echo 'lignes par pages : ';
				selectarray('nb_rows', array_combine($range_rows, $range_rows), $_POST['nb_rows'], null, 'onchange="doDisplay(this.form, '.$from_row.')" style="width: auto;"');
				echo ' page '.ceil($from_row / $nb_rows).'/'.ceil($cnt/$nb_rows);
				echo ' ('.$from_row.' - '.min($cnt, $from_row+$nb_rows-1).')';
				echo '</td><td align="right">';
				if ($cnt > $from_row + $nb_rows)
					echo '<a href="#" onclick="doDisplay(document.forms[\'frm_search\'], '.(int)($from_row+$nb_rows).')"><img src="img/right.gif" border="0"></a>';
				echo '</td></tr>';
				// �crire les en-t�tes
				echo '<tr>';
				foreach(array_keys($row) as $k)
				{
					echo '<th><a href="#" onclick="return doSort(\''.$k.'\',\''.($_POST['key']==$k && $_POST['order']=='asc' ? 'desc' : 'asc').'\')">';
					echo $k;
					if ($_POST['key']==$k)
						echo '&nbsp;<img align="absmiddle" border="0" src="img/'.($_POST['order']=='desc' ? 'down' : 'up').'.gif">';
					echo '</a></th>';
				}
				echo '</tr>';
			}
			$row['id'] = '<a href="reservation.php?id='.$row['id'].'">'.$row['id'].'</a>';
			echo '<tr><td>'.join('</td><td>', $row).'</td></tr>';
		}
	?>
	</table>
	</td>
</tr>
</table>
</form>
</body>
</html>
