<?
class PartenaireCoupon_Collection extends Ada_Collection
{
	/** @var Partenaire */
	protected $_partenaire;

	/**
	* Cr�e la liste des coupons depuis un partenarie
	*
	* @param Partenaire $partenaire
	* @return PartenaireCoupon_Collection
	*/
	static public function factory(Partenaire $partenaire)
	{
		$sql = 'select pc.*, p.origine, p.nom partenaire_nom, cl.nom classe_nom';
		$sql.=' from partenaire_coupon pc';
		$sql.=' join partenaire p on p.id=pc.partenaire';
		$sql.=' left join partenaire_classe cl on cl.id=pc.classe';
		$sql.=' where p.id='.$partenaire->getId();

		// renvoyer la collection
		$x = new PartenaireCoupon_Collection();
		$x->_partenaire = $partenaire;
		$x->_loadFromSQL($sql);
		return $x;
	}
	public function getPartenaire()
	{
		return $this->_partenaire;
	}
	static public function getSQLListOfLocapass() {
		return "
				select pc.code, concat(p.nom, ' ', pc.code) nom
				from partenaire_coupon pc
				join partenaire p on p.id=pc.partenaire
				where nullif(pc.locapass,'') is not null
  					and pc.actif=1 and p.actif=1
  					and ifnull(pc.echeance,current_date) >= current_date
				order by pc.code";
	}
}
?>
