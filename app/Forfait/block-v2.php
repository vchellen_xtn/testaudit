<?
	/** @var Forfait */
	$forfait = $this;
	$categorie = $forfait->getCategorie();
?>
<div class="promo_prix">
	<? if ($forfait->isLocapass()) : ?>
	<img src="../css/img/locapass.png" alt="Locapass" />
	<? else : ?>
	<div class="prix_toggle prix_jour" style="display: none;" <? if (SITE_MODE!='PROD') :?>onclick="$('.prix_toggle.prix_total').show(); $('.prix_toggle.prix_jour').hide();"<?endif;?>>
		<span id="forfait_t_prix"><span class="intitule--prix-par-jour">Prix par jour :</span> <?=show_money($forfait->getPrixParJour());?>�</span>
		<? if ($forfait->hasPromo()) : ?>
		<p id="forfait_prix">au lieu de <span><?=show_money($forfait->getPrixBarreParJour());?>�/j</span></p>
		<? endif; ?>
	</div>
	<div class="prix_toggle prix_total" <? if (SITE_MODE!='PROD') :?>onclick="$('.prix_toggle.prix_total').hide(); $('.prix_toggle.prix_jour').show();"<?endif;?>>
		<span id="forfait_t_prix"><?=$forfait->getPrix();?>�</span>
		<? if ($forfait->hasPromo()) : ?>
		<p id="forfait_prix">au lieu de <span><?=$forfait->getPrixBarre()?>�</span></p>
		<? endif; ?>
	</div>
	<span id="forfait_kmsup"><? if ($kmSupp = $forfait->getPrixKmSupp()) echo show_money($kmSupp).'� le km supp.';?></span>
	<a href="../popup.php?page=reservation/details&agence=<?=$forfait->getAgence()->getId()?>&km_sup=<?=$kmSupp?>" onclick="return wOpen(this.href, 400, 470)">D�tails</a>
	<? endif; ?>
</div>
<p class="promo_infos"<? if ($forfait->isLocapass()) echo 'style="background: none;"';?>>
	<?=str_replace(',<br/>',', ', $forfait->getPromoDescription());?>
</p>
<div class="fam_cat">
<? if ($categorie['famille_nom']) : ?>
	<h2><?=$categorie['famille_nom']?></h2>
<? endif; ?>
<? if ($categorie['famille_nom']!=$categorie['categorie_nom']) : ?>
	<h2 class="rouge"><?=$categorie['categorie_nom']?></h2>
<?endif;?>
</div>
<div class="fam_cat_infos">
	<p id="forfait_duree">
		Dur�e : <span><?=$forfait->getDuree();?></span>
	</p>
	<? if (!$forfait->isLocapass()) : ?>
	<p id="forfait_km">
		Km inclus : <span><?=$forfait->getKm()?></span>
	</p>
	<? endif; ?>
	<p id="forfait_categorie_commentaire">
		<?=$categorie['commentaire']?>
	</p>
	<p id="forfait_categorie_description">
		Type : <span><?=$categorie['description']?></span>
	</p>
</div>
<img src="../<?=$categorie->getImg('big');?>" id="forfait_categorie_img" alt="" />
<div id="forfait_categorie_specs">
	<ul class="options">
		<?=$categorie->getSpecifications('li');?>
	</ul>
	<div class="clear"></div>
</div>
