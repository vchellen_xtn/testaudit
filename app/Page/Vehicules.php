<?php

class Page_Vehicules extends Page {
	public function getUniversTitre() {
		return 'Nos véhicules';
	}
	public function indexAction() {
		// pour l'instant le contenu est vide
		self::redirect(self::getURL('vehicules/tourisme.html'));
		exit();
	}
	public function utilitairesAction() {
		// on ne veut plus indexer cette page
		self::noIndex();
	}

	// on affiche la recherche d'agences
	public function sans_permisAction() {
		$this->addJS('scripts/agence_liste.js', false);
	}
	public function deux_rouesAction() {
		$this->addJS('scripts/agence_liste.js', false);
	}
	public function prestigeAction() {
		$this->addJS('scripts/agence_liste.js', false);
	}
	public function getUniversMenu() {
		$menu = array(
			'tourisme'		=> 'Voitures de tourisme',
			'utilitaires'	=> 'Véhicules utilitaires',
			'sans_permis'	=> 'Voitures sans permis',
			'deux_roues'	=> 'Deux roues',
			'vehicule_du_mois'	=> 'Véhicule du mois',
			'prestige'		=> 'Véhicules prestige',
		);

		return $menu;
	}
}

?>