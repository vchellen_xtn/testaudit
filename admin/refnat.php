<?
	require_once("secure.php"); 
	require_once("lib.php");

	// identifiant
	$id = getarg("id");
	$tbl = getarg("tbl");
	$info = RefnatCanon::$TYPES[$tbl];
	if (!$info) die("La table $tbl n'est pas autoris�e !");
	
	// modification, cr�ation
	if ($tbl && getarg("update"))
	{
		$a = array('id'=>$_POST['id'], 'refnat'=>strip_tags($_POST['refnat'], HTML_TAGS_ALLOWED), 'footer' => strip_tags($_POST['footer'], HTML_TAGS_ALLOWED));
		save_record($info['tbl'], $a);
		$histo = array_merge($a, array('id_histo'=>'','type'=>$tbl,'login'=>$adminUser['login']));
		save_record('refnat_histo', $histo);
	}
	if ($id)
	{
		$rs = sqlexec("select id, nom, refnat".($info['footer'] ? ", footer" : '')." from ".$info['tbl']." where id = '".addslashes($id)."' ");
		if (!$rs || !($row = mysql_fetch_assoc($rs)))
			die("Aucun enregistrement correspondant.");
	}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script language="javascript" src="../scripts/check.js"></script>
	<script>
	<!-- //
	// doSubmit
	function doSubmit(frm)
	{
		var err = "";
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		return true;
	}
	// -->
	</script>
</head>
<body>
<div style="float: right; padding: 1em; border: 1px dotted grey; margin: 2em;">
L'�l�ment "refnat" peut contenir les �l�ments suivants :
<dl>
	<dt>#agence#</dt><dd>remplac� par le nom de l'agence</dd>
	<dt>#categorie#</dt><dd>remplac� par le nom de la cat�gorie ou voiture ou utilitaire</dd>
	<dt>#dept#</dt><dd>remplac� par le nom du d�partement</dd>
	<dt>#localisation#</dt><dd>remplac� par le premier �l�ment disponible dans la liste : agence, dept, region, zone</dd>
	<dt>#marque#</dt><dd>remplac� par la marque du v�hicule</dd>
	<dt>#modele#</dt><dd>remplac� par le mod�le du v�hicule</dd>
	<dt>#region#</dt><dd>remplac� par le nom de la r�gion</dd>
	<dt>#type#</dt><dd>remplac� par le type de v�hicule</dd>
	<dt>#zone#</dt><dd>remplac� par le nom de la zone</dd>
</dl>
Si dans le contexte une valeur n'est pas renseign�e, elle est supprim�e.
<br>Le texte doit donc rester coh�rent avec ou sans la pr�sence de ces textes.
<br/>
<br/>Ref. Nat : pour les <strong>zone</strong>, <strong>d�partement</strong>, <strong>ville</strong> et <strong>emplacement</strong> une balise &lt;hr&gt; permet de s�parer en 2 parties la description
<br/>
<br/>Le champ "Footer" sera ins�r� en bas de la page.
<br/>Chaque ligne correspond � un point de la liste.
<br/>Des �l�ments HTML peuvent �tre ins�r�s dans le texte.
<br/>Les liens doivent �tre en relatifs par rapport � la racine du site sans le "/".
<br/>Les �l�ments autoris�s sont <?=htmlspecialchars(HTML_TAGS_ALLOWED);?>
</div>
<form action="" method="post">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre">
	<img src="img/puce.gif" align="absmiddle" hspace="1">
<? 
	echo "$tbl - r�f�rencement naturel";
?>
	</th>
</tr>
<tr>
	<td>
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="15%">
		<col width="15%">
		<col width="15%">
		<col width="55%">
		<tr>
			<td><label for="page">ID</label></td>
			<td>
				<?=$id?>
				<input type="<?=($id ? 'hidden' : 'text')?>" maxlength="128" name="id" value="<?=$id?>">
				<input type="hidden" name="tbl" value="<?=$tbl?>">
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td><label for="nom">Nom</label></td>
			<td colspan="3"><?=$row['nom']?></td>
		</tr>
		<tr>
			<td><label for="refnat">R�f. Nat.</label></td>
			<td colspan="3"><textarea id="refnat" name="refnat" rows="10" style="width: 100%;"><?=htmlspecialchars($row['refnat']);?></textarea></td>
		</tr>
		<? if ($info['footer']) : ?>
		<tr>
			<td><label for="footer">Footer</label></td>
			<td colspan="3"><textarea id="footer" name="footer" rows="10" style="width: 100%;"><?=htmlspecialchars($row['footer']);?></textarea></td>
		</tr>
		<? endif; ?>
		</table>
	</td>
</tr>
<tr>
	<td align="right">
		<input type="submit" name="<?=$id ? "update" : "create"?>" value="enregistrer" onclick="return doSubmit(this.form);">
	</td>
</tr>
</table>
</form>
<? if ($row) : ?>
	<? if ($rs = sqlexec(sprintf("select * from refnat_histo where type='%s' and id='%s' order by id_histo desc", $tbl, $row['id']))) : ?>
		<h2>Historique</h2>
		<table border="1" cellpadding="2" cellspacing="0" class="stats resultat">
		<? while ($histo = mysql_fetch_assoc($rs)) : ?>
		<tr><th colspan="2"><?=euro_iso($histo['creation']).' - '.$histo['login']?></th></tr>
		<tr><th>Ref. Nat.</th><td><pre><?=htmlspecialchars($histo['refnat'])?></pre></td></tr>
		<tr><th>Footer</th><td><pre><?=htmlspecialchars($histo['footer'])?></pre></td></tr>
		<? endwhile; ?>
		</table>
	<? endif; ?>
<? endif; ?>
</body>
</html>
