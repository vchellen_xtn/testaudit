<?
	require_once("secure.php"); 
	require_once("lib.php");

	// identifiant
	$id = getarg("id");
	
	// suppression
	if ($id && getarg("delete")) 
	{
		$sql = "delete from zone where id = '".$id."'";
		getsql($sql);
		unset($id);
	}
	
	// modification, cr�ation
	if (getarg("update") || getarg("create"))
	{
		save_record('zone', $_POST);
	}

	if ($id)
	{
		$rs = sqlexec("select * from zone where id = '".$id."' ");
		if (!$rs || !($row = mysql_fetch_assoc($rs)))
			die("Aucun enregistrement correspondant.");
	}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script language="javascript" src="../scripts/check.js"></script>
	<script>
	<!-- //

	// doDelete
	function doDelete(frm)
	{
		return (confirm("Voulez-vous r�ellement supprimer cet �l�ment ?\nCette op�ration est irr�versible."));
	}
	
	// doSubmit
	function doSubmit(frm)
	{
		var err = "";
		if(frm.id && !frm.id.value.length)
			err+= "Veuillez indiquer un Id\n";
		if(!frm.nom.value.length)
			err+= "Veuillez indiquer le nom\n";
		if (!frm.vp_delai_min.value.match(/^\d+$/))
			err+= "Veuillez indiquer un d�lai VP correct\n";
		if (!frm.vu_delai_min.value.match(/^\d+$/))
			err+= "Veuillez indiquer un d�lai VU correct\n";
		if (!frm.sp_delai_min.value.match(/^\d+$/))
			err+= "Veuillez indiquer un d�lai SP correct\n";
		if (!frm.ml_delai_min.value.match(/^\d+$/))
			err+= "Veuillez indiquer un d�lai ML correct\n";
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		return true;
	}
	
	// -->
	</script>
</head>
<body>
<form action="" method="post" onsubmit="return doSubmit(this);">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre">
	<img src="img/puce.gif" align="absmiddle" hspace="1">
<? 
	echo "Zone";
?>
	</th>
</tr>
<tr>
	<td>
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="15%">
		<col width="35%">
		<col width="15%">
		<col width="35%">
		<tr>
			<td><label for="id">ID</label></td>
			<td><?
				echo $id;
				echo '<input type="'.($id ? 'hidden' : 'text').'" maxlength="2" id="id" name="id" value="'.$id.'">';
			?></td>
			<td><label for="nom">Nom</label></td>
			<td><input type="text" style="width:100%" name="nom" id="nom" value="<?=$row["nom"]?>"></td>
		</tr>
		<tr>
			<td>Devise</td>
			<td><?=$row['devise']?></td>
			<td>TVA</td>
			<td><?=100*$row['tva']?> %</td>
		</tr>
		<tr>
			<td colspan="2"><strong>D�lai en heures (au minimum)</strong></td>
			<td><label for="delai_mode">Type de d�lai</label></td>
			<td><? selectarray('delai_mode', array('C'=>'Calendaire', 'O'=>'Ouverture'), $row['delai_mode']); ?></td>
		</tr>
		<tr>
			
		</tr>
		<tr>
			<td><label for="vp_delai_min">D�lai VP</label></td>
			<td><input type="text" maxlength="2" style="width: 3em;" name="vp_delai_min" id="vp_delai_min" value="<?=$row['vp_delai_min']?>"></td>
			<td><label for="vu_delai_min">D�lai VU</label></td>
			<td><input type="text" maxlength="2" style="width: 3em;" name="vu_delai_min" id="vu_delai_min" value="<?=$row['vu_delai_min']?>"></td>
		</tr>
		<tr>
			<td><label for="sp_delai_min">D�lai SP</label></td>
			<td><input type="text" maxlength="2" style="width: 3em;" name="sp_delai_min" id="sp_delai_min" value="<?=$row['sp_delai_min']?>"></td>
			<td><label for="ml_delai_min">D�lai ML</label></td>
			<td><input type="text" maxlength="2" style="width: 3em;" name="ml_delai_min" id="ml_delai_min" value="<?=$row['ml_delai_min']?>"></td>
		</tr>
		<!--
		<tr>
			<td><label for="reservation">R�servation</label></td>
			<td colspan="2"><? selectarray('reservation', array('0' => 'ferm� � la r�servation', '1' => 'ouvert � la r�servation'), $row['reservation']); ?></td>
			<td>&nbsp;</td>
		</tr>
		-->
		</table>
	</td>
</tr>
<tr>
	<td>
	<table width="100%">
	<tr>
		<td align="left">
<?	if ($id) { ?>
			<input type="button" onclick="parent.list.location.href='lists/zone_groupe.php?zone=<?=$id?>'" value="groupes">
			<input type="button" onclick="parent.list.location.href='lists/zone_saison.php?zone=<?=$id?>'" value="saisons">
			<input type="button" value="tarifs" onclick="parent.list.location.href='lists/forfait_tarif.php?zone=<?=$id?>'; return false;">
<?	} ?>
		</td>
		<td align="right">
			<input type="submit" name="<?=$id ? "update" : "create"?>" value="enregistrer">
<?
	if ($id) 
	{
?>
<!--
		&nbsp;
		<input type="submit" onclick="return doDelete(this.form);" name="delete" value="supprimer">
-->
<?
	}
?>
		</td>
	</tr>
	</table>
	</td>
</tr>
</table>
</form>
</body>
</html>