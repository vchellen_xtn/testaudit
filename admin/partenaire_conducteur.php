<?
	require_once('secure.php'); 
	require_once('lib.php');

	// identifiant
	$id = (int) getarg('id');
	
	// suppression
	if ($id && getarg('delete')) 
	{
		$sql = "delete from partenaire_conducteur where id = '".(int)$id."'";
		getsql($sql);
		unset($id);
	}
	
	// modification ou cr�ation
	if (getarg('update') || getarg('create')) {
		save_record("partenaire_conducteur", $_POST);
		if (!$id) $id = mysql_insert_id();
	}

	if ($id)
	{
		$row = PartenaireConducteur::factory((int)$id);
		if (!$row || $row->isEmpty())
			die("Aucun enregistrement correspondant.");
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script type="text/javascript" language="javascript" src="../scripts/script.js"></script>
	<script type="text/javascript">
	<!-- //
	// doDelete
	function doDelete(frm)
	{
		return (confirm("Voulez-vous r�ellement supprimer cet �l�ment ?\nCette op�ration est irr�versible."));
	}
	
	// doSubmit
	function doSubmit(frm)
	{
		var err = "";
		if (!frm.email.value.length)
			err+="Veuillez indiquer votre email\n";
		else if(!validateEmail(frm.email))
			err+="Ce n'est pas une adresse email valide\n";
		if(!validateSelect(frm.titre))
			err += "Veuillez indiquer la civilite\n";
		if(!validateSelect(frm.partenaire))
			err += "Veuillez indiquer le partenaire\n";
		if(!frm.prenom.value.length)
			err += "Veuillez indiquer un prenom\n";
		if(!frm.nom.value.length)
			err += "Veuillez indiquer un nom\n";
		if (!validateDate(frm.naissance))
			err+="La date de naissance est invalide\n";
		if (!frm.tel.value.match(/^\+?[0-9\s\-]+$/))
			err+="Veuillez indiquer un num�ro de t�l�phone\n";
		if (!frm.gsm.value.match(/^\+?[0-9\s\-]+$/))
			err+="Veuillez indiquer un num�ro de gsm\n";
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		return true;
	}
	// -->
	</script>
</head>

<body>
<form action="" method="post" onsubmit="return doSubmit(this);">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre">
		<img src="img/puce.gif" align="middle" hspace="1" alt=" ">
		<? echo 'Conducteur'; ?>
	</th>
</tr>
<tr>
	<td>
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="10%">
		<col width="40%">
		<col width="10%">
		<col width="40%">
		<tr>
			<td>ID</td>
			<td>
				<? if ($id) echo $id; ?>
				<input type="hidden" name="id" class="l" value="<?if ($id) echo $id; ?>"/>
			</td>
			<td><label for="actif">Actif</label></td>
			<td><? selectarray('actif', array('1'=>'oui','0'=>'non'), $row['actif']); ?></td>
		</tr>
		<tr>
			<td><label for="partenaire">Partenaire</label></td>
			<td colspan="3">
				<? if ($id) : ?>
					<a href="partenaire.php?id=<?=$row['partenaire']?>"><?=getsql('select nom from partenaire where id='.$row['partenaire'])?></a>
				<?
				else : 
					$sql = "select p.id, concat(c.code,' - ', p.nom) nom";
					$sql.=" from partenaire_coupon c";
					$sql.=" join partenaire p on p.id=c.partenaire";
					$sql.=" where c.locapass is not null and p.actif=1";
					$sql.=" and coalesce(echeance,current_date)>=current_date";
					if (preg_match('/^\d+$/', $_GET['partenaire']))
						$sql.=" and p.id=".(int) $_GET['partenaire'];
					$sql.=" order by p.nom";
					selectsql('partenaire', $sql, (int)$_GET['partenaire'], '-- choisissez un partenaire --');
				 endif; 
				 ?>
			</td>
		</tr>
		<tr>
			<td><label for="email">Email</label></td>
			<td colspan="3"><input type="text" id="email" name="email" class="l" value="<?=filter_var($row['email'], FILTER_SANITIZE_EMAIL);?>" size="64" maxlength="128"/></td>
		</tr>
		<tr>
			<td><label for="titre">Titre</label></td>
			<td><? selectarray("titre", $CIVILITE, $row["titre"], "--"); ?></td>
			<td colspan="2"></td>
		</tr>
		<tr>
			<td><label for="prenom">Pr�nom</label></td>
			<td><input type="text" id="prenom" name="prenom" value="<?=filter_var($row['prenom'], FILTER_SANITIZE_STRING)?>" size="64" maxlength="64"/></td>
			<td><label for="nom">Nom</label></td>
			<td><input type="text" id="nom" name="nom" value="<?=filter_var($row['nom'], FILTER_SANITIZE_STRING)?>" size="64" maxlength="64"/></td>
		</tr>
		<tr>
			<td><label for="naissance">Date de naissance</label></td>
			<td><input autocomplete="off" type="text" name="naissance" id="naissance" value="<?=euro_iso($row['naissance'])?>" size="12" maxlength="10"/></td>
			<td colspan="2"></td>
		</tr>
		<tr>
			<td>Tel</td>
			<td><input type="text" name="tel" value="<?=$row["tel"]?>" size="18" maxlength="18"></td>
			<td>GSM</td>
			<td><input type="text" name="gsm" value="<?=$row["gsm"]?>" size="18" maxlength="18"></td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%">
		<tr>
			<td>
			</td>
			<td align="right">
				<input type="submit" name="<?=$id ? "update" : "create"?>" value="enregistrer"/>
				<? if ($id && (1 > getsql('SELECT COUNT(*) FROM res_info WHERE conducteur='.$id))) : ?>
				&nbsp;
				<input type="submit" onclick="return doDelete(this.form);" name="delete" value="supprimer"/>
				<? endif; ?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
</form>
</body>
</html>