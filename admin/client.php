<?
	require_once("secure.php");
	require_once("lib.php");

	// identifiant
	$id = (int) getarg("id");
	if (!$id) $id = null;


	// suppression
	if ($id && getarg("delete"))
	{
		$sql = "delete from client where id = '".(int)$id."'";
		getsql($sql);
		unset($id);
	}

	// modification ou cr�ation
	if (getarg("update") || getarg("create")) {
		// si l'e-mail change
		if ($_POST['email2'] && $_POST['email2']!=$_POST['email']) {
			if (getsql("SELECT COUNT(*) FROM client WHERE email='".filter_var($_POST['email'], FILTER_SANITIZE_EMAIL)."'")) {
				$msg = "L'adresse e-mail ".filter_var($_POST['email'], FILTER_SANITIZE_EMAIL)." est d�j� utilis�e!";
				$_POST['email'] = $_POST['email2'];
			}

		}
		save_record("client", $_POST);
		if (!$id) $id = mysql_insert_id();
	}

	if ($id)
	{
		$rs = sqlexec("select * from client where id = '".(int)$id."'");
		if (!$rs || !($row = mysql_fetch_assoc($rs)))
			die("Aucun enregistrement correspondant.");
	}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script type="text/javascript" language="javascript" src="../scripts/check.js"></script>
	<script type="text/javascript">
	<!-- //
	<? if ($msg) echo 'alert("'.$msg.'");' ?>
	// doDelete
	function doDelete(frm)
	{
		return (confirm("Voulez-vous r�ellement supprimer cet �l�ment ?\nCette op�ration est irr�versible."));
	}

	// doSubmit
	function doSubmit(frm)
	{
		var err = "";
		if (!frm.id || !frm.id.value.match(/^\d+$/)) {
			alert("Vous ne povuez pas cr�er de client dans le back office");
			return false;
		}
		if (!frm.email.value.length)
			err+="Veuillez indiquer votre email\n";
		else if(!frm.email.value.match(/^[^@\ ]+@[^@\ ]+\.[^@\ ]+$/))
			err+="Ce n'est pas une adresse email valide\n";
		if(!ValidateSelect(frm.titre))
			err += "Veuillez indiquer la civilite\n";
		if(!frm.prenom.value.length)
			err += "Veuillez indiquer un prenom\n";
		if(!frm.nom.value.length)
			err += "Veuillez indiquer un nom\n";
		if(!frm.adresse.value.length)
			err += "Veuillez indiquer une adresse\n";
		if (!frm.cp.value.length)
			err+="Veuillez indiquer un code postal\n";
		if (!frm.ville.value.length)
			err+="Veuillez indiquer une ville\n";
		if (!frm.pays.value.length)
			err+="Veuillez indiquer un pays\n";
		if (frm.naissance && frm.naissance.value.length && !ValidateDate(frm.naissance))
			err+="La date de naissance est invalide\n";
		if(!frm.f_adresse.value.length)
			err += "Veuillez indiquer une adresse de facturation\n";
		if (!frm.f_cp.value.length)
			err+="Veuillez indiquer le code postal de facturation\n";
		if (!frm.f_ville.value.length)
			err+="Veuillez indiquer la ville de facturation\n";
		if (!frm.f_pays.value.length)
			err+="Veuillez indiquer le pays de facturation\n";
		if (!frm.tel.value.match(/^\+*[0-9\s\-]+$/))
			err+="Veuillez indiquer un num�ro de t�l�phone\n";
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		return true;
	}

	// -->
	</script>
</head>

<body>

<form action="" method="post" onsubmit="return doSubmit(this);">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre">
	<img src="img/puce.gif" align="middle" hspace="1" alt=" ">
	<? echo "Client"; ?>
	</th>
</tr>
<tr>
	<td>
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="10%">
		<col width="40%">
		<col width="10%">
		<col width="40%">
		<tr>
			<td>ID</td>
			<td><? echo $id.'<input type="hidden" name="id" class="l" value="'.$id.'">';?></td>
			<td><label for="actif">Actif</label></td>
			<td><? selectarray('actif', array('1'=>'oui','0'=>'non'), $row['actif']); ?></td>
		</tr>
		<tr>
			<td><label for="email">Email</label></td>
			<td>
				<input type="text" id="email" name="email" class="l" value="<?=$row['email']?>" size="64" maxlength="128">
				<input type="hidden" name="email2" value="<?=$row['email']?>">
			</td>
			<td>Mot de passe</td>
			<td>
			<?
				$x = new Client($row);
				echo '<a href="'.str_replace('admin/','', $x->getURLResetPassword()).'" target="_blank">r�-initialiser</a>';
			?>
			</td>
		</tr>
		<tr>
			<td><label for="titre">Titre</label></td>
			<td><? selectarray("titre", $CIVILITE, $row["titre"], "--"); ?></td>
			<td colspan="2"></td>
		</tr>
		<tr>
			<td><label for="prenom">Pr�nom</label></td>
			<td><input type="text" id="prenom" name="prenom" value="<?=stripslashes($row["prenom"])?>" size="64" maxlength="64"/></td>
			<td><label for="nom">Nom</label></td>
			<td><input type="text" id="nom" name="nom" value="<?=stripslashes($row["nom"])?>" size="64" maxlength="64"/></td>
		</tr>
		<tr>
			<td><label for="naissance">Date de naissance</label></td>
			<td><input autocomplete="off" type="text" name="naissance" id="naissance" value="<?=euro_iso($row["naissance"])?>" size="12" maxlength="10"/></td>
			<td colspan="2"></td>
		</tr>
		<tr>
			<td><label for="naissance_pays">Pays de naissance</label></td>
			<td><? selectarray('naissance_pays', Pays_Collection::factory(), $row['pays'], '--');?></td>
			<td><label for="naissance_lieu">Lieu de naissance</label></td>
			<td><input type="text" id="naissance_lieu" name="naissance_lieu" value="<?=$row['naissance_lieu']?>" size="64" maxlength="64"/></td>
		</tr>
			<td><label for="identite_type">Pi�ce identit�</label></td>
			<td><? selectarray('identite_type', Client::$IDENTITE_TYPE, $row['identite_type'], '--');?></td>
			<td><label for="identite_numero">Num�ro Pi�ce</label></td>
			<td><input type="text" id="identite_numero" name="identite_numero" value="<?=$row['identite_numero']?>" size="32" maxlength="32"/></td>
		<tr>
		</tr>
		<tr>
			<td><label for="adresse">Adresse</label></td>
			<td colspan="2"><input type="text" id="adresse" name="adresse" value="<?=$row['adresse']?>" size="64" maxlength="<?=max(64,strlen($row['adresse']))?>"/></td>
			<td></td>
		</tr>
		<tr>
			<td><label for="adresse2">Compl�ment</label></td>
			<td colspan="2"><input type="text" id="adresse2" name="adresse2" value="<?=$row['adresse2']?>" size="64" maxlength="64"/></td>
			<td></td>
		</tr>
		<tr>
			<td><label for="cp">Code postal</label></td>
			<td><input type="text" id="cp" name="cp" value="<?=$row["cp"]?>" size="12" maxlength="12"></td>
			<td><label for="ville">Ville</label></td>
			<td><input type="text" name="ville" value="<?=$row["ville"]?>" size="64" maxlength="64"/></td>
		</tr>
		<tr>
			<td><label for="pays">Pays</label></td>
			<td><? selectsql("pays", "select * from pays", $row["pays"])?></td>
		</tr>
		<tr>
			<td>Tel</td>
			<td><input type="text" name="tel" value="<?=$row["tel"]?>" size="18" maxlength="18"></td>
			<?php
			if(isset($row["gsm"]) && strlen($row["gsm"]) > 0) {
			?>
			<td>GSM</td>
			<td><input type="text" name="gsm" value="<?=$row["gsm"]?>" size="18" maxlength="18"></td>
            <?php
            }
            ?>
		</tr>
		<tr>
			<td>Partenaire</td>
			<td><? selectarray("partenaire", array("O"=>"Oui", "N"=>"Non"), $row["partenaire"])?></td>
			<td>Promo</td>
			<td><? selectarray("promo", array("O"=>"Oui", "N"=>"Non"), $row["promo"])?></td>
		</tr>
		<tr><th colspan="4">Facturation</th></tr>
		<tr>
			<td><label for="f_societe">Soci�t�</label></td>
			<td><input type="text" id="f_societe" name="f_societe" value="<?=$row['f_societe']?>" size="64" maxlength="64"></td>
			<td>Pays</td>
			<td><? selectsql("f_pays", "select * from pays", $row["f_pays"])?></td>
		</tr>
		<tr>
			<td><label for="f_adresse">Adresse</label></td>
			<td colspan="2"><input type="text" id="f_adresse" name="f_adresse" value="<?=$row["f_adresse"]?>" size="64" maxlength="<?=max(64,strlen($row['f_adresse']))?>"></td>
			<td></td>
		</tr>
		<tr>
			<td><label for="f_adresse2">Adresse</label></td>
			<td colspan="2"><input type="text" id="f_adresse2" name="f_adresse2" value="<?=$row["f_adresse2"]?>" size="64" maxlength="64"></td>
			<td></td>
		</tr>
		<tr>
			<td>CP</td>
			<td><input type="text" name="f_cp" value="<?=$row["f_cp"]?>" size="12" maxlength="12"></td>
			<td>Ville</td>
			<td><input type="text" name="f_ville" value="<?=$row["f_ville"]?>" size="64" maxlength="64"></td>
		</tr>
		<? if ($row['id']) : ?>
		<tr>
			<td>Cr�ation</td><td><?=euro_iso($row['creation']);?></td>
			<td>Modification</td><td><?=euro_iso($row['modification']);?></td>
		</tr>
		<? endif; ?>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%">
		<tr>
			<td>
			<? if ($id) : ?>
				<input type="button" onclick="var w = (parent && parent.list ? parent.list : window); w.location.href='lists/reservation.php?client=<?=$id?>'" value="r�servations">
			<? endif; ?>
			</td>
			<td align="right">
				<input type="submit" name="<?=$id ? "update" : "create"?>" value="enregistrer">
				<? if ($id && (1 > getsql('SELECT COUNT(*) FROM reservation WHERE client_id='.$id))) : ?>
				&nbsp;
				<input type="submit" onclick="return doDelete(this.form);" name="delete" value="supprimer">
				<? endif; ?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
</form>

</body>
</html>