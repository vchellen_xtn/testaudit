<?
	require_once("secure.php"); 
	require_once("lib.php");

	// identifiant
	$id = getarg('id');
	$reservation = Reservation::factory($id);

	// si le remboursement est demand� :
	if ($id && $reservation && ($_POST['annuler'] || $_POST['rembourse_ada'] || $_POST['rembourse_courtage'] || $_POST['rembourse_materiel']))
	{
		// cr�er la transaction pour l'annulation
		if ($x = Transaction::factory($_POST['mode']))
		{
			// pr�parer les conditions d'annulation
			$motif = ResAnnulationMotif::factory($_POST['motif'], ($_POST['mail']=='1'), ($_POST['campagne'] ? PromotionCampagne::factory((int)$_POST['campagne']) : null));
			$motif->setAdminUser($adminUser['login']);
			foreach(array('ada','courtage','materiel') as $f)
				$motif->setAdminMontant($f, $_POST['rembourse_'.$f]);

			// demander l'annulation
			$_SERVER['SCRIPT_NAME'] = dirname($_SERVER['SCRIPT_NAME']); // pour que les images de l'e-mail soient correctement r�f�renc�s...
			$x->doCancel($reservation, $motif);
		}
	}

	// information sur la r�servation
	if(!$reservation) die("Aucun enregistrement correspondant.");
	$client = $reservation->getClient('id, email');
	$categorie = $reservation->getCategorie('id, mnem, nom');
	$facturations = $reservation->getFacturations(true);
	$avoirs = $facturations->getAvailableAvoirs();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script language="javascript" src="../scripts/check.js"></script>
	<script>
	<!-- //
	// doSubmit
	function doSubmit(frm)
	{
		var err = "";
		if (frm.rembourse_ada)
		{
			if(!frm.rembourse_ada.value.match(/^\d+(\.\d{2})?$/))
				err += "Veuillez indiquer le montant � rembourser\n";
			else if (parseFloat(frm.rembourse_ada.value) > parseFloat(frm.rembourse_ada.defaultValue))
				err += "Veuillez indiquer un montant � rembourser inf�rieur ou �gal au montant pay�\n";
		}
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		var msg = '';
		if (frm.mail.options[frm.mail.selectedIndex].value == "1")
			msg += "- confirmer par e-mail l'annulation ?\n";
		if (frm.rembourse_ada)
		{
			msg += "- rembourser "+frm.rembourse_ada.value+"� ";
			if (frm.rembourse_courtage)
				msg += " + " + frm.rembourse_courtage.value + " � ";
			if (frm.rembourse_materiel)
				msg += " + " + frm.rembourse_materiel.value + " � ";
			msg += "sur un total de <?=$reservation['total']?>� ?\n";
		}
		else
			msg += "- annuler cette r�servation ?\n";
		if (frm.campagne && frm.campagne.value)
			msg += "- distribuer un code de r�duction ?\n";
		return confirm("Voulez-vous r�ellement :\n" + msg + "Cette op�ration est irr�versible.");
	}

	// -->
	</script>
</head>
<body>
<?
if ($reservation['paiement_mode']=='PP')
{
	$res = Paypal::GetTransactionDetails ($reservation['numerotrans']);
	if ($res)
	{
		echo '<pre style="float:right">Informations Paypal'."\n";
		foreach ($res as $k => $v)
			echo "$k :\t$v\n";
		echo '</pre>';
	}
}
?>
<form action="" method="post" onsubmit="return doSubmit(this)">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre">
	<img src="img/puce.gif" align="absmiddle" hspace="1">
<? 
	echo "Annuler une r�servation";
?>
	</th>
</tr>
<tr>
	<td>
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<? if ($x && $x->hasMessages()) : ?> 
		<tr>
			<td colspan="2">
				<strong><?=join('<br/>', $x->getMessages())?></strong>
				<? if ($code = $motif->getPromotionCode()) : ?>
				<br/><br/><strong>Le code <?=$code['code']?> valable jusqu'au <?=$code->getExpirationDate()?> a �t� attribu�.</strong>
				<? endif; ?>
			</td>
		<tr>
		<? endif; ?>
		<tr><td>R�servation</td><td><?=$reservation->getResIDClient()?></td></tr>
		<tr><td>Client</td><td><?='(#'.$reservation['client_id'].') '.$client['email']?></td></tr>
		<tr><td>Canal</td><td><?=$reservation['canal'].($reservation['canal']=='TEL' ? ' ('.$reservation['agent'].')': '')?></td>
		<tr><td>Agence</td><td><?=$reservation['agence']?></td></tr>
		<tr><td>Forfait</td><td><?=$reservation['forfait']?></td></tr>
		<tr><td>Cat�gorie</td><td><?=$categorie['mnem']?></td></tr>
		<tr><td>D�but</td><td><?=$reservation->getDebut()?></td></tr>
		<tr><td>Fin</td><td><?=$reservation->getFin()?></td></tr>
		<tr><td>Paiement</td><td><?=$reservation->getDate('paiement')?></td></tr>
		<? if (!$reservation->isLocapass()) : ?>
		<tr><td>Montant pay�</td><td><?=$reservation->getTotal() ?>&nbsp;�</td></tr>
		<tr><td colspan="2"><strong>D�tail Facturation et Avoir</strong></td>
		<? foreach($facturations as $f) : ?>
		<tr>
			<td>
				<?=strtoupper($f['facturation']).' - '.($f['type']=='F' ? 'Facture' : 'Avoir')?>
			</td>
			<td>
				<?=$f->getDate('d/m/Y H:i')?> - <?=$f['montant']?> �
				<? if ($f['erreur']) echo ' - <strong style="color:red">'.$f['erreur'].'</strong>'; ?>
			</td>
		</tr>
		<? endforeach; ?>
		<? endif; ?>
		<? if ($reservation['statut']=='P') : ?>
			<? if ($reservation['paiement_mode']=='PB' && !$reservation->isCancellableByPaybox()) : ?>
			<tr><td colspan="2"><strong>Attention! Le paiement a eu lieu il y a plus de 2 mois!<br/>Vous devrez rembourser par ch�que.</strong></td></tr>
			<? endif; ?>
		<tr><td colspan="2"><strong>Remboursement</strong></td>
		<tr>
			<td><label for="mode">Mode de remboursement</label></td>
			<td>
				<?
					$a = array();
					if ($reservation->isCancellableByPaybox())
						$a['PB'] = 'Paybox';
					if ($reservation['paiement_mode']=='PP')
						$a['PP'] = 'PayPal';
					if ($reservation['paiement_mode']=='LP')
						$a['LP'] = 'Locapass';
					else
						$a['CQ'] = 'Ch�que';
					selectarray('mode', $a, null);
				?>
			</td>
		</tr>
		<tr>
			<td><label for="motif">Motif</label></td>
			<td><? selectarray('motif', ResAnnulationMotif_Collection::factory(), null, '--'); ?></td>
		</tr>
		<? if (SITE_MODE!='PROD') : ?>
		<tr>
			<td>Code R�duction</td>
			<td><? selectarray('campagne', PromotionCampagne_Collection::factory(true, date('Y-m-d')), $_POST['campagne'], '-- distribuez un code de r�duction --'); ?></td>
		</tr>
		<? endif; ?>
		<tr>
			<td><label for="mail">Envoi d'un e-mail</label></td>
			<td>
				<? selectarray('mail', array('1'=>'oui','0'=>'non'), null); ?>
			</td>
		</tr>
		<? foreach($avoirs as /** @var Facturation */ $f) : // montant � rembourser ?>
		<tr>
			<td><label for="rembourse_<?=$f['facturation']?>">Montant <?=strtoupper($f['facturation'])?></label></td>
			<td>
				<? if ($f['facturation']=='ada') : ?>
				<input type="text" id="rembourse_ada" name="rembourse_ada" style="text-align: right;" value="<?=$f['montant']?>" size="8" maxlength="8"/>&nbsp;�
				<? else : ?>
				<? selectarray('rembourse_'.$f['facturation'], array($f['montant']=>$f['montant'].' �', 0 => '0 �'), null, '', 'style="width: inherit;"'); ?>
				<? endif; ?>
				<? echo $f['erreur']; // s'il y a une erreur lors de la connexion � UNIPRO porur le mat�riel ?>
			</td>
		</tr>
		<? endforeach; ?>
		<tr><td colspan="2" align="right"><input type="submit" name="annuler" value="rembourser"></td></tr>
		<? endif; ?>
		<? if ($reservation['remboursement']) : ?>
		<tr><td>Date du remboursement</td><td><?=$reservation->getDate('remboursement')?></td></tr>
		<tr><td>Montant rembours�</td><td><?=($reservation['rembourse'] ? $reservation['rembourse'] : '-')?>&nbsp;�</td></tr>
		<tr><td>R�sidu</td><td><?=(($reservation['total']==$reservation['rembourse']) ? '-' : $reservation['total']-$reservation['rembourse'])?>&nbsp;�</td></tr>
		<tr><td>Mode de remboursement</td><td><?=$reservation['remboursement_mode']?> <em>(PB: PayBox, PP: PayPal, CQ: ch�que)</em></td></tr>
		<tr><td>Motif(s)</td><td><?=$reservation['annulation_motif'];?></td></tr>
		<? endif; ?>
		</table>
	</td>
</tr>
<tr>
	<td>
		<input type="button" onclick="if (window.opener) window.close();" name="close" value="fermer">
	</td>
</tr>
</table>
</form>
</body>
</html>
