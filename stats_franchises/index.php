<?
	require_once('../constant.php');
	require_once('../admin/lib.php');
	// acc�s soit par v�rification d'une cl� soit par l'admin
	if ($_GET['code_societe'] && $_GET['key'] && $_GET['key']==stats_franchise_key($_GET['code_societe']))
	{
		require_once(dirname(dirname(__file__)).'/constant.php');
		require_once(BP.'/lib/lib.sql.php');
		require_once(BP.'/lib/lib.util.php');
	} else {
		$adminUser = Acces::createFromCookie($_COOKIE['ADA001_ADMIN_AUTH']);
		if (!$adminUser || !$adminUser->hasRights(Acces::SB_ADMIN)) {
			die("Vous n'avez pas les droits pour voir cette information !");
		}
		unset($_GET['key']);
	}
	require_once(BP.'/lib/lib.tcp.php');
	// on doit avoir un code soci�t�
	$code_societe = filter_var($_GET['code_societe'], FILTER_SANITIZE_STRING);
	if (!preg_match('/^[A-Z0-9]{2}$/', $code_societe))
		die("Vous devez pr�ciser le code soci�t�");
	// on r�cup�re la liste des agences associ�es
	$agences = Agence_Collection::factory(array('code_societe'=>$code_societe), null, false);
	if (!$agences || $agences->isEmpty())
		die("Aucune agence ne correspond � ce code soci�t� $code_societe !");
	$societe = getsql("select societe from agence where code_societe='$code_societe' and statut&1=1 limit 1");
	// faire la liste des mois
	$mois = array();
	$timeBase = strtotime(date('Y-m-01'));
	$timeLimit = strtotime('2012-01-01');
	for($i=0;$i<=48; $i++)
	{
		$t = strtotime("-$i month", $timeBase);
		if ($t < $timeLimit) break;
		$mois[date('Y-m-01', $t)] = $MOIS[date('n',$t)].' '.date('Y',$t);
	}
	// v�rifier et ou initialiser $_GET['agence'] et $_GET['periode']
	if ($_GET['agence'] && !$agences[$_GET['agence']])
		unset($_GET['agence']);
	if (!$_GET['periode'] || !$mois[$_GET['periode']])
		$_GET['periode'] = key($mois);
	$debut = strtotime($_GET['periode']);
	$fin = strtotime('+'.(date('t',$debut)-1).' days', $debut);
	$format = in_array($_GET['format'], array('stats','satisfactions')) ? $_GET['format'] : 'stats';

	/**
	* method=stats	==> invariable
	* agences=code agence ou plusieurs codes agence s�par�s par une virgule (ex: FR525A ou C3972B,FR525A)
	* datedeb=date de d�but de la p�riode au format dd-mmm-yyyy (ex: 1-jan-2012)
	* datefin=date de fin de la p�riode au format dd-mmm-yyyy (ex: 13-apr-2012)
	*/
	define('NSP_SERVER', 'http://ada.smartp.com');
	define('NSP_URL_STATS', '/sp_ada.cfm?method=stats&agences=%s&datedeb=%s&datefin=%s');
	define('NSP_URL_TOP', '/sp_ada.cfm?method=top5&agence=%s&year=%d&month=%d');
	define('NSP_FMT_DATE', 'j-M-Y');
	
	/**
	* Renvoyer un tableau de donn�es associatif depuis les donn�es de SMARTP
	* 
	* @param mixed $url
	*/
	function get_data_from_nsp($url)
	{
		$content = trim(file_get_contents($url));
		if ($content)
		{
			$json = json_decode($content, true);
			if ($json)
			{
				$data = array();
				$columns = $json['COLUMNS'];
				foreach($json['DATA'] as $row)
				{
					$x = array();
					foreach ($row as $k => $v)
						$x[$columns[$k]] = $v;
					$data[] = $x;
				}
				return $data;
			}
		}
	}
	// �crire une ligne du tableau des statistiques
	function write_stat($stat, $lbl, $sans_famille = true, $odd = true)
	{
		$SEUIL_INDISPO = 30;
		if ($stat['RECHERCHES'] > 0)
			$stat['TAUX_INDISPO'] = (100 * $stat['INDISPOS']) / $stat['RECHERCHES'];
?>
			<tr class="row<?=($odd ? "1" : "2")?>">
				<td><?=$lbl?></td>
				<td align="right"><?=($sans_famille ? max(0,$stat['RECHERCHES']) : '-')?></td>
				<td align="right"><?=($sans_famille ? max(0, $stat['INDISPOS']) : '-')?></td>
				<td align="right" <? if ($stat['TAUX_INDISPO'] > $SEUIL_INDISPO) echo ' class="alerte"';?>><?=($sans_famille && $stat['RECHERCHES'] ? number_format($stat['TAUX_INDISPO'], 2, '.', ' ').' %' : '-')?></td>
				<td align="right"><?=$stat['RESERVATIONS']?></td>
				<td align="right"><?=max(0, $stat['ANNULATIONS'])?></td>
				<td align="right"><?=($sans_famille && $stat['RECHERCHES'] ? number_format((100 * $stat['RESERVATIONS'])/$stat['RECHERCHES'], 2, '.', ' ').' %' : '-');?></td>
				<td align="right"><?=number_format(max($stat['CA'], 0), 0, '.', ' ')?>  �</td>
			</tr>
<?
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<title>ADA - Statistiques Franchis�s</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1"/>
	<link rel="stylesheet" href="style.css"/>
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
	<script type="text/javascript">
		google.load('jquery', '1.4');
		google.load('visualization', '1.0', {'packages':['corechart']});
	</script>
	<script type="text/javascript">
	// <![CDATA[
		$(document).ready(function()
		{
			$('#periode, #agence').change(function(){ this.form.submit(); });
			$('#lien-satisfactions, #lien-stats').click(function(){
				tag = this.id.substr(5);
				$('#format').val(tag);
				$('#satisfactions, #stats').each(function(){
					if (this.id == tag)
						$(this).show();
					else
						$(this).hide();
				});
			});
		})
	// ]]>
	</script>
	<style type="text/css">
		select,input { width: inherit;}
		label { padding-left: 2px; }
		label.col1 { padding-left: 0; float: left; width: 65px;}
		tr.row-type { background-color: #cccccc;  }
		tr.row0 { background-color: #cccccc; }
		tr.row2 { background-color: #eeeeee; }
		.stats-franchises table.stats td, .stats-franchises table.stats th { border : none; padding: 3px; }
		.stats-franchises table.stats tr.row-type th { text-align: left; padding: 6px 3px 3px 3px; border-top: 1px solid #333333;}
		.stats-franchises table.stats tr.row0 th { border-bottom: 1px solid #333333;}
	</style>
</head>
<body class="stats-franchises">
<div class="container">
	<h1 class="logo"><img src="img/logo_ada.png" alt="Ada, location de v�hicules" /></h1>
	<h1>Tableau de bord pour <?=$societe?> (<?=$code_societe?>)</h1>
	<form action="" method="get">
		<input type="hidden" id="format" name="format" value="<?=$format?>"/>
		<? if ($_GET['key']) : ?>
		<input type="hidden" name="key" value="<?=$_GET['key']?>"/>
		<? endif; ?>
		<input type="hidden" name="code_societe" value="<?=$code_societe?>"/>
		<label for="periode">P�riode :</label><? selectarray('periode', $mois, $_GET['periode']); ?>
		<label for="agence">Agence : </label>
		<select id="agence" name="agence">
			<option value="">Cumul</option>
			<?
			foreach($agences as $k => $agence)
			{
				echo '<option value="'.$k.'"';
				if ($k == $_GET['agence']) echo ' selected="selected"';
				echo '>'.$k.' - '.$agence.'</option>'."\n";
			}
			?>
		</select>
	</form>
	<div id="stats" class="col agences" <? if ($format!='stats') echo 'style="display: none;"';?>>
		<h2>R�sultats</h2>
		<?
		// statistiques
		$url = NSP_SERVER.sprintf(NSP_URL_STATS, $_GET['agence'] ? $_GET['agence'] : join(',', $agences->getIds()), strtolower(date(NSP_FMT_DATE, $debut)), strtolower(date(NSP_FMT_DATE, $fin)));
		if ($data = get_data_from_nsp($url))
		{
			// on construit le tableau des statistiques regroupant par cat�gories
			$stats  = array();
			$stats_famille = array();
			foreach ($data as $row)
			{
				if (!$first)
				{
					$first = true;
				}
				// le cas de D+...
				if ($row['CAT']=="catD'")
					$row['CAT'] = 'catD+';
				$cat = $row['CAT'];
				if (!$cat) continue;
				foreach(array('CAT', 'RECHERCHES','INDISPOS','RESERVATIONS') as $k)
				{
					$stats[$cat][$k] += $row[$k];
					$stats['TOTAL'][$k] += $row[$k];
				}
			}
			
			// requ�te SQL pour les informations depuis la base de donn�es
			$sql = "select IFNULL(CONCAT('cat',c.mnem),'TOTAL') CAT, sum(r.total) CA, count(r.id) RESERVATIONS, COUNT(NULLIF(r.statut,'P')) ANNULATIONS";
			$sql.=" from categorie c";
			$sql.=" left join reservation r on (";
			$sql.=" r.paiement between '".date('Y-m-d ', $debut)."' and '".date('Y-m-d 23:59:59', $fin)."'";
			$sql.=" and r.statut in ('P','A') and r.pdv in ('".($_GET['agence'] ? $_GET['agence'] : join("','", $agences->getIds()))."')";
			$sql.=" and r.categorie=c.id";
			$sql.=" )";
			$sql.=" where c.publie=1";
			$sql.=" group by c.mnem with rollup";
			
			// ex�cuter la requ�te et compl�ter les stats
			$rs = sqlexec($sql);
			while($row = mysql_fetch_assoc($rs))
			{
				$cat = $row['CAT'];
				foreach(array('CA','RESERVATIONS', 'ANNULATIONS') as $k)
					$stats[$cat][$k] = $row[$k];
			}
			// et on rassemble toutes les informations avant d'�crire le tableau
			$categories = array();
			foreach(Categorie::$TYPE_LBL as $type =>  $lbl)
			{
				if ($type == 'ml') continue; 
				foreach(Categorie_Collection::factory('fr', $type) as $categorie)
				{
					$cat = 'cat'.$categorie['mnem'];
					$categories[$type][$cat] = $categorie['nom']; // $categorie['mnem'].' - '.$categorie['nom'];
					$famille = $categorie['famille_nom'];
					if ($famille)
					{
						$stats[$cat]['famille'] = $famille;
						$stats_famille[$famille]['NB'] += 1;
						foreach(array('CA','RESERVATIONS','RECHERCHES','INDISPOS','ANNULATIONS') as $k)
							$stats_famille[$famille][$k] += $stats[$cat][$k];
					}
				} //!foreach CATEGORIES
			} // !foreach TYPE
			$headers = array('Cat�gorie', 'Recherche','Indispo','% Indispo','R�servations','Annulations','Tx Transfo','CA');
			?>
			<table class="framed stats">
			<colgroup>
				<col width="130" />
				<col width="95" />
				<col width="95" />
				<col width="95" />
				<col width="95" />
				<col width="95" />
				<col width="95" />
				<col width="64" />
			</colgroup>
			<? 
			// titres
			echo '<tr class="row-type"><th colspan="8">Tous les types</th></tr>'."\n";
			echo '<tr class="row0"><th>'.join('</th><th>', $headers).'</th></tr>'."\n";
			// total
			write_stat($stats['TOTAL'], '<strong>Toutes</strong>');
			// les types
			foreach(Categorie::$TYPE_LBL as $type =>  $lbl)
			{
				if ($type == 'ml') continue; 
			?>
				<tr class="row-type"><th colspan="8"><?=ucfirst($lbl)?></th></tr>
				<?
				echo '<tr class="row0"><th>'.join('</th><th>', $headers).'</th></tr>'."\n";
				$odd = true;
				foreach($categories[$type] as $cat => $name)
				{
					// �crire la famille si elle a chang�
					if ($famille = $stats[$cat]['famille'])
					{
						if ($stats_famille[$famille]['NB'] == 1)
							$famille = null;
						else
						{
							if (!$stats_famille[$famille]['DONE'])
							{
								write_stat($stats_famille[$famille], $famille, true, $odd);
								$odd = !$odd;
								$stats_famille[$famille]['DONE'] = true;
							}
							// on n'�crit pas le d�tail pour les cat�gories appartenant � une famille
							continue;
						}
					}
					write_stat($stats[$cat], $name, is_null($famille), $odd);
					$odd = !$odd;
				} //!foreach CATEGORIES
			} // !foreach TYPE
			?>
			</table>
		<? } // !if ($data NSP) ?>
	</div>

	<!-- TOP 5 ET SATISFACTIONS -->
	<div class="col top5">
	<?
		// satisfaction clients
		$themes = array('agence' => "l'agence", 'vehicule' => "le v�hicule",);
		$args = array(
			'date_to'	=> date('d-m-Y 23:59:59', $fin > time() ? time() : $fin),
			'pdv'	=> ($_GET['agence'] ? $_GET['agence'] : $agences->getIds())
		);
		$args['date_from'] = date('01-m-Y', strtotime('-2 months', $debut));
		$x = QuestionResults::factory($args);
		if ($total = $x->getTotal()) : ?>
			<? $franchise = 100 * (1 - $x->getThemeNote('services additionnels')); ?>
			<div class="satisfaction-client">
				<h2>Satisfaction client</h2>
				<p>
					<?=$total ?> r�ponses
					<br/>du <?=$args['date_from'];?>
					<br/>au <?=substr($args['date_to'], 0, 10)?>
				</p>
				<? foreach($themes as $theme) : ?>
					<? $note = $x->getThemeNote($theme); ?>
					<? $pct = round((100 * $note) / 4); ?>
					<?=$theme?> :<br />
					<div class="rating-box">
						<div class="rating" style="width:<?=$pct?>%;"></div>
					</div>
				<? endforeach; ?>
				<br/>Assurance propos� � <strong style="color: <?=$franchise >= 50 ? 'green' : 'red';?>"><?=$franchise?>%</strong> des clients
				<br/><a href="#" id="lien-satisfactions" class="btn">d�tail<span class="end"></span></a>
			</div>
		<?
		endif; // total questionnaires 
		// TOP5
		$url = NSP_SERVER.sprintf(NSP_URL_TOP, $_GET['agence'] ? $_GET['agence'] : join(',', $agences->getIds()), date('Y', $debut), date('n', $debut));
		if (false && $data = get_data_from_nsp($url)) :
		?>
			<h2>TOP 5</h2>
			<table class="framed stats top5" border="1" cellpadding="2" cellspacing="0">
			<colgroup>
				<col width="18" />
				<col width="80" />
				<col width="100" />
			</colgroup>
			<tr>
				<th colspan="3" style="background: url(img/bg_list.gif) repeat-x scroll 0% 100% transparent;">Top 5 CA <?=$mois[$_GET['periode']]?></th>
			</tr>
			<?
			foreach($data as $row) :
				$agence = $agences[$row['AGENCE']];
				if ($agence)
					$class = "current";
				else
				{
					$class = '';
					$agence = Agence::factory($row['AGENCE']);
				}
				?>
				<tr class="<?=$class?>">
					<td align="right"><?=$row['CLASSEMENT']?>.</td>
					<td align="right" class="chiffre-affaire"><?=show_money($row['CA'])?> �</td>
					<td><?=$row['AGENCE']?> - <?=$agence?></td>
				</tr>
			<? endforeach; ?>
			</table>
		<? endif; // $data NSP ?>
	</div>
	<!-- DETAILS SATISFACTIONS -->
	<div id="satisfactions" <? if ($format!='satisfactions') echo 'style="display: none;"';?>>
		<a href="#" id="lien-stats" class="btn">retour<span class="end"></span></a>
		<? if ($total = $x->getTotal()) : ?>
		<div class="satisfaction-questions">
		<?
			foreach($x->getResults() as $theme => $questions) :
				if (!in_array($theme, $themes)) continue;
			?>
				<div class="satisfaction-<?=array_search($theme, $themes);?>">
					<h2><?=$theme;?></h2>
					<? foreach($questions as $question) : ?>
						<div class="satisfaction-detail">
							<h3><?=$question['question'];?></h3>
							<div class="container-satis">
							<? 
							$k = 0;
							foreach (array_reverse($question['choix']) as $choix => $nb) :
								$k++;
 								$pct = round((100 * $nb)/$question['total']);
 								?>
								<?=$choix?><br />
								<span class="barre-satis barre-satis-<?=$k?>" style="width:<? echo ($pct * 1.2)?>px;"></span><span class="nbr-satis"><?=$pct?>%</span>
								<br/>
							<? endforeach; ?>
							</div>
						</div>
					<? endforeach; ?>
					<div class="clear"></div>
				</div>
				<? if ($theme=="l'agence") : // on met le cambert entre "agnece" et "v�hicule" ?>
					<div class="satisfaction-services">
						<h2>services</h2>
						<div class="satisfaction-detail camembert">
							<h3>vous a-t-on propos�<br />l'assurance franchise remboursable ?</h3>
							<div class="container-satis">
								<div id="visualization"></div>
							</div>
						</div>
					</div>
				<? endif; ?>
			<? endforeach; ?>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
		<script type="text/javascript">
		// <![CDATA[
			function drawVisualization()
			{
				// Create and populate the data table.
				var data = google.visualization.arrayToDataTable([
					['Question', 'R�ponses'],
					['Oui', <?=$franchise?>],
					['Non', <?=100-$franchise?>]
				]);
				// Create and draw the visualization.
				new google.visualization.PieChart(document.getElementById('visualization')).draw(data, {
					title:"Assurance", 
					colors:['#01ad12', '#c74b3e'],
					width: 160,
					height:138
				});
			}
			// Set a callback to run when the Google Visualization API is loaded.
			google.setOnLoadCallback(drawVisualization);
			// ]]>
		</script>
	<? else: ?>
		<p>Aucune r�ponse � l'enqu�te de satisfaction pour cette agence dans cette p�riode.<br/>Vous pouvez consulter une autre agence ou voir le tableau de bord en cliquant sur le bouton <strong>retour</strong>.</p>
	<? endif; // r�sultats questionnaires ?>
	</div>
	<div class="clear"></div>
</div>
<? 
// Stats NSP
if (!$adminUser)
{
	$page = new Page();
	$page->setId('admin/stats_franchise');
	$page->setTrackPage($_GET['code_societe'], true);
	echo $page->writeJSStats('admin');
}
?>
</body>
</html>
