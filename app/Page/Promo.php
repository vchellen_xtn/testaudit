<?
class Page_Promo extends Page
{
	
	public function getUniversTitre() {
		return "Promotions";
	}
	
	public function indexAction()
	{
		// on filtre les arguments de $_GET
		if ($_GET['type'] && !Categorie::$TYPE[strtolower($_GET['type'])])
			unset($_GET['type']);
		if ($_GET['agence'] && !Agence::isValidID($_GET['agence']))
			unset($_GET['agence']);
		if ($_GET['dept'] && !preg_match('/^\d{2}$/', $_GET['dept'])) 
			unset($_GET['dept']);
		/* 2012-10-01 : on redirige les pages agences et département */
		if ($_GET['agence'])
		{
			if ($agence = Agence::factory($_GET['agence']))
			{
				self::redirect($agence->getURLAgence(), 301);
				return;
			}
		}
		if ($_GET['dept'])
		{
			if ($dept = Dept::factory($_GET['dept']))
			{
				self::redirect($dept->getURL(), 301);
				return;
			}
		}
			
	}
}
?>
