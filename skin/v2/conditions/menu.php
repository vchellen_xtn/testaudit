<?
	$menu = array(
		'internet'	=> 'Réservation sur internet',
		'assurance_complementaire'	=> 'Assurance complémentaire',
		'location'	=> 'Conditions de location',
//		'rachat_de_franchise'	=> 'Rachat de franchise',
	);
	$action = basename($page);
?>
<ul class="menu">
	<? foreach($menu as $k => $lbl) : ?>
		<li>
			<a href="<?=$k?>.html"<?if ($action==$k) echo ' class="current"'; ?>><?=$lbl;?></a>
		</li>
	<? endforeach; ?>
</ul>
