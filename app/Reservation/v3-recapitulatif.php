<?php
/**
* $this est Reservation
* $page page o� se trouve le bloc
* $showTotal : afficher le total ou pas ?
* $showPrixOptions : affichr ou non le prix des options
* $lblTotal : label pour le Prix Total
*/
	if (!$lblTotal) $lblTotal = 'Prix';
	/** @var Reservation */
	$reservation = $this;
	/** @var ResOption_Collection */
	$options = $reservation->getResOptions();
	/** @var Categorie */
	$categorie = $reservation->getCategorie();
	/** @var Agence */
	$agence = $reservation->getAgence();
?>
<div class="reservation_recapitulatif">
	<div id="agence" class="col col_width33 col_1">
		<p class="infos_agence_adresse">
			<span class="nom_agence"><?=$agence['nom']?></span>&nbsp;
			<br /><?=$agence->getHtmlAddress('<br/>','adresse1,adresse2,cp_ville,tel'); ?>
			<? if ($agence->isPointLoc()) echo "<img class='picto-point-loc' src='../css/img/structure-2015/picto-point-loc.png' alt='point loc partenaire ADA' />"; ?>
		</p>
		<a href="#" onclick="if (appADA && appADA.showPopIn) { appADA.showPopIn('agence', {agence: '<?=$agence['id']?>'}, {dialogClass: 'popin-options'}); }; return false;" class="infos_agence">
			> Horaires et informations de localisation
		</a>
	</div>
	<div id="vehicule" class="col col_width33 col_2">
		<img src="../<?=$categorie->getImg('med', 'categories3')?>" class="visu" alt="" />
		<? if (strstr('AP', $reservation['statut'])) : ?>
			<p class="numero_resa"><strong>R�servation n&#176;</strong><? echo $reservation->getResIDClient(); ?></p>
		<? endif; ?>
		<p  class="dates">R�servation du <?=$reservation->getDebut('d/m/y H:i');?> au <?=$reservation->getFin('d/m/y H:i');?></p>
		<p  class="nom_vehicule">
			<span class="famille"><?=$categorie['famille_nom']?></span>
			<span class="categorie"><?=$categorie['categorie_nom']?></span>
		</p>
		<p>Pour <?=$reservation->getDuree();?></p>
		<? if (!$reservation->isLocapass()) :?>
			<p class="km_inclu"><?=$reservation->getKm(false);?></p>
		<? elseif($conducteur = $reservation->getConducteur()) :?>
			<p class="conducteur"><strong>Conducteur </strong><?=$conducteur['prenom'].' '.$conducteur['nom'];?></p>
		<? endif; ?>
	</div>
	<div id="resa" class="col col_width33 col_3">
	<? if ($reservation->isLocapass()) :?>
		<img src="../css/img/locapass.png" alt="Locapass" />
	<? else :?>
		<? if ($showPrixOptions) : ?>
		<ul>
			<li class="res_opt_detail">
			<table class="res_opt_nom res_opt_dk02">
				<tr>
					<td class="res_opt_name">Tarif de r�servation</td>
					<td class="res_opt_prix"><?=sprintf('%01.2f', $reservation->getPrixForfait());?>&nbsp;�</td>
				</tr>
				<? if ($surcharge = $reservation->getPrixSurcharge()) : ?>
				<tr>
					<td class="res_opt_name"><?=$reservation->getAgence()->getSurchargeLabel();?></td>
					<td class="res_opt_prix"><?=sprintf('%01.2f', $surcharge);?>&nbsp;�</td>
				</tr>
				<? endif; ?>
				<? if ($prix_coupon = $reservation->getPrixCoupon()) : ?>
				<tr>
					<td class="res_opt_name">Offre Pro</td>
					<td class="res_opt_prix"><?=sprintf('%01.2f', $prix_coupon);?>&nbsp;�</td>
				</tr>
				<? endif; ?>
			</table>
			</li>
		</ul>
		<? endif; ?>
		<? if ($reservation->getResOptions()->count()) : ?>
			<? echo $reservation->getResOptions()->toHtml3($showPrixOptions); ?>
		<? endif; ?>
		<? if ($code = $reservation->getPromotionCode()) : ?>
		<ul>
			<li class="res_opt_detail">
			<table class="res_opt_nom res_opt_dk02">
				<tr>
					<td class="res_opt_name">R�duction <?=$code['campagne_nom']?></td>
					<td class="res_opt_prix">-<?=sprintf('%01.2f', $code['reduction']);?>&nbsp;�</td>
				</tr>
			</table>
			</li>
		</ul>
		<? endif; ?>
		<div class="spacer-prix-total"></div>
		<? if ($showTotal): ?>
			<p class="total">
				<span class="total"><?=$lblTotal;?></span>
				<span class="prix_total" id="recap_prix_total" data-var="<?=$reservation->getPrixTotal()-round($code['reduction_max']-$code['reduction'],2);?>">
					<?=show_money($reservation->getPrixTotal())?> &euro;
				</span>
				<? if ($reservation['statut'] == 'C' && $reservation->getId()) : ?>
				<a href="#" class="price-details" onclick="if (appADA && appADA.showPopIn) { appADA.showPopIn('forfait_details', {'reservation': '<?=$reservation->getId();?>', 'key': '<?=Page::getKey($reservation['id'])?>'})};  return false;">D�tails et devis</a>
				<? endif; ?>
			</p>
		<? endif; ?>
	<?endif; // !$isLocapass ?>
	</div>
	<div class="clear"></div>
</div>
<? if (!strstr('VPXA', $reservation['statut'])) : ?>
<form id="frm_recapitulatif" name="reservation" method="post" action="<?=Page::getURL('reservation/resultat.html');?>">
<?
	foreach($reservation->getFields() as $k => $v) {
		if (!$v) continue;
		echo '<input type="hidden" name="'.$k.'" value="'.$v.'"/>';
	}
	if ($reservation->hasForfait()) {
		echo '<input type="hidden" name="forfait" value="'.$reservation->getForfait()->doSerialize().'"/>';
	}
	$type = strtolower($reservation['type']);
	$date_retrait = null;
	foreach($reservation->getResOptions() as $k => $option) {
		if ($option['quantite'])
			echo '<input type="hidden" name="options_u['.$k.']" value="'.$option['quantite'].'"/>';
		else
			echo '<input type="hidden" name="options[]" value="'.$k.'"/>';
		if (!$date_retrait && $option['retrait'])
			$date_retrait = $option['retrait'];
	}
	if ($date_retrait)
		echo '<input type="hidden" name="options_retrait" value="'.$date_retrait.'"/>';
?>
</form>
<script type="text/javascript">
/* <![CDATA[ */
$(document).ready(function() {
	$("#nav-resa li:first a").click(function() {
		if (appADA.showOverlayLoader) {
			appADA.showOverlayLoader();
		}
		$("#frm_recapitulatif").submit();
		return false;
	});
	$("#nav1").click(function() {
		if (appADA.showOverlayLoader) {
			appADA.showOverlayLoader();
		}
		$("#frm_recapitulatif").submit();
		return false;
	});
	$("#nav1").css({cursor: "pointer"});
});
/* ]]> */
</script>
<? endif; ?>
