<?

class Client extends Ada_Object
{
	const SALT_LENGTH = 8;

	static public $IDENTITE_TYPE = array(
		'cni'		=>"Carte nationale d'identit�",
		'passeport'	=> 'Passeport'
	);
	static private $_STRING_FIELDS = array
	(
		'pwd',
		'prenom',
		'nom',
		'naissance_lieu',
		'identite_numero',
		'adresse',
		'adresse2',
		'cp',
		'ville',
		'f_societe',
		'f_adresse',
		'f_adresse2',
		'f_cp',
		'f_ville',
	);


	/** @var Partenaire */
	protected $_partenaire = null;

	/**
	* Cr�e un objet � partir d'un identifiant
	*
	* @param string $id
	* @return Client
	*/
	static public function factory($id)
	{
		// v�rifier les arguemnts
		if (!is_numeric($id) && !filter_var($id, FILTER_VALIDATE_EMAIL))
			return null;
		// cr�er le client
		$x = new Client();
		if ($x->load($id)->isEmpty())
			return null;
		return $x;
	}

	/**
	* Cr�ation d'un nouveau client avec validation des donn�es
	*
	* @param array $data
	* @param array $errors
	* @param bool $isUTF8
	* @returns Client
	*/
	static public function create(&$data, &$errors, $isUTF8 = false)
	{
		// si le client existe d�j�, on le met � jour, sinon on le cr�e
		$sendNotification = false;
		$x = Client::factory($data['email']);
		if (!$x) {
			$x = new Client(array('id' => ''));
			if (!$data['pwd']) {
				include_once(BP.'/lib/lib.fips181.php');
				$fips = generate_password(6, 8);
				$data['pwd'] = $fips[0];
				$sendNotification = true;
			}
		}
		// conversion depuis UTF-8 si n�cessaire
		if ($isUTF8) {
			foreach(self::$_STRING_FIELDS as $k) {
				if (isset($data[$k])) {
					$data[$k] = iconv('UTF-8', 'WINDOWS-1252//TRANSLIT', $data[$k]);
				}
			}
		}


		// valider les donn�es avant de les enregistrer
		if ($x->validateData($data, $errors))
		{
			// r�cup�rer la lsite des champs, avec le mot de passe si n�cessaire et sans l'identifiant
			$fields = array_keys($x->_getValidatingRules());
			if ($pwd = trim($data['pwd'])) {
				$data['pwd'] = self::generateHash($pwd);
				$fields[] = 'pwd';
			}
			unset($fields[array_search('id', $fields)]);
			// mettre � jour les donn�es du client
			foreach($fields as $k) {
				if (isset($data[$k])) {
					$x->setData($k, $data[$k]);
				}
			}
			// enregistrer les informations
			$x->save();
			if (!$x['id']) {
				$x->setId(mysql_insert_id());
			}
			// envoyer un mail au client si demand�
			if ($sendNotification) {
				$a = array(
					'date_jour'		=> date('d-m-Y'),
					'email'			=> $x['email'],
					'titre'			=> $x['titre'],
					'prenom'		=> $x['prenom'],
					'nom'			=> $x['nom'],
					'mot_de_passe'	=> $pwd,
					'url_reset_pwd'	=> $x->getURLResetPassword(),
				);
				$html = load_and_parse('emails/em-adafr-client-creation.html', $a);
				send_mail($html, $x['email'], SUBJECT_ADAFR_ACCOUNT, EMAIL_FROM, EMAIL_FROM_ADDR, '', BCC_EMAILS_TO);
			}
			return $x;
		}
		return null;
	}
	/**
	* V�rifie la cl� et renvoie un client s'il existe
	*
	* @param string $x
	* @param string $msg
	* @returns Client
	*/
	static public function checkKey($x, &$msg)
	{
		list($t, $email, $key) = unserialize(base64_decode($x));
		if ($key != md5($t.$email.PRIVATE_KEY))
		{
			$msg = "Votre identifiant n'est pas reconnu, ou vous ne pouvez pas mettre � jour votre mot de passe par ce moyen.";
			return;
		}
		if ($t + 10*3600 < time())
		{
			$msg = "Vous aviez 24h pour modifier votre mot de passe.";
			$msg.='<br/>Vous pouvez demander de nouveau � <a href="'.Page::getURL('client/motdepasse.html?email='.htmlspecialchars(strip_tags($email))).'">r�-initialiser votre mot de passe</a>';
			return;
		}
		// on essaie de cr�er le client
		$client = Client::factory($email);
		if (!$client)
			$msg = "Votre identifiant ".htmlspecialchars(strip_tags($email))." n'est pas reconnu";
		return $client;
	}

	/**
	* G�n�re un mot de passe d'apr�s http://phpsec.org/articles/2005/password-hashing.html
	* Usage :
	* 	$crypt  = Client::generateHash('foo');
	*   $crypt == Client::generateHash('foo', $crypt);
	*
	* @param string $plainText	mot de passe en clair
	* @param string $salt		vide si on le cr�e, sinon le mot de passe encrypt� pour r�cup�rer le SALT
	* @returns string mot de passe encrypt�
	*/
	static public function generateHash($plainText, $salt = null)
	{
		if (!$salt)
			$salt = substr(md5(uniqid(rand(), true)), 0, self::SALT_LENGTH);
    	else
			$salt = substr($salt, 0, self::SALT_LENGTH);
		return $salt.md5($salt.trim($plainText));
	}

	protected function _prepareSQL($id, $cols='*', $data=null)
	{
		$sql = "select c.*, p.nom pays_nom, fp.nom f_pays_nom from {$this->getTableName()} c left join pays p on p.id=c.pays left join pays fp on fp.id=c.f_pays where ";
		if (is_numeric($id))
			$sql.= "c.id='".addslashes($id)."'";
		else
			$sql.="c.email = '".addslashes($id)."'";
		return $sql;
	}
	/**
	* Renvoie l'URL pour remettre � z�ro l'url
	*
	*/
	public function getURLResetPassword()
	{
		$a = array(time(), $this->_data['email']);
		$a[] = md5(join('', $a).PRIVATE_KEY);
		$x = base64_encode(serialize($a));
		return Page::getURL('client/changer-mot-de-passe.html?_x='.$x);
	}
	/**
	* renvoie le mot de passe oubli�
	*
	*/
	public function sendPassword()
	{
		$a = $this->toArray();
		$a['url_reset_pwd'] = $this->getURLResetPassword();
		$html = load_and_parse('emails/em5.html', $a);
		send_mail($html, $this->_data['email'], SUBJECT_LOST_PWD, EMAIL_FROM, EMAIL_FROM_ADDR, '', BCC_EMAILS_TO);
	}
	/**
	* V�rifie le mot de passe
	*
	* @param string $pwd
	* @returns bool
	*/
	public function checkPassword($pwd)
	{
		// on encrypte si le mot de passe ne l'a pas d�j� �t�
		if (strlen($this->_data['pwd']) < 40)
		{
			$this->setData('pwd', self::generateHash($this->_data['pwd']))->save('pwd');
		}
		return ($this->_data['pwd'] == self::generateHash($pwd, $this->_data['pwd']));
	}
	/**
	* Modifie le mot de passe d'un client
	*
	* @param string $pwd
	*/
	public function changePassword($pwd)
	{
		$this->setData('pwd',self::generateHash($pwd))->save('pwd');
		return $this;
	}
	/**
	* Renvoie les r�servations assoc�es � ce client
	*
	* @param string $fromDate : depuis la date
	* @param int $limit : nombre maximum attendu
	* @param bool $loadFacturation r�cup�rer les facturations ?
	* @return Reservation_Collection
	*/
	public function getReservations($fromDate = null, $limit = null, $loadFacturation = true)
	{
		return Reservation_Collection::factory($this, $fromDate, $limit, $loadFacturation);
	}
	/**
	* Un client est-il Progamme Kilom'�tre
	*
	* @param bool $tryRemote
	* @returns bool
	*/
	public function isPgKm($tryRemote = true)
	{
		// si on ne le sait pas et qu'on peut interroger PgKM
		if (!$this->_data['pgkm'] && $tryRemote)
		{
			if (PgKm::checkEmail($this->_data['email']))
				$this->setData('pgkm', 1)->save('pgkm');
		}
		return ($this->_data['pgkm'] ? true : false);
	}
	/**
	* Renvoie une adresse en HTML
	*
	* @param string $sep s�parateur entre les �l�ments de l'adresse
	* @param mixed $with champs � mettre dans l'adresse, sinon valeur par d�faut
	* @param miexed $except champs � ne pas mettre (adresse1, adresse2, cp_ville, tel, fax)
	* @returns string
	*/
	public function getHtmlAddress($sep = '<br/>', $with = array(), $except = array())
	{
		// champs � prendre en compte
		if ($with && !is_array($with))
			$with = explode(',', $with);
		if ($except && !is_array($except))
			$except = explode(',', $except);
		if (!$with || !is_array($with))
			$with = array('prenom_nom', 'adresse','adresse2','cp_ville', 'pays_nom');

		// cr�er cp_ville si n'existe pas
		if (!$this->hasData('cp_ville'))
			$this->setData('cp_ville', $this->getData('cp').' '.$this->getData('ville'));
		if (!$this->hasData('prenom_nom'))
			$this->setData('prenom_nom', $this->getData('prenom').' '.$this->getData('nom'));
		$prefix = array();
		foreach ($with as $k)
		{
			if (in_array($k, $except)) continue;
			if ($v = $this->getData($k))
				$html[] = $prefix[$k].$v;
		}
		return is_array($html) ? join($sep, $html) : '';
	}

	/**
	 * Renvoie une adresse dans un tableau
	 * Contenant
	 *
	 *
	 * @returns string
	 */
	public function getAddress()
	{
	    $address = array(
	        "prenom" => $this->getData('prenom'),
	        "nom" => $this->getData('nom'),
	        "adresse" => $this->getData('adresse'),
	        "adresse2" => $this->getData('adresse2'),
	        "cp" => $this->getData('cp'),
	        "ville" => $this->getData('ville'),
	        "pays_nom" => $this->getData('pays_nom')
	    );

	    return $address;
	}

	/**
	* Renvoie le partenaire associ�e au client s'il existe
	* @returns Partenaire
	*/
	public function getPartenaire()
	{
		if (!$this->_partenaire)
		{
			if ($id = getsql('select partenaire from partenaire_client where client='.$this->getId()))
				$this->_partenaire = Partenaire::factory($id);
		}
		return $this->_partenaire;
	}
	public function hasComptePro()
	{
		return ($this->getPartenaire() != null);
	}
	/**
	* Renvoie le tableau des r�gles de validation
	*/
	protected function _getValidatingRules()
	{
		return array (
			'id'	=> $this->_addRuleInt(),
			'email'	=> $this->_addRuleEmail("Vous devez pr�ciser l'e-mail", "L'e-mail %s n'est pas valide !"),
//			'pwd'	=> $this->_addRuleString(),
			'titre'	=> $this->_addRuleString("Vous devez indiquer la civilit�"),
			'prenom'=> $this->_addRuleString("Vous devez indiquer le pr�nom"),
			'nom'	=> $this->_addRuleString("Vous devez indiquer le nom"),
			'naissance'	=> $this->_addRuleDate(null, "La date de naissance n'est pas valide"),
			'naissance_lieu' => $this->_addRuleString(),
			'naissance_pays' => $this->_addRuleString(),
			'identite_type'=> $this->_addRuleRegExp('/^cni|passeport$/', null, "Le type de pi�ce %s n'est pas valide."),
			'identite_numero' => $this->_addRuleString(),
			'adresse'	=> $this->_addRuleString("Vous devez indiquer l'adresse."),
			'adresse2'	=> $this->_addRuleString(),
			'cp'	=> $this->_addRuleString("Vous devez pr�ciser le code postal."),
			'ville'	=> $this->_addRuleString("Vous evez pr�ciser la ville"),
			'pays'	=> $this->_addRuleString("Vous devez pr�ciser le pays"),
			'f_societe'	=> $this->_addRuleString(),
			'f_adresse'	=> $this->_addRuleString(),
			'f_adresse2'=> $this->_addRuleString(),
			'f_cp'	=> $this->_addRuleString(),
			'f_ville'	=> $this->_addRuleString(),
			'f_pays'	=> $this->_addRuleString(),
			'tel'	=> $this->_addRulePhone("Vous devez pr�ciser un num�ro de t�l�phone", "Le num�ro de t�l�phone %s n'est pas valide!"),
			'gsm'	=> $this->_addRulePhone(null, "Votre num�ro de t�l�phone mobile %s n'est pas valide !"),
			'promo'	=> $this->_addRuleString(),
			'partenaire'	=> $this->_addRuleString(),
			'pgkm'	=> $this->_addRuleBoolean(),
			'ref'	=> $this->_addRuleString()
		);
	}
	/**
	* Renvoie l'�ge du client � une date donn�e
	*
	* @param string $when YYYY-MM-DD
	* @returns int
	*/
	public function getAge($when = null)
	{
		$age = null;
		if ($naissance = $this->_data['naissance'])
		{
			if (!$when) $when = date('Y-m-d');
			list($bYear, $bMonth, $bDay) = explode('-', $naissance);
			list($cYear, $cMonth, $cDay) = explode('-', $when);
			$age = $cYear - $bYear - 1;
			if ($cMonth > $bMonth)
				$age++;
			else if ($cMonth == $bMonth && $cDay >= $bDay)
				$age++;
		}
		return $age;
	}
	public function fixEncoding()
	{
		$fields = array();
		foreach(self::$_STRING_FIELDS as $k) {
			if ($k == 'pwd') continue;
			if (isset($this->_data[$k])) {
				if (mb_detect_encoding($this->_data[$k], 'ASCII,UTF-8,ISO-8859-1', true) == 'UTF-8') {
					$fields[] = $k;
					$this->_data[$k] = iconv('UTF-8', 'WINDOWS-1252//TRANSLIT', $this->_data[$k]);
				}
			}
		}
		if ($fields) {
			$this->save(implode(',', $fields));
			return true;
		}
		return false;
	}
}
?>