<?

class Zone extends Ada_Object
{
	/** @var ZoneSaison_Collection */
	protected $_saisons;
	
	static public function isValidID($id)
	{
		return preg_match('/^[a-z]{2}$/', $id);
	}
	
	/**
	* Cr�e un objet � partir d'un identifiant
	* 
	* @param string $id
	* @return Zone
	*/
	static public function factory($id)
	{
		if (self::isValidID($id))
		{
			$x = new Zone();
			if ($x->load($id)->isEmpty())
				return null;
		}
		return $x;
	}
	public function __toString()
	{
		return $this->getData('nom');
	}
	
	/**
	* Renvoie la saison associ�e � une zone pour une date donn�e
	* 
	* @param int $t TIMESTAMP
	* @return int identifiant de la saison
	*/
	public function getSaison($t)
	{
		if (!$this->_saisons)
		{
			$this->_saisons = ZoneSaison_Collection::factory($this);
		}
		// obtenir le jour et le mois
		$j = date('j', $t);
		$m = date('n', $t);
		return $this->_saisons->getSaison($j, $m);
/*		
		// on repr�sente la date sous la forme "28 mai" sous la forme 5.28
		$x = $m + (0.01 * $j);
		// on cherche la saison
		$sql = 'SELECT id';
		$sql.=' FROM zone_saison';
		$sql.=" WHERE zone='".$this->getId()."'";
		$sql.=' AND (';
		$sql.='  ('.$x.' BETWEEN (debut_mois + 0.01 * debut_jour) AND (fin_mois + 0.01 * fin_jour))';
		// ou pour les saisons sur 2 ann�es... de 11 � 2 par exemple
		$sql.='  OR';
		$sql.='  (debut_mois > fin_mois ';
		$sql.='   AND (';
		$sql.='       ('.$x.' >= (debut_mois + 0.01 * debut_jour))';
		$sql.='       OR';
		$sql.='       ('.$x.' <= (fin_mois + 0.01 * fin_jour))';
		$sql.='       )';
		$sql.='  )';
		$sql.=' )';
		return getsql($sql);
*/
	}
}
?>