<?
	require_once("secure.php"); 
	require_once("lib.php");

	// identifiant
	$id = getarg("id");
	$agence = getarg('agence');

	// suppression
	if ($id && getarg("delete")) 
	{
		$sql = "delete from agence_horaire where id = '".$id."'";
		getsql($sql);
		unset($id);
	}

	// modification, cr�ation
	if (getarg("update") || getarg("create"))
	{
		save_record('agence_horaire', $_POST);
		if (!$id) $id = mysql_insert_id();
	}

	if ($id)
	{
		$rs = sqlexec("select * from agence_horaire where id = '".$id."'");
		if (!$rs || !($row = mysql_fetch_assoc($rs)))
			die("Aucun enregistrement correspondant.");
		$agence = $row['agence'];
	}
	if (!$agence) die("Vous devez pr�ciser l'agence !")
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script language="javascript" src="../scripts/check.js"></script>
	<script>
	<!-- //
	// doDelete
	function doDelete(frm)
	{
		return (confirm("Voulez-vous r�ellement supprimer cet �l�ment ?\nCette op�ration est irr�versible."));
	}
	
	// doSubmit
	function doSubmit(frm)
	{
		var err = "";
		if(!frm.agence.value.length)
			err += "Veuillez indiquer une agence\n";
		if(!frm.jour.options[frm.jour.selectedIndex].value.length)
			err += "Veuillez indiquer un jour\n";
		if(!ValidateTime(frm.ouverture))
			err += "L'heure d'ouverture est incorrecte\n";
		if(!ValidateTime(frm.fermeture))
			err += "L'heure de fermeture est incorrecte\n";
		if(frm.pause_debut.value.length && !ValidateTime(frm.pause_debut))
			err += "L'heure de d�but de pause est incorrecte\n";
		if(frm.pause_fin.value.length && !ValidateTime(frm.pause_fin))
			err += "L'heure de fin de pause est incorrecte\n";
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		return true;
	}
	
	// -->
	</script>
</head>
<body>
<form action="" method="post">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre">
	<img src="img/puce.gif" align="absmiddle" hspace="1">
<? 
	echo "Horaire";
	if ($id) echo " #$id";
?>
	</th>
</tr>
<tr>
	<td>
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="15%">
		<col width="35%">
		<col width="15%">
		<col width="35%">
		<tr>
			<td><label for="agence">Agence</label></td>
			<td colspan="3">
				<input type="hidden" name="id" value="<?=$id?>">
				<input type="hidden" name="agence" value="<?=$agence?>">
				<a href="agence_pdv.php?id=<?=$agence?>"><?=getsql("select concat(id, ' - ', nom) from agence_pdv where id='$agence'");?></a>
			</td>
		</tr>
		<tr>
			<td><label for="jour">Jour</label></td>
			<td>
			<?
				$j = array("1" => "Lundi", "2" => "Mardi", "3" => "Mercredi", "4" => "Jeudi", "5" => "Vendredi", "6" => "Samedi", "7" => "Dimanche");
				selectarray("jour", $j, $row["jour"], "--");
			?>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td><label for="ouverture">Ouverture</label></td>
			<td><input type="text" style="width:100%" id="ouverture" name="ouverture" value="<?=$row["ouverture"]?>"></td>
			<td><label for="fermeture">Fermeture</label></td>
			<td><input type="text" style="width:100%" id="fermeture" name="fermeture" value="<?=$row["fermeture"]?>"></td>
		</tr>
		<tr>
			<td><b>Pause</b></td>
		</tr>
		<tr>
			<td><label for="pause_debut">d�but</label></td>
			<td><input type="text" style="width:100%" id="pause_debut" name="pause_debut" value="<?=$row["pause_debut"]?>"></td>
			<td><label for="pause_fin">Fin</label></td>
			<td><input type="text" id="pause_fin" name="pause_fin" style="width:100%" value="<?=$row["pause_fin"]?>"></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td align="right">
		<input type="submit" name="<?=$id ? "update" : "create"?>" value="enregistrer" onclick="return doSubmit(this.form);">
<?
	if ($id) 
	{
?>
		&nbsp;
		<input type="submit" onclick="return doDelete(this.form);" name="delete" value="supprimer">
<?
	}
?>
	</td>
</tr>
</table>
</form>

</body>
</html>
