<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?
	$this->writeHead();
?>
	<script type="text/javascript">
	// <![CDATA[ 
	var appADA = {
		"baseUrl": '<?=Page::getURL(null, null, true);?>'
	};
	if (typeof $ !== "undefined")
	{
		// gestion de la nav principale
		$(document).ready(function()
		{
			// clic pour les tablettes
			$('#menu--header .dropdown-trigger').click(function(){
				if ($('#container--dropdown .dropdown-menu--header:animated, #container--dropdown:animated').length > 0) {
					return false;
				}
				if ($(this).hasClass('active'))
				{
					$(this).removeClass('active');
					$('#container--dropdown').slideUp(500);
					$('#container--dropdown .dropdown-menu--header:visible').hide(0);
				} else {
					$(this).mouseenter();
				}
				return false;
			});
			// dropdown menu
			$('#menu--header .dropdown-trigger').mouseenter(function()
			{
				if($('#container--dropdown .dropdown-menu--header:animated, #container--dropdown:animated').length > 0)
					return false;
				var $link = $(this);
				var $container = $('#container--dropdown');
				var fadeDuration = 0;
				if($(this).hasClass('active')) {
					return false;
				} else if(!$container.is(':visible')) {
					// ouvrir le menu
					$(this).addClass('active');
					$container.slideDown(500, function() { showSsMenu($link, fadeDuration); });
				} else {
					$('#menu--header .dropdown-trigger.active').removeClass('active');
					$(this).addClass('active');
					$('#container--dropdown .dropdown-menu--header:visible').fadeOut(fadeDuration, function() {showSsMenu($link, fadeDuration);});
				}
				return false;
			});
			// cacher le menu lorsuq'on sort du header
			$('#header').mouseover(function() {
				var closeTimeout = $(this).data('closeTimeout');
				if (typeof(closeTimeout) == 'number') {
					$(this).data('closeTimeout', null);
					window.clearInterval(closeTimeout);
				}
			});
			$('#header').mouseleave(function() {
				if ($('#menu--header .dropdown-trigger.active').length) {
					var closeTimeout = window.setTimeout(function() {
						$("#header").data('closeTimeout', null);
						$('#container--dropdown').slideUp(500);
						$('#container--dropdown .dropdown-menu--header:visible').hide(0);
						$('#menu--header .dropdown-trigger').removeClass("active");
					}, 800);
					$(this).data('closeTimeout', closeTimeout);
				}
			});
			
			function showSsMenu($link, duration) {
				var $ssmenu = $($link[0].hash);
				$ssmenu.fadeIn(duration);
			}
			
			// toggle esp-client
			$('#smenu--header .smenu--espace-client').hide();
			$('#smenu--header .trigger-toggle').click(function(){
				$smenu = $('#smenu--header .smenu--espace-client');
				if (!$smenu.length) return true;
				$smenu.stop(true, true).slideToggle();
				return false;
			});
			
			// le formulaire de connexion
			$("#login-form--header").submit(function() {
				// on v�rifie les 2 champs
				var ok = true;
				if (!validateEmail(this.email)) {
					$(this.email).addClass("error");
					ok = false;
				} else {
					$(this.email).removeClass("error");
				}
				if (!this.pwd.value.length) {
					$(this.pwd).addClass("error");
					ok =false;
				} else {
					$(this.pwd).removeClass("error");
				}
				if (!ok) return false;
				// soumettre le formulaire en AJEX
				var frm = this;
				var url = this.action;
				$.post(url.replace('client/index.html', 'login.php?action=login').replace(/^https?\:/, window.location.protocol), $(this).serialize(), function (xml) { 
					if (xml) {
						$xml = $(xml);
						if ($xml.find("error").length) {
							alert($xml.find("error").text());
						} else {
							// remplir la zone de connexion
							$auth = $("#customer_connected");
							$auth.find("span.prenom").text($xml.find("prenom").text());
							$auth.find("span.nom").text($xml.find("nom").text());
							$auth.find("a.logout").attr("title", "Vous �tes " + $xml.find("email").text());
							// montrer la connexion et cacher le formulaire
							$auth.show();
							$(frm).hide();
						}
					}
				});
				return false;
			});
			
			// les publicit�s
			if ($.fn.slides)
			{
				$('#slides').slides({
					preload: false,
					preloadImage: '../img/ajax-loader.gif',
					play: 3000,
					effect: 'fade',
					paginationLinkText: 'title'
				});
			}
		});
	}
	// ]]>
	</script>
<? if (USE_ABTASTY) : /* ABTesting : https://app.abtasty.com/ */ ?>
	<script type="text/javascript" src="//try.abtasty.com/a1b0a7dd1812a39ae90024dcf53a9ae9.js"></script>
<? endif; ?>
<? if (USE_WEB2ROI) : /* Web2ROI : http://rnd.web2roi.com/ */ ?>
	<script type="text/javascript">
	/* <![CDATA[  */ 
	(function() {
		window.sy = {
			uid : "adafr"
			,usid : ""
		};
		var w2r = document.createElement('script'); w2r.type = 'text/javascript'; w2r.async = true;
		w2r.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 't.sytsem.com/v2/tracker.js';
		var sw2r = document.getElementsByTagName('script')[0]; sw2r.parentNode.insertBefore(w2r, sw2r);
	})();
	/* ]]> */
	</script>
<? endif; ?>
</head>
<body class="<?=dirname($this->getId()).' '.basename($this->getId());?>">
	<div id="fb-root"></div>
	<div id="header">
		<div class="inner--header">
			<!-- menu principal -->
			<ul class="menu--header clearfix" id="menu--header">
				<li class="container--logo">
					<div class="inner--container--logo">
						<? if ($page == 'reservation/index') echo '<h1>'; ?>
							<a href="<?=$here?>" class="logo"><img src="<?=$baseurl?>css/img/logo-ada.png" alt="location de voiture et utilitaire - ADA.fr" /></a>
						<? if ($page == 'reservation/index') echo '</h1>'; ?>
					</div>
				</li>
				<li><a href="<?=$here?>agences/index.html" class="trouver-agence">Agence</a></li>
				<? if (false): ?>
				<li><a href="<?=$here?>location-voiture-promo.html" class="item2">Promotions</a></li>
				<? endif; ?>
				<li><a href="<?=$here?>vehicules/index.html#dropdown-vehicules" class="dropdown-trigger" rel="nofollow">V�hicules</a></li>
				<li><a href="<?=$here?>offre/index.html#dropdown-offres" class="dropdown-trigger"  rel="nofollow">Offres</a></li>
				<li><a href="<?=$here?>demenagement/index.html#dropdown-demenagement" class="dropdown-trigger"  rel="nofollow">D�m�nagement</a></li>
				<li><a href="<?=$here?>pro/index.html#dropdown-esp-pro" class="dropdown-trigger" rel="nofollow">Professionnels</a></li>
				<li class="last"><a href="<?=$here;?>faq/index.html">Questions</a></li>
			</ul>
			<!-- sous-menu franchis� & espace client -->
			<ul class="smenu--header" id="smenu--header">
				<li><a href="http://franchise.ada.fr" target="_blank" class="picto--franchise">Devenez franchis�</a></li><!--
				--><li><a href="<?=Page::getURL('client/index.html', null, USE_HTTPS);?>" class="picto--esp-client trigger-toggle">Espace client</a>
				<? if ($page != 'client/index' && $page != 'autoeurope/index' && basename($page) != 'inscription') : ?>
					<ul class="smenu--espace-client">
						<li>
							<? $client = $this->getConnectedClient(); ?>
							<!-- si connect� -->
							<div class="type--table connected--header" id="customer_connected" <? if (!$client) echo ' style="display: none;"';?>>
								<div class="type--table-cell">
									Bienvenue <span class="prenom"><?=$client['prenom'];?></span> <span class="uppercase nom"><?=$client['nom']?></span>
								</div>
								<div class="type--table-cell">
									<a rel="noindex,nofollow" href="<?=Page::getURL('client/index.html', null, USE_HTTPS);?>" class="espace_client type--button">Mon compte</a>
									<a rel="noindex,nofollow" href="<?=Page::getURL('?logout');?>" class="logout" title="Vous �tes <?=$client['email'];?>">D�connexion</a>
								</div>
							</div>
							<? if (!$client) :?>	
							<!-- si pas connect� -->
							<form action="<?=Page::getURL('client/index.html', null, USE_HTTPS);?>" method="post" id="login-form--header">
								<input type="hidden" name="id" value="0"/>
								<input type="hidden" name="key" value="<?=Page::getKey(0)?>"/>
								<div class="type--table">
									<label for="login-email" class="type--table-cell">
										<span class="intitule">E-mail</span>
										<input type="text" id="login-email" name="email" placeholder="E-mail" />
									</label>
									<label for="login-mdp" class="type--table-cell">
										<span class="intitule">Mot de passe</span>
										<input type="password" id="login-mdp" name="pwd" placeholder="Mot de passe" />
									</label>
									<div class="type--table-cell">
										<button type="submit">OK</button>
									</div>
								</div>
								<a href="<?=Page::getURL('client/motdepasse.html', null, USE_HTTPS)?>">mot de passe oubli� ?</a>
							</form>
							<? endif; ?>
						</li>
					</ul>
				<? endif; ?>
				</li>
			</ul>
			<div class="container--dropdown-menu--header" id="container--dropdown">
				<div id="dropdown-vehicules" class="dropdown-menu--header">
					<ul>
						<li>
							<a href="<?=$here?>vehicules/tourisme.html"><img src="<?=$baseurl?>css/img/structure-2014/dropdown--vp.png" alt="" /><br />voiture de tourisme</a>
						</li>
						<li>
							<a href="<?=$here?>vehicules/utilitaires.html"><img src="<?=$baseurl?>css/img/structure-2014/dropdown--vu.png" alt="" /><br />v�hicules utilitaires</a>
						</li>
						<li>
							<a href="<?=$here?>vehicules/sans_permis.html"><img src="<?=$baseurl?>css/img/structure-2014/dropdown--sp.png" alt="" /><br />v�hicules sans permis</a>
						</li>
						<li>
							<a href="<?=$here?>vehicules/deux_roues.html"><img src="<?=$baseurl?>css/img/structure-2014/dropdown--dr.png" alt="" /><br />deux roues</a>
						</li>
					</ul>
				</div>
				<div id="dropdown-offres" class="dropdown-menu--header">
					<ul>
						<li>
							<a href="<?=$here?>jeunes/index.html"><img src="<?=$baseurl?>css/img/structure-2014/dropdown--jeunes.png" alt="" /><br />Ada jeunes</a>
						</li>
						<li>
							<a href="<?=$here?>offre/service_livraison.html"><img src="<?=$baseurl?>css/img/structure-2014/dropdown--livraison.png" alt="" /><br />Ada service livraison</a>
						</li>
					</ul>
				</div>
				<div id="dropdown-demenagement" class="dropdown-menu--header">
					<ul>
						<li>
							<a href="<?=$here?>demenagement/trouver-son-camion.html"><img src="<?=$baseurl?>css/img/structure-2014/dropdown--choix-utilitaire.png" alt="" /><br />choisir un utilitaire</a>
						</li>
						<li>
							<a href="<?=$here?>demenagement/materiel.html"><img src="<?=$baseurl?>css/img/structure-2014/dropdown--materiel.png" alt="" /><br />pr�voir son mat�riel</a>
						</li>
						<li>
							<a href="<?=$here?>demenagement/memo.html"><img src="<?=$baseurl?>css/img/structure-2014/dropdown--organisation.png" alt="" /><br />organiser son d�m�nagement</a>
						</li>
						<li>
							<a href="<?=$here?>demenagement/homebox.html"><img src="<?=$baseurl?>css/img/structure-2014/dropdown--garde-meuble.png" alt="" /><br />louer un garde meuble</a>
						</li>
					</ul>
				</div>
				<div id="dropdown-esp-pro" class="dropdown-menu--header">
					<ul>
						<li>
							<a href="<?=$here?>pro/ada_ce.html"><img src="<?=$baseurl?>css/img/structure-2014/dropdown--ce.png" alt="" /><br />CE</a>
						</li>
						<li>
							<a href="<?=$here?>pro/index.html"><img src="<?=$baseurl?>css/img/structure-2014/dropdown--entreprise.png" alt="" /><br />Entreprise</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	
	<div id="content">
	<? if ($page=="reservation/index"): ?>
		<div id="slides">	
			<div class="slides_container">
		<? foreach ( VisuelPromo_Collection::factory() as $row ): ?>
			<div class="slide" title="<?= $row['commentaire'] ?>" data-time="<?= $row['time'] ?>">
				<?= $row->showVisuel($this->getBaseURL(false)); ?>
			</div>
		<? endforeach; ?>
			</div>
		</div>
	<? endif; //($page=="reservation/index") ?>
	
	
	<?
		// bloc pour administrer le contenu
		$this->writeEditAdmin();
		// maintenance et t�l�-conseiller
		$this->writeInfo();
		// inclure la page courante
		include($this->getSkinnedPagePath());
	?>
	</div>

	<div class="content_bottom">
		<? if ($this->hasLinkTop() && !count($_POST) && count($_GET) < 2 && isset($_GET['page'])) : ?>
		<a href="<?=$this->getAction();?>.html#header" onclick="window.location.hash='header'; return false;" class="btn right haut">Haut de page<span class="end"></span></a>
		<? endif; ?>
	</div>

	<div class="footer">
		<div class="footer-sup">
			<div class="inner--footer-sup">
				<ul class="table">
					<li>
						<a href="http://www.groupeg7.com" target="_blank"><img src="<?=$baseurl?>css/img/structure-2014/logo-g7.png" alt="" class="left type-img-footer" /> Ada est une soci�t�<br />du Groupe G7</a>
					</li>
					<li>
						<a href="<?=$here?>societe/index.html" title="Qui sommes nous ?">Qui<br />sommes-nous ?</a>
					</li>
					<li>
						<a href="<?=$here?>societe/reseau.html" title="Devenir franchis�">Devenir<br /> franchis�</a>
					</li>
					<li>
						<a href="<?=$here?>contact/index.html" title="Nous contacter">Nous<br />contacter</a>
					</li>
					<li>
						<a href="<?=$here?>conditions/internet.html" title="Conditions g�n�rales">Conditions<br /> g�n�rales</a>
					</li>
					<li>
						<a href="<?=$here?>conditions/assurance_complementaire.html" title="Conditions assurance compl�mentaire">Conditions assurance<br />compl�mentaire</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="footer-sub">
			<div class="inner--footer-sub">
				<div class="table">
					<div class="col-1-5">
						<ul>
							<li><a href="<?=$here?>agences/index.html">Agences de location de voiture</a></li>
							<? if (false): ?>
							<li><a href="<?=$here?>location-voiture-promo.html">Promotions location voiture</a></li>
							<? endif; ?>
							<li><a href="<?=$here?>jeunes/index.html">Location jeune conducteur</a></li>
							<li class="type--mode-paiement">
								Moyens de paiements accept�s<br />
								<img src="<?=$baseurl?>css/img/structure-2014/mode-paiement.png" alt="" />
							</li>
						</ul>
					</div>
					<div class="col-1-5">
						<ul>
							<li>Loisirs
								<ul>
									<li><a href="<?=$here?>vehicules/tourisme.html">Location de voiture de tourisme</a></li>
									<li><a href="<?=$here?>offre/forfaits.html">Forfaits</a></li>
									<li><a href="<?=$here?>vehicules/sans_permis.html">Location voiture sans permis</a></li>
									<li><a href="<?=$here?>vehicules/deux_roues.html">Location deux roues</a></li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="col-1-5">
						<ul>
							<li><a href="<?=$here?>demenagement/index.html">D�m�nagement</a>
								<ul>
									<li><a href="<?=$here?>demenagement/trouver-son-camion.html">Choisir son utilitaire</a></li>
									<li><a href="<?=$here?>demenagement/materiel.html">Pr�voir son mat�riel de d�m�nagement</a></li>
									<li><a href="<?=$here?>demenagement/memo.html">Organiser son d�m�nagement</a></li>
									<li><a href="<?=$here?>demenagement/homebox.html">Louer un garde meuble</a></li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="col-1-5">
						<ul>
							<li>Professionnels
								<ul>
									<li><a href="<?=$here?>pro/ada_ce.html">Comit�s d'Entreprises</a></li>
									<li><a href="<?=$here?>pro/index.html">Entreprises</a></li>
								</ul>
							</li>
							<li>Utilitaires
								<ul>
									<li><a href="<?=$here?>vehicules/utilitaires.html">V�hicules utilitaires</a></li>
									<li><a href="<?=$here?>offre/forfaits.html">Forfaits</a></li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="col-1-5">
						<ul>
							<li>Vos services
								<ul>
									<li><a href="<?=$here?>aide/index.html">Aide en ligne</a></li>
									<li><a href="<?=Page::getURL('client/index.html', null, USE_HTTPS);?>">Mon espace client</a></li>
									<li><a href="<?=$here?>newsletter/index.html">Newsletter</a></li>
									<li><a href="<?=$here?>societe/mentions_legales.html">Mentions l�gales</a></li>
									<li><a href="<?=$here?>juridique/regles_communiques.html">Informations r�glement�es</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="content--baseline">
		<?
			if (!$this->isNoIndex() && $this->hasData('footer')) {
				// r�cup�rer le texte de la baseline en corrigeant les liens
				if ($baseline = $this->getData('footer')) {
					$baseline = preg_replace("/href=\"(?!http)/" , 'href="'.$here, $baseline);
					echo '<ul class="baseline">'."\n";
					echo '<li>'.join("</li>\n<li>", explode("\n", $baseline))."</li>\n";
					echo '</ul>'."\n";
				}
			}
			if (!$this->isNoIndex() && $this->hasData('baseline')) {
				echo $this->getData('baseline');
			}
		?>
	</div>
	
	
<?
	// informations de mise au point
	$this->writeDebugInfo();
	$this->writeJSStats();
?>
</body>
</html>