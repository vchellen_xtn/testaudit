<?
/**
* Conditions d'annulation ADA Malin (� l'heure)
* - pas d'annulation
*/
class Reservation_ConditionsAnnulationMalin extends Reservation_ConditionsAnnulation
{
	public function isCancellable($preavis, &$msg)
	{
		$msg = "L'annulation n'est pas possible pour la location � l'heure";
	}
	public function getCancellationFees($preavis)
	{
		return 0;
	}
	public function getConditions()
	{
		$str = <<<EOT
		<p style="margin-bottom:10px;">
			Vous n'avez pas la possibilit� d'annuler votre r�servation.
		</p>
EOT;
		return $str;
	}
	public function getInfos()
	{
		$str = <<<EINFO
		<p>Vous n'avez pas la possibilit� d'annuler votre r�servation.</p>
EINFO;
		return $str;
	}
}
?>
