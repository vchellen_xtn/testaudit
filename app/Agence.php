<?

class Agence extends Ada_Object
{
	const GOOGLEPLUS_ACCOUNT_ADA = '114111659841944050801';
	const GOOGLEPLUS_URL = 'https://plus.google.com/%s';
	const NUMERO_COURT = '<span class="numero-court-tel">3659</span> <span class="numero-court-tarif">(0,12�/min)</span>';

	// les statuts
	const STATUT_VISIBLE	= 1;
	const STATUT_RESAWEB	= 2;
	const STATUT_ABANDONNISTES = 4; // pr�venir l'agence que des clients ont abandonn� une r�servation
	const STATUT_STOP_PREMIER_PRIX = 8; // Stop ADA Premier Prix
	const STATUT_JEUNES = 16; // ouverte � la r�servation pour ADA JEUNES
	const STATUT_LIVRAISON_SEULEMENT = 32;
	const STATUT_ASSURANCES = 64;	// proposer les assurances, lien avec option.agence_statut
	const STATUT_TEL_CLIENT = 128; // fournir le num�ro de t�l�phone du client � l'agence
	const STATUT_STOP_NEGO = 256; // ne pas autoriser les n�gociations pour cette agence

	// les services
	const SERVICE_LIVRAISON     = 'livraison';
	const SERVICE_J7            = 'j7';
	const SERVICE_MALIN         = 'malin';
	const SERVICE_HOLIDAY_BIKES = 'holiday_bikes';

	static public $STATUTS = array
	(
		self::STATUT_VISIBLE => 'Visible',
		self::STATUT_RESAWEB => 'R�s. en ligne',
		self::STATUT_ABANDONNISTES => 'Abandonnistes',
		self::STATUT_STOP_PREMIER_PRIX => 'Stop Premier Prix',
		self::STATUT_JEUNES => 'Jeunes',
		self::STATUT_LIVRAISON_SEULEMENT => 'Livraison uniquement',
		self::STATUT_ASSURANCES => 'Assurances',
		self::STATUT_TEL_CLIENT => 'T�l. Client',
		self::STATUT_STOP_NEGO => 'Stop N�gociation'
	);
	// Type
	const TYPE_VP = 1; // voiture
	const TYPE_VU = 2; // utilitaire
	const TYPE_2R = 4; // deux roues
	const TYPE_SP = 8; // sans permis
	const TYPE_ML =16; // malin : � l'heure
	const TYPE_AS = 32; // aller simple
	const TYPE_PS = 64; // prestige

	static public $VEHICULES_TYPE = array(
		'vp' => self::TYPE_VP,
		'vu' => self::TYPE_VU,
		'2r' => self::TYPE_2R,
		'sp' => self::TYPE_SP,
		'ml' => self::TYPE_ML,
		'as' => self::TYPE_AS,
		'ps' => self::TYPE_PS,
	);
	static public $VEHICULES_TYPE_LBL = array(
		'vp' => 'voiture',
		'vu' => 'utilitaire',
		'2r' => 'deux roues',
		'sp' => 'sans permis',
		'ml' => "malin : � l'heure",
		'as' => 'aller simple',
		'ps' => 'prestige',
	);


	const TXT_BAL = "Cette agence vous permet de b�n�ficier d'un service boite aux lettres. Vous pouvez ainsi rendre votre v�hicule en dehors des heures d'ouverture de l'agence. Nous vous invitons � signaler que vous souhaitez b�n�ficier de cette option lors de la prise du v�hicule.";

	/** @var Zone */
	protected $_zone;
	/** @var array */
	protected $_fermetures;
	/** @var array */
	protected $_tarifsWeb = array();
	/** les r�gles de yield pour cette agence */
	protected $_yields = array();

	/** @var PromotionForfait_Collection */
	protected $_promotions;
	/** @var AgenceBonPlan_Collection */
	protected $_bonsplans;

	// Emplacement
	const EMPL_VILLE	= 1;
	const EMPL_GARE		= 2;
	const EMPL_AERO		= 3;
	const EMPL_AUTRE	= 0;

	static public $RESEAU = array('ADA','CITER');
	static public $EMPLACEMENT = array(self::EMPL_VILLE => 'Ville', self::EMPL_GARE => 'Gare', self::EMPL_AERO => 'A�roport', self::EMPL_AUTRE => 'Autre');
	static public $EMPLACEMENT_LIVRAISON = array(self::EMPL_VILLE => 'en ville', self::EMPL_GARE => 'en gare', self::EMPL_AERO => "� l'a�roport", self::EMPL_AUTRE => 'en ville');
	static public $SURCHARGE = array(self::EMPL_VILLE => 'Forfait de stationnement', self::EMPL_GARE => 'Forfait livraison Gare / A�roport', self::EMPL_AERO => 'Forfait livraison Gare / A�roport', self::EMPL_AUTRE => 'Forfait de stationnement');

	/**
	* V�rifie si un identifiant d'agence est correct
	*
	* @param string $id identifiant d'agence
	* @returns bool
	*/
	static public function isValidID($id)
	{
		return (strlen($id)==6 && preg_match('/^(C|FR|FR[A-Z])\d+[A-Z]$/i', $id));
	}


	/**
	* Cr�e une agence � partir d'un identifiant
	*
	* @param string $id
	* @param bool $hidePartner - n'affiche pas les coordonn�es du partenaire
	* @param string $cols la liste des colonnes
	* @param int statut de l'agence
	* @return Agence
	*/
	static public function factory($id, $hidePartner = false, $cols = '*', $statut = self::STATUT_VISIBLE)
	{
		$x = new Agence();
		if (!self::isValidID($id))
			return $x;
		if ($x->load($id, $cols, $statut)->isEmpty())
			return null;
		if ($hidePartner)
			$x->hidePartner();
		return $x;
	}
	/**
	 * V�rifie si un identifiant d'agence correspond � un point LOC
	 * L'identifiant d'une agence pointLoc se termine par un P majuscule
	 * On test le dernier caract�re du code agence pour renvoyer true si le dernier charact�re est 'P'
	 * false sinon
	 * Attention! Les alias �ventu�elllement cr��s auraient un "id" se terminant par Q, R, S...
	 * 
	 * 
	 * @returns bool
	 */
	public function isPointLoc()
	{
		if (substr($this->getData('agence'), -1) == 'P') {
			return true;
		} else {
			return false;
	    }
	}

	/**
	* Renvoie le SQL pour une agence avec toutes les informations
	* @param string $id
	* @param string $cols
	* @param int $data : statut de l'agence recherch�e
	*/
	protected function _prepareSQL($id, $cols='*', $data = null)
	{
		if ($cols == '*')
			$cols = "a.*, ap.*, aa.id, aa.pdv, aa.agence, aa.nom, ap.nom nom_pdv, aa.canon, aa.dept, aa.recherche, aa.agglo, aa.emplacement, aa.pageweb, aa.refnat, aa.footer, NULLIF(TRIM(CONCAT(IFNULL(ap.commentaire_localisation,''),' ',IFNULL(ap.commentaire_offre,''))),'') commentaire";
		$sql = " SELECT $cols\n";
		$sql.= " FROM agence_alias aa\n";
		$sql.= " JOIN agence_pdv ap ON ap.id=aa.pdv\n";
		$sql.= " JOIN agence a ON a.id=aa.agence\n";
		$sql.= " WHERE aa.id = '".addslashes($id)."'";
		if ($data)
			$sql.= "AND (a.statut & ".((int)$data).")=".((int)$data);

		return $sql;
	}

	public function __toString()
	{
		return $this->getData('nom');
	}

	/**
	* V�rifie le(s) statut(s) pass�s
	* @param int $statut
	* @returns bool
	*/
	public function hasStatut($statut)
	{
		return (($this->_data['statut'] & $statut) == $statut);
	}
	/**
	* L'agence propose-t-elle ce type de v�hicule ?
	*
	* @param string $type
	* @return bool
	*/
	public function hasVehiculeType($type) {
		return (($this->_data['type'] & self::$VEHICULES_TYPE[$type]) != 0);
	}
	/**
	* Renvoie une AgenceAgglo coorespondant � la ville
	* @returns AgenceAgglo
	*/
	public function getAgglo()
	{
		if ($this->_data['zone']=='fr')
		{
			$agglo = ($this->_data['agglo'] ? $this->_data['agglo'] : getsql(sprintf("select ville from agence_alias aa where aa.id='%s'", $this->getId())));
			return AgenceAgglo::factory($agglo);
		}
		return null;
	}
	/**
	* Renvoie un d�partement associ�e
	* @return Dept
	*/
	public function getDept()
	{
		$zone = strtolower($this->_data['zone']);
		if ($zone=='fr' || $zone=='co')
			return Dept::factory($this->_data['dept']);
	}
	/**
	* Retourne les agences autour de celle ci
	*
	* @param int $limit nombre d'agences � rechercher
	* @param int statut filtre sur le statut des agences
	* @param int distance � quelle distance rechercher les agences
	* @return Agence_Collection
	*/
	public function getAgencesAround($limit, $statut = null, $distance = null)
	{
		return Agence_Collection::factory(array('agence'=>$this->getData('pdv'),'limit'=>$limit, 'statut'=>$statut, 'distance' => $distance), Agence_Collection::MODE_AGENCES_LISTE, false);
	}

	/**
	* Renvoie les horaires, un tableau de tableau
	* (jour, ouverture, fermeture, pause_debut, pause_fin)
	*/
	public function getHoraires($index = null)
	{
		if (!$this->hasData('horaires'))
		{
			$agence = $this->getData('pdv');
			if (!$agence) $agence = $this->getData('agence');
			if (!$agence) $agence = $this->getData('id');

			// req�te pour les horaires
			$sql = "SELECT jour, date_format(ouverture, '%H:%i') ouverture, date_format(fermeture, '%H:%i') fermeture, date_format(pause_debut, '%H:%i') pause_debut, date_format(pause_fin, '%H:%i') pause_fin";
			$sql.= "  FROM agence_horaire ";
			$sql.= " WHERE agence = '".$agence."'";
			$sql.= " ORDER BY jour";

			// remplir le tableau
			$horaires = new AgenceHoraire_Collection();
			for($rs=sqlexec($sql), $row = mysql_fetch_assoc($rs), $i=1; $i<=7; $i++)
			{
				if ($row && $row['jour'] == $i)
				{
					$horaires->add(new AgenceHoraire($row));
					$row = mysql_fetch_assoc($rs);
				}
				else
					$horaires->add(new AgenceHoraire(array('jour'=>$i)));
			}
			$this->setData('horaires', $horaires);
		}
		$x = $this->getData('horaires');
		return ($index ? $x->getItem($index) : $x);
	}
	/**
	* Renvoie un tableau avec la liste des fermetures
	* @returns array
	*/
	public function getFermetures()
	{
		if (!is_array($this->_fermetures))
		{
			$this->_fermetures = array();
			// charger les fermetures
			$sql = "select debut, fin";
			$sql.=" from agence_fermeture f";
			$sql.=" left join agence_ouverture o on (o.agence='".$this->_data['agence']."' and o.fermeture=f.id)";
			$sql.= " where f.zone='".$this->_data['zone']."' and f.agence in ('%','".$this->_data['agence']."') and f.fin >= '".date('Y-m-d')."'";
			$sql.= " and o.id is null";
			$sql.= " order by debut";

			for ($rs=sqlexec($sql); $a=mysql_fetch_assoc($rs); )
			{
				if ($a['debut'] == $a['fin'])
					$this->_fermetures[$a['debut']] = true;
				else
				{
					for ($t=strtotime($a['debut']); $t <= strtotime($a['fin']); $t += 24 * 3600)
						$this->_fermetures[date('Y-m-d', $t)] = true;
				}
			}
			// on conserve dans $this->_data pour l'utiilser dans l'export JSON
			$this->setData('fermetures', array_keys($this->_fermetures));
		}
		return $this->getData('fermetures');
	}
	/**
	* L'agence est-elle ferm�e ce jour-l� ?
	*
	* @param string $date YYYY-MM-DD
	* @param int $dayOfWeek 1:lundi ... 7: dimanche
	* @param int $hour - optionnel heure pour laquelle il faut v�rifier l'ouverture de l'agence
	* @returns bool
	*/
	public function isClosed($date, $dayOfWeek, $hour = null)
	{
		// on charge les dates de fermeture si n�cessaire
		if (!is_array($this->_fermetures)) {
			$this->getFermetures();
		}
		// on v�rifie dans les dates de fermeture
		if ($this->_fermetures[$date])
			return true;
		// sinon on regarde les horaires d'apr�s la date du jour
		if (/** @var AgenceHoraire */ $horaires = $this->getHoraires($dayOfWeek))
		{
			if ($horaires->isClosed($hour))
				return true;
		}
		return false;
	}

	/**
	* V�rifie la validit� d'une date / time et cherche une autre possibilit�
	*
	* @param int $t	timestamp
	* @param string $sens null, depart ou retour
	* @returns int	un timestamp v�rifi� ou modifi�, sinon null si aucune solution
	*/
	public function adjustDateTime($t, $sens = null, $duree = null)
	{
		$dayOfWeek = date('N', $t);
		// si l'agence est ferm�e
		if ($this->isClosed(date('Y-m-d', $t), $dayOfWeek, date('G', $t)))
		{
			// on essaie le jour d'avant ou d'ap�s
			if ($sens == 'depart' && $duree == 1)
			{
				$offset = -1;
				$h = '23:30';
			}
			else if ($sens=='retour')
			{
				$offset = 1;
				$h = '00:30';
			}
			else
				return null;
			$t = strtotime($offset.' day', $t);
			return $this->adjustDateTime(strtotime(date('Y-m-d', $t).' '.$h));
		}
		// sinon on regarde les horaires d'apr�s la date du jour
		if (/** @var AgenceHoraire */ $horaires = $this->getHoraires($dayOfWeek))
		{
			// on v�rifie si l'heure est possible et on essaye de l'adapter
			$h = date('H:i', $t);
			$hAdjust = $horaires->adjustTime($h, $sens);
			if (!$hAdjust) return null;
			if ($hAdjust != $h)
				$t = strtotime(date('Y-m-d', $t).' '.$hAdjust);
			return $t;
		}
		return null;
	}

	/**
	* Renvoie l'URL de r�servation pour l'agence
	* @returns string
	*/
	public function getURLAgence()
	{
		if ($this->_checkCanon())
			return Page::getURL(gu_agence($this->_data));
	}
	/**
	* Renvoie les promotions associ�es � une agence
	* @returns PromotionForfait_Collection
	*/
	public function getPromotions()
	{
		if (!$this->_promotions) {
			$this->_promotions = PromotionForfait_Collection::factory(array('zone' => $this->getData('zone'), 'agence'=>$this->getId()), false, true);
		}
		return $this->_promotions;
	}
	/**
	* Renvoie les bons plans associ�es � cette agence
	* @returns AgenceBonPlan_Collection
	*/
	public function getBonsPlans() {
		if (!$this->_bonsplans) {
			$this->_bonsplans = AgenceBonPlan_Collection::factory($this);
		}
		return $this->_bonsplans;
	}

	public function getURLResa($type = null)
	{
		if ($this->_checkCanon())
			return Page::getURL(gu_recherche($this->_data, $type));
	}
	public function getURLPromo($type = null)
	{
		if ($this->_checkCanon())
			return Page::getURL(gu_promo($this->_data, $type));
	}
	protected function _checkCanon()
	{
		if (!$this->hasData('canon'))
		{
			if ($this->hasData('id') && $canon = getsql("select canon from agence_alias where id='{$this->getId()}'"))
			{
				$this->setData('canon', $canon);
				return true;
			}
			return false;
		}
		return true;
	}

	/**
	* renvoie le texte pour l'option bo�te aux lettres
	*/
	public function getBal ($elt = null, $attrs = null, $suffix=null)
	{
		if (!$this->getData('bal')) return;
		if ($elt) $s = "<$elt $attrs>";
		$s .= self::TXT_BAL;
		if ($elt) $s .= "</$elt>";
		return $s.$suffix;
	}
	/**
	* Dans certains cas on ne veut pas montrer les coordonn�es des agences partenaires
	* @return Agence
	*/
	public function hidePartner()
	{
		if ($this->getData('reseau')!='ADA')
		{
			foreach(array('adresse1','adresse2','tel','fax') as $k)
				$this->setData('o_'.$k, $this->getData($k))->setData($k, null);
			$this->setData('adresse1', 'AGENCE PARTENAIRE Relais ADA');
			$this->setData('commentaire', 'Les coordonn�es de notre partenaire vous sont communiqu�es une fois votre r�servation valid�e');
		}
		return $this;
	}
	public function getNumeroCourt() {
		if (in_array($this->_data['zone'], array('fr','co')) && !$this->isPointLoc()) {
			return self::NUMERO_COURT.'<br/>'.str_repeat('&nbsp;', 8);
		}
		return '';
	}
	/**
	* Renvoie une adresse en HTML
	*
	* @param string $sep s�parateur entre les �l�ments de l'adresse
	* @param mixed $with champs � mettre dans l'adresse, sinon valeur par d�faut
	* @param mixed $except champs � ne pas mettre (adresse1, adresse2, cp_ville, tel, fax)
	* @returns string
	*/
	public function getHtmlAddress($sep = '<br/>', $with = array(), $except = array())
	{
		// champs � prendre en compte
		if ($with && !is_array($with))
			$with = explode(',', $with);
		if ($except && !is_array($except))
			$except = explode(',', $except);
		if (!$with || !is_array($with))
			$with = array('adresse1','adresse2','cp_ville','tel');// ,'fax');

		// cr�er cp_ville si n'existe pas
		if (!$this->hasData('cp_ville'))
			$this->setData('cp_ville', $this->getData('cp').' '.$this->getData('ville'));
		$prefix = array('tel'=>'<span class="label-tel">T�l: </span>', 'fax' => '<span class="label-tel">Fax: </span>');
		foreach ($with as $k)
		{
			if (in_array($k, $except)) continue;
			if ($v = $this->getData($k)) {
				if ($k == 'tel') {
					$v = $this->getNumeroCourt().'<span class="label-tel">'.$v.'</span>';
				}
				$html[] = $prefix[$k].$v;
			}
		}
		return is_array($html) ? join($sep, $html) : '';
	}
	/**
	* Message pour l-e-mail de validation
	*
	*/
	public function getHtmlMsgPartner($before = '', $after = '')
	{
		if ($this->getData('reseau')=='CITER')
			$x = "ATTENTION : le retrait du v�hicule se fera aupr�s de l'agence ENTERPRISE, partenaire de ADA (coordonn�es ci-dessous).";
		if ($x)
			$x = $before.htmlentities($x).$after;
		return $x;
	}
	/**
	* Le lien vers les conditions g�n�rales de location
	*/
	public function getHrefCGL()
	{
		return 'conditions/location.html#rubrique9';
	}
	/* Que comprend les tarifs pour cette agence  ? */
	public function has2ndDriver() { return ($this->getData('reseau') == 'ADA'); }
	public function getHtmlRatesIncludes()
	{
		$reseau = $this->getData('reseau');
		$a = array();
		$a[] = '- Les kilom�tres inclus dans le forfait';
		//$a[] = '- La garantie dommage au v�hicule (CDW) avec <a href="../conditions/location.html#rubrique12" target="_blank">franchise r�duite</a>';
		$a[] = '- La garantie en cas de vol du v�hicule avec <a href="../conditions/location.html#rubrique12" target="_blank">franchise r�duite</a>';
		$a[] = '- <a href="../conditions/location.html#rubrique5" target="_blank">Une assistance</a> 24h/24, 7 jours/7';
		if ($reseau == 'ADA')
			$a[] = "- L'assurance conducteur (PAI).";
		return '<li>'.join("</li>\n<li>", $a)."</li>\n";
	}
	public function getHtmlRatesExcludes($km_supp = true, $more = true)
	{
		$reseau = $this->getData('reseau');
		$a = array();
		if ($reseau == 'CITER')
		{
			$a[] = '- Les frais pour conducteur suppl�mentaire';
			$a[] = '- Les frais pour jeune conducteur';
			$a[] = "- L'assurance conducteur (PAI)";
		}
		$a[] = '- Les frais de carburants';
		if ($km_supp)
			$a[] = '- Les kilom�tres suppl�mentaires';
		$a[] = '- Les journ�es suppl�mentaires de location';
		$a[] = '- Les options demand�es lors de votre r�servation en ligne';
		if ($more)
			$a[] = '<a href="../conditions/location.html#rubrique10" target="_blank">Plus de d�tails</a>';
		return '<li>'.join("</li>\n<li>", $a)."</li>\n";
	}
	// DELAIS ET SURCHARGE
	/**
	* Renvoie la zone assoic�e � l'agence
	* @returns Zone
	*/
	public function getZone()
	{
		if (!$this->_zone)
			$this->_zone = Zone::factory($this->_data['zone']);
		return $this->_zone;
	}
	/**
	* Peut-on r�server dans cette agence
	* @returns bool
	*/
	public function isBookableOnline()
	{
		$zone = $this->getZone();
		return ($zone['reservation'] /* && (($this->getData('statut') & self::STATUT_RESAWEB)==self::STATUT_RESAWEB)*/);
	}
	public function getDelaiMode()
	{
		return $this->getZone()->getData('delai_mode');
	}
	public function getDelaiMin($type)
	{
		$k = strtolower($type).'_delai_min';
		if (!($delai = $this->getData($k)))
			$delai = $this->getZone()->getData($k);
		return $delai;
	}
	public function getSurcharge($type)
	{
		return $this->_data['surcharge_'.strtolower($type)];
	}
	// renvoie le type de taxe / surcharge
	public function getSurchargeLabel()
	{
		return self::$SURCHARGE[$this->_data['emplacement']];
	}
	/**
	* Met � jour le champ livraison_km en fonction des options
	*/
	public function updateLivraisonKm()
	{
		$km = 'null';
		$rs = sqlexec(sprintf("SELECT DISTINCT o.groupe FROM agence_option ao JOIN `option` o ON o.id=ao.`option` WHERE ao.agence='%s' AND o.theme='voiturier' ORDER BY o.groupe", $this->getId()));
		while ($row = mysql_fetch_assoc($rs))
		{
			if ($row['groupe'] == 'voiturier_25')
			{
				$km = 25;
				break;
			}
			$km = 10;
		}
		sqlexec(sprintf("UPDATE agence SET livraison_km = %s WHERE id='%s'", $km, $this->getId()));
	}
	/**
	* Schema.org Functions : www.schema.org/AutoRental
	*
	*/
	public function getItemScopeType($type)
	{
		// on n'indique pas un itemscope et un itemtype pour les alias
		// sinon on aurait plusieurs agences avec la m�me adresse
		if ($this->_data['id']==$this->_data['pdv'])
			return sprintf('itemscope itemtype="http://schema.org/%s"', $type);
		return;
	}
	/**
	* On identifie les points de livraison : les points de vente qui ne sont pas des agences principales et qui sont en Gare ou en A�rport
	* @returns bool
	*/
	public function isPointDeLivraison() {
		if ($this->_data['agence']!=$this->_data['pdv']) {
			if ($empl = $this->_data['emplacement']) {
				return ($empl == self::EMPL_GARE || $empl == self::EMPL_AERO);
			}
		}
		return false;
	}
	/**
	* Renvoie le type de tarifs pour le Web selon le type de v�hicules
	*
	* @param string $type
	* @returns string ADAFR ou RESAS
	*/
	public function getTarifsWeb($type) {
		$type = strtolower($type);
		// on constitue le tableau des tarifs
		if (!count($this->_tarifsWeb)) {
			// on construire le SQL diff�remment si on a le code soci�t ou pas
			if ($this->_data['code_societe']) {
				$sql = sprintf("select c.* from agence_config c where c.agence='%s'", $this->_data['code_societe']);
			} else if ($this->_data['agence'] || $this->_data['id']) {
				$sql = sprintf("select c.* from agence_config c join agence a on a.code_societe=c.agence where a.id='%s'", ($this->_data['agence'] ? $this->_data['agence'] : $this->_data['id']));
			}
			// et maintenant on constitue le tableau
			if ($sql) {
				$row = mysql_fetch_assoc(sqlexec($sql));
				foreach(Categorie::$TYPE as $k => $v) {
					$this->_tarifsWeb[$k] = ($row['tarifs_'.$k] ? $row['tarifs_'.$k] : 'ADAFR');
				}
				$this->_tarifsWeb['promo_yield'] = (is_null($row['promo_yield']) ? 0 : $row['promo_yield']);
			}
		}
		return $this->_tarifsWeb[$type];
	}
	/**
	* Renvoie les r�gles de Yields associ�es � une agence et un type de v�hicule
	*
	* @param string $type
	* @returns AgencePromoYield_Collection
	*/
	public function getYields($type) {
		// charger les r�gles de Yield pour ce type de v�hicule
		if (!isset($this->_yields[$type])) {
			$this->_yields[$type] = AgencePromoYield_Collection::factory($this, $type);
		}
		return $this->_yields[$type];
	}
	/**
	* Renvoie la recherche associ�e � une agence
	*
	* @param string $agenceID
	* @returns string
	*/
	static public function getRechercheStringFromID($agenceID)
	{
		if ($agenceID && self::isValidID($agenceID)) {
			return getsql(sprintf('select recherche from agence_alias where id="%s"', $agenceID));
		}
	}


	/*
	** Google Plus
	*/
	static public function getADAGooglePlusURL() {
		return sprintf(self::GOOGLEPLUS_URL, self::GOOGLEPLUS_ACCOUNT_ADA);
	}
	public function getGooglePlusURL() {
		if ($googleplus = $this->_data['googleplus']) {
			return sprintf(self::GOOGLEPLUS_URL, $googleplus);
		}
		return null;
	}
	/**
	* Renvoie une note sur 4 pour l'agence
	* @returns float
	*/
	public function getAvisNote() {
		// en TEST on donne une fausse note
		if (SITE_MODE != 'PROD' && !$this->_data['satisfaction_agence']) {
			$this->_data['satisfaction_agence'] = round(rand(20, 100) / 25, 2);
			$this->_data['satisfaction_vehicule'] = round(rand(30, 100) / 25, 2);
		}
		// s'il y a des r�ponses on renvoie une note sur 4
		if ($this->_data['satisfaction_reponses']) {
			return round(($this->_data['satisfaction_agence'] + $this->_data['satisfaction_vehicule'])/2, 2);
		}
		return null;
	}
	/**
	* Renvoie le nombre total de personnes qui ont donn� leur avis et ayant permis de calculer la note
	* @returns int
	*/
	public function getAvisTotal() {
		// en TEST on calcule un nombre au hasard
		if (SITE_MODE != 'PROD' && !$this->_data['satisfaction_reponses']) {
			$this->_data['satisfaction_reponses'] = rand(5, 300);
		}
		// on ne renvoie le nombre de r�ponses que si la note est sup�rieure � 2 et que le nombre d'avis est sup�rieur � 10
		//if ($this->_data['satisfaction_reponses'] > 10 && $this->getAvisNote() >= 2) {
		if (!empty($this->_data['satisfaction_reponses'])) {
			return $this->_data['satisfaction_reponses'];
		}
		return 0;
	}
	/**
	* Renvoie les commentaires pour une agence
	* Champs du tableau
	* 	publication
	* 	txt
	 * @param boolean $getNbAvis Permet de conna�tre le nombre r�el de commentaires
	 * afin de ne pas impacter la liste actuelle des commentaires dans une fiche agence limit�e � 10 avis clients
	* @returns array
	*/
	public function getAvisCommentaires($getNbAvis = null) {
		$comments = array();
		$sql = "select ac.publication AS publication, ac.prenom, coalesce(ac.refnat, r.txt) txt";
		$sql.=" from agence_commentaires ac join question_reponse r on (r.reservation=ac.reservation and r.question=ac.question)";
		$sql.=" where ac.publie=1";
		if (SITE_MODE == 'PROD') {
			$sql.=" and ac.agence='".$this->getId()."'";
		}
		$sql.= " UNION ";
		$sql.= "select publication, prenom, commentaire AS txt from refnat_commentaire";
		if (SITE_MODE == 'PROD' || SITE_MODE == 'PPROD') {
		    $sql.=" where agence='".$this->getId()."'";
		}
		$sql.=" order by publication desc";
		if ($getNbAvis == false) {
			$sql.=" limit 10";
		}
		$rs = sqlexec($sql);
		while ($row = mysql_fetch_assoc($rs)) {
			$comments[] = $row;
		}
		return $comments;
	}

	/**
	* Renvoie la liste des services
	*
	* @return array
	*/
	public function getServices() {
		$output = array();
		if ($this->isPointDeLivraison()) {
			$output[] = self::SERVICE_LIVRAISON;
		}
		if ($this->getData('type') & self::TYPE_ML) {
			$output[] = self::SERVICE_MALIN;
		}
		if ($this->getData('j7')) {
			$output[] = self::SERVICE_J7;
		}
		if ($this->getData('type') & self::TYPE_2R) {
			$output[] = self::SERVICE_HOLIDAY_BIKES;
		}
		return $output;
	}
}