<?php

class VisuelMoyenneDuree_Collection extends Ada_Collection
{
	/**
	* Renvoie les visules moyenne dur�e
	* 
	* @param string $type
	* @return VisuelMoyenneDuree_Collection
	*/
	static public function factory($type)
	{
		if (!in_array($type, array('vp','vu')))
			$type = 'vu';
		$x = new VisuelMoyenneDuree_Collection();
		$sql = "select * from visuel_moyenne_duree where publie=1 and type='$type' order by position";
		$x->loadFromSQL($sql);
		return $x;
	}
}
?>
