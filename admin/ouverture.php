<?
	require_once("secure.php"); 
	require_once("lib.php");

	// identifiant
	$id = getarg("id");
	
	// suppression
	if ($id && getarg("delete")) 
	{
		$sql = "delete from agence_ouverture where id = '".$id."'";
		getsql($sql);
		unset($id);
	}
	
	// modification, cr�ation
	if (getarg("update") || getarg("create"))
	{
		save_record("agence_ouverture", $_POST);
		if (!$id) 
			$id = mysql_insert_id();
	}

	$agence = $_GET["agence"];
	if ($id)
	{
		$rs = sqlexec("select * from agence_ouverture where id = ".$id);
		if (!$rs || !($row = mysql_fetch_assoc($rs)))
			die("Aucun enregistrement correspondant.");
		$agence = $row["agence"];
	}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script language="javascript" src="../scripts/check.js"></script>
	<script>
	<!-- //

	// doDelete
	function doDelete(frm)
	{
		return (confirm("Voulez-vous r�ellement supprimer cet �l�ment ?\nCette op�ration est irr�versible."));
	}
	
	// doSubmit
	function doSubmit(frm)
	{
		var err = "";
		if(!ValidateSelect(frm.agence))
			err += "Veuillez indiquer une agence\n";
		if(!ValidateSelect(frm.fermeture))
			err += "Veuillez choisir une fermeture\n";
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		return true;
	}
	
	// -->
	</script>
</head>

<body>

<form action="" method="post" onsubmit="return doSubmit(this);">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre"><img src="img/puce.gif" align="absmiddle" hspace="1">Exception � une fermeture</th>
</tr>
<tr>
	<td>
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="15%">
		<col width="35%">
		<col width="15%">
		<col width="35%">
		<tr>
			<td>Id</td>
			<td>
				<?	if ($id) echo $id; ?>
				<input type="hidden" name="id" value="<?=$id?>">
			</td>
			<td><label for="agence">Agence</label></td>
			<td>
			<?
				$sql = "select id, concat(id, '-', nom) from agence where zone='".$_SESSION['ADA001_ADMIN_ZONE']."'";
				if ($agence) $sql.=" and id like '".$agence."'";
				$sql.=" order by id";
				selectsql("agence", $sql, $agence, '--');
			?>
			</td>
		</tr>
		<tr>
			<td><label for="fermeture">Fermeture</label></td>
			<td colspan="3">
			<? 
				$fermeture = ($row['fermeture'] ? $row['fermeture'] : $_GET['fermeture']);
				$sql = "SELECT id, CONCAT('du ', DATE_FORMAT(debut,'%d/%m/%Y'),' au ', DATE_FORMAT(fin,'%d/%m/%Y')) FROM agence_fermeture WHERE agence='%' AND ((debut >= CURRENT_DATE() AND zone='".$_SESSION['ADA001_ADMIN_ZONE']."')";
				if ($fermeture) $sql .= " OR id=".$fermeture;
				$sql .= ") ORDER BY debut";
					selectsql("fermeture", $sql, $fermeture, '--');
			?>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td align="right">
		<input type="submit" name="<?=$id ? "update" : "create"?>" value="enregistrer">
<?
	if ($id) 
	{
?>
		&nbsp;
		<input type="submit" onclick="return doDelete(this.form);" name="delete" value="supprimer">
<?
	}
?>
	</td>
</tr>
</table>
</form>
</body>
</html>