<?
	require_once("secure.php"); 
	require_once("lib.php");
	$id_pdv = $_GET['pdv'] ? $_GET['pdv'] : 'FR001H';
	if (!Agence::isValidID($id_pdv))
		die("identifiant agence non valide !");
	$sql = " SELECT a.*";
	$sql.= " FROM agence_pdv AS a";
	$sql.= " WHERE a.id = '".$id_pdv."' ORDER BY a.ville";	

	$rs = sqlexec($sql);
	if ($rs)
		$row = mysql_fetch_assoc($rs);
		
	$row['latitude'] = $_GET['lat'] ? $_GET['lat'] : deg2rad($row['latitude']);
	$row['longitude'] = $_GET['lng'] ? $_GET['lng'] : deg2rad($row['longitude']);
?>	
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration - Google Maps</title>
	<link rel="stylesheet" href="style.css">
</head>
<body>
	<h3>Plan</h3>
	D�placez le marker <img src="http://maps.google.com/intl/fr_ALL/mapfiles/marker.png" alt="marker" /> sur l'emplacement de l'agence.
	<div id="map" style="height: 300px; width: 400px;"></div>
	
	<table class="noborder" width="400" cellspacing="0" cellpadding="2">
	<col width="25%" colspan="4" />
	<tr>
		<td>Nom</td>
		<td colspan="3"><?=$row["nom"]?></td>
	</tr>
	<tr>
		<td>Adresse</td>
		<td colspan="3">
			<?=stripslashes($row["adresse1"]).($row["adresse2"] ? ', '.stripslashes($row["adresse2"]) : '')?>
		</td>
	</tr>
	<tr>
		<td>CP, Ville</td>
		<td colspan="3">
		<?=$row["cp"]?>, <?=$row["ville"]?>
		</td>
	</tr>
	<tr>
		<td>Latitude</td>
		<td colspan="3"><input type="text" id="latitude_pop" name="latitude_pop" value="<?=$row["latitude"]?>"></td>
	</tr>
	<tr>
		<td>Longitude</td>
		<td colspan="3"><input type="text" id="longitude_pop" name="longitude_pop" value="<?=$row["longitude"]?>"></td>
	</tr>
	</table>
	<script src="//maps.googleapis.com/maps/api/js?sensor=false" type="text/javascript"></script>
	<script type="text/javascript">
		var lat = document.getElementById('latitude_pop');
		var lng = document.getElementById('longitude_pop');
		
		// creation de la carte
		var mapCenter = new google.maps.LatLng(<?=$row['latitude']?>, <?=$row['longitude']?>);
		var mapOptions = {
			zoom: 15,
			center: mapCenter,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			scaleControl: true
		};
		var map = new google.maps.Map(document.getElementById("map"), mapOptions);

		// marker
		var marker = new google.maps.Marker({
			position: mapCenter,
			map: map,
			draggable: true
		});
		
		//Event
		google.maps.event.addListener(marker, "dragend", function() {
			window.opener.document.forms[0].longitude.value = lng.value = marker.getPosition().lng();
			window.opener.document.forms[0].latitude.value = lat.value = marker.getPosition().lat();
		});		
	</script>
	<a href="#" onclick="window.close();return false;">fermer</a>
</body>
</html>