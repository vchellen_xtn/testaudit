<?
	$menu = array(
		'forfaits'		=> 'Forfaits',
		'service_livraison'	=> 'Service livraison',
		'service_voiturier'	=> 'Service voiturier',
		'mariage'		=> 'Mariage',
		'pizza'		=> 'Pizza',
		'../jeunes/index'	=> 'Jeune conducteur',
		'trajet_unique'		=> 'Trajet unique',
		'malin'		=> 'Location � l\'heure'
	);
	$action = basename($page);
?>
<ul class="menu type-li-4">
	<? foreach($menu as $k => $lbl) : ?>
		<li <?if ($action==$k) echo ' class="current"'; ?>>
			<a href="<?=$k?>.html"<?if ($action==$k) echo ' class="current"'; ?>><?=$lbl;?></a>
		</li>
	<? endforeach; ?>
</ul>