<?php

class Tracking_WebROI
{
	static public function doTracking(Page $page)
	{
		//Web2ROI http://rnd.web2roi.com/
		if (USE_WEB2ROI && in_array($page->getId(), array('reservation/validation','jeunes/validation'))) :
			/** @var Reservation */
			$reservation = $page->getData('reservation');
?>
		<script type="text/javascript">
		/* <![CDATA[  AB Tasty */
			(function() {
				var timestamp=new Date().getTime();
				window.sy = {
					uid : "adafr"
					,usid : ""
					,roiRef : "WEB<?=$reservation->getResIDClient();?>"
					,prixHT : "<?=$reservation->getPrixTotalHT();?>"
					,prixTTC : "<?=$reservation->getPrixTotal();?>"
					,fdp : "0"
					,typeV : "1"
				};
				window.cart = [];
				var w2r = document.createElement('script'); w2r.type = 'text/javascript'; w2r.async = true;
				w2r.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 't.sytsem.com/v2/roi.js';
				var sw2r = document.getElementsByTagName('script')[0]; sw2r.parentNode.insertBefore(w2r, sw2r);
			})();
		/* ]]> */
		</script>
<?		endif;
		//!Web2ROI
	}
}
?>
