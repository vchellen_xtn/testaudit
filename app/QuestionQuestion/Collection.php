<?
class QuestionQuestion_Collection extends Ada_Collection
{
	/**
	* Renvoie les questions d'un questionaire
	* 
	* @param  QuestionQuestionnaire $questionnaire
	* @return QuestionQuestion_Collection
	*/
	static public function factory(QuestionQuestionnaire $questionnaire)
	{
		$x = new QuestionQuestion_Collection();
		$sql  = 'SELECT q1.*, q2.id AS prev_question_id, q2.theme AS prev_question_theme, q2.type AS prev_question_type'
				.', q3.id AS next_question_id, q3.theme AS next_question_theme, q3.type AS next_question_type'
				.'	FROM question_question q1'
				.'	LEFT JOIN question_question q2 ON q2.position = q1.position - 1 AND q2.questionnaire = q1.questionnaire'
				.'	LEFT JOIN question_question q3 ON q3.position = q1.position + 1 AND q3.questionnaire = q1.questionnaire'
				.'	WHERE q1.questionnaire = '.(int)$questionnaire->getId()
				.'	ORDER BY q1.position;';
		$x->_loadFromSQL($sql);
		return $x;
	}
	
	/**
	* Charger tous les items des questions
	* @returns QuestionQuestion_Collection
	*/
	protected function _loadQuestionItems()
	{
		// le SQL pour r�cup�rer tous les items
		$sql  = 'SELECT i.*, q.type AS question_type';
		$sql.=' FROM question_item i';
		$sql.=' JOIN question_question q ON q.id = i.question';
		$sql.=' WHERE i.question IN ('.join(',',$this->getIds()).')';
		$sql.=' ORDER BY i.question, i.position';
		
		// puis cr�er les diff�rents items et les ajouter � la question correspondante
		$rs = sqlexec($sql);
		while ($row = mysql_fetch_assoc($rs))
		{
			$item = new QuestionItem($row);
			/** @var QuestionQuestion */
			$question = $this->getItem($item['question']);
			$question->addItem($item);
		}
		return $this;
	}
	
	/**
	* Renvoie le HTML d'une QuestionQuestion_Collection
	* 
	* @return string
	*/
	public function getHTML()
	{
		// pr�-charger tous les items
		$this->_loadQuestionItems();
		// puis parcourir les questions
		foreach ($this->_items as /** @var QuestionQuestion */ $question)
		{
			if ($last_theme != $question->getData('theme'))
			{
				if ($last_theme)
					$output.= "</div>\n";
				$output.= '<div class="theme"><span class="libelle"><h1 class="rouge">'.$question->getTheme()."</h1></span>\n";
				$last_theme = $question->getData('theme');
			}
			if ($question->getData('type') == 'G')
			{
				if ($question->isNewType() || $question->isNewTheme())
				{
					$output.= '<table cellpadding="0" cellspacing="0" border="0" class="questions-groupe">'."\n";
					// colonne des libell�s
					if ($items = $question->getItems())
					{
						$output.= '<tr><td class="empty"></td>'."\n";
						foreach ($items as $item)
							$output.= '<td>'.$item->getData('libelle').'</td>';
						$output.= '</tr>'."\n";
					}
				}
			}
			$output.= $question->getHTML()."\n";
			if ($question->getData('type') == 'G' && ($question->isLastType() || $question->isLastTheme()))
				$output.= '</table>'."\n";
			if ($last_theme != $question->getData('theme'))
			{
				/* do nothing */
			}
		}
		$output.= "</div>\n";
		return $output;
	}
	
	/**
	* Renvoie le JS pour le questionnaire
	* @returns string
	*/
	public function getJS()
	{
		$questionnaire_questions = '"question-'.implode('":0, "question-', $this->getIds()).'":0';
		$output = <<<HTML
<script type="text/javascript">
<!--
var questionnaire_questions = {{$questionnaire_questions}};
//-->
</script>\n
HTML;
		return $output;
	}
}
