<?php
class VisuelMoyenneDuree extends Ada_Object
{
	/**
	* Renvoie un VisuelMoyenneDuree
	* 
	* @param int $id
	* @returns VisuelMoyenneDuree
	*/
	static public function factory($id)
	{
		$x = new VisuelMoyenneDuree();
		return $x->load((int)$id);
	}
}
?>
