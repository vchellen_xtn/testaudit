		<div class="bg">
			<div class="header">
				<div class="univers_titre"><?=$this->getUniversTitre();?></div>
			</div>
			<div class="content">
				<div id="col">
				<?
					// répertoire spécifique
					$d = dirname(__file__).'/'.dirname($this->getId());
					if (is_file($f = $d.'/menu.php'))
						include ($f);
					$this->showBannieres('left');
				?>
				</div>
				
				<div id="page">
				<?
					include($this->getPagePath());
				?>
				</div>
				
				<div class="clear"></div>
			</div>
		</div>