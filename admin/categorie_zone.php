<?
	require_once("secure.php");
	require_once("lib.php");

	// identifiant
	$id = (int) $_REQUEST['categorie'];

	if(!$id)
		die('Vous devez pr�ciser une categorie !');
	// infos de la categorie
	$categorie = Categorie::factory($id);

	// modification, cr�ation
	if(getarg("update") || getarg("create"))
	{
		// on supprimer les informations actuellement pr�sentes
		getsql("DELETE FROM categorie_zone WHERE categorie=".$categorie['id']);
		$msg = array();
		foreach ($_POST['categorie_zone'] as $k => $row)
		{
			if (!$row['age'] && !$row['permis'] && !$row['km_sup']) continue;
			if (!$row['age'])
				$msg[] = $row['zone'].': �ge non pr�cis�';
			if (!$row['permis'])
				$msg[] = $row['zone'].': permis non pr�cis�';
			if (!is_numeric($row['km_sup']))
				$msg[] = $row['zone'].': km sup non pr�cis�';
			if ($row['age'] && $row['permis'] && is_numeric($row['km_sup']))
				save_record('categorie_zone', $row);
		}
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script>
	<!-- //
	// doSubmit
	function doSubmit(frm)
	{
		var err = "";
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		return true;
	}
	// -->
	</script>
</head>
<body>
<?
if (count($msg))
echo "<strong>Erreurs lors de l'enregistrement<br/>".join('<br/>', $msg).'</strong>';
?>
<form action="" method="post" onsubmit="return doSubmit(this);" enctype="multipart/form-data">
<input type="hidden" name="categorie" value="<?=$id?>">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre">
		<img src="img/puce.gif" align="absmiddle" hspace="1">
		<a href="categorie.php?id=<?=$id?>">Cat�gorie <?=$categorie['nom'].' ('.$categorie['mnem'].', '.$categorie['type'].', '.$categorie['id'].')'?></a>
		Informations par zone
	</th>
</tr>
<tr>
	<td>
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="30%">
		<col width="25%">
		<col width="25%">
		<col width="20%">
		<tr>
			<td><strong>Zone</strong></td>
			<td><strong>Age</strong></td>
			<td><strong>Permis</strong></td>
			<td><strong>Km. Sup. (�)</strong></td>
		</tr>
		<tr>
			<td><strong>France (par d�faut)</strong></td>
			<td><?=$AGE[$categorie['age']]?></td>
			<td><?=$PERMIS[$categorie['permis']]?></td>
			<td><?=$categorie['km_sup']?>&nbsp;�</td>
		</tr>
<?
		$sql = "select z.id, z.nom, cz.age, cz.permis, cz.km_sup";
		$sql.=" from zone z left join categorie_zone cz on (cz.zone=z.id and cz.categorie='".$categorie['id']."')";
		$sql.=" where z.id!='fr'";
		$sql.=" order by z.nom";
		$rs = sqlexec($sql);
		while ($row = mysql_fetch_assoc($rs))
		{
			$fld = 'categorie_zone['.$row['id'].']';
?>
		<tr>
			<td>
				<strong><?=$row['nom']?></strong>
				<input type="hidden" name="<?=$fld?>[zone]" value="<?=$row['id']?>"/>
				<input type="hidden" name="<?=$fld?>[categorie]" value="<?=$categorie['id']?>"/>
			</td>
			<td><? selectarray($fld.'[age]', $AGE, $row['age'], '--par d�faut--'); ?></td>
			<td><? selectarray($fld.'[permis]', $PERMIS, $row['permis'], '--par d�faut--'); ?></td>
			<td><input style="width: inherit;" type="text" name="<?=$fld?>[km_sup]" value="<?=$row['km_sup']?>" maxlength="4" size="4"/></td>
		</tr>
<?
			
		}
?>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%">
		<tr>
			<td align="right">
				<input type="submit" name="<?=$id?"update":"create"?>" value="enregistrer" onclick="return doSubmit(this.form);">
			<?
				if($row)
					echo '<input type="submit" onclick="return doDelete(this.form);" name="delete" value="supprimer">';
			?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
</form>
</body>
</html>

