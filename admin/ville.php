<?
	require_once("secure.php"); 
	require_once("lib.php");

	// identifiant
	$id = (int)getarg('id');
	if (!$id) $id = null;
	
	// suppression
	if ($id && getarg("delete")) 
	{
		$sql = "delete from ville where id = '".$id."'";
		getsql($sql);
		unset($id);
	}
	
	// modification, cr�ation
	if (getarg("update") || getarg("create"))
	{
		$_POST["latitude"]  = deg2rad($_POST["latitude"]);
		$_POST["longitude"] = deg2rad($_POST["longitude"]);
		save_record('ville', $_POST);
		if (!$id) 
			$id = mysql_insert_id();
	}
	if ($id)
	{
		$rs = sqlexec("select * from ville where id = ".$id);
		if (!$rs || !($row = mysql_fetch_assoc($rs)))
			die("Aucun enregistrement correspondant.");
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script language="javascript" src="../scripts/check.js"></script>
	<script>
	<!-- //

	// doDelete
	function doDelete(frm)
	{
		return (confirm("Voulez-vous r�ellement supprimer cet �l�ment ?\nCette op�ration est irr�versible."));
	}
	
	// doSubmit
	function doSubmit(frm)
	{
		var err = "";

		if(!frm.nom.value.length)
			err += "Veuillez indiquer un nom\n";
		
		if(!frm.canonique.value.length)
			err += "Veuillez indiquer un nom canonique\n";
		
		if (!frm.cp.value.length)
			err+="Veuillez indiquer un code postal\n";
		
		var latitude = frm.latitude.value.replace(/,/, ".")
		if(!frm.latitude.value.length || isNaN(latitude))
			err += "Veuillez indiquer une latitude\n";
		
		var longitude = frm.longitude.value.replace(/,/, ".")
		if(!frm.longitude.value.length || isNaN(longitude))
			err += "Veuillez indiquer une longitude\n";
		
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		return true;
	}
	
	// -->
	</script>
</head>
<body>
<form action="" method="post">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre">
	<img src="img/puce.gif" align="absmiddle" hspace="1">Ville
	</th>
</tr>
<tr>
	<td>
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="15%">
		<col width="35%">
		<col width="15%">
		<col width="35%">
		<tr>
			<td>ID</td>
			<td>
<? 
		echo $id;
		echo '<input type="hidden" name="id" value="'.$id.'">';
?>
			</td>
			<td>Nom</td>
			<td>
				<input type="text" name="nom" value="<?=$row["nom"]?>">
			</td>
		</tr>
		<tr>
			<td>Canonique</td>
			<td><input type="text" name="canonique" value="<?=$row["canonique"]?>"></td>
			<td>CP</td>
			<td><input type="text" name="cp" value="<?=$row["cp"]?>" maxlength="8"></td>
		</tr>
		<tr>
			<td>
				Latitude
				<a href="#"  onclick="showLatLng(document.forms[0]);" ><img src="img/geoloc.gif" alt="calcul des coordonn�es" title="calcul des coordonn�es" border="0"/></a>
			</td>
			<td><input type="text" name="latitude" value="<?=rad2deg($row["latitude"])?>"></td>
			<td>Longitude</td>
			<td><input type="text" name="longitude" value="<?=rad2deg($row["longitude"])?>"></td>			
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td align="right">
		<input type="submit" name="<?=$id ? "update" : "create"?>" value="enregistrer" onclick="return doSubmit(this.form);">
<?
	if ($id) 
	{
?>
		&nbsp;
		<input type="submit" onclick="return doDelete(this.form);" name="delete" value="supprimer">
<?
	}
?>
	</td>
</tr>
<tr>
	<td>
		<div id="map" style="height: 200px; width: 300px;"></div>
	</td>
</tr>
</table>
</form>
<script src="//maps.googleapis.com/maps/api/js?sensor=false" type="text/javascript"></script>
<script type="text/javascript">
	// creation map
	var mapCenter = new google.maps.LatLng(<?=rad2deg($row["latitude"])?>, <?=rad2deg($row["longitude"])?>);
	var mapOptions = {
		zoom: 12,
		center: mapCenter,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		scaleControl: true
	};
	var map = new google.maps.Map(document.getElementById("map"), mapOptions);
	var marker = new google.maps.Marker({
		map: map,
		position: mapCenter
	});

	// changer coordonn�es
	function showLatLng(frm) 
	{
		var lat = frm.latitude;
		var lng = frm.longitude;
		
		var address = frm.cp.value + ' ' + frm.nom.value + ' France';
		var geocoder = new google.maps.Geocoder();	
		geocoder.geocode( { 'address': address}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) 
			{
				var resultPoint = results[0].geometry.location;
				if(resultPoint)
				{
					
					var msg = 'Les coordonn�es de vont �tre mises � jour :\n';
					msg += lat.value + ' -> ' + resultPoint.lat() + "\n";
					msg += lng.value + ' -> ' + resultPoint.lng() + "\n";
					if (confirm(msg)) {
						lat.value = resultPoint.lat();
						lng.value = resultPoint.lng();
					}
				}
				//affichage sur la map
				marker.setPosition(resultPoint);
				map.setCenter(resultPoint);
			} 
			else if(status == google.maps.GeocoderStatus.ZERO_RESULTS)
			{
				alert('Aucun r�sultat trouv� !');
			}
			else
			{
				alert("L'erreur suivante s'est produite : " + status);
			}
		});
	}

</script>
</body>
</html>
