<?
/**
* Conditions d'annulation apr�s la mise en production (2011-05-03) / Refonte Graphique V45
	- pas d'annulation possible 48h avant la r�servation (au lieu de 24)
	- il y a toujours 20� de frais de dossier
*/
class Reservation_ConditionsAnnulation02 extends Reservation_ConditionsAnnulation
{
	const FRAIS_ANNULATION = 20;

	public function isCancellable($preavis, &$msg)
	{
		if ($preavis <= 48)
		{
			$msg = "L'annulation doit �tre faite au moins 48 heures avant le jour de votre d�part. En de�� de 48h vous n'avez pas la possibilit� d'annuler votre r�servation.";
			$msg .= '<br /><span class="rouge">Vous ne pouvez plus annuler cette r�servation.</span>';
			return false;
		}
		// on d�tailles les conditions d'annulation selon le d�lai
		$msg = "L'annulation intervient plus de 48 heures avant votre d�part : le montant de votre r�servation va vous �tre rembours�, avec une d�duction de ".$this->getCancellationFees($preavis)." � TTC de frais de dossier.";
		return true;
	}
	public function getCancellationFees($preavis)
	{
		return self::FRAIS_ANNULATION;
	}
	
	public function getConditions()
	{
		$str = <<<EOT
		<p style="margin-bottom:10px;">
			L'annulation doit �tre faite au moins 48 heures avant le jour de votre d�part. En de�� de 48h vous n'avez pas la possibilit� d'annuler votre r�servation.<br />
			<br />
			Si votre annulation intervient :
		</p>
		<ul class="tiret">
			<li><span class="rouge gras">plus de 48h avant votre d�part</span> : le montant de votre r�servation vous est rembours�, avec une d�duction de 20 � TTC de frais de dossier 
			<li><span class="rouge gras">moins de 48h avant votre d�part</span> : le montant de votre r�servation ne vous sera pas rembours�</li>
			<li><span class="rouge gras">2 mois apr�s votre r�glement initial (et avant votre d�part)</span> : Cette r�servation ne peut pas �tre annul�e en ligne. Pour demander un remboursement, merci d'adresser un email � <a href="mailto:reservation@ada.fr">reservation@ada.fr</a> en pr�cisant le n� de la r�servation concern�e. Nous vous confirmerons l'annulation dans les plus brefs d�lais.</li>
		</ul>
		<p style="margin-bottom:10px;">
			<strong>Il n'y a pas lieu � remboursement :</strong>
		</p>
		<ul class="tiret">
			<li>pour toute location plus courte que la dur�e pr�vue dans la r�servation</li>
			<li>si le conducteur principal ne se pr�sente pas pour la prise du v�hicule</li>
			<li>si le conducteur ne pr�sente pas le jour de sa location l'int�gralit� des documents n�cessaires � celle-ci, conform�ment aux conditions g�n�rales de location</li>
			<li>si le client est dans l'incapacit� de fournir le d�p�t de garantie pr�vu aux conditions g�n�rales de location lorsqu'il vient retirer le v�hicule</li>
			<li>en cas de retard dans la prise du v�hicule</li>
			<li>en cas de d�faut de prise du v�hicule � l'heure pr�vue de d�but de location</li>
			<li>en cas d'annulation faite moins de 48h avant votre d�part</li>
		</ul>
EOT;
		return $str;
	}
	public function getInfos()
	{
		$str = <<<EINFO
		Vous pouvez annuler votre r�servation jusqu'� 48h avant la prise du v�hicule.<br />
		Des frais de dossier de 20 euros TTC vous seront factur�s pour toute annulation au-del� de 48h avant votre d�part. Moins de 48h avant votre d�part : le montant de votre r�servation ne vous sera pas rembours�.<br/>
EINFO;
		return $str;
	}
}
?>
