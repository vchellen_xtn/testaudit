<?php
class Page_Resas extends Page_ResasBase
{
	/**
	* Authentification
	*
	*/
	public function loginAction()
	{
		if (!$this->_adminUser && isset($_POST['login']) && isset($_POST['password']))
		{
			$adminUser = Acces::factory($_POST['login']);
			if ($adminUser && $adminUser->checkPassword($_POST['password']))
			{
				$this->_adminUser = $adminUser;
				self::setCookie('ADA001_ADMIN_AUTH', $adminUser->getCookie());
			}
			else {
				$this->_addMessage("Vous n'avez pas pu vous identifier sur ce site.");
			}
		}
		if ($this->_adminUser) {
			self::redirect(self::getURL('resas/agence.html', null, true));
		}
	}

	/**
	* Choix de l'agence
	*/
	public function agenceAction()
	{
		// OK on est connect�s, il faut maintenant choisir une agence
		if (isset($_POST['agence']) && Agence::isValidID($_POST['agence'])) {
			$msg = '';
			if ($this->changeAgence($_POST['agence'], $msg)) {
				self::redirect(self::getURL(($_POST['goto']=='ADMIN' ? 'resas/admin/index.html' : 'resas/index.html'), null, true));
				return;
			}

			// s'il y a un message d'erreur on l'affiche
			if ($msg) {
				$this->_addMessage($msg);
			}
		}
		$this->addJS('scripts/agence_liste.js', false);
	}
	// affchage du formulaire de recherche
	public function indexAction()
	{
		// gestion du formulaire de r�servation
		$this->addJS('scripts/form_reservation.js');

		// on change l'agence si demand� dans le POST
		if (isset($_POST['changer_agence']) && $_POST['changer_agence'] != $this->_resasUser['agence']) {
			$msg = '';
			$this->changeAgence($_POST['changer_agence'], $msg);
			if ($msg) {
				$this->_addMessage($msg);
			}
		}

		// suivi de l'utilisateur pour SmartProfile
		$this->setTrackInfo('user', $this->getAdminUser() ? 'teleconseillers' : 'franchises');
		$this->setTrackInfo('login', $this->getResasUser()->getData('login'));
	}

	/**
	* Calcule et affiche les r�sultats
	*
	*/
	public function resultatAction()
	{
		$this->addJS('scripts/form_reservation.js');
		$this->addJS('scripts/agence_liste.js', false);

		// on change l'agence si demand� dans le POST
		if (isset($_POST['agence']) && $_POST['agence'] != $this->_resasUser['agence']) {
			$msg = '';
			$this->changeAgence($_POST['agence'], $msg);
			if ($msg) {
				$this->_addMessage($msg);
			}
		}
		// cr�er la r�servation � partir de $_POST
		if (/** @var AgenceResa */ $resa = AgenceResa::create($this->_resasUser, $this->_agence, $_POST, $msg))
		{
			// stocker la r�servation et r�cup�rer les offres
			$this->setData('resa', $resa);
			// pr�ciser le type dans le trackng
			$this->setTrackPage(strtolower($resa['type']));
			$track = array();
			$track['agence_search'] = Tracking_SmartProfile::getAgenceSearch($resa->getReservation());
			$track['agence_init'] = $track['agence_search'];
			$track['pays'] = $resa->getAgence()->getData('zone');
			$this->setTrackInfo($track);

			// se souvenir de la recherche
			$this->_setCookie('ADA001_LAST_SEARCH', serialize($resa->getReservation()->getFields()));
			// et le cookie
			self::_setCookie('ADA001_TRACK_AGENCE', $track['agence_search']);
		}
		$this->_addMessage($msg);
		return;
	}
	// demande d'appel d'un t�l� conseiller
	public function telephoneAction()
	{
		// cr�er la r�servation � partir de $_POST
		if (/** @var AgenceResa */ $resa = AgenceResa::create($this->_resasUser, $this->_agence, $_POST, $msg))
		{
			// stocker la r�servation et r�cup�rer les offres
			$this->setData('resa', $resa);
			if (isset($_POST['agence_choisie']) && Agence::isValidID($_POST['agence_choisie'])) {
				$agenceChoisie = Agence::factory($_POST['agence_choisie']);
				$this->setData('agence_choisie', $agenceChoisie);
			}
			// si le formulaire client est renseign� on essaie de cr�er le client
			if (isset($_POST['frm_telephone']) && isset($_POST['client'])) {
				// est ce que le client existe d�j� ?
				$clientData = $_POST['client'];
				$errors = array();
				if ($client = AgenceClient::create($this->_agence, $clientData, $errors)) {
					// on enregistre le prospect
					$prospect = ProspectResa::create($resa, $client, $agenceChoisie, filter_var($_POST['delai'], FILTER_SANITIZE_STRING));
					$this->setData('prospect_resa', $prospect);
					// le tracking
					$this->setTrackPage('resas_appel', false);
					$this->setTrackInfo('callback3', $_COOKIE['ADA001_TRACK_AGENCE']);
					return;
				}
				if (count($errors)) {
					$this->_addMessage($errors);
					$this->setData('client_data', $clientData);
				}
			}
			$this->setTrackInfo('callback2', $_COOKIE['ADA001_TRACK_AGENCE']);
			$this->addJS('scripts/jquery.validate.min.js');
		}
		$this->_addMessage($msg);
		return;
	}

	// affiche des optoins compl�mentaires skin = vN
	public function optionsAction()
	{
		// cr�er la r�servation � partir de $_POST
		if ($resa = AgenceResa::create($this->_resasUser, $this->_agence, $_POST, $msg))
		{
			// stocker la r�servation et r�cup�rer les offres
			$this->setData('resa', $resa);
			// pr�ciser le type dans le trackng
			$this->setTrackPage(strtolower($resa['type']));
			return;
		}
		$this->_addMessage($msg);
		return;
	}
	/**
	 *
	 */
	// affichage du formulaire d'inscription
	public function inscriptionAction()
	{

		// recherche d'un client en JSON
		if (isset($_GET['email'])) {
			$json = '{}';
			if ($email = filter_var($_GET['email'], FILTER_VALIDATE_EMAIL)) {
				$fields = array('email','titre','prenom','nom','naissance','societe','adresse','adresse2','cp','ville','pays','tel','gsm','promo');
				// dans ADAFR, puis dans RESAS puis dans Programme Kilom'�tre
				if ($client = Client::factory($email)) {
					$json = $client->toJson(array_merge($fields, array('f_societe','f_adresse','f_adresse2','f_cp','f_ville','f_pays')));
				} else if ($client = AgenceClient::factory($this->_agence, $email)) {
					$json = $client->toJson($fields);
				} else if ($xml = @simplexml_load_string(PgKm::searchUser($email))) {
					if ($person = $xml->personne[0]) {
						$json = json_encode(array(
							'email'		=> $email,
							'nom'		=> (string) $person->nom,
							'prenom'	=> (string) $person->prenom,
							'naissance'	=> (string) $person->naissance,
							'adresse'	=> (string) $person->adresse1,
							'adresse2'	=> (string) $person->adresse2,
							'cp'		=> (string) $person->codepostal,
							'tel'		=> (string) $person->telephone
						));
					}
				}
			}
			header('Content-Type: application/json; charset=utf-8');
			die($json);
		}
		//
		if (isset($_GET['searchuser'])) {
		    $unipro = new Unipro();
		    $check = $unipro->searchUser($_GET['agence'],$_GET['nom'],$_GET['prenom'],$_GET['ville'],$_GET['tel']);
		    $json = json_encode($check);
		    header('Content-Type: application/json; charset=utf-8');
		    echo $json;
		    die();

		}
		// cr�er la r�servation � partir de $_POST
		$this->addJS('scripts/jquery.validate.min.js');
		if ($resa = AgenceResa::create($this->_resasUser, $this->_agence, $_POST, $msg, true))
		{
			// si le formulaire client est renseign� on essaie de cr�er le client
			if (isset($_POST['frm_inscription']) && isset($_POST['client'])) {
				// si le vendeur est pr�cis� on l'enregistre
				if (isset($_POST['vendeur'])) {
					if (preg_match('/^[a-zA-Z0-9\-\.]{2,64}$/', $_POST['vendeur'])) {
						$this->setCookie('ADA001_RESAS_VENDEUR', $_POST['vendeur']);
					} else {
						unset($_POST['vendeur']);
					}
				}

				// on r�cup�re les informations du client
				$clientData = $_POST['client'];
				$errors = array();
				/* OK. Il y a maintenant 2 sc�narios
				** 1. T�l�conseiller et PREPA : on cr�e un client ADAFR si n�cessaire et on cr�e une r�servation ADAFR pour la payer en ligne
					paiement.html -> validation.html
				** 2. dans tous les autres cas, on cr�e un client RESAS (agence_client) et r�servation RESAS (agence_resa) pour l'envoyer � UNIPRO
					recapitulatif.html
				*/
				if ($resa['operation'] == 'PREPA' && $this->isTeleConseillerLogged() && !$resa->isLocapass()) {
					// on r�cup�re ou on cr�e un client ADAFR
					// ici on teste si l'adresse mail n'est pas une adresse g�n�r�e automatiquement
					// pas le formualaire d'inscription de resa (en @nomail.ada.fr)
					// on bloque on ne peut pas cr�er un compte sans mail valide

					if (substr($clientData['email'],-14)=="@nomail.ada.fr") {
					    $errors[]="La creation du compte utilisateur ada.fr necessite une adresse mail valide.";
					}
					elseif ($client = Client::create($clientData, $errors)) {
						// puis on cr�e une nouvelle r�servation
						$reservation = $resa->getReservation();
						$reservation['forfait_type'] = 'ADAFR';
						$reservation['canal'] = 'TEL';
						$reservation['agent'] = $this->_resasUser['login'];
						// enregistrer la r�servation dans la base de donn�es
						$reservation->save();
						// y associer le client
						$reservation->checkClient($client, true);
						// commentaire et le v�hicule si renseign�
						$resInfo = null;
						foreach(array('unipro_vehicule','commentaire') as $k) {
							if ($resa[$k]) {
								$resInfo = $reservation->getResInfo();
								$resInfo->setData($k, $resa->getData($k));
							}
						}
						if ($resInfo) {
							$resInfo->save();
						}
						// rediriger vers la page de paiement pour cette r�servation
						self::redirect(self::getURL('resas/paiement.html?reservation='.(int)$reservation->getId(), null, true));
					}
				} else if ($client = AgenceClient::create($this->_agence, $clientData, $errors)) {
					// on pr�cise le canal
					if ($this->_adminUser) {
						$resa['canal'] = ($this->_adminUser->hasRights(Acces::SB_ADMIN) ? 'ADM' : 'TEL');
					}
					// on a le client, on enregistre maintenant la r�servation
					$resa->setClient($client);
					$resa->save();
					self::redirect(self::getURL('resas/recapitulatif.html?resa='.(int)$resa->getId(), null, true));
					return;
				}
				if (count($errors)) {
					$this->_addMessage($errors);
					$this->setData('client_data', $clientData);
				}
			}

			// stocker la r�servation et r�cup�rer les offres
			$this->setData('resa', $resa);

			// on initialise le vendeur
			$this->setData('vendeur', ($_COOKIE['ADA001_RESAS_VENDEUR'] ? filter_var($_COOKIE['ADA001_RESAS_VENDEUR'], FILTER_SANITIZE_STRING) : $this->_adminUser['login']));

			// pr�ciser le type dans le trackng
			$reservation = $resa->getReservation();
			$this->setTrackPage(strtolower($reservation['type']));
			return;
		}
		$this->_addMessage($msg);
		return;
	}
	// affichage de AgenceResa avant envoi d�finitif
	public function recapitulatifAction()
	{
		// on v�rifie l'identifiant de la r�servation
		$resaID = (int) $_GET['resa'];
		if (!$resaID) {
			$this->_addMessage("Aucune r�servation n'est indiqu�e");
			return;
		}
		if ($resa = AgenceResa::factory($this->_agence, $resaID)) {
			if (!$resa['client']) {
				$this->_addMessage("Aucun client n'est associ�e � cette r�servation !");
				return;
			}
			$this->setData('resa', $resa);
			if (isset($_POST['frm_recapitulatif'])) {
				if ($operation = $_POST['operation']) {
					$msg = '';
					if (!$resa->sendResa($operation, $msg)) {
						$this->_addMessage($msg);
					} else {
						// on n'egistre la transformation du CA que pour le paiement imm�diat
						if ($resa['operation']=='PREPA') {
							$this->setTrackInfo(Tracking_SmartProfile::reservationValidation($resa));
						}
						$this->setTrackPage('resas_validation', false);
					}
				}
			} else {
				// le formulaire n'a pas �t� soumis,on informe de la connexion pour l'utilisateur
				$this->setTrackInfo('pr_in', Tracking_SmartProfile::getPR($resa));
			}
			$this->setTrackPage(strtolower($resa['type']));
		} else {
			// erreur : la r�servation ne peut pas �tre vue
			$this->_addMessage("La r�servation $resaID n'est pas associ�e � l'agence.");
		}
	}

	/* paiement en ligne pour les t�l�-conseillers */
	public function paiementAction() {
		// on v�rifie l'acc�s � la r�servation
		$resID = (int) $_GET['reservation'];
		if (!$resID) {
			$this->_addMessage("Aucune r�servation n'est indiqu�e !");
			return;
		}
		$reservation = Reservation::factory($resID);
		if (!$reservation || $reservation['canal']!='TEL' || $reservation['agent'] != $this->_resasUser['login']) {
			$this->_addMessage(sprintf("Vous n'�tes pas autoris� � acc�der � la r�servation #%ld (%s, %s) !", $resID, $reservation['canal'], $reservation['agent']));
			return;
		}
		// si d�j� pay�e,on va vers la page de paiement
		if ($reservation['statut'] == 'P') {
			self::redirect(self::getURL('resas/validation.html?reservation='.$resID, null, true));
			return;
		}
		if ($reservation['statut'] != 'V') {
			$this->_addMessage(sprintf("La r�servation #%ld a un statut incorrect : %s.", $resID, $reservation['statut']));
			return;
		}
		// code grillable : le r�cup�rer et le tester
		$msg = null;
		$code = $reservation->getPromotionCode();
		if (!$code && isset($_POST['grillable'])) {
			$code = PromotionCode::factory($_POST['grillable'], true, $msg);
		}
		if ($code)
		{
			if ($code->isValidFor($reservation, $msg, true))
			{
				$this->setData('grillable', $code);
				$reservation->setData('total', $reservation->getPrixTotal() - $code['reduction']);
			}
		}
		// renseigner le message si n�cessaire
		if ($msg) {
			$this->setData('msg_grillable', $msg);
		}
		// !ATTENTION : dans le cas d'un appel AJAX on renvoie en JSON
		if ($_POST['frm_grillable'])
		{
			$json = new Ada_Object();
			if ($msg)
				$json['grillable_error'] = $msg;
			else
			{
				$json['grillable_reduction'] = show_money($code['reduction']);
				$json['grillable_reduction_max'] = show_money($code['reduction_max']);
				$json['grillable_code'] = $code->toString();
			}
			die($json->toJson());
		}

		// ajouter Validates
		$this->addJS('scripts/jquery.validate.min.js');
		// on enregistre la r�servation pour la page
		$client = $reservation->getClient();
		$this->setData('reservation', $reservation);
		$this->setData('client', $client);
		// .. puis on fait le tracking
		$this->setTrackPage(strtolower($reservation['type']));
		$this->setTrackInfo('login', $client['email']);
		$this->setTrackInfo('pr_in', Tracking_SmartProfile::getPR($reservation));
	}
	/* validation du paiement en ligne pour les t�l�-conseillers */
	public function validationAction() {
		// on v�rifie l'acc�s � la r�servation
		$resID = (int) ($_POST['reservation'] ? $_POST['reservation'] : $_GET['reservation']);
		if (!$resID) {
			$this->_addMessage("Aucune r�servation n'est indiqu�e !");
			return;
		}
		$reservation = Reservation::factory($resID);
		if (!$reservation || $reservation['canal']!='TEL' || $reservation['agent'] != $this->_resasUser['login']) {
			$this->_addMessage(sprintf("Vous n'�tes pas autoris� � acc�der � la r�servation #%ld (%s, %s) !", $resID, $reservation['canal'], $reservation['agent']));
			return;
		}
		// si d�j� pay�e,on va vers la page de paiement
		if (!in_array($reservation['statut'], array('V','P'))) {
			self::redirect(self::getURL('resas/paiement.html?reservation='.$resID, null, true));
			return;
		}
		// ajouter Validates
		$this->addJS('scripts/jquery.validate.min.js');
		// on enregistre la r�servation pour la page
		$client = $reservation->getClient();
		$this->setData('reservation', $reservation);
		$this->setData('client', $client);
		// .. puis on fait le tracking
		$this->setTrackPage(strtolower($reservation['type']));
		// on ne faite le paiement que s'il n'a pas d�j� eu lieu...
		if ($reservation['statut'] == 'P') {
			$this->_addMessage("Cette r�servation est d�j� en cours de r�glement.<br>Un e-mail de confirmation de r�servation vous a �t� envoy�.");
		} else if ($reservation['statut'] == 'V') {
			$resInfo = $reservation->getResInfo();
			// s'il y a un commentaire on l'enregistre
			if ($commentaire = filter_var($_POST['commentaire'], FILTER_SANITIZE_STRING)) {
				$resInfo->setData('commentaire', $commentaire);
				$resInfo->save();
			}
			// s'il y a des CGV � enregistrer
			if (isset($_POST['cgv']) && is_array($_POST['cgv']))
			{
				foreach($_POST['cgv'] as $cgv)
				{
					$a = array('client' => $reservation['client_id'], 'cgv' => filter_var($cgv, FILTER_SANITIZE_STRING));
					save_record('client_cgv', $a);
				}
			}
			// effectuer le paiement
			$transaction = Transaction::factory('PB');
			if (!$transaction)
			{
				$this->_addMessage("Le mode de paiement n'a pas pu �tre d�termin�");
				return;
			}
			// effectuer le paiement proprement dit
			$transaction->doPayment($reservation, $_POST);
			$this->_addMessage($transaction->getMessages());
			// si le paiement a r�ussi...
			if ($reservation['statut'] == 'P') {
				// tracking NSP
				$this->setTrackInfo(Tracking_SmartProfile::reservationValidation($reservation));
			}
		}
	}
}
?>
