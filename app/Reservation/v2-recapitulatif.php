<?php
/**
* $this est Reservation
* $page page o� se trouve le bloc
* $showTotal : afficher le total ou pas ?
* $showPrixOptions : affichr ou non le prix des options
*/
	/** @var Reservation */
	$reservation = $this;
	/** @var ResOption_Collection */
	$options = $reservation->getResOptions();
	/** @var Categorie */ 
	$categorie = $reservation->getCategorie();
	/** @var Agence */ 
	$agence = $reservation->getAgence();
?>
<div class="reservation_recapitulatif">
	<h2>R�capitulatif de votre r�servation</h2>
	<strong class="famille"><?=$categorie['famille_nom']?></strong>
	<strong class="categorie"><?=$categorie['categorie_nom']?></strong>
	<img src="../<?=$categorie->getImg('big')?>" class="visu" alt="" />
	<img src="../css/img/col_fleche.png" class="col_fleche" alt="" />
	<div class="offre_total2">
	<? if ($reservation->isLocapass()) :?>
		<img src="../css/img/locapass.png" alt="Locapass" />
	<? elseif ($showTotal): ?>
		<div class="offre_total">
			<p>
				<span class="total">Total TTC</span>
				<span class="prix_total"><?=show_money($reservation->getPrixTotal())?></span>
				<span class="euro">�</span>
				<span class="end"></span>
			</p>
			<? if (!$reservation->isInPayment()) : ?>
			<span class="paiement_immediat">tarif pour un paiement imm�diat</span>
			<? endif; ?>
		</div>
		<?
	endif; // !$isLocapass ?>
	</div>
	<div class="clear"></div>
	<p>
		<strong><?=$agence['nom']?></strong>&nbsp;|&nbsp;<a href="../popup.php?page=agences/agence&id=<?=$agence['id']?>" onclick="return wOpen(this.href, 630, 800)" class="infos_agence">Horaires et plan d'acc�s</a>
		<br /><?=$agence->getHtmlAddress('<br/>','adresse1,cp_ville'); ?>
		<br/>
		<br/>
		<hr />
		<? if (strstr('AP', $reservation['statut'])) : ?>
		<br/><strong>R�servation n&#176;</strong><? echo $reservation->getResIDClient(); ?>
		<? endif; ?>
		<br/><strong>Location </strong>du <?=$reservation->getDebut('d/m/y H:i');?> au <?=$reservation->getFin('d/m/y H:i');?>
		<br/><strong>Dur�e </strong><?=$reservation->getDuree();?>
		<? if (!$reservation->isLocapass()) :?>
		<br/><strong>Kilom�trage inclus </strong><?=$reservation->getKm(false);?>
		<? elseif($conducteur = $reservation->getConducteur()) :?>
		<br/><strong>Conducteur </strong><?=$conducteur['prenom'].' '.$conducteur['nom'];?>
		<? endif; ?>
		<? if ($reservation->getPrixCoupon()) : ?>
		<br /><strong>Offre Pro</strong>
		<? endif; ?>
		<br/>
		<br/>
		<hr />
		<? echo $reservation->getResOptions()->toHtml2($showPrixOptions); ?>
	</p>
	<? if (!strstr('VPXA', $reservation['statut'])) : ?>
	<form name="reservation" method="post" action="<?=Page::getURL('reservation/resultat.html');?>">
	<?
		foreach($reservation->getFields() as $k => $v)
		{
			if (!$v) continue;
			echo '<input type="hidden" name="'.$k.'" value="'.$v.'"/>';
		}
		$type = strtolower($reservation['type']);
		$date_retrait = null;
		foreach($reservation->getResOptions() as $k => $option)
		{
			if ($option['quantite'])
				echo '<input type="hidden" name="options_u['.$k.']" value="'.$option['quantite'].'"/>';
			else
				echo '<input type="hidden" name="options[]" value="'.$k.'"/>';
			if (!$date_retrait && $option['retrait'])
				$date_retrait = $option['retrait'];
		}
		if ($date_retrait)
			echo '<input type="hidden" name="options_retrait" value="'.$date_retrait.'"/>';
	?>
		<input type="hidden" name="categorie_choisie" value="<?=$reservation['categorie']?>"/>
		<p class="valid"><input class="modifiez" type="submit" value="Modifiez" /></p>
	</form>
	<? endif; ?>
	<? if (!in_array(dirname($page), array('jeunes','resas'))  && !$reservation->isInPayment()) :?>
	<img src="../css/img/audiotel_p3-4.png" alt="Besoin d'aide ? Nos �quipes sont � votre disposition au 0899 46 46 36" style="margin:25px 0 0;" />
	<? endif; ?>
</div>
