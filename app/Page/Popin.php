<?php
class Page_Popin extends Page
{
	/** La page supporte-t-elle le HTTPS ? @returns bool */
	protected function isThatPageNeedsHttps()
	{
		return true;
	}

	// pr�parer les donn�es selon les pages
	protected function checkParameters()
	{
		$action = $this->getAction();
		if (preg_match('/franchise/', $action))
		{
			$categorie = Categorie::factory((int)$_REQUEST['categorie']);
			if (!$categorie || $categorie->isEmpty())
			{
				$this->set404();
				exit();
			}
			$this->setData('categorie', $categorie);
		} else if (in_array($action, array('agence','forfait_details'))) {
			if (isset($_REQUEST['agence']) && Agence::isValidID($_REQUEST['agence'])) {
				if ($agence = Agence::factory($_REQUEST['agence'])) {
					$this->setData('agence', $agence);
				}
			}
		}
	}

	public function categorie_360Action() {
		$id_categorie = (int) $_REQUEST['categorie'];
		$categorie = Categorie::factory($id_categorie);
		if ($categorie && !empty($categorie['izmocar'])) {
			$iframe_url = BASE_URL_360 . '/' . $categorie['izmocar'];
			$this->setData('iframe_url', $iframe_url);
		} else {
			$this->set404();
			exit();
		}
	}

	/**
	* Afficher les d�tails d'un forfait :
	* - soit depuis la page reservation/resultat � partir d'un forfait
	* - soit depuis la page reservation/inscription � partir d'une r�servation
	* 
	*/
	public function forfait_detailsAction()
	{
		// s'il y a une r�servation
		if (isset($_POST['reservation']) && isset($_POST['key']) && $_POST['key'] == Page::getKey((int)$_POST['reservation'])) {
			if ($reservation = Reservation::factory((int) $_POST['reservation'])) {
				$this->setData('reservation', $reservation);
				$this->setData('agence', $reservation->getAgence());
				$this->setData('categorie', $reservation->getCategorie());
				$this->setData('km_sup', $reservation->getKmSupp());
			}
		}
		// on doit avoir une agence par d�faut
		if (!$this->hasData('agence')) {
			$this->setData('agence', Agence::factory(getsql("select id from agence where statut&1=1 and zone='fr' and reseau='ADA' order by id limit 1")));
		}
		if (preg_match('/^\d+(\.\d+)?$/', $_REQUEST['km_sup'])) {
			$this->setData('km_sup', $_REQUEST['km_sup']);
		}
		if (preg_match('/^\d+$/', $_POST['categorie'])) {
			if ($categorie = Categorie::factory((int)$_POST['categorie'])) {
				$this->setData('categorie', $categorie);
			}
		}
	}
	public function forfait_negociationAction() {
		if (isset($_POST['forfait'])) {
			if ($forfait = Forfait::doUnserialize($_POST['forfait'])) {
				$this->setData('forfait', $forfait);
			}
		}
	}
}