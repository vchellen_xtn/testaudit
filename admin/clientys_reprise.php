<?
include('secure.php');

// on renvoie du texte et sans timeout
header('Content-Type: text/plain; charset=iso-8859-1');
set_time_limit(0);
die("STOPPED !");

// includes
include ('../constant.php');
include ('../lib/lib.util.php');

echo date('H:i:s')."\n";
$cnt = 0; 
/* 2011-09 : Reprise de l'historique
	$sql = "select f.* from facturation_ada f left join compta_clientys cc on cc.reservation=f.reservation where f.creation > '2011' and f.mode!='LP' and cc.id is null order by id";
*/
/* 2012-09-25 : reprise suite � erreur dans facture_ada pour Vente Priv�e 
 $ids = '24924, 24925';
 getsql("delete from compta_clientys where reservation in ($ids)");
 $sql = "select f.* from facturation_ada f left join compta_clientys cc on cc.reservation=f.reservation where f.reservation BETWEEN 914042 AND 921191 and cc.id is null order by f.id";
*/
/* 2012-11-19 : reprise pour les coupons vente priv�e et facturations � 0� 
//$sql = "select f.* from facturation_ada f left join compta_clientys cc on cc.reservation=f.reservation where f.creation > '2012-09-01' and f.mode!='LP' and cc.id is null order by f.id";
$sql = "select f.id, f.reservation, 'A' type, montant from facturation_ada f where f.reservation in (915078,916078,916325,916399,916451,916820,917333,917360,917920,918467,919046,920529,921038,923793,928049,933432,934480,934979,935394,936783,939500,939778,942233)";
*/
/* 2014-10-27 : r�servations faites dans RESAS par les T�l�Conseillers, la r�duction a �t� enregistr�e par erruer */
$ids = '1588268, 1602868, 1602868, 1604511, 1605812, 1605879, 1611656, 1622648, 1622743, 1623162, 1623403, 1623490, 1623611, 1624156, 1624256, 1624259, 1624417, 1624767, 1624962, 1625942, 1626036, 1626222, 1626672, 1628503, 1628632';
getsql("delete from compta_clientys where reservation in ($ids)");
$sql = "select fa.reservation, fa.type, fa.montant + coalesce(fm.montant,0) montant, fa.montant fa_montant, fm.montant fm_montant from facturation_ada fa left join facturation_materiel fm on (fa.reservation=fm.reservation and fa.type=fm.type) left join compta_clientys cc on cc.reservation=fa.reservation where fa.reservation IN ($ids) and cc.id is null order by fa.id";
$rs = sqlexec($sql);
echo mysql_numrows($rs)." facturations\n";
while ($row = mysql_fetch_assoc($rs))
{
	// on joue les �critures comptables
	$reservation = Reservation::factory($row['reservation']);
	if ($row['type']=='F') {
		$cnt += ComptaClientys::doReservation($reservation, $row['montant']);
	} else {
		$cnt += ComptaClientys::doCancel($reservation, $row['montant']);
	}
}
echo "$cnt lignes ins�r�es\n";
echo date('H:i:s')."\n";