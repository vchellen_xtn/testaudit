<?php
class PromotionForfait extends Ada_Object
{

	/**
	* factory
	* 
	* @param int $id
	* @param bool $tryArchive
	* @return PromotionForfait
	*/
	static public function factory($id, $tryArchive = false)
	{
		if (!$id) return null;
		$x = new PromotionForfait();
		if ($x->load($id)->isEmpty()) {
			// on essaye dans les archives
			$x->setTableName('_archive_promotion_forfait');
			if (!$tryArchive || $x->load($id)->isEmpty())
				return null;
			$x->setData('archive', 1);
		}
		return $x;
	}
	
	/**
	* Construit l'URL pour profiter de la promotion
	* 
	*/
	public function getURLPromo()
	{
		$url = ''; $q = array();
		if ($this->getData('categorie'))
		{
			$q['type'] = strtolower($this->getData('type'));
			$q['categorie'] = $this->getData('categorie');
		}
		if ($this->getData('agence'))
			$url = gu_recherche(array('id'=>$this->getData('agence'), 'canon'=>$this->getData('canon')), $this->getData('type'));
		else if ($this->getData('type'))
			$q['type'] = strtolower($this->getData('type'));
		if (count($q))
			$url.='?'.http_build_query($q);
		return Page::getURL($url);
	}
	/**
	* Renvoie le montant de la promotion avec le mode
	* @returns string
	*/
	public function getPromoMontant() {
		$str = show_money($this->_data['montant']);
		return $str.($this->_data['mode']=='P' ? '%' : '�');
	}
	/**
	* Renvoie les dates de la promotion
	* @resturns sting
	*/
	public function getPromoDates() {
		return PromotionEdito::createInterval($this->_data['debut'], $this->_data['fin']);
	}
	
}
?>
