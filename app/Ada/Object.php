<?

class Ada_Object implements ArrayAccess
{
	/** @var array */
	protected $_data = array();
	/** @var string */
	protected $_idFieldName = null;
	/** @var string */
	protected $_tableName = null;
	/** Setter/Getter underscore transformation cache @var array */
	protected static $_underscoreCache = array();
	protected static $_camelizeCache = array();

	// convertit une chaine en utf8
	static public function u8($string)
	{
		$string = str_replace('�', "'", $string);
		return iconv('WINDOWS-1252', 'UTF-8', $string);
	}
	/**
	* encrypte une variable
	*
	* @param mixed $data
	* @returns string
	*/
	static protected function _encryptData($data)
	{
		return base64_encode(serialize(array($data, self::_calcKey($data))));
	}

	/**
	* decrypte une cha�ne
	*
	* @param string $string
	* @returns string
	*/
	static protected function _decryptData($encrypted)
	{
		list($data, $key) = unserialize(base64_decode($encrypted));
		if ($key == self::_calcKey($data))
			return $data;
		return null;
	}

	/**
	* calcule une cl� pour un variable
	*
	* @param mixed $a
	* @returns string
	*/
	static protected function _calcKey($a)
	{
		return md5(serialize($a).PRIVATE_KEY);
	}

	/**
	* By default is looking for first argument as array and assignes it as object attributes
	* This behaviour may change in child classes
	*/
	public function __construct()
	{
		$args = func_get_args();
		if (empty($args[0])) {
			$args[0] = array();
		}
		$this->_data = $args[0];
		$this->_construct();
	}
	protected function _construct() { return; }

	/**
	* set name of object id field
	*
	* @param   string $name
	* @return  Ada_Object
	*/
	public function setIdFieldName($name) { $this->_idFieldName = $name; return $this; }

	/**
	* Retrieve name of object id field
	*
	* @param   string $name
	* @return  Ada_Object
	*/
	public function getIdFieldName() { return $this->_idFieldName ? $this->_idFieldName : 'id'; }
	protected function setTableName ($table) { $this->_tableName = $table; }
	public function getTableName ()
	{
		if (!$this->_tableName)
			$this->_tableName = $this->_underscore(get_class($this));
		return $this->_tableName; 
	}
	
	/**
	* Load from database
	* @param string $id identifiant
	* @param string $cols liste des colonnes
	* @param mixed $data to be passed to 
	*/
	public function load($id, $cols = '*', $data = null)
	{
		if (!$id) return $this;
		$sql = $this->_prepareSQL($id, $cols, $data);
		$this->_data = mysql_fetch_assoc(sqlexec($sql));
		return $this;
	}
	protected function _prepareSQL($id, $cols='*', $data=null)
	{
		if (preg_match('/^\d+$/', $id)) {
			$id = (int) $id;
		} else {
			$id = "'".addslashes($id)."'";
		}
		return "select $cols from {$this->getTableName()} where {$this->getIdFieldName()}=$id";
	}
	/*
	* Validation des champs pour l'enregistrement
	
	*/
	/**
	* V�rifie la validit� des champs
	* 
	* @param array $data donn�es � vlaider
	* @param array $errors erreurs renvoy�es
	* @param mixed $fields
	* @returns bool
	*/
	public function validateData(&$data, &$errors, $fields = null)
	{
		// on r�cup�re les r�gles
		$rules = $this->_getValidatingRules();
		if (!$rules)
		{
			$data = array();
			$errors[] = "Aucune r�gle de validation n'est fournie.";
			return false;
		}
		// la liste des champs � v�rifier : soit elle est pass�e soit tous les champs
		if ($fields && !is_array($fields))
			$fields = explode(',', $fields);
		else
			$fields = array_keys($rules);
		// puis parcourir les champs et appliquer les r�gles
		$cnt = count($errors);
		foreach ($fields as $k)
		{
			if (isset($data[$k]) || $rules[$k]['empty'])
				$data[$k] = $this->_validateRule($data[$k], $rules[$k], $errors);
		}
		return (count($errors) == $cnt);
	}
	
	protected function _validateRule(&$val, $rule, &$errors)
	{
		if ($rule)
		{
			if (is_array($val))
			{
				foreach ($val as $k => $v)
					$val[$k] = $this->_validateRule($val[$k], $rule, $errors);
			}
			else
			{
				// faire la diff�rence avec NULL apr�s filter_var()
				if (!isset($val))
					$val = '';
				else
					$val = trim($val);
				$unfiltered = $val;
				// on ne filtre pas les don�nes vides si empty est autoris�e
				if ($val!='' || $rule['empty'])
					$val = filter_var($val, $rule['filter'], $rule['options']);
				if ($val === false && $rule['filter'] != FILTER_VALIDATE_BOOLEAN)
					$val = null;
				if ($val === null && $rule['invalid'])
				{
					$errors[] = sprintf($rule['invalid'], filter_var($unfiltered, FILTER_SANITIZE_STRING));
					return $val;
				}
				if (($val===null || $val==='') && $rule['empty'])
				{
					$errors[] = sprintf($rule['empty'], filter_var($unfiltered, FILTER_SANITIZE_STRING));
					return $val;
				}
				// les booleans sont pass�s en int pour la base
				if ($rule['filter'] == FILTER_VALIDATE_BOOLEAN)
					$val = (int) $val;
			}
		}
		return $val;
	}
	/**
	* Renvoie une liste des chmaps avec les r�gles de validation � appliquer par validateArgs
	* Chaque r�gle peut contenir les champs suivants :
	* 	empty		message d'erreur si vide
	* 	invalid		message d'erreur si invalide
	* 	filter		filtre au sens de filter_var() - http://fr2.php.net/manual/en/filter.filters.php
	* 	options		options au sens de filter_var()
	* 
	*/
	protected function _getValidatingRules()
	{
		return array();
	}
	protected function _addRule($empty, $invalid, $filter, $options = null)
	{
		$a = array();
		if ($empty) $a['empty'] = $empty;
		if ($invalid) $a['invalid'] = $invalid;
		if ($filter) $a['filter'] = $filter;
		if ($options) $a['options'] = $options;
		return $a;
	}
	protected function _addRuleString($empty = null)
	{
		return $this->_addRule($empty, null, FILTER_SANITIZE_STRING);
	}
	protected function _addRuleEmail($empty, $invalid=null)
	{
		return $this->_addRule($empty, $invalid, FILTER_VALIDATE_EMAIL, FILTER_NULL_ON_FAILURE);
	}
	protected function _addRuleRegExp($regexp, $empty, $invalid)
	{
		return $this->_addRule($empty, $invalid, FILTER_VALIDATE_REGEXP, array( 'options' => array('regexp' => $regexp), 'flags' => FILTER_NULL_ON_FAILURE ));
	}
	protected function _addRulePhone($empty, $invalid)
	{
		return $this->_addRuleRegExp('/^\+?[0-9\s-\.]{10,16}$/', $empty, $invalid);
	}
	protected function _addRuleInt($empty=null, $invalid=null, $min=null, $max=null)
	{
		if ($min)
			$options['options']['min_range'] = $min;
		if ($max)
			$options['options']['max_range'] = $max;
		return $this->_addRule($empty, $invalid, FILTER_VALIDATE_INT, $options);
	}
	protected function _addRuleBoolean($empty=null)
	{
		return $this->_addRule($empty, null, FILTER_VALIDATE_BOOLEAN);
	}
	protected function _addRuleDate($empty, $invalid)
	{
		return $this->_addRuleRegExp('/^((\d{2}[\/\-]\d{2}[\/\-]\d{4})|(\d{4}[\/\-]\d{2}[\/\-]\d{2}))( \d{2}:\d{2}(:\d{2})?)?$/', $empty, $invalid);
	}
	
	
	/** enregistre l'�l�ment dans la table */
	public function save($cols = '*')
	{
		if ($cols == '*')
			save_record($this->getTableName(), $this->_data);
		else if ($this->getId())
		{
			$sql = "UPDATE ".$this->getTableName()." SET ";
			foreach(explode(',', $cols) as $k)
			{
				$v = $this->getData($k);
				if (is_null($v) || $v === '')
					$v = 'null';
				else
					$v = "'".addslashes($v)."'";
				$x[] = "$k = $v";
			}
			$sql .= join(',', $x);
			$sql.=" WHERE {$this->getIdFieldName()}='".$this->getId()."'";
			sqlexec($sql);
		}
		return $this;
	}
	/**
	* Retrieve object id
	*
	* @return mixed
	*/
	public function getId()
	{
		if ($this->getIdFieldName()) {
			return $this->getData($this->getIdFieldName());
		}
		return $this->getData('id');
	}

	/**
	* Set object id field value
	*
	* @param   mixed $value
	* @return  Ada_Object
	*/
	public function setId($value)
	{
		if ($this->getIdFieldName()) {
			$this->setData($this->getIdFieldName(), $value);
		}
		else {
			$this->setData('id', $value);
		}
		return $this;
	}

	/**
	* Overwrite data in the object.
	*
	* $key can be string or array.
	* If $key is string, the attribute value will be overwritten by $value
	*
	* If $key is an array, it will overwrite all the data in the object.
	*
	* $isChanged will specify if the object needs to be saved after an update.
	*
	* @param string|array $key
	* @param mixed $value
	* @param boolean $isChanged
	* @return Ada_Object
	*/
	public function setData($key, $value=null)
	{
		if(is_array($key)) {
			$this->_data = $key;
		} else {
			$this->_data[$key] = $value;
		}
		return $this;
	}

	/**
	* Unset data from the object.
	*
	* $key can be a string only. Array will be ignored.
	*
	* $isChanged will specify if the object needs to be saved after an update.
	*
	* @param string $key
	* @param boolean $isChanged
	* @return Ada_Object
	*/
	public function unsetData($key=null)
	{
		if (is_null($key)) {
			$this->_data = array();
		} else {
			unset($this->_data[$key]);
		}
		return $this;
	}

	/**
	* Retrieves data from the object
	*
	* If $key is empty will return all the data as an array
	* Otherwise it will return value of the attribute specified by $key
	* @param string $key
	* @return mixed
	*/
	public function getData($key='', $index = null)
	{
		if (''===$key) {
			return $this->_data;
		}
		// accept a/b/c as ['a']['b']['c']
		if (strpos($key,'/'))
		{
			$keyArr = explode('/', $key);
			$data = $this->_data;
			foreach ($keyArr as $k)
			{
				if ($k==='')
					return null;
				if (is_array($data))
				{
					if (!isset($data[$k]))
						return null;
					$data = $data[$k];
				}
				else if ($data instanceof Rnd_Object)
					$data = $data->getData($k);
				else
					return null;
			}
			return $data;
		}
		
		// cas g�n�ral
		$v = $this->_data[$key];
		if ($index && $v && is_array($v))
			return $v[$index];
		return $v;
	}

	/**
	* If $key is empty, checks whether there's any data in the object
	* Otherwise checks if the specified attribute is set.
	*
	* @param string $key
	* @return boolean
	*/
	public function hasData($key='')
	{
		if (empty($key) || !is_string($key)) {
			return !empty($this->_data);
		}
		return array_key_exists($key, $this->_data);
	}

	/**
	* Convert object attributes to array
	*
	* @param  array $arrAttributes array of required attributes
	* @return array
	*/
	public function __toArray(array $arrAttributes = array())
	{
		if (empty($arrAttributes))
			return $this->_data;

		$arrRes = array();
		foreach ($arrAttributes as $attribute)
		{
			if (isset($this->_data[$attribute]))
				$arrRes[$attribute] = $this->_data[$attribute];
			else
				$arrRes[$attribute] = null;
		}
		return $arrRes;
	}

	/**
	* Public wrapper for __toArray
	*
	* @param array $arrAttributes
	* @return array
	*/
	public function toArray(array $arrAttributes = array())
	{
		return $this->__toArray($arrAttributes);
	}

	/**
	* Convert object attributes to XML
	*
	* @param  array $arrAttributes array of required attributes
	* @param string $rootName name of the root element
	* @return string
	*/
	protected function __toXml(array $arrAttributes = array(), $rootName = 'item', $addOpenTag=false, $addCdata=true)
	{
		$xml = '';
		if ($addOpenTag) {
			$xml.= '<?xml version="1.0" encoding="UTF-8"?>'."\n";
		}
		if (!empty($rootName)) {
			$xml.= '<'.$rootName.'>'."\n";
		}
		$arrData = $this->toArray($arrAttributes);
		foreach ($arrData as $fieldName => $fieldValue)
		{
			if (is_object($fieldValue) && method_exists($fieldValue, 'toXml')) {
				$xml .= $fieldValue->toXml();
			} else if (is_array($fieldValue)) {
				continue;
			} else {
				$fieldValue = u8($fieldValue);
				if ($addCdata === true)
					$fieldValue = "<![CDATA[$fieldValue]]>";
				else
					$fieldValue = htmlspecialchars($fieldValue);
				$xml.= "<$fieldName>$fieldValue</$fieldName>"."\n";
			}
		}
		if (!empty($rootName)) {
			$xml.= '</'.$rootName.'>'."\n";
		}
		return $xml;
	}

	/**
	* Public wrapper for __toXml
	*
	* @param array $arrAttributes
	* @param string $rootName
	* @return string
	*/
	public function toXml(array $arrAttributes = array(), $rootName = 'item', $addOpenTag=false, $addCdata=true)
	{
		return $this->__toXml($arrAttributes, $rootName, $addOpenTag, $addCdata);
	}

	/**
	* Convert object attributes to JSON
	*
	* @param  array $arrAttributes array of required attributes
	* @return string
	*/
	protected function __toJson(array $arrAttributes = array())
	{
		$arrData = $this->toArray($arrAttributes);
		$lines = array();
		foreach($arrData as $fieldName => $fieldValue) {
			$line = '"'.$fieldName.'": ';
			if (is_object($fieldValue) && method_exists($fieldValue, 'toJson')) {
				$line .= $fieldValue->toJson();
			} else {
				if (is_array($fieldValue)) {
					$fieldValue = array_map('u8', $fieldValue);
				} else if (is_string($fieldValue)) {
					$fieldValue = u8($fieldValue);
				}
				$line .= json_encode($fieldValue);
			}
			$lines[] = $line;
		}
		$json = "{\n\t".implode(",\n\t", $lines)."\n}";
		return $json;
	}

	/**
	* Public wrapper for __toJson
	*
	* @param array $arrAttributes
	* @return string
	*/
	public function toJson(array $arrAttributes = array())
	{
		return $this->__toJson($arrAttributes);
	}
	/**
	* Convert object attributes to Symbolic Expression
	*
	* @param  array $arrAttributes array of required attributes
	* @return string
	*/
	protected function __toSExp(array $arrAttributes = array(), $rootName = null, $depth = 0)
	{
		$sxp = '';
		$prefix = str_repeat("\t", $depth);
		if (!empty($rootName))
		{
			$sxp.= $prefix.'('.$rootName."\n";
			$prefix = str_repeat("\t", $depth+1);
		}
		$arrData = $this->toArray($arrAttributes);
		foreach ($arrData as $fieldName => $fieldValue)
		{
			if (is_null($fieldValue))
				$fieldValue = '';
			if (is_string($fieldValue))
			{
				$fieldValue = u8(str_replace(array('\\','(',')','"',';'), array('\\\\', '\\(','\\)','\\"','\\;'), $fieldValue));
				$a = preg_split("/\n/", $fieldValue);
			} else if (is_array($fieldValue)) {
				$a = $fieldValue;
			} else {
				$a = array($fieldValue);
			}
			foreach ($a as $v)
			{
				if (is_object($v))
					$sxp .= $v->toSExp(array(), null, null, $depth+1);
				else
				{
					$v = '"'.$v.'"';
					$sxp .= $prefix."($fieldName $v)\n";
				}
			}
		}
		if (!empty($rootName))
		{
			$prefix = str_repeat("\t", $depth);
			$sxp.= $prefix.")\n";
		}
		return $sxp;
	}
	/**
	* Public wrapper for __toSExp
	*
	* @param array $arrAttributes
	* @return string
	*/
	public function toSExp(array $arrAttributes = array(), $rootName = null, $depth = 0)
	{
		return $this->__toSExp($arrAttributes, $rootName, $depth);
	}

	/**
	* Convert object attributes to string
	*
	* @param  array  $arrAttributes array of required attributes
	* @param  string $valueSeparator
	* @return string
	*/
	public function __toString()
	{
		$arrAttributes = array();
		$valueSeparator = ',';
		$arrData = $this->toArray($arrAttributes);
		return implode($valueSeparator, $arrData);
	}

	/**
	* Public wrapper for __toString
	*
	* Will use $format as an template and substitute {{key}} for attributes
	*
	* @param string $format
	* @return string
	*/
	public function toString($format='')
	{
		if (empty($format)) {
			$str = implode(', ', $this->getData());
		} else {
			preg_match_all('/\{\{([a-z0-9_]+)\}\}/is', $format, $matches);
			foreach ($matches[1] as $var) {
				$format = str_replace('{{'.$var.'}}', $this->getData($var), $format);
			}
			$str = $format;
		}
		return $str;
	}
	/**
	* Renvoie du HTML � partir d'un template
	* 
	*/
	public function toHtml($template, $args = array())
	{
		// augmenter les variables locales
		if ($args)
			extract($args);
		// on inclut le mod�le
		ob_start();
		include(BP.'/app/'.get_class($this).'/'.$template.'.php');
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}

	/**
	* Set/Get attribute wrapper
	*
	* @param   string $method
	* @param   array $args
	* @return  mixed
	*/
	public function __call($method, $args)
	{
		switch (substr($method, 0, 3)) {
			case 'get' :
				$key = $this->_underscore(substr($method,3));
				$data = $this->getData($key, isset($args[0]) ? $args[0] : null);
				return $data;

			case 'set' :
				$key = $this->_underscore(substr($method,3));
				$result = $this->setData($key, isset($args[0]) ? $args[0] : null);
				return $result;

			case 'uns' :
				$key = $this->_underscore(substr($method,3));
				$result = $this->unsetData($key);
				return $result;

			case 'has' :
				$key = $this->_underscore(substr($method,3));
				return isset($this->_data[$key]);
		}
		throw new Exception("Invalid method ".get_class($this)."::".$method."(".print_r($args,1).")");
	}

	/**
	* Attribute getter
	*
	* @param string $var
	* @return mixed
	*/
	public function __get($var)
	{
		$var = $this->_underscore($var);
		return $this->getData($var);
	}

	/**
	* Attribute setter
	*
	* @param string $var
	* @param mixed $value
	*/
	public function __set($var, $value)
	{
		$var = $this->_underscore($var);
		$this->setData($var, $value);
	}

	/**
	* checks whether the object is empty
	*
	* @return boolean
	*/
	public function isEmpty()
	{
		if (empty($this->_data)) {
			return true;
		}
		return false;
	}

	/**
	* Converts field names for setters and geters
	*
	* $this->setMyField($value) === $this->setData('my_field', $value)
	* Uses cache to eliminate unneccessary preg_replace
	*
	* @param string $name
	* @return string
	*/
	protected function _underscore($name)
	{
		if (isset(self::$_underscoreCache[$name])) {
			return self::$_underscoreCache[$name];
		}
		$result = strtolower(preg_replace('/(.)([A-Z])/', "$1_$2", $name));
		self::$_underscoreCache[$name] = $result;
		return $result;
	}

	protected function _camelize($name)
	{
		return uc_words($name, '');
	}

	/**
	* Tiny function to enhance functionality of ucwords
	*
	* Will capitalize first letters and convert separators if needed
	*
	* @param string $str
	* @param string $destSep
	* @param string $srcSep
	* @return string
*/
	static protected function uc_words($str, $destSep='_', $srcSep='_')
	{
		return str_replace(' ', $destSep, ucwords(str_replace($srcSep, ' ', $str)));
	}

	/**
	* Implementation of ArrayAccess::offsetSet()
	* @link http://www.php.net/manual/en/arrayaccess.offsetset.php
	* 
	* @param string $offset
	* @param mixed $value
	*/
	public function offsetSet($offset, $value)
	{
		$this->_data[$offset] = $value;
	}
	public function offsetExists($offset)
	{
		return isset($this->_data[$offset]);
	}
	public function offsetUnset($offset)
	{
		unset($this->_data[$offset]);
	}
	public function offsetGet($offset)
	{
		return isset($this->_data[$offset]) ? $this->_data[$offset] : null;
	}
}
?>
