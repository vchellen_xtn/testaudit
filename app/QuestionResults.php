<?php
class QuestionResults extends Ada_Object
{
	static public $FIELDS = array('zone','agence','pdv','code_societe','reseau','type','categorie');
	
	protected $_totalSend = -1;
	protected $_total = -1;
	protected $_results = null;
	protected $_sqlResults = null;
	
	static public function factory($args)
	{
		// filtrer les param�tres et les valider
		$a = array();
		foreach(array('date_from','date_to') as $k) {
			if (isset($args[$k]) && preg_match('/^\d{2,4}\-\d{2}\-\d{2,4}( \d{2}(\:\d{2}){0,2})?$/', $args[$k])) {
				$a[$k] = $args[$k];
			}
		}
		// ils peuvent �tre sous forme de tableau
		foreach(self::$FIELDS as $k) {
			if (isset($args[$k])) {
				$v = $args[$k];
				if (is_array($v)) {
					$b = array();
					foreach($v as $u) {
						if ($u = self::checkField($k, $u)) {
							$b[] = $u;
						}
					}
					if (count($b)) {
						$a[$k] = $b;
					}
				} else {
					if ($v = self::checkField($k, $v)) {
						$a[$k] = $v;
					}
				}
			}
		}
		$x = new QuestionResults($a);
		return $x;
	}
	static public function checkField($field, $value) {
		switch($field) {
			case 'zone':
				if (preg_match('/^[a-z]{2}$/', $value)) {
					return $value;
				}
				break;
			case 'agence':
			case 'pdv':
				if (Agence::isValidID($value)) {
					return $value;
				}
				break;
			case 'code_societe':
				if (preg_match('/^[a-zA-Z0-9]{2}$/', $value)) {
					return $value;
				}
				break;
			case 'reseau':
				if ($value == 'ADA') {
					return $value;
				}
				break;
			case 'type':
				$value = strtolower($value);
				if (isset(Categorie::$TYPE[$value])) {
					return $value;
				}
				break;
			case 'categorie':
				if (preg_match('/^\d{1,3}$/', $value)) {
					return (int)$value;
				}
			default:
				return null;
		}
	}
	
	public function getTotalSend()
	{
		if ($this->_totalSend < 0)
		{
			// combien de personnes interrog�s ?
			$sql = "select count(*)";
			$sql.=" from reservation r";
			$sql.=" join agence a on a.id=r.agence";
			$sql.=" where r.fin between '".addslashes(euro_iso($this->_data['date_from']))."'and '".addslashes(euro_iso($this->_data['date_to']))."'";
			$sql.=" and r.relance > r.fin";
			foreach(self::$FIELDS as $k)
			{
				if ($v = $this->_data[$k])
				{
					if (in_array($k, array('zone','code_societe','reseau')))
						$sql .= " and a.";
					else
						$sql .= " and r.";
					if (is_array($v))
						$sql.= $k." in ('".join("','", $v)."')";
					else
						$sql.= $k."='".addslashes($v)."'";
				}
			}
			$this->_totalSend = getsql($sql);
		}
		return $this->_totalSend;
	}
	public function getTotal()
	{
		if ($this->_total < 0)
		{
			$this->getResults();
		}
		return $this->_total;
	}
	
	public function getResults()
	{
		if (is_null($this->_results))
		{
			// SQL avec jonture sur question_user pour avoir tous les items m�mes ceux � z�ro
			$rs = sqlexec($this->getSQLResults());
			$results = array();
			$first_q_id = $total = null;
			while ($row = mysql_fetch_assoc($rs))
			{
				if (!$results[$row['theme']][$row['q_id']])
				{
					if (!$first_q_id) $first_q_id = $row['q_id'];
					$results[$row['theme']][$row['q_id']]['question'] = $row['question'];
					$results[$row['theme']][$row['q_id']]['type'] = $row['q_type'];
				}
				if ($row['choix'])
					$results[$row['theme']][$row['q_id']]['choix'][$row['choix']] = $row['nb'];
				$results[$row['theme']][$row['q_id']]['total'] += $row['nb'];
				// calculer le total des participants sur la premi�re question
				if ($row['q_id'] == $first_q_id)
					$total += $row['nb'];
			}
			$this->_results = $results;
			$this->_total = $total;
		}
		return $this->_results;
	}
	/**
	* On calcule la note pour un theme en prenant toutes les questions
	* 	pas du tout satisfait	0
	*   pas satisfait			1
	*   satisfait				3
	*   tr�s satisfait			4
	* et en divisant par le total des r�ponses
	* @param string $test le theme
	* @returns double la note
	*/
	public function getThemeNote($test)
	{
		foreach($this->getResults() as $theme => $questions)
		{
			if ($theme == $test)
			{
				$score = 0; $total = 0;
				foreach($questions as $question)
				{
					$total += $question['total'];
					foreach (array_values($question['choix']) as $k => $nb)
						$score += ($k > 1 ? $k+1 : $k) * $nb;
				}
				return ($total > 0 ? round($score / $total, 2) : null);
			}
		}
		return null;
	}
	
	
	public function getSQLResults()
	{
		if (!$this->_sqlResults)
		{
			$sql = "select q.theme, q.libelle question, i.libelle choix, sum(case when u.reservation is null or (q.type='O' and r.txt is null) then 0 else 1 end) nb, q.id q_id, q.type q_type";
			$sql.=" from question_question q";
			$sql.=" join question_item i on i.question=q.id";
			$sql.=" left join question_reponse r on r.item=i.id";
			$sql.=" left join question_user u on (u.reservation=r.reservation";
			$sql.=" and u.fin between '".addslashes(euro_iso($this->_data['date_from']))."'and '".addslashes(euro_iso($this->_data['date_to']))."'";
			foreach(self::$FIELDS as $k)
			{
				if ($v = $this->_data[$k])
				{
					if (is_array($v))
						$sql.=" and u.$k in ('".join("','", $v)."')";
					else
						$sql.=" and u.$k='".addslashes($v)."'";
				}
			}
			$sql.= ')';
			$sql.=" where q.questionnaire=1";
			$sql.=" group by q.id, q.type, q.theme, q.libelle, i.libelle, q.position, i.position";
			$sql.=" order by q.position, i.position";
			// assigner � la variable de la classe
			$this->_sqlResults = $sql;
		}
		return $this->_sqlResults;
	}
	public function getSQLComments($q_id)
	{
		return preg_replace(
			'/^select (.+) from (.+) where (.+) group by (.+) order by (.+)$/i', 
			'select u.reservation, r.question, u.agence, u.pdv, u.type, u.categorie, r.creation, r.txt, ap.nom, c.prenom,'
				.' coalesce(ac.publie,0)  publie, ac.refnat'
			.' from \2'
			.' join reservation on reservation.id=u.reservation'
			.' join client c on c.id=reservation.client_id'
			.' left join agence_pdv ap on ap.id=u.pdv'
			.' left join agence_commentaires ac on (ac.reservation=r.reservation and ac.question=r.question)'
			.' where \3'
				.' and r.question='.(int)$q_id
				.' and r.txt is not null'
				.' and u.reservation is not null'
			.' order by r.creation desc',
			$this->getSQLResults()
		);
	}
	public function getSQLExportResults()
	{
		$total = $this->getTotal();
		return str_replace(', q.id q_id, q.type q_type', ", concat(round((100 * sum(case when u.reservation is null or (q.type='O' and r.txt is null) then 0 else 1 end)) / $total, 2), '%') pourcentage", $this->getSQLResults());
	}
	public function getSQLExportByAgence()
	{
		return preg_replace(
			'/^select (.+) from (.+) where (.+) group by (.+) order by (.+)$/i'
			,'select u.code_societe, u.pdv,'
				.' count(distinct(u.reservation)) reponses,'
				.' count(r.txt) commentaires,'
				.' round(avg(case when q.theme="l\'agence" then (case when i.position > 2 then i.position else i.position-1 end) else null end),2) agence,'
				.' round(avg(case when q.theme="le v�hicule" then (case when i.position > 2 then i.position else i.position-1 end) else null end),2) vehicule,'
				." concat(round((100*sum(case when q.id=8 then 2-i.position else 0 end))/count(distinct(u.reservation)), 2),' %') rachat_franchise,"
				.' ap.nom'
			.' from \2'
			.' left join agence_pdv ap on ap.id=u.pdv'
			.' where \3'
				.' and (q.theme in ("l\'agence","le v�hicule") or q.id=8 or r.txt is not null)'
				.' and u.reservation is not null'
			.' group by u.code_societe, u.pdv, ap.nom'
			,$this->getSQLResults()
		);
	}
	public function getSQLExportByAgenceQuestion()
	{
		return preg_replace(
			'/^select (.+) from (.+) where (.+) group by (.+) order by (.+)$/i'
			,'select u.code_societe, u.pdv, q.theme, q.libelle,'
				.' count(distinct(u.reservation)) nombre,'
				.' round(avg(case when q.id!=8 then (case when i.position > 2 then i.position else i.position-1 end) else null end),2) note,'
				." concat(round((100*sum(case when q.id=8 then 2-i.position else null end))/count(distinct(u.reservation)), 2),' %') pourcentage,"
				.' ap.nom'
			.' from \2'
			.' left join agence_pdv ap on ap.id=u.pdv'
			.' where \3'
				.' and (q.theme in ("l\'agence","le v�hicule") or q.id=8)'
				.' and u.reservation is not null'
			.' group by u.code_societe, u.pdv, ap.nom, q.theme, q.libelle'
			,$this->getSQLResults()
		);
	}
	public function getSQLExportComments()
	{
		return preg_replace(
			'/^select (.+) from (.+) where (.+) group by (.+) order by (.+)$/i'
			, 'select q.libelle, u.agence, ap.nom, ucase(u.type) type, c.mnem, r.creation, ifnull(ac.publie,0) publie, replace(r.txt,"&#39;","\'") commentaire, ac.refnat commentaire2'
				.' from \2'
				.' left join categorie c on c.id=u.categorie'
				.' left join agence_pdv ap on ap.id=u.pdv'
				.' left join agence_commentaires ac on ac.reservation=u.reservation and ac.question=q.id'
				.' where \3 and q.type=\'O\' and r.txt is not null and u.reservation is not null'
				.' order by q.position, r.creation desc'
			, $this->getSQLResults()
		);
	}
}
?>
