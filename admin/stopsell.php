<?
	require_once("secure.php"); 
	require_once("lib.php");

	// identifiant
	$id = (int) getarg('id');
	
	// suppression
	if ($id && getarg('delete')) 
	{
		$sql = 'delete from stop_sell where id = '.$id;
		getsql($sql);
		unset($id);
	}
	
	// modification, cr�ation
	if (getarg('update') || getarg('create'))
	{
		$_POST['zone'] = $_SESSION['ADA001_ADMIN_ZONE'];
		if ($id)
			save_record('stop_sell', $_POST);
		else
		{
			// un stop sell pour chaque cat�gorie
			$a = $_POST;
			$categories = $_POST['categorie'];
			if (!is_array($categories))
				$categories = array('');
			if (count($categories)==1)
			{
				$a['categorie'] = $categories[0];
				save_record('stop_sell', $a);
				$id = mysql_insert_id();
			}
			else
			{
				echo "Cr�ation de ".count($categories)."\n<ol>\n";
				foreach ($categories as $c)
				{
					$a['id'] = '';
					$a['categorie'] = $c;
					save_record('stop_sell', $a);
					$id = mysql_insert_id();
					echo '<li><a href="stopsell.php?id='.$id.'">#'.$id.' - '.Categorie::factory($c)->getData('nom')."</li>\n";
				}
				echo "</ol>";
				exit();
			}
		}
	}

	if ($id)
	{
		$rs = sqlexec('select * from stop_sell where id = '.$id);
		if (!$rs || !($row = mysql_fetch_assoc($rs)))
			die("Aucun enregistrement correspondant.");
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script type="text/javascript" src="<?=Page::getProtocol()?>://www.google.com/jsapi"></script>
	<script type="text/javascript">
		google.load('jquery', '1.4');
		//google.load('jqueryui', '1.7');
	</script>
	<link rel="stylesheet" type="text/css" href="../css/anytime.min.css" />
	<script type="text/javascript" src="../scripts/anytime.min.js"></script>
	<script type="text/javascript" src="../scripts/script.js"></script>
	<script type="text/javascript" src="admin.js"></script>
	<script>
	// <![CDATA[
	// doDelete
	function doDelete(frm)
	{
		return (confirm("Voulez-vous r�ellement supprimer cet �l�ment ?\nCette op�ration est irr�versible."));
	}
	
	// doSubmit
	function doSubmit(frm)
	{
		var err = "";
		var cntCat = countCheckboxes('categorie[]', frm);
		if (frm.nb && !frm.nb.value.match(/^\d*$/))
			err += "Veuillez indiquer un nombre valide !\n";
		// on ne peut pr�ciser un nombre que pour une cat�gorie et une agence...
		else if (frm.agence && frm.nb.value.match(/^\d+$/))
		{
			if (frm.agence.selectedIndex < 1)
				err += "Vous devez pr�ciser une agence pour indiquer une disponibilit� restreinte\n";
			if (cntCat == 0)
				err += "Vous devez pr�ciser au moins une cat�gorie pour indiquer une disponibilit� restreinte\n";
			if (frm.forfait_prefix.selectedIndex > 0 || frm.forfait.selectedIndex > 0)
				err += "Vous ne pouvez pas pr�ciser de forfaits si vous indiquer une disponibilit� restreinte\n";
		}
		// pr�venir pour les stop sell larges
		if(frm.agence && frm.agence.selectedIndex == 0 && cntCat==0 
			&& frm.forfait_prefix.selectedIndex == 0 && frm.forfait.selectedIndex == 0)
		{
			if (!confirm("Vous cr�ez un stop sell valable pour l'ensemble des cat�gories et des agences.\nCeci revient � emp�cher toute op�ration dans la p�riode indiqu�e.\n Confirmez-vous ?"))
				return false;
		}
		if(!frm.fin.value.length)
		{
			if (!confirm("Vous cr�ez un stop sell sans date de fin.\nCeci revient � emp�cher toute op�ration dans l'avenir pour l'agence et la cat�gorie indiqu�es.\n Confirmez-vous ?"))
				return false;
		}
		if (!err.length) return true;
		alert("Attention !\n" + err);
		return false;
	}

	// initilisation AnyTime
	var dateFormat = '%d-%m-%Y %H:%i';
	var dateOptions = getOptionsAnyTime(dateFormat, "D�but");
	var dateConverter = new AnyTime.Converter({format: dateFormat});

	// onload
	$(document).ready(function()
	{
		AnyTime.picker('debut', dateOptions);
		dateOptions.labelTitle = "Fin";
		AnyTime.picker('fin', dateOptions);
		$('#clear_debut').click(function(e){$('#debut').val('');});
		$('#clear_fin').click(function(e){$('#fin').val('');});
		// les mises � jour de listes
		$("#reseau, #groupe").change(function(){
			updateSelect(this.form.code_societe);
		});
		$("#reseau, #groupe, #code_societe").change(function(){
			updateSelect(this.form.agence);
		});
		$("#type, #forfait_prefix").change(function(){
			updateSelect(this.form.forfait);
		});
		$('#type').change(function(){
			$('tr.tr_type').hide();
			$('tr.tr_type :checkbox').each(function()
			{
				this.checked = false;
			});
			if (v = $(this).val())
				$('#type_'+v).show();
		});
		// on met � jour au chargement
		var frm = document.forms[0];
		if (frm.agence)
			updateSelect(frm.agence, "<?=filter_var($_GET['agence'], FILTER_SANITIZE_STRING)?>");
		if (frm.code_societe)
			updateSelect(frm.code_societe, null);
		if (frm.forfait)
			updateSelect(frm.forfait, "<?=filter_var($_GET['forfait'], FILTER_SANITIZE_STRING)?>");
	});
	// ]]>
	</script>
	<style>
		#debut, #fin, #clear_debut, #clear_fin { width: inherit; }
	</Style>
</head>
<body>
<form action="" method="post">
<input type="hidden" name="id" value="<?=$id ? $id : ''?>">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre">
		<img src="img/puce.gif" align="absmiddle" hspace="1">
		<?=($id ? "Stop-Sell [#".$id."] <strong>[".ucwords($row['provenance']).']</strong>' : "Nouveau Stop-Sell");?>
	</th>
</tr>
<tr><td>
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="10%">
		<col width="40%">
		<col width="10%">
		<col width="40%">
		<tr><th colspan="4">Dates</th></tr>
		<tr>
			<td><label for="debut">D�but</label></td>
			<td>
				<input autocomplete="off" type="text" name="debut" id="debut" value="<?=substr(euro_iso($row ? $row['debut'] : date('Y-m-d 00:00:00')), 0, -3);?>" maxlength="16" size="16"/>
				<input type="button" id="clear_debut" value="effacer"/>
			</td>
			<td><label for="fin">Fin</label></td>
			<td>
				<input autocomplete="off" type="text" name="fin" id="fin" value="<?=substr(euro_iso($row ? $row['fin'] : date('Y-m-d 23:59:59')), 0, -3)?>" maxlength="16" size="16"/>
				<input type="button" id="clear_fin" value="effacer"/>
			</td>
		</tr>
		<tr><th colspan="4">Quelles agences ?</th></tr>
		<tr>
			<td><label for="reseau">R�seau</label></td>
			<td>
			<?
				if (!$id)
					selectarray('reseau', array_combine(Agence::$RESEAU, Agence::$RESEAU), '', 'Tous');
				else
					echo $row['reseau'] ? $row['reseau'] : '-';
			?>
			</td>
			<td><label for="groupe">Groupe</label></td>
			<td>
			<?
				if (!$id)
					selectarray('groupe', ZoneGroupe_Collection::factory($_SESSION['ADA001_ADMIN_ZONE']), '', 'Tous');
				else
					echo ($row['groupe'] ? ZoneGroupe::factory($row['groupe'])->getData('nom') : '-');
			?>
			</td>
		</tr>
		<tr>
			<td><label for="code_societe">Soci�t�</label></td>
			<td>
			<?
				if (!$id)
					selectarray('code_societe', array(), '', 'Toutes');
				else
					echo ($row['code_societe'] ? getsql("select concat(code_societe, ' ', nom) from agence where code_societe='".addslashes($row['code_societe'])."' limit 1") : '-');
			?>
			</td>
			<td><label for="agence">Agence</label></td>
			<td>
			<?
				if (!$id)
					selectarray('agence', array(), '', 'Toutes');
				else if ($row['agence'])
				{
					$agence = Agence::factory($row['agence']);
					echo '<a href="agence_pdv.php?id='.$agence['id'].'">'.$agence['id'].' '.$agence['nom'].'</a>';
				}
				else
					echo '-';
			?>
			</td>
		</tr>
		<tr><th colspan="4">Quels V�hicules et Forfaits ?</th></tr>
		<tr>
			<td><label for="type">Type</label></td>
			<td>
			<? 
				if (!$id)
					selectarray('type', Categorie::$TYPE, $_GET['type'], 'Tous les types');
				else if ($row['type'])
					echo strtoupper($row['type']).' - '.Categorie::$TYPE[strtolower($row['type'])];
				else
					echo 'Toutes';
			?>
			</td>
			<? if ($id) :?>
			<td><label for="categorie">Cat�gorie</label></td>
			<td><? echo ($row['categorie'] ? Categorie::factory($row['categorie']) : 'Toutes'); ?></td>
			<? else : ?>
			<td colspan="2">Si aucune cat�gorie s�lectionn�e, elles seront totues concern�es</td>
			<? endif; ?>
		</tr>
		<?
		// afficher la liste des cat�goires pour chauqe type
		if (!$id)
		{
			foreach(Categorie::$TYPE as $k => $v)
			{
				echo '<tr class="tr_type" id="type_'.$k.'" style="display: none;">';
				echo '<td></td>';
				echo '<td colspan="3">';
				foreach(Categorie_Collection::factory('fr', $k) as $x)
				{
					echo '<input type="checkbox" name="categorie[]" id="categorie_'.$x['id'].'" value="'.$x['id'].'"/>';
					echo '<label for="categorie_'.$x['id'].'" title="'.(string)$x.'">'.$x['mnem'].'</label>';
				}
				echo '<br/>';
				echo '<a href="#" onclick="return doCheckbox(this, true);">cocher</a>&nbsp;&nbsp;';
				echo '<a href="#" onclick="return doCheckbox(this, false);">d�cocher</a>';
				echo '</td>';
				echo '</tr>';
			}
		}
		?>
		<tr>
			<td><label for="forfait_prefix">Pr�fixe</label></td>
			<td>
			<?
				if (!$id)
				{
					$sql = "SELECT DISTINCT CONCAT(LEFT(id, case when instr(id,'ki')>0 then 6 else 4 end),'%') k, CONCAT(UCASE(type), ' ',LEFT(id, case when instr(id,'ki')>0 then 6 else 4 end)) v FROM forfait2 ORDER BY v";
					selectsql ('forfait_prefix', $sql, $row ? $row["forfait_prefix"] : $_GET["forfait_prefix"], 'Tous les forfaits');
				}
				else
					echo ($row['forfait_prefix'] ? substr($row['forfait_prefix'], 0, -1) : '-');
			?>
			</td>
			<td><label for="forfait">Forfait</label></td>
			<td>
			<?
				if (!$id)
					selectarray('forfait', array(), null, 'Tous');
				else
					echo ($row['forfait'] ? getsql("select concat(id,' ', nom) from forfait2 where id='".addslashes($row['forfait'])."'") : '-');
			?>
			</td>
		</tr>
		<? if (!$row['forfait'] && !$row['forfait_prefix']) : ?>
		<tr><th colspan="4">Jauges</th></tr>
		<tr>
			<td><label for="nb">Dispo.</label></td>
			<td colspan="3">
				<input type="text" id="nb" name="nb" value="<?=$row['nb']?>" size="3" maxlength="2"/>
				Pr�ciser une jauge inf�rieure � la jauge de la cat�gorie
			</td>
		</tr>
		<? endif; ?>
		<?
		if ($id)
		{
			echo '<tr>';
			foreach (array('creation'=>'cr�ation','modification'=>'modification') as $k => $v)
				echo '<td>'.$v.'</td><td>'.euro_iso($row[$k]).'</td>';
			echo "</tr>\n";
		}
		?>
		</table>
	</td>
</tr>
<tr>
	<td align="right">
		<input type="submit" name="<?=$id ? "update" : "create"?>" value="enregistrer" onclick="return doSubmit(this.form);">
<?
	if ($id) 
	{
?>
		&nbsp;
		<input type="submit" onclick="return doDelete(this.form);" name="delete" value="supprimer">
<?
	}
?>
	</td>
</tr>
</table>
</form>
</body>
</html>
