<?
class ResInfo extends Ada_Object
{
	/** @var PartenaireConducteur */
	protected $_conducteur;
	
	/**
	* Cr�e un ResInfo � partir d'une r�servation
	* 
	* @param Reservation $reservation
	* @return ResInfo
	*/
	static public function factory(Reservation $reservation)
	{
		$x = new ResInfo();
		$x->load($reservation->getId());
		if ($x->isEmpty()) {
			$x->setData('reservation', $reservation->getId());
		}
		return $x;
	}
	public function _construct()
	{
		parent::_construct();
		$this->setIdFieldName('reservation');
	}
	protected function _prepareSQL($id, $cols='*', $data=null)
	{
		if (!is_numeric($id))
			$id = "'".addslashes($id)."'";
		return "select $cols, p.nom f_pays_nom from {$this->getTableName()} left join pays p on p.id=f_pays where {$this->getIdFieldName()}=$id";
	}
	/**
	* Renvoie le conducteur associ�e � la r�servation s'il existe
	* @returns PartenaireConducteur
	*/
	public function getConducteur() {
		if ($this->_data['conducteur'] && !$this->_conducteur) {
			$this->_conducteur = PartenaireConducteur::factory($this->_data['conducteur']);
		}
		return $this->_conducteur;
	}
	public function fixEncoding()
	{
		$fields = array();
		foreach(array('commentaire','commentaire_client','f_societe','f_adresse','f_adresse2','f_cp','f_ville') as $k) {
			if (isset($this->_data[$k])) {
				if (mb_detect_encoding($this->_data[$k], 'ASCII,UTF-8,ISO-8859-1', true) == 'UTF-8') {
					$fields[] = $k;
					$this->_data[$k] = iconv('UTF-8', 'WINDOWS-1252//TRANSLIT', $this->_data[$k]);
				}
			}
		}
		if ($fields) {
			$this->save(implode(',', $fields));
			return true;
		}
		return false;
	}
}
?>
