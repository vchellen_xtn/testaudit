<?
// $Id: lib.tcp.php,v 1.13 2012/03/23 14:16:24 gdubourguet Exp $
	define ("DEBUG", false);

	// get_http_request
	function get_http_request($server, $url, $auth=null, &$cookie=null, $verb="GET", $type="application/x-www-form-urlencoded", &$http_status=null)
	{
		// protocole, serveur et port
		if (preg_match("/((https*):\/\/)?([^:\/]+)\:?(\d+)?/", $server, $m))
			list(,,$protocol, $server, $port) = $m; 

		// par d�faut
		if (!$protocol)
			$protocol = "http";
		if (!$port)
			$port = ($protocol == 'https') ? 443 : 80;

		// les donn�es en POST ou en GET
		if ($verb == "POST")
			list($url, $data) = explode('?', $url);

		// les donn�es
		$req = $verb." ".$url." HTTP/1.0\r\nHOST: ".str_replace("ssl://", "", $server)."\r\n";
		if ($verb == "POST")
		{
			$req.= "Content-length: ".strlen($data)."\r\n";
	   		$req.= "Content-type: application/$type\r\n";
		}
		if ($auth)
			$req.= "Authorization: Basic ".base64_encode($auth)."\r\n";
		if ($cookie)
			$req.= "Cookie: ".trim($cookie)."\r\n";

		$req.= "Referer: $protocol://$server/\r\n";
   		$req.= "Connection: keep-alive\r\n";
		$req.= "\r\n";
		$req.= $data;

		if ($protocol == 'https')
			$server = "ssl://".$server;

		$content = get_request ($server, $req, $port);
	
		if (preg_match("/Set-Cookie: (.+)/", $content, $m))
			$cookie = $m[1];

		list($headers, $content) = explode("\r\n\r\n", $content);
		if (preg_match("/\s+(.+)/", $headers, $m))
			$http_status = trim($m[1]);

		return $content;
	}

	// get_request
	// renvoie les résultats d'une requ�te
	function get_request($server, $req, $port=80, $timeout=15)
	{
		if (DEBUG)
		{
			echo("Server : ".$server." (".$port.")\n");
			echo($req."\n\n");
		}

		if (!$server || !$port)
			return null;
		$fp = @fsockopen($server, $port, $errno, $errstr, $timeout);

		// serveur indisponible, continuer quand m�me
		if (!$fp) 
		{
			if (DEBUG)
				echo "Impossible de contacter le serveur : ".$server." (timeout=".$timeout.")\n";
			return null;
		}

		socket_set_timeout($fp, $timeout);
		fwrite($fp, $req);
		$status = socket_get_status($fp);

		$content = "";
		while (!$status['timed_out'] && !feof($fp)) 
		{
			$ln = fgets($fp, 128);
			if (DEBUG)
				echo($ln);
			$content .= $ln;
			$status = socket_get_status($fp);
		}
		if (DEBUG)
		{
			echo ("- Status:\n");
			print_r($status);
			echo ("- Returning:\n");
		}
		fclose($fp);
		return ($content);
	}
?>