<?php
class RefnatCanon extends Ada_Object
{
	static public $TYPES_GEO = array('agence','agglo','dept','region','emplacement','zone');
	static public $TYPES = array
	(
		'zone'		=> array(
			'title'	=> 'Zone',
			'tbl'	=> 'zone',
			'footer'=> true
		),
		'emplacement'=> array(
			'title'	=> 'Lieux',
			'tbl'	=> 'agence_emplacement',
			'footer'=> true
		),
		'region'	=> array(
			'title'	=> 'R�gion',
			'tbl'	=> 'region',
			'cond'	=> "zone='fr'",
		),
		'dept'		=> array(
			'title'	=> 'D�partement',
			'tbl'	=> 'dept',
			'cond'	=> "zone='fr'",
			'footer'=> true
		),
		'agglo'		=> array(
			'title'	=> 'Agglo.',
			'tbl'	=> 'agence_agglo',
			'footer'=> true
		),
		'agence'	=> array(
			'title'	=> 'Agence',
			'tbl'	=> 'agence_alias',
			'cond'	=> '(statut & 1)=1',
			'footer'=> true
		),
		'type'		=> array(
			'title'	=> 'Type',
			'tbl'	=> 'vehicule_type'
		),
		'categorie'	=> array(
			'title'	=> 'Cat�gorie',
			'tbl'	=> 'categorie',
			'cond'	=> 'publie=1'
			
		),
		'marque'	=> array(
			'title'	=> 'Marque',
			'tbl'	=> 'vehicule_marque'
		),
		'modele'	=> array(
			'title'	=> 'Mod�le',
			'tbl'	=> 'vehicule_modele'
		),
	);
	
	static public function isGeographic($type)
	{
		return in_array($type, self::$TYPES_GEO);
	}
	static public function hasRefnat($type)
	{
		return true;
	}
	static public function hasFooter($type)
	{
		if (isset(self::$TYPES[$type]))
			return self::$TYPES[$type]['footer'];
		return false;
	}
}
?>
