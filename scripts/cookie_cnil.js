var cookieCNIL = {};
(function($) {
	cookieCNIL = {
		has_cookie: function(){ return !!(/cookie_cnil=1/.exec(document.cookie)); },
		init: function(options) {
			var defaults = {
				domain: document.location.hostname.split('.').slice(-2).join('.'),
				text: "En poursuivant votre navigation sur ce site, vous acceptez l'utilisation de cookies",
				url: null,
				css: {
					background: '#fff',
					color: '#333',
					position: 'absolute',
					zIndex: '1000',
					border:'1px solid #666',
					padding: '20px',
					font: '1em arial',
					marginLeft: 'auto',
					marginRight: 'auto',
					left: 0,
					right: 0,
					textAlign: 'center',
					width: '80%'
				}
			};
			if (typeof(options) === 'object' && typeof(options.css) === 'object') {
				options.css = $.extend(defaults.css, options.css);
			}
			options = $.extend(defaults, options);
			// il y a un cookie, on ne fait rien
			if (cookieCNIL.has_cookie()) {
				return;
			}
			// on append la popin
			var html = '<div id="cookie_cnil">'
				+ '<a href="#" id="btn_cookie_cnil_close" style="position:absolute;font-size:1.4em;top:5px; right:10px;color:'+options.css.color+';text-decoration:none;font-weight:bold;">&times;</a>'
				+ '<span>' + options.text + '</span><br />'
				+ (options.url ? '<a href="' + options.url + '" style="color:'+options.css.color+';text-decoration:none;font-weight:bold;">En savoir plus</a> &mdash; ' : '')
				+ '<a href="#" id="btn_cookie_cnil_valid" style="color:'+options.css.color+';text-decoration:none;font-weight:bold;">J\'accepte</a>'
				+ '</div>';
			$(document.body).prepend(html);
			$('#cookie_cnil').css(options.css);
			$('#btn_cookie_cnil_valid, #btn_cookie_cnil_close').click(function() {
				if (options.domain.match(/^\w+$/)) {
					document.cookie = 'cookie_cnil=1; expires=Fri, 1 Jan 2100 05:43:11 UTC';
				} else {
					document.cookie = 'cookie_cnil=1; path=/; domain=' + options.domain + '; expires=Fri, 1 Jan 2100 05:43:11 UTC';
				}
				$('#cookie_cnil').remove();
				return false;
			});
		}
	}
})(jQuery);