<?

// includes
include_once (BP.'/lib/lib.tcp.php');
include_once (BP.'/admin/lib.agence.php');

/**
* CronADA
* $cron = new CronADA(dirname(__file__).'/fichiers/logs/');
* $cron->run();
*/
class CronADA extends CronTask
{
	private $operations = null;
	/**
	* @param string $logpath chemin vers les logs
	* @param array $options tableau des options
	* @return CronADA
	*/
	function __construct ($logpath, $operations = null)
	{
		$this->operations = ($operations && is_array($operations) && count($operations) > 0) ? $operations : null;
		parent::__construct($logpath);
	}
	/* doit-on ex�cuter un �l�ment du process */
	protected function isAllowed($action)
	{
		// si les options ne sont pas pr�cis�s toutes les actions sont autoris�s sinon on v�rifie
		return (!$this->operations || in_array($action, $this->operations));
	}
	/* impl�mentations abstract */
	protected function getEmailTpl() { return BP.'/emails/em-cron.html'; }
	protected function work()
	{
		$this->secondVoucher(); // envoie un rappel pour les r�servations � venir
		$this->notifyQuestionnaire();
		// avec UNIPRO
		if (!UNIPRO_CONNECT) return;
		try
		{
			$unipro = @new Unipro(); 
			if (!$unipro || !$unipro->login())
			{
				$this->error('Impossible de se connecter � UNIPRO: '.($unipro ? $unipro->getLastError() : 'cr�ation impossible'));
			}
		}
		catch (Exception $e)
		{
			$this->error('Erreur lors de la connexion � Unipro: '.(string) $e); 
		}
		if ($unipro)
		{
			$this->msg('Connexion UNIPRO: '.$unipro->getSessionID());
			$this->uniproFix($unipro); // renvoie � UNIPRO si n�cessaire
			$this->uniproImport($unipro); // importe les donn�es en provenance d'UNIPRO
		}
		$this->updateGeoLoc();
		$this->updateRefNat();
		$this->satisfactionPointDeVente();
		$this->archiveHisto();
		$this->notifyAgenceOfCustomers();
	}
	/* fonctinos private */
	/**
	* rappeler la r�servation 48h avant
	* pour toutes les r�servations sans livraison � domicile
	*/
	private function secondVoucher()
	{
		if (!$this->isAllowed('2ndvoucher')) return;
		$this->msg('secondVoucher', 'I', true);
		$sql = "select u.email, u.titre civilite, u.nom, u.pgkm, r.id, r.agence, r.forfait_type, r.coupon, date_format(r.debut, '%H:%i') debut, date_format(r.debut, '%d/%m/%Y') date, lcase(r.type) vehicule_type";
		$sql.=", concat(aa.nom,' ',ap.adresse1,' ',ap.cp,' ',ap.ville) agence_nom, a.zone, a.tel agence_tel, a.reseau";
		$sql.=", date_format(h.ouverture,'%H:%i') ouverture, date_format(h.fermeture, '%H:%i') fermeture";
		$sql.=", date_format(h.pause_debut,'%H:%i') pause_debut, date_format(h.pause_fin,'%H:%i') pause_fin";
		$sql.=" from reservation r";
		$sql.=" join client u on u.id=r.client_id";
		$sql.=" join agence a on a.id=r.agence";
		$sql.=" join agence_pdv ap on ap.id=r.pdv";
		$sql.=" join agence_alias aa on aa.id=r.alias";
		$sql.=" join agence_horaire h on (h.agence=ap.id and h.jour=1+weekday(r.debut))";
		$sql.=" left join res_option o on (o.reservation=r.id and `option` like 'l_v_')";
		$sql.=" where r.relance is null and r.statut='P'"; //r�servation pay� et non relanc�
		$sql.=" and o.reservation is null"; // on ne veut pas de livraison � domicile
		$sql.=" and r.debut BETWEEN NOW() AND date_add(current_date(), interval 3 day)"; // dont le d�part est dans les prochaines 48h � partir de maintenant
		$cnt = 0; $rs = $this->sqlExec ($sql);
		while ($row = mysql_fetch_assoc($rs))
		{
			$coupon = null;
			$row['resid'] = resid_client ($row);
			if ($row['pause_debut'] && $row['pause_debut']!=$row['pause_fin'])
			{
				$horaires = $row['ouverture'].' � '.$row['pause_debut'];
				$horaires.=" et de ".$row['pause_fin'].' � '.$row['fermeture'];
			}
			else
				$horaires = $row['ouverture'].' � '.$row['fermeture'];
			$row['horaires'] = $horaires;
			if ($row['coupon'])
			{
				$coupon = mysql_fetch_assoc(sqlexec("select concat(pc.code,' - ',pc.nom) entreprise, p.origine from partenaire_coupon pc join partenaire p on p.id=pc.partenaire where pc.id=".$row['coupon']));
				$row['entreprise'] = $coupon['entreprise'];
			}
			// obtenir le message pour l'agence
			$agence = new Agence($row);
			$row['msg_partenaire'] = $agence->getHtmlMsgPartner('<br><span style="color:#CC0000">', '</span>');
			
			// renseigner l'e-mail
			$eml = TPL_EMAIL_2ND_VOUCHER;
			if (in_array($coupon['origine'],array('ce','locapass')))
				$eml .= '-'.$coupon['origine'];
			else if (in_array($reservation['forfait_type'], array('JEUNES','LOCAPASS'))) 
				$eml .= '-'.strtolower($reservation['forfait_type']); // em12-jeunes.html
			$html = load_and_parse ('emails/'.$eml.'.html', $row);
			send_mail ($html, $row['email'], SUBJECT_2ND_VOUCHER, EMAIL_FROM, EMAIL_FROM_ADDR, '', ((/*$cnt < 2 ||*/ trim($html)=='' )? 'gdubourguet@rnd.fr' : ''));
			$cnt++;
			if (SITE_MODE != 'PROD')
				$this->msg('envoi � '.$row['email'],'I', true);
			else
			{
				// on n'enregistre pas cette requ�te dans le log
				getsql('UPDATE reservation SET relance = NOW() WHERE id='.$row['id']);
			}
			// on v�rifie l'appartenance au programme kilom�tre
			if (!$row['pgkm'] && PgKm::checkEmail($row['email'], true))
				$this->msg('programme km pour '.$row['email'], 'I', true);
		}
		if ($cnt)
		{
			$this->incActions();
			$this->msg("$cnt e-mails envoy�s", 'I', true);
		}
	}
	private function notifyQuestionnaire()
	{
		if (!$this->isAllowed('questionnaire')) return;
		
		// la liste des r�servations qui viennent de se terminer et qui n'ont pas �t� invit�s
		// on r�-utilise le champ reservation.relance
		$sql = "SELECT r.id reservation, r.forfait_type, r.fin, r.type, ap.nom agence, u.email, CASE WHEN u.titre='M' THEN 'Cher' ELSE 'Ch�re' END titre, u.prenom, u.nom";
		$sql.=" FROM reservation r";
		$sql.=" JOIN agence_pdv ap on ap.id=r.pdv";
		$sql.=" JOIN client u on u.id=r.client_id";
		$sql.=" WHERE r.fin BETWEEN DATE_ADD(CURRENT_DATE, INTERVAL -3 DAY) AND CURRENT_DATE";
		$sql.=" AND r.statut='P'";
		$sql.=" AND (r.relance IS NULL OR r.relance < r.fin)";
		
		$cnt = 0; $rs = $this->sqlExec ($sql);
		while ($row = mysql_fetch_assoc($rs))
		{
			$row['vehicule_type'] = $row['type'];
			$row['url_questionnaire'] = QuestionQuestionnaire::getURL($row['reservation']);

			// renseigner l'e-mail
			$eml = TPL_EMAIL_QUESTIONNAIRE;
			if (in_array($reservation['forfait_type'], array('JEUNES')))
				$eml .= '-'.strtolower($reservation['forfait_type']); // em15-jeunes.html
			$html = load_and_parse ('emails/'.$eml.'.html', $row);
			send_mail ($html, $row['email'], SUBJECT_QUESTIONNAIRE, EMAIL_FROM, EMAIL_FROM_ADDR, '', ((/*$cnt < 2 ||*/ trim($html)=='' )? 'gdubourguet@rnd.fr' : ''));
			$cnt++;
			if (SITE_MODE != 'PROD')
				$this->msg('envoi � '.$row['email'],'I', true);
			else
			{
				// on n'enregistre pas dans le log cette requ�te
				getsql('UPDATE reservation SET relance = NOW() WHERE id='.$row['reservation']);
			}
		}
		if ($cnt)
		{
			$this->incActions();
			$this->msg("$cnt invitations au questionnaire envoy�es", 'I', true);
		}
	}
	
	/**
	* relance les r�servations sans r�ponse de UNIPRO
	* @param Unipro $unipro
	*/
	private function uniproFix($unipro)
	{
		if (!$unipro) return;
		if (!$this->isAllowed('unipro')) return;
		$this->msg('uniproFix','I',true);
		// ins�rer dans res_unipro si des r�servations n'ont pas pu y �tre ins�r�es
		$sql = "INSERT INTO res_unipro (reservation, statut, creation, modification)";
		$sql.=" SElECT r.id reservation, r.statut, r.paiement, r.paiement";
		$sql.=" FROM reservation r ";
		$sql.=" JOIN agence a on a.id=r.agence";
		$sql.=" LEFT JOIN res_unipro u ON u.reservation=r.id";
		$sql.=" WHERE r.paiement BETWEEN '".date('Y-m-d H:i:s', strtotime('-2 weeks'))."' AND '".date('Y-m-d H:i:s', strtotime('-2 minutes'))."'";
		$sql.=" AND r.statut IN ('A','P')";
		$sql.=" AND a.notification='rpc' AND a.reseau='ADA'";
		$sql.=" AND u.reservation IS NULL";
		$sql.=" ORDER BY r.id";
		$this->sqlExec($sql);
		$cnt = mysql_affected_rows();
		if ($cnt)
			$this->msg($cnt." r�servations UNIPRO � reprendre",'I', true);

		// liste des r�servations pay�es mais non enregistr�es dans unipro
		$sql = 'select r.*';
		$sql.=' from res_unipro u';
		$sql.=' join reservation r on r.id=u.reservation';
		$sql.=' join agence a on a.id=r.agence';
		$sql.=" where u.unipro is null and u.validation is null and a.notification='rpc' and u.modification <= '".date('Y-m-d H:i:s', strtotime('-1 minute'))."'";
		$rs = $this->sqlExec($sql);

		// on parcourt la liste des r�servations
		$cnt = 0;
		while ($reservation = mysql_fetch_assoc($rs))
		{
			$cnt++;
			$unipro->sendReservation ( new Reservation($reservation), true);
		}
		$this->msg($cnt.' r�servations envoy�s � UNIPRO', 'I', true);
		if ($cnt) $this->incActions();
		
		// puis la liste des r�servations annul�es
		$sql = 'select r.*, u.unipro';
		$sql.=' from res_unipro u join reservation r on r.id=u.reservation';
		$sql.=" where u.unipro is not null and r.statut='A' and u.annulation is null";
		$cnt = 0; $rs = $this->sqlExec($sql);
		while ($reservation = mysql_fetch_assoc($rs))
		{
			$cnt++;
			$unipro->cancelReservation (new Reservation($reservation));
		}
		$this->msg($cnt.' annulations envoy�s � UNIPRO', 'I', true);
		if ($cnt) $this->incActions();
		
		// ... enfin on met � jour le mat�riel annul�
		$sql = 'select r.*';
		$sql.=' from facturation_materiel f';
		$sql.=' join res_unipro u on f.reservation=u.reservation';
		$sql.=' join reservation r on r.id=u.reservation';
		$sql.=' join agence a on a.id=r.agence';
		$sql.=" where f.creation >= '".date('Y-m-d', strtotime('-1 week'))."' and f.type='A'";
		$sql.=" and u.unipro is not null and u.validation is not null and u.materiel is null and a.notification='rpc'";
		$sql.=" and r.statut='P'";
		$rs = $this->sqlExec($sql);

		// on parcourt la liste des r�servations
		$cnt = 0;
		while ($reservation = mysql_fetch_assoc($rs))
		{
			$cnt++;
			$unipro->updateOptions(new Reservation($reservation));
		}
		$this->msg($cnt.' retrait de mat�riel envoy�es � UNIPRO', 'I', true);
		if ($cnt) $this->incActions();
		
		// r�capitulatif des erreurs du jour
		$sql = 'SELECT count(*) nb, action, erreur_code, erreur_message';
		$sql.=' FROM `log_unipro` WHERE cast(creation as date)=current_date';
		$sql.=' GROUP BY action, erreur_code, erreur_message ORDER BY action, nb desc';
		$rs = $this->sqlExec($sql);
		if (!$rs || !mysql_num_rows($rs)) return;
		$this->msg('Liste des erreurs', 'I', true);
		$cnt = 0;
		while ($row = mysql_fetch_assoc($rs))
		{
			if (!$cnt) $this->msg(join("\t", array_keys($row)),'I',true);
			$cnt += $row['nb'];
			$this->msg(join("\t",$row),'I',true);
		}
		$this->msg($cnt.' erreurs renvoy�es par UNIPRO','I',true);
	}
	/**
	* Importation depuis UNIPRO
	* @param Unipro $unipro
	*/
	private function uniproImport(Unipro $unipro)
	{
		if (!$unipro) return;
		$this->msg('uniproImport','I',true);
		$this->importAgences($unipro);
		$this->importHoraires($unipro);
		$this->importFermetures($unipro);
		$this->importJaugesVariables($unipro);
		return;
	}
	/**
	* importe les agences dans la base ADAFR
	* 
	* @param Unipro $unipro
	*/
	private function importAgences (Unipro $unipro)
	{
		if (!$this->isAllowed('agences')) return;
		// obtenir les agences ADA
		$agences = $unipro->getAgences();
		if (!$agences)
		{
			$this->error($unipro->getLastError());
			return;
		}
		$this->getSql('TRUNCATE TABLE unipro_agence');
		$cnt = 0;
		foreach ($agences as $a)
		{
			$cnt++;
			$ok = true;
			$a = $unipro->convertAgence($a);
			// on v�rifie les informations
			foreach ($a as $k => $v)
			{
				if ($v || $k == 'adresse2' || $k == 'fax') continue;
				$this->msg("champ non renseign� pour ".$a['id'].': '.$k, 'W', true);
				if ($k!='tel' && $k!='fax' && $k!='email')
					$ok = false;
			}
			if (!$ok) 
			{
				$this->msg('agence '.$a['id'].' non ins�r�','W',true);
				continue;
			}
			// on enregistre l'agence
			save_record ('unipro_agence', $a);
		}
		$this->msg($cnt.' agences import�es', 'I', true);
		if (!$cnt) return; // si il n'y avait pas d'agences on arr�te tout
		$this->incActions();
		// on remet � NULL les champs qui peuvent l'�tre
		$this->tableSetNulls('unipro_agence');
		// v�rifiers les donn�es
		$fields = $this->tableGetFields('unipro_agence');
		unset($fields[array_search('adresse2', $fields)]);
		unset($fields[array_search('fax', $fields)]);
		$rs = $this->sqlExec('select id from unipro_agence where concat('.join(',',$fields).') is null');
		if ($cnt = mysql_num_rows($rs))
		{
			while ($row = mysql_fetch_assoc($rs))
				$ids[] = $row['id'];
			$this->msg($cnt.' agences UNIPRO avec des valeurs NULL: '.join(',', $ids), 'W', true);
		}
		
		// mettre � jour les informations sur les agences
		$sql = 'UPDATE agence a JOIN unipro_agence u ON a.id=u.id SET a.modification=NOW(), a.fax=u.fax';
		$fields = array('zone','nom','societe','rcs','code_base','code_groupe','code_societe','code_region','franchise','tel','email');
		foreach ($fields as $v)
			$sql.= ", a.$v=u.$v";
		$sql.=' WHERE CONCAT(u.'.join(',u.', $fields).') IS NOT NULL';
		$this->getSql($sql);
		$this->msg(mysql_affected_rows().' agences mises � jour', 'I', true);
		// mettre � jour les points de vente
		$sql = 'UPDATE agence_pdv ap JOIN unipro_agence u ON ap.id=u.id SET ap.modification=NOW()';
		foreach (array('nom','emplacement','adresse1','adresse2','cp','ville') as $v)
			$sql.= ", ap.$v=u.$v";
		$sql.=' WHERE CONCAT(u.nom,u.emplacement,u.adresse1,u.cp,u.ville) IS NOT NULL';
		$this->sqlExec($sql);
		$this->msg(mysql_affected_rows().' points de vente mis � jour', 'I', true);
		// on d�publie les agences qui ne sont pas dans la liste UNIPRO
		$cnt = 0; $rs = $this->sqlExec("SELECT a.id, a.nom, a.statut FROM agence a LEFT JOIN unipro_agence u ON a.id=u.id WHERE a.reseau='ADA' AND (a.statut&1)=1 AND u.id IS NULL");
		while ($row = mysql_fetch_assoc($rs))
		{
			$cnt++; $this->msg('fermeture agence '.$row['id'].' - '.$row['nom'], 'I', true);
			$row['statut'] &= ~1;
			if (SITE_MODE == 'PROD') {
				// ...on ne d�publie pas en DEV et TEST...
				save_record('agence', $row);
			}
		}
		if ($cnt) $this->msg($cnt.' agences ferm�es','I',true);
		// on ouvre des agences qui �taient ferm�es
		$cnt = 0; $rs = $this->sqlExec('SELECT a.id, a.statut, a.nom FROM agence a JOIN unipro_agence u ON u.id=a.id WHERE (a.statut&1)=0');
		while ($row = mysql_fetch_assoc($rs))
		{
			$cnt++; $this->msg('ouverture agence '.$row['id'].' - '.$row['nom'], 'I', true);
			$row['statut'] |= 1;
			save_record('agence', $row);
		}
		if ($cnt) $this->msg($cnt.' agences r�-ouvertes','I',true);
		
		// mise � jour des statuts et des zones pour les points de vente et les alias
		$this->getSql('UPDATE agence_pdv ap JOIN agence a ON a.id=ap.agence SET ap.statut = ap.publie * a.statut, ap.zone = a.zone');
		$this->msg(mysql_affected_rows().' statut et zone de point de vente mis � jour', 'I', true);
		$this->getSql('UPDATE agence_alias aa JOIN agence_pdv ap ON aa.pdv=ap.id SET aa.statut=ap.statut, aa.zone=ap.zone');
		$this->msg(mysql_affected_rows()." statut et zone d'alias mis � jour", 'I', true);
		
		// il ne reste plus qu'� cr�er les nouvelles agences
		$cnt = 0; $rs = $this->sqlExec('SELECT u.* FROM unipro_agence u LEFT JOIN agence a ON a.id=u.id WHERE a.id IS NULL');
		while ($row = mysql_fetch_assoc($rs))
		{
			$cnt++;
			$this->msg('ajout agence '.$row['id'].' - '.$row['nom'], 'I', true);
			$row['statut'] = Agence::STATUT_VISIBLE; // visible uniquement
			$row['type'] = Agence::TYPE_VP + Agence::TYPE_VU; // VP et VU
			save_record('agence', $row);
			// cr�er les disponibilit�s
			$this->getSql("INSERT INTO agence_dispo (agence, categorie, type, nb) SELECT '".$row['id']."', id, type, 0 FROM categorie WHERE zone='fr' and publie=1");
			$this->msg(mysql_affected_rows()." disponibilit�s cr��s pour l'agence ".$row['id']);
			// puis cr�er le point de vente
			$row['agence'] = $row['id'];
			save_record ('agence_pdv', $row);
			$this->msg('ajout point de vente '.$row['id']);
			// puis l'alias
			save_alias($row, false);
			$this->msg('ajout alias '.$row['id']);
		}
		$this->msg($cnt.' agences ajout�es','I', true);
		// on v�rifie l'unicit� de recherche
		$cnt = 0; $rs = $this->sqlExec('SELECT recherche FROM agence_alias WHERE (statut&1)=1 GROUP BY recherche HAVING COUNT(*) > 1');
		while ($row = mysql_fetch_assoc($rs))
		{
			$cnt++; $this->msg('doublon recherche '.$row['recherche'], 'I', true);
		}
		if ($cnt) $this->msg($cnt.' doublons pour la recherche des agences', 'W', true);
	}
	protected function importHoraires(Unipro $unipro)
	{
		if (!$this->isAllowed('horaires')) return;
		$this->msg('importation des horaires', 'I', true);
		$horaires = $unipro->getHoraires();
		if (!$horaires || !is_array($horaires) || !count($horaires))
		{
			$this->msg('aucun horaire d�fini', 'I', true);
			return;
		}
		$this->msg('fin de r�ception des horaires ');
		// on parcourt les jauges et on les enregistre
		$this->getSql('TRUNCATE TABLE unipro_horaire');
		$sql = 'INSERT INTO unipro_horaire (agence, jour, ouverture, fermeture, pause_debut, pause_fin) VALUES ';
		$values = array();
		// il y a plusieurs lignes pour chaque journ�e
		// il faut retrouver les pauses et traiter les cas particuliers
		$cnt = count($horaires);
		for ($i=0; $i < $cnt; $i++)
		{
			$j = 0; $a = array();
			$x = $unipro->convertHoraire($horaires[$i]);
			if ($x['type']!='G' || $x['ouverture']==$x['fermeture'])
				continue;
			$a[] = $x;
			while ($i+$j+1 < $cnt)
			{
				$y = $unipro->convertHoraire($horaires[$i+$j+1]);
				if ($y['jour']!=$x['jour']) break;
				$j++;
				if ($y['type']!='G' || $y['ouverture']==$y['fermeture'])
					continue;
				$a[] = $y;
			}
			// on augment $i
			$i = $i+$j;
			// consturire la ligne � partir des donn�es dans a[]
			$line = "('".$x['agence']."',".$x['jour'];
			if (count($a) >= 3)
			{
				$this->msg('Plus de 3 horaires pour agence '.$x['agence'].', jour '.$x['jour'], 'W', true);
				continue;
			}
			else if (count($a) == 2)
				$line .= ",'".join("','", array($a[0]['ouverture'], $a[1]['fermeture'], $a[0]['fermeture'], $a[1]['ouverture']))."')";
			else
				$line .= ",'".$x['ouverture']."','".$x['fermeture']."',NULL,NULL)";
			$values[] = $line;
		}
		if (!count($values)) return;
		getsql($sql.join(',', $values));
		$this->msg(mysql_affected_rows().' horaires import�es', 'I', true);
		// mettre � jour les horaires des agences (on ne supprime pas les B, C, D : Unipro n'a pas d'informations sur les points de vente)
		// ... mais on met � jour les 'P' qui sont les POINTLOC
		$this->sqlExec("DELETE agence_horaire FROM agence_horaire LEFT JOIN unipro_horaire u ON (agence_horaire.agence=u.agence AND agence_horaire.jour=u.jour) WHERE u.agence IS NULL AND RIGHT(agence_horaire.agence,1) IN ('A','P') AND LEFT(agence_horaire.agence,2)='FR'");
		$this->msg(mysql_affected_rows().' horaires supprim�s','I', true);
		$this->sqlExec("UPDATE agence_horaire h JOIN unipro_horaire u ON (h.agence=u.agence AND h.jour=u.jour) SET h.ouverture=u.ouverture, h.fermeture=u.fermeture, h.pause_debut=u.pause_debut, h.pause_fin=u.pause_fin");
		$this->msg(mysql_affected_rows().' horaires mis � jour','I', true);
		$this->sqlExec('INSERT INTO agence_horaire(agence, jour, ouverture, fermeture, pause_debut, pause_fin) SELECT u.* FROM unipro_horaire u LEFT JOIN agence_horaire h ON (h.agence=u.agence AND h.jour=u.jour) WHERE h.agence IS NULL');
		$this->msg(mysql_affected_rows().' horaires ins�r�s','I', true);
		
		/* mettre � jour agence_pdv.j7 */
		// liste des agences ouvertes 7 jours
		$rs = $this->sqlExec('SELECT agence FROM agence_horaire GROUP BY agence HAVING SUM(jour)=28 ORDER BY agence');
		$this->msg(mysql_num_rows($rs).' agences ouvertes 7/7', 'I', true);
		$a = array();
		while ($row = mysql_fetch_row($rs))
			$a[] = $row[0];
		$agences = "'".join("','",$a)."'";
		$this->getSql("UPDATE agence_pdv SET j7=0 WHERE j7=1 AND id NOT IN ($agences)");
		$this->getSql("UPDATE agence_pdv SET j7=1 WHERE j7=0 AND id IN ($agences)");
	}
	/**
	* importation des fermetures
	* 
	* @param Unipro $unipro
	*/
	protected function importFermetures(Unipro $unipro)
	{
		if (!$this->isAllowed('fermetures')) return;
		$this->msg('importation des fermetures', 'I', true);
		$fermetures = $unipro->getFermetures();
		if (!is_array($fermetures))
		{
			$this->msg('aucune fermeture d�finie', 'I', true);
			return;
		}
		// on parcourt les jauges et on les enregistre
		$this->getSql('TRUNCATE TABLE unipro_fermeture');
		$sql = 'INSERT INTO unipro_fermeture (agence, debut, fin) VALUES ';
		$values = array();
		foreach ($fermetures as $fermeture)
		{
			if ($x = $unipro->convertFermeture($fermeture))
				$values[] = "('".join("','", $x).("')");
		}
		if (count($values))
		{
			getsql($sql.join(',', $values));
			$this->msg(mysql_affected_rows().' fermetures import�es', 'I', true);
		}
		// on supprime les fermetures Unipro qui ne sont plus pr�sentes
		$this->sqlExec("DELETE agence_fermeture FROM agence_fermeture LEFT JOIN unipro_fermeture u ON (agence_fermeture.agence=u.agence AND agence_fermeture.debut=u.debut AND agence_fermeture.fin=u.fin) WHERE agence_fermeture.provenance='unipro' AND u.agence IS NULL AND agence_fermeture.fin > current_date");
		$this->msg(mysql_affected_rows().' fermetures suppirm�es','I', true);
		// on ins�re celles qui ne sont pas d�j� connues
		$sql = "INSERT INTO agence_fermeture (agence,zone,provenance,debut,fin) SELECT u.agence, a.zone, 'unipro', u.debut, u.fin";
		$sql.=" FROM unipro_fermeture u JOIN agence a ON a.id=u.agence LEFT JOIN agence_fermeture f ON (f.agence=u.agence AND f.debut=u.debut AND f.fin=u.fin)";
		$sql.=" WHERE f.id IS NULL";
		$this->sqlExec($sql);
		$this->msg(mysql_affected_rows()." fermetures ins�r�es");
	}
	/**
	* importation des jauges variables
	* 
	* @param Unipro $unipro
	*/
	protected function importJaugesVariables(Unipro $unipro)
	{
		if (!$this->isAllowed('jauges')) return;
		$this->msg('importation des jauges variables', 'I', true);
		$jauges = $unipro->getJauges();
		if (!$jauges || !is_array($jauges) || !count($jauges))
		{
			$this->msg('aucune jauge variable d�finie', 'I', true);
			return;
		}
		// on parcourt les jauges et on les enregistre
		$this->getSql('TRUNCATE TABLE unipro_jauge');
		$sql = 'INSERT INTO unipro_jauge (agence, categorie, debut, fin, nb) VALUES ';
		$values = array();
		foreach ($jauges as $jauge)
		{
			if ($x = $unipro->convertJauge($jauge))
			{
				$x['categorie'] = addslashes($x['categorie']);
				$values[] = "('".join("','", $x)."')";
			}
		}
		if (!count($values)) return;
		getsql($sql.join(',', $values));
		$this->msg(mysql_affected_rows().' jauges import�es', 'I', true);
		// on supprime les stop sell qui ont �t� annul�s
		$sql = "DELETE stop_sell FROM stop_sell JOIN categorie c ON c.id=stop_sell.categorie";
		$sql.=" LEFT JOIN unipro_jauge u ON (u.agence=stop_sell.agence AND u.categorie=c.mnem AND c.zone='fr' AND c.publie=1 AND u.debut=stop_sell.debut AND u.fin=stop_sell.fin)";
		$sql.=" WHERE stop_sell.provenance='unipro' AND stop_sell.fin >= current_date AND u.agence IS NULL";
		$this->sqlExec($sql);
		$this->msg(mysql_affected_rows().' stop_sell supprim�s','I',true);
		// on met � jour les stop_sell
		$sql = "UPDATE stop_sell s JOIN categorie c ON c.id=s.categorie";
		$sql.=" JOIN unipro_jauge u ON (u.agence=s.agence AND s.provenance='unipro' AND u.categorie=c.mnem AND u.debut=s.debut AND u.fin=s.fin)";
		$sql.=" SET s.modification = (CASE WHEN s.nb!=u.nb THEN NOW() ELSE s.modification END), s.nb = u.nb";
		$this->sqlExec($sql);
		$this->msg(mysql_affected_rows().' stop_sell mis � jour','I',true);
		// on ins�re les stop sell
		$sql = 'INSERT INTO stop_sell (provenance, zone, agence, type, categorie, nb, debut, fin, creation, modification)';
		$sql.=" SELECT 'unipro', a.zone, u.agence, c.type, c.id, u.nb, u.debut, u.fin, NOW(), NOW()";
		$sql.=" FROM unipro_jauge u JOIN agence a ON a.id=u.agence JOIN categorie c ON (c.mnem=u.categorie AND c.zone='fr' AND c.publie=1)";
		$sql.=" LEFT JOIN stop_sell s ON (s.provenance='unipro' AND s.zone=a.zone AND s.agence=u.agence AND s.categorie=c.id AND s.debut=u.debut AND s.fin=u.fin)";
		$sql.=" WHERE s.id IS NULL";
		$this->sqlExec($sql);
		$this->msg(mysql_affected_rows().' stop_sell ins�r�es','I',true);
	}
	
	/**
	* updateGeoLoc - met � jour les coordonn�es des point de vente
	* 
	*/
	protected function updateGeoLoc()
	{
		if (!$this->isAllowed('geoloc')) return;
		$this->msg('updateGeoLoc', 'I', true);
		$this->getSql("UPDATE agence_pdv SET latitude=NULL, longitude=NULL WHERE latitude=0 AND longitude=0");
		// s�lectionner les agences � mettre � jour
		$sql = "SELECT ap.id, ap.nom, ap.adresse1, ap.adresse2, ap.cp, ap.ville, z.nom pays FROM agence_pdv ap JOIN zone z ON z.id=ap.zone WHERE (longitude+latitude is null) and (statut&1)=1 and ap.publie=1";
		$rs = $this->sqlExec($sql);
		$this->msg(mysql_num_rows($rs).' agences sans coordonn�es','I', true);
		$count = 0;
		while ($row = mysql_fetch_assoc($rs))
		{
			$a = array();
			// on enl�ve le CEDEX s'il existe
			$ville = preg_replace('/\scedex\s*$/i','', $row['ville']);
			if ($row['cp']=='...') $row['cp'] = '';
			$pays = $row['pays'];
			// l'adrese de base
			$a[] = $row['adresse1'].' '.$row['adresse2'].' '.$row['cp'];
			if ($row['adresse2'])
			{
				$a[] = $row['adresse1'].' '.$row['cp'];
				$a[] = $row['adresse2'].' '.$row['cp'];
			}
			// puis sans le code postal
			$a[] = $row['adresse1'].' '.$row['adresse2'];
			if ($row['adresse2'])
			{
				$a[] = $row['adresse1'];
				$a[] = $row['adresse2'];
			}
			// on essaye pour chaque adresse
			$best = null;
			foreach ($a as $adresse)
			{
				$adresse .= " $ville, $pays";
				$this->msg('geoloc '.$row['id'].': '.$adresse);
				$x = Geocoding::factory(utf8_encode($adresse));
				if ($x->isOK())
				{
					$locationType = $x->getLocation($lat, $lng);
					$this->msg('geoloc r�ussi '.$row['id']." - $locationType "."($lat, $lng) : $adresse", 'I');
					$row['location_type'] = $locationType;
					$row['latitude'] = deg2rad($lat);
					$row['longitude'] = deg2rad($lng);
					if ($locationType == 'ROOFTOP' || $locationType == 'RANGE_INTERPOLATED')
					{
						$best = $row;
						break;
					}
					// sinon est ce qu'on a mieux
					if (!$best)
						$best = $row;
					else if ($locationType == 'GEOMETRIC_CENTER' && $best['location_type']=='APPROXIMATE')
						$best = $row;
				}
			}
			if (!$best || $best['location_type']=='APPROXIMATE') 
				$this->msg('pas de g�olocalisation pour '.join(' - ', $row), 'W', true);
			else
				save_record('agence_pdv', $best);
		}
		// puis on met � jour les distances entre agence
		$this->msg('mise � jour de la table agence_distance');
		$this->sqlExec('TRUNCATE TABLE agence_distance');
		$sql = "INSERT INTO agence_distance";
		$sql.=" SELECT a.id centre, b.id autour, 6378 * acos(cos(b.latitude) * cos(a.latitude) * cos(a.longitude - b.longitude) + sin(b.latitude) * sin(a.latitude)) as distance";
		$sql.=" FROM agence_pdv a ";
		$sql.=" JOIN agence_pdv b ON (a.zone=b.zone AND (b.statut&1)=1 AND (a.zone!='fr' OR (LEFT(b.cp,2)=LEFT(a.cp,2) OR LEFT(b.cp,2) IN (SELECT contour FROM dept_contour WHERE dept=LEFT(a.cp,2)))))";
		$sql.=" WHERE (a.statut&1)=1 AND a.id!=b.id and (a.longitude+a.latitude+b.longitude+b.latitude) is not null";
		$this->sqlExec($sql);
		$this->msg(mysql_affected_rows().' distances entre agences', 'I',true);
	}
	/**
	* met � jour sitemap.txt et les sitemap.xml
	* 
	*/
	protected function updateRefNat()
	{
		if (!$this->isAllowed('refnat')) return;
		$this->msg('updateRefNat : mise � jour map.txt', 'I', true);
		$x = make_maptxt();
		if (!$x || !is_array($x) || !count($x)) return;
		foreach ($x as $v)
			$this->msg('map.txt: '.$v, 'I', true);
	}
	protected function satisfactionPointDeVente() {
		if (!$this->isAllowed('satisfaction')) return;
		$this->msg('Calcul satisfaction points de vente '.date('H:i:s'), 'I', true);
		// on r�cup�re le SQL permettant d'avoir les notes de chaque point de vente sur l'ann�e �coul�e
		$args = array('date_from' => date('d-m-Y', strtotime('-1 year')), 'date_to' => date('d-m-Y'));
		$x = QuestionResults::factory($args);
		$sql = $x->getSQLExportByAgence();
		// on supprime la r�r�rence aux codes soci�t�s
		$sql = str_replace('u.code_societe,', '', $sql);
		// on met � jour les points de vente
		$sql = sprintf('UPDATE agence_pdv ap JOIN (%s) t ON ap.id=t.pdv SET ap.satisfaction_reponses=t.reponses, ap.satisfaction_commentaires=t.commentaires, ap.satisfaction_agence=t.agence, ap.satisfaction_vehicule=t.vehicule', $sql);
		$this->sqlExec($sql);
		$this->msg(mysql_affected_rows().' satisfactions points de vente mises � jour, '.date('H:i:s'), 'I', true);
	}
	
	protected function archiveHisto()
	{
		if (!$this->isAllowed('archive')) return;
		$this->msg('Archivage: d�but '.date('H:i:s'), 'I', true);
		// _archive_reservation
		$this->sqlExec("INSERT INTO _archive_reservation SELECT r.* FROM reservation r LEFT JOIN _archive_reservation a ON a.id=r.id WHERE r.creation < CURRENT_DATE AND r.statut IN ('C','V') AND a.id IS NULL");
		$this->msg(mysql_affected_rows().' r�servations archiv�es, '.date('H:i:s'), 'I', true);
		$this->sqlExec("DELETE reservation FROM reservation JOIN _archive_reservation a ON reservation.id=a.id");
		$this->msg(mysql_affected_rows().' r�servations supprim�es, '.date('H:i:s'), 'I', true);
		// archiver les res_option correspondantes
		$this->sqlExec("INSERT INTO _archive_res_option SELECT o.* FROM res_option o LEFT JOIN reservation r ON r.id=o.reservation WHERE r.id IS NULL");
		$this->msg(mysql_affected_rows().' options archiv�es, '.date('H:i:s'), 'I', true);
		$this->sqlExec("DELETE res_option FROM res_option LEFT JOIN reservation r ON r.id=res_option.reservation WHERE r.id IS NULL");
		$this->msg(mysql_affected_rows().' options supprim�es, '.date('H:i:s'), 'I', true);
		// archiver les stop_sell
		$this->sqlExec("INSERT INTO _archive_stop_sell SELECT s.* FROM stop_sell s LEFT JOIN _archive_stop_sell a ON a.id=s.id WHERE s.fin IS NOT NULL AND s.fin < CURRENT_DATE AND a.id IS NULL");
		$this->msg(mysql_affected_rows().' stop sell archiv�s, '.date('H:i:s'), 'I', true);
		$this->sqlExec("DELETE stop_sell FROM stop_sell JOIN _archive_stop_sell a ON stop_sell.id=a.id");
		$this->msg(mysql_affected_rows().' stop sell supprim�s, '.date('H:i:s'), 'I', true);
		// archiver les promotions forfaits
		$this->sqlExec("INSERT INTO _archive_promotion_forfait SELECT pf.* FROM promotion_forfait pf LEFT JOIN _archive_promotion_forfait a ON a.id=pf.id WHERE pf.fin IS NOT NULL AND pf.fin < CURRENT_DATE AND a.id IS NULL");
		$this->msg(mysql_affected_rows().' promotions archiv�es, '.date('H:i:s'), 'I', true);
		$this->sqlExec("DELETE promotion_forfait FROM promotion_forfait JOIN _archive_promotion_forfait a ON promotion_forfait.id=a.id");
		$this->msg(mysql_affected_rows().' promotions supprim�es, '.date('H:i:s'), 'I', true);
		// archiver les agence_promo_forfait
		$date_archive = date('Y-m-d', strtotime('-6 weeks'));
		$this->sqlExec("INSERT INTO _archive_agence_promo_forfait SELECT pf.* FROM agence_promo_forfait pf LEFT JOIN _archive_agence_promo_forfait a ON a.id=pf.id WHERE pf.fin IS NOT NULL AND pf.fin < '".$date_archive."' AND a.id IS NULL");
		$this->msg(mysql_affected_rows().' promotions agences archiv�es, '.date('H:i:s'), 'I', true);
		$this->sqlExec("DELETE agence_promo_forfait FROM agence_promo_forfait JOIN _archive_agence_promo_forfait a ON agence_promo_forfait.id=a.id");
		$this->msg(mysql_affected_rows().' promotions agences supprim�es, '.date('H:i:s'), 'I', true);
		
		// supprimer les inforamtions de paiement conserv�es pour l'analyse
		$date_archive = date('Y-m-d', strtotime('-3 months'));
		$this->sqlExec(sprintf("DELETE FROM log_paybox WHERE creation <= '%s'", $date_archive));
		$this->sqlExec(sprintf("DELETE FROM paybox WHERE creation <= '%s'", $date_archive));
		$date_archive = date('Y-m-d', strtotime('-1 week'));
		$this->sqlExec(sprintf("DELETE FROM paybox_secure WHERE creation <= '%s'", $date_archive));
	}
	public function notifyAgenceOfCustomers()
	{
		if (!$this->isAllowed('clients')) return;
		$this->msg('Relance client : d�but '.date('H:i:s'), 'I', true);
		
		// le d�lai � seruveiller
		$days = 2;

		// on conserve dans une table les r�servations � relancer
		$sql ="insert into res_abandon (jour, agence, reservation, client, type, categorie, creation, duree, km, debut, fin, total, paiement_mode, code_reponse)";
		$sql.=" select current_date(), ar.agence, ar.id, ar.client_id, ar.type, ar.categorie, ar.creation, ar.duree, ar.km, ar.debut, ar.fin, ar.total, ar.paiement_mode, ar.code_reponse";
		$sql.=" from _archive_reservation ar";
		$sql.=" left join reservation r on (ar.client_id=r.client_id and r.statut='P' and r.paiement>=date_add(current_date(), INTERVAL -$days DAY))";
		$sql.=" where ar.creation >= date_add(current_date(), INTERVAL -$days DAY) and ar.statut='V' and ar.forfait_type IN ('ADAFR','JEUNES') and ar.debut >= current_date()";
		$sql.=" and r.id is null";
		$sql.=" order by ar.agence, ar.client_email, ar.creation";
		$this->sqlExec($sql);
		$cnt = mysql_affected_rows();
		$this->msg($cnt.' r�servations pouvant �tre relanc�es', 'I', true);

		// on recherche les clients ayant valid� une r�servation mais qui n'ont rien pay�
		$sql = "select ra.agence, a.email agence_email";
		$sql.=",ra.reservation `#`, concat(c.type,'/',c.mnem) `cat.`, ra.creation, ra.duree, ra.km, ra.debut, ra.fin, ra.total";
		$sql.=", case ra.paiement_mode when 'PB' then 'CB' when 'UN' then '1EURO' else 'non' end `paiement`";
		$sql.=", ra.code_reponse";
		$sql.=", u.prenom, u.nom, u.tel, u.email, u.cp, u.ville, p.nom pays";
		$sql.=" from res_abandon ra";
		$sql.=" join client u on u.id=ra.client";
		$sql.=" join categorie c on c.id=ra.categorie";
		$sql.=" join agence a on a.id=ra.agence";
		$sql.=" join pays p on p.id=u.pays";
		$sql.=" where ra.jour = current_date()";
		$sql.=' and (a.statut & '.(Agence::STATUT_ABANDONNISTES + Agence::STATUT_VISIBLE).')='.(Agence::STATUT_ABANDONNISTES  + Agence::STATUT_VISIBLE);
		$sql.=" order by ra.agence, u.email, ra.creation";

		$rs = $this->sqlExec($sql);
		$agence = $email = null;
		// on parcourt la liste
		// on rassemble les ifnormations pour une agence et on l'envoie � l'e-mail correspondant
		while($row = mysql_fetch_assoc($rs))
		{
			if ($agence != $row['agence'])
			{
				// s'il y a d�j� une agence de renseign� on envoie le mail
				if ($agence)
				{
					$this->_notifyAgenceOfCustomers($agence, $email, $data);
				}
				// initialisation pour la nouvelle agence
				$agence = $row['agence'];
				$email = defined('EMAIL_AGENCE_DEBUG') ? EMAIL_AGENCE_DEBUG : $row['agence_email'];
				$data = array();
			}
			// pr�paere les donn�es pour l'envoi
			if ($row['code_reponse'])
				$row['paiement'] = '['.$row['code_reponse'].'] : '.Paybox::getMessage($row['code_reponse'], 'PB', false);
			unset($row['code_reponse']);
			unset($row['agence']);
			unset($row['agence_email']);
			foreach(array('creation','debut','fin') as $k)
				$row[$k] = date('d/m/y H:i', strtotime($row[$k]));
			$data[] = $row;
		}
		if ($agence)
			$this->_notifyAgenceOfCustomers($agence,  $email, $data);
		$this->msg('Relance client : fin '.date('H:i:s'), 'I', true);
	}
	/**
	* Effectue un envoi pour une agence
	* 
	* @param string $agence	- identifiant de l'agence
	* @param string $email  - e-mail agence
	* @param array $data	- tableau des donn�es
	*/
	protected function _notifyAgenceOfCustomers($agence, $email, &$data)
	{
		// pr�parer le tableau des donn�es
		foreach ($data as $k => $row)
		{
			if (!$table)
			{
				$table = '<table border="1" cellpadding="2" cellspacing="0">'."\n";
				$table.= '<tr><th>'.join('</th><th>', array_keys($row)).'</th></tr>'."\n";
			}
			$table .= '<tr><td>'.join('</td><td>', $row).'</td></tr>'."\n";
		}
		$table .= '</table>';
		// pr�paration et envoi du mail
		$html = load_and_parse(BP.'/emails/em-relance-agence.html', array('agence'=>$agence, 'email'=>$email, 'table'=>$table));
		$subject = (SITE_MODE=='PROD' ? '[ADA]' : '[ADA-'.SITE_MODE.']');
		$subject .= " Relance client pour $agence";
		send_mail($html, $email, $subject, EMAIL_FROM, EMAIL_FROM_ADDR, '', BCC_EMAILS_TO_ABANDONNISTES);
		$this->msg("relance : ".count($data)." pour $agence - $email", 'I');
	}
}

?>