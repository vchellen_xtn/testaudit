
	// configuration de CKEditor
	// voir http://cdn.ckeditor.com/

	// je n'ai trouvé nulle part la liste exhaustive des boutons :(
	// ---------------------------------------------------------------------
	var configEditor = { toolbar: 
		[
		    ['Source', '-'],
		    ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-'],
		    ['Undo', 'Redo', 'RemoveFormat', '-'],
		    ['NumberedList', 'BulletedList', 'Outdent', 'Indent', '-'],
		    ['Bold', 'Italic', 'Underline', 'Subscript', 'Superscript', '-'],
		    ['Link', 'Unlink', 'Anchor', '-'],
		    ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-'],
		    ['Table', 'HorizontalRule', 'SpecialChar', '-'],
		    ['Format', 'Font', 'FontSize'],
		    ['TextColor', 'BGColor']
		]
	};

