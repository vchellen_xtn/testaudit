<?
	require_once('../constant.php');
	require_once("secure.php"); 
	require_once("lib.php");
	if (!$adminUser->hasRights(Acces::SB_ADMIN))
		die("Vous n'avez pas les droits pour voir cette information !");

	if (!defined('USE_1CLIC'))
		define('USE_1CLIC', false);
	
	// lien et format
	$href = array();
	$format = array
	(
		'Mois' => '%02d',
		'Heure' => '%02d',
		'CA' => 'money', //'%01.2f �', 
		'CA ADA' => 'money', //'%01.2f �', 
		'CA COURTAGE' => 'money', //'%01.2f �', 
		'CA MATERIEL' => 'money', //'%01.2f �', 
		'CA PRO' => 'money', //'%01.2f �', 
		'Moy. CA' => 'money',
		'Moy. R�duction' => 'money',
		'R�ductions' => 'money',
		'Moy. Yield' => 'money',
		'Yield' => 'money',
	);
	$modes = array(
		'today'	=> "Aujourd'hui",
		'paybox_bin'=> 'BIN',
		'paybox_1clic'=> '1 clic',
		'paybox_trans'=> 'Paybox Trans.',
		'days'	=> 'Derniers Jours',
		'weeks'	=> 'Derni�res Semaines',
		'months'=> 'Derniers Mois'
	);
	if (!count($_GET)) $_GET['today'] = '';
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script type="text/javascript" src="<?=Page::getProtocol()?>://www.google.com/jsapi"></script>
	<script type="text/javascript">
		google.load('jquery', '1.4');
	</script>
	<script type="text/javascript">
	// <![CDATA[
	// ]]>
	</script>
	<style type="text/css">
		select,input { width: inherit;}
		label { padding-left: 2px; }
		label.col1 { padding-left: 0; float: left; width: 65px;}
		table.framed { width: 900px; }
	</style>
</head>
<body>
<h1>Statistiques R�servations</h1>
<p>Les donn�es ci-dessous correspondent au total TTC pay� hors annulation.</p>
<p>
<?

	foreach($modes as $k => $v)
	{
		if (isset($_GET[$k]))
		{
			$h1 = $v;
			echo '<strong>';
		}
		else
			echo '<a href="'.basename(__file__).'?'.$k.'">';
		echo $v;
		echo (isset($_GET[$k]) ? '</strong>' : '</a>').str_repeat('&nbsp;', 4);
	}
?>
</p>

<? 
echo "<h1>$h1</h1>";
if (isset($_GET['today'])) :
	// r�servations
	$sql = "select hour(r.paiement) Heure, paiement_mode Mode, count(*) NB, sum(r.total) CA";
	$sql.=" from reservation r";
	$sql.=" where r.paiement >= CURRENT_DATE and r.statut IN ('A','P')";
	$sql.=" group by hour(paiement), paiement_mode";
	$sql.=" order by Heure desc";
	show_table("R�servations aujourd'hui", $sql, $href, $format);
	
	// paybox
	$sql = "select hour(p.creation) Heure, 'PB' Mode, count(*) NB, count(ID3D) 3DS, (SUM(p.MONTANT)/100) CA";
	$sql.=" from paybox p";
	$sql.=" where p.creation > CURRENT_DATE and p.TYPE='00003' and CODEREPONSE='00000'";
	$sql.=" group by hour(creation)";
	$sql.=" order by Heure desc";
	show_table("PAYBOX aujourd'hui", $sql, $href, $format);
	
	// d�tails 3D Secure
	$sql = "select reservation, Amount, StatusPBX, creation";
	$sql.=" from paybox_secure";
	$sql.=" where creation > CURRENT_DATE";
	$sql.=" order by creation desc";
	show_table("3DSecure aujourd'hui", $sql, $href, $format);

endif;
if (isset($_GET['paybox_bin'])) :

	// nombre de BIN
	$sql = "select PAYS, TYPECARTE, count(*) NB";
	$sql.=" from paybox_bin";
	$sql.=" group by PAYS, TYPECARTE";
	show_table("BIN par pays et type de carte", $sql, $href, $format);
	
	// vitesse de cr�ation des BIN
	$sql = "select yearweek(creation, 3) Semaine, count(*) NB";
	$sql.=" from paybox_bin";
	$sql.=" group by yearweek(creation, 3) with rollup";
	show_table("Nouveaux BIN", $sql, $href, $format);
endif;
if (isset($_GET['paybox_1clic'])) :
	// nombre d'abonn�s 1CLIC
	if (USE_1CLIC)
	{
		show_table("Abonn�s 1Clic", "select count(*) Cartes, count(distinct(client)) Clients from paybox_abonne", $href, $format);
		show_table("Abonn�s 1Clic par semaine", "select yearweek(creation, 3) Semaine, count(*) Cartes from paybox_abonne group by yearweek(creation, 3)", $href, $format);
	}
endif;
if (isset($_GET['paybox_trans'])) :
	// erreurs PAYBOX
	$from = date('Y-m-01', strtotime('-1 month'));
	$types = array(
		Paybox::TYPE_AUTORISATION => 'Autorisation',
		Paybox::TYPE_ABT_INSCRIPTION => 'Inscription abonn�',
		Paybox::TYPE_PAIEMENT => 'Paiement',
		Paybox::TYPE_ABT_PAIEMENT => 'Paiement abonn�',
		Paybox::TYPE_ABT_SUPPRESSION => 'Suppression abonn�',
		Paybox::TYPE_ANNULATION => 'Annulation',
		Paybox::TYPE_REMBOURSEMENT => 'Remboursement'
	);
	foreach ($types as $type => $libelle)
	{
		$type = str_pad($type, 5, '0', STR_PAD_LEFT);
		$sql = "select CODEREPONSE, count(*) NB, COMMENTAIRE";
		$sql.=" from paybox";
		$sql.=" where creation >= '$from' and TYPE='$type'";
		$sql.=" group by CODEREPONSE, COMMENTAIRE";
		show_table(sprintf('PAYBOX %s (%ld) depuis %s', $libelle, $type, euro_iso($from)), $sql, $href, $format);
	}

endif;
// la base des requp�tes suivantes
$sql_base =", sum(case when r.forfait_type ='LOCAPASS' then 1 else 0 end) `NB Locapass`";
$sql_base.=", sum(case when r.forfait_type ='JEUNES' then 1 else 0 end) `NB JEUNES`";
$sql_base.=", sum(case when r.forfait_type!='LOCAPASS' then 1 else 0 end) `NB ADAFR`";
$sql_base.=", sum(case when r.forfait_type!='LOCAPASS' then r.total else 0 end) CA";
$sql_base.=", avg(case when r.forfait_type!='LOCAPASS' then r.total else null end) `Moy. CA`";
$sql_base.=", sum(case when r.forfait_type!='LOCAPASS' and r.reduction > 0 then 1 else 0 end) `NB R�ductions`";
$sql_base.=", sum(case when r.forfait_type!='LOCAPASS' and r.reduction > 0 then r.reduction else null end) `R�ductions`";
$sql_base.=", avg(case when r.forfait_type!='LOCAPASS' and r.reduction > 0 then r.reduction else null end) `Moy. R�duction`";
$sql_base.=", sum(case when r.forfait_type!='LOCAPASS' and r.yield is not null then 1 else 0 end) `NB Yields`";
$sql_base.=", sum(case when r.forfait_type!='LOCAPASS' then r.yield else null end) `Yield`";
$sql_base.=", avg(case when r.forfait_type!='LOCAPASS' then r.yield else null end) `Moy. Yield`";
$sql_base.=" from reservation r";
$sql_base.=" where r.paiement >= '%s' and r.statut IN ('A','P')";

// analyse facture
$sql_facture =", ifnull(sum(fa.montant),0) `CA ADA`";
$sql_facture.=", ifnull(sum(fc.montant),0) `CA COURTAGE`";
$sql_facture.=", ifnull(sum(fm.montant),0) `CA MATERIEL`";
$sql_facture.=", ifnull(sum(fp.montant),0) `CA PRO`";
$sql_facture.=", sum(r.total) `CA`";
$sql_facture.=" from reservation r";
$sql_facture.=" join facturation_ada fa on fa.reservation=r.id and fa.type='F'";
$sql_facture.=" left join facturation_courtage fc on fc.reservation=r.id and fc.type='F'";
$sql_facture.=" left join facturation_materiel fm on fm.reservation=r.id and fm.type='F'";
$sql_facture.=" left join facturation_pro fp on fp.reservation=r.id and fp.type='F'";
$sql_facture.=" where r.paiement >= '%s' and r.statut IN ('A','P')";

// liste des options
$sql_options ="select ro.facturation, o.theme, ro.`option`, o.nom, sum(coalesce(ro.quantite,1)) NB, sum(ro.prix) CA";
$sql_options.=" from reservation r";
$sql_options.=" join res_option ro on ro.reservation=r.id";
$sql_options.=" left join `option` o on o.id=ro.`option`";
$sql_options.=" where r.paiement >= '%s' and r.statut IN ('A','P') and ro.paiement='L'";
$sql_options.=" group by ro.facturation, o.theme, ro.`option`, o.nom";


// ... et les requ�ts puor l'analyse des promotions
$sql_promo = "select r.promotion, count(*) NB";
$sql_promo.=", sum(r.reduction) `R�ductions`, avg(r.reduction) `Moy. R�duction`";
$sql_promo.=", sum(r.total) CA, avg(r.total) `Moy. CA`";
$sql_promo.=", coalesce(apf.nom, concat('[archive]', aapf.nom), p.nom, concat('[archive]', ap.nom)) Nom";
$sql_promo.=" from reservation r";
$sql_promo.=" left join agence_promo_forfait apf on apf.id=r.promotion";
$sql_promo.=" left join _archive_agence_promo_forfait aapf on aapf.id=r.promotion";
$sql_promo.=" left join promotion_forfait p on p.id=r.promotion";
$sql_promo.=" left join _archive_promotion_forfait ap on ap.id=r.promotion";
$sql_promo.=" where r.paiement > '%s' and r.promotion is not null and r.forfait_type!='LOCAPASS'";
$sql_promo.=" group by r.promotion";
$sql_promo.=" order by `R�ductions` desc";

$hrefPromo = array('promotion' => 'agence_promo_forfait.php?id=#promotion#');

if (isset($_GET['days'])) :

	// derniers jours
	$offset = date('N')-1 + 1*7;
	$from = date('Y-m-d', strtotime("-$offset days"));
	$sql = "select date(r.paiement) Jour".sprintf($sql_base, $from)." group by date(r.paiement) order by Jour desc";
	show_table("R�servations par jour depuis ".euro_iso($from), $sql, $href, $format);
	$sql = "select date(r.paiement) Jour".sprintf($sql_facture, $from)." group by date(r.paiement) order by Jour desc";
	show_table("Facturation par jour depuis ".euro_iso($from), $sql, $href, $format);
	show_table("Options depuis ".euro_iso($from), sprintf($sql_options, $from), $href, $format);
	show_table("Promotions depuis ".euro_iso($from), sprintf($sql_promo, $from), $hrefPromo, $format);
endif;
if (isset($_GET['weeks'])) :
	// derni�res semaines
	$offset = date('N')-1 + 8*7;
	$from = date('Y-m-d', strtotime("-$offset days"));
	$sql = "select left(yearweek(creation, 3),4) `Ann�e`, right(yearweek(creation, 3),2) Semaine".sprintf($sql_base, $from)." group by yearweek(r.paiement, 3) order by yearweek(r.paiement, 3) desc";
	show_table("R�servations par semaine depuis ".euro_iso($from), $sql, $href, $format);
	show_table("Promotions depuis ".euro_iso($from), sprintf($sql_promo, $from), $hrefPromo, $format);
endif;
if (isset($_GET['months'])) :
	echo "<p>Dans chaque case, le nombre de r�servations ADAFR, le nombre de r�servations Locapass, le CA total, le CA Moyen, le total des r�ductions, le nombre de r�duction et la moyenne des r�ductions.</p>\n";
	// par mois
	$types = array('', 'VP', 'VU', 'SP', 'ML');
	$from = date('Y-01-01', strtotime("-1 year"));
	foreach ($types as $type)
	{
		$sql = "select year(r.paiement) annee, month(r.paiement) mois".sprintf($sql_base, $from);
		if ($type)
			$sql.= " and r.type='$type'";
		$sql.=" group by year(r.paiement), month(r.paiement) with rollup";
		//show_table("R�servations par mois depuis ".euro_iso($from), $sql, $href, $format);
		
		$rs = sqlexec($sql);
		if ($type) $type = " pour $type";
		echo "<h2>CA depuis $from $type</h2>\n";
		echo '<table class="framed stats" cellpadding="0" cellspacing="0" border="0">'."\n";
		echo '<tr style="background: url(img/bg_list.gif) repeat-x 0% 100%;"><th></th><th>'.join('</th><th>', range(1, 12)).'</th><th>Total</th></tr>'."\n";
		while ($row = mysql_fetch_assoc($rs))
		{
			// on n'affiche pas le cumul sur plusieurs ann�es
			if (!$row['annee'] && !$row['mois'])
				break;
			if ($annee != $row['annee'])
			{
				$mois = 0;
				$annee = $row['annee'];
				echo '<tr><th>'.$annee.'</th>';
			}
			if ($row['mois'])
			{
				if ($mois + 1 < $row['mois'])
					echo str_repeat('<td>&nbsp;</td>', $row['mois'] - $mois - 1);
				$mois = $row['mois'];
			}
			else if ($mois < 12)
			{
				echo str_repeat('<td>&nbsp;</td>', 13 - $mois - 1);
			}
			echo '<td align="right">';
			// pour le total, il n'y a pas le mois
			echo number_format($row['NB ADAFR'], 0, '.', ' ').'<br/>';
			echo '('.$row['NB Locapass'].')<br/>';
			echo '<nowrap>'.number_format($row['CA'], 0, '.', ' ').'�</nowrap><br/>';
			echo number_format($row['Moy. CA'], 0, '.', ' ').'�<br/>';
			echo '<nowrap>-'.number_format($row['R�ductions'], 0, '.', ' ').'� ('.$row['NB R�ductions'].', -'.number_format($row['Moy. R�duction'], 0, '.', ' ').'�)</nowrap>';
			echo '</td>';
			if (!$row['mois']) echo '</tr>';
		}
		echo '</table>';
	}

endif;
?>
</body>
</html>