<?
	require_once("secure.php");

	// identifiant
	$id = (int) getarg('id');

	// suppression
	if ($id && getarg("delete"))
	{
		$sql = "delete from promotion_campagne where id = '".$id."'";
		getsql($sql);
		unset($id);
	}

	// modification, cr�ation
	if (getarg("update") || getarg("create"))
	{
		if (isset($_POST['montant_base_options']) && is_array($_POST['montant_base_options'])) {
			$_POST['montant_base_options'] = join(',', $_POST['montant_base_options']);
		}
		save_record('promotion_campagne', $_POST);
		if (!$id) $id = mysql_insert_id();

	}
	// enregistrement courant
	if ($id)
	{
		$row = PromotionCampagne::factory($id, true);
		if ($row->isEmpty())
			die("Aucun enregistrement correspondant.");
		$used = $row['distribues'];
		if ($_GET['exporter'])
		{
			$row->addHisto($adminUser['login'], 'codes export�s: '.$_GET['exporter']);
			$fname = 'campagne-'.str_pad($id, 4, '0', STR_PAD_RIGHT).'-'.date('Y-m-d-H-i', ($_GET['exporter']=='TOUS' ? time() : strtotime($_GET['exporter'])));
			$sql = "select code, creation, distribution, validation, paiement from promotion_code where campagne=$id";
			if ($_GET['exporter']!='TOUS')
				$sql .=" and creation='".addslashes($_GET['exporter'])."'";
			$sql.=" order by id";
			$exporter = new SQLExporter(array('sql'=>$sql, 'fname'=>$fname, 'sqlpreserve'=>true));
			$exporter->toExcel();
			exit();
		}
		if ($_GET['distribuer'])
		{
			$sql = "update promotion_code set distribution=now() where campagne=$id and distribution is null";
			if ($_GET['distribuer']!='TOUS')
				$sql.=" and creation='".addslashes($_GET['distribuer'])."'";
			getsql($sql);
			$row->addHisto($adminUser['login'], mysql_affected_rows().' codes distribu�s : '.$_GET['exporter']);
		}

		// cr�er des novueaux codes si demand�es
		if ($nb = (int)$_POST['add_codes'])
			$row->createCodes($nb, $adminUser['login']);
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Outils d'administration</title>
<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
<link rel="stylesheet" href="style.css">
<script type="text/javascript" src="<?=Page::getProtocol()?>://www.google.com/jsapi"></script>
<script type="text/javascript">
		google.load('jquery', '1.4');
	</script>
<link rel="stylesheet" type="text/css" href="../css/anytime.min.css" />
<script type="text/javascript" src="../scripts/anytime.min.js"></script>
<script type="text/javascript" src="../scripts/script.js"></script>
<script type="text/javascript" src="admin.js"></script>
<script type="text/javascript">
	// <![CDATA[
	// doDelete
	function doDelete(frm)
	{
		return (confirm("Voulez-vous r�ellement supprimer cet �l�ment ?\nCette op�ration est irr�versible."));
	}

	// doSubmit
	function doSubmit(frm)
	{
		var err = "", msg = "";
		if (!$.trim(frm.nom.value).length)
			err += "- le nom\n";
		if (!$.trim(frm.unik.value).match(/^[a-z0-9]*$/i))
			err += "- le code unique n'est pas valide\n";
		if (!frm.franchise_montant.value.match(/^\d*$/))
			err +="- le montant n'est pas correct\n";
		if (frm.classe && !frm.classe.selectedIndex > 0)
			err += "- une classe de promotions\n";
		// v�rifier les dates
		if(frm.debut.value.length && !validateDate(frm.debut))
			err += "- la date de debut est incorrecte\n";
		if(!frm.fin.value.length)
			msg += "- sans date de fin\n";
		else if(!validateDate(frm.fin.value))
			err += "- la date de fin est incorrecte\n";
		// et v�rifier la date
		else if (checkDateFin(makeDate(frm.fin.value)))
			err += "- la date ne peut pas �tre ant�rieure � aujourd'hui ou � une date d�j� pass�e\n";
		if (!frm.montant_min.value.match(/^\d*$/))
			err += "- le montant minimum n'est pas valide\n";
		else if (frm.montant_min.value.length && !frm.montant_base.value.length)
			err += "- la base de comparaison doit �tre renseign�e\n";
		else if (!frm.montant_min.value.length && frm.montant_base.value.length)
			err += "- vous devez indiquer un montant minimum ou ne pas choisir de base\n";
		if(frm.montant_base.value=="options" && $(":selected", "#montant_base_options").length ==0)
			err += "- vous devez choisir au moins une option\n";
		if (err.length)
		{
			alert("Attention ! Veuillez pr�ciser :\n" + err);
			return false;
		}
		// v�rifier la modification du montant
		if (frm.franchise_montant.value != frm.franchise_montant.defaultValue && !confirm("Vous modifiez le montant revers� au franchis� pour cette campagne\nde "+(1*frm.franchise_montant.defaultValue)+"�\n�  "+(1*frm.franchise_montant.value)+"�\nEtes vous s�r de cette modification ?"))
			return false;
		// v�rifier la cr�ation
		if (frm.add_codes && frm.add_codes.value.length && !confirm("Voulez vous vraiment cr�er "+ frm.add_codes.value +" nouveaux codes ?"))
			return false;
		return true;
	}

	// initilisation AnyTime
	var dateFormat = '%d-%m-%Y';
	var dateOptions = getOptionsAnyTime(dateFormat, "D�but");
	var dateConverter = new AnyTime.Converter({format: dateFormat});

	// checkDateFin: renvoie true si la date n'est pas correcte, contr�le l'affichage dans le calendrier
	// si la promotion est utilis�e, la date de fin ne peut pas �tre avant le minimum entre la date de fin pr�c�dente ou aujourd'hui
	function checkDateFin(dateFin, y, m, d, h, i)
	{
		var fin = makeDate("<?=euro_iso($row['fin'])?>");
		// ne peut pas �tre inf�rieure � maintenant
		var now = new Date();
		now.setHours(0,0,0,0);
		if (fin && fin.getTime() < now.getTime())
			now = fin;
		if (dateFin && dateFin.getTime() < now.getTime())
			return true;
		return false;
	}
	// initialiser le forumaire
	$(document).ready(function()
	{
		var fin = null;
<? if (!$used) : /* on ne peut plus changer la date de d�but avant utuilisation */ ?>
		dateOptions.labelTitle = "d�but";
		AnyTime.picker('debut', dateOptions);
<? elseif ($row['fin']) : ?>
		fin = dateConverter.parse("<?=euro_iso($row['fin'])?>");
<? endif; ?>
		dateOptions.labelTitle = "fin";
		dateOptions.earliest = ((fin && fin.getTime && (fin.getTime() < today.getTime())) ? fin : today);
		AnyTime.picker('fin', dateOptions);
		// vider les dates
		$('a.cal_empty').click(function()
		{
			$("#" + this.id.substring(4)).val("").change();
			return false;
		});
		$('#serviceclient').change(function()
		{
			if (this.checked)
			{
				if (!this.form.cible.value.length)
					this.form.cible.value = "Service Client";
			}
			else if (this.form.cible.value == "Service Client")
			{
				this.form.cible.value = "";
			}
		});
		$('#add_codes').change(function()
		{
			if (!this.value.match(/^\d*$/))
			{
				alert("Vous devez indiquer un chiffre!");
				this.focus();
				return false;
			}
			return true;
		});

	    // FLA Masque le champs option par defaut
	    if ($("#montant_base").val() != 'options') {
			$('#montant_base_options_label').hide();
			$('#montant_base_options').hide();
		}
		// affiche la liste des option si "compar� �" vaut "options"
		$('#montant_base').change(function()
		{
			if (this.value == 'options') {
				$('#montant_base_options_label').show();
				$('#montant_base_options').show();
			} else {
				$('#montant_base_options > option').removeAttr("selected");
				$('#montant_base_options_label').hide();
				$('#montant_base_options').hide();
			}
		});
	});
	// ]]>
	</script>
</head>
<body>
	<form action="promotion_campagne.php" method="post" enctype="multipart/form-data" onsubmit="return doSubmit(this);">
		<input type="hidden" name="id" value="<?=$id ? $id : '';?>">
		<table class="framed" border="0" cellspacing="0" cellpadding="2">
			<tr>
			<th class="bg_titre">
				<img src="img/puce.gif" align="absmiddle" hspace="1">
				<?
				if (!$id) {
					echo "Nouvelle Campagne";
				} else {
					echo "Campagne [#".$id."]";
					if ($used) echo " ($used codes distribu�s)";
				}
				?>
			</th>
			</tr>
			<tr>
				<td>
					<table class="noborder" width="100%" cellspacing="0" cellpadding="0">
						<col width="15%">
						<col width="35%">
						<col width="15%">
						<col width="35%">
		<? if (!$id || $row['serviceclient']) : ?>
		<tr>
			<td></td>
			<td>
				<? if (!$id) : ?>
				<input type="checkbox" id="serviceclient" name="serviceclient[]" value="1" />
				<input type="hidden" name="serviceclient[]" value="0" />
				<? endif; ?>
				<label for="serviceclient">Service Client uniquement</label>
			</td>
			<td colspan="2"><em>utilis� par le Service Client pour distribuer des codes de r�duction</em></td>
		</tr>
		<? endif; ?>
		<? if (!$id || $row['automatique']) : ?>
		<tr>
			<td></td>
			<td>
				<? if (!$id) : ?>
				<input type="checkbox" id="automatique" name="automatique[]" value="1" />
				<input type="hidden" name="automatique[]" value="0" />
				<? endif; ?>
				<label for="automatique">Cr�ation automatique des codes</label>
			</td>
			<td colspan="2"><em>cr�ation des codes de r�duction � la demande</em></td>
		</tr>
		<? endif; ?>
		<tr>
			<td><label for="nom">Nom</label></td>
			<td colspan="2"><input type="text" id="nom" name="nom" value="<?=htmlspecialchars($row['nom'])?>" maxlength="64" size="64" /></td>
			<td><em>client: e-mail, facture</em></td>
		</tr>
		<tr>
			<td><label for="unik">Code Unique</label></td>
			<td colspan="2"><input type="text" id="unik" name="unik" value="<?=htmlspecialchars($row['unik'])?>" maxlength="32" size="32" /></td>
			<td><em>client: code unique permettant d'obtenir un code de r�duction</em></td>
		</tr>
		<tr>
			<td><label for="cible">Cible</label></td>
			<td colspan="2"><input type="text" id="cible" name="cible" value="<?=htmlspecialchars($row['cible'])?>" maxlength="64" size="64" /></td>
			<td><em>franchis�: clientys, admin: regrouper des campagnes</em></td>
		</tr>
		<tr>
			<td><label for="franchise_montant">Montant Franchis�</label></td>
			<td><input type="text" name="franchise_montant" id="franchise_montant" value="<?=$row['franchise_montant']?>" size="5" maxlength="3" style="text-align: right;" />�</td>
			<td colspan="2"><em>Montant revers� au franchis� lors d'une r�servation</em></td>
		</tr>
		<tr>
			<td><label for="slogan">Slogan e-mail</label></td>
			<td colspan="2"><input type="text" id="slogan" name="slogan" value="<?=htmlspecialchars($row['slogan']);?>" maxlength="64" size="64" /></td>
			<td><em>client: e-mail annulation</em></td>
		</tr>
		<tr>
			<td><label for="hotline">Hotline oblig.</label></td>
			<td colspan="3">
				<input type="hidden" name="hotline[]" value="0" />
				<input type="checkbox" id="hotline" name="hotline[]" value="1" <? if ($row['hotline']) echo ' checked';?> />
				Si coch� il est obligatoire de passer par un t�l�-conseiller pour utiliser le code (<?=IP_SERENIS.', '.IP_ADA?>)
			</td>
		</tr>
		<tr>
			<td><label for="classe">Promotions</label></td>
			<td colspan="2">
			<?
				if ($id)
					echo '<a href="lists/promotion_forfait.php?classe='.(int)$row['classe'].'">'.getsql('select nom from partenaire_classe where id='.(int)$row['classe']).'</a>';
				else
					selectsql("classe", 'select id, nom from partenaire_classe where objectif="campagne" order by nom ', (int)$_GET['classe'], '--');
			?>
			</td>
			<td></td>
		</tr>
		<tr><th colspan="4">Validit� des codes ?</th></tr>
		<tr>
			<td><label for="debut">D�but</td>
			<td>
				<input autocomplete="off" type="text" name="debut" id="debut" value="<?=euro_iso($row["debut"])?>" size="10" maxlength="10" <?=($used ? ' readonly="readonly"' : '');?>>
				<? if (!$used) : ?>
				<a class="cal_empty" href="#" id="btn_debut" style="vertical-align: middle">effacer</a>
				<? endif; ?>
			</td>
			<td><label for="fin">Fin</label></td>
			<td>
				<input autocomplete="off" type="text" name="fin" id="fin" value="<?=euro_iso($row['fin'])?>" size="10" maxlenth="10">
				<a class="cal_empty" href="#" id="btn_fin" style="vertical-align: middle">effacer</a>
			</td>
		</tr>
		<tr>
			<td><label for="montant_min">Montant minimum</label></td>
			<td><input type="text" id="montant_min" name="montant_min" value="<?=$row['montant_min'];?>" size="4" maxlength="4" />&euro;</td>
		</tr>
		<tr>
			<td><label for="montant_base">Compar� � :</label></td>
			<td>
			<?
				$a = array('forfait'=>'Forfait','forfait_complementaires' => 'Forfait et Options Compl�mentaires','materiel' => 'Mat�riel (achats uniquement)', 'demenagement' => 'Mat�riel (achats et locations)',  'total' => 'Total', 'options' => 'Options');
				selectarray('montant_base', $a, $row['montant_base'], '---');
			?>
			</td>
			<td><label id="montant_base_options_label" for="montant_base_options">Compar� � :</label></td>
			<td>
				<select id="montant_base_options" name="montant_base_options[]" multiple="multiple" size="5">
				<?php
				$test_options = array();
				if ($row && isset($row['montant_base_options'])) {
					$test_options = explode(',', $row['montant_base_options']);
				}
				$rs = sqlexec("SELECT o.id, CONCAT(o.theme,'/',LCASE(o.type),'/',LCASE(o.id),'/',o.nom) lbl from `option` o WHERE o.theme NOT IN ('annulation','assurances') ORDER BY o.position");
				while( $t = mysql_fetch_assoc($rs)) {
					echo '<option value="'.$t['id'].'"';
					if (in_array($t['id'], $test_options)) echo ' selected="selected"';
					echo '>'.$t['lbl'].'</option>';
				}
				?>
				</select>
			</td>
		</tr>
		<tr>
			<td><label for="forfait_type">Type Forfait</label></td>
			<td>
			<?
				$a = array('ADAFR' => 'ADA.FR', 'JEUNES' => 'JEUNES');
				selectarray('forfait_type', $a, $row['forfait_type'], 'Tous');
			?>
			</td>
			<td colspan="2"></td>
		</tr>
		<? if ($id) : ?>
		<tr>
			<th colspan="3">Codes associ�es � la campagne</th>
			<th style="text-align: right;"><a href="lists/promotion_code.php?campagne=<?=$id?>">voir la liste</a></th>
		</tr>
		<tr>
			<td colspan="4">
			<table cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<th>Date</th>
				<th>Cr��s</th>
				<th>Distribu�s</th>
				<th>Valid�s</th>
				<th>Utilis�s</th>
				<th>Exporter</th>
				<th>Distribuer</th>
			</tr>
			<? if ($stats = sqlexec("select g.creation, count(g.id) crees, count(g.distribution) distribues, count(g.validation) valides, count(g.paiement) utilises from promotion_campagne p left join promotion_code g on g.campagne=p.id where p.id=$id group by g.creation with rollup")) : ?>
			<? while($stat = mysql_fetch_assoc($stats)) : ?>
			<? if ($row['automatique'] && $stat['creation']) continue; ?>
			<tr>
				<td>
				<?
				if ($stat['creation'])
					echo $stat['creation'];
				else
				{
					echo '<strong>TOTAL</strong>';
					$stat['creation'] = 'TOUS';
				}
				?>
				</td>
				<? foreach(array('crees','distribues','valides','utilises') as $k) : ?>
				<td align="right"><?=$stat[$k]?></td>
				<? endforeach; ?>
				<td><input type="button" value="exporter" onclick="window.location.href='promotion_campagne.php?id=<?=$id?>&exporter=<?=urlencode($stat['creation']);?>';" /></td>
				<td>
					<? if ($stat['crees']!=$stat['distribues']) : ?>
					<input type="button" value="distribuer" onclick="if (confirm('Etes vous s�r de voulir marquer comme distribuer ces codes ?\nCette op�ration est irr�verisble.')) window.location.href='promotion_campagne.php?id=<?=$id?>&distribuer=<?=urlencode($stat['creation']);?>';" />
					<? endif; ?>
				</td>
			</tr>
			<? endwhile; ?>
			<? endif; ?>
			<tr>
				<td colspan="7">
					<? if ($row['automatique']) : ?>
					<strong>Les codes de cette campagne sont cr��s au fur et � mesure des besoins.</strong>
					<? else: ?>
					<input type="text" id="add_codes" name="add_codes" value="" size="6" maxlength="6" /> <label for="add_codes">nouveaux codes</label>
					<? endif; ?>
				</td>
			</tr>
			</table>
			</td>
		</tr>
		<? endif; ?>
		</table>
		</td>
	</tr>
	<tr>
		<td align="right">
			<input type="submit" name="<?=$id ? "update" : "create"?>" value="enregistrer"/>
			<? if ($id && !$used) : ?>
			&nbsp;
			<input type="submit" onclick="return doDelete(this.form);" name="delete" value="supprimer"/>
			<? endif; ?>
		</td>
	</tr>
	<? if ($id && !$row['automatique'] &&  ($histo = sqlexec('select creation, login, commentaire from promotion_campagne_histo where campagne='.$id.' order by id'))  && mysql_num_rows($histo)) : ?>
	<tr>
		<td>
		<table class="noborder" width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<th colspan="3">Historique des actions</th>
		</tr>
		<tr>
			<th>date</th>
			<th>qui</th>
			<th>description</th>
		</tr>
		<? while($h = mysql_fetch_assoc($histo)) : ?>
		<tr>
			<td><?=join('</td><td>', $h);?></td>
		</tr>
		<? endwhile; ?>
	</table>
	</td>
</tr>
<? endif; ?>
</table>
</form>
</body>
</html>
