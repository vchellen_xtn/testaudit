<?php
class PayboxBin extends Ada_Object
{
	static public function factory(Reservation $reservation, $numcarte, $valid, $cvv)
	{
		$x = new PayboxBin();
		if ($numcarte)
		{
			$id = substr($numcarte, 0, 6);
			$x->load($id);
			// pour l'instant on ne fait pas de requ�tes sp�cifiques pour obtenir le BIN
			if ($x->isEmpty())
			{
				// on fait une demande � PAYBOX pour obtenir les informations TYPECARTE et PAYS
				$a = Paybox::paiement($reservation->getResID(), 1, $numcarte, $valid, $cvv, Paybox::TYPE_AUTORISATION);
				if ($a['TYPECARTE'] && $a['PAYS'])
				{
					$x->setData('id', $id)
					  ->setData('TYPECARTE', $a['TYPECARTE'])
					  ->setData('PAYS', $a['PAYS'])
					  ->save();
				}
			}
		}
		return $x;
	}
}
?>
