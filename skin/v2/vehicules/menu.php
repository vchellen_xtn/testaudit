<?
	$menu = array(
		'tourisme'		=> 'Voitures de tourisme',
		'utilitaires'	=> 'Véhicules utilitaires',
		'sans_permis'	=> 'Voitures sans permis',
		'deux_roues'	=> 'Deux roues',
	);
	$action = basename($page);
?>
<ul class="menu">
	<? foreach($menu as $k => $lbl) : ?>
		<li>
			<a href="<?=$k?>.html"<?if ($action==$k) echo ' class="current"'; ?>><?=$lbl;?></a>
		</li>
	<? endforeach; ?>
</ul>