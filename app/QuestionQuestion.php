<?
class QuestionQuestion extends Ada_Object
{
	static public $THEMES = array
	(
		"l'agence"		=> "Qu'avez vous pens� de votre agence ?",
		"le v�hicule"	=> "Qu'avez vous pens� de votre v�hicule ?",
		"le site web"	=> "Votre r�servation sur ADA.fr",
		"services additionnels"	=> "Les produits d'assurance propos�s",
		"la marque"		=> "La marque ADA",
		"votre avis"	=> "Donnez nous votre avis",
	);

	/** QuestionItem_Collection */
	protected $_items;
	
	
	/**
	* Ajotue un QuestionItem � la Question
	* 
	* @param QuestionItem $item
	* @returns QuestionQuestion
	*/
	public function addItem(QuestionItem $item)
	{
		if (!$this->_items)
			$this->_items = new QuestionItem_Collection();
		$this->_items->add($item);
		return $this;
	}

	/**
	** Renvoie le th�me associ�e � la question
	** @returns string
	*/
	public function getTheme()
	{
		$theme = $this->getData('theme');
		if (isset(self::$THEMES[$theme]))
			$theme = self::$THEMES[$theme];
		return $theme;
	}
	
	/**
	* Renvoie les items d'une QuestionQuestion
	* 
	* @return QuestionItem_Collection
	*/
	public function getItems()
	{
		if (!$this->_items)
			$this->_items = QuestionItem_Collection::factory($this->_data['id']);
		return $this->_items;
	}
	/**
	* Renvoie le HTML d'une QuestionQuestion
	* 
	* @return string
	*/
	public function getHTML()
	{
		$output = '';
		if ($this->_data['type'] == 'G')
		{
			$output.= '<tr class="question question-'.$this->_data['type'].' question-'.$this->_data['id'].'">';
			$output.='<th class="libelle">'.$this->_data['libelle'].'</th>'."\n";
		}
		else if ($this->_data['type'] == 'F')
		{
			$output.= '<ul class="question question-'.$this->_data['type'].' question-'.$this->_data['id'].'">';
			$output.='<li class="libelle">'.$this->_data['libelle'].'</li>'."\n";
		}
		else if ($this->_data['type'] == 'O')
		{
			$output.= '<ul class="question question-'.$this->_data['type'].' question-'.$this->_data['id'].'">';
			$output.='<li class="libelle"><label for="question_'.$this->_data['id'].'_reponse">'.$this->_data['libelle'].'</label></li>'."\n";
			//	.'<li><input type="text" id="question_'.$this->_data['id'].'_reponse" name="question-'.$this->_data['id'].'-txt" /></li>'."\n";
		}
		if ($items = $this->getItems())
			$output.= $items->getHTML();
		$output.= ($this->_data['type'] == 'G' ? '</tr>' : '</ul>')."\n";
		return $output;
	}
	/**
	* La QuestionQuestion est-elle un nouveau type de question ?
	* 
	* @return boolean
	*/
	public function isNewType() {
		return $this->_data['prev_question_type'] != $this->_data['type'];
	}
	/**
	* La QuestionQuestion est-elle un nouveau th�me de question ?
	* 
	* @return boolean
	*/
	public function isNewTheme() {
		return $this->_data['prev_question_theme'] != $this->_data['theme'];
	}
	/**
	* La QuestionQuestion est-elle la derni�re de son type ?
	* 
	* @return boolean
	*/
	public function isLastType() {
		return $this->_data['next_question_type'] != $this->_data['type'];
	}
	/**
	* La QuestionQuestion est-elle la derni�re de son th�me ?
	* 
	* @return boolean
	*/
	public function isLastTheme() {
		return $this->_data['next_question_theme'] != $this->_data['theme'];
	}
}