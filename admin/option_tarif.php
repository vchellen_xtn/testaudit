<?
	require_once("secure.php"); 
	require_once("lib.php");

	// identifiant
	$id = (int) getarg("id");
	if (!$id)
	{
		$option = getarg('option');
		if (!$option || !preg_match('/^[a-z0-9]{3,4}$/i', $option)) 
			die('Vous devez indiquer une option !');
	}
	
	// suppression
	if ($id && getarg("delete")) 
	{
		$sql = "delete from option_tarif where id='".$id."'";
		getsql($sql);
		unset($id);
	}
	
	// modification, cr�ation
	if (getarg("update") || getarg("create"))
	{
		// seuls les administrateurs peuvent modifier la description d'une option
		if (getarg("create"))
			$_POST['zone'] = $_SESSION['ADA001_ADMIN_ZONE'];
		if ($adminUser->hasRights(Acces::SB_ADMIN))
		{
			save_record('option_tarif', $_POST);
			if (!$id) $id = mysql_insert_id();
		}
	}
	// obtenir les donn�es
	if ($id)
	{
		$sql = "select t.*, o.nom, o.tarification";
		$sql.=" from option_tarif t join `option` o on o.id=t.option";
		$sql.=" where t.id = '".$id."'";
		$rs = sqlexec($sql);
		if (!$rs || !($row = mysql_fetch_assoc($rs)))
			die("Aucun enregistrement correspondant.");
		$option = $row['option'];
	}
	else if ($option)
	{
		$rs = sqlexec("select o.nom, o.tarification from `option` o where o.id = '$option'");
		if (!$rs || !($row = mysql_fetch_assoc($rs)))
			die("Aucune option ne correspondant.");
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script>
	<!-- //
	// doDelete
	function doDelete(frm)
	{
		return (confirm("Voulez-vous r�ellement supprimer cet �l�ment ?\nCette op�ration est irr�versible."));
	}
	
	// doSubmit
	function doSubmit(frm)
	{
		var err = "";
		// v�rifier les prix
		if(!frm.prix.value.length)
			err += "Veuillez indiquer le prix\n";
		else if(!frm.prix.value.match(/\d+(\.\d+)?/))
			err += "Le prix est invalide\n";
		if (frm.jours && frm.jours.value.length && !frm.jours.value.match(/\d+/))
			err += "Le nombre de jours est invalide !\n";
		if (frm.jours_max && frm.jours_max.value.length && !frm.jours_max.value.match(/\d+/))
			err += "Le nombre de jours max est invalide !\n";
		if (frm.prix_jour && frm.prix_jour.value.length && !frm.prix_jour.value.match(/\d+(\.\d+)?/))
			err += "Le prix � la journ�e n'est pas valide !\n";
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		return true;
	}
	// -->
	</script>
</head>
<body>
<form action="" method="post" onsubmit="return doSubmit(this)">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre">
		<img src="img/puce.gif" align="absmiddle" hspace="1">Tarif des options - <?=$adminUser->getCurrentZone()->getData('nom');?>
	</th>
</tr>
<tr>
	<td>
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="20%">
		<col width="30%">
		<col width="20%">
		<col width="30%">
		<tr>
			<td>ID</td>
			<td>
			<?
				echo $id;
				echo '<input type="hidden" name="id" value="'.$id.'"/>';
				echo '<input type="hidden" name="option" value="'.$option.'"/>';
			?>
			</td>
			<td colspan="2">
				<a href="option.php?id=<?=$option?>"><?=($id ? $row['nom'] : $option)?></a>
			</td>
		</tr>
		<tr>
			<td>R�seau</td>
			<td>
			<?
				if ($row['reseau']) echo '<strong>'.$row['reseau'].'</strong>';
				else
					selectarray("reseau", array('ADA'=>'ADA','CITER'=>'CITER'), $row['reseau']);
			?>
			</td>
			<td><label for="paiement"/>Paiement</label></td>
			<td><? selectarray('paiement', Option::$MODE_PAIEMENT, $row['paiement'], '-- par d�faut --');?></td>
		</tr>
		<tr>
			<td><label for="prix">Prix</label></td>
			<td><input type="text" id="prix" name="prix" value="<?=$row['prix']?>" maxlength="6"></td>
			<td colspan="2"></td>
		</tr>
		<tr>
			<td><label for="jours">Jours inclus</label></td>
			<td><input type="text" id="jours" name="jours" value="<?=$row['jours']?>" maxlength="2"></td>
			<td><label for="jours_max">Jours max</label></td>
			<td><input type="text" id="jours_max" name="jours_max" value="<?=$row['jours_max']?>" maxlength="2"></td>
		</tr>
		<? if ($row['tarification'] == 'J') : ?>
		<tr>
			<td><label for="prix_jour">Prix Jour Supp</label></td>
			<td><input type="text" id="prix_jour" name="prix_jour" value="<?=$row['prix_jour']?>" maxlength="6"/></td>
			<td colspan="2"></td>
		</tr>
		<? endif; ?>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%">
		<tr>
			<td align="right">
		<? 
			if ($id || $adminUser->hasRights(Acces::SB_ADMIN)) { // seuls les administrateurs peuvent cr�er une nouvelle option 
		?>
				<input type="submit" name="<?=$id ? "update" : "create"?>" value="enregistrer" onclick="return doSubmit(this.form);">
		<?
			}
			if ($id && $adminUser->hasRights(Acces::SB_ADMIN)) 
			{
		?>
				&nbsp;
				<input type="submit" onclick="return doDelete(this.form);" name="delete" value="supprimer">
		<?
			}
		?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
</form>

</body>
</html>
