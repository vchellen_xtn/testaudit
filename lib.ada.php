<?
	/**
	 * Class autoload
	 * @param string $class
	 */
	function __autoload($class)
	{
		if (strpos($class, '/')!==false) return;
		$classFile = BP.'/app/'.str_replace('_', '/', $class).'.php';
		if (is_file($classFile))
			include($classFile);
	}

	/* URL pour le r�f. nat. */
	/**
	* gu_canon : obtient la valeur canon pour un type et identifiant en cachant les valeurs dans le tableau $refnat
	* @param array $refnat tableau des valeurs canon
	* @param string $type type de valeur
	* @param string $id identifiant
	* @return string
	*/
	function gu_canon (&$refnat, $type, $id)
	{
		// cas particulier du 'type'
		if ($type=='type' && !$id) return 'vehicule';
		// si non pr�cis� ou d�j� dans le cache
		if (!$type || !$id) return null;
		if (!$refnat[$type][$id])
		{
			// sinon on fait une requ�te pour remplir le cache
			$rs = sqlexec('select id, nom, canon from '.get2tbl($type)." where id='".$id."'");
			while ($row = mysql_fetch_assoc($rs))
				$refnat[$type][$row['id']] = $row;
		}
		return $refnat[$type][$id]['canon'];
	}

	/**
	* gu_annuaire : construit un lien "type" annuaire
	* @param array $a contient les param�tres de l'URL � afficher
	* @param array $refnat contient la liste des valeurs canons
	* @param string $remove un des param�tres � supprimer
	* @return string lien annuaire
	*/
	function gu_annuaire ($a, &$refnat, $remove=null)
	{
		// on supprime �ventuellement un �l�ment
		if ($remove) unset($a[$remove]);
		// puis on v�rifie la coh�rence
		if ($remove == 'zone') unset($a['region'], $a['dept'], $a['agence'], $a['marque'], $a['modele'], $a['categorie']);
		if ($remove == 'region') unset($a['dept'], $a['agence']);
		if ($remove == 'dept') unset($a['agence']);
		if ($remove == 'type') unset($a['categorie']);
		if ($remove == 'marque') unset($a['modele']);

		// cas particulier pour la racine de SERVER_ANNU
		if (SERVER_RESA!=SERVER_ANNU && count($a)==0)
			return null;

		// et on construit l'url
		// � partir des �l�ments canons
		$url = 'location_#type#_#categorie#_#modele#_#localisation#.html';
		foreach (array('type','categorie') as $k)
			$args[$k] = gu_canon($refnat, $k, $a[$k]);
		foreach (array('modele', 'marque') as $k)
		{
			$args['modele'] = gu_canon($refnat, $k, $a[$k]);
			if ($args['modele']) break;
		}
		foreach (array('agence','dept','region','zone') as $k)
		{
			$args['localisation'] = gu_canon($refnat, $k, $a[$k]);
			if ($args['localisation']) break;
		}
		// pr�parer le tableau de remplacement
		$_search = array(); $_replace = array();
		foreach ($args as $k => $v)
		{
			$_search[] = '/#'.$k.'#/';
			$_replace[] = $v;
		}
		$_search[] = '/_+/'; $_replace[] = '_';
		$_search[] = '/_\./'; $_replace[] = '.';
		// remplacer la partie variable de l'url
		return preg_replace($_search, $_replace, $url);
	}
	/**
	* renvoie la table associ�e au param�tre
	* @param string $k
	* @return string
	*/
	function get2tbl($k)
	{
		if ($k=='agence') return 'agence_alias';
		if (in_array($k, array('marque','modele','type'))) return 'vehicule_'.$k;
		return $k;
	}
	// parse un texte pour le r�f�rencement naturel
	function refnat_parse_text ($text, &$params, &$refnat)
	{
		foreach (array('type','categorie','agence','dept','localisation','region','zone','modele','marque') as $k)
			$args[$k] = $refnat[$k][$params[$k]]['nom'];
		foreach (array('agence','dept','region','zone') as $k)
		{
			if (!$args[$k]) continue;
			$args['localisation'] = $args[$k];
			break;
		}
		if (!$args['type'] && !$args['categorie'])
			$args['type'] = 'v�hicule';
		if (!$args['modele'] && $args['marque'])
			$args['modele'] = $args['marque'];
		// pr�parer le tableau de remplacement
		$_search = array(); $_replace = array();
		foreach ($args as $k => $v)
		{
			$_search[] = '/#'.$k.'#/';
			$_replace[] = $v;
		}
		$_search[] = '/\s+/'; $_replace[] = ' ';
		// remplacer la partie variable de l'url
		return preg_replace($_search, $_replace, $text);
	}

	// zone, d�partement et agence
	function gu_type ($type, $url=true, $default='voiture')
	{
		switch (strtolower($type))
		{
			case 'vp':
				return 'voiture';
			case 'vu':
				return 'utilitaire';
			case 'sp':
				return ($url ? 'sans-permis' : 'sans permis');
			case 'ml':
				return ($url ? 'a-l-heure' : "� l'heure");
			default:
				return $default;
		}
	}
	function gu_agence($row) { return 'location-voiture-'.$row['canon'].'.html'; }
	function gu_promo ($row, $type=null) { return 'location-'.gu_type($type).'-promo'.($row ? '-'.$row['canon'] : '').'.html'; }
	function gu_recherche ($row, $type=null) { return 'recherche-location-'.gu_type($type).'-'.$row['canon'].'.html'; }

	// clean_name - pour mettre un nom dans une URL
	function clean_name($name) {
		return strtr($name, "''/ ���������������", "----aaaceeeeiioouuu");
	}

	// resid - identifiant de la r�sevation pour PayBox
	function resid($r)
	{
		return ($r['canal'].resid_client($r).strtoupper($r['nom']));
	}
	// resid_client
	function resid_client($r)
	{
		return sprintf("%08d", $r["id"]).$r["agence"];
	}
	// resfacture - facture de la r�servation
	function resfacture($r)
	{
		return date("ym", $r['t_facturation'])."I".sprintf("%05d", $r['num_fact']);
	}
	// resavoir - avoir de la r�servation
	function resavoir($r)
	{
		return date("ym", $r['t_annulation'])."I".sprintf("%05d", $r['num_avoir']);
	}

	// recherche fulltext
	function where_fulltext($fields, $search)
	{
		// on enl�ve les apostrophes
		$search = str_replace("'", " ", filter_chars($search));
		// on calcule la cha�ne pour le mode boolean
		$s = '';
		// s'il y a certains caract�res (+, ", ..;) dans la cha�ne on la parse
		if (strstr($search, '+') || strstr($search, '"'))
		{
			$words = explode(' ', $search); $open_quote = false;
			foreach ($words as $w)
			{
				$c = substr($w, ($open_quote ? -1 : 0), 1);
				$op = ($open_quote || strstr('-+', $c)) ? ' ' : ' +';
				if ($c == '"') $open_quote = !$open_quote;
				$s .= $op.$w;
			}
		}
		else
		{ // on rajoute des guillemets autour de l'expression
			$s = '"'.$search.'"';
		}
		return " MATCH(".$fields.") AGAINST ('".mysql_escape_string(trim($s))."' IN BOOLEAN MODE)";
	}

	/**
	* canonize : donne une forme canonique
	* @param string $str cha�ne � canoniser
	* @param string $tbl optionnel, table pour v�rifier l'unicit�
	* @return string
	*/
	function canonize($str, $tbl = null)
	{
		$canon = trim(preg_replace('/[^a-z0-9]+/', '-', strtolower(filter_chars($str))), '-');
		if ($tbl)
		{
			// pr�parer la requ�te SQL
			$sql = "select count(*) from $tbl where canon='%s'";
			if ($tbl == 'agence_alias') // pour les agences on limite aux agences actuellement ouverte
				$sql .=' and (statut&1)=1';
			// chercher le premier canon disponible
			$j = 0; $x = $canon;
			while (getsql(sprintf($sql, $x)))
			{
				$j++;
				$x = $canon.'-'.$j;
			}
			$canon = $x;
		}
		return $canon;
	}

	// convertit une chaine en utf8
	function u8($string)
	{
		if (is_string($string))
		{
			$string = str_replace('�', "'", $string);
			return iconv('WINDOWS-1252', 'UTF-8', $string);
		}
		else if (is_array($string))
			return array_map('u8', $string);
		else
			return $string;
		
	}

	// convertit une chaine en utf8
	function hn($n, $text, $color='000000', $size=16, $width=-1, $image=true)
	{
		if ( $image ) {
			$alt = stripslashes(strip_tags($text));
			//return '<h'.$n.'><img src="../img/index.php?t='.urlencode($text).'&amp;c='.$color.'&amp;s='.$size.'&amp;w='.$width.'" class="titre" alt="'.$alt.'"/></h'.$n.'>';
			return '<h'.$n.' style="background-image:url(../img/index.php?t='.urlencode($text).'&amp;c='.$color.'&amp;s='.$size.'&amp;w='.$width.');" class="hn">'.$alt.'</h'.$n.'>';
		}
		return '<h'.$n.'>'.$text.'</h'.$n.'>';
	}

	// insertSWF
	function insertSWF($swf_path, $width, $height, $flashvars = '', $version = 10, $wmode = 'transparent', $bgcolor = '#000000', $texte = '')
	{
		if (!$texte)
			$texte = '<p>Ce contenu requiert Adobe Flash Player 9. <a href="http://www.macromedia.com/go/getflash/" target="_blank">Obtenir Flash Player 9</a></p>';
		$swf_path = preg_replace('/\.swf$/', '', $swf_path);
		$id = str_replace('-', '_', canonize($swf_path));
		return "
		<div class='$id'>
			<script type='text/javascript'>
				if (typeof AC_FL_RunContent === 'undefined' || typeof DetectFlashVer === 'undefined') {
					alert('Cette page n�cessite le fichier AC_RunActiveContent.js.');
				} else {
					var hasRightVersion = DetectFlashVer($version, 0, 0);
					if(hasRightVersion)  // si nous avons d�tect� une version acceptable
					{
						// int�grer le clip Flash
						AC_FL_RunContent(
							'codebase', 'http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=$version,0,0,0',
							'id', '$id',
							'name', '$id',
							'src', '$swf_path',
							'height', '$height',
							'width', '$width',
							'movie', '$swf_path',
							'flashvars', '$flashvars',
							'wmode', '$wmode',
							'quality', 'high',
							'pluginspage', 'http://www.macromedia.com/go/getflashplayer',
							'align', 'middle',
							'play', 'true',
							'loop', 'true',
							'scale', 'showall',
							'devicefont', 'false',
							'menu', 'true',
							'bgcolor', '$bgcolor',
							'allowFullScreen', 'true',
							'allowScriptAccess','sameDomain',
							'salign', ''
							); //end AC code
					} else {  // version Flash trop ancienne ou d�tection du plug-in impossible
					var alternateContent = '<div class=\"contenu-alternatif\">$texte</div>'
					document.write(alternateContent);  // Ins�rer contenu non-Flash
				}
				}
			</script>
			<noscript>
				<object type='application/x-shockwave-flash' data='$swf_path.swf?$flashvars' width='$width' height='$height'>
					<param name='movie' value='$swf_path.swf?$flashvars' />
				</object>
			</noscript>
		</div>";
	}

	if (!function_exists('jour_iso')) {
		function jour_iso($ts)
		{
			$j = date("w", $ts);
			return ($j == 0) ? 7 : $j;
		}
	}

	if (!function_exists('show_money')) {
		function show_money($x)
		{
			$prec = (round($x) == $x) ? 0 : 2;
			return number_format($x, $prec, ",", " ");
		}
	}
?>