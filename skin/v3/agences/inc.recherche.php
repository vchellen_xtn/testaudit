<div class="round-box grey-background">
	<h2 class="hn-like h2-like small inblock valign-baseline">Nos agences de location de voiture en France</h2>
	<a href="#" id="btn-geoloc" class="btn-geoloc valign-baseline">Me géolocaliser</a>

	<form action="../agences/liste.html" method="post" id="frm_agence" class="form-inline">
		<label for="ville">Recherchez les agences les plus proches d'une ville ou d'une adresse</label>
		<div id="autocomplete" class="inblock valign-baseline">
			<input type="text" id="ville" name="ville" value=""/>
		</div>
		<input type="hidden" id="cp" name="cp"/>
		<input type="hidden" id="longitude" name="lon" value=""/>
		<input type="hidden" id="latitude" name="lat" value=""/>
		<button type="submit" id="validez" class="btn-default">lancez la recherche</button>
	</form>
</div>

<script type="text/javascript" language="javascript">
//<![CDATA[ 
	// prévenir si la géolocalisation ne fonctionne pas
	function handleNoGeolocation() {
		var msgNoGeolocation = "Désolé, nous n'avons pas pu vous localiser !";
		appADA.hideOverlayLoader();
		alert(msg);
	}
	
	// onload
	$(document).ready(function()
	{
		// géolocalisation
		$("#btn-geoloc").click(function() {
			appADA.showOverlayLoader();
			// on recherche avec la géolocalisation par le navigteur
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(function(position)
				{
					if (position.coords) {
						$("#latitude").val((position.coords.latitude *  Math.PI) / 180);
						$("#longitude").val((position.coords.longitude *  Math.PI) / 180);
						$("#frm_agence").submit();
					} else {
						handleNoGeolocation();
					}
				}, function() {
					handleNoGeolocation();
				});
			} else {
				// Browser doesn't support Geolocation
				handleNoGeolocation();
			}
			return false;
		});
		
		// validation du formulaire
		$("#frm_agence").submit(function() {
			if (this.lat.value.match(/^\-?\d+(\.\d+)?$/) && this.lon.value.match(/^\-?\d+(\.\d+)?$/)) {
				this.ville.value = "";
				this.cp.value = "";
				return true;
			} else if (this.cp.match(/^\d{5}$/)) {
				this.ville.value = "";
				this.lat.value = "";
				this.lon.value = "";
				return true;
			} else if ($.trim(frm.ville.value).length > 0) {
				this.cp.value = "";
				this.lat.value = "";
				this.lon.value = "";
				return true;
			}
			$(this.ville).focus();
			return false;
		});
		
		// recherche sur la ville
		$('#autocomplete').tagdragon({
			'field': 'ville',
			'url': '../json.ville.php',
			'max': 30,
			'charMin': 3,
			'delay': 100,
			'onSelectedItem': function(val) { $('#cp').attr('value', val.cp); }
		});
		$('#ville').focus();
		
		// toggles des départements
		$('#menu_dept a.toggle').each(function(i)
		{
			if ( i == 0 )
			{
				$(this.hash).show();
				$(this).addClass("current")
			} else {
				$(this.hash).hide();
			}
		}).click(function()
		{
			$('#menu_dept a.toggle').removeClass('current');
			$(this).addClass("current");
			$(this.hash).show().siblings('.dept').hide();
			return false;
		});
		//toggle destinations
		$('.titre-autres-destinations').click(function()
		{
			$('.autres-destinations').addClass('current');
			$('.titre-autres-destinations').addClass('current');
			$('.departements').removeClass('current');
			$('.titre-departements').removeClass('current');
			return false;
		});
		$('.titre-departements').click(function()
		{
			$('.departements').addClass('current');
			$('.titre-departements').addClass('current');
			$('.autres-destinations').removeClass('current');
			$('.titre-autres-destinations').removeClass('current');
			return false;
		});
	});
// ]]>
</script>