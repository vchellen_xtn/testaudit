<?

class Pays_Collection extends Ada_Collection
{
	/**
	* Cr�� une collection de zones
	*
	* @return Pays_Collection
	*/
	public static function factory($ids = null)
	{
		if ($ids && !is_array($ids))
			$ids = explode(',', $ids);
		// on cr�e la collection qui sera retourn�e
		$x = new Pays_Collection();
		// SQL pour les applications
		$sql = "SELECT id, nom FROM pays";
		if ($ids)
			$sql.=" WHERE id IN ('".join("','", $ids)."')";
		$sql.=" ORDER BY nom";
		$x->_loadFromSQL($sql);
		return $x;
	}
}
?>