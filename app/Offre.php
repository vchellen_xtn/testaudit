<?
/**
* Chass Offre contient :
* 	categorie	identifiant de la cat�gorie
* 	type		type de la cat�gorie
* 	mnem		mn�monique de la cat�gorie
* 	nb			nombre de v�hicules disponibles ADAFR
* 	indispo		uniquement si ajout� car non trouv� dans la base
* 	age			�ge pour cette cat�gorie
* 	permis		ann�e de permis pour cette cat�gorie
* UNIPRO / RESAS
* 	max					nombre maximum de v�hicules
* 	unipro_vehicules	liste des v�hicules pour cette cat�gorie
*/
class Offre extends Ada_Object
{
	/** @var Offre_Collection */
	protected $_collection = null;
	
	/**
	* Renvoie le forfait associ�e � l'offre
	* @returns Forfait
	*/
	public function getForfait()
	{
		if ($this->_collection) 
			return $this->_collection->getForfait($this->_data['categorie']);
	}
	public function setCollection(Offre_Collection $collection)
	{
		$this->_collection = $collection;
		return $this;
	}
}
?>
