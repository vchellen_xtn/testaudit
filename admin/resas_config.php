<?
	session_start();
	require_once('secure.php'); 
	require_once('lib.php');
	// remettre � z�ro
	if ($_POST['reset'])
	{
		$_POST = array();
		$_SESSION['admin_resas_config'] = array();
	}
	// faire une recherche
	if (count($_POST))
		$_SESSION['admin_resas_config'] = $_POST;
	else if ($_SESSION['admin_resas_config'])
	{
		foreach($_SESSION['admin_resas_config'] as $k => $v)
			$_POST[$k] = $v;
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script type="text/javascript" src="<?=Page::getProtocol()?>://www.google.com/jsapi"></script>
	<script type="text/javascript">
		google.load('jquery', '1.4');
	</script>
	<link rel="stylesheet" type="text/css" href="../css/anytime.min.css" />
	<script type="text/javascript" src="admin.js"></script>
	<style type="text/css">
		select,input { width: inherit;}
		label { padding-left: 2px; }
		label.col1 { padding-left: 0; float: left; width: 65px;}
	</style>
</head>
<body>
<?= SQLExporter::getForm(); ?>
<form action="" method="post">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre">
		<img src="img/puce.gif" align="absmiddle" hspace="1">
		Recherche Configuration Soci�t�s (R�sas)
	</th>
</tr>
<tr>
	<td>
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<tr><td>
		<fieldset><legend>Identification</legend>
			<p>
				<label class="col1" for="id">Agence</label><input type="text" value="<?=htmlspecialchars($_POST['id'])?>" id="id" name="id" size="7" maxlength="6"/>
			</p>
			<p>
				<label class="col1" for="cp">Code postal</label><input type="text" value="<?=htmlspecialchars($_POST['cp'])?>" id="cp" name="cp" size="5" maxlength="5"/>
				<label for="ville">Ville</label><input type="text" value="<?=htmlspecialchars($_POST['ville'])?>" id="ville" name="ville" size="32" maxlength="64"/>
			</p>
		</fieldset>
		<fieldset><legend>Localisation</legend>
			<p>
				<label class="col1" for="zone">Zone</label><? selectsql('zone', "select id, nom from zone order by nom", $_POST['zone'], '---');?>
				<label for="code_societe">Soci�t�</label><? selectsql('code_societe', "select distinct code_societe, concat(code_societe, ' - ', societe) from agence where reseau='ADA' and statut&1=1 and nullif(code_societe,'') is not null order by code_societe", $_POST['code_societe'], '---');?>
			</p>
		</fieldset>
		</td></tr>
		</table>
	</td>
</tr>
<tr>
	<td align="right">
		<input type="submit" name="search" value="rechercher" onclick="return doDisplay(this.form, 1);">
		&nbsp;<input type="submit" name="reset" value="effacer">
	</td>
</tr>
<tr>
	<td>
	<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
	<?
		// champ des agences
		$flds_agence = array('zone','cp','ville');
		$flds_form = array('reset','search','date_from','date_to','nb_rows','from_row','key','order');
	
		if (!$_POST['key']) $_POST['key'] = 'id';
		if (!$_POST['order']) $_POST['order'] = 'asc';
		// cr�er le sql
		$sql = "SELECT a.code_base, a.code_societe, a.societe, a.franchise, MIN(a.id) agence, c.oav_acces oav, c.promo_simple promo, c.promo_early early, c.promo_yield yield";
		$sql.=" FROM agence a";
		$sql.=" JOIN agence_pdv ap ON ap.agence=a.id";
		$sql.=" LEFT JOIN agence_config c ON c.agence=a.code_societe";
		$sql.=' WHERE '.count($_SESSION['admin_resas_config']);
		$sql.=" AND a.reseau='ADA' AND a.statut&1=1";
		foreach ($_POST as $k => $v)
		{
			if (in_array($k, $flds_form) || !$v) continue;
			if (!preg_match('/^[a-z0-9_]+$/i', $k)) continue;
			// pour les agences
			if (in_array($k, $flds_agence))
				$sql.= " AND ap.$k LIKE '".addslashes($v)."%'";
			else {
				$sql.=" AND a.$k='".addslashes($v)."'";
			}
		}
		$sql.=' GROUP BY a.code_base, a.code_societe, a.societe, a.franchise, c.promo_simple, c.promo_early, c.promo_yield';
		$sql.=' ORDER BY a.code_base, a.code_societe';
//echo '<pre>'.$sql.'</pre>';
		// requ�te SQL
		$rs = sqlexec($sql);
		$cnt = mysql_num_rows($rs);
		echo "<caption>$cnt soci�t�s</caption>";
	
		// afficher les r�sultats
		while ($row = mysql_fetch_assoc($rs))
		{
			$agence =$row['agence'];
			unset($row['agence']);
			if (!$first)
			{
				$first = true;
				
				// export des clients
				$fld_export = 'a.code_base, a.code_societe, a.societe, a.franchise, c.promo_simple promo, c.promo_early early, c.promo_yield yield';
				$tbl_export = '';
				$sql_config_export = preg_replace("/^select\s+(.+)from\s+(.+)\s+where /i", "select $fld_export\n from \\2 $tbl_export\nwhere ", $sql);

				// les boutons pour exporter
				echo '<tr><td colspan="'.count($row).'" align="right">';
					echo SQLExporter::getButton('export config', 'resas_config', $sql_config_export);
				echo '</td></tr>';
				foreach(array_keys($row) as $k) {
					echo '<th>'.$k.'</th>';
				}
				echo '</tr>';
			}
			if ($adminUser) {
				$key = AccesAgence::getKey('admin', $adminUser['login'], $agence);
				$href = sprintf('../resas/admin/config.html?role=%s&login=%s&agence=%s&key=%s', 'admin', $adminUser['login'], $agence, $key);
				$row['code_societe'] = '<a href="'.$href.'" target="_blank">'.$row['code_societe'].'</a>';
			}
			echo '<tr><td>'.join('</td><td>', $row).'</td></tr>';
		}
	?>
	</table>
	</td>
</tr>
</table>
</form>
</body>
</html>
