<?
// on renvoie du texte et sans timeout

header('Content-Type: text/plain; charset=iso-8859-1');
set_time_limit(0);

// si appel� en ligne de commandes...
if (!isset($_SERVER['SERVER_NAME']))
{
	foreach ($_SERVER['argv'] as $arg)
	{
		list($key, $val) = explode('=', $arg);
		$_GET[$key] = $val;
	}
}

// includes
$dir = dirname(__file__);
include ($dir.'/constant.php');

define('PROJECT_ID', 'ADA001');
define('EMAIL_CRON_REPORT', EMAIL_REPORT);
if (!$_SERVER['DOCUMENT_ROOT'])
{
	// on change le SCRIPT_NAME pour que get_path() fonctionne lors des envois d'e-mails..
	$_SERVER["SCRIPT_NAME"] = '/cron.php';
	// on red�finit le r�pertoire courant
	@chdir($dir);
} else if ($_SERVER['REMOTE_ADDR'] && !in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1', IP_RND, IP_ADA))) {
	// on n'autorise que certaines IPs � d�clencher le cron.php
	header('HTTP/1.0 403 Forbidden');
	die('You are not allowed !');
	exit();
}

/* on r�cup�re la liste des op�rations possibles
	2ndvoucher	e-mails de relance
	questionnaire envoi de l'inviation � remplir le questionnaire
	unipro		relance des r�servations et annulations UNIPRO
	agences		importation des agences
	horaires	importation des horaires
	fermetures	importation des fermetures
	jauges		importation des jauges
	geoloc		g�olocalisation
	refnat		canon, map.txt, etc
	satisfaction calcul des satisfactions par agence
	archive		archive les donn�es inutiles
	clients		informe les agences des clients � relancer
*/
$options = $_GET['op'] ? explode(",", $_GET['op']) : null;

// on ex�cute le Cron
$cron = new CronADA($dir.'/cache/logs/', $options);
$cron->run();

?>
