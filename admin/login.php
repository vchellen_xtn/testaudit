<?
	session_start();
	require_once('../constant.php');
	require_once('../lib/lib.util.php');
	require_once('../lib/lib.sql.php');

	// forcer le passage en HTTPS si posible
	if (SERVER_NAME!=SERVER_ANNU && USE_HTTPS && $_SERVER['HTTPS']!='on')
	{
		header('Location: https://'.SERVER_NAME.$_SERVER['SCRIPT_NAME'].($_SERVER['QUERY_STRING'] ? '?'.$_SERVER['QUERY_STRING'] : ''));
	}

	if ($_GET['force'])
	{
		// s'assurer que le collie soit disponible � la racine
		$_SERVER['SCRIPT_NAME'] = rtrim(dirname(dirname($_SERVER['SCRIPT_NAME'])),'/').'/'.basename($_SERVER['SCRIPT_NAME']);
		Page::setCookie('ADA001_ADMIN_AUTH', null);
	}
	else
	{
		$login = $_POST['login'];
		$pwd = $_POST["password"];
	
		if ($login)
		{
			if ($adminUser = Acces::factory($login))
			{
				if ($adminUser->checkPassword($pwd))
				{
					// s'assurer que le collie soit disponible � la racine
					$_SERVER['SCRIPT_NAME'] = rtrim(dirname(dirname($_SERVER['SCRIPT_NAME'])),'/').'/'.basename($_SERVER['SCRIPT_NAME']);
					Page::setCookie('ADA001_ADMIN_AUTH', $adminUser->getCookie());
					$url = $_POST["url"];
					if (!$url)
						$url = "index.php?";
				}
			}
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<title>Outils d'administration ADA.fr</title>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" /> 
	<script type="text/javascript">
	// <![CDATA[
	if (window.top && window.top != window)
	{
		window.top.location.href = window.location.href;
	}
<?
		if (isset($url)) echo 'location.href = "'.$url.'";';
?>
	// ]]>
	</script>
	<style type="text/css">
	html, body { height: 100%;	width: 100%; overflow: no;	}
	body { font-family: trebuchet ms,Tahoma, Verdana, Arial; color: #444444; font-size: 0.7em; margin: 0; padding: 0; background: white url(img/bg_page.gif) }
	
	form { margin:0px; }
	#header { background: url(img/bg_header.jpg) repeat-x 0% 0%; width: 100%; height: 46px;	}
	
	#header img { margin-top:1px; }
	
	#contenu { height: 90%;	width: 100%; margin: 0;	padding: 0;	overflow: no; text-align:center; }
	#bloc {	background-color:white;	height:250px; width:622px; margin-left: auto; margin-right: auto; text-align: left;	}
	#bloc_haut { border: solid;	border-width: 0px 1px; border-color: transparent #EBEBEB; text-align:center; padding-bottom:50px; }
	#bloc_bas { background:url(img/login02.gif) repeat-x 0% 0%; height:35px; }
	#bloc_form	{ background:url(img/login05.gif) repeat-x 0% 0%; height:122px; width:552px; margin-left: auto; margin-right: auto;	}
	#login, #password { background-color:white; border: 1px solid #979797; }
	</style>
</head>

<body>
<div id="header">
	<img src="img/logo.png" hspace="10" alt="logo ADA">
</div>
<div id="contenu">
		<div id="bloc">
			<div id="bloc_haut">
				<img src="img/login04.gif">
				<div id="bloc_form">
					Bienvenue
					<p>
					<form action="login.php" method="post">
						<input type="hidden" name="url" value="<?=$_POST["url"]?>">
						<table border="0" cellspacing="2" cellpadding="2" align="center">
						<tr>
							<td nowrap align="right">
								<label for="login">Nom d'utilisateur</label>&nbsp;&nbsp;</td>
							<td align="left">
								<input type="text" id="login" name="login" value="" maxlength="32">
							</td>
						</tr>
						<tr>
							<td nowrap align="right">
								<label for="password">Mot de passe</label>&nbsp;&nbsp;</td>
							<td align="left">
								<input type="password" name="password" id="password" value="" maxlength="32">
								<input type="image" alt="Cliquez pour continuer" src="img/continuer.gif" border="0" align="absmiddle">
							</td>
						</tr>
						</TABLE>
					</form>
				</div>
			</div>
			<div id="bloc_bas">
				<img src="img/login01.gif" style="float:left">
				<img src="img/login03.gif" style="float:right">
			</div>
		
		</div>
</div>

</body>
</html>

