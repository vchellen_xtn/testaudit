<?php
class Tracking_GoogleConversion
{
	static public function doTracking(Page $page)
	{
		//Google Conversion Code pour finalisation e-commerce
		$id = $page->getId();
		if (in_array($id, array('reservation/validation','jeunes/validation')) && (/** @var Reservation */ $reservation = $page->getData('reservation')) && !$reservation->isLocapass() && $reservation['statut']=='P') :
			// conversion Google AdWords
			$google_conversion_id = '989271275';
			$google_conversion_label = 'hOBjCJ2RzAgQ66nc1wM';
			$google_conversion_value = $reservation['total'];
?>
			<!-- Google Code for Achat VOD Conversion Page -->
			<script type='text/javascript'>
			/* <![CDATA[ */
				var google_conversion_id = <?=$google_conversion_id?>;
				var google_conversion_language = 'fr';
				var google_conversion_format = '2';
				var google_conversion_color = 'ffffff';
				var google_conversion_label = '<?=$google_conversion_label?>';
				var google_conversion_value = <?=$google_conversion_value?>; // valeur dynamique extraite de la commande
			/* ]]> */
			</script>
			<? if (SITE_MODE == 'PROD') : ?>
			<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
			<noscript>
				<div style="display: inline;"><img height="1" width="1" style="border-style: none;" alt="" src="//www.googleadservices.com/pagead/conversion/<?=$google_conversion_id?>/?label=<?=$google_conversion_label?>&amp;guid=ON&amp;script=0" /></div>
			</noscript>
			<? endif;
		elseif (!$page->isSkin('resas')): /* sur toutes les autres pages on met le tag de remarketing */ ?>
			<!-- Code Google de la balise de remarketing -->
			<script type="text/javascript">
			/* <![CDATA[ */
				var google_conversion_id = 989271275;
				var google_custom_params = window.google_tag_params;
				var google_remarketing_only = true;
			/* ]]> */
			</script>
			<? if (SITE_MODE == 'PROD') : ?>
			<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
			<noscript>
				<div style="display: inline;"><img height="1" width="1" style="border-style: none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/989271275/?value=0&amp;guid=ON&amp;script=0" /></div>
			</noscript>
			<? endif; ?>
<?		endif;
		//!Google Conversion Code pour finalisation e-commerce
		//Google Conversion Code pour le remarketing Homebox - 2014-02-24
		if (in_array($id, array('reservation/resultat', 'jeunes/resultat')) && (/** @var Reservation */ $reservation = $page->getData('reservation')) && strtolower($reservation['type'])=='vu') :
?>
			<!-- Google Code for Tag ADA -->
			<!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide: google.com/ads/remarketingsetup -->
			<script type="text/javascript">
			/* <![CDATA[ */
				var google_conversion_id = 1071806241;
				var google_conversion_label = "vOiPCNr1rAQQoe6J_wM";
				var google_custom_params = window.google_tag_params;
				var google_remarketing_only = true;
			/* ]]> */
			</script>
			<? if (SITE_MODE=='PROD') : ?>
			<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
			<noscript>
				<div style="display: inline;"><img height="1" width="1" style="border-style: none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1071806241/?value=0&amp;label=vOiPCNr1rAQQoe6J_wM&amp;guid=ON&amp;script=0" /></div>
			</noscript>
			<? endif;
		endif;
		//!Google Conversion Code pour le remarketing
		return;
	}
}

