/*
* Affiche une liste d'agences sur un plan Google Map
* 	map		Google Map
* 	center	{point: GLatLng, title: titre associ�e}
* 	agences	tableau d'objet JSON {id, nom, lat, lon, html}
* 	fn		callback � appeler sur l'�v�nement click
* Renvoie GLatLngBounds � jour
*/

var infowindow = null;
function createMarkersFromAgences(map, center, agences, fn)
{
	var bounds = new google.maps.LatLngBounds();
	var m, p;
	if (fn && typeof(fn)!='function')
		fn = null;

	// ajouter le point central s'il est d�fini
	if (center && center.point)
	{
		if (!center.icon)
		{
			var image = new google.maps.MarkerImage("//maps.google.com/intl/fr_ALL/mapfiles/marker_green.png",
				new google.maps.Size(20, 34),
				new google.maps.Point(0,0),
				// The anchor for this image is the base of the flagpole at 0,32.
				new google.maps.Point(0, 34)
			);
			var shadow = new google.maps.MarkerImage('//www.google.com/mapfiles/shadow50.png',
				// The shadow image is larger in the horizontal dimension
				// while the position and offset are the same as for the main image.
				new google.maps.Size(37, 34),
				new google.maps.Point(0,0),
				new google.maps.Point(0, 34)
			);
		}
		bounds.extend(center.point);
		option = ({
			position: center.point,
			icon: image,
			shadow: shadow,
			title: center.title
		})
		create_marker(map, option, fn);
	}

	// pr�paration de l'ic�ne ADA
	var baseIcon = new google.maps.MarkerImage("../img/gmap_marker.png",
		new google.maps.Size(12, 18),
		new google.maps.Point(0,0),
		// The anchor for this image is the base of the flagpole at 0,32.
		new google.maps.Point(6, 18)
	);

	// affichage des agences
	for (var i = 0; i < agences.length; i++)
	{
		p = new google.maps.LatLng(agences[i].lat, agences[i].lon);
		bounds.extend(p);
		options = ({
			position: p,
			icon: baseIcon ,
			title: agences[i].nom,
			autoPan:true,
			agence_id: agences[i].id,
			html : agences[i].html
		});
		create_marker(map, options, fn);
	}
	return bounds;
}

// cr�er un marker
function create_marker(map, options, fn)
{
	var pushPin = new google.maps.Marker({ map: map });
	pushPin.setOptions(options);
	google.maps.event.addListener(pushPin, 'click', function()
	{
		if (infowindow)
			infowindow.close();
		infowindow = new google.maps.InfoWindow({ content: options.html});
		infowindow.open(map, pushPin);
		if (fn) fn(this);
	});
}

// charger le polygone � d�ployer sur la map
function load_poly(map, dept, zoom)
{
	var limites = new google.maps.LatLngBounds();
	var file = "../fichiers/gmp/"+dept+".gmp";
	$.get(file, function(data)
	{
		if((0!=data.length))
		{
			var iStart = 0;
			var arePoints = false;
			var polylineLevels = "";
			do
			{
				var iEnd = data.indexOf("<br>",iStart);
				if( false == arePoints)
				{
					//Levels
					polylineLevels = data.substring(iStart,iEnd);
					arePoints = true;
				}
				else
				{
					var polylineEncodee = data.substring(iStart,iEnd);
					/**
					 * La m�thode statique decodePath() retourne un tableau de LatLng
					 * polylineDecodee est donc un tableau que l'on va parcourir
					 * afin d'optimiser l'affichage de la polyline
					 */
					var polylineDecodee = google.maps.geometry.encoding.decodePath(polylineEncodee);
					for(i=0; i<polylineDecodee.length; i++){
						limites.extend(polylineDecodee[i]);
					}
					map.fitBounds(limites);
					var optionsPolyline = {
						map: map,
						path: polylineDecodee,
						zoom: zoom,
						strokeColor: '#CC0000',
						strokeOpacity: 1.0,
						strokeWeight: 2,
						fillColor: '#CC0000',
						fillOpacity: 0.3
					};
					/**
					 * Cr�ation et affichage de la Polyline
					 */
					var polygone = new google.maps.Polygon(optionsPolyline);
				}
				iStart = iEnd + 4;//jump above '<br>' to the next polylines
			}while( data.length > iStart)
		}
	});
}
