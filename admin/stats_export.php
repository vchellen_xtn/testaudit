<?
	require_once('../constant.php');
	require_once("secure.php"); 
	require_once("lib.php");
	if (!$adminUser->hasRights(Acces::SB_ADMIN))
		die("Vous n'avez pas les droits pour voir cette information !");
	/* tableau des exportations possibles */
	$EXPORT_LIST = array
	(
		'agence_pdv' => array
		(
			'fname'		=> 'agence_pdv',
			'title'		=> 'Liste des points de vente ADA et Partenaires visibles',
			'sql'		=> "select ap.id, ap.zone, z.nom zone_nom, a.reseau, zg.nom groupe, aa.nom"
							.", case ap.emplacement when 1 then 'Ville' when 2 then 'Gare' when 3 then 'A�roport' else 'Autre' end emplacement"
							.", case when a.statut&2=2 then 'oui' else 'non' end resaweb"
							.", CASE WHEN ap.photo IS NOT NULL THEN 'oui' ELSE 'non' END photo"
							.", COALESCE(ac.tarifs_vp, 'ADAFR') tarifs_vp"
							.", COALESCE(ac.tarifs_vu, 'ADAFR') tarifs_vu"
							.", COALESCE(ac.tarifs_sp, 'ADAFR') tarifs_sp"
							.", ap.surcharge_vp, ap.surcharge_vu"
							.", ap.vp_delai_min, ap.vu_delai_min"
							.", ap.satisfaction_reponses, ap.satisfaction_commentaires, ap.satisfaction_agence, ap.satisfaction_vehicule"
							.", ap.adresse1, ap.adresse2, ap.cp, ap.ville, a.tel"
							.", DEGREES(ap.latitude) latitude, DEGREES(ap.longitude) longitude"
							.", concat('http://www.ada.fr/location-voiture-',aa.canon,'.html') url"
							.", concat('http://www.ada.fr/',ap.photo) photo_url"
		 				." from agence_pdv ap"
						." join agence a on a.id=ap.agence"
						." join agence_alias aa on aa.id=ap.id"
						." left join agence_config ac on ac.agence=a.code_societe"
						." join zone z on z.id=a.zone"
						." left join zone_groupe zg on zg.id=a.groupe"
						." where (a.statut&1)=1 and ap.publie=1"
						." order by a.zone, ap.cp, aa.nom"
		),
		'agence_alias' => array
		(
			'fname'		=> 'agence_alias',
			'title'		=> 'Liste des alias et points de vente ADA et Partenaires visibles',
			'sql'		=> "select aa.agence, aa.pdv, aa.id alias, ap.zone, z.nom zone_nom, a.reseau, zg.nom groupe, aa.nom"
							.", case aa.emplacement when 1 then 'Ville' when 2 then 'Gare' when 3 then 'A�roport' else 'Autre' end emplacement"
							.", case when a.statut&2=2 then 'oui' else 'non' end resaweb"
							.", CASE WHEN ap.photo IS NOT NULL THEN 'oui' ELSE 'non' END photo"
							.", CASE WHEN aa.refnat IS NOT NULL THEN 'oui' ELSE 'non' END texte"
							.", CASE WHEN aa.pageweb=1 THEN 'oui' ELSE 'redirection' END pageweb"
							.", aa.dept, aa.canon, aa.agglo, aa.cp, aa.ville, aa.recherche, concat('http://www.ada.fr/location-voiture-',aa.canon,'.html') url"
		 				." from agence_pdv ap"
						." join agence a on a.id=ap.agence"
						." join agence_alias aa on aa.pdv=ap.id"
						." join zone z on z.id=a.zone"
						." left join zone_groupe zg on zg.id=a.groupe"
						." where (a.statut&1)=1 and ap.publie=1"
						." order by a.zone, aa.agence, aa.pdv, aa.id"
		),
		'agence_dispo' => array
		(
			'fname'		=> 'agence_dispo',
			'title'		=> "Liste des jauges ADA et Partenaires ouverts � la r�servation",
			'sql'		=> "SELECT a.zone, a.reseau, d.agence, d.type, c.mnem, d.nb, d.modification"
						 ." FROM `agence` a"
						 ." JOIN agence_dispo d ON d.agence=a.id"
						 ." JOIN categorie c ON c.id=d.categorie"
						 ." WHERE a.statut&3=3 AND c.publie=1"
						 ." ORDER BY a.zone, a.reseau, d.agence, d.type, c.position"
		),
		'agence_horaire' => array
		(
			'fname'		=> 'agence_horaire',
			'title'		=> 'Horaire des points de vente ADA ouverts � la r�servation',
			'sql'		=> "select h.agence, h.jour, h.ouverture, h.fermeture, h.pause_debut, h.pause_fin"
		 				." from agence_horaire h"
						." join agence_pdv ap on ap.id=h.agence"
						." join agence a on ap.agence=a.id"
						." where (a.statut&3)=3 AND a.reseau='ADA' and ap.publie=1"
						." order by h.agence, h.jour"
		),
		'agence_refnat' => array
		(
			'fname'		=> 'agence_refant',
			'title'		=> 'Zone, D�partement, Agglo et Agence avec un texte',
			'sql'		=> "select 'zone' type, id, 'oui' publie, canon, nom, concat('http://www.ada.fr/location-voiture-', canon, '.html') url, creation, modification from zone where refnat is not null"
						." union"
						." select 'dept' type, id, 'oui' publie, canon, nom, concat('http://www.ada.fr/location-voiture-', canon, '.html') url, creation, modification from dept where refnat is not null"
						." union"
						." select 'agglo' type, canon id, 'oui' publie, canon, nom, concat('http://www.ada.fr/location-voiture-', canon, '.html') url, creation, modification from agence_agglo where refnat is not null"
						." union"
						." select 'agence' type, aa.id id, case when ((aa.statut&1)=1 and ap.publie=1) then 'oui' else 'non' end publie, aa.canon, aa.nom, concat('http://www.ada.fr/location-voiture-', aa.canon, '.html') url, aa.creation, aa.modification from agence_alias aa join agence_pdv ap on ap.id=aa.pdv where aa.refnat is not null"
		),
		'options_liste' => array
		(
			'fname'		=> 'options_liste',
			'title'		=> 'Liste des options',
			'sql'		=> "select type, paiement,facturation, id,  nom from `option` order by type, paiement, facturation, id"
		),
		'options_vendues' => array
		(
			'fname'		=> 'options_vendues',
			'title'		=> "Options vendues par ann�e et mois",
			'sql'		=> "select year(r.paiement) annee, month(r.paiement) mois, ro.`option`, ro.paiement, count(*) nb, sum(ro.prix) montant, o.theme, coalesce(o.nom,ro.nom) nom"
				." from reservation r"
				." join res_option ro on ro.reservation=r.id"
				." left join `option` o on o.id=ro.`option`"
				." where year(r.paiement) >= ".(date('Y')-1)
				." group by year(r.paiement), month(r.paiement), ro.`option`, ro.nom, ro.paiement, o.theme"
		),
		'resa_jeunes' => array
		(
			'fname'		=> 'resa_jeunes',
			'title'		=> "R�sas jeune conducteur par ann�e et mois",
			'sql'		=> "select year(r.paiement) annee, month(r.paiement) mois, r.type, c.mnem, r.duree, r.age, rg.nom region, count(*) reservations"
						." from reservation r"
						." join categorie c on c.id=r.categorie"
						." join client u on u.id=r.client_id"
						." join dept d on d.id=left(u.cp,2)"
						." join region rg on rg.id=d.region"
						." where year(r.paiement)>=year(current_date)-1 and r.age < 25 and r.forfait_type!='LOCAPASS' and u.pays='fr'"
						." group by year(r.paiement), month(r.paiement), r.type, c.mnem, r.duree, r.age, rg.nom"
		),
		'stats_newsletter' => array
		(
			'fname'		=> 'stats_newsletter',
			'title' 	=> 'Stats Newsletter',
			'sql'		=> 'select year(creation) annee, origine, count(*) from prospect group by year(creation), origine'
		),
		'export_moyenne_duree' => array(
			'fname'		=> 'prospect_moyenne_duree',
			'title'		=> 'Export Moyenne Dur�e',
			'sql'		=> "select * from prospect_pro where origine='moyenne_duree' order by creation"
		),
		'stats_webpro' => array
		(
			'fname'		=> 'stats_webpro',
			'title'		=> 'Stats WebPro',
			'sql'		=> 
					"select p.id partenaire_id, p.nom partenaire_nom, date(p.creation) partenaire_creation, p.naf, p.siret, p.interlocuteur, p.email, p.tel, p.cp, p.ville,"
						." pc.id coupon, concat(pc.code,coalesce(concat(' (',pc.locapass,')'),'')) code, pc.echeance,"
						." (case when p.actif * pc.actif then 'O' else 'N' end) actif,"
						." min(date(r.paiement)) `premi�re r�sa`,"
						." max(date(r.paiement)) `derni�re r�sa`,"
						." sum(case when year(r.paiement)='2013' and r.statut='P' then 1 else 0 end) `2013 (P)`, "
						." sum(case when year(r.paiement)='2013' and r.statut='A' then 1 else 0 end) `2013 (A)`, "
						." sum(case when year(r.paiement)='2013' and r.forfait_type!='LOCAPASS' then r.total-ifnull(r.rembourse,0) else 0 end) `2013 (CA)`,"
						." sum(case when year(r.paiement)='2012' and r.statut='P' then 1 else 0 end) `2012 (P)`, "
						." sum(case when year(r.paiement)='2012' and r.statut='A' then 1 else 0 end) `2012 (A)`, "
						." sum(case when year(r.paiement)='2012' and r.forfait_type!='LOCAPASS' then r.total-ifnull(r.rembourse,0) else 0 end) `2012 (CA)`,"
						." sum(case when year(r.paiement)='2011' and r.statut='P' then 1 else 0 end) `2011 (P)`, "
						." sum(case when year(r.paiement)='2011' and r.statut='A' then 1 else 0 end) `2011 (A)`, "
						." sum(case when year(r.paiement)='2011' and r.forfait_type!='LOCAPASS' then r.total-ifnull(r.rembourse,0) else 0 end) `2011 (CA)`"
					." from partenaire p"
					." left join partenaire_coupon pc on pc.partenaire=p.id"
					." left join reservation r on (r.coupon=pc.id and r.paiement >= '2011-01-01')"
					." where p.origine='webpro'"
					." group by p.id, p.nom, p.actif, date(p.creation),  p.naf, p.siret, p.interlocuteur, p.email, p.tel, p.cp, p.ville, pc.actif, pc.echeance, pc.id, pc.code, pc.locapass "
					." order by p.nom, pc.code"
		),
	);
	// s'il y a une exportation demand�e on la renvoie
	if ($_POST['exportation'] && $a = $EXPORT_LIST[$_POST['exportation']])
	{
		$fname = $a['fname'].'_'.date('Y-m-d');
		$exporter = new SQLExporter(array('sql'=>$a['sql'], 'fname'=>$fname, 'sqlpreserve'=>true));
		$exporter->toExcel();
		die();
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script type="text/javascript" src="<?=Page::getProtocol()?>://www.google.com/jsapi"></script>
	<script type="text/javascript">
		google.load('jquery', '1.4');
	</script>
	<script type="text/javascript">
	// <![CDATA[
	// ]]>
	</script>
	<style type="text/css">
		select,input { width: inherit;}
		label { padding-left: 2px; }
		label.col1 { padding-left: 0; float: left; width: 65px;}
	</style>
</head>
<body>
<h1>Exportations</h1>
<form action="" method="post">
<p>S�lectionner les donn�es que vous souhaitez exporter :</p>
<?
	$a = array();
	foreach($EXPORT_LIST as $k => $v)
		$a[$k] = $v['title'];
	selectarray('exportation', $a, null, '-- choisissez dans la liste --', 'onchange="this.form.submit();"');
?>
</form>

</body>
</html>