<?
	require_once('../constant.php');
	require_once("secure.php"); 
	require_once("lib.php");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script type="text/javascript" src="<?=Page::getProtocol()?>://www.google.com/jsapi"></script>
	<script type="text/javascript">
		google.load('jquery', '1.4');
	</script>
	<script type="text/javascript">
	// <![CDATA[
	// ]]>
	</script>
	<style type="text/css">
		select,input { width: inherit;}
		label { padding-left: 2px; }
		label.col1 { padding-left: 0; float: left; width: 65px;}
	</style>
</head>
<body>
<?
	$campagne = (int) $_GET['campagne'];

	$sql = "select c.id, c.nom, c.debut, c.fin, c.nb";
	$sql.=", count(g.id)`cr��s`, count(distribution) `distribu�s`, count(paiement) `grill�s`, count(annulation) `annul�s`, sum(case when paiement is null then 0 else reduction end) `r�ductions`\n";
	$sql.=" from promotion_campagne c\n";
	$sql.=" left join promotion_code g on g.campagne=c.id\n";
	if ($campagne)
		$sql.=" where c.id=$campagne\n";
	else
		$sql.=" where (case when c.fin < '".date('Y-m-d')."' then 1 else 0 end) = %ld";
	$sql.=" group by c.id, c.nom, c.debut, c.fin, c.nb\n";
	$sql.=" order by c.nom";
	
	$link = 'stats_campagne.php?campagne=#id#';
	$href = array('id' => $link, 'nom'=> $link);
	$format = array
	(
		'total courtage' => 'money',
		'r�ductions' => 'money', 
		'total r�ductions' => 'money',
		'total pay�' => 'money',
		'moyenne r�duction' => 'money'// '%01.2f �'
	);
	
	if (!$campagne)
	{
		show_table("Campagnes en cours", sprintf($sql, 0), $href, $format);
		show_table("Campagnes archiv�s", sprintf($sql, 1), $href, $format);
	}
	else // si la campagne est resneign�e on affiche les d�tails
	{
		echo '<a href="stats_campagne.php">Toutes les camapgnes</a><br/>';
		$campagne = PromotionCampagne::factory($campagne, true);
		$title = 'Campagne #'.$campagne['id'].' : '.$campagne['nom'].'&nbsp;[<a href="promotion_campagne.php?id='.$campagne['id'].'">�diter</a>]';
		show_table($title, $sql, $href, $format);

		// puis les statistiques
		$sql_base =", count(g.id) `grill�s`, avg(g.reduction) `moyenne r�duction`, sum(g.reduction) `total r�ductions`, sum(r.total) `total pay�` from promotion_code g join reservation r on r.id=g.reservation where g.campagne=".$campagne['id']." and g.paiement is not null";
		
		show_table('Courtage par Type', "select ucase(r.type) type, count(f.id) `courtage`, sum(f.montant)  `total courtage`".str_replace(' where ', ' left join facturation_courtage f on f.reservation=r.id where ', $sql_base).' group by ucase(r.type) with rollup', null, $format);
		show_table('Par mois', "select year(g.paiement) `ann�e`, lpad(month(g.paiement),2,'0') mois $sql_base group by year(g.paiement), month(g.paiement) order by year(g.paiement) desc, month(g.paiement) desc", null, $format);
		show_table('Par semaine', "select left(yearweek(g.paiement, 3), 4) `ann�e`, right(yearweek(g.paiement, 3), 2) semaine $sql_base group by yearweek(g.paiement, 3) order by yearweek(g.paiement, 3) desc", null, $format);
		show_table('Par jour', "select date(g.paiement) jour $sql_base group by date(g.paiement) order by date(g.paiement) desc", null, $format);
	}
?>
</body>
</html>