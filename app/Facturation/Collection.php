<?

class Facturation_Collection extends Ada_Collection
{
	
	/** @var Reservation */
	protected $_reservation;
	
	/**
	* Cr�� une collection de facturaiton
	*
	* @return Facturation_Collection
	*/
	static public function factory(Reservation $reservation)
	{
		// on cr�e la collection qui sera retourn�e
		$x = new Facturation_Collection();
		$x->_reservation = $reservation;
		// SQL pour les applications
		if ($id = $reservation->getId())
		{
			$sql = "SELECT 'ada' facturation, id, reservation, type, mode, montant, creation FROM facturation_ada WHERE reservation=$id\n";
			$sql.= "UNION\n";
			$sql.= "SELECT 'materiel' facturation, id, reservation, type, mode, montant, creation FROM facturation_materiel WHERE reservation=$id\n";
			$sql.= "UNION\n";
			$sql.= "SELECT 'courtage' facturation, id, reservation, type, mode, montant, creation FROM facturation_courtage WHERE reservation=$id\n";
			$sql.= "UNION\n";
			$sql.= "SELECT 'pro' facturation, id, reservation, type, mode, montant, creation FROM facturation_pro WHERE reservation=$id\n";
			$sql.= "ORDER BY facturation, type DESC";
			$x->_loadFromSQL($sql);
		}
		return $x;
	}
	public function add(Ada_Object $item)
	{
		if ($item)
			$item->setReservation($this->_reservation);
		return parent::add($item);
	}
	/**
	* renvoie une facturation en fonction des param�tres
	* @param string $facturation : ada | courtage
	* @param string $type : F | A
	* @returns Facturation
	*/
	public function find($facturation, $type)
	{
		foreach ($this->_items as $f)
		{
			if ($f['facturation']==$facturation && $f['type']==$type)
				return $f;
		}
		return null;
	}
	/**
	* Renvoyer la liste des avoirs 
	* 
	*/
	public function getAvailableAvoirs()
	{
		$x = new Facturation_Collection();
		$x->_reservation = $this->_reservation;
		foreach($this->_items as /** @var Facturation */ $f)
		{
			/* les pro, les avoirs et les factures ayant d�j� un avoir ne peuvent plus �tre annul�es */
			if ($f['facturation']=='pro' || $f['type']=='A' || $this->find($f['facturation'], 'A')) continue;
			$avoir = $f->createAvoir();
			if ($f['facturation'] == 'materiel')
			{
				$resUnipro = ResUnipro::factory($this->_reservation);
				$enlevement = $resUnipro->getDateElevement($error);
				if ($enlevement) 
				{
					$this->find('materiel', 'F')->setData('erreur', 'mat�riel retir� le '.date('d/m/Y', strtotime($enlevement)));
					continue;
				}
				else if ($error)
					$avoir['erreur'] = $error;
			}
			$x->add($avoir);
		}
		return $x;
	}
}
?>
