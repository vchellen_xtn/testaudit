<?
	require_once("secure.php"); 
	require_once("lib.php");

	// identifiant
	$id = getarg("id");
	
	// suppression
	if ($id && getarg("delete")) 
	{
		$sql = "delete from page where id = '".$id."'";
		getsql($sql);
		unset($id);
	}
	
	// modification, cr�ation
	if (getarg("update") || getarg("create"))
	{
		if ($_POST['footer'])
			$_POST['footer'] = strip_tags($_POST['footer'], HTML_TAGS_ALLOWED);
		$a = $_POST;
		save_record('page', $a);
		$b = array('id_histo'=>'', 'login'=>$adminUser['login']);
		$histo = array_merge($a, $b);
		save_record('page_histo', $histo);
	}
	if ($id)
	{
		$rs = sqlexec("select * from page where id = '".$id."' ");
		if (!$rs || !($row = mysql_fetch_assoc($rs)))
			die("Aucun enregistrement correspondant.");
	}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script language="javascript" src="../scripts/check.js"></script>
	<script>
	<!-- //
	// doDelete
	function doDelete(frm)
	{
		return (confirm("Voulez-vous r�ellement supprimer cet �l�ment ?\nCette op�ration est irr�versible."));
	}
	
	// doSubmit
	function doSubmit(frm)
	{
		var err = "";
		if(!frm.id.value.length)
			err+= "Veuillez indiquer un nom de page\n";
		if(!frm.url.value.length)
			err+= "Veuillez indiquer l'URL\n";
		if(!frm.titre.value.length)
			err+= "Veuillez indiquer le titre\n";
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		return true;
	}
	// -->
	</script>
</head>
<body>
<div style="float: right; padding: 1em; border: 1px dotted grey;">
Les �l�ments "titre", "description" et "h1" peuvent contenir les �l�ments suivants :
<dl>
	<dt>#agence#</dt><dd>remplac� par le nom de l'agence</dd>
	<dt>#categorie#</dt><dd>remplac� par le nom de la cat�gorie ou voiture ou utilitaire</dd>
	<dt>#dept#</dt><dd>remplac� par le nom du d�partement</dd>
	<dt>#localisation#</dt><dd>remplac� par le premier �l�ment disponible dans la liste : agence, dept, region, zone</dd>
	<dt>#marque#</dt><dd>remplac� par la marque du v�hicule</dd>
	<dt>#modele#</dt><dd>remplac� par le mod�le du v�hicule</dd>
	<dt>#region#</dt><dd>remplac� par le nom de la r�gion</dd>
	<dt>#type#</dt><dd>remplac� par le type de v�hicule</dd>
	<dt>#zone#</dt><dd>remplac� par le nom de la zone</dd>
</dl>
Si dans le contexte une valeur n'est pas renseign�e, elle est supprim�e.
<br/>Le texte doit donc rester coh�rent avec ou sans la pr�sence de ces textes.
<br/>
<br/>Le champ "Footer" sera ins�r� en bas de la page.
<br/>Chaque ligne correspond � un point de la liste.
<br/>Des �l�ments HTML peuvent �tre ins�r�s dans le texte.
<br/>Les liens doivent �tre en relatifs par rapport � la racine du site sans le "/".
<br/>Les �l�ments autoris�s sont <?=htmlspecialchars(HTML_TAGS_ALLOWED);?>
</div>
<form action="" method="post">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre">
	<img src="img/puce.gif" align="absmiddle" hspace="1">
<? 
	echo "Page";
?>
	</th>
</tr>
<tr>
	<td>
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="15%">
		<col width="15%">
		<col width="15%">
		<col width="55%">
		<tr>
			<td><label for="page">Page</label></td>
			<td colspan="3">
				<?=$id?>
				<input type="<?=($id ? 'hidden' : 'text')?>" maxlength="128" name="id" value="<?=$id?>"></td>
		</tr>
		<tr>
			<td><label for="url">URL</label></td>
			<td colspan="3"><input type="text" style="width:100%" name="url" id="url" value="<?=$row["url"]?>" maxlength="128" size="64"/></td>
		</tr>
		<tr>
			<td><label for="titre">Titre</label></td>
			<td colspan="3"><input style="width:100%;"type="text" name="titre" id="titre" value="<?=$row["titre"]?>" maxlength="196" size="64"/> </td>
		</tr>
		<tr>
			<td><label for="h1">H1</label></td>
			<td colspan="3"><input style="width:100%;" type="text" name="h1" id="h1" value="<?=$row["h1"]?>" maxlength="255" size="64"/></td>
		</tr>
		<tr>
			<td><label for="description">Description</label></td>
			<td colspan="3"><textarea id="description" name="description" rows="3" style="width: 100%;"><?=htmlspecialchars($row['description']);?></textarea></td>
		</tr>
		<tr><th colspan="4"><label for="footer">Footer</label></th></tr>
		<tr><td colspan="4"><textarea id="footer" name="footer" rows="6" style="width: 100%;"><?=htmlspecialchars($row['footer']);?></textarea></td></tr>
		</table>
	</td>
</tr>
<tr>
	<td align="right">
		<input type="submit" name="<?=$id ? "update" : "create"?>" value="enregistrer" onclick="return doSubmit(this.form);">
<?
	if ($id) 
	{
?>
		&nbsp;
		<input type="submit" onclick="return doDelete(this.form);" name="delete" value="supprimer">
<?
	}
?>
	</td>
</tr>
</table>
</form>
<? if ($row) : ?>
	<? if ($rs = sqlexec(sprintf("select * from page_histo where id='%s' order by id_histo desc", $row['id']))) : ?>
		<h2>Historique</h2>
		<table border="1" cellpadding="2" cellspacing="0" class="stats resultat">
		<? while ($histo = mysql_fetch_assoc($rs)) : ?>
		<tr><th colspan="2"><?=euro_iso($histo['creation']).' - '.$histo['login']?></th></tr>
		<? foreach(array('url','titre','h1','description','footer') as $k) : ?>
		<tr><th><?=ucwords($k)?></th><td><pre><?=htmlspecialchars($histo[$k])?></pre></td></tr>
		<? endforeach; ?>
		<? endwhile; ?>
		</table>
	<? endif; ?>
<? endif; ?>
</body>
</html>
