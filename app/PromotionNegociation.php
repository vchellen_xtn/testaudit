<?php
/**
* G�re la n�gociation avec le client
* cf http://my.rnd.fr/projects/ada/wiki/Negociations
*/
class PromotionNegociation extends Ada_Object {
	
	const STEP_INIT = 1;
	const STEP_MAX = 3;
	
	
	/**
	* Renvoie une promotion n�gociation
	* 
	* @param int $id
	* @return PromotionNegociation
	*/
	static public function factory($id) {
		$x = new PromotionNegociation();
		$x->load((int)$id);
		return $x;
	}
	
	/**
	* V�rifie si l'�cart est suffisant pour proposer la n�gociation
	* 
	* @param int $prix_actuel
	* @param int $prix_negociable
	* @returns bool
	*/
	static public function isEcartSuffisant($prix_actuel, $prix_negociable) {
		// on veut au moins un �cart de 5% pour n�gocier et 10� minimum en dessous de 200�
		if ($prix_actuel >= 200) {
			// un �cart de 5%
			return (($prix_actuel * 0.95) >= $prix_negociable);
		}
		// sinon 5 � minimum
		return (($prix_actuel - 5) >= $prix_negociable);
	}
	/**
	* Effectue la n�gociation � partir des donn�es re�ues en POST
	* 
	* @param array $args contenent "forfaite" et "proposition"
	* @returns Ada_Object
	*/
	static public function doNegociation($args) {
		$data = array('status' => 'KO');
		// on r�cup�re les param�tres
		$forfait = Forfait::doUnserialize($args['forfait']);
		$proposition = (int) $args['proposition'];
		// quelques v�rifications
		if (!$forfait || !$proposition) {
			$data['msg'] = "Une erreur s'est produite lors de l'envoi de votre proposition !";
		} else if (!$forfait->hasNego()) {
			$data['msg'] = "La n�gociation n'est actuellement pas possible !";
		} else if ($forfait->getPrix() <= $proposition) {
			$data['msg'] = "Votre proposition est sup�rieure au prix initial de la location !";
		} else {
			// OK on commence par v�rifier les �tapes
			$step = $forfait->getData('n_step');
			if (!$step || $step < self::STEP_INIT || $step >= self::STEP_MAX) {
				$data['msg'] = "La n�gociation ne peut pas �tre initialis�e !";
			} else if ((int)$step != (int)$args['step']) {
				$data['msg'] = "Une erreur s'est produite pandant la n�gociation !";
			} else {
				// on autmenter l'�tape
				if ($forfait->tryPrixNegociation($proposition)) {
					$data['proposition'] = $proposition;
					$data['status'] = 'OK';
				} else if ($forfait['n_step'] == self::STEP_MAX) {
					$data['proposition'] = $forfait->stopNegociation();
				}
				$data['step'] = $forfait['n_step'];
				// on renvoie le forfait
				$data['forfait'] = $forfait->doSerialize();
			}
		}
		return new Ada_Object($data);
	}
	
}
?>
