<?
	/** @var Forfait */ $forfait = $this;
	$categorie = $forfait->getCategorie();
?>

<div class="row">
	<div class="col-xs-12">
		<h2 class="title-like no-margin-bottom"><?=$categorie['nom']?></h2>
		<p>
			<?=$categorie['description']?>
		</p>
	</div>
	<div class="col-xs-6">
		<img src="../<?=$categorie->getImg('small', 'categories3');?>" id="forfait_categorie_img" alt="" class="visu" />
	</div>
	<div class="col-xs-6 no-padding">
		<table class="fullwidth">
			<? if ($forfait->isLocapass()) : ?>
			<tr>
				<td colspan="2">
					<img src="../css/img/locapass.png" alt="Locapass" />
				</td>
			</tr>
			<? else : ?>
			<tr class="total-price">
				<th>Prix total :</th>
				<td class="text-right">
					<span id="forfait_t_prix" class="digit-<?=strlen($forfait->getPrix());?>"><?=$forfait->getPrix();?>�</span>
				</td>
			</tr>
				<tr>
					<td colspan="2" class="price-promo">
					<? if ($forfait->hasPromo()) : ?>
						au lieu de <span><?=$forfait->getPrixBarre()?>�</span><br />
					<? endif; ?>
						<a href="#" onclick="if (appADA && appADA.showPopIn) { appADA.showPopIn('forfait_details', {'agence': '<?=$forfait->getAgence()->getId()?>', 'categorie': '<?=$categorie['id']?>', 'km_sup': '<?=$forfait->getPrixKmSupp()?>'})};  return false;">&rsaquo; D�tails et devis</a>
					</td>
				</tr>
			<? endif; ?>
			<? if ($surcharge = $forfait->getSurcharge()) : ?>
			<tr class="get-surcharge">
				<th><span class="forfait_surcharge_label"><?=$forfait->getAgence()->getSurchargeLabel()?></span></th>
				<td><span class="forfait_surcharge_prix"><?=$surcharge;?> &euro;</span></td>
			</tr>
			<? endif; ?>
		</table>
		<? if ($forfait->hasNego()) : ?>
		<div class="forfait_nego">
			<a id="btn_negociation" href="#" onclick="if (appADA && appADA.showPopIn) { appADA.showPopIn('forfait_negociation', {'forfait': '<?=$forfait->doSerialize();?>'}, {dialogClass: 'popin popin-negociation', position: {my: 'right bottom', at:'left center', of: '#btn_negociation' }}); }  return false;">N�gociez votre prix ici !</a>
		</div>
		<? endif; ?>
	</div>
</div>