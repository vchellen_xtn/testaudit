<?
	session_start();
	require_once("secure.php"); 
	require_once("lib.php");
	
	if (!$adminUser->hasRights(Acces::SB_GESTION) && !$adminUser->hasRights(Acces::SB_OFFRES))
		die("Vous n'avez pas le droit de modifier les tarifs");
	$zone = $adminUser->getCurrentZone();
	if (!$zone || $zone->isEmpty())
		die("Vous devez avoir choisi la zone !");

	// g�rer l'export vers Excel
	$format = 'html';
	if ($_POST['export'])
	{
		// provoquer le dialogue de t�l�chargement
		header("content-Type: application/ms-excel");
		header("Content-Disposition: attachment; filename=ADA_".SITE_MODE."_Tarifs_".$zone['id'].'_'.$_POST['type'].'_'.date('Y-m-d').".xls");
		$format = 'xls';
	}
// pour �crire les checkbox
function write_checkboxes($label, $name, $sql, $hidden = false)
{
	$rs = sqlexec($sql);
	$cnt = mysql_num_rows($rs);
	if (!$cnt) return;
	echo '<p><label class="col1">'.$label.'</label>';
	if ($hidden)
		write_checkbox($name, '0', 'd�faut');
	while ($row = mysql_fetch_row($rs))
		write_checkbox($name, $row[0], (isset($row[1]) ? $row[1] : $row[0]));
	if ($cnt > 2)
	{
		echo '<br/><label class="col1">&nbsp;</label>';
		echo '<a href="#" onclick="return doCheckbox(this, true);">cocher</a>&nbsp;&nbsp;';
		echo '<a href="#" onclick="return doCheckbox(this, false);">d�cocher</a>';
	}
	echo '</p>';	
}
function write_checkbox($name, $id, $lbl)
{
	echo '<input type="checkbox" name="'.$name.'[]" id="'.$name.'_'.$id.'" value="'.$id.'"';
	if (!isset($_POST[$name]) || in_array($id, $_POST[$name]))
		echo ' checked';
	echo '><label for="'.$name.'_'.$id.'">'.$lbl.'</label>';
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Tarification forfaits</title>
	<meta http-equiv="Content-Type" content="text/html;charset=windows-1252">
<? if ($format=='html') { ?>
	<link rel="stylesheet" href="style.css">
	<script type="text/javascript" src="<?=Page::getProtocol()?>://www.google.com/jsapi"></script>
	<script type="text/javascript">
		google.load('jquery', '1.4');
		
		function doCheckbox(src, flag)
		{
			$(':checkbox', src.parentNode).each(function(){
				this.checked = flag;
			});
			return false;
		}
		function checkInt(c)
		{
			if (c.value.length && !c.value.match(/^\d+(\.\d{2})?$/))
			{
				$(c).addClass('error');
				setTimeout(function(){c.select(); c.focus();}, 5);
				return false;
			}
			$(c).removeClass('error');
			return true;
		}
		function doSubmit(frm)
		{
			// v�rifier les erreurs
			var errors = $('.error');
			if (errors.length)
			{
				alert("Veuillez corriger les valeurs erron�es");
				errors.select().focus();
				return false;
			}
			return true;
		}
	</script>
	<style type="text/css">
		select, input { width: auto;}
		label { padding-left: 2px; }
		label.col1 { padding-left: 0; float: left; width: 65px;}
		input.error {color: red;}
		input.int {text-align: right; border: solid 1px	;}
	</style>
<? } // !$foormat == 'html' ?>
</head>
<body>
<form action="" method="post" onsubmit="return doSubmit(this);">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<? if ($format=='html') {?>
<tr>
	<th class="bg_titre">
		<img src="img/puce.gif" align="absmiddle" hspace="1">
		<strong>Tarification Forfait <?=$zone['nom']?></strong>
	</th>
</tr>
<? } // ! $format='html'?>
<? if ($_POST['save'] && is_array($_POST['forfait_tarif'])) { ?>
<tr>
	<td>
<?
	$cnt = 0; $match = '/^\d+(\.\d{2})?$/';
	foreach($_POST['forfait_tarif'] as $t)
	{
		if ($t['id'] && ($t['prix']!=$t['o_prix'] || $t['prix_jour']!=$t['o_prix_jour']))
		{
			foreach($t as $k => $v)
			{
				if ($v && !preg_match($match, $v))
					echo "<strong>Erreur ".$t['id']." - $k : $v</strong><br/>\n";
			}
			// enregistrer
			if (preg_match($match, $t['prix']) && preg_match($match, $t['prix_jour']))
			{
				save_record('forfait2_tarif', $t);
				$cnt++;
			}
		}
	}
	echo "<strong>$cnt tarifs modifi�s</strong>";
?>
	</td>
</tr>
<? 
} // fin enregistrement
if ($format == 'html') 
{
?>
<tr>
	<td>
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<tr><td>
		<fieldset><legend>Rechercher</legend>
<?
				// R�seaux, Groupes et Saisons
				write_checkboxes('Groupes', 'groupes', "select id, nom from zone_groupe where zone='".$zone['id']."' order by nom", true);
				write_checkboxes('Saisons', 'saisons', "select id, libelle from zone_saison where zone='".$zone['id']."' order by debut_mois, debut_jour", true);
?>

			<p>
				<input type="hidden" name="o_type" value="<?=$_POST['type']?>"/>
				<label class="col1" for="type">Type</label>
				<? selectsql('type', "select distinct UCASE(type), UCASE(type) from categorie where publie=1 order by type", $_POST['type'], '---', 'onchange="this.form.submit();"');?>
			</p>
<?
			if ($type = $_POST['type'])
			{
				if (!preg_match('/^[a-z]{2}$/i', $type))
					die("Type inconnu");
				if ($type != $_POST['o_type'])
				{
					unset($_POST['categories']);
					unset($_POST['forfaits']);
					$refresh = true;
				}
				// cat�goires et forfaits
				write_checkboxes('Cat�gories', 'categories', "select id, mnem from categorie where zone='fr' and type='$type' and publie=1 order by position");
				write_checkboxes('Pr�fixes', 'forfaits', "select f.prefix, concat(f.prefix,' (',count(t.id),')') nom from forfait2 f left join forfait2_tarif t on t.forfait=f.id and t.zone='".$zone['id']."' where f.type='".$type."' group by f.prefix order by f.prefix");
			}
?>
		</fieldset>
		</td>
		</tr>
		<tr>
			<td align="right">
				<input type="submit" name="search" value="rechercher">
			</td>
		</tr>
		</table>
	</td>
</tr>
<? } // !$format == 'html' ?>
<tr>
	<td>
<?
if ($_POST['type'] && $_POST['categories'] && $_POST['forfaits'])
{
	// afficher la liste des tarifs
	$sql = "select t.id, t.forfait, c.mnem, coalesce(g.nom,'d�faut') groupe, coalesce(s.libelle, 'd�faut') saison, t.prix, t.prix_jour, f.nom\n";
	$sql.=" from forfait2_tarif t\n";
	$sql.=" join forfait2 f on f.id=t.forfait\n";
	$sql.=" join categorie c ON c.id=t.categorie\n";
	$sql.=" left join zone_groupe g ON g.id=t.groupe\n";
	$sql.=" left join zone_saison s on s.id=t.saison\n";
	$sql.=" where t.zone='".$zone['id']."' and f.prefix in ('".join("','", $_POST['forfaits'])."')\n";
	$sql.=" and t.categorie in (".join(',', $_POST['categories']).")\n";
	if ($_POST['groupes'])
		$sql.=" and IFNULL(t.groupe,0) in (".join(',', $_POST['groupes']).")";
	if ($_POST['saisons'])
		$sql.=" and IFNULL(t.saison,0) in (".join(',', $_POST['saisons']).")";
	$sql.=" order by f.type, f.jours_min, f.jours_max, f.km, f.id, c.position, g.id, s.debut_mois, s.debut_jour";
//echo "<pre>$sql</pre>";
	// afficher les tarifs
	$rs = sqlexec($sql);
	echo '<table border="1" cellpadding="2" cellspacing="0">'."\n";
	echo '<caption>'.mysql_num_rows($rs).' forfaits � �diter</caption>'."\n";
	echo '<tr><th>Cat�gorie</th><th>Groupe</th><th>Saison</th><th>Prix</th><th>'.(strtolower($_POST['type'])=='ml'? 'Heure' : 'Jour').' Sup</th></tr>'."\n";
	while ($row = mysql_fetch_assoc($rs))
	{
		if ($forfait != $row['forfait'])
		{
			$forfait = $row['forfait'];
			echo '<tr>';
			echo '<th colspan="4">';
			if ($format == 'html')
				echo '<a href="forfait.php?id='.$forfait.'" target="_blank">';
			echo $forfait;
			if ($format == 'html')
				echo '</a>';
			echo ' : '.$row['nom'];
			echo '</th>';
			echo '<th align="right">';
			if ($format=='html' && $adminUser->hasRights(Acces::SB_GESTION))
				echo '<a target="_blank" href="forfait_tarif.php?forfait='.$forfait.'">[ajouter]</a>';
			echo '</th>';
			echo "</tr>\n";
		}
		echo '<tr>';
		echo '<td>'.$row['mnem'];
		if ($format=='html' && $adminUser->hasRights(Acces::SB_GESTION))
			echo ' <a target="_blank" href="forfait_tarif.php?id='.$row['id'].'">[�diter]</a>';
		echo '</td>';
		foreach(explode(',', 'groupe,saison') as $k)
			echo '<td>'.$row[$k].'</td>';
		if ($format == 'xls')
		{
			foreach(array('prix','prix_jour') as $k)
				echo '<td align="right">'.$row[$k].' �</td>';
		}
		else
		{
			echo '<td align="right">';
			foreach(array('id'=>'id','prix'=>'o_prix','prix_jour'=>'o_prix_jour') as $k => $v)
				echo '<input type="hidden" name="forfait_tarif['.$row['id'].']['.$v.']" value="'.$row[$k].'"/>';
			echo '<input type="text" class="int" id="ft_prix_'.$row['id'].'" name="forfait_tarif['.$row['id'].'][prix]" value="'.$row['prix'].'" size="7" maxlength="7" onchange="return checkInt(this);"/>';
			echo '</td>';
			echo '<td align="right">';
			echo '<input type="text" class="int" id="ft_prixjour_'.$row['id'].'" name="forfait_tarif['.$row['id'].'][prix_jour]" value="'.$row['prix_jour'].'" size="7" maxlength="7" onchange="return checkInt(this);"/>';
			echo '</td>';
		}
		echo '</tr>';
	}
	if ($format == 'html')
	{
		echo '<tr><td align="right" colspan="5">';
		echo '<input type="submit" name="export" value="exporter">';
		echo '<input type="submit" name="save" value="enregistrer">';
		echo "</td></tr>\n";
	}
	echo '</table>';
}
?>
	</td>
</tr>
</table>
</form>
</body>
</html>
