<?
/**
* Classe pour g�rer les ernegistrements comptables
*/

class ComptaClientys extends Ada_Object
{
	// frais de gestion
	const CLIENTYS_FRAIS = 0.040; // 0.025; modifi� le 09/03/2009 sur demande de gfranckhauser@ada.fr
	// postes comptables
	const CLIENTYS_CPTE_AC = '467000'; // Comptes pour les achats
	const CLIENTYS_CPTE_FF = '401100'; // Fournisseur (agence)
	const CLIENTYS_CPTE_FG = '467500'; // Frais de gestion
	const CLIENTYS_CPTE_AN = '708843'; // Produits annulation
	const CLIENTYS_CPTE_PR = '708842'; // Produit Compte PRO

	/** renvoie une ligne format�e pour le fichier clientys 
	* @returns string
	*/
	public function getLine()
	{
		$x = $this->_data;
		$a = array(
			'JOURNAL'		=> str_pad($x['JOURNAL'], 3, ' ', STR_PAD_RIGHT)
			,'DATECOMPTABLE'=> str_replace('-','', euro_iso($x['DATECOMPTABLE']))
			,'TYPEPIECE'	=> str_pad($x['TYPE_PIECE'], 2,' ', STR_PAD_RIGHT)
			,'GENERAL'		=> str_pad($x['GENERAL'], 17,' ', STR_PAD_RIGHT)
			,'TYPE_CPTE'	=> str_pad($x['TYPE_CPTE'], 1,' ', STR_PAD_RIGHT)
			,'AUXILIAIRE'	=> str_pad($x['AUXILIAIRE'], 17,' ', STR_PAD_RIGHT)
			,'REFINTERNE'	=> str_pad($x['REFINTERNE'], 35,' ', STR_PAD_RIGHT)
			,'LIBELLE'		=> str_pad(substr($x['LIBELLE'],0,35), 35,' ', STR_PAD_RIGHT)
			,'MODEPAIE'		=> str_pad($x['MODEPAIE'], 3,' ', STR_PAD_RIGHT)
			,'ECHEANCE'		=> str_replace('-','', euro_iso($x['ECHEANCE']))
			,'SENS'			=> str_pad($x['SENS'], 1,' ', STR_PAD_RIGHT)
			,'MONTANT1'		=> str_pad(sprintf('%.2f', $x['MONTANT1']), 8,' ', STR_PAD_LEFT).str_pad(null,12)
			,'TYPE_ECRITURE'=> str_pad($x['TYPE_ECRITURE'], 1,' ', STR_PAD_RIGHT)
			,'NUMEROPIECE'	=> str_pad($x['NUMEROPIECE'], 8,'0', STR_PAD_LEFT)
			,'DEVISE'		=> str_pad('EUR', 3,' ', STR_PAD_RIGHT)
			,'TAUXDEV'		=> str_pad(null, 10,' ')
			,'CODEMONTANT'	=> str_pad('E--', 3,' ', STR_PAD_RIGHT)
			,'SPACE_1_1'	=> str_pad(null, 47, ' ')
			,'REFEXTERNE'	=> str_pad(null, 35, ' ')
			,'SPACE_1_2'	=> str_pad(null, 47,' ')
			,'QTE1'			=> str_pad(null, 20,' ', STR_PAD_RIGHT)
			,'SPACE_1_3'	=> str_pad(null, 208,' ')
			,'LIBRETEXTE0'	=> str_pad('Central Resa', 30,' ', STR_PAD_RIGHT)
			,'LIBRETEXTE1'	=> str_pad($x['type'], 30,' ', STR_PAD_RIGHT)
			,'LIBRETEXTE2'	=> str_pad('Resa Hors Agence', 30,' ', STR_PAD_RIGHT)
			,'SPACE_2'		=> str_pad(null, 402,' ')
		);
		return implode('', $a);
	}
	
	/**
	* Enregistre le mouvement comptable associ�e � une r�servation
	* 
	* @param Reservation $reservation
	* @param float $montant
	*/
	static public function doReservation(Reservation $reservation, $montant)
	{
		// v�rifier les arguments
		if (!$reservation || $reservation->isLocapass() || !$reservation['paiement'])
			return false;

		// les �l�ments communs
		list($comptable, $echeance, $aux, $libelle) = self::_getCommon($reservation, 'paiement');
		$a = array();
		
		// si rfvu ou rfvp, on enl�ve ce montant avant de proc�der � la r�partition pour l'agence
		$rf = 'rf'.strtolower($reservation['type']);
		if ($reservation->getResOptions()->hasOption($rf))
			$montant = round($montant - $reservation->getResOptions()->getItem($rf)->getData('prix'), 2);
		
		// le montant peut valoir 0.00 en cas de coupon de r�duction
		if ($montant > 0)
		{
			// obtenir un num�ro
			$numero = self::_getNumeroPiece();
			$a[] = self::_fillRecord($reservation, 'ACH', $comptable, 'FF', self::CLIENTYS_CPTE_AC, null, $libelle, $echeance, 'D', $montant, $numero);
			$a[] = self::_fillRecord($reservation, 'ACH', $comptable, 'FF', self::CLIENTYS_CPTE_FF, $aux, $libelle, $echeance, 'C', $montant, $numero);
			
			// puis les frais de gestion
			$numero = self::_getNumeroPiece();
			$montant = round($montant*self::CLIENTYS_FRAIS,2);
			$libelle_frais = 'FRAIS DE GESTION '.$libelle;
			$a[] = self::_fillRecord($reservation, 'OD', $comptable, 'OD', self::CLIENTYS_CPTE_FG, null, $libelle_frais, $echeance, 'C', $montant, $numero);
			$a[] = self::_fillRecord($reservation, 'OD', $comptable, 'OD', self::CLIENTYS_CPTE_FF, $aux, $libelle_frais, $echeance, 'D', $montant, $numero);
		}
		// versement au franchis� pour les codes grillables
		if ($code = $reservation->getPromotionCode())
		{
			if ($montant = $code['franchise_montant'])
			{
				$libelle_coupon = sprintf('COUPON %s %s', addslashes(mb_strtoupper($code['campagne_cible'])), $libelle);
				$numero = self::_getNumeroPiece();
				$a[] = self::_fillRecord($reservation, 'COU', $comptable, 'FF', self::CLIENTYS_CPTE_AC, null, $libelle_coupon, $echeance, 'D', $montant, $numero);
				$a[] = self::_fillRecord($reservation, 'COU', $comptable, 'FF', self::CLIENTYS_CPTE_FF, $aux, $libelle_coupon, $echeance, 'C', $montant, $numero);
			}
		}
		return self::_saveArray($a);
	}
	
	/**
	* Enregistre le mouvement comptable associ�e � une annulation
	* 
	* @param Reservation $reservation
	* @param float $montant
	*/
	static public function doCancel(Reservation $reservation, $montant)
	{
		// v�rifier les arguments
		if (!$reservation || $reservation->isLocapass() || !$reservation['remboursement'])
			return false;

		// les �l�ments communs
		list($comptable, $echeance, $aux, $libelle) = self::_getCommon($reservation, 'remboursement');
		$a = array();
		
		// s'il s'agit d'une annulation et que rfvu ou rfvp, on enl�ve ce montant avant de proc�der � la r�partition pour l'agence
		if ($reservation['statut']=='A')
		{
			$rf = 'rf'.strtolower($reservation['type']);
			if ($reservation->getResOptions()->hasOption($rf))
				$montant = round($montant - $reservation->getResOptions()->getItem($rf)->getData('prix'), 2);
		}

		// on compte les frais de dossier � part et on modifie le montant pour les autres �critures
		if ($frais_dossier = $reservation['fraisdossier'])
		{
			// on augmente le montant � r�-�crire pour ADA / agence
			if ($frais_siege = $reservation->getCancellationFeesSiege())
				$frais_siege = min($frais_siege, $frais_dossier);
			$montant = $montant + $frais_siege;
		}
		
		// le montant vaut parfois 0.00 pour des annulations vente priv�e
		if ($montant > 0)
		{
			if ($frais_siege > 0)
			{
				// comptabilise � part les frais de dossier
				$numero = self::_getNumeroPiece();
				$a[] = self::_fillRecord($reservation, 'ANN', $comptable, 'OD', self::CLIENTYS_CPTE_AC, null, 'FRAIS ANNUL '.$libelle, $echeance, 'D', $frais_siege, $numero);
				$a[] = self::_fillRecord($reservation, 'ANN', $comptable, 'OD', self::CLIENTYS_CPTE_AN, null, 'FRAIS ANNUL '.$libelle, $echeance, 'C', $frais_siege, $numero);
			}
			// obtenir un num�ro
			$numero = self::_getNumeroPiece();
			$a[] = self::_fillRecord($reservation, 'ACH', $comptable, 'AF', self::CLIENTYS_CPTE_AC, null, $libelle, $echeance, 'C', $montant, $numero);
			$a[] = self::_fillRecord($reservation, 'ACH', $comptable, 'AF', self::CLIENTYS_CPTE_FF, $aux, $libelle, $echeance, 'D', $montant, $numero);
			
			// puis les frais de gestion
			$numero = self::_getNumeroPiece();
			$montant = round($montant*self::CLIENTYS_FRAIS,2);
			$libelle_frais = 'FRAIS DE GESTION '.$libelle;
			$a[] = self::_fillRecord($reservation, 'OD', $comptable, 'OD', self::CLIENTYS_CPTE_FG, null, $libelle_frais, $echeance, 'D', $montant, $numero);
			$a[] = self::_fillRecord($reservation, 'OD', $comptable, 'OD', self::CLIENTYS_CPTE_FF, $aux, $libelle_frais, $echeance, 'C', $montant, $numero);
		}
		
		// reprise au franchis� du versement pour les codes grillables
		// mais uniquement pour les vraies annulation (statut A) et pas pour les rembourseemnts partiels (statut P)
		if ($reservation['statut']=='A')
		{
			if ($code = $reservation->getPromotionCode())
			{
				if ($montant = $code['franchise_montant'])
				{
					$libelle_coupon = sprintf('COUPON %s %s', addslashes(mb_strtoupper($code['campagne_cible'])), $libelle);
					$numero = self::_getNumeroPiece();
					$a[] = self::_fillRecord($reservation, 'COU', $comptable, 'FF', self::CLIENTYS_CPTE_AC, null, $libelle_coupon, $echeance, 'C', $montant, $numero);
					$a[] = self::_fillRecord($reservation, 'COU', $comptable, 'FF', self::CLIENTYS_CPTE_FF, $aux, $libelle_coupon, $echeance, 'D', $montant, $numero);
				}
			}
		}
		return self::_saveArray($a);
	}
	
	static public function doInscriptionPro(Reservation $reservation, Partenaire $partenaire, $montant)
	{
		// v�rifier les arguments
		if (!$reservation || !$montant || $reservation->isLocapass() || !$reservation['paiement'])
			return false;

		// les �l�ments communs
		list($comptable, $echeance, $aux, $libelle) = self::_getCommon($reservation, 'paiement');
		
		// pour le libell� on prend celui du partenaire
		$libelle = 'FRAIS INSCRIP '.$partenaire['nom'];
		
		// obtenir un num�ro
		$numero = self::_getNumeroPiece();
		$a[] = self::_fillRecord($reservation, 'PRO', $comptable, 'OD', self::CLIENTYS_CPTE_AC, null, $libelle, $echeance, 'D', $montant, $numero);
		$a[] = self::_fillRecord($reservation, 'PRO', $comptable, 'OD', self::CLIENTYS_CPTE_PR, null, $libelle, $echeance, 'C', $montant, $numero);
		
		return self::_saveArray($a);
	}
	
	/**
	* Renvoie des informations communes
	* 
	* @param Reservation $reservation
	* @param array $date
	*/
	static protected function _getCommon(Reservation $reservation, $date)
	{
		$agence = $reservation->getAgence();
		$client = $reservation->getClient();
		$t = $reservation->getDate($date,'U');
		
		// list($comptable, $echeance, $aux, $libelle)
		$a = array
		(
			 date('Y-m-d', $t)
			,date('Y-m-d' , strtotime('+2 days', $t))
			,$agence['code_societe'].$reservation['agence']
			,addslashes(mb_strtoupper($client['nom']))
		);
		return $a;
	}
	
	/**
	* Enregistre une ligne comptable
	* 
	* @param Reservation $reservation	identifiant de la r�servation associ�e
	* @param string $journal	ACH, OD ou FRA
	* @param string $comptable	date de pi�ce
	* @param string $type_piece	FF, OD ou AF
	* @param string $poste		401100, 467500, 467000
	* @param string $auxiliaire	null ou code agence
	* @param string $libelle	nom du client ou FRAIS DE GESTION + nom du client
	* @param string $echeance	date �ch�ance, date comptable si inconnu
	* @param string $sens		(D)�bit ou (C)r�dit
	* @param string $montant	en Euros
	* @param int $numero		num�ro dans le fichier
	*/
	static protected function _fillRecord(Reservation $reservation, $journal, $comptable, $type_piece, $poste, $auxiliaire, $libelle, $echeance, $sens, $montant, $numero)
	{
		$a = array(
			 'reservation'	=> $reservation->getId()
			,'type'			=> strtoupper($reservation['type'])
			,'creation'		=> date('Y-m-d H:i:s')
			,'JOURNAL'		=> $journal
			,'DATECOMPTABLE'=> $comptable
			,'TYPE_PIECE'	=> $type_piece
			,'GENERAL'		=> $poste
			,'TYPE_CPTE'	=> ($auxiliaire ? 'X' : '')
			,'AUXILIAIRE'	=> $auxiliaire
			,'REFINTERNE'	=> $reservation->getResIDClient()
			,'LIBELLE'		=> $libelle
			,'ECHEANCE'		=> $echeance
			,'SENS'			=> $sens
			,'MONTANT1'		=> $montant
			,'NUMEROPIECE'	=> $numero
		);
		return $a;
	}
	
	static protected function _saveArray($a)
	{
		if (!$a || !is_array($a) || !count($a))
			return false;
		foreach ($a as $x)
		{
			if (!$fields) $fields = array_keys($x);
			$values[] = "(NULLIF('".join("',''),NULLIF('", $x)."',''))";
		}
		$sql = 'INSERT INTO compta_clientys('.join(',', $fields).') VALUES '.join(',', $values);
		getsql($sql);
		return mysql_affected_rows();
	}
	
	/** renvoie un num�ro de pice 
	* @returns int
	*/
	static protected function _getNumeroPiece()
	{
		getsql("INSERT INTO compta_piece(id) VALUES (NULL)");
		return mysql_insert_id();
	}
}
?>
