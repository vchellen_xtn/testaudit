<?

class ResAnnulationMotif_Collection extends Ada_Collection
{
	/**
	* Cr�� une collection de res_annulation_motif
	*
	* @return ResAnnulationMotif_Collection
	*/
	public static function factory($cols = null)
	{
		if (!$cols) $cols = 'id, libelle, explications';
		// on cr�e la collection qui sera retourn�e
		$x = new ResAnnulationMotif_Collection();
		// SQL pour les applications
		$sql = "SELECT $cols FROM res_annulation_motif WHERE actif=1 ORDER BY position";
		$x->_loadFromSQL($sql);
		return $x;
	}
}
?>