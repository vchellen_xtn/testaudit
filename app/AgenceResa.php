<?php

class AgenceResa extends Ada_Object
{
	const SUBJECT_RESAS_DEVIS = 'Votre demande de devis';

	static public $OPERATION_LABEL = array(
		'DEVIS'	=> "Envoi d'un devis par mail",
		'RESA'	=> "R�servation avec paiement en agence",
		'PREPA' => "R�servation et paiement imm�diat",
	);

	/** @var AccesAgence */
	protected $_resasUser = null;
	/** @var AgenceClient */
	protected $_client = null;
	/** @var Reservation */
	protected $_reservation = null;
	/** @var ResOption_Collection */
	protected $_options = null;
	/** @var Offre_Collection */
	protected $_offres = null;
	/** @var array */
	protected $_grfInfo = null;


	/**
	* Cr�e une nouvelle AgenceResa en v�rifiant l'agence
	*
	* @param Agence $agence
	* @param int $id
	* @return AgenceResa
	*/
	static public function factory(Agence $agence, $id)
	{
		$x = new AgenceResa();
		$x->load($id);
		// l'agence doit correpondre, le point de vente peut varier
		if ($x['agence'] == $agence['agence']) {
			// cr�er la r�servation sous-jacente
			$data = $x->toArray(array('type','categorie','agence','distance'));
			if ($x['agence'] != $x['pdv']) {
				// il faut pr�ciser le point de vente
				$data['agence'] = $x['pdv'];
			}
			$data['age'] = 25;
			$data['permis'] = 5;
			foreach(array('debut'=>'depart', 'fin' => 'retour') as $k => $v) {
				$t = strtotime($x->getData($k));
				$data[$v] = date('d-m-Y', $t);
				$data['h_'.$v] = date('H:i', $t);
			}
			// finir de compl�ter la r�servation pour contr�ler son comportement
			if ($x->_reservation = Reservation::create($data, $msg, $url)) {
				foreach(array('forfait','km','tarif','surcharge','total') as $k)
					$x->_reservation->setData($k, $x->getData($k));
				if (!$x->_reservation->isLocapass()) {
					$x->_reservation->setData('forfait_type', 'RESAS');
				}
				$x->_reservation->setData('paiement', $x->getData('validation'));
				return $x;
			}
		}
		return null;
	}


	/**
	* Renvoie une AgenceResa avec la Reservation sous-jacente
	*
	* @param array $data
	* @param array $msg
	* @return AgenceResa
	*/
	static public function create(AccesAgence $resasUser, Agence $agence, $data, &$msg, $fixGRF = false)
	{
		// la GRF est-elle incluse ?
		$grf_inclus = $resasUser->getConfigData('grf_inclus', 'agence', $agence['id']);
		if ($grf_inclus && $fixGRF) {
			// on ajoute la GRF si l'ARF n'est pas pr�sente
			$type = strtolower($data['type']);
			if (in_array($type, array('vp','vu')) && (!isset($data['options']) || is_array($data['options']))) {
				if (!in_array('af'.$type, $data['options'])) {
					$data['options'][] = 'rf'.$type;
				}
			}
		}

		// cr�er la r�servation
		if ($reservation = Reservation::create($data, $msg, $url))
		{
			// la GRF incluse dans la r�servation ?
			$reservation->setData('grf_inclus', $grf_inclus);
			// changer le type de forfait pour contr�ler le comportement
			if (!$reservation->isLocapass()) {
				$reservation->setData('forfait_type', 'RESAS');
			}
			// cr�er AgenceResa
			$a = $reservation->toArray(array('statut','agence','pdv','alias','type','categorie','debut','fin','distance','jours','duree','km', 'coupon', 'forfait_type'));
			$a['id'] = '';
			$a['canal'] = 'LOC';
			$a['agent'] = $resasUser['login'];
			if (isset($data['vendeur']) && trim($data['vendeur'])) {
				$a['vendeur'] = filter_var(trim($data['vendeur']), FILTER_SANITIZE_STRING);
			}
			$a['ip'] = $_SERVER['REMOTE_ADDR'];
			$x = new AgenceResa($a);
			$x->_reservation = $reservation;
			$x->_resasUser = $resasUser;
			$x->setData('grf_inclus', $grf_inclus);
			if (isset($data['commentaire'])) {
				$x->setData('commentaire', filter_var(trim($data['commentaire']), FILTER_SANITIZE_STRING));
			}
			if (isset($data['commentaire_client'])) {
				$x->setData('commentaire_client', filter_var(trim($data['commentaire_client']), FILTER_SANITIZE_STRING));
			}
			// si un forfait est associ� on r�cup�re les donn�es
			if ($f = $reservation->getForfait()) {
				// les donn�es tarifaires et le choix du v�hicule
				foreach(array('local_tarif_ttc','local_promo_ttc','local_yield_ttc','unipro_vehicule','operation') as $k) {
					$x->setData($k, $f->getData($k));
				}
				// les informations sur le forfait, le kilom�trage et les promotions
				foreach(array('local_km' => 'km', 'local_forfait' => 'forfait', 'local_promo_id' => 'promotion','local_yield' => 'yield') as $k => $v) {
					$x->setData($v, $f->getData($k));
				}
				foreach(array('tarif','surcharge','total') as $k) {
					$x->setData($k, $reservation->getData($k));
				}
				// s'il y a la GRF il faut r�tablir les choses.. puisqu'on a ajotu� l'option au dessus...
				if ($fixGRF) {
					if ($grf_prix = $f->getData('grf_prix')) {
						foreach(array('local_tarif_ttc','tarif','total') as $k) {
							$x->setData($k, round($x->getData($k) - $grf_prix, 2));
						}
					}
				}
			}
			return $x;
		}
		return null;
	}
	/**
	* Renvoie la r�servation associ�e si existante
	* @returns Reservation
	*/
	public function getReservation()
	{
		return $this->_reservation;
	}
	/**
	* Renvoie l'agence associ�e si existante
	* @returns Agence
	*/
	public function getAgence() {
		return $this->_reservation->getAgence();
	}
	/**
	 * Renvoie un PartenaireCoupon
	 * @returns PartenaireCoupon
	 */
	public function getPartenaireCoupon()
	{
	    if (!$this->_coupon && $this->_data['coupon'])
	        $this->_coupon = PartenaireCoupon::factory($this->_data['coupon']);
	    return $this->_coupon;
	}

	/**
	* Renvoie le client associ�e si existant
	* @returns AgenceClient
	*/
	public function getClient()
	{
		if (!$this->_client && $this->_data['client']) {
			$this->_client = AgenceClient::factory($this->getAgence(), $this->_data['client']);
		}
		return $this->_client;
	}
	/**
	* Renvoi la description de l'op�ration choixie
	*
	*/
	public function getOperationLabel() {
		if ($this->_data['operation']) {
			return self::$OPERATION_LABEL[$this->_data['operation']];
		}
	}

	/**
	* Renvoie les offres pour cette r�servation
	* @returns Offre_Collection
	*/
	public function getOffres()
	{
		if ($this->_reservation)
		{
			if (!$this->_offres)
			{
				$this->_offres = $this->_reservation->getOffres('resas');
				if ($this->_offres && $this->_resasUser) {
					// charger les tarifs UNIPRO
					$this->_offres->loadTarifsUnipro($this->_resasUser);
				}
			}
		}
		return $this->_offres;
	}
	/**
	* Associe un client � la r�servation
	*
	* @param AgenceClient $client
	* @returns AgenceResa
	*/
	public function setClient(AgenceClient $client) {
		if ($client && !$this->_client && !$this->_data['client']) {
			if ($this->getData('agence') == $client->getData('agence')) {
				$this->_client = $client;
				$this->setData('client', $client->getId());
				$this->setData('email', $client->getData('email'));
			}
		}
		return $this;
	}
	/**
	* Renvoie une Cat�gorie
	* @returns Categorie
	*/
	public function getCategorie() {
		return $this->_reservation->getCategorie();
	}
	/**
	* Renvoie la liste des options
	* @return ResOption_Collection
	*/
	public function getResOptions() {
		if ($this->getId()) {
			if (!$this->_options) {
				$this->_options = ResOption_Collection::factory($this);
			}
			return $this->_options;
		} else {
			return $this->_reservation->getResOptions();
		}
	}
	public function getAllOptions() {
		return $this->getReservation()->getAllOptions();
	}
	public function getDebut($format = null) {
		return $this->_reservation->getDebut($format);
	}
	public function getFin($format = null) {
		return $this->_reservation->getFin($format);
	}
	/**
	* Renvoie le nombre de kilom�tres
	*
	* @param bool $withKmSupp
	* @returns string
	*/
	public function getKm($withKmSupp = false) {
		return $this->_reservation->getKm($withKmSupp);
	}
	public function getPrixTotal() {
		return $this->_reservation->getPrixTotal();
	}
	public function getPrixTotalHT()
	{
		// certaines options n'ont pas de TVA
		$courtage = $this->getResOptions()->getTotal('COURTAGE');
		return Tva::TTC2HT($this->getPrixTotal() - $courtage) + $courtage;
	}
	public function getPrixOptionsHT()
	{
		// les asurances sont sans TVA
		$courtage = $this->getResOptions()->getTotal('COURTAGE');
		return Tva::TTC2HT($this->getPrixOptions() - $courtage) + $courtage;
	}
	public function getPrixOptions()
	{
		return $this->getResOptions()->getTotal();
	}
	public function getPrixForfait()
	{
		return $this->_reservation->getPrixForfait();
	}

	/**
	* Enregistre la r�servation
	*
	* @param string $cols
	* @return AgenceResa
	*/
	public function save($cols = '*')
	{
		if (!$this->getId())
		{
			// on enregistre
			$this->setData('id', '');
			parent::save();
			if ($this->_reservation)
				$this->_options = $this->_reservation->getResOptions();
			if ($this->_options)
				$this->_options->setReservation($this);
		}
		else
		{
			parent::save($cols);
		}
		return $this;
	}
	/**
	* Renvoie le tableau d'informations sur la GRF
	* @returns array
	*/
	public function getGRFInfo() {
		if (!$this->_grfInfo) {
			$this->_grfInfo = array('prix' => 0, 'prix_jour' => 0);
			if ($this->getData('grf_inclus')) {
				/** @var Option */
				if ($rf = $this->_reservation->getAllOptions(null, false, true)->getItem('rf'.strtolower($this->_reservation['type']))) {
					$this->_grfInfo['prix_jour'] = $rf->getPrix();
					$this->_grfInfo['prix'] = $rf['prix'];
				}
			}
		}
		return $this->_grfInfo;
	}
	/**
	* Renvoie le prix total de la GRF
	* @returns dobule
	*/
	public function getGRFPrix() {
		$info = $this->getGRFInfo();
		return $info['prix'];
	}
	/**
	* Renvoie le prix par jour
	* @returns string
	*/
	public function getGRFPrixJour() {
		$info = $this->getGRFInfo();
		return $info['prix_jour'];
	}
	/**
	* Renvoie un identifiant RESA
	* @returns string
	*/
	public function getResIDClient() {
		if ($this->_data['id']) {
			return sprintf('RESA%08d%s', $this->getId(), $this->_data['agence']);
		}
		return;
	}
	/**
	* Envoie la r�servation � UNIPRO
	* @param string $operation DEVIS, RESA ou PREPA
	* @param string $msg messages d'erreurs
	* @returns bool
	*/
	public function sendResa($operation, &$msg)
	{
		// si l'op�ration nest pas onnu on arr�te l�
		if (!isset(self::$OPERATION_LABEL[$operation])) {
			$msg = "L'op�ration demand�e est inconnue.";
			return false;
		}
		// on enregistre l'op�ration et on valide la r�servatino
		if ($this->_data['statut'] != 'P') {
			$this->setData('operation', $operation);
			$this->setData('validation', date('Y-m-d H:i:s'));
			$this->setData('statut', 'V');
			$this->save('operation,validation,statut');
		}
		// on envoie le devis par mail si demand�
		if (($operation == 'DEVIS' || $operation == 'RESA' ) && !$this->_data['envoi']) {
			$this->sendDevisByMail();
			$this->setData('envoi', date('Y-m-d H:i:s'));
			$this->save('envoi');
		}
		// on cr�e l'acc�s � UNIPRO
		if (!$this->_data['unipro']) {
			try { $unipro = @new Unipro(); }
			catch (Exception $e) { $e = (string) $e; }
			if (!$unipro) {
				$msg = "Une erreur est survenue lors de la connxion � UNIPRO : ".$e;
				return false;
			}
			if ($unipro->sendAgenceResa($this, $operation)) {
				return true;
			}
			if ($error = $unipro->getLastError()) {
				$msg = $error;
			} else {
				$msg = "Une erreur a eu lieu durant l'envoi � UNIPRO.";
			}
			return false;
		}
		return true;
	}
	/**
	* Envoie le devis par mail
	*
	*/
	public function sendDevisByMail ()
	{
		global $CIVILITE;

		$agence = $this->getAgence();
		$client = $this->getClient();
		$categorie = $this->getCategorie();
		$options = $this->getResOptions();
		$reservation = $this->getReservation();
		// pr�parer les donn�es
		// v�hicule
		$vehicule = $categorie['mnem'].' '.$categorie['nom'];
		if ($categorie['commentaire'])
			$vehicule .= ': <br>'.$categorie['commentaire'];
		// modifier le texte pour les options
		ResOption_Collection::$OPTIONS_EMAIL_DESC['L'] = 'demand�es';

		// remplir le tableau
		$data = array(
			 'date_jour'=>date('d/m/Y')
			,'civilite'	=> $CIVILITE[$client['titre']]
			,'nom'		=> $client['prenom'].' '.$client['nom']
			,'adresse'	=> $client['cp']
			,'debut'	=> $reservation->getDebut()
			,'fin'		=> $reservation->getFinAvecVerificationHoraires()
			,'jours'	=> $reservation->getDuree()
			,'km'		=> $reservation->getKm()
			,'vehicule_type' => strtolower($reservation['type'])
			,'vehicule' => $vehicule
			,'agence'	=> $agence->getHtmlAddress(' ', 'nom,adresse1,cp_ville')
		    ,'agence_name'	=> $agence->getHtmlAddress(' ', 'nom')
		    ,'agence_adress'	=> $agence->getHtmlAddress(' ', 'adresse1,cp_ville')
			,'agence_email' => $agence['email']
			,'agence_bal'=> $agence->getBal('strong', null, '<br>')
			,'agence_url'=> $agence->getURLAgence()
			,'commentaire_agence'=> $agence['commentaire']
			,'commentaire_agence_localisation'=> $agence['commentaire_localisation']
			,'commentaire_agence_offre'=> $agence['commentaire_offre']
			,'commentaire_pour_client'=> $reservation['commentaire_client'] ? '<strong>Commentaire de l\'agence:</strong><br>'.$reservation['commentaire_client'].'<br><br>' : ''
			,'agence_tel'		=> $agence['tel']
			,'tarif'	=> number_format($this->getPrixForfait(),2)
			,'tarif_ht'	=> TVA::TTC2HT($this->getPrixForfait())
			,'options'	=> number_format($options->getTotal(),2)
			,'options_ht'=>number_format($this->getPrixOptionsHT(), 2)
			,'surcharge'=> $this->_getHtmlSurcharge($reservation)
			,'total'	=> $this->getPrixTotal()
			,'total_ht'	=> $this->getPrixTotalHT()
			,'list_options' => $options->toHtmlEmail()
			,'href_cgl'	=> $agence->getHrefCGL()
				,'resa_id' => $this->getResIDClient()
		);
		$data['materiel_retrait'] = '';
		if ($retrait = $options->getDateRetrait())
		{
			if ($reservation->getHoursBeforeStart() <= 48)
				$data['materiel_retrait'] = "Confirmation de votre commande de mat�riels sous 24h";
			else
				$data['materiel_retrait'] = "Le mat�riel de d�m�nagement sera � votre disposition dans votre agence � la date que vous avez s�lectionn� : le ".Util_Format::dateFR('l d F Y', strtotime($retrait)).'.';
		}

		// pr�parer le mail
		$html = load_and_parse(BP.'/emails/em-resas-devis.html', $data);
		$bcc = BCC_EMAILS_TO;
		if (SITE_MODE == 'PROD') {
			$bcc .=','.$agence['email'];
		}

		//dans le cas @nomail.ada.fr
		if (substr($client['email'],-14)=="@nomail.ada.fr") {
		    $client['email']=$agence['email'];
		}

		// envoyer le mail
		send_mail (
			$html,
			$client['email'],
			self::SUBJECT_RESAS_DEVIS,
			$agence['nom'],
			$agence['email'],
			'',
			$bcc
		);
	}
	/**
	* Renvoie le HTML associ�e � une surcharge
	*
	* @param Reservation $reservation
	* @returns string
	*/
	protected function _getHtmlSurcharge()
	{
		if ((int)$this->_data['surcharge'] > 0)
		{
			$html = '<tr>';
			$html.='<td width="180" height="22" style="font:bold 12px arial;"><img src="img/puce1.gif" alt=""><strong>'.$this->getAgence()->getSurchargeLabel().'</strong></td>';
			$html.='<td width="144" align="right" background="img/bg_case.gif" style="font:bold 12px arial;">'.$this->_data['surcharge'].' &euro; TTC</td>';
			$html.='</tr>';
			return $html;
		}
	}

	public function isLocapass()
	{
	    // recupere le coupon avec $this->get-partenairecoupon();
        $coupon = $this->getPartenaireCoupon();
        if( $coupon) {
            return !is_null($coupon['locapass']);
        }
	    //return ($this->_data['forfait_type'] == 'LOCAPASS');
	}


}


?>
