<?
class Page_Autoeurope extends Page
{
	/** La page supporte-t-elle le HTTPS ? @returns bool */
	protected function isThatPageNeedsHttps()
	{
		return (self::getProtocol()=='https');
	}
	
	protected function checkParameters()
	{
		// simuler le HTTPS si demand�
		if ($_GET['https']) {
			$_SERVER['HTTPS'] = 'on';
		}
		// appeler la version de base
		if (!parent::checkParameters())
			return false;
		$this->_baseHref = false;
		$this->_linkTop = false;
	}
}
?>
