<?
	require_once("secure.php");
	require_once("lib.php");

	if (!$adminUser || !$adminUser->hasRights(Acces::SB_GESTION)) die("Vous ne pouvez pas afficher cette page");
	
	// identifiant
	$id = $_REQUEST['agence'];
	$agence = Agence::factory($id);
	if(!$agence)
		die('Vous devez pr�ciser une agence !');
	// modification, cr�ation
	if(getarg('update') || getarg('create'))
	{
		// on supprimer les informations actuellement pr�sentes
		getsql("DELETE FROM agence_option WHERE agence='$id'");
		foreach ($_POST['agence_option'] as $k => $row)
		{
			if ($row['paiement'])
				save_record('agence_option', $row);
		}
		// mette � jour agence.livraison_km
		$agence->updateLivraisonKm();
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script>
	<!-- //
	// doSubmit
	function doSubmit(frm)
	{
		var err = "";
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		return true;
	}
	// -->
	</script>
</head>
<body>
<form action="" method="post" onsubmit="return doSubmit(this);" enctype="multipart/form-data">
<input type="hidden" name="agence" value="<?=$id?>">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre">
		<img src="img/puce.gif" align="absmiddle" hspace="1">
		<a href="agence.php?id=<?=$id?>"><?=$agence['nom'].' ('.$id.')'?></a>
		<br/>Personnalisation des options
	</th>
</tr>
<tr>
	<td>
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="10%">
		<col width="40%">
		<col width="25%">
		<col width="25%">
		<tr>
			<th>Option</th><th>Nom</th><th>D�faut</th><th>Agence</th>
		</tr>
<?
		// afficher la liste des options
		$sql = "select distinct o.type, o.id, o.nom, o.paiement paiement_defaut, ao.paiement\n";
		$sql.=" from `option` o\n";
		$sql.=" join option_tarif t on (t.zone='".$agence['zone']."' and t.reseau='".$agence['reseau']."' and t.`option`=o.id)\n";
		$sql.=" left join agence_option ao on ao.agence='$id' and ao.`option`=o.id\n";
		$sql.=" where o.par_agence=1\n";
		$sql.=" group by o.type, o.id, o.nom, o.paiement, o.position, t.prix, ao.paiement\n";
		$sql.=" order by o.type, o.position, o.id";
		$rs = sqlexec($sql);
		if (!$rs || !mysql_num_rows($rs))
			echo '<tr><td colspan="4">Aucune option configurable !</td></tr>'."\n";
		while ($row = mysql_fetch_assoc($rs))
		{
			if ($type != strtoupper($row['type']))
			{
				$type = strtoupper($row['type']);
				echo '<tr><th colspan="4">'.$type."</th></tr>\n";
			}
			$fld = 'agence_option['.$row['id'].']';
			echo '<tr>';
			echo '<td><a href="option.php?id='.$row['id'].'">'.$row['id'].'</a></td>';
			echo '<td>'.$row['nom'].'</td>';
			echo '<td>'.Option::$MODE_PAIEMENT[$row['paiement_defaut']].'</td>';
			echo '<td>';
			echo '<input type="hidden" name="'.$fld.'[agence]" value="'.$id.'"/>';
			echo '<input type="hidden" name="'.$fld.'[option]" value="'.$row['id'].'"/>';
			selectarray($fld.'[paiement]', Option::$MODE_PAIEMENT, $row['paiement'], '-- par d�faut --');
			echo '</td>';
			echo "</tr>\n";
		}
?>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%">
		<tr>
			<td align="right">
				<input type="submit" name="<?=$id?"update":"create"?>" value="enregistrer" onclick="return doSubmit(this.form);">
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
</form>
</body>
</html>

