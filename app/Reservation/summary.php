<?
// rappel de la réservation dans skin www
/** @var Reservation */
$x = $this;
$agence = $x->getAgence();
?>
	<div>Location du <span class="rouge"><?=$x->getLongDate('debut');?> - <?=$x->getDebut('H:i')?></span>
		au <span class="rouge"><?=$x->getLongDate('fin')?> - <?=$x->getFin('H:i')?></span>
	</div>
	<div style="margin-top: 10px;">
		<strong>Agence</strong><?=$agence->getHtmlAddress(' ','nom,adresse1,cp_ville')?>
		<br/>
		<a href="../popup.php?page=agences/agence&id=<?=$agence['id']?>" onclick="return wOpen(this.href, 630, 800)" style="font-size:10px;color:#cc0000">Informations sur l'agence</a>
	</div>
	<?
		echo $agence->getBal('div', 'class="agence_bal"');
		if ($str = $agence['commentaire'])
			echo '<div style="margin-top: 10px;">'.$str.'</div>';
	?>
	<p class="clear"></p>

