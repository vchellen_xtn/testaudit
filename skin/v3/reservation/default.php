<div class="header">
	<? if ($navResa = $this->getNavResa()) : ?>
	<? $i = 1; ?>

	<ol id="nav-resa" class="nav-resa nav-resa<?=count($navResa)?>">
		<li>
			<a href="../" class="item-chemin-resa rouge nouvelle_recherche">y</a>
		</li>
				<? foreach($navResa as $k => $title) : ?>
		<li>
			<p class="<? if ($k == $this->getAction()) echo 'current ';?>item-chemin-resa" id="<?php echo "nav".$i ?>">
				<span class="number"><?=$i++?></span>
				<span class="title"><?=$title?></span>
			</p>
	    </li>
		<? endforeach; ?>
	</ol>
	<? endif; ?>
</div>
<div class="bg">
	<div class="content tpl_reservation">
	<?
		include($this->getPagePath());
	?>
		<div class="clear"></div>
	</div>
</div>