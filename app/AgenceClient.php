<?php

class AgenceClient extends Ada_Object
{
	/** @var Agence */
	protected $_agence;

	/**
	* Cr�e un objet � partir d'un identifiant
	*
	* @param mixed $agence
	* @param string $id
	* @return AgenceClient
	*/
	static public function factory($agence, $id)
	{
		// v�rifier les arguemnts
		if (!is_numeric($id) && !filter_var($id, FILTER_VALIDATE_EMAIL))
			return null;
		if (!is_object($agence) && Agence::isValidID($agence)) {
			$agence = Agence::factory($agence);
			if (!$agence) return null;
		}
		// on v�rifie l'identifiant
		// cr�er le client
		$x = new AgenceClient();
		$x->_agence = $agence;
		if ($x->load($id)->isEmpty())
			return null;
		return $x;
	}

	/**
	* Cr�ation d'un nouveau client avec validation des donn�es
	*
	* @param Agence $agence
	* @param array $data
	* @param array $errors
	* @returns Client
	*/
	static public function create(Agence $agence, &$data, &$errors)
	{
		// le client existe-t-il d�j� ?
		$x = AgenceClient::factory($agence, $data['email']);
		if (!$x) {
			$x = new AgenceClient(array('id'=>'', 'agence'=>$agence['agence']));
			$x->_agence = $agence;
		}

		// v�rifier les champs avant de les enregistrer
		$fields = array('email','unipro', 'titre','prenom','nom','naissance','societe','adresse','adresse2','cp','ville','pays','tel','gsm','promo');
		if ($x->validateData($data, $errors, $fields)) {
			// si promo existait d�j� on le conserve s'il n'est pas fourni
			if (!$data['promo'])
				$data['promo'] = 'N';
			// on met les nouvelles valeurs
			foreach($fields as $k) {
				if (isset($data[$k])) {
					if ($k == 'cp' || $k == 'ville') {
						// si on change le code postal ou la ville, il faut remettre l'adresse � vide
						if ($x[$k] != $data[$k]) {
							foreach(array('adresse', 'adresse2') as $u) {
								if (!isset($data[$u]) && $x->hasData($u)) {
									$x->setData($u, null);
								}
							}
						}
					}
					$x->setData($k, $data[$k]);
				}
			}
			$x->save();
			// s'il y a un optin on l'enregistre dans la newsletter (Prospect)
			if ($data['promo'] == 'O') {
				$prospect = Prospect::factory($data['email']);
				if ($prospect && !$prospect->isEmpty()) {
					if ($prospect['desabo']) {
						$prospect->setData('desabo', 0);
						$prospect->save('desabo');
					}
				} else {
					// sinon cr�er le prospect
					$a = $data;
					$a['origine'] = 'RESAS';
					$a['agence'] = $x['agence'];
					$a['naissance'] = '0000-00-00';
					unset($a['id']);
					$y = Prospect::create($a, $e);
				}
			}
			return $x;
		}
		return null;
	}

	/* pr�pare le SQL pour charger les donn�es clients */
	protected function _prepareSQL($id, $cols='*', $data=null)
	{
		$sql = "select c.* from {$this->getTableName()} c";
		$sql.=" where c.agence='".$this->_agence['agence']."'";
		if (is_numeric($id))
			$sql.= " and c.id='".(int)$id."'";
		else
			$sql.=" and c.email = '".mysql_real_escape_string(filter_var($id, FILTER_SANITIZE_EMAIL))."'";
		return $sql;
	}

	/**
	* Renvoie le tableau des r�gles de validation
	*/
	protected function _getValidatingRules()
	{
		return array (
//			'id'	=> $this->_addRuleInt(),
//			'agence'=> $this->_addRuleRegExp('/^FR[A-Z0-9]{4}$/', "L'agence doit �tre indiqu�e", "L'identifiant de l'agence n'est pas valide"),
			'email'	=> $this->_addRuleEmail("Vous devez pr�ciser l'e-mail", "L'e-mail %s n'est pas valide !"),
			'titre'	=> $this->_addRuleString("Vous devez indiquer la civilit�"),
			'prenom'=> $this->_addRuleString("Vous devez indiquer le pr�nom"),
			'nom'	=> $this->_addRuleString("Vous devez indiquer le nom"),
			'naissance'	=> $this->_addRuleDate(null, "La date de naissance n'est pas valide"),
			'societe'	=> $this->_addRuleString(),
			'adresse'	=> $this->_addRuleString(),
			'adresse2'	=> $this->_addRuleString(),
//			'cp'	=> $this->_addRuleString("Vous devez pr�ciser le code postal."),
			'cp'	=> $this->_addRuleString(),
			'ville'	=> $this->_addRuleString(),
			'pays'	=> $this->_addRuleString(),
			'tel'	=> $this->_addRulePhone("Vous devez pr�ciser un num�ro de t�l�phone", "Le num�ro de t�l�phone %s n'est pas valide!"),
			'gsm'	=> $this->_addRulePhone(null, "Votre num�ro de t�l�phone mobile %s n'est pas valide !"),
			'promo'	=> $this->_addRuleRegExp('/^[ON]$/', null, "La valeur de l'optin n'est pas valide !"),
//			'partenaire'	=> $this->_addRuleString(),
//			'pgkm'	=> $this->_addRuleBoolean(),
            'unipro'=> $this->_addRuleString()
		);
	}
}
?>