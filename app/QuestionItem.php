<?
class QuestionItem extends Ada_Object
{
	/**
	* Renvoie le HTML d'un QuestionItem
	* 
	* @return string
	*/
	public function getHTML()
	{
		extract($this->_data);
		if ($question_type == 'G')
			$output = '<td class="item">'."\n";
		else
			$output = '<li class="item">'."\n";

		// le champs texte d'une question ouverte
		if ($question_type == 'O')
		{
			//$output.= '<input type="text" id="question_'.$question.'_reponse" name="question-'.$question.'-txt" /></li>'."\n";
			$output.= '<textarea id="question_'.$question.'_reponse" name="question-'.$question.'-txt" rows="4" cols="40"></textarea></li>'."\n";
		}
		$output.= '<input type="'.($question_type == 'O' ? 'hidden' : 'radio').'" id="item-'.$id.'" name="question-'.$question.'[]" value="'.$id.'" />';		
		if ($question_type == 'F')
			$output.= '<label for="item-'.$id.'">'.$libelle.'</label>';
		if ($question_type == 'G')
			$output.= '</td>'."\n";
		else
			$output.= '</li>'."\n";
		return $output;
	}
}