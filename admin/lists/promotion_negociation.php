<?
	include ('../secure.php');
	$title 	 = "Négociations";
	$url 	 = "../promotion_negociation.php";
	$key	 = "p.id";
	$order	 = "";

	$sql = "select p.id, p.depuis, p.jusqua, c.nom as classe, p.nom, p.type, p.jour, p.categories, p.actif";
	$sql.= " from promotion_negociation p";
	$sql.= " left join partenaire_classe c on c.id=p.classe";
	$sql.= " where 1";
	if (isset($_GET['classe']))
		$sql.= " and p.classe=".(int) $_GET['classe'];
	
	$out_url = "list.php?sql=" . urlencode($sql);
	$out_url.= "&title=" . urlencode($title);
	$out_url.= "&url=" . urlencode($url);
	$out_url.= "&new_url=" . urlencode($url . "?" . $_SERVER['QUERY_STRING']);
	$out_url.= "&color=4587ba";
	$out_url.= "&key=".$key."&order=".$order."&nb_rows=30";
	header("location: ".$out_url);
?>
