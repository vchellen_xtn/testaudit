<?php
class Page_Offre extends Page
{
	public function getUniversTitre() {
		return 'Offres';
	}
	
	public function indexAction() {
		// pour l'instant le contenu est vide
		self::redirect(self::getURL('offre/forfaits.html'));
		exit();
	}
	
	// affichage du moteur de recherche
	public function service_voiturierAction()
	{
		$this->addJS('scripts/agence_liste.js');
	}
	public function malinAction() {
		$this->addJS('scripts/agence_liste.js', false);
	}
	public function trajet_uniqueAction() {
		$this->addJS('scripts/agence_liste.js', false);
	}
}
?>
