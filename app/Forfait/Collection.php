<?
class Forfait_Collection extends Ada_Collection
{
	// contr�ole sur la mani�re d'appliquer les promotions
	const PROMO_ALL = 1;		// toutes les promotions
	const PROMO_NATIONAL = 2;	// promotions nationales uniquement
	const PROMO_NONE = 3;		// Aucune promotion � appliquer
	const PROMO_COUPON = 4;		// Promotions uniquement s'il y a un coupon et que le groupe de l'agence est coh�rent avec le groupe de la promotion
	const PROMO_LOCAL = 5;		// Promotions locales uniquement
	
	static public $PROMO_MODES_DESC = array(
		self::PROMO_ALL			=> 'Toutes les promotions',
		self::PROMO_NATIONAL 	=> 'Promotions nationales uniquement',
		self::PROMO_NONE		=> 'Aucune promotion',
		self::PROMO_COUPON		=> 'Promotions avec code promotion et groupe agence sp�cifique',
		self::PROMO_LOCAL		=> 'Promotions locales uniquement',
	);
	
	/* promotions d�finies dans agence_pdv.promo_mode */
	static public $PROMO_MODES = array(
		'locales'	=> self::PROMO_LOCAL,
		'nationales'=> self::PROMO_NATIONAL,
		'aucune'	=> self::PROMO_NONE
	);
	static public $PROMO_MODES_LBL = array(
		'locales'	=> 'uniquement les promotions locales',
		'nationales'=> 'uniquement les promotions nationales',
		'aucune'	=> "aucune promotion ne doit s'appliquer"
	); 
	
	
	// choix du tarif � appliquer (forfait2_tarif ou agence_forfait_tarif)
	const TARIF_NATIONAL = 'ADAFR';
	const TARIF_LOCAL = 'RESAS';
	
	protected $_forfaits = null; // tableau des forfaits pour toutes les cat�gories
	protected $_kilometrages = array();
	protected $_tarifScope = null;
	protected $_promoRestrictions = null;
	
	/**
	* Renvoie les forfaits r�pondant � une r�servation
	* Les forfiats sont calcul�s � partir de :
	* 	$zone, $agence, $debut, $fin, $saison, $categorie, $j1, $j2, $duree, $distance, $illimite, $coupon
	* @param Reservation $reservation
	* @param mixed $catID identifiant de la cat�gorie ou tableau d'identifiants
	* @param int $promoRestrictions nature des restrictions sur les promoitons
	* @param string $tarifScope Scope pour les tarifs
	* @param PromotionNegociation $negociation N�gociation avec une classe particuli�re
	* 
	* @returns Forfait_Collection
	*/
	static public function factory(Reservation $reservation, $catID = null, $promoRestrictions = null, $tarifScope = self::TARIF_NATIONAL, PromotionNegociation $negociation = null)
	{
		// pour identifier les kilom�trages proppos�s
		$kilometrages = array();
		$hasIllimite = false;
		
		if (!$catID)
			$catID = $reservation['categorie'];
		if (!is_array($catID))
			$catID = array($catID);
		$agence = $reservation->getAgence();
		$zone = $agence['zone'];
		$reseau = $agence['reseau'];
		$groupe = $agence['groupe'];
		$type = strtolower($reservation['type']);
		$debut = $reservation['debut'];
		$fin = $reservation['fin'];
		$saison = $reservation['saison'];
		$age = (int) $reservation['age'];
		
		$j1 = $reservation->getJ1();
		$j2 = $reservation->getJ2();
		$jour_du_mois = $reservation->getDebut('j');
		$duree = (int) $reservation['duree'];
		$distance = (int) $reservation['distance'];
		$illimite = $reservation['illimite'];
		
		$coupon = $reservation->getPartenaireCoupon();
		if (!$coupon)
			$coupon = $reservation->getOrigineCoupon();
		$jours_avant = floor( $reservation->getHoursBeforeStart() / 24);

		// les classes de promotion
		$promoClasses = array();
		if ($coupon && !is_null($coupon['classe'])) {
			$promoClasses[] = (int) $coupon['classe'];
		}
		if ($negociation && !is_null($negociation['classe'])) {
			$promoClasses[] = (int) $negociation['classe'];
		}
		
		// 2012-02-28 : HACK horrible pour �viter que SERENIS voit les promotions lcoales pour Vente priv�e....
		// 2014-02-24 : Neutralisation des promotions pour NICE A�roport
		// 2014-04-11 : en mode RESAS, il faut tenir compte des promotions pour SERENIS
		// 2014-09-26 : et on force la tarification nationale dans ce cas
		if (IS_SERENIS && $reservation['forfait_type']!='RESAS') { 
			$promoRestrictions = self::PROMO_NONE;
			$tarifScope = self::TARIF_NATIONAL;
		} else if ($agence['promo_mode']) {
			$promoRestrictions = self::$PROMO_MODES[$agence['promo_mode']];
		}

		// rechercher le meilleur tarif
		$sql = "select SQL_NO_CACHE f.id, f.nom, f.type, f.prefix, t.categorie categorie, f.jours, f.jours_max, f.km, f.km_jour, f.km_max,\n";
		if ($tarifScope == self::TARIF_NATIONAL) {
			$sql.= "	t.prix, t.prix_jour, 'national' prix_origine,\n";
		} else {
			$sql.= "	COALESCE(at.prix, st.prix, t.prix) prix, COALESCE(NULLIF(at.prix_jour,0), NULLIF(st.prix_jour,0), t.prix_jour) prix_jour, CASE WHEN COALESCE(at.prix, st.prix) IS NOT NULL THEN 'local' ELSE 'national' END prix_origine,\n";
		}
		$sql.= "	c.mnem, COALESCE(t.km_sup, cz.km_sup, c.km_sup) km_sup, (f.nopromo + c.nopromo) nopromo,\n"; // co�t du km suppl�mentaire pour la zone
		$sql.= "	p.id p_id, p.nom p_nom, p.agence p_agence, p.classe p_classe, p.mode p_mode, p.montant p_montant, NULL p_km_sup, NULL p_prix_jour, date_format(p.fin, '%d/%m') p_fin,\n";
		$sql.= "	p.promo_type, p.km_min p_km_min, p.km_max p_km_max,\n";
		$sql.= "	case when f.depart_debut is null then 1 else 0 end as non_borne\n";
		$sql.= "from forfait2 f\n";
		if ($tarifScope == self::TARIF_LOCAL) {
			$sql.= "  join forfait2_tarif t on (t.zone='$zone' and t.forfait=f.id and t.categorie IN (".join(',', $catID).") and t.groupe".($groupe ? "=$groupe" :" is null")." and t.saison is null)\n";
			$sql.= "  left join agence_forfait_tarif at on (at.agence='".$agence['agence']."' and at.forfait=t.forfait and at.categorie=t.categorie )\n";
			$sql.= "  left join agence_forfait_tarif st on (st.agence='".$agence['code_societe']."' and st.forfait=t.forfait and st.categorie=t.categorie)\n";
		} else {
			$sql.= "  join forfait2_tarif t on (t.zone='$zone' and t.forfait=f.id and t.categorie IN (".join(',', $catID).") and t.groupe".($groupe ? "=$groupe" :" is null")." and t.saison".($saison ? "=$saison" :" is null").")\n";
		}
		$sql.= "  join categorie c on c.id = t.categorie\n";
		$sql.= "  left join categorie_zone cz ON (cz.zone=t.zone and cz.categorie=t.categorie)\n";
		// faut-il limiter le stop sell pour le calcul sur ADAFR uniquement ? si oui, comment sur RESAS obtenir le tarif ADAFR ?
		// Attention! cf plus bas where s.id is null
		if (true || $reservation['forfait_type'] != 'RESAS') {
			$sql.= "  left join stop_sell s\n";
			$sql.= "        on ((s.agence is null or s.agence='".addslashes($reservation['agence'])."' or s.agence='".addslashes($reservation['pdv'])."')\n";
			$sql.= "        and s.zone='".$zone."'\n";
			$sql.= "        and (s.type is null or s.type=f.type)\n";
			$sql.= "        and (s.reseau is null or s.reseau='$reseau')\n";
			$sql.= "        and (s.groupe is null or s.groupe='$groupe')\n";
			$sql.= "        and (s.code_societe is null or s.code_societe='".addslashes($agence['code_societe'])."')\n";
			$sql.= "        and (s.categorie is null or s.categorie=t.categorie)\n";
			$sql.= "        and (s.debut is null or s.debut <= '".addslashes($fin)."')\n";
			$sql.= "        and (s.fin is null or s.fin >= '".addslashes($debut)."')\n";
			$sql.= "        and s.nb is null\n";
			$sql.= "        and (coalesce(s.forfait, s.forfait_prefix) is not null)\n";
			$sql.= "        and (s.forfait_prefix is null or f.id like s.forfait_prefix)\n";
			$sql.= "        and (s.forfait is null or s.forfait=f.id)\n";
			$sql.= "        )\n";
		}
		$sql.= "  left join agence_promo_forfait p on (\n";
		if ($promoRestrictions == self::PROMO_NONE) {
			$sql.= "            (0)\n";
		} else if ($promoRestrictions == self::PROMO_NATIONAL) {
			$sql.= "            (p.agence is null)\n";
		} else if ($promoRestrictions == self::PROMO_LOCAL) {
			// l'agence exacte ou les coupons de r�duction
			$sql.= "            (p.agence='".addslashes($reservation['pdv'])."' OR (p.agence IS NULL AND p.classe IS NOT NULL))\n";
		} else {
			$sql.= "            (p.agence is null or p.agence='".addslashes($reservation['pdv'])."')\n";
		}
		$sql.= "        and (p.zone='".$zone."')\n";
		$sql.= "        and (p.reseau is null or p.reseau='".$reseau."')\n";
		$sql.= "        and (p.perimetre='".$tarifScope."' OR p.classe IS NOT NULL)";
		if ($groupe && ($promoRestrictions == self::PROMO_COUPON))
			$sql.= "        and (p.groupe='$groupe')\n";
		else
			$sql.= "        and (p.groupe is null or p.groupe='$groupe')\n";
		if ($promoRestrictions == self::PROMO_NATIONAL)
			$sql.= "        and (p.code_societe is null)\n";
		else
			$sql.= "        and (p.code_societe is null or p.code_societe='".addslashes($agence['code_societe'])."')\n";
		$sql.= "        and (p.type is null or p.type=f.type)\n"; 
		$sql.= "        and f.nopromo=0 and c.nopromo=0 and (p.categories is null or find_in_set(c.mnem, p.categories))\n";
		$sql.= "        and (p.forfait is null or p.forfait=f.prefix)\n";
		// p.km_min et p.km_max ne peuvent pas �tre dans le SQL : la distance n'est pas toujours indiqu�e et le kilom�trage des forfaits peuvent �tre mdoifi�es s'ils sont additionn�es
		$sql.= "        and (p.jours_min is null or ".$duree." >= p.jours_min)\n";
		$sql.= "        and (p.jours_max is null or ".$duree." <= p.jours_max)\n";
		$sql.= "        and (p.debut is null or p.debut <= '".substr($debut,0,10)."')\n";
		$sql.= "        and (p.fin is null or p.fin >= '".substr($fin,0,10)."')\n";
		$sql.= "        and (p.jour_mois_debut is null or p.jour_mois_debut <= ".$jour_du_mois.")\n";
		$sql.= "        and (p.jour_mois_fin is null or p.jour_mois_fin >= ".$jour_du_mois.")\n";
		$sql.= "        and (p.actif=1)\n";
		$sql.= "        and (p.depuis IS NULL OR p.depuis <= CURRENT_DATE)\n";
		$sql.= "        and (p.jusqua IS NULL OR p.jusqua >= CURRENT_DATE)\n";
		$sql.= "        and (p.jours_avant IS NULL OR p.jours_avant <= ".$jours_avant.")\n";
		$sql.= "        and ($age BETWEEN IFNULL(p.age_min, 18) AND IFNULL(p.age_max, 120))\n";
		if ($promoRestrictions == self::PROMO_COUPON)
			$sql.= "        and (p.classe=".(int) $coupon['classe'].")\n";
		else if (!empty($promoClasses))
			$sql.= "        and (p.classe IS NULL OR p.classe IN (".implode(',', $promoClasses)."))\n";
		else
			$sql.= "        and p.classe IS NULL\n";
		$sql.= "        )\n";
		$sql.= " where f.type = '".$reservation['type']."'\n";
		$sql.= "   and f.jours_min <= $duree\n";
		$sql.= "   and f.jours_max >= $duree\n";
		$sql.= "   and (($j1 between f.depart_debut and f.depart_fin and $j2 between f.retour_debut and f.retour_fin + 0.005) or f.depart_debut is null)\n";
		if ($distance > 0) {
			$sql.="  and (f.km_max is null or f.km_max >= $distance)\n";
		}
		if (true || $reservation['forfait_type'] != 'RESAS') {
			$sql.= "   and s.id is null\n";
		}
		$sql.= " order by non_borne\n";

// echo "<pre>$sql</pre>";

		// dans certains cas, on veut r�duire d'une demi-journ�e le tarif
		$deltaDay = 0;
		if ($duree > 1 && $zone=='fr' && $agence['reseau']=='ADA' && in_array($reservation['forfait_type'], array('ADAFR','RESAS'))
			&& $reservation->getDebut('H:i') >= '17:00' && $reservation->getFin('H:i') <= '09:00'
		)
		{
			// 2014-11-13 : on n'enl�ve plus une demi-journ�e au tarif...
			// $deltaDay = 0.5;
		}

		// calculer le co�t pour chaque forfait s�lectionn�
		// on calcule le co�t avec la promotion en parall�le
		for($rs=sqlexec($sql); $ref=mysql_fetch_assoc($rs);)
		{
			// ne pas parcourir les forfaits non born�s si au moins un forfait borne correspond � la demande
			if ($ref['non_borne'] == 0)
				$borne[$ref['categorie']] = true;
			else if ($borne[$ref['categorie']])
				continue;

			// on peut multiplier chaque forfait saus pour le type ML (� l'heure)
			$ref['duree'] = $duree;
			for ($i=1; ($i==1 || $type!='ml'); $i++)
			{
				// on peut multiplier le forfait jusqu'au maximum
				$row = $ref;
				if (($row['jours'] = $i * $ref['jours']) > $row['jours_max'])
					break;
				// ..en ajustant en proportion le tarif et le nb de km
				if (!is_null($row['km']))
					$row['km'] = $i * $row['km'];
				$row['prix'] = $i * $row['prix'];
				// pour une promotion on initialise avec le prix de base
				$row['km_sup'] = round($row['km_sup'], 2);
				if ($row['p_id'])
				{
					$row['p_prix'] = $row['prix'];
					if (!$row['p_prix_jour']) $row['p_prix_jour'] = $row['prix_jour'];
					if (!$row['p_km_sup']) $row['p_km_sup'] = $row['km_sup'];
				}

				// ajuster le tarif par rapport � la dur�e
				// dans certains cas, on retranche une demi-journ�e...
				$x = max(0, $duree - $row['jours']);
				if ($x > 0 || $deltaDay > 0)
				{
					// on applique la r�duction d'une 1/2 journ�e sauf pour les cat�gorie "nopromo"
					if ($deltaDay > 0 && !$row['nopromo'])
						$x -= $deltaDay;
					$row['prix'] += $x * $row['prix_jour'];
					if ($row['p_id'])
						$row['p_prix'] += $x * $row['p_prix_jour'];
					if (!is_null($row['km']) && $x > 0)
						$row['km'] += $x * $row['km_jour'];
					$row['jours'] = $duree;
				}
				// pour les forfaits Dimancche, on ram�ne tout � une journ�e
				if ($row['prefix'] == 'DIMVU') {
					$row['duree'] = 1;
					$row['jours'] = 1;
				}
				// ajouter les kilom�tres sauf pour VP/0 (ADA Premier Prix)
				if ($row['km'] && (strtolower($row['type'])!='vp' || $row['mnem']!='0' || $i==1))
					$kilometrages[$row['km']] = true;
				else
					$hasIllimite = true;

				// s'il y a un km_max au forfait et que la distance indiqu�e est sup�rieure on s'arr�te l�
				if ($row['km_max'] && $distance > $row['km_max'])
					break;
				
				// ajuster par rapport � la distance
				if ($distance && !is_null($row['km']))
				{
 					if (($x = $distance - $row['km']) > 0)
					{
						$row['prix'] += $x * $row['km_sup'];
						if ($row['p_id'])
							$row['p_prix'] += $x * $row['p_km_sup'];
						$row['km'] += $x;
					}
				}
				// on valide que le nombre de kilom�tres est coh�rent avec la promotion
				if ($row['p_id'] && $row['km']) {
					// si le nombre de kilom�tres ne correspond pas, on neutralise la promotion
					// on ne peut pas le mettre dans le SQL car la distance n'est pas connue � l'avance et les forfaits peuvent �tre additionn�s...
					if (($row['p_km_min'] && $row['km'] < $row['p_km_min']) || ($row['p_km_max'] && $row['km'] > $row['p_km_max'])) {
						unset($row['p_id'], $row['p_montant'], $row['p_nom']);
					}
				}

				// s'il y a une promotion p_montant, l'appliquer sur p_prix
				if ($row['p_id'] && $row['p_montant'])
				{
					// 2014-08-28 : le sens de la variation (+ ou -) se trouve dans p_montant
					$row['p_prix'] += ($row['p_montant']) * ($row['p_mode'] == 'P' ? ($row['p_prix']/100) : 1);
					if ($row['p_prix'] < 0) $row['p_prix'] = 0;
				}
				// dans tous les cas conserver le prix minimum dans t_prix et calculer le montant de la promotion
				$row['t_prix'] = (isset($row['p_prix']) ? $row['p_prix'] : $row['prix']);
				if ($row['p_id']) {
					// 2014-08-28 : la r�duction peut �tre n�gative si la promotion �tait une variation positive...
					$row['reduction'] = round($row['prix']) - round($row['p_prix']);
				}
				// arrondir le total
				$row['prix'] = round($row['prix']);
				$row['t_prix'] = round($row['t_prix']);
				$row['illimite'] = $illimite;
				if ($coupon['id'] && ($coupon['classe'] || $coupon['locapass'])) 
					$row['coupon'] = $coupon['id'];
				$row['forfait_type'] = $coupon['locapass'] ? 'LOCAPASS' : $reservation['forfait_type'];
				// ajouter � la liste des forfaits calcul�s
				$f = new Forfait($row);
				$f->setAgence($reservation->getAgence());
				$forfaits[$row['categorie']][] = $f;
			} // !for : cumul des forfaits
		} // !whle : parcours des forfaits
		// trier les forfaits
		$x = new Forfait_Collection();
		if (is_array($forfaits))
		{
			foreach ($forfaits as $k => $a)
			{
				usort($forfaits[$k], array('Forfait_Collection','compare'));
				$x->_items[$k] = $forfaits[$k][0];
			}
		}
		// les forfaits
		$x->_forfaits = $forfaits;
		// le kilom�trage
		$kilometrages = array_keys($kilometrages);
		sort($kilometrages);
		$cnt = count($kilometrages);
		if ($cnt > 1)
			$kilometrages[] = $kilometrages[0] + $x->_kilometrages[$cnt-1];
		foreach($kilometrages as $v)
			$x->_kilometrages[$v] = $v.' km';
		if ($hasIllimite)
			$x->_kilometrages['illimit�'] = 'illimit�';
		$x->_tarifScope = $tarifScope;
		$x->_promoRestrictions = $promoRestrictions;
		return $x;
	}
	// trier les forfaits
	static public function compare (Forfait $a, Forfait $b)
	{
		// m�moriser le moins cher
		// ou, � prix �gal, le plus court
		if (
			($a['illimite'] && is_null($a['km']) && !is_null($b['km']))
			||
			(
				(!$a['illimite'] || is_null($a['km']) || !is_null($b['km']))
				&&
				(
					($a["t_prix"] < $b["t_prix"])
					|| 
					($a["t_prix"] == $b["t_prix"] && $a["jours"] < $b["jours"])
					||
					($a["t_prix"] == $b["t_prix"] && $a["jours"] == $b["jours"] && $a['km']>$b['km'])
				)
			)
		)
		{
			return -1;
		}
		return 1;
	}
	
	/**
	* Applique un Yield sur le meilleure forfait de la cat�gorie
	* On pr�serve la liste des forfaits pour comprendre la construction du tarif
	* 
	* @param AgencePromoYield $yield - le champ 'categorie' doit avoir �t� d�finie
	* @returns Forfait_Collection
	*/
	public function applyYield(AgencePromoYield $yield)
	{
		// on v�rifie d'abord que le Yield peut s'appliquer s'il n'y a pas de restrictions
		if (!is_null($this->_promoRestrictions)) {
			if (in_array($this->_promoRestrictions, array(self::PROMO_NATIONAL, self::PROMO_NONE, self::PROMO_COUPON))) {
				return $this;
			}
		}
		// v�rifier la cat�gorie
		if ($catID = $yield['cat_id']) {
			// r�cup�rer le forfait
			if (/** @var Forfait */ $f = $this->_items[$catID]) {
				// appliquer le Yield...
				$f = $f->applyYield($yield);
				// ... et mettre � jour le forfait
				$this->_items[$catID] = $f;
				array_unshift($this->_forfaits[$catID], $f);
			}
		}
		return $this;
	}
	
	/**
	* Affiche les forfaits pour DEBUG
	* 
	*/
	public function dump()
	{
		if ($this->isEmpty()) return;
		// il faut �tre connect� sur l'administration
		if (isset($_COOKIE['ADA001_ADMIN_AUTH']))
		{
			if ($adminUser = Acces::createFromCookie($_COOKIE['ADA001_ADMIN_AUTH']))
			{
				// en production ce n'est possible que depuis RnD et ADA, (et Gilles Franckhauser...)
				if (SITE_MODE == 'PROD' && !IS_ADA && !IS_RND)
					return;
				// r�cup�rer la liste des mn�moniques
				$rs = sqlexec('select id, mnem from categorie where id in ('.join(',', array_keys($this->_forfaits)).') order by zone, type, position, mnem');
				while ($row = mysql_fetch_assoc($rs)) {
					$list[$row['id']] = $row['mnem'];
				}
				// liste des chmaps
				$fields = array ('id','jours','jours_max','km','km_jour','prix_origine','prix','prix_jour','km_sup','t_prix','p_montant','p_fin','p_id','p_yield_desc','n_id','n_promotion','n_prix');
				echo '<table border="1" cellpadding="2" cellspacing="0" class="table-debug">'."\n";
				echo '<caption>';
					echo 'Tarifs '.$this->_tarifScope;
					if ($this->_promoRestrictions) {
						echo ' - '.self::$PROMO_MODES_DESC[$this->_promoRestrictions];
					}
				echo '</caption>'."\n";
				echo '<tr><th>'.join('</th><th>', $fields).'</th></tr>'."\n";
				foreach ($list as $k => $mnem)
				{
					$a = $this->_forfaits[$k];
					echo '<tr><th colspan="'.count($fields).'">Cat�gorie: '.$k.' - '.$mnem.'</th></tr>';
					foreach ($a as /** @var Forfait */ $f) {
						$values = $f->toArray($fields);
						if ($values['p_montant']) {
							$values['p_montant'] = sprintf('%+.2f', $values['p_montant']);
						}
						echo '<tr><td>'.join('</td><td>', $values)."</td></tr>\n";
					}
				}
				echo '</table>';
			}
		}
	}
	/**
	* Renvoie le tableau des kilom�tres
	* @returns array
	*/
	public function getKilometrages()
	{
		return $this->_kilometrages;
	}
	/**
	* Renvoie le p�rim�tre tairifaire
	* @returns string
	*/
	public function getTarifScope() {
		return $this->_tarifScope;
	}
	/**
	* Renvoie les restrictions de promotions d�finies
	* @returns int
	*/
	public function getPromoRestrictions() {
		return $this->_promoRestrictions;
	}
}
?>
