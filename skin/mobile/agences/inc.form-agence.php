<?php
/* Inclus depuis agences/liste et agences/index */
	/** @var Page_Agences */
	$that = $this;

	// recherche m�moris�e avec priorit� au GET, et le valider en passant par la r�servation
	$search = Reservation::createSearchFromCookieAndGet();
	// contr�le sp�cifique � l'agence
	if (!$search['type'] || ($search['type'] && $agence && !$agence->hasVehiculeType($search['type'])))
		$search['type'] = 'vp';
	// forcer les informations sur l'agence
	if (!is_null($agence)) {
		// contr�le sp�cifique � l'agence
		list($search['agence'], $search['zone']) = array($agence['id'], $agence['zone']);
	} else {
		// si la recherche est g�olocalis�e, on choisit la premi�re agence
		if (!is_null($agences) && is_object($agences) && $agences->showCenter()) {
			$ids = $agences->getIds();
			$search['agence'] = reset($ids);
		}
		// si une agence est indiqu�e et qu'elle est dans la liste on la s�lectionne
		if ($search['agence']) {
			if (!is_null($agences) && isset($agences[$search['agence']])) {
				$agence = $agences[$search['agence']];
				$search['zone'] = $agence['zone'];
				$agence = null;
			} else {
				$search['zone'] = 'fr';
				$search['agence'] = '';
			}
		}
	}
?>
<!-- formulaire de r�servation -->
<form method="post" action="<?=Page::getURL('reservation/vehicule.html')?>" name="form_res_agence" id="form_res_agence">
	<input type="hidden" name="age" value="<?=key($AGE);?>"/>
	<input type="hidden" name="permis" value="<?=key($PERMIS);?>"/>
	<div class="inner--form-res-agences">
		<? if (!is_null($agence) && $agence['zone']!='fr' && $agence['zone']!='co') : /* que des VP ailleurs qu'en FR */ ?>
			<input type="hidden" name="type" id="type" value="vp"/>
		<? else : ?>
		<div id="familles" class="row">
			<? foreach(Categorie::$TYPE as $k => $v) : ?>
			<? if ((!is_null($agence) && !$agence->hasVehiculeType($k)) || (is_null($agence) && $k == 'sp')) continue; /* afficher soit les types de l'agence, soit toos les types sauf sans permis */?>
			<div class="col-xs-6">
				<div class="form-group">
					<input type="radio" name="type" id="radio_<?=$k?>" value="<?=$k?>" <? if ($k=='vp') echo ' checked="checked"';?>/>
					<label for="radio_<?=$k?>"><?=$v?></label>	
				</div>
			</div>
			<? endforeach; ?>
		</div>
		<? endif; ?>
		<? if (is_null($agences)) : ?>
		<input type="hidden" id="agence" name="agence" value="<?=$agence['id']?>"/>
		<? else : ?>
		<div class="form-group">
			<label for="agence">Choisissez votre agence :</label>
			<? selectarray('agence', $agences, $search['agence'], '-- Choisissez une agence --'); ?>
		</div>
		<? endif; ?>
		<div id="calendar">
			<div class="row">
				<div class="form-group col-xs-6">
					<label for="depart">D�part :</label>
					<input type="text" name="depart" id="depart" value="<?=$search['depart'];?>" />
					<!--<img class="btn_cal" src="../img/calendar2.gif" alt="date de d�part" id="btn_depart" />-->
				</div>
				<div class="form-group col-xs-6">
					<span id="span_h_depart" <?=($search['type']=='sp' ? 'style="display:none;"' : '');?>>
						<label class="duree_a" for="h_depart">�</label>
						<select name="h_depart" id="h_depart"><option value="">--</option></select>
					</span>	
				</div>
			</div>
			<div class="row">
				<div class="form-group col-xs-6">
					<label for="retour">Retour :</label>
					<span id="span_ml_retour"<? if ($type!='ml') echo ' style="display: none; "';?>>le m�me jour</span>
					<input type="text" name="retour" id="retour" value="<?=$search['retour']?>" />
					<!--<img class="btn_cal" src="../img/calendar2.gif" alt="date du retour" id="btn_retour" />-->
				</div>
				<div class="form-group col-xs-6">
					<span id="span_h_retour" <?=($search['type']=='sp' ? 'style="display:none;"' : '');?>>
						<label class="duree_a" for="h_retour">�</label>
						<select name="h_retour" id="h_retour"><option value="">--</option></select>
					</span>
				</div>
			</div>
			<p id="div_sp_info" style="<?=($search['type']!='sp' ? 'display:none;' : 'clear:both;');?>" class="form-note">
				Vous pouvez r�server un v�hicule sans permis pour une p�riode de 7 jours � un mois. Au del� d'un mois, le renouvellement de la location se fait directement en agence.
			</p>
		</div>
		<div class="container-inline">
			<div class="text-center">
				<input type="submit" name="valider" value="R�server dans cette agence" class="btn btn-default btn-submit arrow smaller" />
			</div>
		</div>
		<br />
	</div>
</form>
<script type="text/javascript">
// <![CDATA[
	// formulaire de r�servation
	var frmReservation = null;
	$(document).ready(function()
	{
		// initialiser le formulaire de r�servation
		frmReservation = new formReservation(document.getElementById("form_res_agence"), <?=json_encode($search);?>, {"notification_date": false});
		$("#familles input:radio").click(function() {
			frmReservation.doType(this.value);
		});
		$("#agence").change(function() {
			frmReservation.doAgence(this.value);
		});

		// options sp�cifiques du datepicker
		var datepickerOptions = {
			showAnim: 'slide',
			numberOfMonths: 1,
			changeMonth: true,
			changeYear: true,
			showOptions: {direction: 'left', duration: 1000, easing: 'easeInOutQuad'},
			duration: 500
		};
		$('#depart').datepicker('option', datepickerOptions);
		$('#retour').datepicker('option', datepickerOptions);
		$("#h_depart").change(function() {
			$("#retour").focus();
		});
	});
// ]]>
</script>
