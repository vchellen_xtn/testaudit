<?
// $Id: lib.sql.php,v 1.20 2015/01/07 08:54:30 gdubourguet Exp $
// ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
//	Realisation RnD (c)2003 
//  Reproduction interdite sans autorisation
//  support@rnd.fr
//  -------------------------------------------------------------------------------------
//	Fonctions d'interface avec MYSQL
// ......................................................................................

	define ("SQL_USER_R", SQL_USER); 			// si non d�fini
	define ("SQL_PASSWORD_R", SQL_PASSWORD);	// si non d�fini
	define ("SQL_USER_W", SQL_USER);			// si non d�fini
	define ("SQL_PASSWORD_W", SQL_PASSWORD);	// si non d�fini

	define ("SQL_DEFAULT_DB", SQL_DB.';'.SQL_HOST_R.';'.SQL_HOST_W);
	define ("SQL_PROFILE", false);
	define ("DEBUG", 0);
	define ("DB_ENGINE", "mysql"); // par d�faut
	
	global $cn_r, $cn_w;
	global $cn_registers, $cn_current;
	global $sql_connect, $sql_select_db, $sql_close, $sql_query, $sql_error, $sql_data_seek, $sql_field_name, $sql_field_table, $sql_num_rows, $sql_num_fields, $sql_fetch_array, $sql_fetch_row, $sql_fetch_assoc, $sql_free_result, $sql_affected_rows, $sql_insert_id;
	global $sql_logger;

	if (DB_ENGINE == "mssql")
	{
		$sql_connect = 'mssql_connect';
		$sql_select_db = 'mssql_select_db';
		$sql_close = 'mssql_close';
		$sql_query = 'mssql_query';
		$sql_error = 'mssql_get_last_message';
		$sql_data_seek = 'mssql_data_seek';
		$sql_field_name = 'mssql_field_name';
		$sql_num_rows = 'mssql_num_rows';
		$sql_num_fields = 'mssql_num_fields';
		$sql_fetch_array = 'mssql_fetch_array';
		$sql_fetch_row = 'mssql_fetch_row';
		$sql_fetch_assoc = 'mssql_fetch_assoc';
		$sql_free_result = 'mssql_free_result';
		$sql_affected_rows = 'mssql_affected_rows';
		$sql_insert_id = 'mssql_insert_id';
	}
	else
	{
		$sql_connect = 'mysql_connect';
		$sql_select_db = 'mysql_select_db';
		$sql_close = 'mysql_close';
		$sql_query = 'mysql_query';
		$sql_error = 'mysql_error';
		$sql_data_seek = 'mysql_data_seek';
		$sql_field_name = 'mysql_field_name';
		$sql_field_table = 'mysql_field_table';
		$sql_num_rows = 'mysql_num_rows';
		$sql_num_fields = 'mysql_num_fields';
		$sql_fetch_array = 'mysql_fetch_array';
		$sql_fetch_row = 'mysql_fetch_row';
		$sql_fetch_assoc = 'mysql_fetch_assoc';
		$sql_free_result = 'mysql_free_result';
		$sql_affected_rows = 'mysql_affected_rows';
		$sql_insert_id = 'mysql_insert_id';
	}

	// register_db_connection
	function register_db_connection ($db, $host_r, $user_r, $pwd_r, $host_w=null, $user_w=null, $pwd_w=null)
	{
		global $cn_registers;
		if (!$cn_registers) 
			$cn_registers = array();
		if (!$host_w) 
			$host_w = $host_r;
		if (!$user_w) 
			$user_w = $user_r;
		if (!$pwd_w) 
			$pwd_w = $pwd_r;
		$key = $db.';'.$host_r.';'.$host_w;
		if (!$cn_registers[$key]) 
			$cn_registers[$key] = array (connect ($host_r, $user_r, $pwd_r, $db), connect ($host_w, $user_w, $pwd_w, $db));
	}

	// switch_to_db
	function switch_to_db ($db=null)
	{
		global $cn_r, $cn_w, $cn_registers, $cn_current;
		if (!$db)
			$db = SQL_DEFAULT_DB;
		if ($db != $cn_current)
		{
			if (!$cn_registers || (!$cn_registers[$db] && $db == SQL_DEFAULT_DB))
				register_db_connection(SQL_DB, SQL_HOST_R, SQL_USER_R, SQL_PASSWORD_R, SQL_HOST_W, SQL_USER_W, SQL_PASSWORD_W);
			list ($cn_r, $cn_w) = $cn_registers[$db];
			$cn_current = $db;
		}
	}

	// connect
	function connect($host, $user=null, $password=null, $db=null)
	{
		if ($user === null)
			$user = defined('SQL_SUPER_USER') ? SQL_SUPER_USER : 
				(defined('SQL_USER') ? SQL_USER : die("SQL_USER par d�faut n'est pas renseign�"));

		if ($password === null)
			$password = defined('SQL_SUPER_PASSWORD') ? SQL_SUPER_PASSWORD : 
				(defined('SQL_PASSWORD') ? SQL_PASSWORD : die("SQL_PASSWORD par d�faut n'est pas renseign�"));

		if ($db == null)
			$db = defined('SQL_DB') ? SQL_DB : die("SQL_DB par d�faut n'est pas renseign�");

		global $sql_connect, $sql_select_db;
		if (!($cn = $sql_connect($host, $user, $password))) 
		   die ("Impossible d'�tablir de connexion � ".$host);

		if (DB_ENGINE == 'mssql')
		{
			mssql_query("SET ANSI_NULLS ON", $cn);
			mssql_query("SET ANSI_WARNINGS ON", $cn);
		}
		
		if (!$sql_select_db($db, $cn))
			die("La base de donn�es ".$db." n'existe pas sur le serveur ".$host);
		return $cn;
	}
	
	// disconnect
	function disconnect()
	{
		global $cn_r, $cn_w;

		@$sql_close($cn_r);
		unset($cn_r);

		@$sql_close($cn_w);
		unset($cn_w);
	}
	
	// escape_string
	function escape_string($str)
	{
		if (DB_ENGINE == "mysql")
		{
			global $cn_current, $cn_r;
			if (!$cn_current)
				switch_to_db(SQL_DEFAULT_DB);
			if (!$cn_r)
				die("Impossible d'�tablir une connexion � la base de donn�es");
			return mysql_real_escape_string($str, $cn_r);
		}
		return $str;
	}
	
	// sqlexec
	function sqlexec($sql, $silent=false)
	{
		global $cn_r, $cn_w, $cn_current, $statements, $sql_query, $sql_error, $sql_logger;
		
		if (DEBUG)
			echo("<div style='font-size:9px; font-family: Tahoma; color:black'>MySQL [".$sql."]</div>\n");
		if (!$cn_current)
			switch_to_db(SQL_DEFAULT_DB);
		if (!($cn = (preg_match("/^select/i", $sql) ? $cn_r : $cn_w)))
			return null;
		if (SQL_PROFILE)
			$x = array('start'=>microtime(true), 'sql' => $sql);
		if (($res = $sql_query($sql, $cn)) == null) 
		{
			if ($silent)
				return FALSE;
			if ($_SERVER["SERVER_NAME"] == 'localhost' || preg_match('/'.DEBUG_REMOTE_ADDR.'/i', $_SERVER['REMOTE_ADDR']) || preg_match('/'.DEBUG_REMOTE_HOST.'/i', $_SERVER['REMOTE_HOST']))
				echo '<pre>'.$sql.'</pre>';
			$error = $sql_error($cn);
			if ( defined('SQL_ADMIN_EMAIL') ) {
				$txt = date('Y-m-d H:i:s')."\n";
				$txt.= ($_SERVER['HTTPS']=='on' ? 'https' : 'http') .'://'.$_SERVER['SERVER_NAME'].($_SERVER['HTTP_X_REWRITE_URL'] ? $_SERVER['HTTP_X_REWRITE_URL'] : $_SERVER['REQUEST_URI'])."\n";
				$txt.= "\n----------------------\n";
				foreach ($_SERVER as $key => $val)
					$txt.= "$key=[$val]\n";
				$txt.= "\n----------------------\n";
				$txt.= $error."\n";
				$txt.= "\n----------------------\n";
				$txt.= $sql."\n";
				$txt.= "\n----------------------\n";
				ob_start();
				debug_print_backtrace();
				$txt .= ob_get_contents();
				ob_end_clean();
				@send_mail($txt, SQL_ADMIN_EMAIL, '['.PROJECT_ID.'] SQL Error '.$_SERVER['SERVER_NAME'], EMAIL_FROM, EMAIL_FROM_ADDR, '', '');
			}
			die(ini_get('display_errors') ? $error : '');
		}

		$statements[] = $sql."; -- ".$_SERVER['SCRIPT_NAME'].' ['.date("Y-m-d H:i:s").']';
		if (SQL_PROFILE)
		{
			$x['end'] = microtime(true);
			$sql_logger[] = $x;
		}
		return $res;
	}
	
	// getsql
	function getsql($sql)
	{
		global $sql_num_rows, $sql_fetch_row, $sql_free_result;
		$rs = sqlexec($sql);

		if (!is_resource($rs) || $sql_num_rows($rs) == 0)
			return null;

		$row = $sql_fetch_row($rs);
		$cnt = ((count($row) == 1) ? $row[0] : $row);
		$sql_free_result($rs);
		return ($cnt);
	}

	// sqlentities
	function sqlentities($sql, $prefix="")
	{
		return is_array($sql) ? implode (',', $sql) : $sql;
		if ($prefix)
			$prefix = '`'.str_replace('`', '', $prefix).'`.';
		if (is_array($sql))
			$sql = implode(",", $sql);
		return preg_replace("/([a-z0-9_]+)(?<!as|case|when|then|else|end)(?![\('a-z0-9_])/i", "$prefix`$1`", str_replace('`', '', $sql));
	}
	
	// rowsaffected
	function rowsaffected()
	{
		global $cn_r, $cn_w, $sql_affected_rows;
		return (($cn_r || $cn_w) ? $sql_affected_rows() : 0);
	}
	
	// parse_query
	function parse_query($sql)
	{
		$sql = strtolower($sql);
	 	preg_match("/select\s+(.*)\s+from\s+(.+)\s*(?:where\s+(.+))*(?:group by(.+))*(?:order by(.+))*$/Ui", $sql, $a);

		// la liste des colonnes 
		// x 				x => x
		// x.y				y => x.y
		// x.y as z 		z => x.y as z
		// foo(y,a) as z	z => foo(y, a)
		$a[1] = preg_replace('/,(?![^\)\(]+\))/', ';', $a[1]);
		foreach (explode(";", $a[1]) as $coldef)
		{
			$b = explode(' as ', trim(strtolower($coldef)));
			$cols[preg_replace('/[^\s\.]\./', '', $b[count($b)-1])] = trim($b[0]);
		}

		// traitement du from de la forme [table alias,]+
		foreach (explode(",", $a[2]) as $tbl)
		{
			$tbl = trim($tbl);
			if (preg_match("/([^,\s]+)/", $tbl, $b))
				$from[$b[1]] = $tbl;
		}

		return array(
			 "cols"  => $cols
			,"from"  => $from
			,"where" => trim($a[3])
			,"group" => trim($a[4])
			,"order" => trim($a[5])
			);
	}
	
	// get_columns_from_schema
	function get_columns_from_schema($tbl)
	{
		global $sql_fetch_assoc;
		if (DB_ENGINE == "mssql")
		   $sql =  "select c.name as [Field], "
				  ." 	   t.name + case when t.name like '%char' then '(' + convert(varchar, c.length) + ')' else '' end as [Type], "
				  ." 	   case when isnullable = 1 then 'Yes' else 'No' end as [Nullable], "
				  ."       case when columnproperty(c.id,c.name,'IsIdentity') = 1 then 'Auto_Increment' else null end as [Extra], "
				  ."	   case when c.name = index_col('".$tbl."', indid, colid) then 'PRI' else '' end as 'Key' "
			  	  ."  from syscolumns c "
				  ."  join systypes t on c.xtype = t.xusertype "
				  ."  left join sysindexes i on i.id = c.id and (i.status & 0x800) = 0x800 "
				  ." where c.id = object_id('".$tbl."')";
		else
			$sql = "show columns from `".$tbl."`";
		for($cols=array(), $rs=sqlexec($sql); $row=$sql_fetch_assoc($rs);)
			$cols[] = $row;
		return $cols;
 	}

	// quotename
	function quotename($s)
	{
		return (DB_ENGINE == "mssql") ? "[".$s."]" : "`".$s."`";
	}

	// mssql_insert_id
	function mssql_insert_id()
	{
		return getsql('select @@identity');
	}

	// sql_profile
	function sqlprofile()
	{
		global $sql_logger;
		if (!$sql_logger || !is_array($sql_logger)) return;
		echo "\n".'<table cellpadding="2" cellspacing="0" border="1">'."\n";
		echo "<tr><th>N</th><th>Temps (ms)</th><th>SQL</th></tr>\n";
		foreach ($sql_logger as $k => $v)
		{
			$v['time'] = round(1000*($v['end']-$v['start']), 2);
			$total += $v['time'];
			echo '<tr><td valign="top">'.(1+$k).'</td><td valign="top" align="right">'.$v['time'].'</td><td><pre>'.$v['sql']."</pre></td></tr>\n";
		}
		echo '<tr><td></td><td align="right"><strong>'.$total.'</strong></td><td></td></tr>'."\n";
		echo "\n</table>\n";
	}
?>