<?
/** @var Reservation */ $reservation = $this;
?>
	<table cellspacing="0" cellpadding="0" border="0" class="table-reservation">
		<tr>
			<td colspan="5" class="titre-resa">
				<div class="num-resa">
					<?=$reservation['show_counter']?>
				</div>
				<p>
					R�servation n�<?=$reservation->getResIDClient().' - <span class="gris">'.$reservation->getStatut().'</span>'?>
				<br />
					<span class="rouge">D�part: <?=$reservation->getDebut('d/m/Y H:i')?></span>
				<?  if ($reservation['show_client']) : ?>
				<br/>
					<span class="rouge"><?=$reservation['show_client']; ?></span>
				<? endif; ?>
				</p>
			</td>
		</tr>
		<tr>
			<th class="vehicule">
				Votre v�hicule
			</th>
			<th class="width-agence">
				Agence ADA
			</th>
			<th class="width-duree">
				Dur�e
			</th>
			<th class="width-km">
				Km inclus
			</th>
			<th class="tarifs">
				Tarifs TTC
			</th>
		</tr>
		<tr>
			<td class="vehicule">
				<strong> <?=$reservation['categorie_nom']?></strong><br />
				<?=$reservation['categorie_description']?>
			</td>
			<td class="width-agence">
				<strong><?=$reservation['agence_nom']?></strong>
			</td>
			<td class="width-duree">
				<strong><?=$reservation->getDuree()?></strong>
			</td>
			<td class="width-km">
				<strong><?if ($reservation['forfait_type']!='LOCAPASS') echo (is_null($reservation['km']) ? 'km illimit�' : $reservation['km'].' km')?></strong>
			</td>
			<td class="tarifs">
				<strong><?if ($reservation['forfait_type']!='LOCAPASS') echo $reservation['total'].' &euro;'?></strong>
			</td>
		</tr>
		<tr>
			<td colspan="5" class="end-resa">	
			</td>
		</tr>
	</table>
	<div class="bloc-print">
		<div class="top"></div>
			<ul>
				<?
					foreach($reservation->getLinks() as $link)
						echo '<li><a href="'.$link['href'].'">'.$link['title'].'</a></li>';
				?>
			</ul>
	</div>
