<?php
  /*** @var Agence */ $agence = $this;
  global $JOURS;
  // affiche les horaires
?>
	<table class="agence--table-horaires" cellpadding="0" cellspacing="0" >
	<? foreach($agence->getHoraires() as $h) : ?>
		<tr>
			<th valign="top"><?=$JOURS[$h['jour']];?></th>
			<td>
				<div class="fake-table">
				<? if (!$h['ouverture'] || !$h['fermeture']) : ?>
					<span class="align-center closed">ferm&eacute;</span>
				<? elseif (!$h['pause_debut'] || !$h['pause_fin'] || $h['pause_debut']==$h['pause_fin']) : ?>
					<span>
					<? if ($h['ouverture']=='00:00' && $h['fermeture'] >= '23:30') : ?>
						24h/24
					<? else : ?>
						<?=sprintf('%s - %s', $h['ouverture'], $h['fermeture']);?>
					<?endif; ?>
					</span>
					<? else : ?>
						<span><?=sprintf('%s - %s', $h['ouverture'], $h['pause_debut']);?></span>
						<span><?=sprintf('%s - %s', $h['pause_fin'], $h['fermeture']);?></span>
					<? endif; ?>
				</div>
			</td>
		</tr>
	<? endforeach; ?>
	</table>

