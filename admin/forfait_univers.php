<?
	require_once("secure.php"); 
	require_once("lib.php");
	if (!$adminUser->hasRights(Acces::SB_ADMIN))
		die("Vous n'avez pas les droits pour �diter cet �l�ment");

	// identifiant
	$id = (int) getarg("id");
	
	// suppression
	if ($id && getarg("delete")) 
	{
		$sql = "delete from forfait2_univers where id = '".addslashes($id)."'";
		getsql($sql);
		unset($id);
	}
	
	// modification, cr�ation
	if (getarg("update") || getarg("create"))
	{
		if ($_POST["depart_fin"] < $_POST["depart_debut"])
			$_POST["depart_fin"] += 7;
		if ($_POST["retour_fin"] < $_POST["retour_debut"])
			$_POST["retour_fin"] += 7;
		save_record('forfait2_univers', $_POST);
		if (!$id) $id = mysql_insert_id();
	}
	
	if ($id)
	{
		$rs = sqlexec('select * from forfait2_univers where id = "'.addslashes($id).'"');
		if (!$rs || !($row = mysql_fetch_assoc($rs)))
			die("Aucun enregistrement correspondant ($id).");
	}

	for ($h=0; $h<=23; $h++)
		$HOURS[$h] = sprintf("%02d:00", $h);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
		<script type="text/javascript" src="<?=Page::getProtocol()?>://www.google.com/jsapi"></script>
	<script type="text/javascript">
		google.load('jquery', '1.4');
	</script>
	<script type="text/javascript" src="../scripts/script.js"></script>
	<script type="text/javascript" src="admin.js"></script>
	<script>
	<!-- //
	// doDelete
	function doDelete(frm)
	{
		return (confirm("Voulez-vous r�ellement supprimer cet �l�ment ?\nCette op�ration est irr�versible."));
	}
	
	// date_from_control
	function date_from_control(ctrl_j, ctrl_h)
	{
		var j = ctrl_j.selectedIndex >= 0 ? ctrl_j.options[ctrl_j.selectedIndex].value : null,
			h = ctrl_h.selectedIndex >= 0 ? ctrl_h.options[ctrl_h.selectedIndex].value : null;
			v = parseInt(j) + parseInt(h) / 100;
		return isNaN(v) ? "" : v;
	}
	
	// doCheck
	function doSubmit(frm)
	{
		var err = "";
		if(!frm.nom.value.length)
			err+= "- veuillez indiquer le nom\n";
		if(!frm.jours_min.value.match(/^\d+$/))
			err+= "- veuillez indiquer le nombre de jours minimum\n";
		if(!frm.jours_max.value.match(/^\d+$/))
			err+= "- veuillez indiquer le nombre de jours maximum\n";
		// format condens� pour les bornes
		frm.depart_debut.value = date_from_control(frm.depart_debut_j, frm.depart_debut_h);
		frm.depart_fin.value = date_from_control(frm.depart_fin_j, frm.depart_fin_h);
		frm.retour_debut.value = date_from_control(frm.retour_debut_j, frm.retour_debut_h);
		frm.retour_fin.value = date_from_control(frm.retour_fin_j, frm.retour_fin_h);
		if (!frm.depart_debut.value || !frm.depart_fin.value || !frm.retour_debut.value || !frm.retour_fin.value)
			err += "- veuillez pr�ciser les contraintes sur le d�part et le retour\n";

		// les date de d�part et de retour doivent �tre indiqu�es
		

		// affiche le message d'erreur
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		return true;
	}
	$(document).ready(function()
	{
		$("#univers, #jours_min, #jours_max").change(function()
		{
			if (this.form.forfait)
				updateSelect(this.form.forfait);
		});
		// on met � jour au chargement
		var frm = document.forms[0];
		if (frm.forfait)
			updateSelect(frm.forfait, "<?=filter_var($_GET['forfait'], FILTER_SANITIZE_STRING)?>");
	});
	// -->
	</script>
</head>
<body>

<form action="" method="post" onsubmit="return doSubmit(this);">
<input type="hidden" name="depart_debut" value="<?=$row['depart_debut']?>">
<input type="hidden" name="depart_fin" value="<?=$row['depart_fin']?>">
<input type="hidden" name="retour_debut" value="<?=$row['retour_debut']?>">
<input type="hidden" name="retour_fin" value="<?=$row['retour_fin']?>">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre" colspan="2">
		<img src="img/puce.gif" align="absmiddle" hspace="1">
		Forfait Contraintes
	</th>
</tr>
<tr>
	<td colspan="2">
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="15%">
		<col width="35%">
		<col width="15%">
		<col width="35%">
		<tr>
			<td>ID</td>
			<td>
				<? if ($id) echo $id; ?>
				<input type="hidden" name="id" value="<?=$id?>">
			</td>
			<td><label for="univers">Univers</label></td>
			<td>
			<? if ($id) :?>
				<?=$row['univers']?><input type="hidden" name="univers" value="<?=$row['univers']?>"/>
			<? else : ?>
				<? selectarray('univers', ForfaitUnivers::$UNIVERS, null)?>
			<? endif; ?>
			</td>
		</tr>
		<tr>
			<td><label for="forfait">Forfait</label></td>
			<td colspan="3">
			<? if ($id) : ?>
				<?=getsql("select concat(id, ' ', nom) from forfait2 where id='".addslashes($row['forfait'])."'");?>
			<? else: ?>
				<? selectarray('forfait', array(), null); ?>
			<? endif; ?>
			</td>
		</tr>
		<tr>
			<td><label for="nom">Nom</label></td>
			<td colspan="3">
				<input type="text" id="nom" name="nom" value="<?=stripslashes($row['nom'])?>" maxlength="128" size="64"/></td>
			</td>
		</tr>
		<tr><th colspan="4">Contraintes calendaires</th></tr>
		<tr>
			<td><label for="jours_min">Dur�e : entre</label></td>
			<td colspan="3">
				<input type="text" class="l" id="jours_min" name="jours_min" value="<?=$row['jours_min']?>" maxlenth="2" size="3">
				<label for="jours_max">&nbsp;et&nbsp;</label>
				<input type="text" class="l" id="jours_max" name="jours_max" value="<?=$row['jours_max']?>" maxlength="2" size="3">&nbsp;jours
			</td>
		</tr>
		<tr>
			<td>D�part entre</td>
			<td><? day_time_control($row, 'depart_debut'); ?></td>
			<td>et</td>
			<td><? day_time_control($row, 'depart_fin'); ?></td>
		</tr>
		<tr>
			<td>Retour entre</td>
			<td><? day_time_control($row, "retour_debut"); ?></td>
			<td>et</td>
			<td><? day_time_control($row, "retour_fin"); ?></td>
		</tr>
		<tr><th colspan="4">Editorial</th></tr>
		<tr>
			<td><label for="description">Description</label></td>
			<td colspan="3"><textarea id="description" name="description" rows="2" cols="64"><?=htmlspecialchars($row['description']);?></textarea></td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td align="left">
	</td>
	<td align="right">
		<input type="submit" name="<?=$id ? "update" : "create"?>" value="enregistrer">
<?
	if ($id) 
	{
?>
		&nbsp;
		<input type="submit" onclick="return doDelete(this.form);" name="delete" value="supprimer">
<?
	}
?>
	</td>
</tr>
</table>
</form>

</body>
</html>
<?
	// day_time_control
	function day_time_control($row, $a)
	{
		global $JOURS, $HOURS;
		$x = $row[$a];
		if ($x >= 8)
			$x -= 7;
		selectarray($a."_j", $JOURS, ($x != null) ? floor($x): null, '--', " style='width:60%'");
		selectarray($a."_h", $HOURS, ($x != null) ? round(100 * ($x - floor($x))) : null, '--', " style='width:37%'");
	}
?>