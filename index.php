<?
	session_start();
	include ('constant.php');

	// choisir le skin entre www.ada.fr et mobile.ada.fr
	$default_page = 'reservation/index';
	$skin = 'v3';
	if (SERVER_NAME == SERVER_MOBI && SERVER_MOBI!=SERVER_RESA) {
		$skin = 'mobile';
	} else if (SERVER_NAME!=SERVER_RESA && preg_match('/([^\.]+)\.ada(\.lbn)?\.fr$/', SERVER_NAME, $match)) {
		$zones = array('gp'=>'guadeloupe');
		if (($zone = array_search($match[1], $zones)) || (isset($zones[$match[1]]) && ($zone = $match[1])))
		{
			// on n'affiche que la page d'accueil
			if ($_GET['page'])
				Page::redirect(Page::getURL(null, SERVER_NAME));
			$default_page = 'zone/'.$zone;
		}
	}
	else if (SITE_MODE != 'PROD')
	{
		if (SITE_MODE=='DEV' && strstr($_SERVER['HTTP_USER_AGENT'], 'iPhone'))
			$skin = 'mobile';
		if ($_GET['skin'])
			$skin = $_GET['skin'];
		else if ($_SESSION['skin'])
			$skin = $_SESSION['skin'];
		if (in_array($skin, array('www')))
			define('SKIN_DEFAULT','www');
	}
	$_SESSION['skin'] = $skin;
	if (preg_match('/^(jeunes|resas)\//', $_GET['page'], $m))
	{
		$skin = $m[1];
		$default_page = $m[1].'/index';
	}
	// pour le skin "mobile", d�finir "v3" comme "skin" par d�faut
	if ($skin == 'mobile' && SITE_MODE != 'PROD') {
		define('SKIN_DEFAULT','v3');
	}
	

	// cr�er la page et l'afficher
	$x = Page::factory($skin, $_GET['page'], $default_page);
	if (!$x) die('Erreur interne');
	if (substr(SERVER_NAME, 0, 3) == 'ww2') {
		$x->noIndex();
	}
	$x->render($skin);

