<?
class QuestionItem_Collection extends Ada_Collection
{
	public static function factory($question)
	{
		$x = new QuestionItem_Collection();
		$sql  = 'SELECT question_item.*, question_question.type AS question_type'
				.'	FROM question_item'
				.'	JOIN question_question ON question_question.id = question_item.question'
				.'	WHERE question_item.question = '.(int)$question
				.'	ORDER BY question_item.position;';
		$x->_loadFromSQL($sql);
		return $x;
	}
	/**
	* Renvoie le HTML d'une QuestionItem_Collection
	* 
	* @return string
	*/
	public function getHTML() {
		foreach ($this->_items as $item) {
			$output.= $item->getHTML()."\n";
		}
		return $output;
	}
}
?>