<?
	require_once("secure.php"); 
	require_once("lib.php");

	// identifiant
	$id = (int) getarg("id");
	if (!$id) $id = null;
	
	// suppression
	if ($id && getarg("delete")) 
	{
		// supprimer l'image correspondante
		$image = getsql(sprintf('SELECT src FROM visuel WHERE id = %ld', $id));
		if ($image) {
			if(is_file('../'.$image)) {
				unlink('../'.$image);
			}
		}
		getsql(sprintf('delete from visuel where id = %ld', $id));
		unset($id);
	}
	
	// modification, cr�ation
	if (getarg('update') || getarg('create'))
	{
		save_record('visuel', $_POST);
		if (!$id) $id = mysql_insert_id();
		if (count($_FILES)) {
			$sql = 'update visuel set id = id';
			foreach ($_FILES as $key => $f)
			{
				if (preg_match('/\.(jpg|jpeg|gif|png|swf)$/', $f['name'])) {
					$fichier = sprintf('fichiers/visuels/visuel-%04ld%s', $id, strrchr($f['name'],'.'));
					move_uploaded_file($f['tmp_name'], '../'.$fichier);
					$sql.= " ,".$key.' = "'.$fichier.'"';
				}
			}
			$sql.= ' where id = '.(int)$id;
			getsql($sql);
		}
	}

	if ($id)
	{
		$rs = sqlexec(sprintf('select * from visuel where id = %ld', $id));
		if (!$rs || !($row = mysql_fetch_assoc($rs)))
			die("Aucun enregistrement correspondant.");
	}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script language="javascript" src="../scripts/check.js"></script>
	<script>
	<!-- //

	// doDelete
	function doDelete(frm)
	{
		return (confirm("Voulez-vous r�ellement supprimer cet �l�ment ?\nCette op�ration est irr�versible."));
	}
	
	// doSubmit
	function doSubmit(frm)
	{
		var err = "";
		if(!frm.libelle.value.length)
			err += "Veuillez indiquer le nom\n";
		if(!frm.src.value.length)
			err += "Veuillez indiquer l'url du visuel\n";
		if(frm.hauteur && frm.largeur)
			if(!frm.hauteur.value.length || !frm.largeur.value.length)
				err += "Veuillez indiquer les dimensions du visuel\n";
			else if(!frm.hauteur.value.match(/^\d*$/) || !frm.largeur.value.match(/^\d*$/))
				err += "Veuillez indiquer des dimensions valides\n";
			
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		return true;
	}
	// -->
	</script>
</head>
<body>
<form action="" method="post" onsubmit="return doSubmit(this);" enctype="multipart/form-data">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre">
	<img src="img/puce.gif" align="absmiddle" hspace="1">
<? 
	echo "Visuel";
?>
	</th>
</tr>
<tr>
	<td>
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="15%">
		<col width="32%">
		<col width="15%">
		<col width="32%">
		<tr>
			<td>ID</td>
			<td>
<?
	echo '<b>'.$id.'</b>';
	echo '<input type="hidden" name="id" value="'.$id.'">';
?>
			</td>
			<td><label for="publie">Publi�</label></td>
			<td>
				<input type="checkbox" id="publie" name="publie[]" value="1" <? if(!$id || $row['publie']) echo 'checked'; ?>>
				<input type="hidden" name="publie[]" value="0">
			</td>
			
		</tr>
		<tr>
			<td><label for="libelle">Libell�</label></td>
			<td colspan="3"><input type="text" class="l" id="libelle" name="libelle" value="<?=$row["libelle"]?>"></td>
		</tr>
		<tr>
			<td valign="top"><label for="href">Lien</label></td>
			<td colspan="3"><input type="text" class="l" name="href" id="href" value="<?=$row["href"]?>"></td>
		</tr>
		<tr>
			<td><label for="src">Visuel</label></td>
			<td colspan="3"><?=url_ctrl("src", $row["src"])?></td>
		</tr>
<?
	if ($id && substr(strrchr($row["src"],'.'),1)=='swf') 
	{
?>
		<tr>
			<td>Dimensions</td>
			<td>
				<label for="hauteur">hauteur</label>
				<input type="text" style="width: 50px;" name="hauteur" id="hauteur" value="<?=$row["hauteur"]?>">
				px
			</td>
			<td colspan="2">
				<label for="largeur">largeur</label>
				<input type="text" style="width: 50px;" name="largeur" id="largeur" value="<?=$row["largeur"]?>">
				px
			</td>
		</tr>
<?
	}
?>	
		
		
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%">
		<tr>
			<td>
<?
	if ($id) 
	{
?>
				<input type="button" value="pages" onclick="parent.list.location.href='lists/visuel_page.php?visuel=<?=$id?>'; return false;">
<?
	}
?>
			</td>
			<td align="right">
				<input type="submit" name="<?=$id ? "update" : "create"?>" value="enregistrer" onclick="return doSubmit(this.form);">
<?
	if ($id && !getsql('SELECT COUNT(*) FROM visuel_page WHERE visuel='.$id)) 
	{
?>
		&nbsp;
		<input type="submit" onclick="return doDelete(this.form);" name="delete" value="supprimer">
<?
	}
?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
</form>

</body>
</html>
