<?
	require_once("secure.php"); 
	require_once("lib.php");

	// identifiant
	$id = getarg("id");
	
	// suppression
	if ($id && getarg("delete")) 
	{
		$sql = "delete from zone_saison where id = '".$id."'";
		getsql($sql);
		$sql = "delete from forfait2_tarif where saison='".$id."'";
		getsql($sql);
		unset($id);
	}
	
	// modification, cr�ation
	if (getarg("update") || getarg("create"))
	{
		save_record('zone_saison', $_POST);
		if (!$id) $id = mysql_insert_id();
		// on recopie les tarifs par d�faut
		if (getarg("create"))
		{
			$sql = "INSERT INTO forfait2_tarif (zone, forfait, categorie, groupe, saison, prix, prix_jour, creation, modification)";
			$sql.=" SELECT zone, forfait, categorie, groupe, ".$_POST['id'].", prix, prix_jour, now(), now()";
			$sql.=" FROM forfait2_tarif";
			$sql.=" WHERE zone='".addslashes($_POST['zone'])."' AND saison IS NULL";
			getsql($sql);
		}
	}

	if ($id)
	{
		$rs = sqlexec("select * from zone_saison where id = '".$id."'");
		if (!$rs || !($row = mysql_fetch_assoc($rs)))
			die("Aucun enregistrement correspondant.");
		$zone = $row['zone'];
	}
	if (!$zone) $zone = $_GET['zone'];
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script language="javascript" src="../scripts/check.js"></script>
	<script>
	<!-- //

	// doDelete
	function doDelete(frm)
	{
		return (confirm("Voulez-vous r�ellement supprimer cet �l�ment ?\nCette op�ration est irr�versible."));
	}
	
	// doSubmit
	function doSubmit(frm)
	{
		var err = "";
		if (frm.zone && !frm.zone.value)
			err += "Veuillez choisir une zone!\n";
		if (!ValidateSelect(frm.debut_jour)) {
			err += "Veuillez choisir le jour de d�but de saison\n"
		}
		if (!ValidateSelect(frm.debut_mois)) {
			err += "Veuillez choisir le mois de d�but de saison\n"
		}
		if (!ValidateSelect(frm.fin_jour)) {
			err += "Veuillez choisir le jour de fin de saison\n"
		}
		if (!ValidateSelect(frm.fin_mois)) {
			err += "Veuillez choisir le mois de fin de saison\n"
		}
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		return true;
	}
	// -->
	</script>
</head>
<body>
<form action="" method="post" onsubmit="return doSubmit(this);">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre">
	<img src="img/puce.gif" align="absmiddle" hspace="1">
<? 
	echo "Saison";
?>
	</th>
</tr>
<tr>
	<td>
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="15%">
		<col width="15%">
		<col width="15%">
		<col width="55%">
		<tr>
			<td>Id</td>
			<td><?
				echo $id;
				echo '<input type="hidden" name="id" value="'.$id.'">';
			?></td>
			<td><label for="zone">Zone</label></td>
			<td>
			<? 
				if ($zone) {
					echo '<a href="zone.php?id='.$zone.'">'.getsql("select nom from zone where id='".$zone."'").'</a>';
					echo '<input type="hidden" name="zone" value="'.$zone.'">';
				}
				else
					selectsql ('zone', 'select id, nom from zone order by nom', null, '--');
			?>
			</td>
		</tr>
		<tr>
			<td><label for="libelle">Libelle</label></td>
			<td colspan="3"><input type="text" name="libelle" id="libelle" value="<?=$row['libelle']?>" maxlength="64"></td>
		</tr>
		<tr>
			<td><label for="debut_mois">D�but (mois)</label></td>
			<td><? selectarray('debut_mois', $MOIS, $row['debut_mois'], '--'); ?></td>
			<td><label for="debut_jour">D�but (jour)</td>
			<td>
			<?
				$jours = array();
				$m = $row['debut_mois'];
				$max = 31; // ($m == 2 ? 28 : (in_array($m, array(4,6,9,11)) ? 30 : 31) );
				for ($i = 1; $i <= $max; $i++)
					$jours[$i] = str_pad($i, 2, '0', STR_PAD_LEFT);
				selectarray('debut_jour', $jours, $row['debut_jour'], '--', 'style="width: auto;"');
			?>
			</td>
		</tr>
		<tr>
			<td><label for="fin_mois">Fin (mois)</label></td>
			<td><? selectarray('fin_mois', $MOIS, $row['fin_mois'], '--'); ?></td>
			<td><label for="fin_jour">Fin (jour)</td>
			<td>
			<?
				$jours = array();
				$m = $row['fin_mois'];
				$max = 31; // ($m == 2 ? 28 : (in_array($m, array(4,6,9,11)) ? 30 : 31) );
				for ($i = 1; $i <= $max; $i++)
					$jours[$i] = str_pad($i, 2, '0', STR_PAD_LEFT);
				selectarray('fin_jour', $jours, $row['fin_jour'], '--', 'style="width: auto;"');
			?>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td align="right">
		<input type="submit" name="<?=$id ? "update" : "create"?>" value="enregistrer">
<?
	if ($id) 
	{
?>
		&nbsp;
		<input type="submit" onclick="return doDelete(this.form);" name="delete" value="supprimer">
<?
	}
?>
	</td>
</tr>
</table>
</form>
</body>
</html>