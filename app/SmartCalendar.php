<?php
class SmartCalendar extends Ada_Collection
{
	/** @var Reservation */
	protected $_reservation = null;
	protected $_lowestPrice = null;
	protected $_nbDispo = 0;
	
	static public function factory (Reservation $reservation, $weeks = 3, ForfaitUnivers $forfaitUnivers = null)
	{
		// la date de d�but
		$oDepart = $reservation->getDebut('U');
		$oRetour = $reservation->getFin('U');
		$duree = $reservation['duree'];
		
		// se ramener au lundi depuis le jour de la semaine (0: dimanche, 1: lundi, ....: 6: samedi)
		$days = 1 - date('N', $oDepart);

		// cr�er le calendrier des r�servations
		$agence = $reservation->getAgence();
		$original = $reservation->getFields();
		
		// mode de promotion
		if ($offres = $reservation->getOffres())
			$promoRestrictions = $offres->getPromoRestrictions();
		
		
		// calculer les diff�rentes possibilit�s
		$items = array(); $allDeparts = array(); $nbDispo = 0;
		for($i=0; $i < 7*$weeks; $i++)
		{
			// on reocpie les donn�es initiales
			$a = $original;
			// on calcule les jours et horaires de d�part et de rtour
			// $days + $i = offset avec la date initiale
			$tDepart = strtotime(($days + $i)." days", $oDepart);
			$tRetour = strtotime(($days + $i).' days', $oRetour);
			
			// puis on les v�rifie en cherchant une approximation
			$tIndex = $tDepart;
			$tDepart = $agence->adjustDateTime($tDepart, 'depart', $duree);
			$tRetour = $agence->adjustDateTime($tRetour, 'retour', $duree);
			
			// si l'agence est ouverte on cr�er la r�servation
			// sinon on laisse un emplacement "vide"
			if (!$tDepart || !$tRetour || ($tDepart == $tRetour))
				$r = null;
			else
			{
				$a['depart'] = date('d-m-Y', $tDepart);
				$a['h_depart'] = date('H:i', $tDepart);
				$a['retour'] = date('d-m-Y', $tRetour);
				$a['h_retour'] = date('H:i', $tRetour);
				$r = Reservation::create($a, $msg, $url, $reservation);
				if ($r && $forfaitUnivers && !$forfaitUnivers->checkReservation($r))
					$r = null;
				if ($r && $allDeparts[$r->getDebut('U')])
					$r = null;
				// enregistrer que ce d�part est pris
				if ($r)
					$allDeparts[$r->getDebut('U')] = true;

				// pour montrer les prix les plus bas il faut les calculer au fur et � mesure
				if ($r && !$r->isLocapass() && ($offres = $r->getOffres('1')))
				{
					$offres->setPromoRestrictions($promoRestrictions);
					$o = $offres->getOffreByCategory($r['categorie']);
					if ($o && $o['nb'] > 0)
					{
						// on v�rifie l'existence d'un forfait
						$f = $o->getForfait();
						if ($f)
						{
							// on note ce jour de disponiblit�
							$nbDispo++;
							// concever le prix le plus bas
							$prix = $f->getPrix();
							if (!$lowest || ((double) $lowest > (double)$prix))
								$lowest = (double) $prix;
						}
					}
				}
			}
			// on �vite l'�crasement d'une possibiliit� �quivalente
			$items[$tIndex] = $r;
		}
		$x = new SmartCalendar();
		$x->_items = $items;
		$x->_setItemClass('Reservation');
		$x->_reservation = $reservation;
		$x->_lowestPrice = $lowest;
		$x->_nbDispo = $nbDispo;
		return $x;
	}
	public function getLowestPrice()
	{
		return $this->_lowestPrice;
	}
	/** y a t'il des disponiblit�s @returns bool*/
	public function hasDispo()
	{
		return ($this->_nbDispo > 0);
	}
	/**
	* Renvoie la TABLE html repr�sentant le SmartCalendar
	* quand le clic est vlaid�, la fonction updateForfait() est pass�
	* @param $agenceSearch $this->getTrackInfo('agence_search')
	* @returns string
	* 
	*/
	public function toHtml($agenceSearch)
	{
		global $JOURS, $MOIS;
		$jsForfaits = array();
		$dates = $this->getIds();
		
		// obtenir le prix courant et le plus bas
		$lowestPrice = $this->getLowestPrice();
		if ($offres = $this->_reservation->getOffres())
		{
			$o = $offres->getOffreByCategory($this->_reservation['categorie']);
			if ($o && ($f = $o->getForfait()))
				$currentPrice = $f->getPrix();
		}
		
		// pr�parer le caption
		$caption[] = ucwords($MOIS[date('n', $dates[0])]).' '.date('Y', $dates[0]);
		$caption[] = ucwords($MOIS[date('n', $dates[count($dates)-1])]).' '.date('Y', $dates[count($dates)-1]);
		
		$h = '<table id="offre_calendar" border="0" cellpadding="0" cellspacing="0">'."\n";
		$h.= "<caption>".($caption[0]==$caption[1] ? $caption[0] : join(' - ', $caption))."</caption>";
		$h.='<tr><th>'.join('</th><th>', $JOURS).'</th></tr>'."\n";
		$h.='<tr>';
		
		// parcourir l'ensemble des prix
		$i = 0; $day = 0; $cnt = $this->count();
		foreach ($this->_items as $k => /** @var Reservation */ $r)
		{
			$o = $f = $txt = null; $class = array();
			$day++;
			// r�cup�rer l'offre
			if ($r && $r->getOffres())
			{
				/** @var Offre */
				$o = $r->getOffres()->getOffreByCategory($r['categorie']);
			}
			// est ce disponible ?
			if (!$r || !$o || !$o['nb'])
			{
				$class[] = 'cal_indispo';
				$txt = '&nbsp;'; //'indis-<br />ponible';
			}
			else if (!$r->isLocapass())
			{
				/** @var Forfait */
				$f = $o->getForfait();
				if (date('d-m-Y',$k) == $this->_reservation['depart'])
					$class[] = 'current';
				if ($f && ($lowestPrice == (double) $f->getPrix()))
					$class[] = 'cal_lowest';
			}
			// afficher la cellule
			$h.='<td id="cal-'.$day.'" class="'.join(' ', $class).'" valign="top">';
			if ($txt || !$f)
			{
				$h.='<strong class="cal-date">'.date('d/m', $k).'</strong>';
				$h.=$txt;
			}
			else
			{
				// cr�er le forfait correspondant
				$jsForfaits[$day] = array(
					 'depart' => $r->getDebut('d-m-Y')
					,'h_depart' => $r->getDebut('H:i')
					,'j_depart' => $JOURS[$r->getDebut('N')]
					,'retour' => $r->getFin('d-m-Y')
					,'h_retour' => $r->getFin('H:i')
					,'j_retour' => $JOURS[$r->getFin('N')]
					,'prix_forfait' => $f->getPrix()
					,'forfait' => $f->doSerialize()
					// suivre pour NSP la vairation en nombre de jours et en prix
					,'sp_suivi' => sprintf('%+dj; %+dE', ($r->getDebut('U') - $this->_reservation->getDebut('U')) / (24*3600), $f->getPrix() - $currentPrice)
				);
				$h.='<label for="cal_jour_'.$day.'">';
				$h.='<strong class="cal-date">'.date('d/m', $k).'</strong>';
				$h.='<strong class="cal-prix">'.$f->getPrix().'�</strong>';
				$h.='</label>';
				$h.='<input id="cal_jour_'.$day.'" type="radio" name="cal_jour" onclick="return updateCal(this.value);" value="'.$day.'"';
				if (in_array('current', $class))
					$h.=' checked="checked"';
				$h.='/>'."\n";
			}
			$h.='</td>';
			$i++;
			if (($i % 7) == 0 && $i < $cnt)
				$h.="</tr>\n<tr>";
		}
		$h.="</tr>\n";
		$h.='</table>'."\n";
		
		// bo�te de dialogue JQUERY et JS
		// http://jqueryui.com/demos/dialog/
		ob_start();
?>
<div id="dialog-confirm-cal" title="Modifier vos dates de r�servations ?">
	<div id="dialog-confirm-cal-msg"></div>
</div>
<script type="text/javascript" language="javascript">
	// <![CDATA[
	var forfaits = Array();
<? 
	foreach($jsForfaits as $k => $f)
		echo "\t".'forfaits['.$k.'] = '.json_encode($f).';'."\n";
?>
	// mise � jour du calendrier
	function updateCal(day, validated)
	{
		var td = $('#cal-'+day);
		// si c'est le "current" on arr�te
		if (td.hasClass('current')) return true;
		// retrouver le forfait
		var f = forfaits[day];
		if (!f) return false;
		if (!validated)
		{
			// on demande confirmation
			var msg = ''; // <span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
			msg += '<p class="rouge">' + "<strong>Attention</strong>, le tarif s�lectionn� est disponible <br/>aux <strong>dates et horaires</strong> suivants :</p>";
			msg += "<br/>D�part : <strong>"+ f.j_depart + ' ' + f.depart + " � " + f.h_depart + "</strong>";
			msg += "<br/>Retour : <strong>"+ f.j_retour + ' ' + f.retour + " � " + f.h_retour + "</strong>";
			$('#dialog-confirm-cal').dialog('destroy');
			$('#dialog-confirm-cal-msg').html(msg);
			$('#dialog-confirm-cal').dialog({
				resizable: false,
			//	show: 'slide',
				minHeight: 20,
			//	height: 200,
				width: 400,
				modal: true,
				buttons: {
					"Modifier les dates": function()
					{
						updateCal(day, true);
						$(this).dialog('close');
					},
					"Annuler": function()
					{
						$(this).dialog('close');
					}
				}
			});
			// ajout de classes sur les boutons
			$.each($('#dialog-confirm-cal').dialog("option", "buttons"), function(i){
				$('#dialog-confirm-cal + .ui-dialog-buttonpane button:contains("'+i+'")').addClass(i.toLowerCase().replace(/[^a-z0-9]+/g, '_'));
			});
			// pour IE7-8 forcer la s�lection du current
			$('#offre_calendar .current input:radio').attr('checked','checked');
			return false;
		}
		// le tracking
		<? $a = explode('|', $agenceSearch);?>
		sp_clic("N", "<?=$this->_reservation['type'].'_'.$a[1]?>", "<?=join('_', array_slice($a, -3))?>", f.sp_suivi);
		// on supprime le current
		$('#offre_calendar .current').removeClass('current');
		updateForfait(f);
		td.addClass('current').find('input:radio').attr('checked','checked');
		return false
	}
// ]]>
</script>
<?
		$h.= ob_get_contents();
		ob_end_clean();
		return $h;
	}
}
?>
