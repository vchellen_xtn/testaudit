<?php
class Tracking_SmartProfile
{
// d�terminer le $hashID
	static public $SP_HASHID = array(
		'DEV'	=> array('ecommerce'=>'DF063BD6F754FBB66B2CA92FC492DBA8', 'clic'=>'43A5CBF3FAFFC3C592B193D667042CDB'),
		'TEST'	=> array('ecommerce'=>'DF063BD6F754FBB66B2CA92FC492DBA8', 'clic'=>'43A5CBF3FAFFC3C592B193D667042CDB'),
		'www'	=> array('ecommerce'=>'80610E3055C97FF7F659EC0F89C87E75', 'clic'=>'8DD48D6A2E2CAD213179A3992C0BE53C'),
		'v2'	=> array('ecommerce'=>'80610E3055C97FF7F659EC0F89C87E75', 'clic'=>'8DD48D6A2E2CAD213179A3992C0BE53C'),
		'jeunes'=> array('ecommerce'=>'E54DF36E8B1826EED47B62F6712EDF1B', 'clic'=>'48DACB31FF4119AEF9D62C3B012CA682'),
		'mobile'=> array('ecommerce'=>'D5474F49F9F0FD8DFE7B0527FECB29F7', 'clic'=>'FA0563273DF2C7F983D7563746F447FF'),
		'admin' => array('ecommerce'=>'4E5CD9DED3DE44348EAA8652F58B4480'),
		'resas' => array('ecommerce'=>'4E5CD9DED3DE44348EAA8652F58B4480'),
	);

	static public function doTracking(Page $page, $hashKey = null)
	{
		// d�terminer la page
		$trackPage = $page->getTrackPage();
		foreach(array(SITE_MODE, $hashKey, $page->getSkin(), 'www') as $k)
		{
			if (isset(self::$SP_HASHID[$k]))
			{
				$SPKey = $k;
				break;
			}
		}
?>
		<!-- <Tag Smartp> -->
		<!-- COPYRIGHT NSP Tous droits r�serv�s. -->
		<script language="javascript" type="text/javascript">
		$(window).load(function()
		{
			var ci = "", sp = new Array();
			sp[0]="?_hid=<?=self::$SP_HASHID[$SPKey]['ecommerce']?>";
			sp[1]="&_title=fr_<?=$trackPage?>";
			sp[2]="&_class=fr_<?=$trackPage?>";
			sp[3]="&_event=<?=htmlspecialchars($_REQUEST['event'])?>";
			sp[4]="&version=<?=$page->getSkin();?>";
<?
			if (is_array($_SESSION['_track']['nsp'])) : // variables de tracking
			foreach($_SESSION['_track']['nsp'] as $key => $val) : // �crire le r�sultat du tracking
				if (!$val) continue;
?>
			sp[sp.length]="&<?=$key?>=<?=$val?>";
<?			endforeach;
			endif;
?>
			if (typeof sp_t != 'undefined') sp_tracker(sp,ci);
			if (typeof(sp_clic_init) == 'function') sp_clic_init("<?=self::$SP_HASHID[$SPKey]['clic']?>");
			if (typeof(sp_ecommerce_init) == 'function') sp_ecommerce_init("<?=self::$SP_HASHID[$SPKey]['ecommerce']?>");
		});
		</script>
		<!-- </Tag Smartp> -->
<?
		return;
	}

	/** Renvoie une cat�gorie pour SmartProfile
	*
	* @param Categorie $categorie
	* @returns string
	*/
	static public function getCat(Categorie $categorie)
	{
		return 'cat'.str_replace('+',"'",$categorie['mnem']);
	}
	/**
	* Renvoie une d�finition de la recherche
	*
	* @param Ada_Object $reservation	Reservation ou AgenceResa
	* @returns string
	*/
	static public function getAgenceSearch(Ada_Object $reservation)
	{
		$week = array('di','lu','ma','me','je','ve','sa');
		$str = '';
		foreach(array('alias', 'agence') as $k) {
			if (isset($reservation[$k])) {
				$str = $reservation[$k];
				break;
			}
		}
		return $str.'|'.self::getCat($reservation->getCategorie()).'|'.$week[$reservation->getDebut('w')].'|'.$week[$reservation->getFin('w')].'|'.$reservation['duree'];
	}

	/**
	* Renvoie le Tracking Produit Vu (pr_vu) pour SmartPofile
	* sous la forme famille.gamme.produit
	* @param Ada_Object $reservation	Reservation ou AgenceResa
	* @param string $page
	* @param Categorie $categorie cat�gorie du v�hicule
	* @param array $themes th�mes pur s�lectionner les options � afficher
	* @return string
	*/
	static public function getPRVU(Ada_Object $reservation, $page, Categorie $categorie = null, $themes = array())
	{
		// cat�gorie et options
		$cat = self::getCat($categorie ? $categorie : $reservation->getCategorie());
		$options = $reservation->getAllOptions();
		$page = basename($page);
		if (empty($themes)) {
			$themes = array();
		}

		// construire le tableau des produits
		$pr = array(); $format = '%s.%s.%s';
		if ($page == 'resultat')
			$pr[] = sprintf($format, $reservation['type'], $cat, 'demande');
		/* GEB : 2013-02-28 : avant ventes additionnelles
		$pr[] = sprintf($format, $reservation['type'], $cat, 'options', $options->count(), round($options->getTotal()));
		*/
		// sortir toutes les options de mani�re d�taill�e
		foreach($options as $id => $option)
		{
			if ((empty($themes) && $option['theme_page'] == $page) || (!empty($themes) && in_array($option['theme'], $themes))) {
				$pr[] = sprintf($format, $option['theme'], $id, $cat);
			}
		}
		return join(';', $pr);
	}


	/**
	* Renvoie le Tracking Produit (pr_in, pr_ach) pour SmartPofile
	* sous la forme famille.gamme.produit.quantite.prix
	* @param Ada_Object $reservation	Reservation ou AgenceResa
	* @return string
	*/
	static public function getPR(Ada_Object $reservation)
	{
		// cat�gorie et options
		$cat = self::getCat($reservation->getCategorie());
		$options = $reservation->getResOptions();

		// construire le tableau des produits

		$pr = array(); $format = '%s.%s.%s.%d.%ld';
		$pr[] = sprintf($format, $reservation['type'], $cat, 'demande', 1, round($reservation->getPrixForfait()));
		/* GEB : 2013-02-28 : avant ventes additionnelles
		$pr[] = sprintf($format, $reservation['type'], $cat, 'options', $options->count(), round($options->getTotal()));
		*/
		// sortir toutes les options de mani�re d�taill�e
		foreach($options as $id => $option)
			$pr[] = sprintf($format, $option['theme'], $id, $cat, max(1, $option['quantite']), round($option['prix']));
		return join(';', $pr);
	}

	/**
	* Renvoyer un tableau avec tous les param�tres � suivre poru la validation d'une r�servation
	*
	* @param Ada_Obect $reservation	Reservation ou AgenceResa
	* @return array
	*/
	static public function reservationValidation(Ada_Object $reservation)
	{
		if (!in_array(get_class($reservation), array('Reservation','AgenceResa')))
			return array();
		/** @var Client */
		$client = $reservation->getClient();
		/** @var ResOption_Collection */
		$options = $reservation->getResOptions();

		// renseigner les informations de tracking
		$track = array();
		$track['equipements'] = implode(',', $options->getIds());
		if ($options->hasFacturation('COURTAGE'))
			$track['assurancesell'] = implode(',', $options->filter('COURTAGE')->getIds());
		$track['cond_sup'] = $options->hasOption('covp') ? 'oui' : 'non';
		$track['agence'] = self::getAgenceSearch($reservation);
		if ($_COOKIE['ADA001_TRACK_AGENCE']) {
			$x = explode('|', $track['agence']);
			$y = explode('|', $_COOKIE['ADA001_TRACK_AGENCE']);
			if ($x[0]==$y[0] && $x[1]!=$y[1]) {
				$track['upsell'] = sprintf('%s|%s|%s', $x[0], $y[1], $x[1]);
			}
		}
		$track['heure_dep'] = $reservation->getDebut('H');
		$track['heure_arr'] = $reservation->getFin('H');
		$track['duree'] = $reservation['duree'];
		$track['type'] = $reservation['type'];
		$track['trans_id'] = $reservation->getResIDClient();
		$track['trans_ca'] = round($reservation->getPrixTotal());
		$track['panier'] = '1.'.round($reservation->getPrixTotal());
		$track['civilite'] = $client['titre'];
		$track['codepostal'] = $client['cp'];
		$track['pr_ach'] = self::getPR($reservation);
		if ($reservation['nego']) {
			if ($resNego = $reservation->getResNego()) {
				$track['nego'] = round($reservation->getPrixForfait() - $resNego['seuil']);
			}
		}
		// indiquer si les options ont �t� prises ou non..;
		foreach($reservation->getAllOptions() as $id => $option)
			$track[$id] = ($options->hasOption($id) ? 'oui' : 'non');
		return $track;
	}

	/**
	* Transforme un texte en label pour sp_clic()
	*
	* @param string $title
	* @return string
	*/
	static public function toLabel($title)
	{
		return preg_replace('/[^a-z0-9 \-]/i', '', str_replace(array('%','�',"'"), array('P','E',' '), strtolower(filter_chars($title))));
	}
	/**
	* Transforme une URL en destination pour sp_clic()
	* @param string $href
	* @returns
	*/
	static public function toTarget($href)
	{
		$sp_target = 'inconnu';
		if ($href && substr($href, 0, 7) != 'http://' )
		{
			if (preg_match('/\/?(.+\.html)?(\?.+)?$/i', $href, $m))
			{
				$sp_target = $m[1] ? 'fr_'.str_replace(array('/', '.html'), array('_',''), $m[1]) : 'fr_reservation_index';
				if ($m[2])
					$sp_target .= str_replace(array('?','&','='), '_', $m[2]);
			}
		}
		else if (preg_match('/^https?\:\/\/([^\/]+)\/.*$/i', $href, $m))
		{
			$sp_target = str_replace('.', '_', $m[1]);
		}
		return strtolower($sp_target);
	}
}

