<?
	session_start();
	include (dirname(__file__).'/constant.php');

	// analyser la demande
	switch ($_GET['method'])
	{
		case 'agence':
			$x = Agence::factory($_GET['agence'], true);
			if ($x)
			{
				$x->getHoraires();
				$x->getFermetures();
				$x['url_resa'] = $x->getURLResa();
				$x['url_agence'] = $x->getURLAgence();
			}
			$attributes = explode(',', 'id,nom,tel,fax,adresse1,adresse2,cp,ville,commentaire,url_resa,url_agence,latitude,longitude,horaires,fermetures');
			$rootName = 'agence';
			break;
		case 'agences':
			$rootName = 'agences';
			$childName = 'agence';
			$attributes = explode(',', 'id,nom,nb_promo,adresse1,adresse2,cp,ville,recherche,tel,fax,livraison_km,latitude,longitude,ortho,url_resa,url_agence');
			$x = Agence_Collection::factory($_GET);
			break;
		case 'client.facebook':
			$_GET['format'] = 'json';
			$x = ClientFacebook::createFromSignedRequest($_COOKIE['fbsr_'.FB_APP_ID]);
			$attributes = explode(',','client,facebook_id,facebook_email,facebook_gender,facebook_name,facebook_first_name,facebook_last_name,facebook_birthday');
			break;
		case 'client.check':
			$_GET['format'] = 'json';
			if (!$x = Client::factory($_GET['email']))
				$x = new Client();
			$attributes = array('email');
			break;
		case 'client.perdu':
			$_GET['format'] = 'json';
			if ($x = Client::factory($_GET['email']))
				$x->sendPassword();
			else
				$x = new Client();
			$attributes = array('email');
			break;
		case 'codepromo.check':
			$msg = null; $errors = null; $url = null;
			$reservation = Reservation::create($_POST, $errors, $url);
			if ($code = filter_var(trim($_REQUEST['codepromo']), FILTER_SANITIZE_STRING))
			{
				if (PromotionCode::checkCode($code))
					$msg = "Ce code de r�duction pourra �tre utilis� sur la page de paiement !";
				else
				{
					if ($coupon = PartenaireCoupon::factory($code, true))
						$coupon->isValidFor($reservation ? $reservation : new Reservation(array('type'=>$_POST['type'], 'debut'=>date('Y-m-d H:0:0'))), $msg);
					else
						$msg = "Ce code promotionnel n'existe pas.";
				}
			}
			$x = new Ada_Object(array('message'=>$msg, 'errors'=>$errors));
			$attributes = array('message', 'errors');
			break;
		case 'dept':
			$attributes = array();
			$rootName = 'data';
			$childName = 'option';
			$x = Dept_Collection::factory();
			break;
		case 'devis':
			$msg = null;
			$x = new Ada_Object();
			if ($devis = Devis::create($_POST, $msg)) {
			    $x->setData('message', $msg);
			} else {
			    $x->setData('message', "L'envoi du devis n'a pas �t� possible.");
			}
			$attributes = array("message");
			break;
		case 'forfait':
			/** @var Forfait */
			$x = Forfait::doUnserialize(isset($_POST['forfait']) ? $_POST['forfait'] : $_GET['forfait']);
			if ($x)
			{
				header("X-Robots-Tag: noindex");
				header('Content-Type: text/html; charset=windows-1252');
				$block = (isset($_POST['block']) && in_array($_POST['block'], array('block-v2','block-v3','block-mobile'))) ? $_POST['block'] : 'block-v2';
				echo $x->setSurcharge(true)->toHtml($block);
			}
			return;
			break;
		case 'negociation':
			$attributes = array();
			$x = PromotionNegociation::doNegociation($_POST);
			break;
		case 'offres':
			// API avec authentification
			if (!empty($_POST['login']) && !empty($_POST['pwd'])) {
				$attributes = array();
				$apiAccess = ApiAccess::factory($_POST['login'], $_POST['pwd'], $_SERVER['REMOTE_ADDR']);
				if ($apiAccess['status'] == 'KO')
					$x = $apiAccess;
				else
					$x = $apiAccess->getOffres($_POST);
			// API pour les prix
			} else {
				$attributes = array('status', 'errors', 'agence', 'offres');
				$apiOffres = ApiOffres::factory($_POST, $_SERVER['REMOTE_ADDR']);
				if ($apiOffres['status'] == 'OK') {
					$apiOffres->getOffres($_POST);
				}
				$x = $apiOffres;
			}
			break;
		case 'promo':
			$attributes = explode(',','id,zone,agence,type,slogan,benefice,depuis,jusqua,debut,fin,url_promo,description,mentions');
			$rootName = 'promos';
			$childName = 'promo';
			$x = PromotionForfait_Collection::factory($_GET, true);
			break;
		default:
			$x = null;
	}
	// puis afficher le retour
	header("X-Robots-Tag: noindex");
	if ($x)
	{
		switch($_GET['format'])
		{
			case 'xml':
				header('Content-Type: text/xml; charset=utf-8');
				echo $x->toXml($attributes, $rootName, $childName);
				break;
			case 'sxp':
				header('Content-Type: text/plain; charset=utf-8');
				echo $x->toSExp($attributes, $rootName, $childName);
				break;
			case 'json':

				header('Content-Type: application/json; charset=utf-8');
				echo $x->toJson($attributes);
				//print_r(json_decode($x->toJson($attributes), true));
				break;
			default:
				/* do nothing*/
		}
	}
?>
