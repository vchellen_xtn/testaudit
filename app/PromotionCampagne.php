<?php
class PromotionCampagne extends Ada_Object
{
	/**
	* Cr�e une PromotionCampagne
	* 
	* @param int $id
	* @param bool $withStats
	* @return PromotionCampagne
	*/
	static public function factory($id, $withStats = false)
	{
		if (!$id) return null;
		$x = new PromotionCampagne();
		$x->load((int)$id);
		$x['archive'] = ($x['fin'] && $x['fin'] < date('Y-m-d'));
		if ($withStats)
			$x->loadStats();
		return $x;
	}
	
	/**
	* Retrouve une campagne associ�e un � code UNIK, cr�e un code si possible et le renvoie
	* 
	* @param string $unik
	* @param string $msg
	* @returns PromotionCode
	*/
	static public function createCodeFromUnik($unik, &$msg)
	{
		// on nettoie le code UNIK
		$unik = preg_replace('/[^a-z0-9]/i', '', filter_chars($unik));
		if (!$unik) return;
		$id = getsql("select id from promotion_campagne where unik='".addslashes($unik)."'");
		if (!$id) return;
		$campagne = PromotionCampagne::factory($id);
		$jour = date('Y-m-d');
		if ($campagne['debut'] && $campagne['debut'] > $jour)
		{
			$msg = "Le code sera valide � partir du ".euro_iso($campagne['debut']);
			return;
		}
		if ($campagne['fin'] && $campagne['fin'] < $jour)
		{
			$msg = "Le code n'est plus valide depuis le ".euro_iso($campagne['fin']);
			return;
		}
		$code = $campagne->getOneCode('ADAFR');
		if (!$code)
		{
			$msg = "Tous les codes de r�duction disponibles ont �t� distribu�s.";
			return;
		}
		return $code;
	}
	/**
	* Charge les statistiques pour le back office
	* 
	*/
	public function loadStats()
	{
		if (!$this->isEmpty() && !isset($x['distribues']))
		{
			$row = mysql_fetch_assoc(sqlexec('select count(g.id) crees, count(g.distribution) distribues, count(g.validation) valides, count(g.paiement) utilises, sum(reduction) reductions from promotion_campagne c left join promotion_code g on g.campagne=c.id where c.id='.$this->getId().' group by c.id'));
			if ($row)
			{
				foreach($row as $k => $v)
					$this->setData($k, $v);
			}
		}
		return $x;
	}
	
	public function __toString()
	{
		$str = $this->_data['slogan'];
		if (!$str) $str = '(slogan manquant)';
		$str.= ' - '.$this->_data['nom'];
		return $str;
	}
	
	/**
	* Cr�e un nombre de codes pour cette campagne
	* 
	* @param int $add nombre de codes � cr�er
	* @param string loign de l'utilisateur
	*/
	public function createCodes($add, $login = null)
	{
		$add = (int) $add;
		if ($add < 1) return $this;
		$nb = $this->getData('nb');
		$now = date('Y-m-d H:i:s');
		$campagne = $this->getId();
		// cr�er le premier terme du code
		$batch = PromotionCode::B32FromID($campagne);
		for($i=0; $i<$add; $i++)
			$codes[] = "($campagne, '$now', '".PromotionCode::makeCode($batch, $nb+$i+1)."')";
		// on ins�re les codes dans la table
		getsql('INSERT INTO promotion_code (campagne, creation, code) VALUES '.join(',', $codes));
		// augmenter le nombre de codes
		getsql("UPDATE promotion_campagne SET nb = nb + $add WHERE id=$campagne");
		$this->setData('nb', $nb + $add);
		if ($this->hasData('crees'))
			$this->setData('crees', $this->getData('crees') + $add);
		if ($login)
			$this->addHisto($login, $add.' codes cr��s');
	}
	public function addHisto($login, $commentaire)
	{
		$a = array('id'=>'', 'campagne'=>$this->getId(), 'login'=>$login, 'commentaire'=>$commentaire);
		save_record('promotion_campagne_histo', $a);
	}
	/**
	* Renvoie le premier code non distribu� et le marque comme distribu�
	* En cr�e un si n�cessaire
	* 
	* @param string $login
	*/
	public function getOneCode($login, Client $client = null, Reservation $reservation=null)
	{
		$sql = "select id from promotion_code where campagne=".$this->getId()." and distribution is null order by id limit 0, 1";
		$id = getsql($sql);
		if (!$id)
		{
			// la campagne autorise-t-elle la cr�ation automatique ?
			if (!$this->_data['automatique'])
				return null;
			$this->createCodes(1, $login);
			$id = getsql($sql);
		}
		$code = PromotionCode::factory($id, false);
		if ($code)
		{
			$code->setData('distribution', date('Y-m-d H:i:s'));
			$code->addCommentaire("distibu� par [$login]");
			if ($client)
			{
				$code->setData('client', $client['id']);
				$code->addCommentaire(sprintf("distribu� au client #%ld - %s", $client['id'], $client['email']));
			}
			if ($reservation)
				$code->addCommentaire(sprintf("distribu� suite annulation r�servation #%ld", $reservation['id']));
			$code->save();
		}
		return $code;
	}
}
?>
