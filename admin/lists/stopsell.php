<?
	session_start();
	include ('../secure.php');
	$title 	 = "Stop Sell";
	$url 	 = "../stopsell.php";
	$key	 = "s.id";
	$order	 = "";

	$sql = "select s.id, s.provenance as prov,";
	$sql.= "	   case when reseau is null then 'Tous' else reseau end as reseau,";
	$sql.= "	   case when groupe is null then 'Tous' else zg.nom end as groupe,";
	$sql.= "	   case when code_societe is null then 'Toutes' else code_societe end as code,";
	$sql.= "	   case when agence is null then 'Toutes' else agence end as agence,";
	$sql.= "	   case when s.type is null then 'Tous' else s.type end as type,";
	$sql.= "	   case when c.mnem is null then 'Toutes' else c.mnem end as categorie,";
	$sql.= "	   case when s.forfait_prefix is null then 'Tous' else s.forfait_prefix end as prefixe,";
	$sql.= "	   case when s.forfait is null then 'Tous' else s.forfait end as forfait,";
	$sql.= "	   s.debut, s.fin";
	$sql.= "  from stop_sell s";
	$sql.=" left join categorie c on s.categorie=c.id";
	$sql.=" left join zone_groupe zg on zg.id=s.groupe";
	$sql.= " where s.zone='".$_SESSION['ADA001_ADMIN_ZONE']."'";
	
	if ($_GET['agence'])
	{
		$a = mysql_fetch_assoc(sqlexec("select ap.id, ap.agence, a.code_societe, a.groupe, a.reseau from agence a join agence_pdv ap on ap.agence=a.id where ap.id='".filter_var($_GET['agence'], FILTER_SANITIZE_STRING)."'"));
		$sql.= " and (s.agence is null or s.agence='".$a['id']."'or s.agence='".$a['agence']."')";
		$sql.= " and (s.code_societe is null or s.code_societe='".addslashes($a['code_societe'])."')";
		$sql.= " and (s.groupe is null or s.groupe='".$a['groupe']."')";
		$sql.= " and (s.reseau is null or s.reseau='".$a['reseau']."')";
	}
	if ($_GET['type'])
		$sql.= " and (s.type is null or s.type='".addslashes($_GET['type'])."')";
	if ($_GET['categorie'])
		$sql.= " and (s.categorie is null or s.categorie='".(int)$_GET["categorie"]."')";
	if ($_GET['forfait'])
		$sql.=" and ((forfait_prefix is null or '".addslashes($_GET['forfait'])."' like forfait_prefix) and (forfait is null or forfait='".addslashes($_GET['forfait'])."'))";
	
	$out_url = "list.php?sql=" . urlencode($sql);
	$out_url.= "&title=" . urlencode($title);
	$out_url.= "&url=" . urlencode($url);
	$out_url.= "&new_url=" . urlencode($url . "?" . $_SERVER['QUERY_STRING']);
	$out_url.= "&color=4587ba";
	$out_url.= "&key=".$key."&order=".$order."&nb_rows=30";
	header("location: ".$out_url);
?>
