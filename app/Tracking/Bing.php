<?php
class Tracking_Bing
{
	static public function doTracking(Page $page)
	{
		$id = $page->getId();
		//Yahoo/Bing
		if (in_array($id, array('reservation/validation','jeunes/validation')) && (/** @var Reservation */ $reservation = $page->getData('reservation')) && !$reservation->isLocapass() && $reservation['statut']=='P') :
			$ms_dedup = 2;
			$ms_revenue = $reservation['total'];
?>
			<script type="text/javascript">
			/* <![CDATA[ */
				if (!window.mstag) mstag = {loadTag : function(){},time : (new Date()).getTime()};
			/* ]]> */
			</script>
			<? if (SITE_MODE == 'PROD') : ?>
			<script id="mstag_tops" type="text/javascript" src="//flex.msn.com/mstag/site/4e345c3e-b854-4502-b253-fdc2e5304903/mstag.js"></script>
			<noscript>
				<iframe src="//flex.msn.com/mstag/tag/4e345c3e-b854-4502-b253-fdc2e5304903/analytics.html?dedup=<?=$ms_dedup;?>&domainId=46005906&type=1&revenue=<?=$ms_revenue;?>&actionid=267844" frameborder="0" scrolling="no" width="1" height="1" style="visibility: hidden; display: none"></iframe>
			</noscript>
			<? endif; ?>
			<script type="text/javascript">
				mstag.loadTag("analytics", {dedup:"<?=$ms_dedup;?>",domainId:"46005906",type:"1",revenue:"<?=$ms_revenue;?>",actionid:"267844"})
			</script>
<?		endif;
		//!Yahoo/Bing
	}
}

