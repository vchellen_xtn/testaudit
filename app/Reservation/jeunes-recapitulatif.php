<?php
/**
* $this est Reservation
* $page page o� se trouve le bloc
* $showTotal : afficher le total ou pas ?
* $showPrixOptions : afficher ou non le prix des options
*/
	/** @var Reservation */
	$reservation = $this;
	/** @var ResOption_Collection */
	$options = $reservation->getResOptions();
	/** @var Categorie */ 
	$categorie = $reservation->getCategorie();
	/** @var Agence */ 
	$agence = $reservation->getAgence();
?>
<div class="colonne_offre">
	<div class="colonne-offre-top">
		<img src="../skin/jeunes/jeunes/img/resultat-offre-bulle.png" alt="" />
	</div>
	<div id="offre_courante">
		<div class="col-wrapper">
			<div class="col-1-4 left">
				<img src="../skin/jeunes/jeunes/img/picto-exclu-web.png" alt="exclusivit� web" />
			</div>
			<div class="col-3-4 left">
				<? if ($reservation->isLocapass()) :?>
					<img src="../css/img/locapass.png" alt="Locapass" />
				<? elseif ($showTotal): ?>
					<span class="promo_prix">
						<span id="forfait_t_prix"><?=show_money($reservation->getPrixTotal())?>�</span>
					</span>
				<? endif; ?>
			</div>
			<div class="clear"></div>
		</div>		
		<h3><?=$categorie['nom']?></h3>
		<br/>
		<img src="../<?=$categorie->getImg('big')?>" class="visu" alt="" />
	</div>	
	<div id="mes_informations">
		<p>
			<strong><?=$agence['nom']?></strong>&nbsp;|&nbsp;<a href="../popup.php?page=agences/agence&id=<?=$agence['id']?>" onclick="return wOpen(this.href, 630, 800)" class="infos_agence">Horaires et plan d'acc�s</a>
			<br /><?=$agence->getHtmlAddress('<br/>','adresse1,cp_ville'); ?>
			<br/>
			<br/>
			<hr />
			<? if (strstr('AP', $reservation['statut'])) : ?>
			<br/><strong>R�servation n&#176;</strong><? echo $reservation->getResIDClient(); ?>
			<? endif; ?>
			<br/><strong>Location </strong>du <?=$reservation->getDebut('d/m/y H:i');?> au <?=$reservation->getFin('d/m/y H:i');?>
			<br/><strong>Dur�e </strong><?=$reservation->getDuree();?>
			<br/><strong>Kilom�trage inclus </strong><?=$reservation->getKm(false);?>
			<? if ($reservation->getPrixCoupon()) : ?>
			<br /><strong>Offre Pro</strong>
			<? endif; ?>
			<br/>
			<br/>
			<hr />
			<? echo $reservation->getResOptions()->toHtml2($showPrixOptions); ?>
		</p>
		<? if (!strstr('VPXA', $reservation['statut'])) : ?>
		<form name="reservation" method="post" action="<?=Page::getURL('jeunes/resultat.html');?>">
		<?
			foreach($reservation->getFields() as $k => $v)
			{
				if (!$v) continue;
				echo '<input type="hidden" name="'.$k.'" value="'.$v.'"/>';
			}
			$type = strtolower($reservation['type']);
			$date_retrait = null;
			foreach($reservation->getResOptions() as $k => $option)
			{
				if ($option['quantite'])
					echo '<input type="hidden" name="options_u['.$k.']" value="'.$option['quantite'].'"/>';
				else
					echo '<input type="hidden" name="options[]" value="'.$k.'"/>';
				if (!$date_retrait && $option['retrait'])
					$date_retrait = $option['retrait'];
			}
			if ($date_retrait)
				echo '<input type="hidden" name="options_retrait" value="'.$date_retrait.'"/>';
		?>
			<input type="hidden" name="categorie_choisie" value="<?=$reservation['categorie']?>"/>
			<p class="valid"><input class="modifiez" type="submit" value="Modifiez" /></p>
		</form>
		<? endif; ?>
		<? if (dirname($page) != 'jeunes' && !$reservation->isInPayment()) :?>
		<img src="../css/img/audiotel_p3-4.png" alt="Besoin d'aide ? Nos �quipes sont � votre disposition au 0899 46 46 36" style="margin:25px 0 0;" />
		<? endif; ?>
	</div>
</div>
