<?php
class VehiculeModele extends Ada_Object
{
	
	/**
	* Cr�e un v�hicule mod�le
	* 
	* @param int $id
	* @return VehiculeModele
	*/
	static public function factory($id)
	{
		$x = new VehiculeModele();
		$x->load((int) $id);
		if ($x->isEmpty()) return null;
		return $x;
	}
	public function __toString() {
		return $this->_data['nom'];
	}
}
?>
