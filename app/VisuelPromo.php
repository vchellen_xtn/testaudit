<?
class VisuelPromo extends Ada_Object
{
	/**
	* Cr�e un VisuelPromo � partir d'un id
	* 
	* @param int $id
	* @return VisuelPromo
	*/
	public function showVisuel($base, $width=670, $height=180)
	{
		$row = $this->getData();
		$sp_target = Tracking_SmartProfile::toTarget($row['href']);
		if ($row['href'] && substr($row['href'], 0, 7) != 'http://' ) {
			$row['href'] = $base.ltrim($row['href'],'/');
		}
		$media = $base.$row['url_img'];
		if (substr($media, -3) == 'swf'):
	?>
		<object type="application/x-shockwave-flash" data="<?=$media?>" width="<?=$width?>" height="<?=$height?>">
			<param name="movie" value="<?=$media?>" />
			<param name="wmode" value="transparent"/>
			<param name="allowScriptAccess" value="samedomain"/>
		</object>
	<? else: ?>
		<? if ($row['href']) : ?>
		<a href="<?=$row['href'];?>" title="<?=$row['commentaire'];?>" target="<?=$row['target']?>" onclick="sp_clic('N', 'bannieres', '<?=Tracking_SmartProfile::toLabel($row['commentaire'])?>', '<?=$sp_target?>'); return true;">
		<? endif; ?>
			<img src="../img/z.gif" width="<?=$width;?>" height="<?=$height;?>" data-original="<?=$media?>" alt="<?= $row['commentaire']; ?>" />
			<noscript><img src="<?=$media?>" alt="<?= $row['commentaire']?>" /></noscript>
		<? if ($row['href']) : ?>
		</a>
		<? endif; ?>
	<? endif;
	}
}
?>
