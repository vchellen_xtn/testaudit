<? if ($page == "pro/offre_hyundai") : ?>
<div class="tpl-col">
	<h2 class="h4-like">ADA partenaire de <img src="../css/img/logo-hyundai.png" alt="Hyundai" class="valign-middle" /></h2>

	<div class="hr-col"></div>

	<img src="../css/img/visu-gamme-hyundai.png" alt="" />
	<h3 class="h4-like">La gamme Hyundai <img src="../css/img/logo-i40-hyundai.png" alt="i40" class="valign-middle" /></h3>
	<p class="small grey">
		Un design sculpt�, une technologie de pointe et des performances de premier ordre.
	</p>

	<div class="hr-col"></div>

	<h3 class="h4-like">D�couvez la Hyundai i40 Pack Business</h3>
	<p class="small grey">
		Le v�hicule id�al pour tous vos d�placements professionnels.
	</p>
	<br />
	<img src="../css/img/illus-offre-hyundai-1.jpg" alt="" /><br />
	<br />
	<h3 class="h4-like">Le confort</h3>
	<p class="small grey">
		Un regard sur le tableau de bord et tout devient clair. Tous les cadrans et les commandes ont �t� parfaitement dimensionn�s et dispos�s par univers logique pour un fonctionnement sans effort. De m�me pour les mat�riaux : leur qualit� et le soin du d�tail sont un ravissement pour les yeux.
	</p>

	<div class="hr-col"></div>

	<img src="../css/img/illus-offre-hyundai-2.jpg" alt="" /><br />
	<br />
	<h3 class="h4-like">La performance</h3>
	<p class="small grey">
		Bo�tes de vitesses � six rapports, syst�me d�arr�t et red�marrage automatique Blue Drive ou encore le calage variable des soupapes ne sont que quelques unes des technologies disponibles sur la gamme i40 pour vous permettre de r�duire votre consommation sans p�naliser les performances.
	</p>

	<div class="hr-col"></div>

	<h3 class="h4-like">Vu � la t�l�vision</h3>
	<a href="#" id="video_hyundai"><img src="../css/img/illus-video-offre-hyundai.jpg" alt="" /></a>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		
	});
</script>

<? else : ?>
<ul class="menu">
	<li>
		<a href="index.html" class="<? if ($page=="pro/index") echo ' current';?>">Vous �tes une entreprise</a>
		<ul class="sous-menu">
			<li><a href="courte_duree.html" <? if ($page=="pro/courte_duree") echo ' class="current"';?>>Location courte dur�e</a></li>
			<li><a href="moyenne_duree.html" <? if ($page=="pro/moyenne_duree") echo ' class="current"';?>>Location au mois � Moyenne dur�e</a></li>
		</ul>
	</li>
	<li>
		<a href="ada_ce.html" class="<?=($page=="pro/ada_ce")?' current':''?>">Vous �tes un CE</a>
	</li>
</ul>
<? endif; ?>

