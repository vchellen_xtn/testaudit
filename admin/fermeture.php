<?
	require_once("secure.php"); 
	require_once("lib.php");

	// identifiant
	$id = (int) getarg("id");
	
	// suppression
	if ($id && getarg("delete")) 
	{
		$sql = "delete agence_fermeture, agence_ouverture from agence_fermeture left join agence_ouverture on agence_ouverture.fermeture=agence_fermeture.id where agence_fermeture.id = '".$id."'";
		//die($sql);
		getsql($sql);
		unset($id);
	}
	
	// modification, cr�ation
	if (getarg("update") || getarg("create"))
	{
		if (!$id) $_POST['zone'] = $_SESSION['ADA001_ADMIN_ZONE'];
		save_record('agence_fermeture', $_POST);
		if (!$id) $id = mysql_insert_id();
	}

	$agence = $_GET["agence"];
	if ($id)
	{
		$rs = sqlexec("select * from agence_fermeture where id = ".$id);
		if (!$rs || !($row = mysql_fetch_assoc($rs)))
			die("Aucun enregistrement correspondant.");
		$agence = $row["agence"];
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script language="javascript" src="../scripts/check.js"></script>
	<script>
	<!-- //

	// doDelete
	function doDelete(frm)
	{
		return (confirm("Voulez-vous r�ellement supprimer cet �l�ment ?\nCette op�ration est irr�versible."));
	}
	
	// doSubmit
	function doSubmit(frm)
	{
		var err = "";
		if(!ValidateSelect(frm.agence))
			err += "Veuillez indiquer une agence\n";
		if(!ValidateDate(frm.debut))
			err += "La date de debut est incorrecte\n";
		if(!ValidateDate(frm.fin))
			err += "La date de fin est incorrecte\n";
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		return true;
	}
	// -->
	</script>
</head>
<body>
<form action="" method="POST" onsubmit="return doSubmit(this);">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre"><img src="img/puce.gif" align="absmiddle" hspace="1">Fermeture</th>
</tr>
<tr>
	<td>
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="15%">
		<col width="35%">
		<col width="15%">
		<col width="35%">
		<tr>
			<td>Id</td>
			<td>
<? 
	echo $id;
	if ($id) echo ' ('.$row['provenance'].')';
	echo '<input type="hidden" name="id" value="'.$id.'">';
?>
			</td>
			<td>Agence</td>
			<td>
			<?
				$sql = "select id, concat(id, '-', nom) from agence where zone='".$_SESSION['ADA001_ADMIN_ZONE']."'";
				if ($agence) $sql.=" and id='".$agence."'";
				$sql.=" order by id";
				selectsql("agence", $sql, $agence, array("%", "Toutes les agences"));
			?>
			</td>
		</tr>
		<tr>
			<td>debut</td>
			<td><input type="text" style="width:100%" name="debut" value="<?=euro_iso($row['debut'])?>"></td>
			<td>fin</td>
			<td><input type="text" style="width:100%" name="fin" value="<?=euro_iso($row['fin'])?>"></td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%">
		<tr>
		<td>
		<? if ($id && $row['agence'] == '%') { ?>
			<input type="button" onclick="parent.list.location.href='lists/ouverture.php?fermeture=<?=$id?>'" value="exceptions">
		<? } ?>
		</td>
		<td align="right">
			<input type="submit" name="<?=$id ? "update" : "create"?>" value="enregistrer" onclick="return doSubmit(this.form);">
<?
	if ($id) 
	{
?>
			&nbsp;
			<input type="submit" onclick="return doDelete(this.form);" name="delete" value="supprimer">
<?
	}
?>
		</td>
		</tr>
	</table>
</tr>
</table>
</form>

</body>
</html>
