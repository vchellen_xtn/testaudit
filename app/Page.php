<?
if (!defined('SKIN_DEFAULT')) {
	define('SKIN_DEFAULT', 'v2');
}


class Page extends Ada_Object
{
	// variables statiques
	static protected $_TIME_TO_GO = null; // temps restant avant amintenance
	static protected $_noindex = false;	// index ou pas la page
	static protected $_nofollow = false;

	// g�rer le JS et CSS minimis�s
	static protected $_toMinify = array();
	static protected $_fromMinify = array();
	static protected $_useMinified;
	static protected $_initMinified = false;
	static protected $_jsGoogleLoad = array (); /* 2015-04-15 : google.load() ne supporte pas les versions r�centes de jquery et jquery-ui */
	static protected $_jqueryVersions = array(
		'default' => array(
			'jquery'	=> '1.3.2',
			'jquery-ui'	=> '1.7.2',
			'jquery-ui-css' => 'css/jquery-ui.custom.css',
		),
		'mobile' => array(
			'jquery'	=> '1.8.3', // version contenant $.live() et $.browser
			'jquery-ui'	=> '1.11.3',
			'jquery-ui-css' => 'css/jquery-ui.custom.1.11.css',
		),
		'v3' => array(
			'jquery'	=> '1.8.3', // version contenant $.live() et $.browser
			'jquery-ui'	=> '1.11.3',
			'jquery-ui-css' => 'css/jquery-ui.custom.1.11.css',
		),
	);

	// informations de base sur la requ�te
	static protected $_protocol; // http ou https
	static protected $_subURI; // pour le fonctionnement en localhost /Ada/ADA001-Refonte/html/

	// autres informations sur la page
	protected $_baseHref = true; // si true pas de base
	protected $_linkTop = true;  // afficher un lien vers le sommet de la page
	protected $_baseurl; // url de base
	protected $_basetarget; // target si n�cessaire
	protected $_action; // action associ�e � la page


	// variables de la classe
	protected $_skin = 'www';
	protected $_meta = array();
	protected $_js = array();
	protected $_css = array();
	protected $_links = array();

	// pour les rechercher / remplacer dans titre, description, h1, etc
	protected $_infoSearch = array();
	protected $_infoReplace = array();

	// messages entre fooAction() et render()
	protected $_messages = array();
	protected $_trackPage = null;
	protected $_gaTracking = array(); // tracking Google Analytics
	protected $_abTracking = array(); // tracking AB Tasty

	/** @var Acces */
	protected $_adminUser;
	/** @var Client */
	protected $_client = null;


	/**
	*
	* @param string $page
	* @returns Page
	*/
	static public function factory($skin=null, $page = null, $default = 'reservation/index')
	{
		// v�rifier l'authentification puor la pr�-production
		if (!$skin)
			$skin = SKIN_DEFAULT;
		// on v�rifie la page
		$page = Page::checkPage($skin, $page, $default);

		// charger la classe correspondante � la gestion de l page
		$classname = 'Page';
		$base = Page::uc_words(Page::uc_words(dirname($page), '', '-'), '_', '/');
		if (is_file(BP.'/app/Page/'.str_replace('_', '/', $base).'.php'))
			$classname.="_$base";
		/** @var Page */
		$x = new $classname();
		$x->setSkin($skin);

		// charger les informations sur la page
		if ($page != 'maintenance/index')
			$x->load($page);
		// si la page n'existe pas dans la base de donn�es, il faut quand m�me pouvoir l'afficher si la page existe sur le dique
		if ($x->isEmpty())
		{
			$x->setId($page);
			$x->setData('url', $page.'.html');
		}

		// v�rifier les param�tres pass�s en GET
		$x->checkParameters();

		// on v�rifie si la page doit �tre redirig�e
		if ($url = $x->check301())
			Page::redirect($url);
		return $x;
	}
	static public function checkFile($file)
	{
		if (is_file(BP."/$file") && substr(realpath(BP."/$file"),0,-strlen($file)-1) == realpath(BP))
			return true;
		return false;
	}
	static protected function set404()
	{
		header('HTTP/1.0 410 Gone');
	}
	/**
	* Retourne la page � afficher
	*/
	static protected function checkPage($skin, $page = null, $default)
	{
		// page par d�faut
		if (!$page && isset($_GET['page']))
			$page = $_GET['page'];
		if (!$page || !preg_match('/^[a-z0-9]+\/[a-z0-9\-\/\_]+$/i', $page) || !(Page::checkFile("skin/$skin/$page.htm") || Page::checkFile('skin/'.SKIN_DEFAULT."/$page.htm")) )
		{
			if ($page)
			{
				Page::set404();
				self::noIndex();
			}
			$page = $default;
		}

		// en cas de maintenance
		global $TIME_TO_GO;
		if (!is_null($TIME_TO_GO))
		{
			self::$_TIME_TO_GO = $TIME_TO_GO;
			if ($TIME_TO_GO <= 0)
				return 'maintenance/index';
		}
		// pour l'annuaire, on utilise toujours la m�me page
		if (SERVER_RESA != SERVER_ANNU)
		{
			if (SERVER_NAME == SERVER_ANNU)
			{
				if ($page == 'reservation/index')
					$page = 'annuaire/index';
				else if ($page != 'annuaire/index')
					Page::redirect('http://'.SERVER_RESA.$_SERVER['SCRIPT_URL']);
				// on ne veut pas indexer location_vehicule.html
				if ('/location_vehicule.html' == $_SERVER['SCRIPT_URL'])
					self::noIndex();
			}
			// pour ne pas afficher l'annuaire sur www.ada.fr ou mobile.ada.fr
			else if ($page=='annuaire/index')
				Page::redirect('http://'.SERVER_ANNU.$_SERVER['SCRIPT_URL']);
		}

		// finalement renvoyer la page
		return $page;
	}
	/**
	* Retourne l'URL demand�e (avant rewrite)
	*
	*/
	static public function getRequestURI()
	{
		// si on est sur IIS
		if ($_SERVER['HTTP_X_REWRITE_URL'])
			return basename(str_replace('?'.$_SERVER['QUERY_STRING'], '', $_SERVER['HTTP_X_REWRITE_URL']));
		// pour Apache en evitant les urls de la forme http://serveur/?page=
		$x = basename($_SERVER['SCRIPT_URI']);
		return ($x == $_SERVER['SERVER_NAME']) ? 'index.php' : $x;
	}
	/**
	* REnvoie l'URI courante du navigateur
	*
	*/
	static public function getBrowserURI()
	{
		return Page::getProtocol().'://'.$_SERVER['SERVER_NAME'].($_SERVER['HTTP_X_REWRITE_URL'] ? $_SERVER['HTTP_X_REWRITE_URL'] : $_SERVER['REQUEST_URI']);
	}
	static public function redirect($url, $status = 301)
	{
		header ('Location: '.$url, true, $status);
		exit();
	}

	/* ===============================================
	**	FONCTIONS NON STATIQUES
	=============================================== */
	/**
	* appeler par Ada_Object::_construct
	*
	*/
	protected function _construct()
	{
		// forcer la table � utiliser
		$this->setTableName('page');
	}
	/**
	* Skin � utiliser
	*
	* @param string $skin
	* @returns Page
	*/
	protected function setSkin($skin = null)
	{
		if (!$skin || !Page::checkFile("skin/$skin.php"))
			$skin = 'www';
		$this->_skin = $skin;
		return $this;
	}
	/** @var Client */
	public function getConnectedClient() {
		return $this->_client;
	}
	/** @returns Acces */
	public function getAdminUser() { return $this->_adminUser; }
	public function isTeleConseillerLogged() {
		return ($this->_adminUser && $this->_adminUser->isTeleConseiller());
	}
	public function getSkin() { return $this->_skin; }
	public function isSkin($skin) { return ($this->_skin == $skin || $skin == SKIN_DEFAULT); }
	public function getSkinPath() { return BP."/skin/{$this->_skin}.php"; }

	/**
	*
	* cherche les fichiers htm dans le skin actuel ou le skin par d�faut
	* renvoie le chemin du fichier trouv�
	* @param string $page
	* @return string
	*/
	public function getPagePath($page = null)
	{
		if (!$page) {
			$page = $this->getId();
		}
		if (is_file($f = BP . '/skin/' . $this->_skin . '/' . $page . '.htm')) {
			return $f;
		}
		return BP . '/skin/' . SKIN_DEFAULT . '/' . $page . '.htm';
	}
	
    /**
     * cherche les 3 fichiers php 
     *   skin/foo/bar.php
     *   skin/foo/defaut.php
     *   skin/defaut.php
     * sinon, appel getPAgePath pour chercher les fichier htm
     * @param string $skin
     * @return string
     */
	public function getSkinnedPagePath($skin = null)
	{
		if (!$skin)
			$skin = $this->getSkin();
		$skinPath = BP."/skin/$skin/";
		if (is_dir($skinPath))
		{
			// on cherche plusieurs possibilit�s d'affichage
			$page = $this->getId();
			$a = array(
				 $skinPath.$page.'.php'
				,$skinPath.dirname($page).'/default.php'
				,$skinPath.'default.php'
			);
			foreach ($a as $f) {
				if (is_file($f)) {
					return $f;
				}
			}
		}
		// sinon le contenu de la page
		return $this->getPagePath();
	}

	/**
	* Affiche la page cournate
	*/
	public function render()
	{
		// d�claration de variables utilis�s dans les templates
		global $JOURS, $MOIS, $CIVILITE, $EMPLACEMENT, $AGE, $PERMIS, $TYPE;
		$page = $this->getId();
		$protocol = self::getProtocol();
		$dir = self::getSubURI();
		// base des URL en HTTP pour �viter la duplication avec les pages en HTTPS
		$here = self::getURL(null, (SERVER_NAME == SERVER_ANNU || dirname($page)=='zone') ? SERVER_RESA : SERVER_NAME);
		// base pour le appels d'images et rester dans le protocole local
		$baseurl = self::getURL(null, (SERVER_NAME == SERVER_ANNU || dirname($page)=='zone') ? SERVER_RESA : SERVER_NAME, ($protocol=='https'));

		// si une m�thode existe pour la page
		$method = str_replace('-', '_', $this->getAction().'Action');
		if (in_array($method, get_class_methods($this)))
			$this->$method();

		// afficher la page dans le bon skin, qui affichera la page � sont tour
		$this->prepareHead();
		$h1 = $this->getH1();
		$description = $this->getDescription();
		include($this->getSkinPath());
	}

	/*
	** Messages
	*/
	/***
	* ajoute un message
	*
	* @param string $msg
	*/
	protected function _addMessage($msg)
	{
		if (is_array($msg) && count($msg))
			$this->_messages = array_merge($this->_messages, $msg);
		else if ($msg)
			$this->_messages[] = $msg;
		return $this;
	}
	public function hideLinkTop() { $this->_linkTop = false; return $this; }
	public function hasLinkTop() { return $this->_linkTop; }
	public function hasMessages() { return !empty($this->_messages); }
	public function getMessages($join = '<br/>') { return join($join, $this->_messages); }
	static public function showError($msg = null, $linkHref='../', $linkText='Revenir � la r�servation')
	{
		$html = '<div class="msg saviez-vous">'."<strong>Une erreur s'est produite</strong><br />";
		$html.= $msg;
		$html.= '<br /><br />';
		if ($linkHref && $linkText)
			$html.= '<a href="'.$linkHref.'" class="btn">'.$linkText.'<span class="end"></span></a>';
		$html.= '<div class="clear"></div>';
		$html.= '<div class="bottom"></div>';
		$html.= '</div>';
		return $html;
	}

	/* Pour les cookies
	*/
	static protected function _setCookie($name, $value = '', $expire = 0, $domain='', $httpOnly = true)
	{
		setcookie($name, $value, $expire, self::getSubURI().'/', $domain, false, $httpOnly);
		if (empty($value))
			unset($_COOKIE[$name]);
		else
			$_COOKIE[$name] = $value;
	}
	static public function setCookie($name, $value = '', $expire = 0)
	{
		self::_setCookie($name, $value, $expire);
	}
	/**
	* renvoie une cl� associ�e � une valeur
	*
	* @param string $stuff
	* @return string
	*/
	static public function getKey($stuff)
	{
		return md5($stuff.session_id().PRIVATE_KEY);
	}
	static public function checkKey($stuff=null, $name='key', $redir='reservation/index')
	{
		if (!isset($stuff)) $stuff = basename($_SERVER['SCRIPT_NAME']);
		$key = Page::getKey($stuff);
		if ($_REQUEST[$name] == $key || $_COOKIE[$name] == $key)
			return true;
		if ($redir)
			self::redirect(self::getURL($redir.'.html'));
		return false;
	}
	/** Key Custoemr */
	static public function getKeyCustomer($stuff = null)
	{
		return Page::getKey($stuff.$_COOKIE['ADA_CUSTOMER_ID']);
	}
	static protected function _checkKeyCustomer($stuff=null, $redir = 'client/index')
	{
		$name = ($stuff ? 'key' : 'ADA_CUSTOMER_KEY');
		if(self::checkKey($stuff.$_COOKIE['ADA_CUSTOMER_ID'], $name, $redir))
			return ($stuff ? true : Client::factory($_COOKIE['ADA_CUSTOMER_ID']));
	}
	/**
	* Identifie le "client" par cookie
	* @param string email
	*/
	static public function setKeyCustomer($email)
	{
		Page::_setCookie('ADA_CUSTOMER_ID', $email);
		Page::_setCookie('ADA_CUSTOMER_KEY', self::getKey($email));
	}

	/*
	* Prtoocoles et URLS
	*
	*/
	static public function getProtocol()
	{
		if (!self::$_protocol)
			self::$_protocol = ($_SERVER['HTTPS']=='on') ? 'https' : 'http';
		return self::$_protocol;
	}
	static public function getSubURI()
	{
		if (!self::$_subURI)
			self::$_subURI = rtrim(dirname($_SERVER["SCRIPT_NAME"]),'/');
		return self::$_subURI;
	}
	static public function getResaURL($page = null) { return self::getURL($page, SERVER_RESA); }
	static public function getURL($page = null, $server = null, $https = false)
	{
		if (!$server) $server = SERVER_NAME;
		return (($https && USE_HTTPS) ? 'https' : 'http').'://'.$server.self::getSubURI().'/'.$page;
	}

	public function getAction()
	{
		if (!$this->_action)
			$this->_action = basename($this->getId());
		return $this->_action;
	}
	public function getBaseURL($withSubDir = true)
	{
		if (!$this->_baseurl)
			$this->_baseurl = self::getProtocol().'://'.SERVER_NAME.self::getSubURI().'/';
		if ($withSubDir)
			return $this->_baseurl.dirname($this->getId()).'/';;
		return $this->_baseurl;
	}
	public function isOffline() { return ($this->getId()=='maintenance/index'); }

	/** Renvoie si la page a besoin de HTTPS @returns bool */
	protected function isThatPageNeedsHttps()
	{
		return false;
	}

	/**
	* V�rification des param�tres $_GET, $_REQUEST, $_POST
	*/
	protected function checkParameters()
	{
		// on ajoute les JS et CSS par d�faut
		$this->addJS('jquery');
		$this->addJS('jquery-ui');
		$this->addJS('scripts/cookie_cnil.js');

		// on ne fait rien si la page est en maintenance
		if ($this->isOffline()) return false;

		// on identifie
		if (isset($_COOKIE['ADA001_ADMIN_AUTH'])) {
			$this->_adminUser = Acces::createFromCookie($_COOKIE['ADA001_ADMIN_AUTH']);
		}
		// si on est en HTTPS et que la page n'en a pas besoin on redirige
		if (self::getProtocol()=='https' && !$this->isThatPageNeedsHttps())
		{
			self::redirect(preg_replace('/^https/i', 'http', self::getBrowserURI()), 301);
			return;
		}
		// ... et inversement
		if (USE_HTTPS && self::getProtocol()=='http' && $this->isThatPageNeedsHttps())
		{
			self::redirect(preg_replace('/^http/i', 'https', self::getBrowserURI()), 301);
			return;
		}
		// pour d�connecter le compte utilisateur
		if (isset($_GET['logout']))
		{
			$this->_setCookie('ADA_CUSTOMER_ID', '', time() - 24*3600);
			$this->_setCookie('ADA_CUSTOMER_KEY', '', time() - 24*3600);
			// d�truire le cookie facebook s'il existe
			$domain = '';
			if (isset($_COOKIE['fbm_'.FB_APP_ID]))
			{
				$domain = str_replace('base_domain=', '', $_COOKIE['fbm_'.FB_APP_ID]);
				$this->_setCookie('fbm_'.FB_APP_ID, '', time() - 24*3600, $domain, false);
			}
			if (isset($_COOKIE['fbsr_'.FB_APP_ID]))
				$this->_setCookie('fbsr_'.FB_APP_ID, '', time() - 24*3600, $domain, false);
		}
		// authentification du client
		if (isset($_COOKIE['ADA_CUSTOMER_ID'])) {
			$this->_client = self::_checkKeyCustomer(null, false);
		}
		// Code Origine
		if ($_REQUEST['co'])
		{
			list($co, $event) = getsql("select id, event from origine where id ='".preg_replace('/[^a-z0-9\.\-\_]/i', '', $_REQUEST['co'])."'");
			if ($co)
			{
				$this->_setCookie('ADA001_ORIGINE', $co, 0);
				if ($event && !$_REQUEST['event'])
					$_REQUEST['event'] = $event;
			}
		}
		// si l'URL comporte co= et/ou codepromo= on remet l'URL canoniuqe
		$args = $_GET;
		$here = $this->getBrowserURI();
		if (isset($args['co']) || isset($args['codepromo'])) {
			$here = trim(preg_replace(array('/(\?|&)co\=[^\&]+(\&|$)/', '/(\?|&)codepromo\=[^\&]+(\&|$)/'), '\1', $here), '&?');
			unset($args['co']);
			unset($args['codepromo']);
		}
		// certains param�tres doivent rendre la page non accessible
		foreach(array('logout','msg','co','sk','codepromo','comptepro') as $k)
		{
			if ($args[$k])
			{
				$this->noIndex();
				break;
			}
		}
		// si page index�e, on pr�cise la r�partiation entre SERVER_RESA et SERVER_MOBI
		if (!$this->isNoIndex() && SERVER_RESA != SERVER_MOBI) {
			if (SERVER_NAME == SERVER_RESA) {
				// sur SERVER_RESA on indique � Google qu'il existe une version mobile
				$this->addCanonical($here);
				$url_mobile = str_replace(SERVER_NAME, SERVER_MOBI, $here);
				$this->addLink(array('rel' => 'alternate', 'media' => 'only screen and (max-width: 640px)', 'href' => $url_mobile));
				$this->setData('url_mobile', $url_mobile);
			} else if (SERVER_NAME == SERVER_MOBI) {
				// sur SERVER_MOBI on indique � Google la version d'origine
				$url_desktop = str_replace(SERVER_MOBI, SERVER_RESA, $here);
				$this->addCanonical($url_desktop);
				$this->setData('url_desktop', $url_desktop);
			}
		}
		return true;
	}
	/**
	* V�rifie si la page doit �tre redirig�e
	*/
	protected function check301()
	{
		$x = Page::getRequestURI();
		if ($x != 'index.php' && $x != '301.php' && basename($_SERVER['SCRIPT_NAME'])!='301.php')
			return;
		if (!$_GET['page']) return;
		$page = $this->getId();
		if ($page == 'agences/liste' || $page == 'agences/agence' || $page=='promo/index')
		{
			if ($_GET['agence'] && Agence::isValidID($_GET['agence'])) {
				$row = mysql_fetch_assoc(sqlexec("select canon from agence_alias where id='".addslashes($_GET['agence'])."'"));
			} else if ($_GET['dept'] && preg_match('/^(\d{2}|2A|2B)$/', $_GET['dept'])) {
				$row = mysql_fetch_assoc(sqlexec("select canon from dept where id='".addslashes($_GET['dept'])."'"));
			} else if ($_GET['zone'] && preg_match('/^[a-z]{2}$/', $_GET['zone'])) {
				$row = mysql_fetch_assoc(sqlexec("select canon from zone where id='".addslashes($_GET['zone'])."'"));
			}
			// selon la page
			if (($page == 'agences/liste' || $page=='agences/agence') && $row)
				$url = gu_agence($row);
			else if ($page == 'promo/index') // si !$row on obtient �galement une url valide
				$url = gu_promo ($row);
			else if ($x == '301.php' && $_GET['url'])
				$url = $_GET['url'];
		}
		else
			$url = $page.'.html';
		return self::getResaURL($url);
	}
	/* =======================================
	* HEADER ET AFFICHAGE
	======================================= */
	protected function _getCustomScripts()
	{
		return array('js' => array(), 'css' => array());
	}
	protected function prepareHead()
	{
		// CSS : 1 css pour le skin ou un ensemble de CSS "historiques"
		if (is_file(BP.'/css/'.$this->getSkin().'.css')) {
			$this->addCss('css/'.$this->getSkin().'.css');
		}
		$dir = dirname($this->getId());
		foreach(array($this->getSkin(), SKIN_DEFAULT) as $skin)
		{
			if (is_file(BP."/skin/$skin/$dir/$dir.css"))
			{
				$this->addCss("skin/$skin/$dir/$dir.css");
				break;
			}
		}

		// JS de base
		foreach (array('scripts/script.js','scripts/AC_OETags.js','scripts/sp_fct.js') as $js) {
			$this->addJS($js);
		}
		// pour la page maintenance on s'arr�te l�
		if ($this->isOffline())
			return;
		// rajouter des JS et CSS sp�cifiques
		$files = $this->_getCustomScripts();
		foreach($files['js'] as $file)
			$this->addJS($file);
		foreach($files['css'] as $file)
			$this->addCSS($file);

		// titre, description, h1
		if ($page = $this->getId())
		{
			// correction de quelques param�tres
			if (($page == 'demenagement/categorie' || $page == 'loisirs/categorie') && $_GET['id'])
				$_GET['categorie'] = (int) $_GET['id'];
			// pr�paration des arguments pour les "token" dans les Title et Description
			$args = array('agence'=>'', 'localisation' => '');
			if ($_GET['agence'] && Agence::isValidID($_GET['agence']))
			{
				$args = mysql_fetch_assoc(sqlexec("SELECT aa.nom agence, (case when NULLIF(latitude*longitude,0) IS NOT NULL THEN concat(degrees(latitude), ',', degrees(longitude)) ELSE null END) geoloc FROM agence_pdv ap JOIN agence_alias aa ON aa.pdv=ap.id WHERE aa.id='".addslashes($_GET['agence'])."'"));
				if (!$args['agence'])
				{
					// si on ne trouve pas l'agence
					$args = array('agence'=>'', 'localisation' => '');
					$this->noIndex();
				}
			}
			// pour la page agences/liste on doit avoir dept, zone et ville renseign�e
			foreach (array('zone','region','dept','agglo','emplacement','categorie', 'marque', 'modele') as $v) {
				$args[$v] = '';
				if (!empty($_GET[$v])) {
					if ($v == 'marque' && preg_match('/^[a-z\-]{1,32}$/i', $_GET[$v])) {
						$args[$v] = getsql(sprintf('SELECT nom FROM refnat_canon WHERE type="%s" AND id="%s"', $v, mysql_real_escape_string($_GET[$v])));
					} else if (in_array($v, array('dept','zone')) && preg_match('/^[0-9a-z]{2}$/i', $_GET[$v])) {
						$args[$v] = getsql(sprintf('SELECT nom FROM refnat_canon WHERE type="%s" AND id="%s"', $v, mysql_real_escape_string($_GET[$v])));
					} else if (preg_match('/^\d+$/', $_GET[$v])) {
						$args[$v] = getsql(sprintf('SELECT nom FROM refnat_canon WHERE type="%s" AND id="%ld"', $v, (int)$_GET[$v]));
					}
				}
			}
			$args['type'] = gu_type($_GET['type'], false, ($page=='annuaire/index' ? 'vehicule' : 'voiture'));
			// pr�ciser la localisation
			foreach (array('agence','agglo','dept','region','emplacement','zone') as $v)
			{
				if (!$args[$v]) continue;
				$args['localisation'] = $args[$v];
				break;
			}

			// traitements sp�cifiques pour certaines pages
			if ($page == 'agences/liste')
			{
				$args['ville'] = htmlspecialchars(strip_tags($_REQUEST['ville']));
				if ($args['ville'])
					$this->noIndex();
			}
			if ($page=='reservation/index')
			{
				// pour la page d'accueil sans arguments
				if (!$args['categorie'] && !$args['agence'] && !$_GET['type'])
				{
					$this->setTitre('Location voiture et utilitaire : ADA');
					$this->setDescription(META_DESCRIPTION);
				}
				else
				{
					if (!$args['categorie']) $args['categorie'] = $args['type'];
					// pour l'instant on n'indexe pas les pages de r�servation agence
					if (isset($args['agence']) || isset($args['categorie']))
						$this->noIndex();
					// si le type n'est pas corret
					if ($_GET['type'] && !Categorie::$TYPE[$_GET['type']])
						$this->noIndex();
					// d�sindexer /?type=vu
					if (strtolower($_GET['type'])=='vu' && !isset($_GET['categorie']))
						$this->noIndex(false);
				}
				if ($this->isNoIndex())
				{
					$this->setTitre('ADA : le sp�cialiste de la location');
					$this->setDescription('');
				}
			}

			// pr�parer le tableau de remplacement
			foreach ($args as $k => $v) {
				$this->_addSearchReplace($k, $v);
			}
		}

		// on ajoute les META
		if ($description = $this->getDescription())
			$this->addMeta('description', htmlspecialchars(strip_tags(nl2br($description))));
		if ($args['geoloc'])
		{
			$this->addMeta('ICBM', $args['geoloc']);
			$this->addMeta('geo.position', str_replace(',', ';', $args['geoloc']));
		}
		if ($this->isNoIndex()) {
			$this->addMeta('robots', $this->getNoIndex());
		}
		$this->addMeta('keywords', META_KEYWORDS);
		if ($page == 'reservation/index' || $page == 'annuaire/index' || dirname($page)=='zone')
		{
			// Google Webmaster Tools : contact@rnd.fr
			$this->addMeta('google-site-verification', 'cwdJQCZXaC51WIkPS8FDe2p_K3gCcKn-1u8FpvriDq0');
			// Google Webmaster Tools : contact-ada@rnd.fr
			$this->addMeta('google-site-verification', 'KME-Ww6gVXEVV03r2-O7ofmonDvyHFd-PWeySSX4z8Y');
			$this->addMeta('verify-v1', 'NNf1ZS8XBX4o+oiQDW0IcetvT9v7WoDGUGK9nnhtFc0=');
		}
	}
	/**
	* Rajoute une cl� / valeur pour le remplacement dans les textes H1, titre et description
	*
	* @returns null
	*/
	protected function _addSearchReplace($search, $replace)
	{
		if ( !in_array('/#'.$search.'#/', $this->_infoSearch) ) {
			$this->_infoSearch[] = '/#'.$search.'#/';
			$this->_infoReplace[] = $replace;
		}
	}
	/**
	* Renvoie un texte remplac� selon le contenu de _infoSearch/Replace
	*
	* @param string $str
	* @return string
	*/
	public function parseText($str)
	{
		if (!$str) return;
		return preg_replace(array_merge($this->_infoSearch, array('/\s+/')), array_merge($this->_infoReplace, array(' ')), $str);
	}
	public function getH1() { return $this->parseText($this->getData('h1')); }
	public function getDescription() { return $this->parseText($this->getData('description')); }
	/**
	* Renvoie l'univers associ�e � la page
	* @return string
	*/
	public function getUniversTitre() {
		return ucwords(dirname($this->getId()));
	}
	public function getTitre()
	{
		$titre = $this->parseText($this->getData('titre'));
		if (!$titre)
		{
			// un mot d�pendant de la page en cours d'affichage
			$titre = ucwords(strtolower(implode(' - ', explode('/', str_replace(array('/index','_','agences/agence', '/liste'), array('', ' ', 'agence', ''), $this->getId())))));
			//... et une phrase g�n�rique
			$titre .= ' - Location de voitures et de v�hicules utilitaires';
			$titre .= ' - ADA';
			// on enregistre le titre
			$this->setData('titre', $titre);
			$this->save();
		}
		return $titre;
	}
	public function writeHead()
	{
		// �crire les URLs en entier dans l'en-t�te
		$here = $this->getBaseURL(false);
		echo "\t<title>{$this->getTitre()}</title>\n";
		// meta
		echo "\t".'<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1"/>'."\n";
		foreach ($this->_meta as $name => $a)
		{
			foreach ($a as $content)
				echo "\t".'<meta name="'.$name.'" content="'.$content.'"/>'."\n";
		}
		// base
		if ($this->_baseHref)
		{
			echo "\t".'<base href="'.$this->getBaseURL().'"';
			if ($this->_basetarget)
				echo ' target="'.$this->_basetarget.'"';
			echo '/>'."\n";
		}
		// CSS et favicon
		echo "\t".'<link href="'.$here.'favicon.ico" type="image/x-icon" rel="shortcut icon"/>'."\n";
		foreach ($this->_css as $css) {
			if (substr($css, 0, 4) != 'http') {
				if (SITE_MODE != 'DEV') {
					$css.='?t='.filemtime(BP.'/'.$css);
				}
				$css = $here.$css;
			}
			echo "\t".'<link type="text/css" rel="stylesheet" media="screen" href="'.$css.'"/>'."\n";
		}
		foreach(array('skin/'.$this->getSkin().'/print.css','skin/'.SKIN_DEFAULT.'/print.css','css/print.css') as $css) {
			if (is_file(BP.'/'.$css)) {
				echo "\t".'<link type="text/css" rel="stylesheet" media="print"  href="'.$here.$css.'"/>'."\n";
				break;
			}
		}
		foreach($this->_links as $link) {
			echo "\t<link";
			foreach ($link as $k => $v)
				echo " $k=\"$v\"";
			echo "/>\n";
		}
		// Javascript
		if ($js = $this->_getGoogleAutoLoader())
			echo "\t".'<script type="text/javascript" src="'.$js.'"></script>'."\n";
		foreach ($this->_js as $js)
		{
			if (self::$_jsGoogleLoad[$js]) continue;
			if (substr($js, 0, 4) != 'http') {
				if (SITE_MODE != 'DEV') {
					$js .= '?t='.filemtime(BP.'/'.$js);
				}
				$js = $here.$js;
			}
			echo "\t".'<script type="text/javascript" src="'.$js.'"></script>'."\n";
		}
	}
	static protected function minifyInit()
	{
		if (self::$_initMinified) return;
		self::$_initMinified = true;
		self::$_useMinified = USE_MINIFIED;
		// fichiers JS � concatener :
		// $minified['nom_fichier_minifi�.js'] = array(liste des fichiers);
		$minified['scripts/lib.min.js'] = array(
			'scripts/script.js',
			'scripts/AC_OETags.js',
			'scripts/sp_fct.js',
		);
		$minified['scripts/lib.agence.min.js'] = array(
			'scripts/agence_liste.js',
			'scripts/jquery.tagdragon.min.js',
		);
		$minified['scripts/form_reservation.min.js'] = array(
			'scripts/form_reservation.js'
		);
		$minified['scripts/form_checker.min.js'] = array(
			'scripts/form_checker.js'
		);
		$minified['scripts/reservation_index.min.js'] = array(
			'scripts/carrousel.js',
			'scripts/jquery.slides.js'
		);
		// dans la variable statique
		self::$_toMinify = $minified;
		// on inverse le tableau pour faciliter les recherches
		foreach ($minified as $k => $a)
		{
			foreach ($a as $js)
				self::$_fromMinify[$js] = $k;
		}
	}
	static public function getFromMinify() { self::minifyInit(); return self::$_fromMinify; }
	static public function getToMinify() { self::minifyInit(); return self::$_toMinify; }
	static public function getMinifiedName($file)
	{
		self::minifyInit();
		if (self::$_useMinified && $x = self::$_fromMinify[$file])
			return $x;
		return $file;
	}

	public function addJS($js, $tryMin = true)
	{
		if (!$js) return;
		// traduction: jquery, gmap et minimisation
		if (self::$_jsGoogleLoad[$js]) {
			$js = $js;
		} else if ($js == 'jquery' || $js == 'jquery-ui') {
			$version = (isset(self::$_jqueryVersions[$this->getSkin()][$js]) ? self::$_jqueryVersions[$this->getSkin()][$js] : self::$_jqueryVersions['default'][$js]);
			if ($js == 'jquery-ui') {
				// ajouter la CSS associ�e
				$this->addCSS((isset(self::$_jqueryVersions[$this->getSkin()][$js.'-css']) ? self::$_jqueryVersions[$this->getSkin()][$js.'-css'] : self::$_jqueryVersions['default'][$js.'-css']));
			}
			$js = sprintf(self::getProtocol().'://ajax.googleapis.com/ajax/libs/%s/%s/%s.min.js', str_replace('-', '', $js), $version, $js);
		} else if ($tryMin) {
			$js = self::getMinifiedName($js);
		}

		// a-t-il d�j� �t� ajout� ?
		if (!in_array($js, $this->_js))
			$this->_js[] = $js;
		return;
	}
	protected function _getGoogleAutoLoader()
	{
		$x = array();
		foreach ($this->_js as $k)
		{
			if (self::$_jsGoogleLoad[$k])
				$x['modules'][] = self::$_jsGoogleLoad[$k];
		}
		if ($x)
			$autoload = '?autoload='.urlencode(json_encode($x)).'&';
		return self::getProtocol().'://www.google.com/jsapi'.$autoload;
	}
	public function addCSS($css, $tryMin = true)
	{
		if (!$css) return;
		if ($tryMin)
			$css = self::getMinifiedName($css);
		// a-t-il d�j� �t� ajout� ?
		if (!in_array($css, $this->_css))
			$this->_css[] = $css;
		return;
	}
	/**
	* ajoute un �l�ment link avec les attributs � d�finir
	*
	* @param array $attr
	*/
	public function addLink($attr)
	{
		if (is_array($attr))
			$this->_links[] = $attr;
		return $this;
	}
	/**
	* Add a canonical link
	*
	* @param string $url
	* @return Page
	*/
	public function addCanonical($url) {
		$this->addLink(array('rel' => 'canonical', 'href' => $url));
		return $this;
	}
	public function addMeta($name, $content)
	{
		$this->_meta[$name][] = $content;
	}
	static public function noIndex($nofollow = true)
	{
		if (!self::$_noindex)
		{
			self::$_noindex = true;
			self::$_nofollow = $nofollow;
		}
	}
	public function isNoIndex()
	{
		return self::$_noindex;
	}
	public function getNoIndex()
	{
		if (self::$_noindex)
		{
			$str = 'noindex,';
			$str.= (self::$_nofollow ? 'nofollow' : 'follow');
			return $str;
		}
		return;
	}
	public function setBaseTarget($target)
	{
		$this->_basetarget = $target;
	}
	/* ==========================================
	** INFO & DEBUG
	========================================== */
	public function writeInfo()
	{
		// passage du site en maintenance
		if (self::$_TIME_TO_GO && self::$_TIME_TO_GO > 0)
		{
			echo '<p align="center"><div style="width:100%; margin:40px 0px; background-color: red; color: white; font-size:18px; text-align:center;"><strong>';
			echo 'Maintenance du site dans '.strftime("%M mn et %S s", self::$_TIME_TO_GO).'.<br/> Nous vous remercions de revenir un peu plus tard.';
			echo '</strong></div></p>';
		}
		// mode t�l�-conseiller
		if ($_COOKIE["ADA001_TC_MODE"])
		{
			echo '<p align="center" id="tc_header">';
			echo '<div style="width:760px; margin:0px 0px; background-color: red; color: white; font-size:18px; text-align: center;">';
			echo 'SITE AGENT - '.$_COOKIE["ADA001_TC_USER"].' - <a href="'.self::getResaURL('tc/index.php?logout').'" style="color: white;">Retour au site ADA</a>';
			echo '</div></p>';
		}
	}
	// �crire un bloc d'acc�s � l'administration
	public function writeEditAdmin()
	{
		if ($this->_adminUser) {
			$this->_adminUser->writeEditAdmin($this->getId(), $_GET);
		}
	}
	// informations de mise au point
	function writeDebugInfo()
	{
		if (SITE_MODE == 'PROD')
			return;
		if (USE_PAYPAL && USE_PAYPAL_SANDBOX)
		{
			echo '<div style="width:100%; margin-top: 40px; background-color: red; color: white; font-size:18px">PAYPAL : Utilisation de la SandBox</div>';
			echo '<p>Vous devez <a href="https://developer.paypal.com/cgi-bin/devscr?cmd=_login-processing&login_cmd=_login-done&login_access=0" target="_blank">vous identifier sur la SandBox PayPal</a> (login: wdauchy@rnd.fr , mdp: ada-paypal)</p>';
			echo '<p>Voir le Wiki <a href="http://ada.rnd.fr/twiki/bin/view/Ada/EtudePaypal" target="_blank">pour plus d\'informations sur les comptes de tests</a></p>';
		}
		if (PBX_COMPTE != "ADAPROD")
			echo '<div style="width:100%; margin:10px 0px; background-color: red; color: white; font-size:18px">PAYBOX : '.PBX_COMPTE.'</div>';
		// debug SQL
		sqlprofile();
	}
	/**
	* Impl�mentation du tracking Google Analytics et AB Tasty
	*
	*/
	public function addGATracking()
	{
		$this->_gaTracking[] = func_get_args();
		return $this;
	}
	public function getGATracking() {
		return $this->_gaTracking;
	}
	public function addABTracking()
	{
		$this->_abTracking[] = func_get_args();
		return $this;
	}
	public function getABTracking() {
		return $this->_abTracking;
	}

	/**
	* Change une valeur pour le suivi des �v�nements sur le site
	*
	* @param mixed $name
	* @param string $value
	* @param string $module (nsp, publicidees, ...)
	*/
	public function setTrackInfo($name, $value = null, $module='nsp')
	{
		if (is_array($name))
		{
			foreach($name as $k => $v)
				$_SESSION['_track'][$module][$k] = $v;
		}
		else
			$_SESSION['_track'][$module][$name] = $value;
		return $this;
	}
	public function getTrackInfo($name, $module='nsp')
	{
		return $_SESSION['_track'][$module][$name];
	}
	/**
	* Pr�cise le nom de la page dans les statistiques
	*
	* @param mixed $page
	* @param mixed $append
	*/
	public function setTrackPage($page, $append = true)
	{
		if ($append)
			$page = $this->getTrackPage().'_'.$page;
		$this->_trackPage = $page;
	}
	public function getTrackPage()
	{
		return ($this->_trackPage ? $this->_trackPage : str_replace('/','_',$this->getId()));
	}
	// JavaScript des statistiques
	public function writeJSStats($hashKey = null)
	{
		if ($_COOKIE["ADA001_TC_MODE"])
			return;
		Tracking_SmartProfile::doTracking($this, $hashKey);
		Tracking_GoogleConversion::doTracking($this);
		Tracking_Bing::doTracking($this);
		Tracking_ABTasty::doTracking($this);
		Tracking_GoogleAnalytics::doTracking($this);
		Tracking_WebROI::doTracking($this);
		Tracking_Facebook::doTracking($this);

		// vider $_SESSION['_track']
		if (is_array($_SESSION['_track']))
			unset($_SESSION['_track']);
	}
	// pour l'affichage des pages et des visuels
	public function showBannieres($position = 'left')
	{
		Visuel_Collection::factory($this->getId(), $position)->showVisuels('fr_'.$this->getTrackPage());
	}

	// pour la gestion dynamique des menus
	public function getUniversMenu() {
		return array();
	}

	// pour la gestion dynamique des menus
	public function getUniversMenuAction() {
		$action = basename($this->_action);
		return $action;
	}

	protected function _getHtmlBlockPath($template)
	{
		return BP.'/app/Page/'.$template.'.php';
	}
}
?>
