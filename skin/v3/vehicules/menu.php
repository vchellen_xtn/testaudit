<?
	$menu = array(
		'tourisme'			=> 'Voitures de tourisme',
		'utilitaires'		=> 'Véhicules utilitaires',
		'sans_permis'		=> 'Voitures sans permis',
		'deux_roues'		=> 'Deux roues',
		'prestige'			=> 'Véhicules prestige',
		'vehicule_du_mois'	=> 'Véhicule du mois',
	);
	$action = basename($page);
?>
<ul class="menu type-li-6">
	<? foreach($menu as $k => $lbl) : ?>
		<li <?if ($action==$k) echo ' class="current"'; ?>>
			<a href="<?=$k?>.html"<?if ($action==$k) echo ' class="current"'; ?>><?=$lbl;?></a>
		</li>
	<? endforeach; ?>
</ul>