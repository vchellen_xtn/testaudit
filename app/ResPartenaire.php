<?php
/**
* Cr�� pour le partenariat avec WonderBox
* Association d'un code � envoyer avec un e-mail
*/

class ResPartenaire extends Ada_Object {
	
	/**
	* Renvoie le code partenaire associ� � cette r�servation
	* 
	* @param Reservation $reservation
	* @return ResPartenaire
	*/
	static public function factory(Reservation $reservation) {
		$x = new ResPartenaire();
		if ($id = $reservation->getId()) {
			$x->setIdFieldName('reservation');
			$x->load($id);
			$x->setIdFieldName('id');
		}
		return $x;
	}
	/**
	* Fournit � une r�servation un nouveau code s'il est disponible
	* 
	* @param string $partenaire		nom du partenaire
	* @param Reservation $reservation
	* @return ResPartenaire
	*/
	static public function create ($partenaire, Reservation $reservation) {
		// on v�rifie d'abord que la r�servation n'est pas d�j� associ�e � un code
		$x = self::factory($reservation);
		if (!$x || $x->isEmpty()) {
			// on v�rifie que la r�servation a bient �t� pay�e
			if (self::isValid($partenaire, $reservation)) {
				// on capture un code
				sqlexec(sprintf("UPDATE res_partenaire SET reservation=%ld, modification=NOW() WHERE partenaire='%s' AND reservation IS NULL ORDER BY id LIMIT 1", (int)$reservation->getId(), mysql_escape_string($partenaire)));
				// si on a r�ussi on va maintenant pouvoir r�cup�rer un code
				$x = self::factory($reservation);
			}
		}
		return $x;
	}

	/**
	 * Associe un coupon code partenaire � une r�servation
	 * @param \Reservation $reservation
	 */
	static public function createPartenaires(Reservation $reservation) {
		foreach(array('restopolitan','wonderbox') as $partenaire) {
			$resPartenaire = self::create($partenaire, $reservation);
			if ($resPartenaire && !$resPartenaire->isEmpty()) break;
		}
		return $resPartenaire;
	}
	/**
	* La r�servation est-elle valide pour ce partenaire
	* 
	* @param string $partenaire
	* @param Reservation $reservation
	*/
	static public function isValid($partenaire, Reservation $reservation) {
		if ($reservation && $reservation['statut']=='P') {
			if ($partenaire == 'wonderbox') {
				// les coupons sont valables jusqu'au 31/12/2016
				// http://my.rnd.fr/issues/8147
				if (time() < strtotime('2016-12-31')) {
					return true;
				}
			}
			if ($partenaire == 'restopolitan') {
				// Cette offre est valable du 31/01/2016 au 26/02/2016
				if (time() >= strtotime('2016-01-13') && time() <= strtotime('2016-02-29')) {
					// V�rifie que l'offre s'applique que sur les VP et que le forfait concern� soit celui du Weekend
					if (strtolower($reservation['type']) == 'vp' && in_array($reservation['forfait'], array('WEVP1K25', 'WEVP2K50','WEVP2_3K75','WEVP3K100'))) {
						// V�rifie que la zone agence de la r�servation soit la France (fr) ou la Corse (co)
						$agence = $reservation->getAgence();
						if (!empty($agence['zone']) && ($agence['zone'] == 'fr' || $agence['zone'] == 'co')) {
							// V�rifie que la date de r�servation soit comprise dans l'intervalle de la dur�e de l'offre
							if ($reservation->getDebut('U') >= strtotime('2016-02-12') && $reservation->getDebut('U') <= strtotime('2016-02-29')) {
								return true;
							}
						}
					}
				}
			}
		}
		return false;
	}
	
	/**
	* REnvoie une cha�ne de caract�res � ins�rer dans un e-mail
	* @returns string
	*/
	public function toEmailBlock() {
		$output = '';
		if (!$this->isEmpty())  {
			$output = $this->toHtml('block-email-'.$this->_data['partenaire']);
		}
		return $output;
	}
	
	
}
?>
