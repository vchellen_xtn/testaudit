<?
/**
* afiche facture et avoir � partir de Facturation 
* includt depuis toHtml()
*/
	/** @var Facturation */
	$x = $this;
	/** @var Reservation */
	$reservation = $x->getReservation();
	/** @var Agence */
	$agence = $reservation->getAgence();
	/** @var Categorie */
	$categorie = $reservation->getCategorie('id,mnem,nom');
	/** @var Client */
	$client = $reservation->getClient();
?>
		<div class="printtitre"><img src="../img/logo.gif" alt="ADA" title="ADA" /></div>
					<p class="droite impr"><a href="../client/compte.html"><img src="../img/bt_retour.gif" alt="retour" title="retour" /></a></p>
					<p class="clear"></p>

					<!-- ADA -->
					<div class="droite" style="width: 250px;">
						<div class="gauche corner_top_left">&nbsp;</div>
						<div class="droite corner_top_right" style="width: 7px;">&nbsp;</div>
						<div class="contentShadowBox_in" style="padding: 10px;">
							<strong><?=$x->getSociete()?>
							<br/>22-28 rue Henri Barbusse
							<br/>92585 CLICHY CEDEX</strong>
						</div>

						<div class="footerShadowBox2">
							<div class="gauche angleShadow_bl"><img src="../img/z.gif" height="8" alt="" /></div>
							<div class="droite angleShadow_br"><img src="../img/z.gif" height="8" alt="" /></div>
						</div>
					</div>
					<!-- ADA - fin -->

					<!-- bloc principal -->
					<div style="width: 535px; margin: 0 auto;">
						<div class="contentShadowBox_in2" style="width: 534px;">
							<div class="topShadowBox" style="padding: 10px;">
								<!-- client -->
								<div class="imprbord" style="width: 150px;">
									<h3><img src="../img/stitre_client.gif" alt="Client" title="Client"/><span>Client</span></h3>
									<strong><?=$client['prenom'].' '.$client['nom']?>
									<br/><?=$client['f_adresse']?>
									<br/><?=$client['f_cp'].' '.$client['f_ville']?>
									<br/><?=$client['f_pays_nom']?>
									</strong>
								</div>
								<br/>
								<br/>
								
								<!-- pointill� -->
								<div class="pointille" style="width: 250px;"><img src="../img/z.gif" alt="" /></div>

								<!-- r�f�rence facture ou avoir -->
								<dl>
									<dd class="stitre">
										<strong><?=$x->getType()?> N�</strong>
										<span class="nongras"> <?=$x->getNumber()?></span>
									</dd>
									<dd>
										<strong>Date</strong> <?=$x->getDate('d-m-Y')?>
									</dd>
								</dl>

								<!-- pointill� -->
								<div class="pointille" style="width: 250px;"><img src="../img/z.gif" alt="" /></div>

								<!-- tarif de la r�servation -->
								<? if ($x->getData('facturation')=='ADA') { ?>
								<div class="droite rouge imprprix" style="background-color: transparent; width: 60px;">
									<strong><?=$reservation->getPrixForfait()?>&nbsp;�</strong>
								</div>
								<? } ?>

								<!-- r�f�rence de la r�servation -->
								<dl>
									<dd class="stitre">R�servation Internet n� <span class="nongras"> <?=$reservation->getResIDClient()?></span></dd>
									<dd class="stitre">Agence :<span class="nongras"> <?=$agence->getHtmlAddress(', ', 'nom,adresse1,cp_ville')?></span></dd>
									<dd class="stitre"><?=($x['facturation']=='ada' ? 'Location' : 'P�riode garantie')?> :<span class="nongras"> du <?=$reservation->getDebut()?> au <?=$reservation->getFin()?></span></dd>
								</dl>
								<!-- r�f�rence du v�hicule -->
								<dl style="width: 250px">
									<dd class="stitre">V�hicule : <span class="nongras">Cat�gorie <?=$categorie['mnem'].' '.$categorie['nom']?></span></dd>
									<dd class="stitre">Kilom�tres inclus : <span class="nongras"><?=$reservation->getKm()?></span></dd>
								</dl>

								<!-- pointill� -->
								<div class="pointille" style="width: 250px;"><img src="../img/z.gif" alt="" /></div>
								
								<!-- Taxe Gare / A�roport -->
								<? if ($x['facturation']=='ada' && $reservation['surcharge']) { ?>
								<div class="droite rouge imprprix" style="width: 50px;">
									<strong><?=$reservation["surcharge"]?>&nbsp;�</strong>
									<p class="clear"></p>
								</div>
								<dl style="width: 350px;">	
									<dd class="stitre"><?=$agence->getSurchargeLabel()?> :</dd>
								</dl>
								<? } ?>
								<dl style="width: 350px;">
									<dd class="stitre"><?=($x['facturation']=='ada' ? 'Option(s) r�gl�e(s) en ligne' : "Garantie(s) d'assurance souscrite(s)")?> :
<?
		/*
		** AFFICHAGE DES OPTIONS
		*/
		$options = $reservation->getResOptions()->filter($x['facturation'],'L');
		if ($options->isEmpty())
			echo '<span class="nongras">Aucune</span>';
?>
									</dd>
								</dl>
<?
		foreach($options as /** @var ResOption */ $option)
		{
?>
								<!-- bloc prix option - debut -->
								<div class="droite rouge imprprix" style="width: 50px;">
									<strong><?=$option['prix']?>&nbsp;�</strong>
									<p class="clear"></p>
								</div>
								<dl style="width: 400px;">
									<dd class="case" style="width: 400px;"><?=$option['nom']?></dd>
								</dl>
<?
		}
?>
								<!-- bloc prix total - debut -->
								<p class="clear"></p>
								<br/>
								<br/>
								<div class="droite imprbord" style="width: 265px;">
									<table cellpadding="0" cellspacing="0" border="0" width="250">
										<colgroup>
											<col width="140">
											<col width="110">
										</colgroup>
<?
		if ($x['facturation'] == 'ada')
		{
			$facture = ($x['type'] == 'F' ? $x : $reservation->getFacturations()->find('ada','F'));
?>
									<tr>
										<td valign="bottom" class="totaldetail2">Total HT:</td>
										<td align="right" class="petitprixrouge"><?=sprintf("%01.2f", $facture->getMontantHT())?>&nbsp;�</td>
									</tr>
									<? if ($x['type']=='A') { // d�but avoir ?>
									<tr>
										<td valign="bottom" class="totaldetail2">Frais de dossier HT:</td>
										<td align="right" class="petitprixrouge"><?=sprintf("%01.2f", $facture->getMontantHT() - $x->getMontantHT())?>&nbsp;�</td>
									</tr>
									<tr>
										<td valign="bottom" class="totaldetail2" style="letter-spacing: -1px;">Remboursement HT:</td>
										<td align="right" class="petitprixrouge"><?=sprintf("%01.2f", $x->getMontantHT())?>&nbsp;�</td>
									</tr>
									<? } // fin avoir ?>
									<tr>
										<td valign="bottom" class="totaldetail2">TVA <?=Tva::format($x->getTVARate());?>:</td>
										<td align="right" class="petitprixrouge"><?=sprintf("%01.2f", $x->getTVA())?>&nbsp;�</td>
									</tr>
									<tr>
										<td class="totaldetail2" style="letter-spacing: -1px;">Total<?=($x['type']=='A' ? ' remboursement ':' ')?>TTC :</td>
										<td align="right" valign="bottom" class="prixrouge"><?=sprintf("%01.2f", $x->getMontant())?>&nbsp;�</td>
									</tr>
<?
		}
		else if ($x['facturation'] == 'courtage')
		{
?>
									<tr>
										<td class="totaldetail2" style="letter-spacing: -1px;">Cotisation(s) d'assurance TTC :</td>
										<td align="right" valign="bottom" class="prixrouge"><?=sprintf("%01.2f", $x->getMontant())?>&nbsp;�</td>
									</tr>
									<tr>
										<td colspan="2" class="totaldetail2">(taxe d'assurance 9% inclus)</td>
									</tr>
<?
		}
?>
									</table>
								</div>
								<!-- bloc prix total - fin -->

								<p class="clear"></p>
								<br />
								<div class="pointille" style="width: 490px;"><img src="../img/z.gif" alt="" /></div>
								<br />
								<? if ($x['type'] == 'F') { ?>
								La totalit� de ce montant a d�j� �t� r�gl� en ligne par carte bancaire le <?=$x->getDate('d-m-Y')?>
								<? } else { ?>
								Ce montant a d�j� �t� rembours� par virement le <?=$x->getDate('d-m-Y')?>
								<? } ?>
								<br/><br/>

								<!-- pointill� -->
								<div class="pointille" style="width: 490px;"><img src="../img/z.gif" alt="" /></div>

								<br />
								<div class="droite imprbord" style="width: 265px;">
									<table cellpadding="0" cellspacing="0" border="0" width="245">
										<colgroup>
											<col width="170">
											<col width="75">
										</colgroup>
									<tr>
										<td align="right" valign="bottom" class="totaldetail2">D�j� <?=($x['type']=='F' ? 'r�gl�' : 'rembours�')?> TTC :</td>
										<td align="right" class="rouge"><strong><?=sprintf("%01.2f", $x->getMontant())?>&nbsp;�</strong></td>
									</tr>
									<tr>
										<td align="right" valign="bottom" class="totaldetail2">Solde d� :</td>
										<td align="right" class="rouge"><strong><?=sprintf("%01.2f", 0)?>&nbsp;�</strong></td>
									</tr>
									<tr>
										<td align="right" class="totaldetail2">Net � payer en euros TTC :</td>
										<td align="right" valign="bottom" class="rouge"><strong><?=sprintf("%01.2f", 0)?>&nbsp;�</strong></td>
									</tr>
									</table>
								</div>
								<p class="clear"></p>
							</div>
						</div>
						<div class="footerShadowBox2">
							<div class="gauche angleShadow_bl"><img src="../img/z.gif" height="8" alt="" /></div>
							<div class="droite angleShadow_br"><img src="../img/z.gif" height="8" alt="" /></div>
						</div>
					</div>
					<p class="clear"></p>
					<br />
<?
// INFORMATIONS REGLEMENTAIRES
if ($x['facturation']=='courtage') { ?>
<div style="font: 8px arial; color: #585858; width: 500px; margin: 0px 120px 20px 120px;">
	<div style="text-align: center; color: inherit;">
	ADA COURTAGE soci�t� de courtage en Assurances - 22/28 RUE HENRI BARBUSSE 92110 CLICHY
	<br/>SARL UNIPERSONNELLE au capital de 7 500 �
	<br/>515 319 671 RCS NANTERRE - N�TVA intracommunautaire FR88515319671
	<br/>N� ORIAS : 09 052 988 www.orias.fr - APE 6622 Z
	</div>
	<br/>
	<div style="text-align: justify; color:inherit;">
	<br/>Les garanties sont g�r�es par le groupe VERSPIEREN, Soci�t� de Courtage en Assurances domicili� au 1, avenue Fran�ois Mitterrand 59290 Wasquehal � SA � Directoire et Conseil de Surveillance au capital de 1 000 000� - SIREN n� 321 502 049 � RCS de Roubaix Tourcoing � N� ORIAS : 07 001 542 www.orias.fr.
	<br/>Le contrat d�assurance "remboursement de franchise" n�201016FR4281 est souscrit aupr�s de AMG Assurances � 25 rue de Li�ge, 75008 PARIS � RCS Paris 499 589 190 � N� ORIAS 07 036 604, agence de souscription en assurances dommages, d�ment habilit�e � agir pour le compte de GOTHAER Allgemeine Versicherung AG, Gothaer allee 1 � D-50969 KLN � Soci�t� Mutuelle d�assurances � cotisations fixes � registre du Commerce de Cologne HRB 35474, num�ro CCA VU 5531.
	<br/>Le contrat d�assurance "marchandises transport�es et effets personnels" n�91-701 est souscrit aupr�s de HELVETIA, Compagnie Suisse d�Assurances, Soci�t� anonyme de droit suisse au capital de 77 480 000 francs suisses enti�rement lib�r�, immatricul�e sous le n�CH � 320.3.001.013.8 � si�ge social : 10 Dufourstrasse, Saint Gall, Suisse. Cette compagnie est repr�sent�e par sa succursale fran�aise, Helvetia Assurances, Direction pour la France, 2 rue Sainte-Marie 92415 Courbevoie Cedex, entreprise priv�e r�gie par le Code des Assurances, immatricul�e au RCS de Nanterre sous le n�775 753 072. SIRET : 775 753 072 00112 � APE : 660 E
	<br/>VERSPIEREN, ADA COURTAGE, AMG et HELVETIA sont soumises au contr�le de l'Autorit� de Contr�le Prudentiel (ACP)
	</div>
</div>
<? } // !fin des mentions ADA COURTAGE ?>

					<div class="gauche impr">
						<a href="javascript:window.print()">
						<? if ($x['type']=='F') { ?>
							<img src="../img/bt_imprimezfacture.gif" alt="Imprimez votre facture" title="Imprimez votre facture"/>
						<? } else { ?>
							<img src="../img/bt_imprimezavoirs.gif" alt="Imprimez votre avoir" title="Imprimez votre avoir"/>
						<? } ?>
						</a>
					</div>
					<div class="gauche impr" style="margin-left: 150px;"><a href="../client/compte.html"><img src="../img/bt_vosreservations.gif" alt="Vos r�servations" title="Vos r�servations" /></a></div>

					<br />
					<br />

					<div class="print" style="text-align: center;">
						ADA - 22/ 28 rue Henri Barbusse 92585 Clichy Cedex<br />
						S.A. au capital de 2 543 619,76 �<br />
						338 657 141 8CS Nanterre - N� TVA Intracommunautaire FR31338657141
					</div>