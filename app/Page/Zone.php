<?php
class Page_Zone extends Page
{
	/** @var Zone */
	protected $_zone;
	
	/**
	* Renvoie les agences de la Zone
	* @returns Agence_Collection
	*/
	public function getAgences()
	{
		return Agence_Collection::factory(array('zone'=>$this->_zone['id']));
	}
	
	/**
	* Renvoie la zone sélectionnée
	* @returns Zone
	*/
	public function getZone()
	{
		return $this->_zone;
	}
	
	
	public function indexAction()
	{
		// vérification sur la zone
		if (!$this->_zone || (SITE_MODE!='DEV' && !in_array(SERVER_NAME, array($this->_zone['id'].'.ada.lbn.fr', $this->_zone['canon'].'.ada.fr'))) )
		{
			self::redirect(self::getURL(null, SERVER_RESA));
			return;
		}
		
		// ajouter le JS et le CSS nécessaire
		$this->addJS('scripts/reservation.js');
		$this->addJS('scripts/agence_liste.js');
		$this->addJS('scripts/carrousel.js');
		$this->hideLinkTop();
	}
	
	// guadeloupe
	public function gpAction()
	{
		$this->_zone = Zone::factory('gp');
		/* 2012-09-28 : on redirige vers la page du site principal */
		self::redirect(self::getURL(gu_agence($this->_zone), SERVER_RESA), 301);
		return;
		$this->indexAction();
	}
};
?>
