<?
	require_once("secure.php"); 
	require_once("lib.php");

	// identifiant
	$id = (int) getarg("id");
	
	// suppression
	if ($id && getarg('delete')) 
	{
		$sql = "delete from agence_dispo where id = '".$id."'";
		getsql($sql);
		unset($id);
	}
	
	// cr�ation ou modification
	if ($_POST["update"] || $_POST["create"])  {
		if ($_POST['create'])
		{
			if (!$id)
			{
				$id = getsql(sprintf("select id from agence_dispo where agence='%s' and categorie=%ld", filter_var($_POST['agence'], FILTER_SANITIZE_STRING), $_POST['categorie']));
				if ($id)
					$_POST['id'] = $id;
			}
			$_POST['type'] = getsql('select type from categorie where id='.(int)$_POST['categorie']);
		}
		save_record ('agence_dispo', $_POST);
		if (!$id) $id = mysql_insert_id();
	}
	// chargement
	if ($id)
	{
		$rs = sqlexec("select * from agence_dispo where id = '".$id."' ");
		if (!$rs || !($row = mysql_fetch_assoc($rs)))
			die("Aucun enregistrement correspondant.");
	}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">

	<script type="text/javascript" language="javascript" src="../scripts/check.js"></script>
	<script>
	<!-- //

	// doDelete
	function doDelete(frm)
	{
		return (confirm("Voulez-vous r�ellement supprimer cet �l�ment ?\nCette op�ration est irr�versible."));
	}
	
	// doSubmit
	function doSubmit(frm)
	{
		var err = "";
		if (!ValidateSelect(frm.agence))
			err += "Veuillez pr�ciser une agence!\n";
		if (!ValidateSelect(frm.categorie))
			err += "Veuillez pr�ciser une cat�gorie!\n";
		if (!frm.nb.value.match(/^\d+$/))
			err += "Veuillez pr�ciser le nombre de v�hicules disponible!\n";
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		return true;
	}
	// -->
	</script>
</head>

<body>

<form action="" method="post" onsubmit="return doSubmit(this)">
<input type="hidden" name="id" value="<?=$id?>">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre">
	<img src="img/puce.gif" align="absmiddle" hspace="1">
<?
	echo ($id ? "Disponibilit� (#$id)- ".$row["agence"] : "Nouvelle dispo");
?>
	</th>
</tr>
<tr>
	<td>
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="15%">
		<col width="35%">
		<col width="15%">
		<col width="35%">
		<tr>
			<td><label for="agence">Agence</label></td>
			<td>
<? 
	$agence = ($id ? $row["agence"] : $_GET["agence"]);
	$sql = "select id, concat(id, '-', nom) from agence where zone='".$_SESSION['ADA001_ADMIN_ZONE']."'";
	if ($agence) $sql.=" and id='".$agence."'";
	$sql.=" order by id";
	selectsql("agence", $sql, $agence);
?>
			</td>
			<td colspan="2">
			<?
			if ($agence)
				echo '<a href="agence.php?id='.$agence.'">'.$agence.'</a>';
			?>
			</td>
		</tr>
		<tr>
			<td><label for="categorie">Cat�gorie</label></td>
			<td>
<?
	$sql = "select id, concat(type, ' - ', mnem, ' - ', nom) libelle from categorie where zone='fr' and publie=1";
	if ($row['categorie']) $sql.=' and id='.$row['categorie'];
	$sql.=" order by libelle";
	selectsql("categorie", $sql, $row["categorie"]);
?>
			</td>
			<td><label for="nb">Quantit�</label></td>
			<td><input type="text" id="nb" name="nb" value="<?=$row["nb"]?>"></td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td align="right">
		<input type="submit" name="<?=$id ? "update" : "create"?>" value="enregistrer">
<?
	if ($id) 
	{
?>
		&nbsp;
		<input type="submit" onclick="return doDelete(this.form);" name="delete" value="supprimer">
<?
	}
?>
	</td>
</tr>
</table>
</form>

</body>
</html>
