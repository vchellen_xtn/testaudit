<?
class Page_Annuaire extends Page_Reservation
{
	/** @var array $_refnat va stocker toutes les valeurs canon [param�tre][id] et refnat les valeurs pour le r�f�rencement naturel */
	protected $_refnat = array();
	/** @var array $_params contient tous les �l�ments utilies pass�es depuis le $_GET */
	protected $_params = array();
	
	/** @var VehiculeModele */
	protected $_modele = null;
	/** @var VehiculeMarque */
	protected $_marque = null;
	/** @var VehiculeType */
	protected $_vehiculeType;
	
	protected static $_STEPS = array(
		1 => array(
			'step'	=> 1,
			'label' => 'Type de v�hicule',
			'title' => 'Quel type de v�hicule recherchez vous ?',
			'title_back' => 'Tous les types de v�hicules',
			'list' => 'type',
		),
		2 => array(
			'step'	=> 2,
			'label' => 'Marque',
			'title' => 'Quelle marque de v�hicule recherchez vous ?',
			'title_back' => 'Toutes les marques',
			'list' => 'marque',
		),
		3 => array(
			'step'	=> 3,
			'label' => 'Mod�le',
			'title' => 'Quel mod�le de v�hicule recherchez vous ?',
			'title_back' => 'Tous les mod�les',
			'list' => 'modele',
		),
		4 => array(
			'title' => 'Pr�cisez votre agence et vos dates de r�servation',
		)
	);
	
	
	/**
	* Analyse des para�mtres appel� par Page::factory()
	* 
	*/
	protected function checkParameters()
	{
		// appeler la version de base
		if (!parent::checkParameters()) {
			return false;
		}
		
		// pour l'annuaire remplir le $_GET � partir de $_GET[url] et on ne prend pas d'autres param�tres
		if ($_GET['url']) {
			$a = explode('_', $_GET['url']);
		}
		// on remetz � z�ro le $_GET
		$_GET = array();
		if (!$a) {
			return;
		}
		$refnat = array(); $where = array();
		foreach ($a as $v) {
			if (!preg_match('/^[a-z0-9\-]+$/i', $v)) continue;
			$where[] = "'".mysql_real_escape_string($v)."'";
		}
		if (!$where) return;
		$rs = sqlexec ($sql = 'select type, id, canon, nom from refnat_canon where canon in ('.join(',', $where).") and type not in ('agglo','emplacement')");
		while ($row = mysql_fetch_assoc($rs)) {
			$this->_params[$row['type']] = $row['id'];
			$refnat[$row['type']][$row['id']] = $row;
		}
		
		// si pas de marque ou de mod�le et si une zone, un d�partement ou une agence sont indiqu�es on renvoie sur le site RESA
		if (!isset($refnat['marque']) && !isset($refnat['modele'])) {
			foreach(array('agence','dept','region','zone') as $type) {
				if (!isset($refnat[$type])) continue;
				if ($type == 'region' || ($type == 'zone' && $this->_params[$type] == 'fr')) {
					self::redirect(self::getURL('agences/index.html', SERVER_RESA));
				} else {
					self::redirect(self::getURL(gu_agence($refnat[$type][$this->_params[$type]]), SERVER_RESA));
				}
				return;
			}
		}
		
		// on ne conserve pas les cat�gories et inforamtions g�ogrpahiques
		foreach(array('categorie','agence','dept','region','zone') as $k) {
			unset($refnat[$k], $this->_params[$k]);
		}
		
		// si on a pas obtenu tous les identifiants on renvoie vers l'URL la plus proche
		// avec le acas particulier de /location_vehicule pour afficher la page d'accueil
		$cnt = count($a);
		if (in_array('vehicule', $a)) {
			$cnt--;
		}

		// v�rifier que tous les param�tres sont r�cup�r�s sinon on indique que la page n'existe pas
		if ($cnt != count($refnat)) {
			// sinon on rediriger vers l'URL la plus proche...
			$url = Page::getURL(gu_annuaire($this->_params, $refnat), SERVER_ANNU);
			self::redirect($url);
			return;
			/* ou on ne les indexe pas... 
			self::set404();
			*/
		}
		// et la marque par rapport au mod�le
		if ($this->_params['modele']) {
			$this->_params['marque'] = getsql(sprintf("select marque from vehicule_modele where id='%s'", mysql_real_escape_string($this->_params['modele'])));
		}
		// on enregistre dans le $_GET pour la gestion des titres...
		$_GET = $this->_params;
		return true;
	}
	
	/**
	* Renvoyer les STEPS ('key' => array('step' =>, 'title' => , 'title_back' => ))
	* @returns array
	*/
	public function getSteps() {
		return self::$_STEPS;
	}
	
	/**
	* Renvoie l'�tape courante
	* @returns int
	*/
	public function getCurrentStep() 
	{
		$step = 1;
		foreach(array_reverse(self::$_STEPS, true) as $step => $a) {
			if (isset($a['list']) && isset($this->_params[$a['list']])) {
				return (1 + $step);
			}
		}
		return $step;
	}
	
	/**
	* Renvoie un VehiculeModele
	* @returns VehiculeModele
	*/
	public function getVehiculeModele()
	{
		if (!$this->_modele && isset($this->_params['modele'])) {
			$this->_modele = VehiculeModele::factory($this->_params['modele']);
		}
		return $this->_modele;
	}
	
	/**
	* Renvoie un VehiculeMarque
	* @returns VehiculeMarque
	*/
	public function getVehiculeMarque()
	{
		if (!$this->_marque && isset($this->_params['marque'])) {
			$this->_marque = VehiculeMarque::factory($this->_params['marque']);
		}
		return $this->_marque;
	}
	/**
	* Renvoie le VehiculeType associ�e � la page
	* @returns VehiculeType
	*/
	public function getVehiculeType() 
	{
		if (!$this->_vehiculeType && isset($this->_params['type'])) {
			$this->_vehiculeType = VehiculeType::factory($this->_params['type']);
		}
		return $this->_vehiculeType;
	}
	/**
	* Renvoiel'objet selon le type pr�cis�
	* 
	* @param mixed $key
	* @return VehiculeModele
	*/
	public function getVehiculeObject($key)
	{
		switch($key)
		{
			case 'type':
				return $this->getVehiculeType();
			case 'marque':
				return $this->getVehiculeMarque();
			case 'modele':
				return $this->getVehiculeModele();
		}
		return null;
	}
	
	
	/**
	* V�rifie si un param�tre est enregistr�
	* 
	* @param string $key
	*/
	public function hasParam($key) 
	{
		return isset($this->_params[$key]);
	}
	
	
	/**
	* G�re la page de l'annuaire
	* 
	*/
	public function indexAction()
	{
		// on renseigne la variable de tracking
		$x = array();
		foreach (array('type', 'marque', 'modele') as $k) {
			if (!$this->_params[$k]) continue;
			$x[] = $k;
		}
		$this->setTrackInfo('annuaire', join('_', $x));
	}
	
	/**
	* Renvoie l'URL pour revenir � l'�tape correspondante
	* 
	* @param string $key une des key de self::$_STEPS
	*/
	public function getUrlToStep($key) {
		$p = $this->_params;
		switch($key) {
			case 'type':
				$p = array();
				break;
			case 'marque':
				unset($p['marque'], $p['modele']);
				break;
			case 'modele':
				unset($p['modele']);
				break;
		}
		return gu_annuaire($p, $this->_refnat);
	}

	/** writeList : �crit une liste dans l'annuaire
		$sel	le param�tre en cours d'affichage
	*/
	public function writeList ($sel)
	{
		// d�terminer ce que l'on fiat en fonction du $sel
		switch($sel)
		{
			case 'type':
				$sql = "select distinct t.id, t.nom, t.canon, t.refnat from vehicule_type t";
				if ($this->_params['modele'])
					$sql.= " join vehicule_modele m on (m.type=t.id and m.id=".(int)$this->_params['modele']." and m.publie=1)";
				else if ($this->_params['marque'])
					$sql.= " join vehicule_modele m on (m.type=t.id and m.marque='".mysql_real_escape_string($this->_params['marque'])."' and m.publie=1)";
				$sql.= " where t.id in ('vp','vu')";
				$sql.=" order by nom desc";
				break;
			case 'marque':
				if (!$this->_params['type']) return;
				$sql = "select distinct mq.id, mq.id nom, mq.canon, mq.refnat from vehicule_marque mq join vehicule_modele m on mq.id=m.marque";
				$sql.=" where m.publie=1";
				if ($this->_params['type'])
					$sql.=" and m.type='".mysql_real_escape_string($this->_params['type'])."'";
				$sql.=" order by m.marque";
				break;
			case 'modele':
				if (!$this->_params['marque']) return;
				$sql = "select distinct m.id, m.nom, m.canon, m.refnat";
				$sql.=" from vehicule_modele m";
				$sql.=" where m.marque='".mysql_real_escape_string($this->_params['marque'])."' and m.publie=1";
				if ($this->_params['type'])
					$sql.=" and m.type='".mysql_real_escape_string($this->_params['type'])."'";
				$sql.=" order by m.nom";
				break;
			default:
				return;
		}
		// finaliser
		$rs = sqlexec($sql);
		$cnt = mysql_num_rows($rs);
		if (!$cnt) return;
		$out = '';
		$out.='<div class="annu_sel annu_sel_'.$sel.'">'."\n";
		$out.="<ul>\n";
		while ($row = mysql_fetch_assoc($rs))
		{
			$this->_refnat[$sel][$row['id']] = $row;
			// pour auto-s�lectionner mais du coup certaines pages n'existent pas 
			//if ($cnt == 1) $params[$sel] = $row['id'];
			// pour pr�ciser l'affichage du formulaire en renseignant $_GET
			if ($cnt == 1) $x = $row['id'];
			if ($this->_params[$sel] && $this->_params[$sel] != $row['id']) continue;
			$out.='<li id="'.sprintf('annu_sel_%s_%s', $sel, $row['id']).'">';
			if ($row['id'] != $this->_params[$sel])
				$out.='<a href="../'.gu_annuaire (array_merge($this->_params, array($sel=>$row['id'])), $this->_refnat).'">';
			$out.=$row['nom'];
			if ($row['id'] == $this->_params[$sel])
				$out.='<a rel="nofollow" href="../'.gu_annuaire ($this->_params, $this->_refnat, $sel).'"><sup>X</sup>';
			$out.="</a></li>\n";
		}
		$out.="</ul>\n";
		$out.="</div>\n";
		// on compl�te $_GET et $this->_params si n�cessaire
		if (!$x) 
			$x =  $this->_params[$sel];
		if ($x && !$this->_params[$sel] && $sel == 'type') {
			$this->_params[$sel] = $x;
		}
		return $out;
	}
	/** writeDivRefnat : �crit un bloc de texte pour le r�f. nat.
	**	$a		une liste de param�tres � consulter dans le tableau refnat, on s'arr�te au premier trouv�
	*/
	public function writeDivRefnat ($a)
	{
		if (!isset($this->_refnat['refnat']))
		{
			$this->_refnat['refnat'] = array();
			$rs = sqlexec('select id, nom, refnat from refnat');
			while ($row = mysql_fetch_assoc($rs))
				$this->_refnat['refnat'][$row['id']] = $row;
		}
		// on parcourt le tableau
		foreach ($a as $k)
		{
			if ($this->_params[$k] && ($this->_refnat[$k][$this->_params[$k]]['refnat'] || $this->_refnat['refnat'][$k]['refnat']))
			{
				$text = $this->_refnat[$k][$this->_params[$k]]['refnat'] ? $this->_refnat[$k][$this->_params[$k]]['refnat'] : $this->_refnat['refnat'][$k]['refnat'];
				$title = $this->_refnat[$k][$this->_params[$k]]['nom'];
				if ($k == 'modele') // cas particulier du mod�le que l'on veut prefixer de la marque
					$title = $this->_refnat['marque'][$this->_params['marque']]['nom'].' '.$title;
				// le HTML
				$out = '';
				$out.= '<div class="ann_details">';
				$out.='<h2>'.$title.'</h2>';
				$out.= refnat_parse_text ($text, $this->_params, $this->_refnat);
				$out.= '<div class="bottom"></div></div>';
				break;
			}
		}
	}
	// �crire un lien � partir du titre et du href
	public function createAnchor ($title, $href)
	{
		return '<a href="'.$href.'">'.$title."</a>";
	}
	// pour chacun des param�tres s�l�ectionn�s on propose les valeurs de l'environnement les autres param�tres restant constant
	public function getLinksAround ($a)
	{
		$links = array();
		$text = 'location #type# #categorie# #modele# #localisation#';
		foreach ($a as $k)
		{
			if (!$this->_params[$k] || !is_array($this->_refnat[$k])) continue;
			// on parcourt les autres possibilit�s et on les propose
			$p = $this->_params; $cnt = 0;
			foreach ($this->_refnat[$k] as $id => $row)
			{
				if ($id == $this->_params[$k]) continue;
				$p[$k] = $id;
				$links[] = $this->createAnchor (refnat_parse_text ($text, $p, $this->_refnat), '../'.gu_annuaire($p, $this->_refnat));
			}
			break;
		}
		return $links;
	}
	/**
	* Renvoie une liste de liens vers le serveur de r�servations
	* @returns array
	*/
	public function getLinksToResa()
	{
		// les liens vers les pages agences et/ou d�partement
		$links = array();
		if ($this->_params['zone'] && $this->_params['zone'] != 'fr')
			$links[] = $this->createAnchor('location voiture '.$this->_refnat['zone'][$this->_params['zone']]['nom'], self::getURL(gu_agence($this->_refnat['zone'][$this->_params['zone']]), SERVER_RESA));
		foreach (array('agence','dept') as $k)
		{
			if (!$this->_params[$k]) continue;
			foreach ($this->_refnat[$k] as $row)
				$links[] = $this->createAnchor('location voiture '.$row['nom'], self::getURL(gu_agence($row), SERVER_RESA));
			break;
		}
		return $links;
	}
}
?>