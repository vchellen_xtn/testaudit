<?
/**
* Appel�e par PAYBOX suite � la validation du paiement ou au d�clenchement d'une erreur
* V�rifier :
* - adresse IP
* - validit� de la signature
* - existence du num�ro d'autorisation (si "XXXXXX" => test)
* - le code erreur �gale � "00000"	(p.23, 61)
Pour le cas d'un paiement refus� le num�ro d'autorisation est inexistant.
Vous pouvez �galement utiliser pour cela la variable E.
* V�rifier :
* - existence du num�ro d'autorisation (si "XXXXXX" => test)
* - le code erreur �gale � "00000"	(p.23, 61)
Pour le cas d'un paiement refus� le num�ro d'autorisation est inexistant.
Vous pouvez �galement utiliser pour cela la variable E.
*/

//error_reporting(E_ALL ^ E_NOTICE);

include('constant.php');

header('Content-Type: text/plain');

// v�rification des adresses IP
$IPs = array(
	// RnD
	 '127.0.0.1'
	,IP_RND
	// ADA
	,IP_ADA
	// PAYBOX
	,'195.101.99.76'
	,'194.2.122.158'
	,'195.25.7.166'
);

// les arguments peuvent �tre pass�es en GET ou en POST
$a = (count($_GET) ? $_GET : $_POST);

// v�rification des IP
if (!in_array($_SERVER['REMOTE_ADDR'], $IPs)) 
	$msg = 'Adresse IP inconnue : '.$_SERVER['REMOTE_ADDR'].'!';
// on v�rifie la signature
else if (!Paybox::checkSignature())
	$msg = "La signature est incorrecte !";
// on r�cup�re la r�servation
else if ( !($reservation = Reservation::factory($a['id'])) )
	$msg = "identifiant inconnu";
// on v�rifie le montant
else if (isset($a['IdSession'])) 
{
	// CAS RETOUR MPI 3D-SECURE
	if ($a['StatusPBX'] != PayboxSecure::STATUT_3DS_OK)
		$msg = 'Appel 3DSecure KO';
	/*
	else
		$msg = 'Appel 3DSecure OK';
	*/
	save_record('paybox_secure', $a);
}
else if (isset($a['CODEREPONSE']))
{
	// CAS RETOUR PAYBOX SYSTEM 1EURO
	if ($a['MONTANT'] != ((string)(100 * $reservation['total'])))
		$msg = "montant incorrect";
	// v�rifier le num�ro d'autorisation en production
	else if (SITE_MODE=='PROD' && preg_match('/^X+/i', $a['AUTORISATION']))
		$msg = "autorisation de test en production";
	// le CODEREPONSE doit �tre 00000 et l'autorisation doit �tre pr�sente
	else if ($a['CODEREPONSE']!='00000' || !$a['AUTORISATION'])
	{
		$msg = "Erreur paiement";
		$a['COMMENTAIRE'] = Paybox::getMessage($a['CODEREPONSE'], 'UN');
	}
	// le statut de la r�servation doit permettre le paiement
	else if ($reservation['statut'] != 'V')
		$msg = 'Statut inattendu ('.$reservation['statut'].')';
	else
	{
		// on cr�e la transaction associ�e au mod�le de paiement
		$x = Transaction::factory('UNEURO');
		if (!$x)
			$msg ='Mode de paiement inconnu ('.$a['mode'].')';
		else
		{
			// valider la r�servation
			$x->doPayment($reservation, $a);
			if ($reservation['statut']!='P' && $x->hasMessages())
				$msg = join(' - ', $x->getMessages());
		}
	}
	// on enregistre le message PAYBOX au cas o� il ne l'ait pas �t�
	if ($msg && $reservation)
		Paybox::logger($reservation, 'UN', $a);
}
else
{
	// cas non g�r�...
	$msg = "Appel de type inconnu";
}

// on envoie un message en cas d'erreur
if ($msg)
{
	if ($reservation)
	{
		$prefix = '#'.$reservation->getId();
		if (isset($a['CODEREPONSE']))
			$prefix .= ' ['.$a['CODEREPONSE'].']';
		else if (isset($a['StatusPBX']))
			$prefix.= ' ['.$a['StatusPBX'].']';
		$msg = $prefix.' - '.$msg;
	}
//	Paybox::sendError($msg, $a);
	die($msg);
}

?>
