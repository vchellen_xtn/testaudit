<?
class LogPaybox_Collection extends Ada_Collection
{
	/**
	* Cr�e la liste des connexions paybox pour une r�servation
	* 
	* @param Reservation $reservation
	* @return LogPaybox_Collection
	*/
	static public function factory(Reservation $reservation)
	{
		if (!$reservation || !$reservation->getId())
			return;
		$x = new LogPaybox_Collection();
		$sql = 'select * from log_paybox where reservation='.$reservation->getId().' order by id';
		$x->loadFromSQL($sql);
		if ($x->isEmpty() && in_array($reservation['paiement_mode'], array('UN','PB')))
		{
			$data = $reservation->toArray(array('numerotrans','numappel','autorisation','code_reponse'));
			$data['mode'] = $reservation['paiement_mode'];
			// date � associer
			foreach (array('remboursement','paiement') as $k)
			{
				if ($reservation[$k] && (!isset($reservation[$k.'_mode']) || in_array($reservation[$k.'_mode'], array('UN','PB'))))
				{
					$data['creation'] = $reservation[$k];
					break;
				}
			}
			$x->add(new LogPaybox($data));
		}
		return $x;
	}
}
?>
