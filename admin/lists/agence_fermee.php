<?
	session_start();
	include('../../constant.php');
	$title 	 = "Agences ferm�es";
	$url 	 = "../agence.php";
	$key	 = "a.id";
	$order	 = "";

	$sql = "select a.id, a.code_societe as code, z.nom as zone, a.nom";
	$sql.= "	  ,case notification when 'rpc' then 'x' end as upro";
	$sql.= "	  ,case when (type & ".Agence::TYPE_VP.") = ".Agence::TYPE_VP." then 'x' end as vp";
	$sql.= "	  ,case when (type & ".Agence::TYPE_VU.") = ".Agence::TYPE_VU." then 'x' end as vu";
	$sql.= "	  ,case when (type & ".Agence::TYPE_2R.") = ".Agence::TYPE_2R." then 'x' end as hb";
	$sql.= "	  ,case when (type & ".Agence::TYPE_ML.") = ".Agence::TYPE_ML." then 'x' end as ml";
	$sql.= "	  ,case when (statut & ".Agence::STATUT_ABANDONNISTES.") = ".Agence::STATUT_ABANDONNISTES." then 'x' end as abandonnistes";
	$sql.= "	  ,case when (statut & ".Agence::STATUT_JEUNES.") = ".Agence::STATUT_JEUNES." then 'x' end as jeunes";
	$sql.= "	  ,case when (statut & ".Agence::STATUT_LIVRAISON_SEULEMENT.") = ".Agence::STATUT_LIVRAISON_SEULEMENT." then 'x' end as livraison";
	$sql.= "	  ,tel, email";
	$sql.= "	 from agence a join zone z on a.zone=z.id";
	$sql.= "	 where a.statut&1=0 AND a.zone='".$_SESSION['ADA001_ADMIN_ZONE']."'";
	
	$out_url = "list.php?sql=" . urlencode($sql);
	$out_url.= "&title=" . urlencode($title);
	$out_url.= "&url=" . urlencode($url);
	$out_url.= "&color=4587ba";
	$out_url.= "&key=".$key."&order=".$order."&nb_rows=30";
	header("location: ".$out_url);
?>
