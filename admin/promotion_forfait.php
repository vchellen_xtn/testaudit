<?
	require_once("secure.php"); 

	// identifiant
	$id = (int) getarg('id');
	
	// suppression
	if ($id && getarg("delete")) 
	{
		$sql = "delete from promotion_forfait where id = '".$id."'";
		getsql($sql);
		unset($id);
	}
	
	// modification, cr�ation
	if (getarg("update") || getarg("create"))
	{
		if (!$id)
		{
			$_POST['zone'] = $_SESSION['ADA001_ADMIN_ZONE'];
			$summary = array();
			// construire le tableau des agences
			if (is_array($_POST['agence']))
				$agences = $_POST['agence'];
			else if ($_POST['agence'])
				$agences = array($_POST['agence']);
			else if ($_POST['code_societe'])
			{
				$agences = array();
				$rs_agence = sqlexec("select ap.id from agence_pdv ap join agence a on ap.agence=a.id where (a.statut&1=1) and a.code_societe='".filter_var($_POST['code_societe'], FILTER_SANITIZE_STRING)."'");
				while ($row_agence = mysql_fetch_assoc($rs_agence))
					$agences[] = $row_agence['id'];
			}
			else
				$agences = array(null);
			$categories = is_array($_POST['categorie']) ? $_POST['categorie'] : array($_POST['categorie']);
			foreach($agences as $a)
			{
				// on �vite le cas o� la valeur "Tous" est s�lectionn�e avec d'autres agences
				if (!$a && count($agences) > 1) continue;
				// on recopie les donn�es du POST
				$search = array();
				$data['agence'] = $a;
				if ($a) $a = Agence::factory($a);
				$search['/#agence_id#/'] = $a['id'];
				$search['/#agence_nom#/'] = $a['nom'];
				foreach($categories as $c)
				{
					$data = $_POST;
					$data['agence'] = $a['id'];;
					$data['categorie'] = $c;
					if ($c) $c = Categorie::factory($c);
					$search['/#categorie_mnem#/'] = $c['mnem'];
					$search['/#categorie_nom#/'] = $c['nom'];
					foreach(array('nom','slogan','slogan_iphone','description','mentions') as $k)
						$data[$k] = preg_replace(array_keys($search), array_values($search), $_POST[$k]);
					save_record('promotion_forfait', $data);
					$summary[$data['id']] = $data['nom'];
				}
			}
			if (count($summary)==1)
				 $id = key($summary);
			else
			{
				echo '<strong>'.count($summary).' promotions cr��es</strong><br/>';
				echo '<ol>';
				foreach($summary as $k => $v)
					echo '<li><a href="promotion_forfait.php?id='.$k.'">#'.$k.' - '.$v."</li>\n";
				echo '</ol>';
				exit();
			}
		}
		else
			save_record("promotion_forfait", $_POST);
	}
	// enregistrement courant
	if ($id)
	{
		$row = PromotionForfait::factory($id, true);
		if (!$row || $row->isEmpty())
			die("Aucun enregistrement correspondant.");
		$used = getsql("SELECT COUNT(*) FROM reservation WHERE statut IN ('V','P','A','X') AND promotion=".$id);
	}
	else if (!$_SESSION['ADA001_ADMIN_ZONE'])
	{
		die("Vous devez pr�ciser une zone avant de pouvoir cr�er une nouvelle promotion !");
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<style type="text/css">
		input.edito, textarea.edito { background-color: yellow; }
		body { background-color: #990000;}
	</style>
	<script type="text/javascript" src="<?=Page::getProtocol()?>://www.google.com/jsapi"></script>
	<script type="text/javascript">
		google.load('jquery', '1.4');
	</script>
	<link rel="stylesheet" type="text/css" href="../css/anytime.min.css" />
	<script type="text/javascript" src="../scripts/anytime.min.js"></script>
	<script type="text/javascript" src="../scripts/script.js"></script>
	<script type="text/javascript" src="admin.js"></script>
	<script type="text/javascript">
	// <![CDATA[
	// doDelete
	function doDelete(frm)
	{
		return (confirm("Voulez-vous r�ellement supprimer cet �l�ment ?\nCette op�ration est irr�versible."));
	}
	
	// doSubmit
	function doSubmit(frm)
	{
		alert("Ce formulaire est neutralis� !");
		return false;
		
		var err = "", msg = "";
		var nbCat = countCheckboxes('categorie[]', frm);
		if (!$.trim(frm.nom.value).length)
			err += "- le nom\n";
		if(frm.agence && frm.agence.selectedIndex <= 0 && nbCat == 0)
			msg += "- valable pour l'ensemble des cat�gories et des agences\n"
		if (frm.agence && frm.agence.options.length==1)
			msg += "- aucune agence ne correspond � votre s�lection\n";
		if (frm.jours_min && !frm.jours_min.value.match(/^\d*$/))
			err += "- un nombre de jours minimum valide\n";
		if (frm.jours_min && frm.jours_min.value.match(/^\d+$/) && frm.jours_max.value.match(/^\d+$/) && parseInt(frm.jours_min.value) > parseInt(frm.jours_max.value))
			err += "- le nombre de jours minimum doit �tre inf�rieur ou �gal au nombre de jours maximum\n";
		if (frm.jours_max && !frm.jours_max.value.match(/^\d*$/))
			err += "- un nombre de jours maximum valide\n";
		if (frm.forfait && frm.forfait.options.length == 1)
			err += "- aucun forfait ne correspond � vos choix, cette promotion ne s'appliquera pas!\n";
		if(frm.forfait_prefix && frm.forfait_prefix.selectedIndex == 0 && frm.forfait.selectedIndex == 0 && nbCat == 0)
			msg += "- valable pour l'ensemble des cat�gories et des forfaits.\n";
		if (frm.age_min && !frm.age_min.value.match(/^\d*$/))
			err += "- un �ge minimum valide\n";
		if (frm.age_min && frm.age_min.value.match(/^\d+$/) && frm.age_max.value.match(/^\d+$/) && parseInt(frm.age_min.value) > parseInt(frm.age_max.value))
			err += "- l'�ge minimum doit �tre inf�rieur ou �gal � l'�ge maximum\n";
		if (frm.age_max && !frm.age_max.value.match(/^\d*$/))
			err += "- un �ge maximum valide\n";
		if (frm.mode && !frm.mode.options[frm.mode.selectedIndex].value.length)
			err += "- les modalit�s de la promotion\n";
		if (frm.montant && !frm.montant.value.match(/^\d+(\.\d+)?$/))
			err += "- le montant de la promotion\n";

		// v�rifier les dates
		if (frm.depuis.value.length)
		{
			if(!validateDate(frm.depuis))
				err += "- la date de debut de validit� est incorrecte\n";
			else if (checkDateDepuis(makeDate(frm.depuis.value)))
				err += "- la date de d�but de validit� doit �tre ant�rieure � la date de d�but de r�servation et � la date de fin de validit�\n";
		}
		if (frm.jusqua.value.length)
		{
			if(!validateDate(frm.jusqua))
				err += "- la date de fin de validit� est incorrecte\n";
			else if (checkDateJusqua(makeDate(frm.jusqua.value)))
				err += "- la date de fin de validit� doit �tre ant�rieure � la date de fin de r�servation\n";
		}
		if (!frm.jours_avant.value.match(/^\d*$/))
			err += "- le nombre de jours avant n'est pas valide : " + frm.jours_avant.value + "\n";
		if(frm.debut.value.length && !validateDate(frm.debut))
			err += "- la date de debut est incorrecte\n";
		if(!frm.fin.value.length)
			msg += "- sans date de fin\n";
		else if(!validateDate(frm.fin.value))
			err += "- la date de fin est incorrecte\n";
		// et v�rifier la date
		else if (checkDateFin(makeDate(frm.fin.value)))
			err += "- la date ne peut pas �tre ant�rieure � aujourd'hui ou � une date d�j� pass�e\n";
		// et l'�ditorial
		if (!$.trim(frm.slogan.value).length)
			err += "- le slogan\n";
		if (!$.trim(frm.description.value).length)
			err += "- la description\n";
		// on auto-s�lectionne s'il n'y a qu'une agence
		if (frm.agence && frm.agence.options.length==2)
			frm.agence.selectedIndex = 1;
		// on compte le nombre d'agence concern�es
		nbAgences = 0;
		if (frm.agence && frm.code_societe)
		{
			if (frm.code_societe.selectedIndex > 0 && frm.agence.selectedIndex <= 0) 
				nbAgences = frm.agence.options.length-1;
			else if (frm.agence.multiple)
			{
				agences = $(frm.agence).val();
				if (agences) nbAgences = agences.length;
			}
		}
		// si plusieurs promotions vont �tre cr��s
		if (nbCat > 1 || nbAgences > 1)
			msg += "- " + (Math.max(nbAgences,1)*Math.max(nbCat,1)) + " promotions vont �tre cr��s\n";
		// demandez confirmation pour les promotions "larges"
		if (msg.length)
		{
			if (confirm("Vous cr�ez une promotion :\n" + msg + "Confirmez-vous ?"))
				msg = "";
		}
		if (err.length || msg.length)
		{
			if (err.length)
				alert("Attention ! Veuillez pr�ciser :\n" + err);
			return false;
		}
		// envoyer un tableau puor les agences si n�cessaires
		if (frm.agence && frm.agence.multiple)
			frm.agence.name = "agence[]";
		return true;
	}
	
	// initilisation AnyTime
	var dateFormat = '%d-%m-%Y';
	var dateOptions = getOptionsAnyTime(dateFormat, "Depuis");
	var dateConverter = new AnyTime.Converter({format: dateFormat});

	// checkDateFin: renvoie true si la date n'est pas correcte, contr�le l'affichage dans le calendrier
	// si la promotion est utilis�e, la date de fin ne peut pas �tre avant le minimum entre la date de fin pr�c�dente ou aujourd'hui
	function checkDateFin(dateFin, y, m, d, h, i)
	{
		var fin = makeDate("<?=euro_iso($row['fin'])?>");
		// ne peut pas �tre inf�rieure � maintenant
		var now = new Date();
		now.setHours(0,0,0,0);
		if (fin && fin.getTime() < now.getTime())
			now = fin;
		if (dateFin && dateFin.getTime() < now.getTime()) 
			return true;
		return false;
	}
	function checkDateDepuis(dateDepuis, y, m, d, h, i)
	{
		// doit �tre avant la date de d�but de r�servation et avant la date de fin de validit�
		dateDepuis.setHours(0,0,0,0);
		var d = makeDate(document.forms[0].debut.value);
		if (d && dateDepuis.getTime() > d.getTime())
			return true;
		d = makeDate(document.forms[0].jusqua.value);
		if (d && dateDepuis.getTime() > d.getTime())
			return true;
		return false;
	}
	function checkDateJusqua(dateJusqua, y, m, d, h, i)
	{
		// doit �tre avant la date de d�but de r�servation et avant la date de fin de validit�
		dateJusqua.setHours(0,0,0,0);
		var d = makeDate(document.forms[0].fin.value);
		if (d && dateJusqua.getTime() > d.getTime())
			return true;
		return false;
	}
	// initialiser le forumaire
	$(document).ready(function()
	{
		dateOptions.labelTitle = "depuis";
		AnyTime.picker('depuis', dateOptions);
		dateOptions.labelTitle = "jusqu'�";
		AnyTime.picker('jusqua', dateOptions);
		var fin = null;
<? if (!$used) : /* on ne peut plus changer la date de d�but avant utuilisation */ ?>
		dateOptions.labelTitle = "d�but";
		AnyTime.picker('debut', dateOptions);
<? elseif ($row['fin']) : ?>
		fin = dateConverter.parse("<?=euro_iso($row['fin'])?>");
<? endif; ?>
		dateOptions.labelTitle = "fin";
		dateOptions.earliest = ((fin && fin.getTime && (fin.getTime() < today.getTime())) ? fin : today);
		AnyTime.picker('fin', dateOptions);
		// vider les dates
		$('a.cal_empty').click(function()
		{
			$("#" + this.id.substring(4)).val("").change();
			return false;
		});
		$('#jours_min, #jours_max').change(function()
		{
			if (this.value.match(/^\d*$/))
			{
				updateSelect(this.form.forfait);
				return true;
			}
			alert("Vous devez indiquer un chiffre!");
			this.focus();
			return false;
		});
		// les mises � jour de listes
		$("#reseau, #groupe").change(function(){
			updateSelect(this.form.code_societe);
		});
		$("#reseau, #groupe, #code_societe").change(function(){
			updateSelect(this.form.agence);
		});
		$("#type, #forfait_prefix").change(function(){
			updateSelect(this.form.forfait);
		});
		$('#type').change(function(){
			$('tr.tr_type').hide();
			$('tr.tr_type :checkbox').each(function()
			{
				this.checked = false;
			});
			if (v = $(this).val())
				$('#type_'+v).show();
		});
		$('#agence_multiple').click(function()
		{
			var $agence = $("#agence");
			if ($agence.attr("multiple"))
			{
				$agence.removeAttr("multiple");
				$agence.removeAttr("size");
				$($agence[0].options[0]).removeAttr("disabled");
			}
			else
			{
				$agence.attr('multiple','multiple');
				$agence.attr("size", Math.min(8, Math.max($agence[0].options.length, 2)));
				if ($agence[0].selectedIndex==0)
					$agence[0].selectedIndex = -1;
				$($agence[0].options[0]).attr("disabled","disabled");
			}
			return false;
		});

		// on met � jour au chargement
		var frm = document.forms[0];
		if (frm.agence)
			updateSelect(frm.agence, "<?=filter_var($_GET['agence'], FILTER_SANITIZE_STRING)?>");
		if (frm.code_societe)
			updateSelect(frm.code_societe, null);
		if (frm.forfait)
			updateSelect(frm.forfait, "<?=filter_var($_GET['forfait'], FILTER_SANITIZE_STRING)?>");
		$("textarea, input[type=text]", frm).change(function()
		{
			$(this).removeClass("edito");
		});
	/*
		createCalendar("depuis", checkDateDepuis, "btn_depuis");
		createCalendar("jusqua", checkDateJusqua, "btn_jusqua");
		createCalendar("debut", null, "btn_debut");
		createCalendar("fin", <?=$used ? "checkDateFin" : "null"?>, "btn_fin");
	*/
	});
	// mettre � jour les donn�es �ditorial
	function updateEdito(frm)
	{
		// un minimum d'information doit �tre s�lectionn�e
		var msg = '';
		if (!frm.mode.value || !frm.montant.value.match(/^\d+(\.\d+)?/))
			msg += "- le montant de la promotion !\n";
		if (msg.length)
		{
			alert("Veuillez pr�ciser les �l�ments suivants :\n" + msg);
			return false;
		}
		// on cr�e l'�ditorial
		var url = location.href.replace(/\/[a-z\-\_\d]+\.php(\?.*)?$/,'') + "/json.promotion_edito.php";
		var lstAgence = frm.agence;
		if (lstAgence.multiple)
			lstAgence.name = "agence[]";
		$.post(url, $(frm).serialize(), function(json)
		{
			lstAgence.name = "agence";
			for(var i in json)
			{
				var ctrl = frm[i];
				if (ctrl && ctrl.name == i && (!$.trim(ctrl.value).length || $(ctrl).hasClass("edito")))
				{
					ctrl.value = json[i];
					$(ctrl).addClass("edito");
				}
			}
		});
		return true;
	}
	// ]]>
	</script>
</head>
<body>
<form action="" method="post" enctype="multipart/form-data" onsubmit="return doSubmit(this);">
<input type="hidden" name="id" value="<?=$id ? $id : '';?>">
<table class="framed" border="0" cellspacing="0" cellpadding="2">
<tr>
	<th class="bg_titre">
	<img src="img/puce.gif" align="absmiddle" hspace="1">
<? 
	if (!$id) 
		echo "Nouvelle Promotion ";
	else
	{
		echo "Promotion [#".$id."]";
		if ($row['archive'])
			echo ' Archiv�';
		if ($used) echo ' (utilis� '.$used.' fois)';
	}
?>
	</th>
</tr>
<tr>
	<td>
		<table class="noborder" width="100%" cellspacing="0" cellpadding="0">
		<col width="10%">
		<col width="40%">
		<col width="10%">
		<col width="40%">
		<tr>
			<td><label for="nom">Nom</label></td>
			<td><input type="text" id="nom" name="nom" value="<?=$row['nom']?>" maxlength="128" style="width: 100%;"/></td>
			<td><label for="actif">Actif</label></td>
			<td><input type="checkbox" id="actif" name="actif[]" value="1" <?=(!$id || $row['actif'] ? 'checked="checked"' : '');?> style="width: auto;"><input type="hidden" name="actif[]" value="0"></td>
		</tr>
		<tr><th colspan="4">Quelles Agences ?</th></tr>
		<tr>
			<td><label for="reseau">R�seau</label></td>
			<td>
			<?
				if (!$id)
					selectarray('reseau', array_combine(Agence::$RESEAU, Agence::$RESEAU), '', 'Tous');
				else
					echo $row['reseau'] ? $row['reseau'] : '-';
			?>
			</td>
			<td><label for="groupe">Groupe</label></td>
			<td>
			<?
				if (!$id)
					selectarray('groupe', ZoneGroupe_Collection::factory($_SESSION['ADA001_ADMIN_ZONE']), '', 'Tous');
				else
					echo ($row['groupe'] ? ZoneGroupe::factory($row['groupe'])->getData('nom') : '-');
			?>
			</td>
		</tr>
		<tr>
			<td><label for="code_societe">Soci�t�</label></td>
			<td>
			<?
				if (!$id)
					selectarray('code_societe', array(), '', 'Toutes');
				else
					echo ($row['code_societe'] ? getsql("select concat(code_societe, ' ', nom) from agence where code_societe='".addslashes($row['code_societe'])."' limit 1") : '-');
			?>
			</td>
			<td>
				<label for="agence">Agence</label>
				<? if (!$id) : ?>
				<br/><a href="#" id="agence_multiple">multiple ?</a>
				<? endif; ?>
			</td>
			<td>
			<?
				if (!$id)
					selectarray('agence', array(), '', 'Toutes');
				else if ($row['agence'])
				{
					$agence = Agence::factory($row['agence']);
					echo '<a href="agence_pdv.php?id='.$agence['id'].'">'.$agence['id'].' '.$agence['nom'].'</a>';
				}
				else
					echo '-';
			?>
			</td>
		</tr>
		<tr><th colspan="4">Quels V�hicules et Forfaits ?</th></tr>
		<tr>
			<td><label for="type">Type</label></td>
			<td>
			<? 
				if (!$id)
					selectarray('type', Categorie::$TYPE, $_GET['type'], 'Tous les types');
				else if ($row['type'])
					echo strtoupper($row['type']).' - '.Categorie::$TYPE[strtolower($row['type'])];
				else
					echo 'Toutes';
			?>
			</td>
			<? if ($id) :?>
			<td><label for="categorie">Cat�gorie</label></td>
			<td><? echo ($row['categorie'] ? Categorie::factory($row['categorie']) : 'Toutes'); ?></td>
			<? else : ?>
			<td colspan="2">Si aucune cat�gorie s�lectionn�e, elles seront toutes concern�es</td>
			<? endif; ?>
		</tr>
		<?
		// afficher la liste des cat�goires pour chauqe type
		if (!$id)
		{
			foreach(Categorie::$TYPE as $k => $v)
			{
				echo '<tr class="tr_type" id="type_'.$k.'" style="display: none;">';
				echo '<td></td>';
				echo '<td colspan="3">';
				foreach(Categorie_Collection::factory('fr', $k) as $x)
				{
					echo '<input type="checkbox" name="categorie[]" id="categorie_'.$x['id'].'" value="'.$x['id'].'"/>';
					echo '<label for="categorie_'.$x['id'].'">'.$x['mnem'].'</label>';
				}
				echo '<br/>';
				echo '<a href="#" onclick="return doCheckbox(this, true);">cocher</a>&nbsp;&nbsp;';
				echo '<a href="#" onclick="return doCheckbox(this, false);">d�cocher</a>';
				echo '</td>';
				echo '</tr>';
			}
		}
		?>
		<tr>
		<? foreach(array('jours_min'=>'Dur�e min', 'jours_max'=>'Dur�e max') as $k => $v) : ?>
			<td><label for="<?=$k?>"><?=$v?></label></td>
			<td>
				<? if ($id) : ?>
					<? echo ($row[$k] ? $row[$k].' jour(s)' : '-');?>
				<? else : ?>
					<input type="text" id="<?=$k?>" name="<?=$k?>" value="<?=$row[$k]?>" maxlength="3" size="3"/> jours
				<? endif; ?>
			</td>
		<? endforeach; ?>
		</tr>
		<tr>
			<td><label for="forfait_prefix">Pr�fixe</label></td>
			<td>
			<?
				if (!$id)
				{
					$sql = "SELECT DISTINCT CONCAT(LEFT(id, case when instr(id,'ki')>0 then 6 else 4 end),'%') k, CONCAT(UCASE(type), ' ',LEFT(id, case when instr(id,'ki')>0 then 6 else 4 end)) v FROM forfait2 ORDER BY v";
					selectsql ('forfait_prefix', $sql, $row ? $row["forfait_prefix"] : $_GET["forfait_prefix"], 'Tous les forfaits');
				}
				else
					echo ($row['forfait_prefix'] ? substr($row['forfait_prefix'], 0, -1) : '-');
			?>
			</td>
			<td><label for="forfait">Forfait</label></td>
			<td>
			<?
				if (!$id)
					selectarray('forfait', array(), null, 'Tous');
				else
					echo ($row['forfait'] ? getsql("select concat(id,' ', nom) from forfait2 where id='".addslashes($row['forfait'])."'") : '-');
			?>
			</td>
		</tr>
		<tr><th colspan="4">Quel �ge d�clar� dans la r�servation ?</th></tr>
		<tr>
		<? foreach(array('age_min'=>'�ge min', 'age_max'=>'�ge max') as $k => $v) : ?>
			<td><label for="<?=$k?>"><?=$v?></label></td>
			<td>
				<? if ($id) : ?>
					<? echo ($row[$k] ? $row[$k].' ans' : '-');?>
				<? else : ?>
					<input type="text" id="<?=$k?>" name="<?=$k?>" value="<?=$row[$k]?>" maxlength="2" size="2"/> ans
				<? endif; ?>
			</td>
		<? endforeach; ?>
		</tr>
		<tr><th colspan="4">Quelle promotion ?</th></tr>
		<tr>
			<td><label for="classe">Pour</label></td>
			<td>
			<?
				if ($id)
					echo ($row['classe'] ? getsql('select nom from partenaire_classe where id='.(int)$row['classe']) : 'Tous publics');
				else
					selectsql("classe", 'select id, nom from partenaire_classe order by nom', $_GET['classe'], 'Tous publics');
			?>
		<? if ($id):  ?>
				<td><label for="mode">Promotion</label></td>
				<td>- <?=$row['montant'].' '.($row['mode']=='P' ? '%' : '�');?></td>
		<? else : ?>
			<td colpan="2"></td>
		</tr>
		<tr>
			<td><label for="mode">Promotion</label></td>
			<td><? selectarray('mode', array('N'=>'en num�raire', 'P'=>'en pourcentage'), $row['mode'], '--'); ?></td>
			<td><label for="montant">Montant</label></td>
			<td><input type="text" id="montant" name="montant" value="<?=$row['montant']?>" maxlength="5" size="5"/></td>
		<? endif; ?>
		</tr>
		<tr><th colspan="4">D�part ?</th></tr>
		<tr>
			<td><label for="debut">D�but</td>
			<td>
				<input autocomplete="off" type="text" name="debut" id="debut" value="<?=euro_iso($row["debut"])?>" size="10" maxlength="10" <?=($used ? ' readonly="readonly"' : '');?>>
				<? if (!$used) { ?>
				<a class="cal_empty" href="#" id="btn_debut" style="vertical-align: middle">effacer</a>
				<? } ?>
			</td>
			<td><label for="fin">Fin</label></td>
			<td>
				<input autocomplete="off" type="text" name="fin" id="fin" value="<?=euro_iso($row["fin"])?>" size="10" maxlenth="10">
				<a class="cal_empty" href="#" id="btn_fin" style="vertical-align: middle">effacer</a>
			</td>
		</tr>
		<tr><th colspan="4">Intervalle de souscription</th></tr>
		<tr>
			<td><label for="depuis">Depuis</td>
			<td>
				<input autocomplete="off" type="text" style="width:inherit;" name="depuis" id="depuis" value="<?=euro_iso($row["depuis"])?>" size="10" maxlength="10">
				<a class="cal_empty" href="#" id="btn_depuis" style="vertical-align: middle">effacer</a>
			</td>
			<td><label for="jusqua">Jusqu'�</label></td>
			<td>
				<input autocomplete="off" type="text" style="width:inherit" name="jusqua" id="jusqua" value="<?=euro_iso($row["jusqua"])?>" size="10" maxlength="10">
				<a class="cal_empty" href="#" id="btn_jusqua" style="vertical-align: middle">effacer</a>
			</td>
		</tr>
		<tr>
			<td><label for="jours_avant">Jours avant</label></td>
			<td><input type="text" style="width: inherit;" id="jours_avant" name="jours_avant" value="<?=$row['jours_avant']?>" size="4" maxlength="2"/></td>
			<td colspan="2"></td>
		</tr>
		<tr><th colspan="4">Editorial <? if (!$id) echo '<input type="button" name="btn_edito" value="g�n�rer automatiquement et v�rifier" onclick="return updateEdito(this.form);"/>';?> </th></tr>
		<tr>
			<td><label for="slogan">Slogan</label></td>
			<td colspan="3"><input type="text" id="slogan" name="slogan" value="<?=$row['slogan']?>" maxlength="255" size="64"></td>
		</tr>
		<tr>
			<td><label for="slogan_iphone">Slogan iPhone</label></td>
			<td colspan="3">
				<input type="text" id="slogan_iphone" name="slogan_iphone" value="<?=$row['slogan_iphone']?>" maxlength="24" size="24">
				<em>allez � l'essentiel</em>
			</td>
		</tr>
		<? if (false) { // pas utilis�... ?>
		<tr>
			<td><label for="visuel">Visuel</label></td>
			<td><?=url_ctrl("visuel", $row["visuel"])?></td>
		</tr>
		<? } ?>
		<tr>
			<td><label for="description">Description</label></td>
			<td colspan="3"><textarea id="description" name="description" rows="4"><?=$row['description']?></textarea></td>
		</tr>
		<tr>
			<td><label for="mentions">Mentions</label></td>
			<td colspan="3"><textarea id="mentions" name="mentions" rows="4"><?=$row['mentions']?></textarea></td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td align="right">
		<? if (false) : ?>
		<input type="submit" name="<?=$id ? "update" : "create"?>" value="enregistrer">
			<? if ($id && !$used) : ?>
			&nbsp;
			<input type="submit" onclick="return doDelete(this.form);" name="delete" value="supprimer">
			<? endif; ?>
		<? endif; ?>
	</td>
</tr>
</table>
</form>
</body>
</html>
