(function($){
	var methods = {
		init : function( options ) {
			settings = {
				prevClass : '.prev',
				nextClass : '.next',
				children : '.carrousel-band li',
				changeCallback : function(index) {},
				moveEndCallback : function() {}
			};
			if ( options ) {
				$.extend( settings, options );
			}
			methods.update.apply( this );
			$that = this;
			this.find(settings.prevClass).bind('click', function(){
				methods.select.apply( $that, [current-1] );
				return false;
			});
			this.find(settings.nextClass).bind('click', function(){
				methods.select.apply( $that, [current+1] );
				return false;
			});
		},
		changed : function(index){
			settings.changeCallback.apply( this, [index] );
		},
		update : function( options ) {
			current = 0;
			// donn�es rattach�es au carrousel
			max = $(settings.children, this).length - 1;
			$unit = $('.carrousel-band', this).children(':first-child');
			unitWidth = $unit.width();
			unitMargin = parseInt($unit.css('margin-left'), 10);
			width = ((1+max) * unitWidth) + (2 * unitMargin);
			bandWidth = $('.carrousel-band', this).css({'width': width, 'left':0});
			unitsVisible = Math.floor($(this).width() / unitWidth);
			// s�lection ?
			var idx = $(settings.children, this).index($(settings.children+'.current', this));
			if (idx > 0) {
				methods.select.apply( this, [idx] );
			} else {
				methods.changed.apply( this, [current] );
			}
			// boutons
			$that = this;
			this.find(settings.children).each(function(i, el){
				$(this).bind('click', function(){
					methods.select.apply( $that, [i] );
					return false;
				});
			});
			methods.updateBtn.apply( this );
		},
		updateBtn : function( options ) {
			// on rend les boutons inatif au besoin
			if ( current == 0 || max <= 3) {
				this.find(settings.prevClass).addClass('inactive');
			} else {
				this.find(settings.prevClass).removeClass('inactive');
			}
			if ( current == max || max <= unitsVisible) {
				this.find(settings.nextClass).addClass('inactive');
			} else {
				this.find(settings.nextClass).removeClass('inactive');
			}
		},
		move : function (idx) {
			$band = this.find('.carrousel-band');
			if (idx < 0 || idx > max || $band.is(':animated') ) {
				return;
			}
			var currentLeft = parseInt($band.css('left'));
			var target_left = ($band.parent().width() / 2) - ((idx + 0.5) * unitWidth);
/* Centrer le caroussel m�me pour les extr�mit�s
		if ( target_left > 0 ) {
				target_left = 0;
			} else if ( Math.abs(target_left - $band.parent().width()) > parseInt($band.width()) ) {
				target_left = -(parseInt($band.width()) - $band.parent().width());
			}
*/
			// mettre un temps d'animation qui d�pend de la distance � parcourir
			$band.animate({'left' : target_left+'px'}, Math.abs(currentLeft - target_left));
		},
		select : function (idx) {
			// carrousel en cours d'animation
			if ( this.find('.carrousel-band:animated').length ) {
				return;
			}
			// �l�ment d�ja selectionn� ou en dehors du set
			if ( idx == current || idx < 0 || idx > max) {
				return;
			}
			// class current
			this.find(settings.children).removeClass('current').eq(idx).addClass('current');
			var old_current = current;
			// nouveau current
			current = idx;
			// appel du callback
			methods.changed.apply( this, [current] );
			// update des boutons
			methods.updateBtn.apply( this );

			// mouvement
			methods.move.apply( this, [idx] );
			settings.moveEndCallback.apply( this );
		}
	};
	$.fn.carrousel = function(method) {
		// global vars
		var settings, current, max, bandWidth, $that;

		// Method calling logic
		if ( methods[method] ) {
			return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, arguments );
		} else {
			$.error( 'Method ' +  method + ' does not exist on jQuery.carrousel' );
		}
		return this;
	};
})(jQuery);