<?php

class AgenceConfig extends Ada_Object
{
	static public $TARIFICATIONS = array
	(
		'ADAFR'	=> 'Tarif National',
		'RESAS'	=> 'Tarif Agence'
	);
	
	// agence st la cl� primaire
	protected function _construct() {
		$this->setIdFieldName('agence');
		return $this;
	}

}
?>
