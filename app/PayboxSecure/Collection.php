<?php
class PayboxSecure_Collection extends Ada_Collection
{
	/**
	* Cr�er � partir d'une r�servation
	* 
	* @param Reservation $reservation
	* @return PayboxSecure_Collection
	*/
	static public function factory(Reservation $reservation)
	{
		$sql = 'select * from paybox_secure where reservation='.(int)$reservation->getId().' order by creation desc';
		$x = new PayboxSecure_Collection();
		$x->loadFromSQL($sql);
		return $x;
	}
	
	/**
	* Renvoie le premier PayboxSecure correct 
	* @returns PayboxSecure
	*/
	public function findOK()
	{
		foreach($this->_items as /** @var PayboxSecure */ $secure)
		{
			if ($secure->isOK())
				return $secure;
		}
		return null;
	}
}
?>
