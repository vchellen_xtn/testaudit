<?
	session_start();
	require_once('secure.php'); 
	require_once('lib.php');
	// remettre � z�ro
	$zone = $_SESSION['ADA001_ADMIN_ZONE'];
	if (!$zone) die("Vous devez choisir une zone !");
	if ($_POST['reset'])
	{
		$_POST = array();
		$_SESSION['admin_promo_search'] = array();
	}
	// faire une recherche
	if (count($_POST))
		$_SESSION['admin_promo_search'] = $_POST;
	else if ($_SESSION['admin_promo_search'])
	{
		foreach($_SESSION['admin_promo_search'] as $k => $v)
			$_POST[$k] = $v;
	} else {
		$_POST['perimetre'] = 'ADAFR';
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script type="text/javascript" src="<?=Page::getProtocol()?>://www.google.com/jsapi"></script>
	<script type="text/javascript">
		google.load('jquery', '1.4');
	</script>
	<link rel="stylesheet" type="text/css" href="../css/anytime.min.css" />
	<script type="text/javascript" src="../scripts/anytime.min.js"></script>
	<script type="text/javascript" src="admin.js"></script>
	<script type="text/javascript">
		function doSort(key, order)
		{
			var frm = document.forms['frm_search'];
			frm.key.value = key;
			frm.order.value = order;
			return doDisplay(frm, 1);
		}
		function doDisplay(frm, from)
		{
			if (frm.from_row)
				frm.from_row.value = from;
			frm.submit();
			return false;
		}
		// onload
		var dateFormat = '%d-%m-%Y';
		var dateConverter = new AnyTime.Converter({format: dateFormat});
		var dateOptions = getOptionsAnyTime(dateFormat, "depuis");

		// onload
		$(document).ready(function()
		{
			AnyTime.picker('date_from', dateOptions);
			dateOptions.labelTitle = "jusqu'�";
			AnyTime.picker('date_to', dateOptions);
		});
	</script>
	<style type="text/css">
		select,input { width: inherit;}
		label { padding-left: 2px; }
		label.col1 { padding-left: 0; float: left; width: 65px;}
		table.framed { width: 900px; }
	</style>
</head>
<body>
<?= SQLExporter::getForm(); ?>
<form id="frm_search" name="frm_search" action="" method="post">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre">
		<img src="img/puce.gif" align="absmiddle" hspace="1">
		Recherche promotions
	</th>
</tr>
<tr>
	<td>
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<tr><td>
		<fieldset><legend>Identification</legend>
			<p>
				<label class="col1" for="id">Promotion</label><input type="text" value="<?=htmlspecialchars($_POST['id'])?>" id="id" name="id" size="8" maxlength="7"/>
			</p>
			<p>
				<label class="col1" for="pdv">Pt de vente</label><input type="text" value="<?=htmlspecialchars($_POST['pdv'])?>" id="pdv" name="pdv" size="7" maxlength="6"/>
				<label for="coupon">Coupon</label><input type="text" value="<?=htmlspecialchars($_POST['coupon'])?>" id="coupon" name="coupon" size="24" maxlength="24"/>
			</p>
			<p>
				<label class="col1" for="campagne">Campagne</label>
				<? selectsql('campagne', "select id, concat(nom,' (#',id,' - ',ifnull(date_format(debut,'%d/%m/%y'),''),' - ',ifnull(date_format(fin,'%d/%m/%y'),''),')') nom from promotion_campagne order by nom", $_POST['campagne'], '--'); ?>
			</p>
			<p>
				<label class="col1" for="classe">Classe</label>
				<? selectsql('classe', "select '%' id, '(Aucune)' nom union select distinct pc.id id, concat(pc.nom,' (#',pc.id,')') nom from agence_promo_forfait p join partenaire_classe pc on pc.id=p.classe order by nom", $_POST['classe'], '--'); ?>
			</p>
		</fieldset>
		<fieldset><legend>Conditions</legend>
			<p>
				<label class="col1" for="forfait">Forfait</label>
				<? 
					$forfaits = AgencePromoForfait::getAllForfaits();
					selectarray('forfait', array_merge(array('%' => '(Tous)'), array_combine($forfaits, $forfaits)), $_POST['forfait'], '---'); 
				?>
				<label for="promo_type">Type promo</label>
				<? selectarray('promo_type', AgencePromoForfait::$PROMO_TYPES, $_POST['promo_type'], '---'); ?>
				<label for="perimetre">P�rim�tre</label>
				<? selectarray('perimetre', AgencePromoForfait::$PROMO_PERIMETRE, $_POST['perimetre'], '(Tous)'); ?>
			</p>
			<p>
				<?
					$flds_date = array('depuis','jusqua','debut','fin'); 
					$flds_date = array_combine($flds_date, $flds_date); 
					foreach(array('date_from' => date('01-01-Y'),'date_to'=>date('31-12-Y')) as $k => $v)
					{
						if (!$_POST[$k])
							$_POST[$k] = $v;
					}
				?>
				<label class="col1" for="date">Date</label><? selectarray('date', $flds_date, $_POST['date'], '--'); ?>
				<? 
				foreach(array('date_from'=>'depuis','date_to'=>"jusqu'�") as $k => $v)
				{
					echo '<label for="'.$k.'">'.$v.'</label>';
					echo '<input type="text" id="'.$k.'" name="'.$k.'" size="10" maxlength="10" value="'.$_POST[$k].'">';
				}
				?>
			</p>
			<p>
				<label class="col1" for="actif">Actif</label><? selectarray('actif', array('1'=>'oui','0'=>'non'), $_POST['actif'], '--');?>
			</p>
		</fieldset>
		<fieldset><legend>Localisation</legend>
			<p>
				<? if (false) : ?>
				<label class="col1" for="zone">Zone</label><? selectsql('zone', "select distinct z.id, z.nom from agence_promo_forfait p join zone z on z.id = p.zone order by nom", $_POST['zone'], '---');?>
				<? endif; ?>
				<label for="reseau">R�seau</label><? selectsql('reseau', "select '%' reseau, '(Tous)' nom union select distinct reseau, reseau from agence order by reseau", $_POST['reseau'], '---');?>
				<label for="code_societe">Soci�t�</label><? selectsql('code_societe', "select distinct a.code_societe, concat(a.code_societe, ' - ', a.societe) from agence_promo_forfait p join agence_pdv ap on ap.id=p.agence join agence a on a.id=ap.agence where p.zone='$zone' order by a.code_societe", $_POST['code_societe'], '---');?>
			</p>
			<p>
				<label class="col1" for="agence">Agence</label>
				<? selectsql('agence', "select '%' id, '(Toutes)' nom union select distinct ap.id id, concat(ap.id,' - ', ap.nom) nom from agence_promo_forfait p join agence_pdv ap on ap.id=p.agence where p.zone='$zone' order by id", $_POST['agence'], '---');?>
			</p>
		</fieldset>
		<fieldset><legend>V�hicule</legend>
			<p>
				<label class="col1" for="type">Type</label><? selectarray('type', array_merge(array('%'=>'(Tous)'), Categorie::$TYPE), $_POST['type'], '---');?>
				<label for="categorie">Cat�gorie</label><? selectsql('categorie', "select mnem, CONCAT(UCASE(type),' - ',mnem,' - ', nom) from categorie where publie=1 order by type, position, mnem", $_POST['categorie'], '---');?>
			</p>
		</fieldset>
		</td></tr>
		</table>
	</td>
</tr>
<tr>
	<td align="right">
		<input type="submit" name="search" value="rechercher" onclick="return doDisplay(this.form, 1);">
		&nbsp;<input type="submit" name="reset" value="effacer">
	</td>
</tr>
<tr>
	<td>
	<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
	<?
		// champ des agences
		$flds_form = array('reset','search','date_from','date_to','nb_rows','from_row','key','order');
	
		if (!$_POST['key']) $_POST['key'] = 'id';
		if (!$_POST['order']) $_POST['order'] = 'asc';
		// cr�er le sql
		$sql = "SELECT p.id, p.actif, p.perimetre, p.reseau, p.groupe, p.classe, p.code_societe, p.agence, p.type, p.categories, p.forfait, p.nom, p.debut, p.fin";
		$sql.=" FROM agence_promo_forfait p";
		$sql.=' WHERE '.count($_SESSION['admin_promo_search']);
		$sql.=" AND p.zone='$zone'";
		foreach ($_POST as $k => $v)
		{
			if (in_array($k, $flds_form) || !$v) continue;
			if (!preg_match('/^[a-z0-9_]+$/i', $k)) continue;
			// pour les dates
			if ($k == 'date')
			{
				if (!in_array($v, $flds_date)) continue;
				foreach(array('date_from'=>'>=','date_to'=>'<=') as $type => $op)
				{
					if ($d = $_POST[$type])
					{
						if ($type == 'date_to') $d .= ' 23:59:59';
						$sql .= " AND (p.{$_POST['date']} IS NULL OR p.{$_POST['date']} $op '".addslashes(euro_iso($d))."')";
					}
				}
			} else if ($k == 'campagne') {
				$sql.=" AND p.classe = ".(int)getsql('select classe from promotion_campagne where id='.(int)$_POST['campagne']);
			} else if ($k == 'coupon') {
				$sql.=" AND p.classe = ".(int)getsql(sprintf("select classe from partenaire_coupon where code='%s'", preg_replace('/((^0+)|[^a-z0-9\-\_\.])/i', '', $_POST['coupon'])));
			} else if ($k == 'categorie') {
				$sql.=' AND (p.categories IS NULL OR FIND_IN_SET("'.preg_replace("/[^a-z0-9\'\+]/i", '', $_POST['categorie']).'", categories))';
			} else if ($k == 'pdv' && !$_POST['agence']) {
				$sql.=" AND p.agence='".filter_var($_POST[$k], FILTER_SANITIZE_STRING)."'";
			} else if ($v == '%') {
				$sql.=" AND p.$k IS NULL";
			} else {
				$sql.=" AND p.$k='".filter_var($v, FILTER_SANITIZE_STRING)."'";
			}
		}
		$sql .= ' ORDER BY p.'.$_POST['key'].' '.$_POST['order'];
 //echo '<pre>'.$sql.'</pre>';
		// requ�te SQL
		$rs = sqlexec($sql);
		$cnt = mysql_num_rows($rs);
		echo "<caption>$cnt promotions</caption>";
		// pagination
		$range_rows = range(20,200,20);
		$nb_rows = $_POST['nb_rows'];
		if (!$nb_rows) $nb_rows = $range_rows[0];
		$from_row = $_POST['from_row'];
		$from_row = floor($from_row / $nb_rows) * $nb_rows + 1;
		if (!($from_row > 0)) $from_row = 1;
		
		// se d�placer dans les donn�es si c'est possible
		if ($cnt > $from_row)
			mysql_data_seek ($rs, $from_row - 1);
		else
			$from_row = 1;
		// afficher les r�sultats
		$i = 0;
		while ($row = mysql_fetch_assoc($rs))
		{
			$i++;
			if ($i > $nb_rows) break;
			if (!$first)
			{
				$first = true;
				
				// export des r�servation
				$fld_export = "p.*";
				$tbl_export = "";
				$sql_promo_export = preg_replace("/^select\s+(.+)from\s+(.+)\s+where /i", "select $fld_export\n from \\2 $tbl_export\nwhere ", $sql);

				// les boutons pour exporter
				echo '<tr><td colspan="'.count($row).'" align="right">';
					echo SQLExporter::getButton('export promos', 'promotions', $sql_promo_export);
				echo '</td></tr>';
				echo '<tr><td align="left">';
				if ($from_row > 1)
					echo '<a href="#" onclick="doDisplay(document.forms[\'frm_search\'], '.(int)($from_row-$nb_rows).')"><img src="img/left.gif" border="0"></a>';
				echo '</td><td colspan="'.(count($row)-2).'" align="center">';
				echo '<input type="hidden" name="from_row" value="'.$from_row.'"/>';
				echo '<input type="hidden" name="key" value="'.$_POST['key'].'"/>';
				echo '<input type="hidden" name="order" value="'.$_POST['order'].'"/>';
				echo 'lignes par pages : ';
				selectarray('nb_rows', array_combine($range_rows, $range_rows), $_POST['nb_rows'], null, 'onchange="doDisplay(this.form, '.$from_row.')" style="width: auto;"');
				echo ' page '.ceil($from_row / $nb_rows).'/'.ceil($cnt/$nb_rows);
				echo ' ('.$from_row.' - '.min($cnt, $from_row+$nb_rows-1).')';
				echo '</td><td align="right">';
				if ($cnt > $from_row + $nb_rows)
					echo '<a href="#" onclick="doDisplay(document.forms[\'frm_search\'], '.(int)($from_row+$nb_rows).')"><img src="img/right.gif" border="0"></a>';
				echo '</td></tr>';
				// �crire les en-t�tes
				echo '<tr>';
				foreach(array_keys($row) as $k)
				{
					echo '<th><a href="#" onclick="return doSort(\''.$k.'\',\''.($_POST['key']==$k && $_POST['order']=='asc' ? 'desc' : 'asc').'\')">';
					echo $k;
					if ($_POST['key']==$k)
						echo '&nbsp;<img align="absmiddle" border="0" src="img/'.($_POST['order']=='desc' ? 'down' : 'up').'.gif">';
					echo '</a></th>';
				}
				echo '</tr>';
			}
			$row['id'] = '<a href="agence_promo_forfait.php?id='.$row['id'].'">'.$row['id'].'</a>';
			echo '<tr><td>'.join('</td><td>', $row).'</td></tr>';
		}
	?>
	</table>
	</td>
</tr>
</table>
</form>
</body>
</html>
