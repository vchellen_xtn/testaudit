<?
	require_once("secure.php");
	require_once("lib.php");

	// identifiant
	$id = (int)getarg("id");
	if (!$id) $id = null;

	// suppression
	if ($id && getarg("delete"))
	{
		$sql = "DELETE FROM partenaire_classe WHERE id = '".$id."'";
		getsql($sql);
		unset($id);
	}

	// modification
	if (getarg("update") || getarg("create"))
	{
		if (isset($_POST['objectif']) && !isset(PartenaireClasse::$OBJECTIFS[$_POST['objectif']])) {
			unset($_POST['objectif']);
		}
		save_record("partenaire_classe", $_POST);
		if(!$id)
			$id = mysql_insert_id();
	}

	if ($id)
	{
		$rs = sqlexec("SELECT * FROM partenaire_classe WHERE id = '".$id."'");
		if (!$rs || !($row = mysql_fetch_assoc($rs)))
			die("Aucun enregistrement correspondant.");
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script language="javascript" src="../scripts/script.js"></script>
	<script>
	<!--

	// doDelete
	function doDelete(frm)
	{
		return (confirm("Voulez-vous r�ellement supprimer cet �l�ment ?\nCette op�ration est irr�versible."));
	}

	// doSubmit
	function doSubmit(frm)
	{
		var err = "";
		if(!frm.nom.value.length)
			err += "Veuillez indiquer le nom\n";
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		return true;
	}
	//-->
	</script>
</head>
<body>
<form action="" method="post" onsubmit="return doSubmit(this);" enctype="multipart/form-data">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre">
		<img src="img/puce.gif" align="absmiddle" hspace="1"> Classe
	</th>
</tr>

<tr>
	<td>
	<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="15%">
		<col width="32%">
		<col width="15%">
		<col width="33%">
		<tr>
			<td><label for="id">Id</label></td>
			<td><? echo $id.'<input type="hidden" id="id" name="id" value="'.$id.'" maxlength="6">';?> </td>
			<td><label for="objectif">Objectif</label></td>
			<td><? selectarray('objectif', PartenaireClasse::$OBJECTIFS, $row['objectif'], '--'); ?></td>
		</tr>
		<tr>
			<td><label for="nom">Nom</label></td>
			<td colspan="2"><input type="text" id="nom" name="nom" value="<?=htmlspecialchars($row['nom'])?>" maxlength="32" size="32"/></td>
			<td><em>client: page r�sultat</em></td>
		</tr>
		<tr>
			<td valign="top"><label for="description">Description</label></td>
			<td colspan="3">
				<textarea id="description" name="description" rows="1" style="width:100%"><?=htmlspecialchars($row['description'])?></textarea>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%">
		<tr>
			<td valign="top">
			<? if ($id) : ?>
			<input type="button" onclick="parent.list.location.href='lists/promotion_forfait.php?classe=<?=$id?>'" value="promos"/>
			<input type="button" onclick="parent.list.location.href='lists/partenaire_coupon.php?classe=<?=$id?>'" value="coupons"/>
			<input type="button" onclick="parent.list.location.href='lists/promotion_campagne.php?classe=<?=$id?>'" value="campagnes"/>
			<input type="button" onclick="parent.list.location.href='lists/promotion_negociation.php?classe=<?=$id?>'" value="n�gociations"/>
			<? endif; ?>
			</td>
			<td align="right" valign="top">
				<input type="submit" name="<?=$id ? "update" : "create"?>" value="enregistrer">
				<? if ($id 
					&& !getsql("select count(*) from partenaire_coupon where classe=$id") 
					&& !getsql("select count(*) from promotion_forfait where classe=$id") 
					&& !getsql("select count(*) from agence_promo_forfait where classe=$id")
					&& !getsql("select count(*) from promotion_campagne where classe=$id")
					&& !getsql("select count(*) from promotion_negociation where classe=$id")
				) :?>
				&nbsp;
				<input type="submit" onclick="return doDelete(this.form);" name="delete" value="supprimer">
				<? endif; ?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
</form>
</body>
</html>
