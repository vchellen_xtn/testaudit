<?
/**
* affiche facture et avoir � partir de Facturation 
* includt depuis toHtml()
*/
	/** @var Facturation */
	$x = $this;
	/** @var Reservation */
	$reservation = $x->getReservation();
	/** @var Agence */
	$agence = $reservation->getAgence();
	/** @var Categorie */
	$categorie = $reservation->getCategorie('id,mnem,nom');
	/** @var Client */
	$client = $reservation->getClient();
	
	$msg = '';
	if (!$agence)
		$msg .= "L'agence est inconnue<br/>";
	if (!$categorie)
		$msg.= "La cat�gorie est inconnue<br/>";
	if (!$reservation)
		$msg .= "La r�servation est inconnue<br/>";
	if (!$client)
		$msg .= "Le client est inconnu<br/>";
	if ($msg)
	{
		echo Page::showError($msg, '../client/index.html', 'Retour au compte client');
		return;
	}
?>
<h1 class="block-rouge">Facture</h1>
<div class="logo-ada">
	<img src="../img/logo.gif" alt="ADA" title="ADA" class="logo-impression left" />
	<p class="left">
		<strong><?=$x->getSociete()?><br/>
		22-28 rue Henri Barbusse<br/>
		92585 CLICHY CEDEX</strong>
	</p>
</div>
<div class="infos-client left clear">
	<h3>CLIENT :</h3>
	<p class="gris medium">
	<?
		$resInfo = $reservation->getResInfo();
		if (!$resInfo) $resInfo = $client;
		if ($resInfo['f_societe'])
			echo $resInfo['f_societe']."\n<br/>";
		echo $client['prenom'].' '.$client['nom'];
		echo "\n<br/>".$resInfo['f_adresse'];
		if ($resInfo['f_adresse2'])
			echo "\n<br/>".$resInfo['f_adresse2'];
		echo "\n<br/>".$resInfo['f_cp'].' '.$resInfo['f_ville'];
		echo "\n<br/>".$resInfo['f_pays_nom'];
	?>
	</p>
</div>
<!-- r�f�rence facture ou avoir -->
<p class="ref-facture right">
	<span class="strong"><?=$x->getType()?> N�<?=$x->getNumber()?></span><br />
	<span class="gris medium">Date <?=$x->getDate('d-m-Y')?></span>
</p>
<div class="tpl_page page-espace-client type-facturation">
	<div class="clear"></div>
	<!-- tarif de la r�servation -->
	<? if ($x->getData('facturation')=='ADA') { ?>
	<p>
		<strong><?=$reservation->getPrixForfait()?>&nbsp;�</strong>
	</p>
	<? } ?>
	<!-- r�f�rence de la r�servation -->
	<ul>
		<li><strong>R�servation Internet :</strong></li>
		<li class="gris">n�<?=$reservation->getResIDClient()?></li>
		<? if ($x['facturation'] != 'pro') : ?>
		<li><strong>Agence : </strong><span class="gris"><?=$agence->getHtmlAddress(', ', 'nom,adresse1,cp_ville')?></span></li>
		<li class="pad-top"><strong><?=($x['facturation']=='ada' ? 'Location' : 'P�riode garantie')?> </strong>: <span class="gris">du <?=$reservation->getDebut()?> au <?=$reservation->getFin()?></span></li>
		<? endif; ?>
	</ul>
	<? if ($x['facturation'] != 'pro') : ?>
	<ul>
		<!-- r�f�rence du v�hicule -->
		<li><strong>V�hicule : </strong><span class="gris"><<?=$categorie['nom']?></span></li>
		<li><strong>Kilom�tres inclus : </strong><span class="gris"><?=$reservation->getKm()?></span></li>
	</ul>
	<? endif; ?>

	<table class="details-facturation">
		<colgroup>
			<col width="500" /> 
			<col width="200" /> 
		</colgroup>
		<!-- Taxe Gare / A�roport -->
		<? if ($x['facturation']=='ada') : ?>
		<tr>
			<td><strong>Prix du forfait :</strong></td>
			<td align="right"><strong class="rouge"><?=number_format($reservation->getPrixForfait(), 2);?>&nbsp;�</strong></td>
		</tr>
		<? if ($reservation['surcharge'] > 0) : ?>
		<tr>
			<td><strong><?=$agence->getSurchargeLabel()?> :</strong></td>
			<td align="right">
				<strong class="rouge"><?=number_format($reservation["surcharge"], 2)?>&nbsp;�</strong>
			</td>
		</tr>
		<? endif; ?>
		<? endif; ?>
		<tr>
			<td colspan="2">
				<strong>
				<?
					if ($x['facturation']=='pro')
						echo "Frais d'adh�sion compte pro";
					else if ($x['facturation']=='courtage')
						echo "Garantie(s) d'assurance souscrite(s)";
					else if ($x['facturation']=='materiel')
						echo 'Mat�riel r�gl� en ligne';
					else
						echo 'Option(s) r�gl�e(s) en ligne';
				?>
				</strong> :
			</td>
		</tr>
		<?
		/*
		** AFFICHAGE DES OPTIONS
		*/
		$options = $reservation->getResOptions()->filter($x['facturation'],'L');
		if ($x['facturation']=='ada' && $options->isEmpty())
			echo '<tr><td colspan="2">Aucune</td></tr>';
		$theme = null;
		foreach($options as /** @var ResOption */ $option) :
		if ($theme != $option['theme']) :
			$theme = $option['theme'];
		?>
		<tr>
			<td colspan="2">
				<strong><?=$option['theme_nom']?></strong>
			</td>
		</tr>
		<?
		endif;
		?>
		<tr>
			<td class="medium">
				<? if ($option['quantite']) echo $option['quantite']; ?>
				<? echo $option['nom']?>
			</td>
			<td align="right">
				<strong class="rouge"><?=$option->getPrix()?></strong>
			</td>
		</tr>
		<? endforeach; ?>
		<?
		if ($x['facturation']!='pro' && $code = $reservation->getPromotionCode())
		{
			$reduction = $code['reduction'];
			$base = array
			(
				'ada'		=> $reservation->getPrixForfait() + $reservation->getPrixSurcharge() + $reservation->getResOptions()->getTotal('ADA', 'L'),
				'materiel'	=> $reservation->getResOptions()->getTotal('MATERIEL', 'L'),
				'courtage'	=> $reservation->getResOptions()->getTotal('COURTAGE', 'L'),
			);
			foreach($base as $k => $v)
			{
				if ($k == $x['facturation'])
				{
					$reduction = min($v, $reduction);
					break;
				}
				$reduction = max(0, $reduction - $v);
			}
		}
		?>
		<? if ($reduction) : ?>
		<tr>
			<td><strong>R�duction <?=$code['campagne_nom']?> :</strong></td>
			<td align="right"><strong class="rouge">-<?=number_format($reduction, 2);?>&nbsp;�</strong></td>
		</tr>
		<? endif; ?>
	</table>
	<!-- bloc prix total - debut -->
	<table class="right details-facturation">
		<colgroup>
			<col width="235" />
			<col width="90" />
		</colgroup>
<?
		if ($x['facturation'] == 'ada' || $x['facturation'] == 'pro')
		{
			$facture = ($x['type'] == 'F' ? $x : $reservation->getFacturations()->find('ada','F'));
?>
		<tr>
			<td class="align-right"><strong>Total HT :</strong></td>
			<td align="right" class="rouge"><strong><?=sprintf("%01.2f", $facture->getMontantHT())?>&nbsp;�</strong></td>
		</tr>
		<? if ($x['type']=='A') { // d�but avoir ?>
		<tr>
			<td class="align-right"><strong>Frais de dossier HT :</strong></td>
			<td align="right" class="rouge"><?=sprintf("%01.2f", $facture->getMontantHT() - $x->getMontantHT())?>&nbsp;�</td>
		</tr>
		<tr>
			<td class="align-right"><strong>Remboursement HT :</strong></td>
			<td align="right" class="rouge"><strong><?=sprintf("%01.2f", $x->getMontantHT())?>&nbsp;�</strong></td>
		</tr>
		<? } // fin avoir ?>
		<tr>
			<td class="align-right"><strong>TVA <?=Tva::format($x->getTVARate());?> :</strong></td>
			<td align="right" class="rouge"><strong><?=sprintf("%01.2f", $x->getTVA())?>&nbsp;�</strong></td>
		</tr>
		<tr>
			<td class="align-right pad-bot-5"><strong>Total<?=($x['type']=='A' ? ' remboursement ':' ')?>TTC :</strong></td>
			<td align="right" class="rouge pad-bot-5"><strong><?=sprintf("%01.2f", $x->getMontant())?>&nbsp;�</strong></td>
		</tr>
<?
		}
		else if ($x['facturation'] == 'courtage')
		{
?>
		<tr>
			<td class="align-right"><strong>Cotisation(s) d'assurance TTC :</strong></td>
			<td align="right" class="rouge"><strong><?=sprintf("%01.2f", $x->getMontant())?>&nbsp;�</strong></td>
		</tr>
		<tr>
			<td colspan="2" align="center">(taxe d'assurance 9% inclus)</td>
		</tr>
<?
		}
?>
	</table>
	<div class="clear"></div>
	<!-- bloc prix total - fin -->
	<? if ($x['type'] == 'F') { ?>
	<p class="type-paiement">
		La totalit� de ce montant a d�j� �t� r�gl� en ligne par carte bancaire le <?=$x->getDate('d-m-Y')?>
	</p>
	<? } else { ?>
	<p class="type-paiement">
		Ce montant a d�j� �t� rembours� par virement le <?=$x->getDate('d-m-Y')?>
	</p>
	<? } ?>
	<table class="right details-facturation">
		<colgroup>
			<col width="205">
			<col width="90">
		</colgroup>
		<tr>
			<td class="align-right"><strong>D�j� <?=($x['type']=='F' ? 'r�gl�' : 'rembours�')?> TTC :</strong></td>
			<td align="right" class="rouge"><strong><?=sprintf("%01.2f", $x->getMontant())?>&nbsp;�</strong></td>
		</tr>
		<tr>
			<td class="align-right pad-bot-5"><strong>Solde d� :</strong></td>
			<td align="right" class="rouge pad-bot-5"><strong><?=sprintf("%01.2f", 0)?>&nbsp;�</strong></td>
		</tr>
		<tr class="border-top">
			<td class="align-right"><strong>Net � payer en euros TTC :</strong></td>
			<td align="right" class="rouge"><strong><?=sprintf("%01.2f", 0)?>&nbsp;�</strong></td>
		</tr>
	</table>	
	<div class="clear"></div>
	
<?
// INFORMATIONS REGLEMENTAIRES
if ($x['facturation']=='courtage') { ?>
	<? $tPaiement = $reservation->getDate('paiement', 'U'); ?>
	<? if ($tPaiement < strtotime('2014-01-01')) : ?>
	<p class="facturation-courtage">
		ADA COURTAGE soci�t� de courtage en Assurances - 22/28 RUE HENRI BARBUSSE 92110 CLICHY
		<br/>SARL UNIPERSONNELLE au capital de 7 500 �
		<br/>515 319 671 RCS NANTERRE - N�TVA intracommunautaire FR88515319671
		<br/>N� ORIAS : 09 052 988 www.orias.fr - APE 6622 Z
	</p>
	<? endif; ?>
	<p class="facturation-courtage-small">
		<? if ($tPaiement < strtotime('2013-01-01')) : ?>
		Les garanties sont g�r�es par le groupe VERSPIEREN, Soci�t� de Courtage en Assurances domicili� au 1, avenue Fran�ois Mitterrand 59290 Wasquehal � SA � Directoire et Conseil de Surveillance au capital de 1 000 000� - SIREN n� 321 502 049 � RCS de Roubaix Tourcoing � N� ORIAS : 07 001 542 www.orias.fr.
		<br/><br/>Le contrat d�assurance "remboursement de franchise" n�201016FR4281 est souscrit aupr�s de AMG Assurances � 25 rue de Li�ge, 75008 PARIS � RCS Paris 499 589 190 � N� ORIAS 07 036 604, agence de souscription en assurances dommages, d�ment habilit�e � agir pour le compte de GOTHAER Allgemeine Versicherung AG, Gothaer allee 1 � D-50969 KLN � Soci�t� Mutuelle d�assurances � cotisations fixes � registre du Commerce de Cologne HRB 35474, num�ro CCA VU 5531.
		<br/><br/>Le contrat d�assurance "marchandises transport�es et effets personnels" n�91-701 est souscrit aupr�s de HELVETIA, Compagnie Suisse d�Assurances, Soci�t� anonyme de droit suisse au capital de 77 480 000 francs suisses enti�rement lib�r�, immatricul�e sous le n�CH � 320.3.001.013.8 � si�ge social : 10 Dufourstrasse, Saint Gall, Suisse. Cette compagnie est repr�sent�e par sa succursale fran�aise, Helvetia Assurances, Direction pour la France, 2 rue Sainte-Marie 92415 Courbevoie Cedex, entreprise priv�e r�gie par le Code des Assurances, immatricul�e au RCS de Nanterre sous le n�775 753 072. SIRET : 775 753 072 00112 � APE : 660 E
		<br/><br/>VERSPIEREN, ADA COURTAGE, AMG et HELVETIA sont soumises au contr�le de l'Autorit� de Contr�le Prudentiel (ACP)
		<? elseif ($tPaiement < strtotime('2014-01-01')) : ?>
		Les garanties sont g�r�es par CG ASSURANCES Soci�t� de Courtage en Assurances domicili�e au 4, rue de Rome � 75008 Paris - SARL au capital de 15.244,90 � - SIREN n� 342 420 213 �N� ORIAS 07 024 104 - www.orias.fr. Le contrat d'assurance est souscrit aupr�s d'Axeria IARD � 27, rue Maurice Flandin � 69444 Lyon Cedex 03 � RCS Lyon 352 893 200 � S.A. au capital de 38.000.000 �.
		<? else : ?>
		Les garanties sont g�r�es par GESTINEO SAS de courtage en assurances au capital de 100000 �, 1 avenue Fran�ois Mitterand 59290 Wasquehal,RCS LILLE Metropole 533643698,N�ORIAS:11062867(www.orias.fr). Le contrat d'assurance est souscrit aupr�s de FOCUS INSURANCECOMPANY Ltd, PO BOX1338, 1st Floor, Grand Ocean Plaza, Ocean Village, Gibraltar. Gibraltar Financial Services Commission under authorisation number 96218
		<? endif; ?>
	</p>
	<div class="clear"></div>
<? } // !fin des mentions ADA COURTAGE ?>
	<a href="../client/compte.html" class="btn no-transform" style="margin:50px 0 0 150px;">Revenir � vos r�servations</a>	
	<a href="javascript:window.print()" class="btn right" style="margin-top:50px;">
		<? if ($x['type']=='F') { ?>
			Imprimez votre facture
		<? } else { ?>
			Imprimez votre avoir
		<? } ?>
	</a>
	
	<div class="clear"></div>
	<p class="print">
		ADA - 22/ 28 rue Henri Barbusse 92585 Clichy Cedex<br />
		S.A. au capital de 2 543 619,76 �<br />
		338 657 141 8CS Nanterre - N� TVA Intracommunautaire FR31338657141
	</p>
</div>