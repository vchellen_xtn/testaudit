<?
class Client_Collection extends Ada_Collection
{
	/**
	* Renvoie les clients g�rant un partenaire
	* 
	* @param Partenaire $partenaire
	* @return Client_Collection
	*/
	static public function factory(Partenaire $partenaire)
	{
		$x = new Client_Collection();
		if ($partenaire)
			$x->_loadFromSQL('select c.* from client c join partenaire_client pc on pc.client=c.id where pc.partenaire='.$partenaire->getId());
		return $x;
	}
}
?>
