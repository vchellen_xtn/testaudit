<?
	$title 	 = "Option";
	$url 	 = "../option.php";
	$key	 = "o.id";
	$order	 = "";

	$sql = "select o.id, o.type, o.theme, o.position, o.nom";
	$sql.= "	  ,case paiement when 'A' then 'en agence' when 'L' then 'en ligne' when 'N' then 'non pr�sent�' else '--' end as paiement";
	$sql.= "	  ,case tarification when 'F' then 'forfaitaire' when 'K' then 'au km' when 'J' then 'par jour' when 'U' then 'unitaire' else '--' end as tarification";
	$sql.= " from `option` o";
	if ($theme = filter_var($_GET['theme'], FILTER_SANITIZE_STRING))
		$sql.=" where '$theme' in (o.theme, o.groupe)";
	
	$out_url = "list.php?sql=" . urlencode($sql);
	$out_url.= "&title=" . urlencode($title);
	$out_url.= "&url=" . urlencode($url);
	$out_url.= "&new_url=" . urlencode($url . "?" . $_SERVER['QUERY_STRING']);
	$out_url.= "&color=4587ba";
	$out_url.= "&key=".$key."&order=".$order."&nb_rows=30";
	header("location: ".$out_url);
?>
