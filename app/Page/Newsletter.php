<?
class Page_Newsletter extends Page
{
	protected function prepareHead()
	{
		parent::prepareHead();
		$this->addJS('scripts/jquery.validate.min.js');
	}
	
	// d�sabonnement � la newsletter
	public function annulationAction()
	{
		if ($email = filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
		{
			// prospect : d�sabonner de la newsletter
			if ($prospect = Prospect::factory($email))
			{
				if (!$prospect->isEmpty())
				{
					$prospect->setData('desabo', 1)->save('desabo');
					$this->setData('email', $email);
				}
			}
		}
		// le compte client
		if ($client = Client::factory($email))
		{
			if (!$client->isEmpty())
			{
				$client->setData('promo', 'N')
					->setData('partenaire', 'N')
					->save('promo,partenaire');
				$this->setData('email', $email);
			}
		}
		if ($this->hasData('email'))
			$this->setTrackPage('newsletter_desinscription');
	}
	
	// enregistre le formulaire de newsletter/index
	public function indexAction()
	{
		if (!count($_POST)) return;
		
		// on essaie de cr�er un prospect moyenne dur�e
		$errors = array();
		$_POST['origine'] = 'ADAFR';
		$x = Prospect::create($_POST, $errors);
		if ($x)
		{
			$this->setData('is_form_valid', true);
			$this->setTrackPage('newsletter_confirmation');
		}
		else if (count($errors))
			$this->_addMessage($errors);
	}
}
?>
