<?php
class PayboxAbonne_Collection extends Ada_Collection
{
	/**
	* Renvoie la liste des cartes d'un client
	* 
	* @param Client $client
	* @return PayboxAbonne_Collection
	*/
	static public function factory(Client $client)
	{
		$x = new PayboxAbonne_Collection();
		$sql = "select * from paybox_abonne where client=".$client->getId();
		$x->loadFromSQL($sql);
		return ($x->isEmpty() ? null : $x);
	}
}
?>
