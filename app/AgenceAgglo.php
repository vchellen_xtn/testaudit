<?php
class AgenceAgglo extends Ada_Object
{
	/**
	* Renvoie une AgenceAgglo
	* 
	* @param int $id
	* @return AgenceAgglo
	*/
	static public function factory($id)
	{
		$x = new AgenceAgglo();
		if (!is_numeric($id))
			$x->setIdFieldName('nom');
		$x->load($id);
		$x->setIdFieldName('id');
		return ($x->isEmpty() ? null : $x);
	}
	
	/**
	* Renvoyer le d�partement associ�
	* @returns Dept
	*/
	public function getDept()
	{
		return Dept::factory($this->_data['dept']);
	}
}
?>
