<div class="bg">
	<div class="header">
		<div class="univers_titre"><?=$this->getUniversTitre();?></div>
		<? if ($navResa = $this->getNavResa()) : ?>
		<? $i = 1; ?>
		<a href="../" class="new-search rouge">&gt; Nouvelle recherche</a>
		<ol class="nav-resa nav-resa<?=count($navResa)?>">
			<? foreach($navResa as $k => $title) : ?>
			<li>
				<span class="<? if ($k == $this->getAction()) echo 'current ';?>item-chemin-resa"><span class="number"><?=$i++?></span> <?=$title?></span>
			</li>
			<? endforeach; ?>
		</ol>
		<? endif; ?>
	</div>
	<div class="content tpl_reservation">
	<?
		include($this->getPagePath());
	?>
		<div class="clear"></div>
	</div>
</div>