<?
	require_once('secure.php'); 
	require_once('lib.php');

	// identifiant
	$id = (int) getarg('id');
	if (!$id) $id = null;
	
	// suppression
	if ($id && getarg('delete')) 
	{
		$sql = 'delete from promotion_negociation where id = '.$id;
		getsql($sql);
		unset($id);
	}
	
	// modification, cr�ation
	if (getarg('update') || getarg('create'))
	{
		// on remet les cat�gories dans un seul champ
		if (is_array($_POST['categories'])) {
			$_POST['categories'] = implode(',', $_POST['categories']);
		}
		save_record('promotion_negociation', $_POST);
		if (!$id) $id = mysql_insert_id();
	}
	if ($id)
	{
		$rs = sqlexec('select * from promotion_negociation where id = '.$id);
		if (!$rs || !($row = mysql_fetch_assoc($rs)))
			die("Aucun enregistrement correspondant.");
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script type="text/javascript" src="<?=Page::getProtocol()?>://www.google.com/jsapi"></script>
	<script type="text/javascript">
		google.load('jquery', '1.4');
	</script>
	<link rel="stylesheet" type="text/css" href="../css/anytime.min.css" />
	<script type="text/javascript" src="../scripts/anytime.min.js"></script>
	<script type="text/javascript" src="../scripts/script.js"></script>
	<script type="text/javascript" src="admin.js"></script>
	<script>
	// <![CDATA[
	// doDelete
	function doDelete(frm)
	{
		return (confirm("Voulez-vous r�ellement supprimer cet �l�ment ?\nCette op�ration est irr�versible."));
	}
	
	// doSubmit
	function doSubmit(frm)
	{
		var err = "";
		var cntCat = countCheckboxes('categories[]', frm);
		// on ne peut pr�ciser un nombre que pour une cat�gorie et une agence...
		if (!$.trim(frm.nom.value).length)
			err += "Vous devez pr�ciser un nom\n";
		if (frm.classe.selectedIndex < 1) {
			err+="Vous devez pr�ciser les promotions associ�es\n";
		}
		if (frm.jour.selectedIndex < 1) {
			err+="Vous devez pr�ciser le jour des n�gociations\n";
		}
		if (frm.type.selectedIndex < 1) {
			err+="Vous devez pr�ciser le type de v�hicule\n";
		}
		if (cntCat == 0)
			err += "Vous devez pr�ciser au moins une cat�gorie pour cette n�gociation\n";
		
		if (!err.length) return true;
		alert("Attention !\n" + err);
		return false;
	}

	// initilisation AnyTime
	var dateFormat = '%d-%m-%Y';
	var dateOptions = getOptionsAnyTime(dateFormat, "Depuis");
	var dateConverter = new AnyTime.Converter({format: dateFormat});

	// onload
	$(document).ready(function()
	{
		AnyTime.picker('depuis', dateOptions);
		dateOptions.labelTitle = "Jusqu'�";
		AnyTime.picker('jusqua', dateOptions);
		$('#clear_depuis').click(function(e){$('#depuis').val('');});
		$('#clear_jusqua').click(function(e){$('#jusqua').val('');});
		$('#type').change(function(){
			$('tr.tr_type').hide();
			$('tr.tr_type :checkbox').each(function()
			{
				this.checked = false;
			});
			if (v = $(this).val())
				$('#type_'+v).show();
		});
	});
	// ]]>
	</script>
	<style>
		#depuis, #jusqua, #clear_depuis, #clear_jusqua { width: inherit; }
	</Style>
</head>
<body>
<form action="" method="post" onsubmit="return doSubmit(this);">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre">
		<img src="img/puce.gif" align="absmiddle" hspace="1">
		<?=($id ? "N�gociation [#".$id."]" : "Nouvelle N�gociation");?>
	</th>
</tr>
<tr><td>
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="10%">
		<col width="40%">
		<col width="10%">
		<col width="40%">
		<tr>
			<td>ID</td>
			<td><?=$id;?><input type="hidden" name="id" value="<?=$id;?>"/></td>
			<td><label for="actif">Actif</label></td>
			<td>
				<input type="checkbox" id="actif" name="actif[]" value="1" <?=(!$id || $row['actif'] ? 'checked="checked"' : '');?> style="width: auto;">
				<input type="hidden" name="actif[]" value="0">
			</td>
		</tr>
		<tr>
			<td><label for="nom">Nom</label></td>
			<td colspan="3"><input type="text" id="nom" name="nom" value="<?=htmlspecialchars($row['nom']);?>" maxlength="128" size="64"/></td>
		</tr>
		<tr>
			<td valign="top"><label for="commentaires" title="pour le backoffice">Commentaires</label></td>
			<td colspan="3">
				<textarea id="commentaires" name="commentaires" rows="1" style="width:100%"><?=htmlspecialchars($row['commentaires'])?></textarea>
			</td>
		</tr>
		<tr><th colspan="4">Dates et Promotions</th></tr>
		<tr>
			<td><label for="depuis">Depuis</label></td>
			<td>
				<input autocomplete="off" type="text" name="depuis" id="depuis" value="<?=euro_iso($row['depuis']);?>" maxlength="16" size="16"/>
				<input type="button" id="clear_depuis" value="effacer"/>
			</td>
			<td><label for="jusqua">Jusqu'�</label></td>
			<td>
				<input autocomplete="off" type="text" name="jusqua" id="jusqua" value="<?=euro_iso($row['jusqua'])?>" maxlength="16" size="16"/>
				<input type="button" id="clear_jusqua" value="effacer"/>
			</td>
		</tr>
		<tr>
			<td><label for="jour">Jour</label></td>
			<td><? selectarray('jour', $JOURS, $row['jour'], '-- choisissez --'); ?></td>
			<td><label for="classe">Promotions</label></td>
			<td><? selectsql('classe', "select id, concat(objectif, ' - ', nom) nom from partenaire_classe where objectif='negociation' order by nom", ($id ? $row['classe'] : (int) $_GET['classe']), '-- choisissez --');?></td>
		</tr>
		<tr><th colspan="4">Quels V�hicules ?</th></tr>
		<tr>
			<td><label for="type">Type</label></td>
			<td><? selectarray('type', Categorie::$TYPE, $row['type'], '-- choisissez un type --'); ?>	</td>
			<td colspan="2"></td>
		</tr>
		<?
		// afficher la liste des cat�goires pour chaque type
		foreach(Categorie::$TYPE as $k => $v)
		{
			echo '<tr class="tr_type" id="type_'.$k.'"';
			if (!$id || $row['type']!=$k)
				echo' style="display: none;"';
			echo '>';
			echo '<td></td>';
			echo '<td colspan="3">';
			$categories = array();
			if ($id) $categories = explode(',', $row['categories']);
			foreach(Categorie_Collection::factory('fr', $k) as $x)
			{
				$checked = '';
				if ($id && $row['type']==$k && in_array($x['mnem'], $categories)) {
					$checked = ' checked="checked"';
				}
				echo '<input type="checkbox" name="categories[]" id="categorie_'.$x['id'].'" value="'.$x['mnem'].'" '.$checked.'/>';
				echo '<label for="categorie_'.$x['id'].'" title="'.(string)$x.'">'.$x['mnem'].'</label>';
			}
			echo '<br/>';
			echo '<a href="#" onclick="return doCheckbox(this, true);">cocher</a>&nbsp;&nbsp;';
			echo '<a href="#" onclick="return doCheckbox(this, false);">d�cocher</a>';
			echo '</td>';
			echo '</tr>';
		}
		?>
		<? if ($id) : ?>
		<tr>
			<? foreach (array('creation'=>'cr�ation','modification'=>'modification') as $k => $v) : ?>
			<td><?=$v?></td><td><?=euro_iso($row[$k]);?></td>
			<? endforeach; ?>
		</tr>
		<? endif; ?>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%">
		<tr>
			<td valign="top">
			<? if ($id) : ?>
			<input type="button" onclick="parent.list.location.href='lists/promotion_forfait.php?classe=<?=$row['classe']?>'" value="promotions"/>
			<? endif; ?>
			</td>
			<td align="right" valign="top">
				<input type="submit" name="<?=$id ? "update" : "create"?>" value="enregistrer">
				<? if ($id) : ?>
				&nbsp;
				<input type="submit" onclick="return doDelete(this.form);" name="delete" value="supprimer">
				<? endif; ?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
</form>
</body>
</html>
