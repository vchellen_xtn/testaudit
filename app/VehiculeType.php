<?php
class VehiculeType extends Ada_Object
{
	/**
	* Cr�e un v�hicule type
	* 
	* @param string $id
	* @return VehiculeType
	*/
	static public function factory($id)
	{
		$x = new VehiculeType();
		if (isset(Categorie::$TYPE[$id])) {
			$x->load($id);
		}
		if ($x->isEmpty()) return null;
		return $x;
	}
	public function __toString() {
		return Categorie::$TYPE_LBL[$this->getId()];
	}
}
?>
