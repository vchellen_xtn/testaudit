<?
	require_once(BP."/lib/lib.tcp.php");
	if (USE_PAYPAL_SANDBOX)
	{
		define ('PAYPAL_API_ID', 'ada-paypal_api1.rnd.fr');
		define ('PAYPAL_API_PWD', 'C5LKXTJ3K4XTLHNB');
		define ('PAYPAL_API_SIGNATURE', 'A4zzCKwy91gKr4UJdo8OLDNdSLeOAh6r0v2q9nuBd9LRI2XlR4adX7JI');
		define ('PAYPAL_API_SERVER', 'https://api-3t.sandbox.paypal.com');
		define ('PAYPAL_SITE_URL', 'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout');
		define ('PAYPAL_SITE_URL', 'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout');
	}
	else
	{
		define ('PAYPAL_API_ID', '**� d�finir**');
		define ('PAYPAL_API_PWD', '**� d�finir**');
		define ('PAYPAL_API_SIGNATURE', '**� d�finir**');
		define ('PAYPAL_API_SERVER', 'https://api-3t.paypal.com');
		define ('PAYPAL_SITE_URL', 'https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout');
	}
	define ('PAYPAL_API_URL', '/nvp');

/*	TODO
	GetTransactionsDetail
	TransactionSearch
*/
class Paypal
{
	// Checkout
	static public function SetExpressCheckout($description, $montant, $base, $url_ok, $url_cancel, $user_data=null)
	{
		$result = self::_callNVP(
			'SetExpressCheckout',
			array (
				 'AMT'			=> $montant
				,'CURRENCYCODE'	=> 'EUR'
				,'CUSTOM'		=> $user_data
				,'RETURNURL'	=> $base.$url_ok
				,'CANCELURL'	=> $base.$url_cancel
				,'DESC'			=> $description
				,'HDRIMG'		=> $base.'../img/logo.gif'
				,'HDRBORDERCOLOR' => 'ff0000'
				,'PAYMENTACTION' => 'sale'
			)
		);
		$token = $result['TOKEN'];
		header('Location: '.PAYPAL_SITE_URL.'&token='.$token);
		return $token;
	}

	// Review
	static public function GetExpressCheckoutDetails($token)
	{
		$result = self::_callNVP('GetExpressCheckoutDetails', array ('TOKEN' => $token));
		return $result;
	}

	// Payment
	static public function DoExpressCheckoutPayment($token, $payer_id, $montant, $description = '')
	{
		$result = self::_callNVP(
			'DoExpressCheckoutPayment',
			array (
				 'AMT'			=> $montant
				,'CURRENCYCODE' => 'EUR'
				,'DESC' => $description
				,'TOKEN' => $token
				,'PAYERID' => $payer_id
				,'PAYMENTACTION' => 'sale'
			)
		);
		return $result;
	}

	// GetTransactionDetails
	static public function GetTransactionDetails ($transaction)
	{
		$result = self::_callNVP(
			'GetTransactionDetails',
			array (
				 'TRANSACTIONID' => $transaction
			));
		return $result;
	}

	// PP_Refund
	static public function RefundTransaction($transaction, $montant, $note=null)
	{
		$result = self::_callNVP(
			'RefundTransaction',
			array (
				 'TRANSACTIONID'	=> $transaction
				,'REFUNDTYPE'		=> "Partial"
				,'AMT'				=> $montant
				,'CURRENCYCODE'		=> 'EUR'
				,'NOTE'				=> $note
			));
		return $result;
			// REFUNDTRANSACTIONID Unique transaction ID of the refund.
			// FEEREFUNDAMT Transaction fee refunded to original recipient of payment.
			// GROSSREFUNDAMT Amount of money refunded to original payer.
	}

	// call_paypal_nvp
	static protected function _callNVP($method, $args)
	{
		$std_args = array(
			 'USER'		=> PAYPAL_API_ID
			,'PWD'		=> PAYPAL_API_PWD 
			,'SIGNATURE'=> PAYPAL_API_SIGNATURE
			,'METHOD'	=> $method
			,'VERSION'	=> '3.2'
			,'LOCALECODE'=> 'FR'
			);
		$result = @get_http_request(PAYPAL_API_SERVER, PAYPAL_API_URL.'?'.http_build_query(array_merge($std_args, $args)), null, $cookie, 'POST');
		$a = explode("&", $result);
		foreach ($a as $val)
		{
			list($key, $val) = explode("=", $val);
			$res[$key] = urldecode($val);
		}
		if (preg_match('/^Success/i', $res['ACK']))
		{
			return $res;
		}
		else
		{
			// on envoie un mail pour notification
			$txt = "Paypal: ".$method."\n\nArguments:\n";
			foreach (array_merge($std_args, $args) as $k => $v)
				$txt .= "\t$k:\t$v\n";
			$txt.= "\n\nR�ponse:\n";
			foreach ($res as $k => $v)
				$txt .= "\t$k:\t$v\n";
			send_mail ($txt, EMAIL_PAYPAL_DEBUG, '[ADA] Erreur PayPal sur '.$_SERVER['SERVER_NAME'], EMAIL_FROM, EMAIL_FROM_ADDR, null, ''); 
			// on arr�te l� en affichant l'erreur
			echo '<div style="color:red"><pre>PAYPAL ERROR :'."\n";
			print_r($res);
			echo '</pre></div>';
			exit();
		}
	}
}
?>
