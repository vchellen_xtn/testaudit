<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?
	$this->addCSS($this->getProtocol().'://fonts.googleapis.com/css?family=Raleway:700,600,500,400');
	$this->writeHead();
?>
<? if (USE_ABTASTY) : /* ABTesting : https://app.abtasty.com/ */ ?>
	<script type="text/javascript" src="//try.abtasty.com/a1b0a7dd1812a39ae90024dcf53a9ae9.js"></script>
<? endif; ?>
<? if (USE_WEB2ROI) : /* Web2ROI : http://rnd.web2roi.com/ */ ?>
	<script type="text/javascript">
	/* <![CDATA[  */
	(function() {
		window.sy = {
			uid : "adafr"
			,usid : ""
		};
		var w2r = document.createElement('script'); w2r.type = 'text/javascript'; w2r.async = true;
		w2r.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 't.sytsem.com/v2/tracker.js';
		var sw2r = document.getElementsByTagName('script')[0]; sw2r.parentNode.insertBefore(w2r, sw2r);
	})();
	/* ]]> */
	</script>
<? endif; ?>
	<script type="text/javascript">
	// <![CDATA[
	var appADA = appADA || {};
	appADA.baseUrl = '<?=Page::getURL(null, null, (self::getProtocol()=='https'));?>';
	if (typeof $ !== "undefined")
	{
		// gestion de la nav principale
		$(document).ready(function()
		{
			$('#menu--header .dropdown-trigger').on('click mouseenter', function(){
				$(this).addClass('active')
				.parent('li').addClass('li-active');
				$(this).siblings('.dropdown-menu--header').stop(true, true).fadeIn(100);

				$('#menu--header .li-active').on('click mouseleave', function(){
					$(this).removeClass('li-active');
					$('.dropdown-trigger', this).removeClass('active')
					.siblings('.dropdown-menu--header').stop(true, true).fadeOut(200);
				});
				return false;
			});

			// toggle esp-client
			$('#smenu--header, #smenu--header .smenu--espace-client').hide();
			$('.menu--header  .trigger-toggle').click(function(){
				$smenu = $('#smenu--header, #smenu--header .smenu--espace-client');
				if (!$smenu.length) return true;
				$smenu.stop(true, true).slideToggle();
				return false;
			});

			// le formulaire de connexion
			$("#login-form--header").submit(function() {
				// on v�rifie les 2 champs
				var ok = true;
				if (!validateEmail(this.email)) {
					$(this.email).addClass("error");
					ok = false;
				} else {
					$(this.email).removeClass("error");
				}
				if (!this.pwd.value.length) {
					$(this.pwd).addClass("error");
					ok =false;
				} else {
					$(this.pwd).removeClass("error");
				}
				if (!ok) return false;
				// soumettre le formulaire en AJEX
				var frm = this;
				var url = this.action;
				$.post(url.replace('client/index.html', 'login.php?action=login').replace(/^https?\:/, window.location.protocol), $(this).serialize(), function (xml) {
					if (xml) {
						$xml = $(xml);
						if ($xml.find("error").length) {
							alert($xml.find("error").text());
						} else {
							// remplir la zone de connexion
							$auth = $("#customer_connected");
							$auth.find("span.prenom").text($xml.find("prenom").text());
							$auth.find("span.nom").text($xml.find("nom").text());
							$auth.find("a.logout").attr("title", "Vous �tes " + $xml.find("email").text());
							// montrer la connexion et cacher le formulaire
							$auth.show();
							$(frm).hide();
						}
					}
				});
				return false;
			});
		});
	}
	// ]]>
	</script>
	<meta property="og:image" content="http://www.ada.fr/css/img/structure-2015/logo-ada.jpg" />
</head>
<body class="<?=dirname($this->getId()).' '.basename($this->getId());?>">
	<div id="fb-root"></div>
	<script>
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.5";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
	<div id="header">
		<div class="inner--header">
			<!-- menu principal -->
			<ul class="menu--header clearfix" id="menu--header">
				<li>
					<? if ($page == 'reservation/index') echo '<h1>'; ?>
					<a href="<?=$here?>" class="logo"><img src="<?=$baseurl?>css/img/structure-2015/logo-ada-small.gif" alt="location de voiture et utilitaire - ADA.fr" /></a>
					<? if ($page == 'reservation/index') echo '</h1>'; ?>
				</li>
				<li><a href="<?=$here?>agences/index.html" class="trouver-agence">Agences</a></li>
				<? if (false): ?>
				<li><a href="<?=$here?>location-voiture-promo.html" class="item2">Promotions</a></li>
				<? endif; ?>
				<li><a href="<?=$here?>vehicules/index.html#dropdown-vehicules" class="dropdown-trigger" rel="nofollow">V�hicules</a>
					<ul id="dropdown-vehicules" class="dropdown-menu--header">
						<li>
							<a href="<?=$here?>vehicules/tourisme.html">voiture de tourisme</a>
						</li>
						<li>
							<a href="<?=$here?>vehicules/utilitaires.html" rel="noindex, nofollow">v�hicules utilitaires</a>
						</li>
						<li>
							<a href="<?=$here?>vehicules/sans_permis.html">v�hicules sans permis</a>
						</li>
						<li>
							<a href="<?=$here?>vehicules/deux_roues.html">deux roues</a>
						</li>
						<li>
							<a href="<?=$here?>vehicules/prestige.html">v�hicule prestige</a>
						</li>
						<li>
							<a href="<?=$here?>vehicules/vehicule_du_mois.html">v�hicule du mois</a>
						</li>
					</ul>
				</li>
				<li><a href="<?=$here?>offre/index.html#dropdown-offres" class="dropdown-trigger"  rel="nofollow">Offres</a>
					<ul id="dropdown-offres" class="dropdown-menu--header">
						<li>
							<a href="<?=$here?>offre/forfaits.html">Forfaits</a>
						</li>
						<li>
							<a href="<?=$here?>offre/service_livraison.html">Service livraison</a>
						</li>
						<li>
							<a href="<?=$here?>offre/service_voiturier.html">Service voiturier</a>
						</li>
						<li>
							<a href="<?=$here?>jeunes/index.html">Ada jeunes</a>
						</li>
						<li>
							<a href="<?=$here?>offre/trajet_unique.html">Trajet unique</a>
						</li>
						<li>
							<a href="<?=$here?>offre/malin.html">Location � l'heure</a>
						</li>
					</ul>
				</li>
				<li><a href="<?=$here?>demenagement/index.html#dropdown-demenagement" class="dropdown-trigger"  rel="nofollow">D�m�nagement</a>
					<ul id="dropdown-demenagement" class="dropdown-menu--header">
						<li>
							<a href="<?=$here?>demenagement/trouver-son-camion.html">choisir un utilitaire</a>
						</li>
						<li>
							<a href="<?=$here?>demenagement/materiel.html">pr�voir son mat�riel</a>
						</li>
						<li>
							<a href="<?=$here?>demenagement/memo.html">organiser son d�m�nagement</a>
						</li>
						<li>
							<a href="<?=$here?>demenagement/homebox.html">louer un garde meuble</a>
						</li>
					</ul>
				</li>
				<li><a href="<?=$here?>pro/index.html#dropdown-esp-pro" class="dropdown-trigger" rel="nofollow">Professionnels</a>
					<ul id="dropdown-esp-pro" class="dropdown-menu--header">
						<li>
							<a href="<?=$here?>pro/ada_ce.html">Comit� d'entreprise</a>
						</li>
						<li>
							<a href="<?=$here?>pro/index.html">Entreprise</a>
						</li>
					</ul>
				</li>
				<li class="last"><a href="<?=$here;?>faq/assistance.html">Questions</a></li>
				<li><a href="<?=Page::getURL('client/index.html', null, USE_HTTPS);?>#dropdown-esp-client" class="picto--esp-client trigger-toggle">espace client</a></li>
			</ul>
			<!-- sous-menu espace client -->
			<? if ($page != 'client/index' && $page != 'autoeurope/index' && basename($page) != 'inscription') : ?>
			<ul class="smenu--header" id="smenu--header">
				<li>
					<ul class="smenu--espace-client" style="display: none;">
						<li>
							<? $client = $this->getConnectedClient(); ?>
							<!-- si connect� -->
							<div class="type--table connected--header" id="customer_connected" <? if (!$client) echo ' style="display: none;"';?>>
								<div class="type--table-cell">
									Bienvenue <span class="prenom"><?=$client['prenom'];?></span> <span class="uppercase nom"><?=$client['nom']?></span>
								</div>
								<div class="type--table-cell">
									<a rel="noindex,nofollow" href="<?=Page::getURL('client/index.html', null, USE_HTTPS);?>" class="espace_client type--button">Mon compte</a>
									<a rel="noindex,nofollow" href="<?=Page::getURL('?logout');?>" class="logout" title="Vous êtes <?=$client['email'];?>">D�connexion</a>
								</div>
							</div>
							<? if (!$client) :?>
							<!-- si pas connect� -->
							<form action="<?=Page::getURL('client/index.html', null, USE_HTTPS);?>" method="post" id="login-form--header">
								<input type="hidden" name="id" value="0"/>
								<input type="hidden" name="key" value="<?=Page::getKey(0)?>"/>
								<div class="type--table">
									<label for="login-email" class="type--table-cell">
										<span class="intitule">E-mail</span>
										<input type="text" id="login-email" name="email" placeholder="E-mail" />
									</label>
									<label for="login-mdp" class="type--table-cell">
										<span class="intitule">Mot de passe</span>
										<input type="password" id="login-mdp" name="pwd" placeholder="Mot de passe" />
									</label>
									<div class="type--table-cell">
										<button type="submit">OK</button>
									</div>
								</div>
								<a href="<?=Page::getURL('client/motdepasse.html', null, USE_HTTPS)?>">mot de passe oubli� ?</a>
							</form>
							<? endif; ?>
						</li>
					</ul>
				</li>
			</ul>
			<? endif; ?>
		</div>
	</div>

	<? if ($page == 'reservation/index') : // overlay pour la page d'accueil sous le formulaire ?>
	<div id="overlay-home"></div>
	<? endif; ?>

	<?
		// afficher la proposition de passer sur le site mobile
		if ((SITE_MODE=='DEV' || $this->hasData('url_mobile')) && $this->getId() == 'reservation/index' || in_array(dirname($this->getId()), array('agences','client'))) :
	?>
	<div id="overlay-mobile" class="overlay-mobile">
		<ul>
			<li><a href="<?=$this->getData('url_mobile');?>"><span class="picto"><img src="<?=$baseurl?>css/img/structure-2015/picto-mobile.png" alt="" /></span><span>Acc�der au site mobile</span><img src="<?=$baseurl?>css/img/structure-2015/picto-chevron-gris.png" alt="" /></a></li>
			<li><a href="#" onclick="if (typeof($) == 'function') $('#overlay-mobile').remove(); return false;"><span class="picto"><img src="<?=$baseurl?>css/img/structure-2015/picto-desktop.png" alt="" /></span><span>Rester sur la version classique</span><img src="<?=$baseurl?>css/img/structure-2015/picto-chevron-gris.png" alt="" /></a></li>
		</ul>
	</div>
	<? endif;  ?>

	<div id="content">
		<?
			// bloc pour administrer le contenu
			$this->writeEditAdmin();
			// maintenance et t�l�-conseiller
			$this->writeInfo();
			// inclure la page courante
			include($this->getSkinnedPagePath());
		?>
	</div>

	<div class="content_bottom">
		<? if ($this->hasLinkTop() && !count($_POST) && count($_GET) < 2 && isset($_GET['page'])) : ?>
		<a href="<?=$this->getAction();?>.html#header" onclick="window.location.hash='header'; return false;" class="lien right haut">Haut de page<span class="end"></span></a>
		<? endif; ?>
	</div>

	<div class="footer">
		<div class="footer-sup">
			<div class="inner--footer-sup">
				<ul class="table">
					<li>
						<a href="http://www.groupeg7.com" target="_blank"><img src="<?=$baseurl?>css/img/structure-2014/logo-g7.png" alt="" class="left type-img-footer" /> Ada est une soci�t�<br />du Groupe G7</a>
					</li>
					<li>
						<a href="<?=$here?>societe/index.html" title="Qui sommes nous ?">Qui<br />sommes-nous ?</a>
					</li>
					<li>
						<a href="http://franchise.ada.fr/" target="_blank" title="Devenir franchis�">Devenir<br /> franchis�</a>
					</li>
					<li>
						<a href="<?=$here?>contact/index.html" title="Nous contacter">Nous<br />contacter</a>
					</li>
					<li>
						<a href="<?=$here?>conditions/location.html" title="Conditions g�n�rales">Conditions<br /> g�n�rales</a>
					</li>
					<li>
						<a href="<?=$here?>conditions/assurance_complementaire.html" title="Conditions assurance compl�mentaire">Conditions assurance<br />compl�mentaire</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="footer-sub">
			<div class="inner--footer-sub">
				<div class="table">
					<div class="col-1-5">
						<ul>
							<li><a href="<?=$here?>agences/index.html">Agences de location de voiture</a></li>
							<? if (false): ?>
							<li><a href="<?=$here?>location-voiture-promo.html">Promotions location voiture</a></li>
							<? endif; ?>
							<li><a href="<?=$here?>jeunes/index.html">Location jeune conducteur</a></li>
							<li><a href="<?=$here?>webzine/les-plus-belles-plages-de-france-mediterranee.html">Notre webzine</a></li>
							<li class="type--mode-paiement">
								Moyens de paiements accept�s<br />
								<img src="<?=$baseurl?>css/img/structure-2014/mode-paiement.png" alt="" />
							</li>
						</ul>
					</div>
					<div class="col-1-5">
						<ul>
							<li>Loisirs
								<ul>
									<li><a href="<?=$here?>vehicules/tourisme.html">Location de voiture de tourisme</a></li>
									<li><a href="<?=$here?>offre/forfaits.html">Forfaits</a></li>
									<li><a href="<?=$here?>vehicules/sans_permis.html">Location voiture sans permis</a></li>
									<li><a href="<?=$here?>vehicules/deux_roues.html">Location deux roues</a></li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="col-1-5">
						<ul>
							<li><a href="<?=$here?>demenagement/index.html">D�m�nagement</a>
								<ul>
									<li><a href="<?=$here?>demenagement/trouver-son-camion.html">Choisir son camion de d�m�nagement</a></li>
									<li><a href="<?=$here?>demenagement/materiel.html">Pr�voir son mat�riel de d�m�nagement</a></li>
									<li><a href="<?=$here?>demenagement/memo.html">Organiser son d�m�nagement</a></li>
									<li><a href="<?=$here?>demenagement/homebox.html">Louer un garde meuble</a></li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="col-1-5">
						<ul>
							<li>Professionnels
								<ul>
									<li><a href="<?=$here?>pro/ada_ce.html">Comit�s d'Entreprises</a></li>
									<li><a href="<?=$here?>pro/index.html">Entreprises</a></li>
								</ul>
							</li>
							<li>Utilitaires
								<ul>
									<li><a href="<?=$here?>vehicules/utilitaires.html" rel="noindex, nofollow">V�hicules utilitaires</a></li>
									<li><a href="<?=$here?>offre/forfaits.html">Forfaits</a></li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="col-1-5">
						<ul>
							<li>Vos services
								<ul>
									<li><a href="<?=$here?>aide/index.html">Aide en ligne</a></li>
									<li><a href="<?=Page::getURL('client/index.html', null, USE_HTTPS);?>">Mon espace client</a></li>
									<li><a href="<?=$here?>newsletter/index.html">Newsletter</a></li>
									<li><a href="<?=$here?>faq/index.html">Questions fr�quentes</a></li>
									<li><a href="<?=$here?>societe/mentions_legales.html">Mentions l�gales</a></li>
									<li><a href="<?=$here?>juridique/regles_communiques.html">Informations r�glement�es</a></li>
									<li><img src="../skin/v3/img/pave-audiotel.png" alt="" /></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="content--baseline">
		<?
			if (!$this->isNoIndex() && $this->hasData('footer')) {
				// r�cup�rer le texte de la baseline en corrigeant les liens
				if ($baseline = $this->getData('footer')) {
					$baseline = preg_replace("/href=\"(?!http)/" , 'href="'.$here, $baseline);
					echo '<ul class="baseline">'."\n";
					echo '<li>'.join("</li>\n<li>", explode("\n", $baseline))."</li>\n";
					echo '</ul>'."\n";
				}
			}
			if (!$this->isNoIndex() && $this->hasData('baseline')) {
				echo $this->getData('baseline');
			}
		?>
	</div>
	<div id="dialog-popin" title="">
		<div id="dialog-popin-msg"></div>
	</div>
<?
	// informations de mise au point
	$this->writeDebugInfo();
	$this->writeJSStats();
?>
</body>
</html>
