<?

class Dept_Collection extends Ada_Collection
{
	protected $_statut; // statut des agences
	/**
	* Cr�� une collection de d�partements en fonction d'un statut
	*
	* @param int $statut - Statut d'une agence
	* @return Dept_Collection
	*/
	public static function factory($statut = Agence::STATUT_VISIBLE, $type = null, Dept $around = null)
	{
		// on cr�e la collection qui sera retourn�e
		$x = new Dept_Collection();
		// SQL pour les applications
		$sql = "SELECT d.id, d.nom, CONCAT(d.nom, ' (',d.id,')') title, d.canon, COUNT(DISTINCT(aa.pdv)) AS agences, d.zone";
		$sql.=" FROM dept d";
		if ($around)
			$sql.=" JOIN dept_contour dc ON (dc.dept='".$around['id']."' AND dc.contour=d.id)";
		$sql.=" JOIN agence_alias aa ON (aa.zone=d.zone AND aa.dept = d.id AND (aa.statut & $statut) = $statut)";
		if ($type)
			$sql.=" JOIN agence a ON (a.id=aa.agence AND (a.type & $type) = $type)";
		$sql.=" WHERE d.zone IN ('fr','co')";
		$sql.=" GROUP BY d.id, d.nom, d.canon";
		$sql.=" ORDER BY d.nom";
		$x->_loadFromSQL($sql);
		if (!$x->isEmpty())
		{
			$x->_statut = $statut;
		}
		return $x;
	}
	protected function _getAttributes()
	{
		return array
		(
			'count' => $this->count(),
			'title' => 's�lectionnez un d�partement',
			'agence'=>'agence',
			'agences'=>'agences'
		);
	}
	public function add(Ada_Object $dept)
	{
		if ($dept['zone']=='co')
		{
			$dept['nom'] = 'Corse';
			$dept['title'] = 'Corse';
			$dept['canon'] = 'corse';
		}
		return parent::add($dept);
	}
}
?>