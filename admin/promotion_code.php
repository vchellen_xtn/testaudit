<?
	require_once("secure.php"); 
	require_once("lib.php");

	// identifiant
	$id = (int) getarg('id');
	$code = PromotionCode::factory($id, false);
	if (!$code)
		die("Aucun enregistrement correspondant.");
	else if ($_POST['annuler']=='annuler')
	{
		$code->doCancel($adminUser['login']);
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script type="text/javascript" src="../scripts/script.js"></script>
	<script type="text/javascript" src="admin.js"></script>
	<script type="text/javascript">
	// <![CDATA[
	function doCancel(btn)
	{
		return confirm("Etes vous s�r de vouloir annuler ce code ?\nCe code ne sera plus utilisable sur le site ADA.\neCette op�ration est irr�versible.");
	}
	// ]]>
	</script>
</head>
<body>
<form action="" method="post">
<table class="framed" border="0" cellspacing="0" cellpadding="4" style="float: left;">
<tr>
	<th class="bg_titre">
		<img src="img/puce.gif" align="absmiddle" hspace="1">
		Code Grillable <?if ($id) echo '#'.$id?>
	</th>
</tr>
<tr>
	<td>
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="15%">
		<col width="35%">
		<col width="15%">
		<col width="35%">
		<tr>
			<td>Code</td>
			<td><?=$code['code']?></td>
			<td colspan="2" align="right">
			<? if (!$code['paiement'] && !$code['annulation']) : ?>
			<input type="submit" name="annuler" value="annuler" onclick="return doCancel(this);"/>
			<? endif; ?>
			</td>
		</tr>
		<tr>
			<td>Campagne</td>
			<td colspan="3"><a href="promotion_campagne.php?id=<?=$code['campagne']?>"><?=$code['campagne_nom']?></a></td>
		</tr>
		<? if ($client = $code->getClient()) :?>
		<tr>
			<td>Client</td>
			<td colspan="3"><a href="client.php?id=<?=$client['id']?>">#<?=sprintf("#%ld - %s - %s %s", $client['id'], $client['email'], $client['prenom'], $client['nom'])?></a></td>
		</tr>
		<? endif; ?>
		<tr>
			<td>Cr�ation</td>
			<td><?=$code['creation']?></td>
			<td colspan="2">Cr�ation du code</td>
		</tr>
		<tr>
			<td>Distribution</td>
			<td><?=$code['distribution']?></td>
			<td colspan="2">Diffusion du code</td>
		</tr>
		<? if ($code['annulation']) : ?>
		<tr>
			<td>Annulation</td>
			<td><?=$code['annulation']?></td>
			<td>Annualtion du code</td>
		</tr>
		<? endif; ?>
		<? if ($code['validation']) :?>
		<tr>
			<td>Validation</td>
			<td><?=$code['validation']?></td>
			<td colspan="2">Derni�re association du code � une r�servation</td>
		</tr>
		<? endif; ?>
		<? if ($code['paiement']) : ?>
		<tr>
			<td>Paiement</td>
			<td><?=$code['paiement']?></td>
			<td colspan="2">Paiement de la r�servation utilisant ce code</td>
		</tr>
		<tr>
			<td>R�servation</td>
			<td><a href="reservation.php?id=<?=$code['reservation']?>"><?=$code['reservation']?></a></td>
			<td>R�duction</td>
			<td><?=money($code['reduction'])?></td>
		</tr>
		<tr>
			<td>Promotion</td>
			<td colspan="3">
				<a href="agence_promo_forfait.php?id=<?=(int)$code['promotion']?>">
				<?
				$promo = AgencePromoForfait::factory((int)$code['promotion'], true);
				if (!$promo || $promo->isEmpty())
					$promo = PromotionForfait::factory((int)$code['promotion'], true);
				echo sprintf('#%ld - %s', (int)$code['promotion'], $promo['nom']);
				if ($promo['archive'])
					echo ' (Archiv�)';
				?>
				</a>
			</td>
		</tr>
		<? endif; ?>
		<? if ($code['commentaire']) : ?>
		<tr>
			<td>Commentaire</td>
			<td colspan="3"><?=nl2br($code['commentaire'])?></td>
		</tr>
		<? endif; ?>
		</table>
	</td>
</tr>
</table>
</form>
</body>
</html>
