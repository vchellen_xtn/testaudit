<?php
class ForfaitUnivers_Collection extends Ada_Collection
{
	// tableau par cat�gorie des tarifs
	protected $_tarifs = array();
	
	/**
	* Renvoie la liste des forfaits jeunes pour un type de v�hicule
	* 
	* @param string $type
	* @return ForfaitUnivers_Collection
	*/
	static public function factory($univers, $type, $groupe = null)
	{
		// initialisation
		$type = strtolower($type);
		$x = new ForfaitUnivers_Collection();
		
		// v�rification des param�tres
		if (!Categorie::$TYPE[$type]) return $x;
		
		// r�cup�rer les forfaits JEUNES pour ce type de v�hicule
		$sql = "SELECT t.categorie, t.prix, t.prix_jour, u.*";
		$sql.=" FROM forfait2_tarif t";
		if ($groupe)
			$sql.=" JOIN zone_groupe g ON g.id=t.groupe";
		$sql.=" JOIN forfait2_univers u ON (u.univers='$univers' AND u.forfait=t.forfait)";
		$sql.=" JOIN categorie c ON c.id=t.categorie";
		$sql.=" WHERE t.zone='fr' AND c.type='".$type."'";
		if ($groupe)
			$sql.=" AND g.nom='$groupe'";
		else
			$sql.=" AND t.groupe IS NULL";
		$sql.=" ORDER BY u.jours_min, ROUND(u.depart_debut)";
		
		// charger les donn�es
		$x->_loadFromSQL($sql);
		return $x;
	}
	
	// renseigner les "tarifs" au fur et � mesure
	public function add(Ada_Object $item)
	{
		// on stocke les informations par cat�gorie
		$this->_tarifs[$item['categorie']][$item['id']] = array('prix' => $item['prix'], 'prix_jour' => $item['prix_jour']);
		// on ajoute les 
		return parent::add($item);
	}
	
	/**
	* Des tarifs existent-ils pour cette cat�gorie
	* 
	* @param int $categorie
	*/
	public function hasTarifs(Categorie $categorie)
	{
		return isset($this->_tarifs[$categorie['id']]);
	}
	
	public function getTarif(Categorie $categorie, ForfaitUnivers $forfait)
	{
		if ($tarif = $this->_tarifs[$categorie['id']][$forfait['id']])
		{
			return $tarif['prix'];
		}
		return;
	}
}
?>
