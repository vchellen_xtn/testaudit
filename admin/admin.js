// mettre � jour un select
function updateSelect(ctrl, def_val)
{
	var url = location.href.replace(/\/[a-z\-\_\d]+\.php(\?.*)?$/,'') + "/xml"+ ctrl.name + ".php";
	$(ctrl).attr("disabled", "disabled");
	$.get(url, $(ctrl.form).serialize(), function(xml)
	{
		populateMenu(xml, ctrl, def_val);
		$(ctrl).attr("disabled", false);
	});
}
// s�lectionner / d�selectionner une s�rie de checkbox
function doCheckbox(src, flag)
{
	$(':checkbox', src.parentNode).each(function(){
		this.checked = flag;
	});
	return false;
}
// countCheckbox
function countCheckboxes(name, frm)
{
	return $(":checkbox[name='"+ name + "']:checked", frm).length;
}

// AnyTime : initialisation
// http://www.ama3.com/anytime/#AnyTime.picker.ajaxOptions
var oneDay = 24*60*60*1000;
var today = new Date(); today.setHours(0,0,0,0);
function getOptionsAnyTime(dateFormat, lblTitle)
{
	options =
	{
		"format": dateFormat,
		"firstDOW": 1,
		"dayAbbreviations": ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'],
		"dayNames": ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
		"monthAbbreviations": ['Jan','F�v','Mar','Avr','Mai','Juin','Juil','Ao�t','Sept','Oct','Nov','D�c'],
		"labelTitle": lblTitle,
		"labelHour": "Heure",
		"labelYear": "Ann�e",
		"labelMonth": "Mois",
		"labelDayOfMonth": "Jour"
	};
	return options;
};

// makeDate
function makeDate(date_val)
{
	if (!date_val.length)
		return null;
	var d = date_val.toString().split(/[\/\-]/);
	obj = new Date(d[2], d[1] - 1, d[0]);
	if (d[1] != obj.getMonth() + 1 || d[0] != obj.getDate() || d[2] != obj.getFullYear()) 
		return null;
	obj.setHours(0,0,0,0);
	return obj;
}

// afficher dans une frma si elle existee
function go_frame(frame, url)
{
	if (!frame) frame = window;
	frame.location.href = url;
}