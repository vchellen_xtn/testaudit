<?
	require_once("secure.php"); 
	require_once("lib.php");

	// v�rification des droits
	if (!$adminUser->hasRights(Acces::SB_ADMIN)) 
		die("Vous n'avez pas le droit d'acc�der � cette fonctionnalit�!");

	// identifiant
	$login = getarg('login');
	if ($login && !preg_match('/^[a-z0-9\.\-\_]+$/i', $login))
		die("Le login n'est pas correct !");
	// suppression
	if ($login && getarg("delete")) 
	{
		$sql = "delete from api_access where login='".$login."'";
		getsql($sql);
		unset($login);
	}
	
	// modification
	if (getarg('update') || getarg('create')) 
	{
		save_record('api_access', $_POST);
		// enregistrer le mot de passe si n�cessaire
		if( getarg('pwd') )
		{
			$sql = "update api_access set pwd =password('".addslashes($_POST['pwd'])."') where login='".$login."'";
			getsql($sql);
		}
	}
	if ($login)
	{
		$rs = sqlexec("select * from api_access where login = '".$login."'");
		if (!$rs || !($row = mysql_fetch_assoc($rs)))
			die("Aucun enregistrement correspondant.");
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script>
	<!--
	// doDelete
	function doDelete(frm)
	{
		return (confirm("Voulez-vous r�ellement supprimer cet �l�ment ?\nCette op�ration est irr�versible."));
	}
	// enablePassword
	function enablePassword()
	{
		var frm = document.forms[0];
		frm.pwd.disabled = !frm.pwd.disabled;
		frm.rpwd.disabled = !frm.rpwd.disabled;
	}
	
	// doSubmit
	function doSubmit(frm)
	{
		var err = "";
		if(!frm.login.value.match(/^[a-z0-9\.\-\_]+$/))
			err += "Veuillez indiquer le login\n";
		if(frm.pwd && !frm.pwd.disabled)
		{
			if (!frm.pwd.value.length)
				err += "Veuillez indiquer le mot de passe\n";
			if (frm.pwd.value != frm.rpwd.value)
				err += "Les 2 mots de passe doivent �tre identiques\n";
		}
		// matche une adresse Ip : 192.88.99.0 ou une liste d'IP : 192.88.99.0,192.88.99.0
		if(frm.authorized_ips.value.length && !frm.authorized_ips.value.match(/^\d{1,3}(\.\d{1,3}){3}(\,\d{1,3}(\.\d{1,3}){3})*$/))
			err += "Veuillez indiquer des adresse IP correctes\n";
		// s'il y a une erreur
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		return true;
	}
	//-->
	</script>
</head>

<body>

<form action="" method="post" onsubmit="return doSubmit(this)">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre">
		<img src="img/puce.gif" align="absmiddle" hspace="1">
		<? echo "API acc�s"; ?>
	</th>
</tr>
<tr>
	<td>
	<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="15%">
		<col width="32%">
		<col width="15%">
		<col width="33%">
		<tr>
			<td><label for="login">Login</label></td>
			<td>
				<? echo $login.'<input type="'.($login ? "hidden" : "text").'" name="login" id="login" value="'.$login.'" maxlength="32"/>'; ?>
			</td>
			<td><label for="origine">Origine</label></td>
			<td>
				<?
				$sql = 'select id, id from origine order by id';
				selectsql('origine', $sql, $row['origine'], '--');
				?>
			</td>
		</tr>
		<? if ($login) $disabled = ' disabled="disabled"'; ?>
		<tr>
			<td><label for="pwd">Mot de passe</label></td>
			<td><input type="password" id="pwd" name="pwd" value="" <?=$disabled?> maxlength="16"></td>
			<td><label for="rpwd">Confirmation</label></td>
			<td><input type="password" id="rpwd" name="rpwd" value="" <?=$disabled?> maxlength="16"></td>
		</tr>
		<? if ($disabled) : ?>
		<tr><td></td><td colspan="3"><a href="#" onclick="enablePassword();">modifier le mot de passe</a></td></tr>
		<? endif; ?>
		<tr>
			<td valign="top"><label for="authorized_ips">Adresses Ip</label></td>
			<td colspan="3">
				<input type="text" id="authorized_ips" name="authorized_ips" value="<?=$row['authorized_ips']?>" size="128" maxlength="512" />
				<br/><span style="font-size:8px;font-style:italic;">Ex: <?=IP_ADA?> ou <?=IP_ADA?>,<?=IP_RND?></span>
			</td>
		</tr>
		<tr>
			<td valign="top"><label for="commentaires">Commentaires</label></td>
			<td colspan="3">
				<textarea id="commentaires" name="commentaires" cols="30" rows="2"><?=htmlspecialchars($row['commentaires'])?></textarea>
			</td>
		</tr>
	</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%">
		<tr>
			<td align="right">
				<input type="submit" name="<?=$login ? "update" : "create"?>" value="enregistrer">
				<? if ($login) : ?>
				&nbsp;
				<input type="submit" onclick="return doDelete(this.form);" name="delete" value="supprimer">
				<? endif; ?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
</form>

</body>
</html>
