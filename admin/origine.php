<?
	require_once('secure.php'); 
	require_once('lib.php');

	// identifiant
	$id = preg_replace('/[^a-z0-9\-\.]/i', '', getarg('id'));
	
	// suppression
	if ($id && getarg('delete')) 
	{
		$sql = "delete from origine where id = '".$id."'";
		getsql($sql);
		unset($id);
	}
	
	// modification, cr�ation
	if (getarg("update") || getarg("create"))
	{
		save_record('origine', $_POST);
	}
	if ($id)
	{
		$rs = sqlexec("select * from origine where id = '".$id."' ");
		if (!$rs || !($row = mysql_fetch_assoc($rs)))
			die("Aucun enregistrement correspondant.");
	}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script language="javascript" src="../scripts/check.js"></script>
	<script>
	<!-- //

	// doDelete
	function doDelete(frm)
	{
		return (confirm("Voulez-vous r�ellement supprimer cet �l�ment ?\nCette op�ration est irr�versible."));
	}
	
	// doSubmit
	function doSubmit(frm)
	{
		var err = "";
		if(!frm.id.value.match(/^[a-z0-9\-\.]+/))
			err+= "Veuillez indiquer un ID valide\n";
		if(!frm.groupe.value.length)
			err+= "Veuillez indiquer le groupe\n";
		if(!frm.url.value.length)
			err+= "Veuillez indiquer une URL\n";
		if (!frm.event.value.match(/^(\d+\.\d+)?$/))
			err+="Veuillez indiquer un �v�nement valide"
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		return true;
	}
	// -->
	</script>
</head>
<body>

<form action="" method="post" onsubmit="return doSubmit(this);">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre">
	<img src="img/puce.gif" align="absmiddle" hspace="1">
<? 
	echo "Code origine";
?>
	</th>
</tr>
<tr>
	<td>
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="15%">
		<col width="15%">
		<col width="15%">
		<col width="55%">
		<tr>
			<td><label for="id">ID</label></td>
			<td>
				<?=$id?>
				<input type="<?=($id ? 'hidden' : 'text')?>" maxlength="8" name="id" value="<?=$id?>"></td>
			<td><label for="groupe">Groupe</label></td>
			<td><input type="text" style="width:100%" name="groupe" id="groupe" value="<?=$row["groupe"]?>" maxlength="8"></td>
		</tr>
		<? if ($id) : ?>
		<tr>
			<td>Exemple:</td>
			<td colspan="3">
				http://www.ada.fr/?co=<?=$id?>
				<br/>
				http://www.ada.fr/jeunes/offre.html?co=<?=$id?>
			</td>
		</tr>
		<? endif; ?>
		<tr>
			<td><label for="url">URL</label></td>
			<td colspan="3">
				<input type="text" style="width:100%" name="url" id="url" value="<?=$row["url"]?>" maxlength="128">
			</td>
		</tr>
		<tr>
			<td><label for="event">Ev�nement</label></td>
			<td><input type="text" id="event" name="event" maxlength="6" value="<?=$row['event']?>"></td>
			<td colspan="2">pour les statistiques dans NSP</td>
		</tr>
		<tr>
			<td><label for="coupon">Code r�duction</label></td>
			<td><input type="text" id="coupon" name="coupon" size="32" maxlength="32" value="<?=htmlspecialchars($row['coupon'])?>"/></td>
			<td colspan="2">
			<? if ($row['coupon']) : ?>
				<? if ($coupon = PartenaireCoupon::factory($row['coupon'], true)) : ?>
				<a href="partenaire_coupon.php?id=<?=$coupon['id']?>">voir le code de r�duction</a>
				<? else : ?>
				Aucun code de r�duction ne correspond !
				<? endif; ?>
			<? endif; ?>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td align="right">
		<input type="submit" name="<?=$id ? "update" : "create"?>" value="enregistrer">
<?
	if ($id) 
	{
?>
		&nbsp;
		<input type="submit" onclick="return doDelete(this.form);" name="delete" value="supprimer">
<?
	}
?>
	</td>
</tr>
</table>
</form>

</body>
</html>
