<?php
class Transaction_Paypal extends Transaction
{
	protected function _doPayment(Reservation $reservation, $args)
	{
		if (!$args['token'] || !$args['payer'])
		{
			$this->_addMessage("Les informations pour le paiement avec Paypal ne sont pas fournies.");
			return false;
		}
		$a = Paypal::DoExpressCheckoutPayment($args['token'], $args['payer'], $reservation['total'], $reservation->toString());
		if ($a)
		{
			$reservation->setData('numerotrans', $a['TRANSACTIONID']) // identifiant de la transaction
						->setData('numappel', $args['payer']) 		// identifiant du payeur
						->setData('paiement_frais', $a['FEEAMT'])	// frais Paypal
						->save('numerotrans,numappel,paiement_frais');
			if ($a['AMT'] != $reservation['total'] || $a['PAYMENTSTATUS'] != 'Completed')
			{
				$this->_addMessage("Une erreur s'est produite lors du paiement Paypal.<br/>");
				$reservation->setData('paiement_log', 'AMT: '.$a['AMT'].', PAYMENTSTATUS:'.$a['PAYMENTSTATUS'].', PENDINGREASON: '.$a['PENDINGREASON']);
				$reservation->save('paiement_log');
				return false;
			}
			return true;
		}
		$this->_addMessage("Le paiement avec Paypal a �chou�.");
		return false;
	}
	protected function _doRefund(Reservation $reservation, $montant)
	{
		$a = Paypal::RefundTransaction($reservation['numerotrans'], $montant, $explications);
		if (empty($a))
		{
			$this->_addMessage("Le remboursement avec Paypal a �chou�");
			return false;
		}
		return true;
	}
	protected function _getModalitesRemboursementTotal()
	{
		return "Votre compte PayPal sera re-cr�dit� du montant de votre r�servation.";
	}
	protected function _getModalitesRemboursementPartiel($rembourse)
	{
		return "Votre compte PayPal va donc �tre re-cr�dit� d'un montant de ".$rembourse."� TTC.";
	}
}
?>
