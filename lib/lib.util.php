<?
// $Id: lib.util.php,v 1.65 2013/02/15 10:45:05 gdubourguet Exp $
	require_once dirname(__file__)."/lib.sql.php";

	/* Cherche un fichier impl�mentant une classe
	*	LIB_PATH	lib
	*	BP			dans constant.php define('BP', dirname(__file__)); // BASE PATH
	*/
	function rnd_autoload($class)
	{
		if (strpos($class, '/')!==false) return;
		$classFile = '/app/'.str_replace('_', '/', $class).'.php';
		if (is_file(BP.$classFile))
			include(BP.$classFile);
		else if (is_file(LIB_PATH.$classFile))
			include(LIB_PATH.$classFile);
	}
	spl_autoload_register('rnd_autoload');

	// get_file
	function get_file($fname, $a=null)
	{
		$txt = implode("", file($fname));
		if (!$a)
			return $txt;

		foreach ($a as $key => $val)
		{
			$pat[] = "/".preg_quote($key, "/")."/";
			$rep[] = $val;
		}
		return preg_replace($pat, $rep, $txt);
	}

	// cnum
	function cnum($s)
	{
		return preg_replace('/[^\d\+\-\.]/', '', str_replace(',', '.', $s));
	}

	// selectsql
	function selectsql ($name, $sql, $ref, $first=null, $more=null)
	{
		return selectrecordset($name, sqlexec($sql), $ref, $first, $more);
	}

	// selectrecordset
	function selectrecordset ($name, $rs, $ref, $first=null, $more=null)
	{
		global $sql_fetch_row;
		echo '<select '.$more.' name="'.$name.'" id="'.$name.'">'."\n";
		if ($first)
		{
			if (!is_array($first))
				$first = array('', $first);
			echo '<option value="'.$first[0].'">'.$first[1]."</option>\n";
		}
		while ($rs && $row = $sql_fetch_row($rs))
		{
			if (!$first && !strlen($ref)) $ref = $row[0];
			echo '<option value="'.$row[0].'"';
			if (strlen($ref) && strtoupper($row[0]) == strtoupper($ref)) echo (' selected="selected"');
			echo '>'.$row[count($row)>1?1:0]."</option>\n";
		}
		echo '</select>'."\n";
		return $ref;
	}

	// selectarray
	function selectarray ($name, $a, $ref, $first=null, $more=null)
	{
		echo '<select '.$more.' name="'.$name.'" id="'.$name.'">'."\n";
		if ($first)
		{
			if (!is_array($first))
				$first = array('', $first);
			echo '<option value="'.$first[0].'">'.$first[1]."</option>\n";
		}
		foreach ($a as $key => $val)
		{
			if (!$first && !strlen($ref)) $ref = $key;
			echo '<option value="'.$key.'"';
			if (strlen($ref) && strtoupper($key) == strtoupper($ref)) echo (' selected="selected"');
			echo '>'.$val."</option>\n";
		}
		echo '</select>'."\n";
		return $ref;
	}

	// get_path
	function get_path($use_https=false, $rel_path=null)
	{
		$url = dirname($_SERVER["SCRIPT_NAME"]);
		if ($rel_path)
			$url.= '/'.$rel_path;
		if ($url == "\\") // patch pour PHP:IIS
			$url = "/";
		else if ($url != "/")
			$url.= "/";
		return ($use_https ? "https" : "http")."://".$_SERVER["SERVER_NAME"].$url;
	}

	// send_mail
	function send_mail ($txt, $to, $subject, $from, $from_addr, $cc=null, $bcc=null, $rel_path=null, $utf8 = false)
	{
		$srvr = get_path(false, $rel_path);
		$mime_type = (strpos($txt,'<html') === false) ? "text/plain" : "text/html";

		$txt = preg_replace(
			array("/(src|href|background)=\"(?!(http|mailto))/", "/url\(/"),
			array("$1=\"".$srvr, "url(".$srvr),
			$txt
			);

		if ( strpos($_SERVER["SERVER_SOFTWARE"], "IIS") ) {
			$from_hdr = $from_addr;
		} else if ( $utf8 ) {
			$from_hdr = '=?utf8?b?'.base64_encode($from).'?='.' <'.$from_addr.'>';
		} else {
			$from_hdr = filter_chars($from).' <'.$from_addr.'>';
		}
		$headers = "MIME-Version: 1.0\n"
			 ."Content-type: ".$mime_type."; Content-type: text/plain; charset: ".( $utf8 ? 'UTF-8' : 'iso-8859-1' )."\n"
			 ."Content-Transfer-Encoding: 8bit\n"
			 ."From: ".$from_hdr."\n"
			 ."Reply-To: ".$from_addr."\n"
			 ."Return-Path: ".$from_addr."\n"
		     ."X-Mailer: PHP/".phpversion()."\n";
		if ($cc)
			$headers .= "Cc: ".$cc."\n";
		if ($bcc === null && defined("BCC_EMAILS_TO"))
			$bcc = BCC_EMAILS_TO;
		if ($bcc)
			$headers .= "Bcc: ".$bcc."\n";

		$subject = ( $utf8 ? '=?utf8?b?'.base64_encode($subject).'?=' : filter_chars($subject) );
		return mail($to, $subject, $txt, $headers."\n", "-f".$from_addr);
	}
	function send_mail_utf8($txt, $to, $subject, $from, $from_addr, $cc=null, $bcc=null, $rel_path=null) {
		return send_mail($txt, $to, $subject, $from, $from_addr, $cc, $bcc, $rel_path, true);
	}

	// filter_chars
	function filter_chars($str)
	{
		return strtr($str,
			"\xe1\xc1\xe0\xc0\xe2\xc2\xe4\xc4\xe3\xc3\xe5\xc5".
			"\xaa\xe7\xc7\xe9\xc9\xe8\xc8\xea\xca\xeb\xcb\xed".
			"\xcd\xec\xcc\xee\xce\xef\xcf\xf1\xd1\xf3\xd3\xf2".
			"\xd2\xf4\xd4\xf6\xd6\xf5\xd5\x8\xd8\xba\xf0\xfa\xda".
			"\xf9\xd9\xfb\xdb\xfc\xdc\xfd\xdd\xff\xe6\xc6\xdf\xf8",
			"aAaAaAaAaAaAacCeEeEeEeEiIiIiIiInNoOoOoOoOoOoOoouUuUuUuUyYyaAso"
		);
}

	// debug
	function debug($content)
	{
		$fname = function_exists("tempnam") ? tempnam("/tmp", "debug") : "debug.txt";
		$h = fopen($fname, "w");
		fwrite($h, $content);
		fclose($h);
	}

	// gestion d'erreur
	function err($msg)
	{
		$url = preg_replace("/([?&])msg=[^\&]+/", "$1", $_SERVER["HTTP_REFERER"]);
		$url.= (strpos($url, "?") ? "&" : "?") ."msg=".rawurlencode($msg);
		die(header("location: ".$url));
	}

	// requ�te
	function req($index, $def=null)
	{
		return ($_REQUEST[$index] ? $_REQUEST[$index] : $def);
	}

	// copy_form
	function copy_form($params)
	{
		foreach($params as $key => $val)
		{
			if (!preg_match("/(^|_)[xy]/", $key))
			{
				if (is_array($val))
				{
					foreach($val as $subval)
						echo '<input type="hidden" name="'.$key.'[]" value="'.htmlentities($subval).'"/>'."\n";
				}
				else
				{
					echo '<input type="hidden" name="'.$key.'" value="'.htmlentities($val).'"/>'."\n";
				}
			}
		}
	}

	// euro_iso
	function euro_iso($date)
	{
		return preg_replace("/^(\d+)[\/\-\.](\d+)[\/\-\.](\d+)/", "\\3-\\2-\\1", $date);
	}

	// date_iso
	function date_iso($date)
	{
		return preg_replace("/^(\d{1,2})[\/\-\.](\d{1,2})[\/\-\.](\d{2,4})/", "\\3-\\2-\\1", $date);
	}

	// save_files
	//
	// enregistre les fichiers dans le cas d'un upload dans un formulaire.
	// � ex�cuter avant save_record parce qu'il peut modifier des �l�ments du POST
	// le 2nd argument est un tableau qui contient les r�pertoires dans lesquels les fichiers doivent �tre enregistr�s
	//
	// par exemple :
	//
	// <form method="post" action="foo.php" enctype="multipart/form-data">
	// 	<�nput type="file" name="monfichier">
	// </form>

	// foo.php :
	//
	// save_files($_POST, array("monfichier" => "../files/foo/bar"));
	// save_record("matable", $_POST);
	//
	function save_files(&$args, $dirs)
	{
		foreach ($_FILES as $key => $f)
		{
			if (!$f['name'] || !$f['size'])
				continue;

			if (!preg_match("/(pdf|gif|jpeg|jpg|png|txt|doc|docx|zip|flv|xls|swf)$/i", $f['name']))
				die ("Type de fichier non support� : ".$f['name']);

			$args[$key] = '';

			$dir = is_array($dirs) ? $dirs[$key] : $dirs;
			if (substr($dir, 0, -1) != '/')
				$dir.= '/';

			$base = strtolower(basename($f['name']));
			if (preg_match ("/^(.*)(\.[^\.]+)$/", $base, $m))
				list(, $base, $ext) = $m;

			for($i=0, $fname=$base.$ext; is_file($dir.$fname); $fname=$base.'.'.(++$i).$ext);
			if (move_uploaded_file($f['tmp_name'], $dir.$fname))
				$args[$key] = $dir.$fname;
		}
	}

	// save_record
	// allow_update = false si on veut "forcer" un insert
	// (pour �viter qu'un nouvel enregistrement �crase un ancien avec la m�me cl� primaire)
	function save_record($tbl, &$args, $allow_update=true, &$error=null)
	{
		global $cols_cache, $sql_insert_id;
		require_once(dirname(__file__).'/lib.sql.php');
		$magic_quotes = get_magic_quotes_gpc();

		if (!$cols_cache[$tbl])
			$cols_cache[$tbl] = get_columns_from_schema($tbl);

		$pri = $vals = array();
		foreach ($cols_cache[$tbl] as $row)
		{
			$fld = $row['Field']; // du 03/12/08 au 15/04/09, strtolower($row['Field'])
			$v = $args[$fld];
			if ($magic_quotes)
				$v = is_array($v) ? array_map("stripslashes", $v) : stripslashes($v);

			if (strtolower($row['Extra']) == 'auto_increment')
				$auto = $fld;
			if ($fld == "modification" || $fld == "creation")
			{
				$v = ($v) ? "'".escape_string($v)."'" : "now()";
				if ($fld == "creation")
					$v = "if(unix_timestamp(ifnull(creation, 0))=0, ".$v.", creation)";
				$vals[$fld] = $v;
			}
			else if (isset($args[$fld]))
			{
				if ($v === '')
					$v = 'null';
				else if (is_array($v))
					$v = preg_match("/^set\(/i", $row["Type"]) ? "'".escape_string(implode(',', $v))."'" : array_sum($v);
				else if (preg_match("/^date/i", $row['Type']))
					$v = "'".escape_string(date_iso($v))."'";
				else if (preg_match("/^(double|float|numeric|decimal|int|smallint|tinyint|money|smallmoney)/i", $row['Type']))
					$v = cnum($v);
				else
					$v = "'".escape_string($v)."'";

				if ($row["Key"] == "PRI")
					$pri[$fld] = $v;
				$vals[$fld] = $v;
			}
			else if ($row['Key'] == 'PRI' && $row['Extra'] != 'auto_increment')
			{
				die("Erreur fatale : une cl� primaire n'est que partiellement renseign�e [$tbl.$fld]");
			}
		}

		$tbl = quotename($tbl);

		if (!count($pri)) 
			$pri = $vals;
		array_walk($pri, 'reduce');
		$cond = implode(' and ', $pri);
		$exists = getsql("select * from $tbl where $cond");

		if ($exists && count($pri))
		{
			if (!$allow_update)
				die("Un enregistrement existe d�j� pour cette valeur de la cl� primaire : ".implode(',', $pri));
				
			array_walk($vals, 'reduce');
			$sql = "update $tbl set ".implode(",", $vals)." where $cond";
			if (DB_ENGINE == "mysql") $sql.= " limit 1";
			$insert = false;
		}
		else
		{
			$sql = "insert into $tbl (".implode(",", array_map("quotename", array_keys($vals))).") values (".implode(",", array_values($vals)).")";
			$insert = true;
		}

		sqlexec($sql);
		if (mysql_errno())
			$error = "[".mysql_errno()."] ".mysql_error();
		if ($insert && $auto)
			$args[$auto] = $sql_insert_id();
		return $insert;
	}

	// reduce
	function reduce(&$val, $key)
	{
		$val = quotename($key)." = ".$val;
	}

	// redir
	function redir($page)
	{
		ob_end_clean();
		die ('<html><head><script language="javascript">window.location.href = "index.php?page='.$page.'"</script></head></html>');
	}

	// rinclude
	function rinclude($inc)
	{
		// recherche le fichier constant
		for(; ($path=realpath(dirname($inc))) != $prev && !is_file($inc); $inc="../".$inc, $prev=$path);
		if (!is_file($inc))
			die("Impossible de trouver le fichier [".$inc."]");
		require($inc);
	}

	// load_and_parse
	function load_and_parse($file, $row)
	{
		if (!is_file($file))
			return null;
		$html = implode("", file($file));
		foreach ($row as $key => $val)
		{
			$from[] = "/#".$key."#/";
			$to[] = $val;
		}
		return preg_replace ($from, $to, $html);
	}

	// getarg
	function getarg($index, $mask=null, $a=null)
	{
		if (!$a)
			$a = $_REQUEST;
		$v = trim($a[$index]);
		switch ($mask)
		{
			case null:
				return strip_tags($v);
			case '':
			case 'string':
			case '%s':
				$mask = '.+';
				break;
			case 'int':
			case '%d':
			case '%ld':
				$mask = '\-*\d+';
				break;
			case 'float':
			case '%f':
				$mask = '\d+(\.\d+)*';
				break;
			case 'date':
				$mask = '(\d{1,2}[-/\.]){2}\d{2-4}';
				break;
		}
		if (!preg_match("/^$mask$/", (string)$v))
		{
			debug(date("Y-m-d H:i")."\t".$v);
			die("[$index] n'est pas du type attendu");
		}
		return strip_tags($v);
	}
?>
