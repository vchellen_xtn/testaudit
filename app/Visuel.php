<?php
class Visuel extends Ada_Object
{
	public function showVisuel($sp_from = 'fr_reservation_index')
	{
		$use_https = (Page::getProtocol()=='https');
		$href = $this->_data['href'];
		$sp_target = Tracking_SmartProfile::toTarget($href);
		$is_http = (substr($href, 0, 4) == 'http');
		if ($href && !$is_http)
			$href = Page::getURL(ltrim($href,'/'), SERVER_RESA);
		if($this->_data['type'] == 'swf') :
		?>
			<script language="javascript" type="text/javascript">
				AC_FL_RunContent(
					"src", "<?=Page::getURL(substr($this->_data['src'],0, -4), SERVER_RESA, $use_https);?>",
					"width", "<?=$this->_data['largeur']?>",
					"height", "<?=$this->_data['hauteur']?>",
					"align", "middle",
					"bgcolor", "#ffffff",
					"flashvars", "Clicktag=<?=$href?>",
					"quality", "high",
					"wmode", "transparent",
					"allowScriptAccess","sameDomain",
					"type", "application/x-shockwave-flash",
					"codebase", document.location.protocol+"//fpdownload.macromedia.com/get/flashplayer/current/swflash.cab",
					"pluginspage", document.location.protocol+"//www.adobe.com/go/getflashplayer"
				);
			</script>
		<? else : ?>
			<? if ($href) : ?>
				<a href="<?=$href;?>" <? if ($is_http ) echo ' target="_blank"'; ?> onclick="sp_clic('N', 'bannieres', '<?=Tracking_SmartProfile::toLabel($this->_data['libelle'])?>', '<?=$sp_target?>'); return true;">
			<? endif; ?>
			<img src="<?=Page::getURL($this->_data['src'], SERVER_RESA, $use_https)?>" alt="<?=$this->_data['libelle']?>"/>
			<? if($href) : ?>
				</a>
			<? endif;
		endif;
	}
}
?>
