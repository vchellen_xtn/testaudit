<?
/*
Gestion sp�cifrique de l'offre pour l'�volution de 03/2011
- � gauche on affiche :
	- un formulaire pour modifier des donn�es de la r�servation et le kilom�trage
	- pour les VP : la liste des cat�gories
	- pour les VU : un calendrier de propositions alternatives
- � droite : le fichier .htm prend la mai net affiche :
	- la cat�gorie s�lectionn�e par d�faut
	- les options
	- le lien l'inscription de la r�servation et l'identification
*/
	/** @var Reservation */
	$reservation = $this->getData('reservation');
	if (!$reservation)
	{
		if ($this->hasMessages())
			$msg = $this->getMessages();
		else
			$msg = "Une erreur s'est produite durant la recherche de votre r�servation.";
		echo $this->showError($msg);
		return;
	}
	$type = $reservation['type'];

?>
<div class="bg">
	<div class="header">
		<div class="univers_titre"><?=$this->getUniversTitre();?></div>
		<? if ($navResa = $this->getNavResa()) : ?>
		<? $i = 1; ?>
		<a href="../" class="new-search rouge">&gt; Nouvelle recherche</a>
		<ol class="nav-resa nav-resa<?=count($navResa)?>">
			<? foreach($navResa as $k => $title) : ?>
			<li>
				<span class="<? if ($k == $this->getAction()) echo 'current ';?>item-chemin-resa"><span class="number"><?=$i++?></span> <?=$title?></span>
			</li>
			<? endforeach; ?>
		</ol>
		<? endif; ?>
	</div>
	<div class="offre_bg">
		<div id="col">
			<form action="" method="post" name="form_modif_reservation" id="form_modif_reservation">
				<?=hn(1, "Votre demande de r�servation", "7f7f7f");?>
				<?
					foreach (explode(',','type,agence,ville,categorie,categorie_choisie,age,permis,codepromo,comptepro') as $k)
					{
						if (!$_POST[$k] && $k!='categorie_choisie') continue;
						echo '<input type="hidden" id="'.$k.'" name="'.$k.'" value="'.htmlspecialchars(strip_tags($_POST[$k])).'" />';
					}
				?>
				<div id="calendar">
					<div class="sel">
						<label for="depart">D�part :</label>
						<input type="text" name="depart" id="depart" value="<?=$reservation->getDebut('d-m-Y');?>" />
						<img class="btn_cal" src="../img/calendar2.gif" alt="date de d�part" id="btn_depart" />
						<span id="span_h_depart" <?=($type=='sp' ? 'style="display:none;"' : '')?>>
							<span class="duree_a">�</span>
							<select name="h_depart" id="h_depart"><option value="">--</option></select>
						</span>
					</div>
					<div class="sel last">
						<label for="retour">Retour :</label>
						<span id="span_ml_retour"<? if ($type!='ml') echo ' style="display: none; "'?>>le m�me jour</span>
						<input type="text" name="retour" id="retour" value="<?=$reservation->getFin('d-m-Y')?>" />
						<img class="btn_cal" src="../img/calendar2.gif" alt="date du retour" id="btn_retour" />
						<span id="span_h_retour" <?=($type=='sp' ? 'style="display:none;"' : '')?>>
							<span class="duree_a">�</span>
							<select name="h_retour" id="h_retour"><option value="">--</option></select>
						</span>
					</div>
					<!--<p id="div_sp_info"<?//($type!='sp' ? ' style="display:none;"' : '')?>>
						Vous pouvez r�server un v�hicule sans permis pour une p�riode de 7 jours � un mois. Au del� d'un mois, le renouvellement de la location se fait directement en agence.
					</p>-->
				</div>
				<input id="illimite" name="illimite" type="hidden" value="<?=$reservation['illimite']?>"/>
				<input id="distance" name="distance" type="hidden" value="<?=$reservation['distance'];?>"/>
				<?
				if ($reservation->getOffres())
					$kilometrages = $reservation->getOffres()->getKilometrages();
				$reseau = $reservation->getAgence()->getData('reseau');
				$cnt = count($kilometrages);
				if (is_array($kilometrages) && !$reservation->isLocapass() && ($cnt > 1 || ($cnt==1 && !$kilometrages['illimit�'] && $reseau=='ADA')))
				{
					// pas plus de 4 possibilit�s
					if ($cnt > 4)
						$kilometrages = array_slice($kilometrages, 0, 4, true);
					if ($reservation['illimite'])
						$distance = 'illimit�';
					else if (!($distance = $reservation['distance']) || ($distance < key($kilometrages)))
					{
						// choisir le kilom�trage correspondant au forfait choisi
						if ($offres = $reservation->getOffres())
						{
							if ($forfait = $offres->getForfait($reservation['categorie']))
								$distance = $forfait['km'];
						}
						if (!$distance)
							$distance = key($kilometrages);
					}
					// pour ADA on peut changer les kilom�trages
					if ($reseau == 'ADA')
						$kilometrages['+'] = '+ de kilom�tres ?';
					$cnt = count($kilometrages);
					echo '<label class="rouge" for="l_distance">Modifiez vos kilom�tres :</label>'."\n";
					echo '<div class="clear"></div>'."\n";
					// la liste des kilom�tres
					selectarray('l_distance', $kilometrages, $kilometrages[$distance] ? $distance : '+', null);
					// le champ texte pour la saisie de l'utilisateur
					echo '<input type="text" id="t_distance" name="t_distance" style="';
					if ($kilometrages[$distance])
						echo 'display:none;" value="Saisissez vos km" maxlength="16"';
					else
						echo '" value="'.$distance.'" maxlength="4"';
					echo '/>';
				}
				?>
				<p class="valid" style="display:none;">
					<input type="submit" value="modifier"/>
				</p>
			</form>
			<?
			if ($reservation['type']=='vp')
			{
				/** @var Offre_Collection */
				$offres = $reservation->getOffres();
				$categorie = $reservation->getCategorie();
				$jsForfaits = array();
				$display = 0;
				// afficher la liste des cat�gories avec les diponiblit�s
				echo '<div id="offre_categories">'."\n";
				echo '<div class="votre_voiture">'.hn(2, "Votre voiture", "7f7f7f").'</div>';
				$categories = Categorie_Collection::factory('fr','vp');
				// s�lectionner la cat�gorie la moins ch�re dans la famille

				foreach($categories as $x)
				{
					if ($x['famille'] != $categorie['famille'])
					{
						if ($catID) // si on est d�j� pass� dans une famille
							break;
						continue;
					}
					if ($offres)
						$o = $offres->getOffreByCategory($x['id']);
					if (!$o) continue;
					if (!$catID) // la premi�re foix on fixe la cat�gorie, si aucune n'est disponible
						$reservation['categorie'] = $x['id'];
					$catID = $x['id'];
					// sinon on s'arr�te � la premi�re cat�gorie disponible dans la famille
					if ((!$_POST['categorie_choisie'] && $o['nb'] > 0) || ($_POST['categorie_choisie']==$x['id']))
					{
						$reservation['categorie'] = $catID;
						break;
					}
				}
				// afficher les offres
				foreach($categories as $x)
				{
					/** @var Offre :  on regarde dans l'offre */
					if ($offres)
						$o = $offres->getOffreByCategory($x['id']);
					if (!$o) continue;
					/** @var Forfait */
					if ($forfait = $o->getForfait())
						$jsForfaits[$o['categorie']] = array
						(
							 'categorie_choisie' => $o['categorie']
							,'mnem' => $o['mnem']
							,'prix_forfait'=>$forfait->getPrix()
							,'forfait'=>$forfait->doSerialize()
							,'franchise' => show_money($x['franchise'])
							,'franchise_rf' => show_money($x['franchise_rf'])
							,'franchise_af' => show_money($x['franchise_af'])
							
						);
					// :on cr�e la ligne "famille si n�cessiare
					if ($famille != $x['famille_nom'])
					{
						if ($famille)
							echo "</div>\n";
						// on affiche la famille suivante sauf pour le fun
						if ($display > 0)
							$display -= 1; //($x['famille_nom'] == 'Fun' ? $display : 1);
						if ($x['famille'] == $categorie['famille'])
							$display = 2;
						echo '<div class="offre_categorie">';
						echo '<h2 id="famille-'.$x['famille'].'" class="famille'.($display ? ' open' : '').'">'.$x['famille_nom'].'</h2>'."\n";
						$famille = $x['famille_nom'];
					}
					$class = 'famille-'.$x['famille'].' '.($o['nb'] > 0 ? 'cat-dispo' : 'cat-indispo');
					if ($reservation['categorie']==$x['id'])
						$class .= ' current';
					echo '<div id="cat-'.$x['id'].'" class="famille-categorie '.$class.'"';
					if (!$display) echo ' style="display: none;"';
					echo '>';
					echo '<p class="categorie_nom">';
					echo '<strong>'.$x['categorie_nom'].'</strong>';
					//if (SITE_MODE != 'PROD') echo '('.$o['nb'].')';
					echo '<span>'.$x['description'].'</span>';
					echo '</p>';
					echo '<div class="visu_prix">';
					echo '<img src="../'.$x->getImg('sml').'" alt="" />';
					if ($forfait && !$forfait->isLocapass()) {
						$str = '<span class="prix_toggle prix_total">'.$forfait->getPrix().' �'.'</span>';
						$str.= '<span class="prix_toggle prix_jour" style="display:none;">'.show_money($forfait->getPrixParJour()).' �'.'</span>';
					} else if (!$forfait || !$o['nb'])
						$str = 'n.d.';
					else // Locapss disponible
						$str = 'dispo';
					echo '<span>'.$str.'</span>';
					echo '</div>';
					echo '<div class="clear"></div>';
					echo '</div>'."\n"; // !categorie
				}
				if ($famille)
					echo '</div>'; // fermer la famille courante
				echo '</div>'."\n"; // !#offres_categories
			}
			// afficher le SmartCalendar si le forfait est d�fini et qu'il y a de la disponibilit� dans l'agence
			if ($offres = $reservation->getOffres())
				$offre = $offres->getOffreByCategory($reservation['categorie']);
			if ($reservation['type']=='vu')
			{
				$smartCalendar = SmartCalendar::factory($reservation, 3);
				if ($smartCalendar->hasDispo())
				{
					echo '<form id="frm_offre_calendar" action="" method="post" onsubmit="return false;">'."\n";
					echo hn(2, "Vous pouvez changer votre date de d�part.", "7f7f7f", 12);
					echo hn(2, "Choisissez le tarif le moins cher.", "7f7f7f", 12);
					echo $smartCalendar->toHtml($this->getTrackInfo('agence_search'));
					echo '</form>'."\n";
				} // SmartCalendar->hasDispo()
			} // $reservation['type']=='vu'
			?>
			<img src="../css/img/audiotel_p3-4.png" alt="Besoin d'aide ? Nos �quipes sont � votre disposition au 0899 46 46 36s" />
		</div>

		<div id="page">
		<?
			include($this->getPagePath());
		?>
		</div>
		<div class="clear"></div>
	</div>
</div>

<script type="text/javascript" language="javascript">
// <![CDATA[
	// informations sur les forfaits
	if (!forfaits)
		var forfaits = Array();
<?
	if (is_array($jsForfaits))
	{
		foreach($jsForfaits as $k => $f)
			echo "\t".'forfaits['.$k.'] = '.json_encode($f).';'."\n";
	}
?>
	var frmReservation = null;
	// onload
	$(document).ready(function()
	{
		frmReservation = new formReservation(document.getElementById("form_modif_reservation"), <?=json_encode($reservation->getFields());?>, {});
		$('#depart, #retour, #h_depart, #h_retour, #t_distance').focus(function(){
			$('#form_modif_reservation p.valid').show();
		});
		$('#l_distance').change(function()
		{
			var v = $(this).val();
			if (v == '+')
			{
				$('#t_distance').attr('maxlength', 16).val('Saisissez vos km').keyup().show().select().focus().siblings('.valid').show();
			}
			else if (v == 'illimit�')
			{
				$('#distance').val('');
				$('#illimite').val('1');
			}
			else
			{
				$('#distance').val(v);
				$('#illimite').val('');
				$('#t_distance').hide();
				$(this).parent().submit();
			}
		});
		$('#t_distance').change(function()
		{
			var v = $.trim(this.value);
			if (v.match(/^\d{2,4}$/))
			{
				$('#distance').val(v);
				$('#illimite').val('');
			}
		}).keyup(function()
		{
			var v = $.trim(this.value);
			if (v.match(/^\d{1,4}$/))
			{
				$(this).attr('maxlength', 4);
				$(this).css('font-style','normal').css('color','black');
			}
			else
				$(this).css('font-style','italic').css('color','#555555');
		});
		$('#offre_categories .famille').click(function()
		{
			if ($(this).siblings('.current').length) return;
			$('#offre_categories .'+this.id).slideToggle('slow');
			$(this).toggleClass('open');
		});
		$('#offre_categories .cat-dispo').click(function(){
			// si c'est le "current" on arr�te
			if ($(this).hasClass('current')) return;
			// retrouver le forfait
			var fDst = forfaits[this.id.substring(4)];
			if (!fDst) return;
			// on supprime le current en r�cup�rant l'id pr�c�dent
			idSrc = $('#offre_categories .current').removeClass('current').attr('id');
			var fSrc = forfaits[idSrc.substring(4)];
			// mettre � jour le forfait
			updateForfait(fDst);
			$(this).addClass('current');
			if (fDst.categorie_choisie)
			{
				$('#categorie').val(fDst.categorie_choisie);
				$('#categorie_choisie').val(fDst.categorie_choisie);
			}
			// suivre les clics par NSP
			if (fSrc && fSrc.mnem)
			{
				sp_clic("N", "<?=$reservation['type'].'_cat'.$jsForfaits[$reservation['categorie']]['mnem'];?>", 'cat' + fSrc.mnem, 'cat' + fDst.mnem);
			}
		});
		// montrer les cat�gories disponibles si aucune n'est visible
		if (!$('#offre_categories .cat-dispo:visible').length || $('#offre_categories .cat-dispo:hidden').length < 4)
		{
			$('#offre_categories .offre_categorie:has(.cat-dispo:hidden) .famille').click();
		}
	});
	// mise � jour du forfait
	function updateForfait (f)
	{
		var frm = document.getElementById('frm_reservation_tarif');
		// mettre � jour et valider le formulaire de r�servation si besoin pour les demandes en provenance du calendrier
		if (f.depart && f.h_depart)
		{
			if (frm)
			{
				frm.forfait.value = f.forfait;
				frm.depart.value = f.depart;
				frm.h_depart.value = f.h_depart;
				frm.retour.value = f.retour;
				frm.h_retour.value = f.h_retour;
				frm.action = "";
				$('<div></div>').appendTo(document.body).addClass('ui-widget-overlay').css({width:$(document.body).width(), height:$(document.body).height()});
				$(frm).submit();
			}
			else
			{
				setHoraireMenu (document.getElementById('h_depart'), $('#depart').datepicker('setDate', parseDate(f.depart)).datepicker('getDate'), f.h_depart);
				setHoraireMenu (document.getElementById('h_retour'), $('#retour').datepicker('setDate', parseDate(f.retour)).datepicker('getDate'), f.h_retour);
				$('#form_modif_reservation').submit();
			}
			return;
		}
		// mettre � jour la partie de droite
		$.get('../api.php?method=forfait&forfait='+f.forfait, function(html)
		{
			$('#offre_courante').html(html);
			// si des prix par jour sont affich�s, on toggle le HTML ajout�
			if ($(".prix_toggle.prix_jour:visible").length) {
				$('#offre_courante .prix_toggle.prix_total').hide();
				$('#offre_courante .prix_toggle.prix_jour').show();
			}
			$('#reservation_impossible').html('');
			// s'assurer que les options sont visibles
			$('#offre_options').show();
		});
		// mettre � jour le forfait et le prix
		if (frm)
		{
			for(prop in f)
			{
				if (frm[prop] && frm[prop].value)
					frm[prop].value = f[prop];
			}
			// mettre � jour les donn�es variables
			$("#options_franchise span[data-var]").each(function()
			{
				var prop = $(this).attr('data-var');
				if (f[prop])
					$(this).html(f[prop]);
			});
			// mettre � jour le total
			updateTotal(frm.prix_forfait, 0, null);
		}
	}
	// parseDate
	function parseDate(str)
	{
		var a = str.split(/[\/\-\:\ ]/), d = new Date(a[2], a[1]-1, a[0], a[3]||0, a[4]||0, a[5]||0);
		return (!isNaN( d ) && d.getYear()%100==a[2]%100 && d.getMonth()==(a[1]-1) && d.getDate()==a[0]) ? d : null;
	}
// ]]>
</script>
<? if ($offres) $offres->getForfaits()->dump(); ?>
