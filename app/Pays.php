<?

class Pays extends Ada_Object
{
	/**
	* Cr�e un objet � partir d'un identifiant
	* 
	* @param string $id
	* @return Pays
	*/
	public static function factory($id)
	{
		$x = new Pays();
		if ($x->load($id)->isEmpty())
			return null;
		return $x;
	}
	public function __toString()
	{
		return $this->getData('nom');
	}
}
?>