<?php
class ProspectResa extends Ada_Object {
	
	/**
	* Renvoie un enregistrement en particuleir
	* 
	* @param int $id
	* @return ProspectResa
	*/
	static public function factory($id) {
		$x = new ProspectResa();
		$x->load((int)$id);
		return $x;
	}
	/**
	* Cr�e un enregistrement ProspectResa et envoie un mail
	* 
	* @param AgenceResa $resa
	* @param AgenceClient $client
	* @param Agence $agenceChoisie
	* @param string $delai
	* @returns AgenceResa
	*/
	static public function create(AgenceResa $resa, AgenceClient $client, Agence $agenceChoisie, $delai) 
	{
		// cr�er les donn�es � ins�rer
		// on ne v�rifie pas les donn�es : cela a �t� fait pr�alablement
		$data = array_merge
		(
			array('id'=> ''),
			$resa->toArray(explode(',', 'agence,type,categorie,debut,fin,distance')),
			$client->toArray(explode(',', 'email,titre,prenom,nom,cp,tel')),
			array(
				'agence_choisie'	=> $agenceChoisie['id'],
				'delai'				=> $delai,
				'mnem'				=> $resa->getCategorie()->getData('mnem')
			)
		);
		$x = new ProspectResa($data);
		$x->save();
		
		// on pr�pare le mail et on l'envoie
		$data = array_merge
		(
			$data, 
			array
			(
				'date_envoi'	=> date('d/m/Y � H:i'),
				'agence_nom'	=> $resa->getAgence()->getData('nom'),
				'agence_choisie_nom'	=> $agenceChoisie['nom'],
				'type_label'			=> Categorie::$TYPE[$resa['type']],
				'categorie_label'		=> (string)$resa->getCategorie(),
				'depart_label'		=> $resa->getReservation()->getLongDate('debut'),
				'retour_label'		=> $resa->getReservation()->getLongDate('fin'),
				'distance_label'	=> ($resa['distance'] ? sprintf('%ld km', $resa['distance']) : ''),
				'url_resa'			=> Page::getURL('resas/', null, true)
			)
		);
		// pr�paration et envoi du mail
		$html = load_and_parse(BP.'/emails/em-resas-prospect.html', $data);
		send_mail($html, EMAIL_PROSPECT_RESAS, '[ADA] Demande de reservation indisponible', EMAIL_FROM, EMAIL_FROM_ADDR, '', '');
		return $x;
	}
	
	
}
?>
