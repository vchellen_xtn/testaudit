<?
/*
2016-01-12 : 
Ce code �tait appel� depuis admin/extraction.php
L'appel a �t� supprim�.
A priori ce n'est plus utilis�.
*/

// construit une ligne compta
function compta_getline(
		$r,
		$sens,		// facture ou avoir
		$date,		// date facturation ou avoir
		$montant	// montant
	)
{
	$ht = round($montant/(1+Tva::getRate($date)),2);
	$ref = $r['ref'];
	if (!$ref) $ref = ($sens=='F' ? resfacture($r) : resavoir($r) );
	$a = array(
		'FACTURE'		=> $ref
		,'AGENCE'		=> $r['agence']
		,'DATEFACTURE'	=> date('d/m/Y', $date)
		,'MOISFACTURE'	=> date('m/Y', $date)
		,'SENS'			=> $sens
		,'SOCIETE'		=> null		// 23/04/07 - non pr�sent dans la base
		,'NOM'			=> $r['nom']
		,'ADRESSE'		=> $r['adresse']
		,'TEL1'			=> $r['tel']
		,'TEL2'			=> $r['gsm']
		,'MODE'			=> 'CB'
		,'PRESTATION'	=> 'CDWEB'
		,'QTE'			=> '1'
		,'LIBELLE'		=> ($r['ref'] ? $r['ref'] : resid_client($r))
		,'PRIXHT'		=> sprintf('%.2f', $ht)
		,'MONTANTHT'	=> sprintf('%.2f', $ht)
		,'TVA'			=> (Tva::getRate($date)*100).'%'
		,'MONTANTTTC'	=> sprintf('%.2f', $montant)
		,'JOURS'		=> $r['jours']
		,'VEHICULE'		=> $r['type']
		,'CATEGORIE'	=> $r['categorie']
		,'KM'			=> $r['km']
	);
	return implode(";", $a)."\n";
}

// exporte les donn�es comptables
function compta_export($zone, $canal, $debut, $fin) {
	// la date de fin est incluse
	$fin .= ' 23:59:59';

	// requ�te SQL
	$sql = "SELECT r.id, r.agence, r.type, r.categorie, r.km km, r.jours, r.total, r.paiement, r.annulation, r.rembourse, r.ref";
	$sql.=", COALESCE(facture.id, r.num_fact) num_fact, UNIX_TIMESTAMP(r.facturation) t_facturation";
	$sql.=", COALESCE(avoir.id, r.num_fact) num_avoir, UNIX_TIMESTAMP(r.annulation) t_annulation";
	$sql.=", CONCAT(UPPER(c.nom),' ',UPPER(c.prenom)) nom\n";
	$sql.=", c.tel, c.gsm, CONCAT(c.f_adresse,' ',c.f_cp,' ',c.f_ville,' ',p.nom) adresse\n";
	$sql.=" FROM reservation r JOIN client c ON r.client_id=c.id JOIN pays p ON p.id=c.f_pays\n";
	$sql.="      JOIN agence a ON a.id=r.agence\n";
	$sql.="      LEFT JOIN facturation_ada facture ON (facture.reservation=r.id AND facture.type='F')\n";
	$sql.="      LEFT JOIN facturation_ada avoir ON (avoir.reservation=r.id AND avoir.type='A')\n";
	$sql.=" WHERE canal='".$canal."'\n";
	$sql.="    AND zone='".$zone."'\n";
	$sql.="    and (1=(CAST(paiement AS DATE) BETWEEN CAST('".$debut."' AS DATE) and CAST('".$fin."' AS DATE))\n";
	$sql.="      or 1=(CAST(annulation AS DATE) BETWEEN CAST('".$debut."' AS DATE) and CAST('".$fin."' AS DATE))\n";
	$sql.="        )\n";
	$sql.=' ORDER BY r.id';

//echo '<pre>'.$sql."</pre>\n";

	// ex�cuter la requ�te
	$debut = strtotime($debut);
	$fin = strtotime($fin);
	$rs = sqlexec($sql);
	$numero = 0;
//echo '<pre>';
	// exporter les donn�es
	while ($rs && $row = mysql_fetch_assoc($rs)) {
		$paiement = strtotime($row['paiement']);
		$annulation = strtotime($row['annulation']);
		$total = $row['total'];
		$rembourse = $row['rembourse'];
		
		// si le paiement est dans l'intervalle
		if ($paiement >= $debut && $paiement <= $fin) {
			// si l'annulation l'est �galement et que le remboursement est total ou non collect�e
			// on passe au suivant
//			if ($annulation >= $debut && $annulation <= $fin 
//				&& (!$rembourse || $rembourse==$total)) 
//				continue;
			echo compta_getline ($row, 'F', $paiement, $total);
			$numero++;
		}
		if ($annulation >= $debut && $annulation <= $fin)
		{
			// il s'agit d'une annulation qui n'est pas dans la m�me p�riode que le paiement
			// ou dont la velur est diff�rente
			echo compta_getline ($row, 'A', $annulation, ($rembourse ? $rembourse : $total));
			$numero++;
		}
	}
	return $numero;
//echo '</pre>';
}
?>