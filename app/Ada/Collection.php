<?
class Ada_Collection implements IteratorAggregate, Countable, ArrayAccess
{
	/** @var array */
	protected $_items = array();
	/** Item object class name @var string */
	protected $_itemClass = 'Ada_Object';
	/** @var bool */
	protected $_isLoaded = false;
	/** @var string */
	protected $_sql;
	
	public function __construct()
	{
		if (get_class($this) == 'Ada_Collection')
			$this->_setItemClass('Ada_Object');
		else
			$this->_setItemClass(str_replace('_Collection', '', get_class($this)));
		return;
	}
	
	public function isLoaded() { return $this->_isLoaded; }
	/**
	* @param bool $flag
	* @return Ada_Collection
	*/
	protected function _setLoaded ($flag = true)
	{
		$this->isLoaded = $flag;
		return $this; 
	}
	protected function _setItemClass ($itemClass)
	{
		$this->_itemClass = $itemClass;
		return $this;
	}
	/**
	* @param Ada_Object
	* @return Ada_Collection
	*/
	public function add(Ada_Object $item)
	{
		$this->_items[$item->getId()] = $item;
		return $this;
	}
	/**
	* @return array
	*/
	public function getIds()
	{
		return array_keys($this->_items);
	}
	/**
	* Load a collection from a SQL request
	* 
	* @param string $sql
	* @return Ada_Collection
	*/
	protected function _loadFromSQL($sql)
	{
		$this->_items = array();
		$this->_sql = $sql;
		$className = $this->_itemClass;
		$rs = sqlexec($sql);
		while ($row = mysql_fetch_assoc($rs))
			$this->add(new $className($row));
		$this->_setLoaded(true);
		$this->_afterLoad();
		return $this;
	}
	public function loadFromSQL($sql)
	{
		return $this->_loadFromSQL($sql);
	}
	/**
	*	Traitement apr�s le chargement
	*	@return Ada_Collection
	*/
	protected function _afterLoad()
	{
		return $this;
	}
	/**
	* Renvoie un objet de la collection
	* @param int $index
	* @return Ada_Object
	*/
	public function getItem($index)
	{
		return $this->_items[$index];
	}
	/**
	* Implementation of IteratorAggregate::getIterator()
	*/
	public function getIterator()
	{
		return new ArrayIterator($this->_items);
	}
	/**
	* * Retireve count of collection loaded items
	* @return int
	*/
	public function count()
	{
		return count($this->_items);
	}
	/**
	* * recup�re les attributs suppl�mentaires a ajouter � la collecion
	* @return array
	*/
	protected function _getAttributes()
	{
		$attributes = array('count' => $this->count());
		return $attributes;
	}
	/** @return bool */
	public function isEmpty() { return empty($this->_items); }
	/**
	* Convert object attributes to Symbolic Expression
	*
	* @param  array $arrAttributes array of required attributes
	* @return string
	*/
	protected function __toSExp(array $arrAttributes = array(), $rootName = null, $childName=null, $depth = 0)
	{
		$sxp = ''; $prefix = str_repeat("\t", $depth);
		if (!empty($rootName))
		{
			$sxp.= $prefix.'('.$rootName."\n";
			$prefix = str_repeat("\t", ++$depth);
		}
		foreach($this->_getAttributes() as $k => $v) 
			$sxp.= $prefix."($k \"$v\")\n";
		foreach ($this->_items as /** @var Ada_Object */ $item)
			$sxp .= $item->toSExp($arrAttributes, $childName, $depth);
		if (!empty($rootName))
		{
			$prefix = str_repeat("\t", --$depth);
			$sxp.= $prefix.")\n";
		}
		return $sxp;
	}
	/**
	* Public wrapper for __toSExp
	*
	* @param array $arrAttributes
	* @return string
	*/
	public function toSExp(array $arrAttributes = array(), $rootName = null, $childName = null, $depth = 0)
	{
		return $this->__toSExp($arrAttributes, $rootName, $childName, $depth);
	}
	/**
	* Convert object attributes to XML
	*
	* @param  array $arrAttributes array of required attributes
	* @return string
	*/
	protected function __toXml(array $arrAttributes = array(), $rootName = null, $childName=null)
	{
		$xml = '';
		if (!empty($rootName))
		{
			$xml .= '<'.$rootName;
			foreach($this->_getAttributes() as $k => $v) 
				$xml.= ' '.$k.'="'.utf8_encode($v).'"';
			$xml.= '>'."\n";
		}
		foreach ($this->_items as /** @var Ada_Object */ $item)
			$xml .= $item->toXml($arrAttributes, $childName, false, false);
		if (!empty($rootName))
			$xml.= "</$rootName>\n";
		return $xml;
	}
	/**
	* Public wrapper for __toSExp
	*
	* @param array $arrAttributes
	* @return string
	*/
	public function toXml(array $arrAttributes = array(), $rootName = null, $childName = null)
	{
		return $this->__toXml($arrAttributes, $rootName, $childName);
	}
	/**
	* Convert object attributes to JSON
	*
	* @param  array $arrAttributes array of required attributes
	* @return string
	*/
	protected function __toJson(array $arrAttributes = array())
	{
		$json = array();
		foreach ($this->_items as /** @var Ada_Object */ $item)
			$json[] = $item->toJson($arrAttributes);
		return '['.join(',', $json).']';
	}
	/**
	* Public wrapper for __toSExp
	*
	* @param array $arrAttributes
	* @return string
	*/
	public function toJson(array $arrAttributes = array())
	{
		return $this->__toJson($arrAttributes);
	}
	/**
	* Implementation of ArrayAccess::offsetSet()
	* @link http://www.php.net/manual/en/arrayaccess.offsetset.php
	* 
	* @param string $offset
	* @param mixed $value
	*/
	public function offsetSet($offset, $value)
	{
		$this->_items[$offset] = $value;
	}
	public function offsetExists($offset)
	{
		return isset($this->_items[$offset]);
	}
	public function offsetUnset($offset)
	{
		unset($this->_items[$offset]);
	}
	public function offsetGet($offset)
	{
		return isset($this->_items[$offset]) ? $this->_items[$offset] : null;
	}
}
?>
