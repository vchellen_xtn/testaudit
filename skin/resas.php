<?php
	/** @var Page_ResasBase */ $that = $this;
	/** @var Agence */ $agence = $that->getAgence();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?
	$that->writeHead();
?>
	<script type="text/javascript">
	/* <![CDATA[ */
	var appADA = appADA || {};
	appADA.baseUrl = '<?=Page::getURL(null, null, true);?>';

	/* ]]> */
	</script>
</head>
<body class="<?=str_replace('/', '-', dirname($that->getId())).' '.basename($that->getId());?>">
	<div id="container" class="container">
		<div id="header" class="header">
			<? if ($page == 'resas/index') echo '<h1>'; ?>
			<a href="<?=Page::getURL('resas/index.html', null, true)?>" class="logo">location de voiture et utilitaire - ADA.fr</a>
			<? if ($page == 'resas/index') echo '</h1>'; ?>
		</div>
		<div id="content" class="content">
			<? if ($agence) : ?>
				<h1 class="banner-title title-right">
					<a href="agence.html"><?= $agence['id'].' - '.$agence['nom'];?></a>
					<?
					$agences = array();
					if ($page == 'resas/index')
					{
						$user = $that->getResasUser();
						foreach($user->getAgences() as $a) {
							if ($a['id'] != $agence['id']) {
								// pour les utilisateurs non connect�s au back office on v�rifie que la soci�t� a acc�s � l'outil
								if ($user->hasRights(Acces::SB_ADMIN) || $that->isTeleConseillerLogged() || $user->getConfigData('oav_acces', 'code_societe', $a['code_societe'])) {
									$agences[$a['id']] = sprintf('%s - %s -  %s', $a['code_societe'], $a['id'], $a['nom']);
								}
							}
						}
					}
					?>
					<? if (count($agences)) : ?>
					<form id="frm_change_agence" action="" method="post"><? selectarray('changer_agence', $agences, $agence['id'], "---", ' onchange="if (this.value!=\'\') this.form.submit();"'); ?></form>
					<? endif; ?>
				</h1>
				<h2 class="banner-title title-left">Outil de vente</h2>
			<? endif; ?>
			<?
				// maintenance et t�l�-conseiller
				$that->writeInfo();
				// inclure la page courante
				include($that->getSkinnedPagePath());
			?>
		</div>
		<?php if ($user = $that->getResasUser()) : ?>
		<div id="footer" class="log-info">
			<strong><?=$user['login']; ?></strong><br/>
			<?=$user->getRole(); ?><br/>
			<a href="<?=Page::getURL('resas/index.html?logout', null, true);?>">d�connexion</a>
		</div>
		<?php endif; ?>
	</div>
<?
	// informations de mise au point
	if (defined('SQL_PROFILE') && SQL_PROFILE) {
		$this->writeDebugInfo();
	}
	$that->writeJSStats();
?>
</body>
</html>