<?
/**
* Cr�� par Forfait_Collection::factory()
* Permet d'acc�der aux donn�es calcul�es � partir des tables
* 	- forfait
* 	- forfiat_tarif (saison, r�seau...)
* 	- stop_sell (pour interdire la vente d'un forfait)
*	- agence_promo_forfait (ex promotion_forfait)
* Les donn�es sont :
	[id]			identifiant du forfait
	[nom]			nom du forfait (non affichable)
	[type]			type de v�hicule
	[categorie]		cat�gorie du v�hicule
	[jours]			nombre de jours inclus dans lforfait
	[jours_max]		nombre de jours maximum pour ce forfait
	[km]			km inclus dans le forfait
	[km_jour]		km suppl�mentaire par jour suppl�mentiare (entre jours et jours_max)
	[km_sup]		prix du km suppl�mentaire
	[nopromo]		si le forfait et/ou la cat�gorie n'accepteent pas de promotions
	[prix]			prix du forfait
	[prix_jour]		prix de la journ�e suppl�mentaire
	[prix_origine]	local ou national
	[p_id]			identifiant de la promotion
	[p_agence]		% (nationale) ou code agence
	[p_classe]		promotion associ�e � une "classe" de promotion
	[p_mode]		P(ourcentage) ou M(on�taire)
	[p_montant]		montant du remboursement � interpr�ter en fonction du mode
	[p_km_sup]		prix du km suppl�mentaire dans la promotion
	[p_prix_jour]	prix de la journ�e suppl�mentaire
	[p_fin]			date de fin de la promotion
	[p_yield]		variation du yield
	[p_yield_desc]	description du yield
	[non_borne]		1: sans contrainte sur les jours de d�part et de retour
	[duree]			nombre de jours calcul�s
	[p_prix]		prix avec la promotion
	[t_prix]		prix final
	[reduction]		montant de la r�duction
	[yield]			montant de la variation d� au yield
	[illimite]		le km demand� est-il illimit� ?
	[coupon]		identifiant du code r�duction / code pro saisi
	[forfait_type]	type de forfait (LOCAPASS, ADAFR ou JEUNES) et RESAS pour celles qui ne doivent pas �tre enregistr�es dans "reservation"
* NEGOCIATIONS
* 	[n_id]			identifiant de la n�gociation
* 	[n_promotion]	promotion ayant permis d'obtenir le prix
* 	[n_prix]		prix minimal pour la n�gociation
* 	[n_step]		1: au d�part, +1 � chaque proposition jusqu'� PromotionNegociation::STEP_MAX
*	[nego]	montant de la r�duction obtenue par la n�gociation
* RESAS
* 	[operation]			DEVIS, RESA ou PREPA
* 	[local_forfait]		identifiant du forfait local s�lectionn�
* 	[local_nom]			nom du forfait local
* 	[local_km]			kilom�tre forfait local
* 	[local_prix_origine] origine du tairf local s'il a �t� red�fini
* 	[local_tarif_ttc]	tarif TTC calcul� avec les tarifs locaux (agence_forfait_tarif)
	[local_promo_ttc]	promotion calcul�e � partir de local_tarif_ttc et agence_promo_forfait
	[local_promo_id]	identifiant de la promotion
	[local_promo_nom]	nom de la promotion
	[local_promo_type]	type de promotion
	[local_promo_yield]	bornes du Yield s�lectionn�
	[local_yield_ttc]	prix apr�s application Yield
	[local_yield]		variation du yield
	[resas_tarif_ttc]	tarifs indiqu�es dans /resas/
	[unipro_vehicule]	immatriculation du v�hicule � s�lectionner
	[grf_prix]			prix de la GRF
*/

class Forfait extends Ada_Object
{
	const BTN_CHANGE = 1;
	const BTN_BOOK = 2;
	
	protected $_displaySurcharge = false; // affiche-t-on ou pas le montant de la surcharge
	protected $_displayButtons = 0; // quels boutons affiche-t-on ?
	
	/** @var Agence */
	protected $_agence;
	/** @var Categorie */
	protected $_categorie;
	
	/** @param bool @returns Forfait */
	public function setSurcharge($flag = true) { $this->_displaySurcharge = $flag; return $this; }
	/** @param bool @returns Forfait */
	public function setButtons($flag) { $this->_displayButtons = $flag; return $this; }
	public function getButtons() { return $this->_displayButtons; }
	public function hasButton($btn) { return ($this->_displayButtons & $btn);}
	public function getSurcharge()
	{
		if (!$this->_displaySurcharge || $this->isLocapass()) return;
		if ($this->_agence)
		{
			return round($this->_agence->getSurcharge($this->_data['type']));
		}
	}

	/** @returns Forfait */
	public function setCategorie(Categorie $categorie) { $this->_categorie = $categorie; return $this; }
	/** @returns Categorie */
	public function getCategorie()
	{
		if (!$this->_categorie && $this->_data['categorie'])
			$this->_categorie = Categorie::factory($this->_data['categorie'], '*', $this->_data['forfait_type']=='JEUNES');
		return $this->_categorie; 
	}
	/** @returns Forfait */
	public function setAgence(Agence $agence) 
	{ 
		$this->_agence = $agence; 
		$this->_data['agence'] = $this->_agence->getId();
		return $this; 
	}
	/** @returns Agence */
	public function getAgence()
	{ 
		if (!$this->_agence && $this->_data['agence'])
			$this->_agence = Agence::factory($this->_data['agence']);
		return $this->_agence; 
	}
	/**
	* Type de forfait LOCAPASS
	* @returns bool
	*/
	public function isLocapass()
	{
		return ($this->_data['forfait_type']=='LOCAPASS');
	}
	/**
	* Y-a-t-il une promotion ?
	* @returns bool
	*/
	public function hasPromo()
	{
		return (!$this->isLocapass() && ($this->getPrix() < $this->getPrixBarre()));
	}
	/**
	* Y-a-t-il une n�gociation ?
	* @returns bool
	*/
	public function hasNego() {
		// il doit y avoir une n�gociation et elle ne doit pas avoir d�j� eu lieu...
		if (!$this->isLocapass() && isset($this->_data['n_id']) && !$this->_data['nego']) {
			// l'�cart de prix est-il suffisant pour l'enregistrer ?
			return (PromotionNegociation::isEcartSuffisant($this->_data['t_prix'], $this->_data['n_prix']));
		}
		return false;
	}
	/**
	* Renvoyer le prix minimal pour la n�gociation
	* @returns double
	*/
	public function getPrixNego() {
		if ($this->hasNego()) {
			return $this->_data['n_prix'];
		}
		return $this->getPrix();
	}
	
	/** @returns double */
	public function getPrix() { 
		return (isset($this->_data['resas_tarif_ttc']) ? $this->_data['resas_tarif_ttc'] : $this->_data['t_prix']); 
	}
	public function getPrixParJour() {
		return round($this->getPrix() / max(1, $this->_data['duree']), 2);
	}
	/** @returns double */
	public function getPrixBase() { 
		return (isset($this->_data['resas_tarif_ttc']) ? $this->_data['resas_tarif_ttc'] : $this->_data['prix']); 
	}
	
	/** @returns double */
	public function getPrixBarre() { 
		return (isset($this->_data['resas_tarif_ttc']) ? $this->_data['resas_tarif_ttc'] : round($this->getPrix() + $this->getReduction())); 
	}
	public function getPrixBarreParJour() {
		return round($this->getPrixBarre() / max(1, $this->_data['duree']), 2);
	}
	public function getPrixKmSupp()
	{
		if (!$this->_data['km'])
			return;
		return (isset($this->_data['km_sup']) ? $this->_data['km_sup'] : $this->getCategorie()->getData('km_sup'));
	}
	public function getReduction() {
		if ($this->_data['coupon'] && $this->_data['reduction']) {
			// s'il y a un code de r�duction il faut toujours pr�tendre � une r�duction
			return ($this->_data['reduction'] + $this->_data['nego'] - min(0, $this->_data['yield']));
		} else if (((int)$this->_data['yield']) < ((int)$this->_data['reduction'] + (int)$this->_data['nego'])) {
			return ($this->_data['reduction'] + $this->_data['nego'] - $this->_data['yield']);
		}
		return 0;
	}
	public function getDuree()
	{
		$x = $this->_data['duree'];
		$x .= ($this->_data['type']=='ml' ? ' heure' : ' jour');
		if ($this->_data['duree'] > 1)
			$x .= 's';
		return $x;
	}
	public function getKm() { 
		if (isset($this->_data['local_km'])) {
			return (is_null($this->_data["local_km"]) ? 'km illimit�' : $this->_data['local_km'].' km'); 
		}
		return (is_null($this->_data["km"]) ? 'km illimit�' : $this->_data['km'].' km'); 
	}
	
	/**
	* Renvoie une description de la promotion
	* @returns string
	*/
	public function getPromoDescription()
	{
		// si pas de promotions
		if (!$this->hasPromo()) return;
		
		// nom de la promotion
		if ($this->_data['coupon'] && ($coupon = PartenaireCoupon::factory($this->_data['coupon'])))
		{
			$origine = $coupon['origine'];
			if ($origine=='webce' || $origine=='ce')
				return "Offre sp�ciale Comit� d'entreprise";
			if ($origine=='webpro')
				return "Offre sp�ciale Entreprise";
			if ($origine=='coupon')
				return 'Offre sp�ciale '.$coupon['nom'];
		}
		if ($this->_data['p_classe'])
			$nom = getsql('select nom from partenaire_classe where id='.(int)$this->_data['p_classe']);
		else if ($this->_data['p_agence'])
		{
			$nom = 'dans cette agence';
			return;
		}
		else
		{
			// on n'affiche pas le message pour les promos nationales
			$nom = 'nationale';
			return;
		}
		// affichage de la promotion
		$x = "Promotion $nom";
		if ($this->_data['p_fin']) 
			$x .= '<br/>Jusqu\'au '.$this->_data['p_fin'];
		$x .= ',<br/>�conomisez '.round($this->_data['p_montant']).($this->_data['p_mode']=='P' ? '%' : '�');
		return $x;
	}
	/**
	* Serialize() donn�es du forfait pour le passage d'un formulaire � l'autre
	* 
	*/
	public function doSerialize()
	{
		$listFields = 'id,agence,type,categorie,coupon,jours,km,prix,km_sup,p_id,p_km_sup,p_agence,p_classe,p_fin,p_mode,p_montant,duree,p_prix,t_prix,reduction,yield,illimite,forfait_type';
		if (isset($this->_data['n_id']))
			$listFields.=',n_id,n_promotion,n_prix,n_step,nego';
		if ($this->_data['forfait_type']=='RESAS')
			$listFields.= ',local_tarif_ttc,local_promo_ttc,local_forfait,local_km,local_promo_id,local_yield_ttc,local_yield';
		$fields = explode(',',$listFields);
		$a = $this->toArray($fields);
		$a['key'] = md5(PRIVATE_KEY.join('_',$a).session_id());
		return strtr(base64_encode(addslashes(gzcompress(serialize($a)))), '+/=', '-_,');
	}
	/**
	* Renvoie un Forfait
	* 
	* @param string $stuff
	* @returns Forfait
	*/
	static public function doUnserialize($stuff)
	{
		// on transforme la cha�ne en tableau
		$a = unserialize(gzuncompress(stripslashes(base64_decode(strtr($stuff, '-_,', '+/=')))));
		if (!is_array($a)) return;
		// on v�rifie la cl�
		$key = $a['key'];
		unset($a['key']);
		if ($key != md5(PRIVATE_KEY.join('_',$a).session_id()))
			return;
		// on renvoie un Forfait
		return new Forfait($a);
	}
	/**
	* Renvoie un nouveau forfait modifi�e par un Yield
	* 
	* @param AgencePromoYield $yield
	* @returns Forfait
	*/
	public function applyYield(AgencePromoYield $yield)
	{
		// recopier le forfait
		$x = new Forfait($this->_data);
		$x->_agence = $this->_agence;
		$x->_categorie = $this->_categorie;
		
		// pour les forfaits sans promotions
		if ($x['nopromo']) {
			return $x;
		}
		
		
		// maintenant on applique le Yield
		$t_prix = $x->getData('t_prix'); // prix avant yield
		$y_prix = $yield->getPrixYield($t_prix); // prix apr�s yield
		$x->setData('t_prix', $y_prix);
		$x->setData('p_yield', $yield['montant']);
		$x->setData('p_yield_desc', $yield->getDescription());
		$x->setData('yield', round($y_prix - $t_prix));
		return $x;
	}
	
	/**
	* Enregistre l'existence d'une n�gociation et ses param�tres
	* 
	* @param PromotionNegociation $negociation
	* @param Forfait $forfaitNego
	* @return Forfait
	*/
	public function initNegociation(PromotionNegociation $negociation, Forfait $forfaitNego) {
		// on v�rifie que c'est bien la m�me cat�gorie...
		if ($this->_data['categorie'] != $forfaitNego['categorie']) return $this;
		// s'il y a d�j� une n�gociation d'enregistr�e elle doit �tre meilleure pour prendre la place
		if (!isset($this->_data['n_id']) || ($this->_data['n_prix'] > $forfaitNego->getPrix())) {
			$this->setData('n_id', $negociation->getId()); // identifiant de la n�gociation
			$this->setData('n_promotion', $forfaitNego->getData('p_id'));//	promotion ayant permis d'obtenir le prix
			$this->setData('n_prix', $forfaitNego->getPrix()); // prix minimal pour la n�gociation
			$this->setData('n_step', PromotionNegociation::STEP_INIT); // 1: au d�part, +1 � chaque proposition jusqu'� PromotionNegociation::STEP_MAX
			$this->setData('nego', 0); // montant de la r�duction obtenue par la n�gociation
		}
		return $this;
	}
	/**
	* On modifie le prix suite � une n�gociation
	* 
	* @param int $proposition
	* @returns bool
	*/
	public function tryPrixNegociation($proposition) {
		if ($this->hasNego()) {
			$this->_data['n_step'] = $this->_data['n_step'] + 1;
			// On analyser la proposition
			$t_prix = $this->_data['t_prix'];
			if ($proposition >= $this->_data['n_prix'] && $proposition < $t_prix) {
				$this->_data['nego'] = $t_prix - $proposition;
				$this->_data['t_prix'] = $proposition;
				return true;
			}
		}
		return false;
	}
	/**
	* On propose un prix pour la n�gociation
	* @return int
	*/
	public function stopNegociation() {
		if ($this->hasNego()) {
			// On cr�e une proposition
			$this->tryPrixNegociation( round(($this->getPrix() + $this->getPrixNego())/2) );
		}
		return $this->getPrix();
	}
}
?>
