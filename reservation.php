<?
	session_start();
	include 'constant.php';

	// on v�rifie la "key"
	Page::checkKey('reservation');
	
	// on r�cup�re la r�servation, le forfait et les options pour d�terminer le prix 
	// avant d'enregistrer la r�servation
	$reservation = Reservation::create($a = $_POST, $msg, $url);
	if (!$reservation || !$reservation->hasForfait())
	{
		if ($url) Page::redirect($url);
		if ($msg)
			echo join('<br>', $msg);
		return;
	}
	$reservation['paiement_log'] = $_SERVER['REMOTE_ADDR']; // 2011-05-19 : pour comprendre le comportement
	if ($_COOKIE['ADA001_TC_MODE'])
	{
		$reservation['reservation'] = 'TEL';;
		$reservation['agent'] = $_COOKIE['ADA001_TC_UID'];
	}
	else if ($_COOKIE['ADA001_ORIGINE'])
		$reservation['agent'] = preg_replace('/[^a-z0-9\.\-\_]/i', '', $_COOKIE['ADA001_ORIGINE']);
	else if ($forfait['coupon'])
		$reservation['agent'] = $reservation->getPartenaireCoupon()->getData('code');
	// enregistrer la r�servation
	$reservation->save();
	$id = $reservation->getId();
	
	// tracking et redirection
	if ($_POST['categorie'] != $reservation['categorie'])
		$efficacite_up = '&efficacite_up=upsell_clique';
	// 'internaute
	$next_page = ($reservation['forfait_type']=='JEUNES' ? 'jeunes/inscription' : 'reservation/inscription');
	header('Location: '.Page::getURL($next_page.'.html?id='.$id.'&key='.Page::getKey($id).$efficacite_up, null, USE_HTTPS));
?>