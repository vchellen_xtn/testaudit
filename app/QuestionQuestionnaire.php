<?
class QuestionQuestionnaire extends Ada_Object
{
	/** @var QuestionQuestion_Collection */
	protected $_questions;
	
	/**
	* Cr�e un Questionnaire � partir d'un id
	*
	* @param string $id
	* @return QuestionQuestionnaire
	*/
	static public function factory($id)
	{
		$x = new QuestionQuestionnaire();
		if ($x->load($id)->isEmpty())
			return null;
		return $x;
	}
	/**
	* Renvoie la liste des questions d'un QuestionQuestionnaire
	*
	* @return QuestionQuestion_Collection
	*/
	public function getQuestions()
	{
		if (!$this->_questions)
			$this->_questions = QuestionQuestion_Collection::factory($this);
		return $this->_questions;
	}
	/**
	* Renvoie la liste des id des questions d'un QuestionQuestionnaire
	*
	* @return array
	*/
	/**
	* Renvoie le HTML d'un QuestionQuestionnaire
	*
	* @return string
	*/
	public function getHTML()
	{
		$output = <<<HTML
<form action="" method="post" class="questionnaire" id="questionnaire-{$this->_data['id']}">
	<input type="hidden" name="questionnaire" value="{$this->_data['id']}" />
HTML;
		$questions = $this->getQuestions();
		if ($questions)
		{
			$output.= $questions->getHTML();
			$output.= $questions->getJS();
		}
		$output.= <<<HTML
<script type="text/javascript">
<!--
$(document).ready(function()
{
	$.each(questionnaire_questions, function(fld, val)
	{
		$(':input[name="'+fld+'\\\\[\\\\]"], :input[name="'+fld+'-txt"]').change(function()
		{
			// pas de choix
			if (($(this).is(':radio') && $(this).is(':checked')) || $(this).is(':text, textarea'))
			{
				questionnaire_questions[fld] = 1;
				$(this).parents('.question').removeClass('error');
			} 
			else if (!questionnaire_questions[fld])
			{
				$(this).parents('.question').addClass('error');
			}
		});
	});
	$('#questionnaire-{$this->_data['id']}').submit(function()
	{
		$(':input[name^="question-"]').trigger('change');
		var count_errors = 0;
		$.each(questionnaire_questions, function(fld, val)
		{
			if (!val)
			{
				count_errors++;
			}
		});
		if (count_errors)
		{
			$('.error input').get(0).focus();
			return false;
		}
	});
});
//-->
</script>\n
HTML;
		$output.= '<p class="valid"><input class="btn" type="submit" value="valider" /></p>';
		$output.= '</form>';
		return $output;
	}
	/**
	* Enregistre les r�ponses d'un QuestionQuestionnaire
	* Renvoie un statut
	*
	* @param QuestionUser $questionUser
	* @param $data le POST des r�ponses
	* @param array $errors
	* @return boolean
	*/
	public function saveReponses(QuestionUser $questionUser, &$data, &$errors)
	{
		$reponses = array();
		// traitement des r�ponses
		foreach ($this->getQuestions()->getIds() as $qID)
		{
			if (!is_array($data['question-'.$qID]))
				$data['question-'.$qID] = array($data['question-'.$qID]);
			$reponse_txt = filter_var($data['question-'.$qID.'-txt'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE);
			foreach ($data['question-'.$qID] as $k => $v)
			{
				$reponse_item = (int)$v;
				if ($reponse_item || $reponse_txt)
				{
					$reponses[$qID] = array
					(
						'question' => $qID,
						'reservation' => $questionUser->getData('reservation'),
						'item' => $reponse_item, // item id
						'txt' => $reponse_txt,
					);
					save_record('question_reponse', $reponses[$qID]);
				}
				else
				{
					$errors[] = "Veuillez r�pondre � la question ".$qID;
				}
			}
		}
		if (!count($errors))
			$questionUser->setData('a_repondu', 1)->save('a_repondu');
		return !count($errors);
	}
	/**
	* Renvoie l'url d'un formulaire depuis un id Reservation
	*
	* @param int $resID
	* @return string
	*/
	static public function getURL($resID)
	{
		return 'contact/satisfaction.html?key='.self::_encryptData($resID);
	}
}