<?php
class AgenceAgglo_Collection extends Ada_Collection
{
	static public function factory($dept=null, $en_avant = false)
	{
		// pr�parer le SQL
		$sql = "select ag.* from agence_agglo ag";
		if ($dept && $en_avant)
			$sql.= " where ag.dept='$dept' or ag.en_avant=1";
		else if ($dept)
			$sql.=" where ag.dept='$dept'";
		else if ($en_avant)
			$sql.=" where ag.en_avant=1";
		$sql.=" order by ag.nom";
		
		// renvoyer la collection
		$x = new AgenceAgglo_Collection();
		$x->loadFromSQL($sql);
		return $x;
	}
}
?>
