<?
// $Id: lib.captcha.php,v 1.4 2011/12/08 15:41:39 gdubourguet Exp $
	// lib.captcha.php
	// par W. Dauchy 16.11.2007 (c) RND 
	// usage :
	// 		include "lib.captcha.php";
	// 		captcha_form(largeur_image, hauteur_image, nb_caract�res)
	// ou pour utiliser les valeurs par d�faut
	// 		captcha_form(); 
	// note : cliquer sur l'image charge une nouvelle image
	 
	// -- valeurs par d�faut
	define ("CAPTCHA_WIDTH", 105);
	define ("CAPTCHA_HEIGHT", 25);
	define ("CAPTCHA_SIZE", 6);
	define ("CAPTCHA_BACK_COLOR", "cfd4fd");
	define ("CAPTCHA_LINE_COLOR", "56679d");

	// si lib.captcha.php n'est pas appel� en direct, la font se trouve dans le m�me rep que lib.captcha.php
	if(basename(__FILE__) != basename($_SERVER['SCRIPT_NAME']))
		define("CAPTCHA_FONT", dirname(__FILE__).DIRECTORY_SEPARATOR."atomicclockradio.ttf");

	// -- modifiable
	define ("CAPTCHA_FONT", "atomicclockradio.ttf");
	define ("CAPTCHA_MARGIN", 4);
	define ("CAPTCHA_MAX_ANGLE", 12);

	// -- ne pas modifier
	define ("CAPTCHA_KEY", md5(date("dmY")));
	// probleme sur OVH avec LIB_PATH_HTTP, redefini dans constant.php pour l'ecraser
	define ("LIB_PATH_HTTP", str_replace("\\", "/", substr(dirname(__file__), strlen($_SERVER["DOCUMENT_ROOT"]))));
	define ("THIS_FILE", LIB_PATH_HTTP."/".basename(__file__));
	
	// v�rifier le font
	if (!is_file(CAPTCHA_FONT))
		die("Il faut copier la police de caract�res dans le r�pertoire du projet : ".CAPTCHA_FONT);

	// v�rifier GD
	if (!function_exists(imagegd))
		die("La biblioth�que GD2 est n�cessaire pour cette fonctionnalit�");

	// afficher l'image
	if ($code = $_GET["code"])
	{
		header("Content-type: image/jpeg");
		captcha_get_image($code, $_GET["w"], $_GET["h"], $_GET["s"]);
		exit();
	}
	
	// v�rifier le formulaire
	if (isset($_GET["captcha_key"]) && isset($_GET["captcha_text"]))
	{
		header("Content-type: text/plain");
		die(captcha_check() ? "" : "le cryptogramme visuel est incorrect !");
	}
	
	// changer d'image
	if ($_GET["reload"])
	{
		$code = md5(rand());
		$key = md5(strtoupper(substr(md5($code.CAPTCHA_KEY), 0, $_GET["reload"])).str_rot13(CAPTCHA_KEY));
		die($code.",".$key);
	}
	
	// constituer le formulaire
	function captcha_form($width=CAPTCHA_WIDTH, $height=CAPTCHA_HEIGHT, $size=CAPTCHA_SIZE)
	{
		$code = md5(rand());
		echo '<table class="captcha_table"><colgroup><col width="'.$width.'" /><col width="*" /></colgroup><tr>'."\n";
		echo '<td valign="bottom">';
		echo '<img onclick="captcha_reload('.$width.','.$height.','.$size.')" id="captcha_image" src="'.THIS_FILE.'?code='.$code.'&w='.$width.'&h='.$height.'&s='.$size.'" border="0" title="Cliquez pour charger une nouvelle image" />';
		echo '</td>'."\n";
		echo '<td valign="bottom">entrez le code ci-contre :<br />'."\n";
		echo '<input type="text" name="captcha_text" id="captcha_text" maxlength="'.$size.'" /></td>'."\n";
		echo '</tr></table>'."\n";
		$code = strtoupper(substr(md5($code.CAPTCHA_KEY), 0, $size));
		$key = md5($code.str_rot13(CAPTCHA_KEY));
		echo '<input type="hidden" name="captcha_key" id="captcha_key" value="'.$key.'" />'."\n";
	}
	
	// v�rifier
	function captcha_check()
	{
		return ($_REQUEST["captcha_key"] == md5(strtoupper($_REQUEST["captcha_text"]).str_rot13(CAPTCHA_KEY)));
	}
	
	// captcha_get_image
	function captcha_get_image($code, $width, $height, $size, $back_color=CAPTCHA_BACK_COLOR, $line_color=CAPTCHA_LINE_COLOR)
	{	
		$code = strtoupper(substr(md5($code.CAPTCHA_KEY), 0, $size));
	    $font = dirname(__file__)."/".CAPTCHA_FONT;
	    $im = @imagecreate($width, $height) or die("GD : impossible de cr�er une image");
	    imagecolorallocate($im, r($back_color), g($back_color), b($back_color)); // d�finit la couleur de fond

		// dessine le quadrillage
		$gd_color = imagecolorallocate ($im, r($line_color), g($line_color), b($line_color));
		imagesetstyle ($im, array($gd_color));
		for ($y=5; $y<$height; $y+=10)
			imageline ($im, 0, $y, $width, $y, IMG_COLOR_STYLED);
		for ($x=5; $x<$width; $x+=20)
			imageline ($im, $x, 0, $x, $height, IMG_COLOR_STYLED);

		// �crit le texte
		for ($i=0, $x=CAPTCHA_MARGIN, $len=strlen($code); $i<$len; $i++)
		{
			$c = substr($code, $i, 1);
			$font_max = get_font_max($font, ($width - $x)/ ($len - $i) - CAPTCHA_MARGIN, $c);
			$font_size = rand($font_max, $font_max + $len - $i);
			$angle = rand(-CAPTCHA_MAX_ANGLE, CAPTCHA_MAX_ANGLE);
		    $gd_color = imagecolorallocate($im, 16 * rand(1, 8), 16 * rand(1, 8), 16 * rand(1, 8));
			$size = get_text_size($font, $font_size, $angle, substr($code, $i, 1));
			$y = rand($size[1] + CAPTCHA_MARGIN, $height - CAPTCHA_MARGIN);
		    imagettftext($im, $font_size, $angle, $x, $y, $gd_color, $font, $c);
			$x += $size[0] + CAPTCHA_MARGIN;
		}
		
		// contenu jpg
		imageJPEG($im);
		return substr($code, 0, $i);
	}

	// get_font_max
	function get_font_max ($font, $width, $char)
	{
		// calcule la taille maxi de la police
		for ($font_max=4;; $font_max++)
		{
			$size = get_text_size($font, $font_max + 1, CAPTCHA_MAX_ANGLE, $char);
			if ($size[0] > $width)
				return $font_max;
		}
		return null;
	}
	
	// get_text_size
	function get_text_size ($font, $font_size, $angle, $text)
	{
		$box = imagettfbbox($font_size, 0, $font, $text);
		$w = $box[2] - $box[0];
		$h = $box[1] - $box[5];
		$sa = abs(sin($angle * pi() / 180));
		$ca = abs(cos($angle * pi() / 180));
		return array($w * $ca + $h * $sa, $w * $sa + $h * $ca);
	}

	function r($color) { return hexdec(substr($color, 0, 2)); }
	function g($color) { return hexdec(substr($color, 2, 2)); }
	function b($color) { return hexdec(substr($color, 4, 2)); }

function captcha_js ()
{
?>

<script language="javascript">

	// surcharger le onload
	onload_fn = window.onload;
	window.onload = function()
	{
		if (onload_fn)
			onload_fn();
		captcha_init();
	}
	function captcha_init()
	{
		// retrouver le form qui contient le captcha
		for (var captcha_frm=null, i=0; i<document.forms.length; i++)
		{
			if (document.forms[i].elements["captcha_text"])
			{
				captcha_frm = document.forms[i];
				break;
			}
		}
		
		if (captcha_frm)
		{
			// remplacer le submit
			submit_fn = captcha_frm.onsubmit;
			captcha_frm.onsubmit = function()
			{
				if (req = (window.ActiveXObject)? new ActiveXObject("Microsoft.XMLHTTP") : new XMLHttpRequest())
				{
					req.onreadystatechange = function()
					{
						if (req.readyState == 4 && req.status == 200) 
							doCheckCaptcha(req.responseText);
					};
					url = "<?=THIS_FILE?>?captcha_key=" + captcha_frm.captcha_key.value + "&captcha_text=" + captcha_frm.captcha_text.value;
					req.open("GET", url, true);
					req.send(null);
				}
				return false;
			}
			
			// doCheckCaptcha
			function doCheckCaptcha(err)
			{
				if (err && err.length)
				{
					alert(err);
					return false;
				}
		
				captcha_frm.onsubmit = submit_fn;
				if (!submit_fn || captcha_frm.onsubmit())
					captcha_frm.submit();
			}
		}
	}

	// captcha_reload
	function captcha_reload(width, height, size)
	{
		if (req = (window.ActiveXObject)? new ActiveXObject("Microsoft.XMLHTTP") : new XMLHttpRequest())
		{
			req.onreadystatechange = function()
			{
				if (req.readyState == 4 && req.status == 200) 
				{
					code = req.responseText.split(',');
					document.getElementById("captcha_image").src = "<?=THIS_FILE?>?code=" + code[0] + "&w=" + width + "&h=" + height + "&s=" + size;
					document.getElementById("captcha_key").value = code[1];
				}
			};
			url = "<?=THIS_FILE?>?reload=" + size;
			req.open("GET", url, true);
			req.send(null);
		}
	}
	
</script>
<?
}
?>
