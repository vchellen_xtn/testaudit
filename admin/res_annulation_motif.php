<?
	require_once("secure.php"); 
	require_once("lib.php");
	// identifiant
	$id = getarg("id");
	
	// suppression
	if ($id && getarg("delete")) 
	{
		$sql = "delete from res_annulation_motif where id = '".addslashes($id)."'";
		getsql($sql);
		unset($id);
		// d�calage des autres positions suite � la suppression
		$sql_pos = "update res_annulation_motif set position = position-1 where position >= ".$_POST['position'];		
		getsql($sql_pos);
	}
	// cr�ation
	if (getarg("create"))
	{
		save_record('res_annulation_motif', $_POST);
		// d�calage des autres positions suite � l'ajout
		$sql_pos = "update res_annulation_motif set position = position+1 where position >= ".$_POST['position']." and id !='".$_POST['id']."'";
		getsql($sql_pos);
	}
	// modification
	if (getarg("update"))
	{
		$rs=sqlexec("select position from res_annulation_motif where id='".$_POST['id']."'"); 
		$old_pos = mysql_result($rs,0);
		save_record('res_annulation_motif', $_POST);
		// d�calage des autres positions
		if ($_POST['position']!=$old_position)
		{
			$sql_pos1 = "update res_annulation_motif set position = position-1 where position >= ".$old_pos." and id !='".$_POST['id']."'";
			$sql_pos2 = "update res_annulation_motif set position = position+1 where position >= ".$_POST['position']." and id !='".$_POST['id']."'";
			getsql($sql_pos1);
			getsql($sql_pos2);			
		}
	}
	if ($id)
	{
		$rs = sqlexec("select * from res_annulation_motif where id = '".addslashes($id)."' ");
		if (!$rs || !($row = mysql_fetch_assoc($rs)))
			die("Aucun enregistrement correspondant.");
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Outils d'administration</title>
	<meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
	<link rel="stylesheet" href="style.css">
	<script language="javascript" src="../scripts/script.js"></script>
	<script language="javascript" src="admin.js"></script>
	<script>
	<!-- //

	// doDelete
	function doDelete(frm)
	{
		return (confirm("Voulez-vous r�ellement supprimer cet �l�ment ?\nCette op�ration est irr�versible."));
	}
	
	// doSubmit
	function doSubmit(frm)
	{
		var err = "";
		if(!frm.id.value.length)
			err+= "Veuillez indiquer un Id\n";
		if(!frm.position.value.length)
			err+= "Veuillez indiquer la position\n";
		if(!frm.libelle.value.length)
			err+= "Veuillez indiquer le libell�\n";
		if (err.length)
		{
			alert("Attention !\n" + err);
			return false;
		}
		return true;
	}
	// -->
	</script>
</head>
<body>
<form action="" method="post">
<table class="framed" border="0" cellspacing="0" cellpadding="4">
<tr>
	<th class="bg_titre">
		<img src="img/puce.gif" align="absmiddle" hspace="1">
		Reservation Annulation Motif
	</th>
</tr>
<tr>
	<td>
		<table class="noborder" width="100%" cellspacing="0" cellpadding="2">
		<col width="15%">
		<col width="35%">
		<col width="15%">
		<col width="35%">
		<tr>
			<td><label for="id">ID</label></td>
			<td>
				<?=$id?>
				<input type="<?=($id ? 'hidden' : 'text')?>" maxlength="10" name="id" id="id" value="<?=$id?>"/>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td><label for="position">Position</label></td>
			<td>
				<input type="text" id="position" name="position" value="<?=$row["position"]?>" maxlength="3" size="3">
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		
		<tr>
			<td><label for="actif">Actif</label></td>
			<td>
			<?
				$a = array('non','oui');
				selectarray('actif', $a, $row['actif'], null, null);
			?>
			</td>
			<td colspan="2">&nbsp;</td>
		</tr>
		
		<tr>
			<td><label for="partiel">Partiel</label></td>
			<td>
			<?
				$a = array('non','oui');
				selectarray('partiel', $a, $row['partiel'], null, null);
			?>
			</td>
			<td colspan="2">Si partiel, la r�servation ne sera pas annul�e</td>
		</tr>
		<tr>
			<td><label for="libelle">Libell�</label></td>
			<td colspan="3"><input type="text" id="libelle" name="libelle" value="<?=$row["libelle"]?>" maxlength="128" size="64"></td>
		</tr>
		<tr>
			<td><label for="explications">Explications</label></td>
			<td colspan="3">
				<textarea id="explications" name="explications"><?=$row["explications"]?></textarea>
				Texte ins�r� dans l'e-mail d'annulation
			</td>
		</tr>
		
		
		
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%">
		<tr>
			<td align="right">
				<? 
					if ($id || $adminUser->hasRights(Acces::SB_ADMIN)) { // seuls les administrateurs peuvent cr�er une nouvelle option 
				?>
						<input type="submit" name="<?=$id ? "update" : "create"?>" value="enregistrer" onclick="return doSubmit(this.form);">
				<?
					}
					if ($id && $adminUser->hasRights(Acces::SB_ADMIN)) 
					{
				?>
						&nbsp;
						<input type="submit" onclick="return doDelete(this.form);" name="delete" value="supprimer">
				<?
					}
				?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
</form>
</body>
</html>