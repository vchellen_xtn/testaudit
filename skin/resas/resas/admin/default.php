<?php
	/** @var Page_Resas_Admin */ $x = $this;
?>
<h2 class="banner-title title-admin">Gestion des offres locales</h2>
<div class="container-menu">
	<?php echo $x->getMenu(); ?>
</div>
<div class="page">
<?php
	include($this->getPagePath());
?>
</div>
<div class="clear"></div>
