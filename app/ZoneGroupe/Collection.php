<?
class ZoneGroupe_Collection extends Ada_Collection
{
	/**
	* Groupes d'une zone géographique
	* 
	* @param mixed $zone
	* @return ZoneGroupe_Collection
	*/
	static public function factory($zone)
	{
		$x = new ZoneGroupe_Collection();
		$sql = "select * from zone_groupe where zone='$zone' order by nom";
		$x->loadFromSQL($sql);
		return $x;
	}
}
?>
